#pragma once

// STL include(s):
#include <vector>

// EDM include(s):
#include "AsgTools/AsgMessaging.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"

#include "KinematicFitTool/KinematicFitTool.h"

// ROOT include(s):
#include "TLorentzVector.h"
#include "TString.h"

// Forward declarations
namespace TMVA { class Reader; }
namespace HG { class EventHandler; class TruthHandler; }
namespace xAOD { class hhWeightTool; class TEvent; }

// Taken from xgboost/c_api.h, mut be identical!
typedef void *BoosterHandle;

namespace HG {
  /*!
     @class HHyybbTool
     @brief The main class for the HH -> yybb analysis.

     This tool, included in the HGamAnalysisFramework, contains all the functions designed
     specifically for the HH -> yybb analysis.
   */
  class HHyybbTool : public asg::AsgMessaging {
  public:
    /**
     * @brief Constructor
     * @param eventHandler The HGam eventHandler to write information to EventInfo
     * @param truthHandler The HGam truthHandler to read truth information
     */
    HHyybbTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler);

    /**
     * @brief Destructor
     */
    virtual ~HHyybbTool();

    /*!
       @fn virtual EL::StatusCode initialize(Config &config)
       @brief Set or fetch default values for members from config
       @param config Set of options
     */
    virtual EL::StatusCode initialize(Config &config);

    /**
     * @brief Function to save all the yybb information (Nominal by default)
     * @details
     *
     * @param photons Container with all photons
     * @param electrons Container with all electrons (before overlap removal)
     * @param muons Container with all muons (before overlap removal)
     * @param jets Container with all jets
     * @param jets_no_JVT Container with all jets (before JVT cut)
     */
    void saveHHyybbInfo(const xAOD::PhotonContainer &photons,
                        const xAOD::ElectronContainer &electrons,
                        const xAOD::MuonContainer &muons,
                        xAOD::JetContainer &jets,
                        const xAOD::JetContainer &jets_no_JVT,
                        const xAOD::MissingETContainer &met);

    /*!
       @fn EL::StatusCode writeContainers()
       @brief Write jet containers to the output files
     */
    EL::StatusCode writeContainers();


    /*!
       @enum jetPreselectionEnum
       @brief Choice of the WP to use for the preselection
       @var WP77
       @brief 0  = select at least 1 WP77 DL1r b-tagged jet
       @var WP85
       @brief 1  = select at least 2 WP85 DL1r b-tagged jet
     */
    enum jetPreselectionEnum {WP77 = 0, WP85  = 1};

    /*!
       @enum CorrectionEnum
       @brief  correction strategy enumeration
       @var noCor
       @brief 0 = do not correcte jet
       @var BJetCal
       @brief 1 = correct the jet 4v BJetCalibration Tool https://gitlab.cern.ch/mobelfki/BJetCalibrationTool
       @var BJetReg
       @brief 2 = correct the jet 4v BJetRegression Tool  https://gitlab.cern.ch/jpearkes/bjetregressiontool
     */
    enum CorrectionEnum {noCor = 0, BJetCal = 1, BJetReg = 2, KF = 3};

    /*!
       @enum analysisSelectionEnum
       @brief analysis selection category enumeration
       @var cutBased
       @brief 0 = simple cut based analysis
     */

    enum analysisSelectionEnum { cutBased = 0 };


    /*!
       @enum yybbCutflowEnum
       @brief Latest step in the cutflow that is passed by the event
     */
    enum yybbCutflowEnum {PHOTONS = 0, LEPVETO = 1, CENJETSMIN = 2, CENJETSVETO = 3, BJETSMIN = 4, BJETSVETO = 5, PASSPRESEL = 6};


  private:
    HG::EventHandler *m_eventHandler; //!
    HG::TruthHandler *m_truthHandler; //!
    std::unique_ptr<xAOD::hhWeightTool> m_hhWeightTool; //!
    BoosterHandle             *m_xgboost_yybb_highMass; //!
    BoosterHandle             *m_xgboost_yybb_lowMass; //!
    BoosterHandle             *m_xgboost_yybb_noHT_highMass; //!
    BoosterHandle             *m_xgboost_yybb_noHT_lowMass; //!
    BoosterHandle             *m_xgboost_yybb_noMbb_highMass; //!
    BoosterHandle             *m_xgboost_yybb_noMbb_lowMass; //!
    BoosterHandle             *m_xgboost_yybb_withTop_highMass; //!
    BoosterHandle             *m_xgboost_yybb_withTop_lowMass; //!
    BoosterHandle             *m_xgboost_yybb_vbf; //!

    KinematicFitTool          *m_kinematicfitTool; //!

    TMVA::Reader  *m_readerMVA_yy; //!
    TMVA::Reader  *m_readerMVA_ttH; //!

    // BDT input variables
    float m_bb_ini, m_yyjj_tilde_ini, pT_bb_ini, pT_yy_ini;
    float deltaR_yy_ini, deltaR_bb_ini, deltaR_yybb_ini, deltaPhi_yy_ini, deltaPhi_bb_ini, deltaPhi_bbyy_ini;
    float Rap_yy_ini, Rap_bb_ini, deltaRap_bbyy_ini, N_j_ini, N_j_btag_ini;
    float HTall_30_ini, pT_bbyy_ini, MET_ini;

    // Algorithm setup
    bool m_detailedHHyybb;       //! Are we running a fully detailed HHyybb analysis?
    bool m_specificHHyybb;       //! Only save speficic variables
    HG::StrV m_specificVariables; //! Which variables to save.

    // User-configurable: jet selection and muon corrections
    float m_jet_eta_max;         //! Maximum eta of jets
    float m_muon_pTmin;          //! Minimum pT of muons
    float m_muon_DRmax;          //! Maximum deltaR between muons and the jet
    float m_electron_pTmin;      //! Minimum pT of muons
    float m_electron_DRmax;      //! Maximum deltaR between muons and the jet

    // Jet pT cuts
    float m_j1_pTMin; //! (in GeV)
    float m_j2_pTMin; //! (in GeV)

    // Mass cuts
    float m_massHiggs; //! what we consider the exact Higgs mass to be (in GeV)
    float m_mjj_min;   //! lower bound of mbb window (in GeV)
    float m_mjj_max;   //! upper bound of mbb window (in GeV)
    float m_myy_min;   //! lower bound of tight myy window (in GeV)
    float m_myy_max;   //! upper bound of tight myy window (in GeV)

    // Jet related quantities required for perform selections
    int m_nJets; //! number of jets
    float m_HTJets; //! scalar sum pT of all jets
    int m_nBJets85; //! number of b-jets at the 85% wp
    int m_nBJets77; //! number of b-jets at the 70% wp
    int m_nCentralJets; //! number of central jets
    float m_weightBTagging; //! event weight from b-tagging
    float m_min_ChiWt; //! min ChiWt of all jets

    // Maps for per-event outputs
    std::map<TString, float> m_eventFloats;
    std::map<TString, int>   m_eventInts;
    // only written out in the case of yybb-Detailed
    std::map<TString, float> m_eventFloatsDetailed;

    // Vectors of aux data to save as jet decorations (some slimming wrt the original number of decorations is needed)
    std::vector<std::string> m_auxIntsToSave;
    std::vector<std::string> m_auxFloatsToSave;
    std::vector<std::string> m_auxDoublesToSave;

    // Which selections are we running (determined by the m_detailedHHyybb flag)
    std::vector<jetPreselectionEnum> m_jetPreselections;
    std::vector<CorrectionEnum> m_Corrections;
    std::vector<analysisSelectionEnum> m_analysisSelections;

    bool m_passpreselection; //!

    // XGBoost BDT score
    double m_score;

    /**
     * @brief Accessor for event handler
     */
    virtual HG::EventHandler *eventHandler();

    /**
     * @brief Accessor for truth handler
     */
    virtual HG::TruthHandler *truthHandler();

    /**
     * @brief Function which performs full yybb selection and saves info to info maps (nominal by default)
     * @param photons Container with all photons
     * @param jetPreSelCat Which jet preselection to use
     */
    void performSelection(const xAOD::PhotonContainer &photons,  xAOD::JetContainer &jets, const jetPreselectionEnum jetPreSelCat, const xAOD::MissingETContainer &met);

    /*!
       \fn void recordCandidateJetCollections(xAOD::JetContainer &jets)
       \brief Write out jet collections for each of the jet preselection possibilities
       \param electrons Container with all electrons (before overlap removal)
       \param muons Container with all muons (before overlap removal)
     */
    void applyLeptonPtCuts(const xAOD::ElectronContainer &electrons, const xAOD::MuonContainer &muons);

    /*!
       \fn void calcJetQuantities(xAOD::JetContainer &jets)
       \brief Calculate jet related quantities used later in performSelection
       \param jets Container before any corrections
     */
    void calcJetQuantities(const xAOD::JetContainer &jets);

    /*!
       \fn void decorateJets(xAOD::JetContainer &jets);
       \brief Decorate jets with various useful derived quantities
     */
    void decorateJets(xAOD::JetContainer &jets);

    /*!
       \fn void recordCandidateJetCollections(xAOD::JetContainer &jets)
       \brief Write out jet collections for each of the jet preselection possibilities
       \param jets Container with all jets
       \param jets_no_JVT Container with all jets before applying JVT cut
     */
    void recordCandidateJetCollections(xAOD::JetContainer &jets, const xAOD::JetContainer &jets_no_JVT);

    /*!
       \fn void recordCandidateJetCollection(xAOD::JetContainer &jets_central, const jetPreselectionEnum &jetPreSelCat);
       \brief Write out jet collection for a given jet preselection
       \param jets_central The jet container with all central jets in this event
       \param jetPreSelCat Which jet preselection to use
     */
    void recordSingleJetCollection(xAOD::JetContainer &jets_central, const jetPreselectionEnum jetPreSelCat);


    /*!
      \fn void sortJets(xAOD::JetContainer &jets_central)
      \brief Return a view of the input collection, sorted using discrete DL1r bin and then by pT in the event of a tie
      \param jets_central The jet container with all central jets in this event
    */

    std::vector<int> sortJets(xAOD::JetContainer &jets_central);

    /*!
       \fn const std::string shallowCopyLeptonCorrectedJets(const jetPreselectionEnum &jetPreSelCat, const CorrectionEnum &CorrType);
       \brief Write jets after lepton correction to the output event
       \param jetPreSelCat Which jet preselection to use
       \param CorrType The lepton correction strategy to be applied (options: noCor, BJetCal, BJetReg)
     */
    const std::string shallowCopyCorrectedJets(const jetPreselectionEnum &jetPreSelCat, const CorrectionEnum &CorrType);

    /*!
       \fn const std::string shallowCopyFittedJets(const xAOD::PhotonContainer &photon, const jetPreselectionEnum &jetPreSelCat, const CorrectionEnum &CorrType);
       \brief Write jets after Kinematic Fit
       \param photon used by the Fit
       \param jetPreSelCat Which jet preselection to use
       \param CorrType The correction tool to be applied (options: KF)
     */
    const std::string shallowCopyFittedJets(const xAOD::PhotonContainer &photon, const jetPreselectionEnum &jetPreSelCat, const CorrectionEnum &CorrType);

    /*!
       \fn xAOD::JetFourMom_t getLeptonCorrectedFourVector(xAOD::Jet *jet, const CorrectionEnum &CorrType)
       \brief Calls the selected lepton correction strategy and returns a corrected 4-vector
       \param jet The Jet object under consideration.
       \param &CorrType The muon correction strategy to be applied (options: BJetCal, BJetReg)
     */

    xAOD::JetFourMom_t getCorrectedFourVector(xAOD::Jet *jet, const CorrectionEnum &CorrType);

    /*!
       \fn void calculateMassesAndCutflow(const xAOD::PhotonContainer &selPhotons, const xAOD::JetContainer *candidate_jets, const TString &jetPreSelLabel, const CorrectionEnum &CorrType);
       \brief Set four object information to maps
       \param selPhotons Container with all photons
       \param candidate_jets Container with candidate jets
       \param jetPreSelLabel Label for the jet preselection being used
       \param CorrType The lepton correction strategy to be applied (options: allMu, allLep, noLepCor)
     */
    void calculateMassesAndCutflow(const xAOD::PhotonContainer             &selPhotons,
                                   const xAOD::JetContainer                *candidate_jets,
                                   const HHyybbTool::jetPreselectionEnum   &jetPreSelType,
                                   const HHyybbTool::CorrectionEnum  &CorrType);

    void performCutBased(const xAOD::PhotonContainer             &selPhotons,
                         const xAOD::JetContainer                *candidate_jets,
                         const HHyybbTool::jetPreselectionEnum   &jetPreSelType,
                         const HHyybbTool::CorrectionEnum  &CorrType);

    float *makeXGBoostDMatrixggF(const xAOD::PhotonContainer            *selPhotons,
                                 const xAOD::JetContainer               *candidate_jets,
                                 const xAOD::MissingETContainer         *met,
                                 const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                                 const HHyybbTool::CorrectionEnum &CorrType);

    float getXGBoostWeightggF(BoosterHandle *boost, float *vars, int len);

    void performXGBoostBDT(const xAOD::PhotonContainer            *selPhotons,
                           const xAOD::JetContainer               *candidate_jets,
                           const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                           const HHyybbTool::CorrectionEnum &CorrType,
                           const xAOD::MissingETContainer         *met,
                           const xAOD::JetContainer               *allJets);

    float *makeXGBoostDMatrixVBF(const xAOD::PhotonContainer            *selPhotons,
                                 const xAOD::JetContainer               *candidate_jets,
                                 const xAOD::JetContainer               *vbf_jets,
                                 const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                                 const HHyybbTool::CorrectionEnum &CorrType);

    float *eventShapeVariables(const xAOD::PhotonContainer    *selPhotons,
                               const xAOD::JetContainer       *candidate_jets);


    void getVBFjets(const xAOD::JetContainer               *candidateJets,
                    const xAOD::JetContainer               *allJets,
                    const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                    const HHyybbTool::CorrectionEnum &CorrType);

    std::vector<double> getXGBoostWeightVBF(BoosterHandle *boost, float *vars, int len);


    float deltaPhi(float angle1, float angle2);

    /*!
       \fn double weightJVT()
       \brief Fetch JVT (in)efficiency SF
     */
    double weightJVT();

    /*!
       \fn double weightBTagging(const xAOD::JetContainer &jets)
       \brief Fetch flavour tagging SF
       \param jets Container with all jets
     */
    double calculateBTaggingWeight(const xAOD::JetContainer &jets);

    /*!
       \fn HHyybbTool::weightNLO()
       \brief do truth hh reweighting to NLO where appropriate
     */
    double weightNLO();

    /*!
       \fn void recordPerEventQuantities()
       \brief Write maps to eventInfo
     */
    void recordPerEventQuantities();

    /*!
       \fn TString getJetPreselLabel(const jetPreselectionEnum &jetPreSelCat)
       \brief Fetch label for jet preselection type
     */
    TString getJetPreselLabel(const jetPreselectionEnum &jetPreSelCat);

    /*!
       \fn TString getCorrLabel(const CorrectionEnum &&CorrType)
       \brief Fetch label for lepton correction type
     */
    TString getCorrLabel(const CorrectionEnum &CorrType);


    /*!
       \fn TString getJetCollectionName(const jetPreselectionEnum &jetPreSelCat, const CorrectionEnum &CorrType)
       \brief Construct appropriate name for this jet collection
     */
    std::string getJetCollectionName(const jetPreselectionEnum &jetPreSelCat, const CorrectionEnum &CorrType = HHyybbTool::noCor);


    void makeMVAResonantScore(const xAOD::PhotonContainer            *selPhotons,
                              const xAOD::JetContainer               *candidate_jets,
                              const xAOD::MissingETContainer         *met,
                              const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                              const HHyybbTool::CorrectionEnum       &CorrType);

  };
}
