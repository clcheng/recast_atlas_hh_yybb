import os
import re
import glob

import json
import numpy as np
import click

from copy import deepcopy
from selections import MC_SELECTION, DATA_SELECTION

import ROOT

PROCESSES = {
    "resonant":{
        "signal": ["HH_ggF_resonant"],
        "background": ["HH_ggF_non_resonant", "HH_VBF_non_resonant",
                       "ttH", "VBF", "ZH", "ggZH", "ggH", "WmH", "WpH", "bbH", "tHjb", "tWH"]
    },
    "non-resonant":{
        "signal": ["HH_ggF", "HH_VBF"],
        "background": ["ttH", "VBF", "ZH", "ggZH", "ggH", "WmH", "WpH", "bbH", "tHjb", "tWH"]
    }
}

VARIABLES = { "resonant": 
                {"m_yy": {
                    "var": "HGamEventInfoAuxDyn.m_yy*0.001",
                    "bins": {"nbins": 22, "lbins": 105, "ubins": 160 }},
                 "weight" : {
                     "var": "HGamEventInfoAuxDyn.crossSectionBRfilterEff*HGamEventInfoAuxDyn.weight*HGamEventInfoAuxDyn.yybb_weight*FJvt",
                     "bins":{"nbins" : 200, "lbins" : -10, "ubins" : 10}}},
              "non-resonant": 
                {"m_yy": {
                    "var": "HGamEventInfoAuxDyn.m_yy*0.001",
                    "bins": {"nbins": 55, "lbins": 105, "ubins": 160 }},
                 "weight" : {
                     "var": "HGamEventInfoAuxDyn.crossSectionBRfilterEff*HGamEventInfoAuxDyn.weight*HGamEventInfoAuxDyn.yybb_weight*FJvt",
                     "bins":{"nbins" : 200, "lbins" : -10, "ubins" : 10}}}
            }

LUMINOSITY = {"mc16a": 36207.66, "mc16d": 44307.4, "mc16e": 58450.1}

HIST_NAMES = {
    "resonant":{
        "HH_ggF_resonant": "CutFlow_MGH7_X{mass}tohh_bbyy_AF2_noDalitz_weighted",
        "HH_ggF_non_resonant": "CutFlow_PowhegPy8_HHbbyy_cHHH01d0_noDalitz_weighted",
        "HH_VBF_non_resonant": "CutFlow_MGPy8_hh_bbyy_vbf_l1cvv1cv1_noDalitz_weighted",
        "ttH": "CutFlow_PowhegPy8_ttH125_fixweight_noDalitz_weighted",
        "VBF": "CutFlow_PowhegPy8EG_NNPDF30_VBFH125_noDalitz_weighted",
        "ZH": "CutFlow_PowhegPy8_ZH125J_noDalitz_weighted",
        "ggZH": "CutFlow_PowhegPy8_ggZH125_noDalitz_weighted",
        "ggH": "CutFlow_PowhegPy8_NNLOPS_ggH125_noDalitz_weighted",
        "WmH": "CutFlow_PowhegPy8_WmH125J_noDalitz_weighted",
        "WpH": "CutFlow_PowhegPy8_WpH125J_noDalitz_weighted",
        "bbH": "CutFlow_PowhegPy8_bbH125_noDalitz_weighted",
        "tHjb": "CutFlow_aMCnloPy8_tHjb125_4fl_noDalitz_weighted",
        "tWH": "CutFlow_aMCnloPy8_tWH125_noDalitz_weighted"
    },
    "non-resonant": {
        "HH_ggF": "CutFlow_PowhegPy8_HHbbyy_cHHH01d0_noDalitz_weighted",
        "HH_VBF": "CutFlow_MGPy8_hh_bbyy_vbf_l1cvv1cv1_noDalitz_weighted",
        "ttH": "CutFlow_PowhegPy8_ttH125_fixweight_noDalitz_weighted",
        "VBF": "CutFlow_PowhegPy8EG_NNPDF30_VBFH125_noDalitz_weighted",
        "ZH": "CutFlow_PowhegPy8_ZH125J_noDalitz_weighted",
        "ggZH": "CutFlow_PowhegPy8_ggZH125_noDalitz_weighted",
        "ggH": "CutFlow_PowhegPy8_NNLOPS_ggH125_noDalitz_weighted",
        "WmH": "CutFlow_PowhegPy8_WmH125J_noDalitz_weighted",
        "WpH": "CutFlow_PowhegPy8_WpH125J_noDalitz_weighted",
        "bbH": "CutFlow_PowhegPy8_bbH125_noDalitz_weighted",
        "tHjb": "CutFlow_aMCnloPy8_tHjb125_4fl_noDalitz_weighted",
        "tWH": "CutFlow_aMCnloPy8_tWH125_noDalitz_weighted"        
    }
}

SAMPLE_PATTERNS = {
    "non-resonant": {
        "HH_ggF": "PowhegPy8_HHbbyy_cHHH01d0.MxAODDetailedNoSkim.",
        "HH_VBF": "MGPy8_hh_bbyy_vbf_l1cvv1cv1.MxAODDetailedNoSkim.",
        "ttH"   : "PowhegPy8_ttH125_fixweight.MxAODDetailed.",
        "VBF"   : "PowhegPy8EG_NNPDF30_VBFH125.MxAODDetailed.",
        "ZH"    : "PowhegPy8_ZH125J.MxAODDetailed.",
        "ggZH"  : "PowhegPy8_ggZH125.MxAODDetailed.",
        "ggH"   : "PowhegPy8_NNLOPS_ggH125.MxAODDetailed.",
        "WmH"   : "PowhegPy8_WmH125J.MxAODDetailed.",
        "WpH"   : "PowhegPy8_WpH125J.MxAODDetailed.",
        "bbH"   : "PowhegPy8_bbH125.MxAODDetailed.",
        "tHjb"  : "aMCnloPy8_tHjb125_4fl_shw_fix.MxAODDetailed.",
        "tWH"   : "aMCnloPy8_tWH125.MxAODDetailed."
    },
    "resonant": {
        "HH_ggF_resonant": "MGH7_X{mass}tohh_bbyy_AF2.MxAODDetailedNoSkim.",
        "HH_ggF_non_resonant": "PowhegPy8_HHbbyy_cHHH01d0.MxAODDetailedNoSkim.",
        "HH_VBF_non_resonant": "MGPy8_hh_bbyy_vbf_l1cvv1cv1.MxAODDetailedNoSkim.",
        "ttH"   : "PowhegPy8_ttH125_fixweight.MxAODDetailed.",
        "VBF"   : "PowhegPy8EG_NNPDF30_VBFH125.MxAODDetailed.",
        "ZH"    : "PowhegPy8_ZH125J.MxAODDetailed.",
        "ggZH"  : "PowhegPy8_ggZH125.MxAODDetailed.",
        "ggH"   : "PowhegPy8_NNLOPS_ggH125.MxAODDetailed.",
        "WmH"   : "PowhegPy8_WmH125J.MxAODDetailed.",
        "WpH"   : "PowhegPy8_WpH125J.MxAODDetailed.",
        "bbH"   : "PowhegPy8_bbH125.MxAODDetailed.",
        "tHjb"  : "aMCnloPy8_tHjb125_4fl_shw_fix.MxAODDetailed.",
        "tWH"   : "aMCnloPy8_tWH125.MxAODDetailed."        
    }
}

# helper function in order to resolve path starting with root://
def list_files(dir_name, ext=".root"):
    is_dir = ROOT.TSystemFile(dir_name, "").IsDirectory()
    if not is_dir:
        raise ValueError("{} is not a valid directory".format(dir_name))
    sys_dir = ROOT.TSystemDirectory(dir_name, dir_name)
    files = [f.GetName() for f in sys_dir.GetListOfFiles()]
    files = [f for f in files if f.endswith(ext)]
    return files

def create_yield_config(mc16a, mc16d, mc16e, variables, selections, luminosity, 
                        sample_patterns, hist_names, outname, yield_outname, 
                        directory, sequencer=["YieldCalculator"]):
    """ Create configuration file(s) for baseline selection and yield calculation"""    
    mxaods = {
        "mc16a": list_files(mc16a),
        "mc16d": list_files(mc16d),
        "mc16e": list_files(mc16e)
    }
    datafiles = {}
    for process in sample_patterns:
        datafiles[process] = {}
        for mc_campaign in mxaods:
            selected_mxaods = [f for f in mxaods[mc_campaign] if sample_patterns[process] in f]
            if not selected_mxaods:
                raise ValueError("No MxAODs found for the process {}".format(process))
            if len(selected_mxaods) == 1: 
                datafiles[process][mc_campaign] = selected_mxaods[0]
            else:
                ptags = []
                for fname in selected_mxaods:
                    ptag_pattern = re.search('_p([0-9]+?)_', fname)
                    if not ptag_pattern:
                        raise ValueError("Cannot extract ptag from MxAOD: {}".format(fname))
                    ptag = int(ptag_pattern.group(1))
                    ptags.append(ptag)
                latest_ptag = ptags[-1]
                if np.count_nonzero(np.array(ptags) == latest_ptag) >1 :
                    raise ValueError("Multiple MxAODs for the process {} with same ptag are found".format(process))
                latest_ptag_index = np.argsort(ptags)[-1]
                datafiles[process][mc_campaign] = selected_mxaods[latest_ptag_index]
    config = {}
    config["sequencer"] = sequencer
    config["name"] = {"yields" : yield_outname}
    config["outdir"] = directory
    config["directories"] = {
        "mc16a": mc16a,
        "mc16d": mc16d,
        "mc16e": mc16e
    }
    config["dumper"] = {"mc16a": True, "mc16d": True, "mc16e": True}
    config["variables"] = variables
    config["selections"] = selections
    config["lumi"] = luminosity
    config["samples"] = {}
    for process in sample_patterns:
        config["samples"][process] = {}
        config["samples"][process]["datafiles"] = datafiles[process]
        config["samples"][process]["histoName"] = hist_names[process]
    with open(outname, 'w') as outfile:
        json.dump(config, outfile, indent=2)
        print('INFO: Created configuration file "{}"'.format(outname))
        

@click.command(name='create_mc_config')   
@click.option('--mc16a', required=True, help='path containing mc16a MxAODs')
@click.option('--mc16d', required=True, help='path containing mc16d MxAODs')
@click.option('--mc16e', required=True, help='path containing mc16e MxAODs')
@click.option('--resonant/--non-resonant', default=False, help='resonant or non-resonant analysis')
@click.option('-o', '--outname', default="Non_Resonant_MC", help='output configuration file name prefix')
@click.option('-d', '--directory', default="data", help='output directory for final output')
def create_mc_config(mc16a, mc16d, mc16e, resonant, outname, directory):
    """ Create configuration file for baseline selection and yield calculation"""
    if resonant:
        mass_points = ["251", "260", "270", "280", "290", "300", "3125", "325", "3375", "350",
                       "375", "400", "425", "450", "475", "500", "550", "600", "700", "800", "900", "1000"]
        variables = VARIABLES["resonant"]
        selections = MC_SELECTION["resonant"]
        # create signal mc configs
        signal_processes = PROCESSES["resonant"]["signal"]
        for mass in mass_points:
            selection_signal = {}
            selection_signal["weight"] = selections["weight"]
            selection_signal[mass] = selections[mass]
            sample_pattern_signal = {k:v.format(mass=mass) for k,v in SAMPLE_PATTERNS["resonant"].items() if k in signal_processes}
            hist_name_signal = {k:v.format(mass=mass) for k,v in HIST_NAMES["resonant"].items() if k in signal_processes}
            outname_signal = "{}_signal_{}.js".format(outname, mass)
            yield_outname = "yields_signal_{}".format(mass)
            create_yield_config(mc16a, mc16d, mc16e, variables, selection_signal, LUMINOSITY,
                                sample_pattern_signal, hist_name_signal, outname_signal, yield_outname, directory,
                                sequencer=["YieldCalculator", "VariablePlotter"])
        # create background mc configs
        bkg_processes = PROCESSES["resonant"]["background"]
        sample_pattern_bkg = {k:v.format(mass=mass) for k,v in SAMPLE_PATTERNS["resonant"].items() if k in bkg_processes}
        hist_name_bkg = {k:v.format(mass=mass) for k,v in HIST_NAMES["resonant"].items() if k in bkg_processes}
        outname_bkg = "{}_background.js".format(outname)
        yield_outname = "yields_background"
        create_yield_config(mc16a, mc16d, mc16e, variables, selections, LUMINOSITY,
                            sample_pattern_bkg, hist_name_bkg, outname_bkg, yield_outname, directory)
    else:
        variables = VARIABLES["non-resonant"]
        selections = MC_SELECTION["non-resonant"]
        # create signal mc configs
        signal_processes = PROCESSES["non-resonant"]["signal"]
        sample_pattern_signal = {k:v for k,v in SAMPLE_PATTERNS["non-resonant"].items() if k in signal_processes}
        hist_name_signal = {k:v for k,v in HIST_NAMES["non-resonant"].items() if k in signal_processes} 
        outname_signal = "{}_signal.js".format(outname)
        yield_outname = "yields_signal"
        create_yield_config(mc16a, mc16d, mc16e, variables, selections, LUMINOSITY,
                            sample_pattern_signal, hist_name_signal, outname_signal, yield_outname, directory,
                            sequencer=["YieldCalculator", "VariablePlotter"])
        # create background mc configs
        bkg_processes = PROCESSES["non-resonant"]["background"]
        sample_pattern_bkg = {k:v for k,v in SAMPLE_PATTERNS["non-resonant"].items() if k in bkg_processes}
        hist_name_bkg = {k:v for k,v in HIST_NAMES["non-resonant"].items() if k in bkg_processes} 
        outname_bkg = "{}_background.js".format(outname)
        yield_outname = "yields_background"
        create_yield_config(mc16a, mc16d, mc16e, variables, selections, LUMINOSITY,
                            sample_pattern_bkg, hist_name_bkg, outname_bkg, yield_outname, directory)        
    
@click.command(name='create_data_config')    
@click.option('-i', '--input_path', required=True, help='path containing data directories')
@click.option('--resonant/--non-resonant', default=False, help='resonant or non-resonant analysis')
@click.option('-o', '--outname', default="Non_Resonant_Data.js", help='output file name')
@click.option('-d', '--directory', default="data", help='output directory for final output')
def create_data_config(input_path, resonant, outname, directory):
    """ Create configuration file for data selection and dumping data mass points for resonant analysis"""    
    resonant_type = "resonant" if resonant else "non-resonant"
    data_campaigns = glob.glob(os.path.join(input_path, 'data*'))
    if not data_campaigns:
        raise ValueError("No data campaign of the format \"data*\" is found")
    config = {}
    config["sequencer"] = ["DataAnalysis"]
    config["outdir"] = directory
    config["directories"] = {}
    config["dumper"] = {}
    config["variables"] = VARIABLES[resonant_type]
    config["variables"].pop("weight", None)
    config["selections"] = DATA_SELECTION[resonant_type]    
    config["lumi"] = {}
    config["samples"] = {"data":{"datafiles":{}, "histoName":"data_hist"}}
    for i, dc in enumerate(data_campaigns):
        dc_i = "data{}".format(i+1)
        config["directories"][dc_i] = dc
        config["dumper"][dc_i] = True
        config["lumi"][dc_i] = 1
        config["samples"]["data"]["datafiles"][dc_i] = ""
    with open(outname, 'w') as outfile:
        json.dump(config, outfile, indent=2)
        print('INFO: Created configuration file "{}"'.format(outname))