// Local include(s):
#include "HGamAnalysisFramework/HGamVariables.h"
#include "HGamAnalysisFramework/VarHandler.h"
#include "HGamTools/HGamExample.h"

// this is needed to distribute the algorithm to the workers
ClassImp(HGamExample)


HGamExample::HGamExample(const char *name)
  : HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}


EL::StatusCode HGamExample::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  histoStore()->createTH1F("phi_y1", 20, -TMath::Pi(), TMath::Pi());
  histoStore()->createTH1F("pt_y1", 50, 0, 2000);
  histoStore()->createTH1F("m_yy", 20, 120, 130);
  histoStore()->createTH1F("m_yyj", 40, 0, 200);
  histoStore()->createTH1F("mu", 40, 5, 45);
  histoStore()->createTH1F("truth_pt_y1", 50, 0, 2000);
  histoStore()->createTH1F("truth_m_yy", 20, 120, 130);

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode HGamExample::execute()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  HgammaAnalysis::execute();

  // Start loop over systematics
  for (auto sys : getSystematics()) {

    // Some MxAODs don't have systematics, skip this!
    if (!isSystematicAvailable(sys)) {
      continue;
    }

    CP_CHECK("HGamExample", applySystematicVariation(sys));

    // Check whether basic event selections are passed
    if (!eventHandler()->pass()) { continue; } // GRL, trig, etc.

    // Check whether event passes HGam kinematic selections
    if (HG::isMAOD()) { // MxAODs store flags

      if (!pass()) { continue; } // kinematic selection

      // This will update pileup weights, even if no objects are passed
      setSelectedObjects();

    } else { // xAODs and DxAODs must get containers to check

      // Get containers
      // Note: nominal containers can also be accessed in MxAODs, but
      // systematically shifted continers are not available

      xAOD::PhotonContainer photons       = photonHandler()->getCorrectedContainer();
      // Only apply pre-selection
      xAOD::PhotonContainer preselphotons = photonHandler()->applyPreSelection(photons);
      // Apply all selections (tight PID, isolation, etc.)
      xAOD::PhotonContainer selphotons    = photonHandler()->applySelection(photons);

      // Get all other selected objects
      xAOD::JetContainer jets    = jetHandler()->getCorrectedContainer();
      xAOD::JetContainer seljets = jetHandler()->applySelection(jets);

      xAOD::JetContainer jets_hv    = jetHandlerPFlow()->getCorrectedContainer();

      xAOD::MuonContainer mus    = muonHandler()->getCorrectedContainer();
      xAOD::MuonContainer selmus = muonHandler()->applySelection(mus);

      xAOD::TauJetContainer taus    = tauHandler()->getCorrectedContainer();
      xAOD::TauJetContainer seltaus = tauHandler()->applySelection(taus);

      xAOD::ElectronContainer els    = electronHandler()->getCorrectedContainer();
      xAOD::ElectronContainer selels = electronHandler()->applySelection(els);

      // Apply overlap removal on objects
      overlapHandler()->removeOverlap(selphotons, seljets, selels, selmus, seltaus);

      xAOD::MissingETContainer met = etmissHandler()->getCorrectedContainer(&selphotons, &jets, &jets_hv, &selels, &selmus, &seltaus);
      xAOD::MissingETContainer selmet = etmissHandler()->applySelection(met);

      if (!pass(&preselphotons, &selels, &selmus, &seljets)) { continue; } // kinematic selection

      // Set the selected objects, to be used by:
      // * var::pT_yy() and other variables
      setSelectedObjects(&selphotons, &selels, &selmus, &seltaus, &seljets, &selmet);
    }
  }

  // Return to nominal for truth event weight()
  CP_CHECK("HGamExample", applySystematicVariation(CP::SystematicSet()));

  // Fill histograms using nominal information
  ATH_MSG_DEBUG("Using nominal to fill histograms");

  // When running over an MxAOD this returns the corrected AND selected photons/jets
  xAOD::PhotonContainer photons = photonHandler()->getCorrectedContainer();
  xAOD::JetContainer    jets    = jetHandler()->getCorrectedContainer();

  // In case you're running over a DxAOD, this will apply the selection
  // (does nothing on MxAODs, but safe to call)
  photons = photonHandler()->applySelection(photons);
  jets = jetHandler()->applySelection(jets);

  // var's in MxAODs are used by default, but if a specific var (like m_yyj)
  // isn't stored, you can calculate it using the nominal containers
  setSelectedObjects(&photons, nullptr, nullptr, nullptr, &jets);

  // Check how pileup weights look
  histoStore()->fillTH1F("mu", eventHandler()->mu(), weight());

  // Use objects retrieved from the event to fill histograms
  histoStore()->fillTH1F("phi_y1", (photons.size() > 0 ? photons.at(0)->phi() : -99), weight());

  // ... or simply fill histograms using event variables
  histoStore()->fillTH1F("pt_y1", var::pT_y1() / HG::GeV, weight());
  histoStore()->fillTH1F("m_yy", var::m_yy() / HG::GeV, HG::isMC() ? eventHandler()->vertexWeight() : 1.0);
  histoStore()->fillTH1F("m_yyj", var::m_yyj() / HG::GeV, weight());

  // If it's MC simulation, we can look at truth containers!
  if (HG::isMC()) {

    // Get selected truth particles
    xAOD::TruthParticleContainer truthphotons    = truthHandler()->getPhotons();
    xAOD::TruthParticleContainer seltruthphotons = truthHandler()->applyPhotonSelection(truthphotons);

    xAOD::TruthParticleContainer truthelectrons    = truthHandler()->getElectrons();
    xAOD::TruthParticleContainer seltruthelectrons = truthHandler()->applyElectronSelection(truthelectrons);

    xAOD::TruthParticleContainer truthmuons    = truthHandler()->getMuons();
    xAOD::TruthParticleContainer seltruthmuons = truthHandler()->applyMuonSelection(truthmuons);

    xAOD::TruthParticleContainer truthtaus    = truthHandler()->getTaus();
    xAOD::TruthParticleContainer seltruthtaus = truthHandler()->applyTauSelection(truthtaus);

    xAOD::JetContainer           truthjets    = truthHandler()->getJets();
    xAOD::JetContainer           seltruthjets = truthHandler()->applyJetSelection(truthjets);

    // Apply overlap removal
    truthHandler()->removeOverlap(seltruthphotons, seltruthjets, seltruthelectrons, seltruthmuons, seltruthtaus);

    xAOD::MissingETContainer truthmet    = truthHandler()->getMissingET();
    xAOD::MissingETContainer seltruthmet = truthHandler()->applyMissingETSelection(truthmet);

    xAOD::TruthParticleContainer higgs = truthHandler()->getHiggsBosons();

    // Check for 2 truth photons (will update pass() functions for use with truth in the future)
    if (seltruthphotons.size() > 1) {
      // Set the selected truth objects, to be used by:
      // * var::pT_yy() and other variables
      setSelectedTruthObjects(&seltruthphotons, &seltruthelectrons, &seltruthmuons, &seltruthtaus, &seltruthjets);
      HG::VarHandler::getInstance()->setHiggsBosons(&higgs);

      // Do the analysis, fill the histograms
      histoStore()->fillTH1F("truth_pt_y1", seltruthphotons[0]->pt() / HG::GeV, weight());
      histoStore()->fillTH1F("truth_m_yy", var::m_yy.truth() / HG::GeV, weight());
    }

  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode HGamExample::finalize()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  HgammaAnalysis::finalize();

  return EL::StatusCode::SUCCESS;
}
