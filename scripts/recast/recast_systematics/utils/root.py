import os
import ROOT

import recast_systematics

def is_corrupt(fname):
    if not fname.endswith('.root'):
        return False
    f = ROOT.TFile(fname)
    if f.IsZombie():
        return True
    if f.TestBit(ROOT.TFile.kRecovered):
        return True
    if f.GetNkeys() == 0:
        return True
    return False

def compile_macro(name, depth=2):
    macros_dir = recast_systematics.macro_path
    macros_path = os.path.join(macros_dir, name)
    print('INFO: Compiling macro "{}"...'.format(name))
    if not os.path.exists(macros_path):
        print('ERROR: Cannot locate macro files from {}. ROOT macros will not be compiled.'.format(macros_dir))
        return -1
    return ROOT.gROOT.LoadMacro('{}{}'.format(macros_path, '+'*depth))

def load_macro(name, depth=2):  
    macros_dir = recast_systematics.macro_path
    macros_path = os.path.join(macros_dir, name)
    compiled_macros_path = os.path.join(macros_dir, name.replace('.', '_'))
    if not os.path.exists(compiled_macros_path + '.so'):
        return compile_macro(name, depth=depth)
    result = ROOT.gSystem.Load(compiled_macros_path)
    if (result != 0) and (result != 1):
        raise RuntimeError("Shared library for the macro {} is incompatible with the current pyROOT version.")
    return result


def close_all_root_files():
    opened_files = ROOT.gROOT.GetListOfFiles()
    for f in opened_files:
        f.Close()