#include <xAODRootAccess/Init.h>

// Local include(s):
#include "HGamAnalysisFramework/RunUtils.h"
#include "HGamTools/HGamEventDisplay.h"

#include <xAODRootAccess/Init.h>

int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  HGamEventDisplay *alg = new HGamEventDisplay("HGamEventDisplay");

  // Use helper to start the job
  HG::runJob(alg, argc, argv);

  return 0;
}
