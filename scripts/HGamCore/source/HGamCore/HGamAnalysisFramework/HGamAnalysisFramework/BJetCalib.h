#pragma once

#include "xAODJet/JetContainer.h"
#include "xAODJet/Jet.h"

#include "TString.h"

namespace HG {

  class BJetCalib {

  private:
    static BJetCalib *m_ptr;
    ~BJetCalib() {};
    BJetCalib() {};

  public:
    static BJetCalib *getInstance();

    void applyBJetEnergyCorrection(xAOD::Jet *jet, TString scaleName = "OneMu", bool applypTreco = true, bool debug = false);
    void applyPtRecoEnergyCorrection(xAOD::Jet *jet, bool debug = false);
    void undoBJetEnergyCorrection(xAOD::Jet *jet);
    void undoBJetEnergyCorrection(xAOD::JetContainer &jetC);

  };
}
