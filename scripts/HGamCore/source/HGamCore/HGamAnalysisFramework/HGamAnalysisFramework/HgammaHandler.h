#pragma once

// EDM include(s):
#include "AsgTools/AnaToolHandle.h"
#include "AsgTools/AsgMessaging.h"
#include "EventLoop/StatusCode.h"
#include "RecoToolInterfaces/ITrackIsolationTool.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "TrackVertexAssociationTool/ITrackVertexAssociationTool.h"
#include "PATInterfaces/SystematicCode.h"
#include "PATInterfaces/SystematicSet.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODTracking/Vertex.h"

// ROOT include(s):
#include "TString.h"

// Local include(s):
#include "HGamAnalysisFramework/Config.h"

namespace HG {

  template <class partType, class partContainer, class auxContainer>
  class HgammaHandler : public asg::AsgMessaging {

  protected:
    TString       m_name;
    xAOD::TEvent *m_event;
    xAOD::TStore *m_store;

    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_trackSelTool; //!
    asg::AnaToolHandle<xAOD::ITrackIsolationTool> m_trackIsoTool; //!
    std::vector<xAOD::Iso::IsolationType>  m_isoT;
    xAOD::TrackCorrection                  m_corrList;

    asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_TTVAtrackSelTool; //!
    asg::AnaToolHandle<CP::ITrackVertexAssociationTool> m_TTVATool; //!
    asg::AnaToolHandle<xAOD::ITrackIsolationTool> m_trackIsoToolTTVA; //!

    TString       m_sysName;
    TString       m_containerName;
    TString       m_pvxName;
    TString       m_truthName;
    TString       m_MxAODname;
    bool          m_isVtxCorr;
    bool          m_sortCandFirst;
    bool          m_useDerivationFrameworkFlags;

    // Old method
    partContainer getShallowContainer(std::string name = "");
    // New methods
    partContainer getShallowContainer(bool &calibStatus, bool makeShallowCopy = true);
    partContainer getStoreContainer(std::string name);
    partContainer getEventContainer(std::string name, std::string post = "", bool makeShallowCopy = true);

    virtual bool correctContainerPV(partContainer &cont);
    virtual void correctPrimaryVertex(const xAOD::Vertex *vertex, partType &part);



  public:
    HgammaHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~HgammaHandler();

    virtual EL::StatusCode initialize(Config &config);

    partContainer              getContainer(std::string name);
    virtual partContainer      getCorrectedContainer() = 0;
    virtual partContainer      applySelection(partContainer &container) = 0;
    virtual CP::SystematicCode applySystematicVariation(const CP::SystematicSet &sys) = 0;
    virtual EL::StatusCode     writeContainer(partContainer &container, TString name = "");
    virtual EL::StatusCode     writeTruthContainer(partContainer &container, TString name = "");

    static bool comparePt(const partType *a, const partType *b);

    virtual void setVertexCorrected(bool flag = true) { m_isVtxCorr = flag; }
    virtual bool markAsCandidate(const xAOD::IParticle *part);

    virtual void decorateRawCalib(partContainer &cont);
  };

}

#include "HGamAnalysisFramework/HgammaHandler.hpp"
