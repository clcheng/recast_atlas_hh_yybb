Introduction on MC related weights
==================================

This section mainly documents the explanations of various weight related variables from MxAODs (produced by HGamCore framework), 
as well as the procedure to weight the MC event.

All the weights are available under the container ``HGamEventInfo``, so they can be read in ROOT as, for example, ``HGamEventInfoAuxDyn.weightTrigSF``

Explanations for weight related branches of MxAOD
-------------------------------------------------
 
.. csv-table::
   :header: " ", "MC generator", "z-vertex", "Pileup", "TriggerSF", "PhotonSF", "LeptonSF", "JetSF"

   "**vertexWeight**", " ", "x", " ", " ", " ", " ", " "
   "**pileupWeight**", " ", " ", "x", " ", " ", " ", " "
   "**weightTrigSF**", " ", " ", " ", "x", " ", " ", " "
   "**weightSF**", " ", " ", " ", "x", "x", " ", " "
   "**weightInitial**", "x", "x", "x", " ", " ", " ", " "
   "**weight**", "x", "x", "x", "x", "x", " ", " "
   "**weightCatCoup_SFXGBoost_ttH**", " ", " ", " ", " ", " ", "x", "x"
   "**weightCatCoup_XGBoost_ttH**", "x", "x", "x", "x", "x", "x", "x"

For this table, the meanings of the header line are described as above:

- MC generator: the generator weight of MC samples. For Higgs samples the average should be close to the cross section.
- z-vertex: weight for z-vertex reweighting. This weight is applied on MC to match the distribution of the z-vertex to the one of data. It is computed with the `VertexPositionReweighting <https://gitlab.cern.ch/cjmeyer/VertexPositionReweighting>`_. The average of this weight is exactly 1.
- Pileup: weight for pile-up reweighting. The average of this weight is exactly 1.
- TriggerSF: scale factor for diphoton trigger "HLT_g35_medium_g25_medium_L12EM20VH"
- PhotonSF: scale factors for photon identification and isolation
- LeptonSF: scale factors for selected muons and electrons
- JetSF: jvt and b-tagging scale factors for jets 

And for each row, it lists the corresponding components (where "x" is marked for the elements of the header line) for each weight branch.

Please note that the other **weightCatCoup_*** variables are similar as the ones described in the table above, but only 
for 2017 Moriond categorization, and not likely to be used anymore.

Weights as ``weightCatCoup_`` makes sense only for the events passing all the cuts, for the others it is set to 1.


How to weight the MC event
--------------------------

Then we want to discuss about how to properly weight a MC event.
In the H->yy simulated sample yy* events (Dalitz events) are included. Their contribution must be taken into account in the yield of the peak.
To keep it simple, we can consider only one single production process *p*, for which the expected yield in the peak can be written as:


.. image:: equation1.png
   :scale: 30


Here the weight efficiency term can be estimated with MC as:

.. image:: equation2.png
   :scale: 40


While the branching ratio term can be derived from MC as well:

.. image:: equation3.png
   :scale: 36


After the substitutions, we can have

.. image:: equation4.png
   :scale: 36


Therefore for the MxAODs files, the weight for a single event based on current categorization method can be depicted in the equation below:

.. image:: Normalize.png


And here **isPassed** means passing all the selection criteria up to diphoton mass requirement (105 GeV < myy < 160 GeV).
**crossSectionBRfilterEff** represents the product of cross section, branching ratio and filter efficiency
for the corresponding MC sample. **CutFlow_<Sample>_noDalitz_weighted** is the cutflow histogram obtained from the MxAOD sample.

For the full coupling analysis, we usually measure a fiducial cross section with an eta cut (<2.5) on photons instead of the total
cross section, and in this case we need to introduce the acceptance effect and write the equation as:

.. image:: equation6.png
   :scale: 36


For more details, please refer to 
`this presentation <https://indico.cern.ch/event/785190/contributions/3304115/attachments/1788804/2913465/20190131_WeightTech_HGamCoupling.pdf>`_.

