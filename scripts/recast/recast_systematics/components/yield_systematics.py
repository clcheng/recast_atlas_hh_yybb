import os
import ROOT
import numpy as np
import pandas as pd

from .base_systematics import BaseSystematics
from recast_systematics.ops.math import *

class YieldSystematics(BaseSystematics):

    HIST_NAMES = {
        'nominal'                  : 'hist_nb_sel_nominal_{category}',
        'nominal_unweighted'       : 'hist_nb_sel_nominal_{category}_unweighted',
        'sys'                      : 'hist_nb_sel_sys_{syst_name}{variation}_{category}',
        'sys_unweighted'           : 'hist_nb_sel_sys_{syst_name}{variation}_{category}_unweighted',
        'com_nominal'              : 'hist_nb_sel_common_part_nominal_{syst_name}{variation}_{category}',
        'uncom_nominal'            : 'hist_nb_sel_uncommon_part_nominal_{syst_name}{variation}_{category}',
        'uncom_nominal_unweighted' : 'hist_nb_sel_uncommon_part_nominal_{syst_name}{variation}_{category}_unweighted',
        'uncom_sys'                : 'hist_nb_sel_uncommon_part_sys_{syst_name}{variation}_{category}',
        'uncom_sys_unweighted'     : 'hist_nb_sel_uncommon_part_sys_{syst_name}{variation}_{category}_unweighted'
    }
    
    PRIMARY_KEYS = {'yield': [('up', 'rel_effect'), ('down', 'rel_effect')]}
    AUXILIARY_KEYS = {
        'yield': [
            [('up', name, nature)  for name in list(HIST_NAMES) for nature in ['val', 'err']],
            [('down', name, nature)  for name in list(HIST_NAMES) for nature in ['val', 'err']],
        ]
    }
    
    def __init__(self, prune_threshold=0.1, prune_significative=True, *args, **kwargs):
        super().__init__(prune_threshold=prune_threshold, 
                         prune_significative=prune_significative,
                         *args, **kwargs)
        
    @staticmethod
    def get_required_hists(syst_object):
        if syst_object == "THEORY":
            return ["nominal", "sys"]
        else:
            return ["nominal", "sys", "nominal_unweighted", "sys_unweighted", "com_nominal",
                    "uncom_nominal", "uncom_nominal_unweighted", "uncom_sys", "uncom_sys_unweighted"]

    def process_data(self):
        if self.dataframe is None:
            return
        df = self.dataframe

        for v in ['up', 'down']:
            # relative effect on yield
            nom_val = df[(v, 'nominal', 'val')].values
            sys_val = df[(v, 'sys', 'val')].values
            nom_unweighted_val = df[(v, 'nominal_unweighted', 'val')].values
            sys_unweighted_val = df[(v, 'sys_unweighted', 'val')].values
            uncom_nom_unweighted = df[(v, 'uncom_nominal_unweighted', 'val')].values
            uncom_sys_unweighted = df[(v, 'uncom_sys_unweighted', 'val')].values
            df[(v, 'rel_effect', 'val')] = get_relative_effect(nom_val, sys_val)
            
            # correlation
            df[(v, 'correlation')] = get_correlation(nom_val, sys_val, 
                                                     nom_unweighted_val, sys_unweighted_val,
                                                     uncom_nom_unweighted, uncom_sys_unweighted)
            
            # error of relative effect on yield for fully correlated case
            full_corr_idx = df[(v, 'correlation')] =='full'
            df_full_corr = df[full_corr_idx]
            fc_nom_val = df_full_corr[(v, 'nominal', 'val')].values
            fc_nom_err = df_full_corr[(v, 'nominal', 'err')].values
            fc_sys_val = df_full_corr[(v, 'sys', 'val')].values
            fc_sys_err = df_full_corr[(v, 'sys', 'err')].values
            df.loc[full_corr_idx, [(v, 'rel_effect', 'err')]] = get_err_relative_effect_fully_correlated(
                    fc_nom_val, fc_nom_err, fc_sys_val, fc_sys_err)
            
            # error of relative effect on yield for partially correlated case
            part_corr_idx = df[(v, 'correlation')] == 'partial'
            df_part_corr = df[part_corr_idx]
            pc_nom_val = df_part_corr[(v, 'nominal', 'val')].values
            pc_com_nom_val = df_part_corr[(v, 'com_nominal', 'val')].values
            pc_uncom_nom_val = df_part_corr[(v, 'uncom_nominal', 'val')].values
            pc_com_nom_err = df_part_corr[(v, 'com_nominal', 'err')].values
            pc_uncom_nom_err = df_part_corr[(v, 'uncom_nominal', 'err')].values
            pc_uncom_sys_val = df_part_corr[(v, 'uncom_sys', 'val')].values
            pc_uncom_sys_err = df_part_corr[(v, 'uncom_sys', 'err')].values
            df.loc[part_corr_idx, [(v, 'rel_effect', 'err')]] = get_err_relative_effect_partially_correlated(
                    pc_nom_val, pc_com_nom_val, pc_uncom_nom_val , pc_com_nom_err,
                    pc_uncom_nom_err, pc_uncom_sys_val, pc_uncom_sys_err)
        
        symmetric = df['symmetric'] == True
        df.loc[symmetric, [('up', 'rel_effect', 'val')]]   = np.abs(df[symmetric][('up', 'rel_effect', 'val')])
        df.loc[symmetric, [('down', 'rel_effect', 'val')]] = -np.abs(df[symmetric][('down', 'rel_effect', 'val')])
        
    def post_process(self, merge_condition=None):
        self.process_data()
        super().post_process(merge_condition=merge_condition)
    
    def _get_systematics_data(self, f, category, syst_name):
        data = {}
        
        systematics_object = self.get_syst_object(syst_name)
        systematics_theme  = self.get_syst_theme(syst_name)
        is_symmetric = self.is_symmetric(syst_name)
        
        if is_symmetric:
            variations = [""]
        else:
            variations = ["__1up", "__1down"]
            
        keys = self.get_required_hists(systematics_object)
            
        for variation in variations:
            for key in keys:
                h_name = self.HIST_NAMES[key].format(syst_name=syst_name, variation=variation, category=category)
                h = f.Get(h_name)
                if not h:
                    raise RuntimeError("In <TFile::Get>, object `{}` not found (file=`{}`)".format(h_name, f.GetName()))
                value = h.GetBinContent(1)
                error = h.GetBinError(1)
                if variation == "":
                    data[('up'  , key, 'val')] = value
                    data[('down', key, 'val')] = value
                    data[('up'  , key, 'err')] = error
                    data[('down', key, 'err')] = error
                elif variation == "__1up":
                    data[('up'  , key, 'val')] = value
                    data[('up'  , key, 'err')] = error
                elif variation == "__1down":
                    data[('down', key, 'val')] = value
                    data[('down', key, 'err')] = error
                    
        data['object']    = systematics_object
        data['theme']     = systematics_theme
        data['symmetric'] = is_symmetric
        
        return data
    
    def get_systematics_from_root_file(self, fname, categories, systematics_list, process, append=True):
        results = []
        if not os.path.exists(fname):
            raise FileNotFoundError("In <TFile::TFile>, file `{}` does not exist".format(fname))
        f = ROOT.TFile(fname)
        if f.IsZombie():
            raise RuntimeError("In <TFile::TFile>, corrupted file `{}`".format(fname))
        for category in categories:
            print("INFO: Evaluating yield systematics for process=`{}`, category=`{}` from "
                  "input file `{}`".format(process, category, fname))
            for systematics_name in systematics_list:
                try:
                    data = self._get_systematics_data(f, category, systematics_name)
                    data['status'] = 1
                except:
                    data = {'status': 0}
                data['process'] = process
                data['category'] = category
                data['systematics'] = systematics_name
                results.append(data)              
        if append:
            self.append(results)
        return results