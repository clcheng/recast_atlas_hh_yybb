#ifndef ROOMINIMIZEREXT_H
#define ROOMINIMIZEREXT_H

#include "RooMinimizer.h"

class RooMinimizerExt : public RooMinimizer{
 public:
  RooMinimizerExt(RooAbsReal& function) : RooMinimizer(function){}
  void applyCovMatrix(TMatrixDSym& V){applyCovarianceMatrix(V);}
  int getNPar(){return fitterFcn()->NDim();}
};

#endif
