// STL include(s):
#include <iostream>

// ROOT include(s):
#include "TFile.h"
#include "TH1.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TSystem.h"

// Local include(s):
#include "HGamAnalysisFramework/HGamCommon.h"


// See header file for documentation

namespace HG {

  void fatal(TString msg)
  {
    printf("\nFATAL\n  %s\n\n", msg.Data());
    abort();
  }

  StrV vectorize(TString str, TString sep)
  {
    StrV result;
    TObjArray *strings = str.Tokenize(sep.Data());

    if (strings->GetEntries() == 0) { delete strings; return result; }

    TIter istr(strings);

    while (TObjString *os = (TObjString *)istr()) {
      // the number sign and everything after is treated as a comment
      if (os->GetString()[0] == '#') { break; }

      result.push_back(os->GetString());
    }

    delete strings;
    return result;
  }

  // convert a text line containing a list of numbers to a vector<double>
  NumV vectorizeNum(TString str, TString sep)
  {
    NumV result;
    StrV vecS = vectorize(str, sep);

    for (uint i = 0; i < vecS.size(); ++i)
    { result.push_back(atof(vecS[i])); }

    return result;
  }

  // checks if a given file or directory exist
  bool fileExist(TString fn)
  {
    return !(gSystem->AccessPathName(fn.Data()));
  }


  TString fourVecAsText(const TLorentzVector &p4)
  {
    return TString::Format("(pT,y,phi,m) = (%6.1f GeV,%6.3f,%6.3f,%5.1f GeV )",
                           p4.Pt() * invGeV, p4.Rapidity(), p4.Phi(), p4.M() * invGeV);
  }

  TString fourVecAsText(const xAOD::IParticle *p)
  {
    return fourVecAsText(p->p4());
  }

  TH1 *getHistogramFromFile(TString fname, TString hname)
  {
    fname = PathResolverFindCalibFile(fname.Data());
    TFile *file = TFile::Open(fname.Data(), "READ");

    if (file == nullptr) {
      std::cout << "HgammaUtils::getHistogramFromFile() : Couldn't open file "
                << fname.Data() << ", returning nullptr." << std::endl;
      return nullptr;
    }

    TH1 *temp = dynamic_cast<TH1 *>(file->Get(hname.Data()));

    if (temp == nullptr) {
      std::cout << "HgammaUtils::getHistogramFromFile() : Couldn't find histogram "
                << hname.Data() << " in file "
                << fname.Data() << ", returning nullptr." << std::endl;
      return nullptr;
    }

    bool status = TH1::AddDirectoryStatus();
    TH1::AddDirectory(false);
    hname = "cloned_" + hname;
    TH1 *hist = dynamic_cast<TH1 *>(temp->Clone(hname.Data()));
    SafeDelete(file);
    TH1::AddDirectory(status);

    return hist;
  }

  // The code for global acess to variables.
  // Users should only need to use isAOD, isMAOD, isDAOD, ...
  GlobalVariables *GlobalVariables::m_ptr = nullptr;

  void setAndLock_InputType(bool isAOD, bool isMAOD)
  {
    GlobalVariables::getInstance()->set_InputType(isAOD, isMAOD);
  }

  void setAndLock_isAFII(bool b)
  {
    GlobalVariables::getInstance()->set_isAFII(b);
  }

  void setAndLock_mcType(TString b)
  {
    GlobalVariables::getInstance()->set_mcType(b);
  }

  TString get_mcTypeUsingAmiTag(TString b)
  {
    return GlobalVariables::getInstance()->get_mcTypeUsingAmiTag(b);
  }

  void setAndLock_isMC(bool b)
  {
    GlobalVariables::getInstance()->set_isMC(b);
  }

  bool isMAOD()
  {
    return GlobalVariables::getInstance()->isMAOD();
  }

  bool isAOD()
  {
    return GlobalVariables::getInstance()->isAOD();
  }

  bool isDAOD()
  {
    return GlobalVariables::getInstance()->isDAOD();
  }

  bool isAFII()
  {
    return GlobalVariables::getInstance()->isAFII();
  }

  TString mcType()
  {
    return GlobalVariables::getInstance()->mcType();
  }

  bool isMC()
  {
    return GlobalVariables::getInstance()->isMC();
  }

  bool isData()
  {
    return GlobalVariables::getInstance()->isData();
  }


  /// GlobalVariables class implementation
  GlobalVariables *GlobalVariables::getInstance()
  {
    if (m_ptr == nullptr) { m_ptr = new GlobalVariables(); }

    return m_ptr;
  }

  void GlobalVariables::set_InputType(bool isAOD, bool isMAOD)
  {
    if (isAOD) { the_inputtype = InputTypeAOD; }
    else if (isMAOD) { the_inputtype = InputTypeMAOD; }
    else { the_inputtype = InputTypeDAOD; }
  }

  void GlobalVariables::set_isAFII(bool b)    { m_isAFII = (b ? FlagTrue : FlagFalse); }
  void GlobalVariables::set_mcType(TString b) { m_mcType = b; }
  void GlobalVariables::set_isMC(bool b)    { m_isMC   = (b ? FlagTrue : FlagFalse); }

  TString GlobalVariables::get_mcTypeUsingAmiTag(TString amiTag)
  {
    if (TPRegexp("r7326").MatchB(amiTag)) { return "MC15b"; }
    else if (TPRegexp("r7267").MatchB(amiTag))  { return "MC15b"; }
    else if (TPRegexp("r7725").MatchB(amiTag))  { return "MC15c"; }
    else if (TPRegexp("a818").MatchB(amiTag))   { return "MC15c"; }
    else if (TPRegexp("r9364").MatchB(amiTag))  { return "MC16a"; }
    else if (TPRegexp("r9781").MatchB(amiTag))  { return "MC16c"; }
    else if (TPRegexp("r10201").MatchB(amiTag)) { return "MC16d"; }
    else if (TPRegexp("r10724").MatchB(amiTag)) { return "MC16e"; }
    else {
      fatal(Form("Failed to determine mcType from amiTag %s.", amiTag.Data()));
    }

    return "MC16a";
  }

  bool GlobalVariables::isAFII()
  {
    if (m_isAFII == FlagNotSet) { HG::fatal(Form(error_message, "isAFII")); }

    return (bool)m_isAFII;
  }

  TString GlobalVariables::mcType()
  {
    if (m_mcType == "") { HG::fatal(Form(error_message, "mcType")); }

    return m_mcType;
  }

  bool GlobalVariables::isMC()
  {
    if (m_isMC == FlagNotSet) { HG::fatal(Form(error_message, "isMC")); }

    return (bool)m_isMC;
  }

  bool GlobalVariables::isData()
  {
    if (m_isMC == FlagNotSet) { HG::fatal(Form(error_message, "isData")); }

    return !isMC();
  }

  bool GlobalVariables::isMAOD()
  {
    if (the_inputtype == InputTypeNotSet) { HG::fatal(Form(error_message, "isMAOD")); }

    return the_inputtype == InputTypeMAOD;
  }

  bool GlobalVariables::isAOD()
  {
    if (the_inputtype == InputTypeNotSet) { HG::fatal(Form(error_message, "isAOD")); }

    return the_inputtype == InputTypeAOD;
  }

  bool GlobalVariables::isDAOD()
  {
    if (the_inputtype == InputTypeNotSet) { HG::fatal(Form(error_message, "isDAOD")); }

    return !isAOD() && !isMAOD();
  }
}
