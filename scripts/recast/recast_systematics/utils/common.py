import os
import sys
import functools
import multiprocessing
from multiprocessing import Pool
from concurrent.futures  import ProcessPoolExecutor

class job_manager:

    def __init__(self, func, nProc=8):
        self.task_args = []
        self.func      = func
        self.nProc     = nProc
        self.pool      = Pool(nProc)

    def add_task(self, task_arg):
        self.task_args.append( task_arg)

    def set_task_args(self, task_args):
        self.task_args = task_args

    def submit(self):
        self.pool.map(self.func, self.task_args)

def get_cpu_count():
    return os.cpu_count()

def parallel_run(func, *iterables, max_workers):

    with ProcessPoolExecutor(max_workers) as executor:
        result = executor.map(func, *iterables)

    return [i for i in result]

def execute_multi_tasks(func, *iterables, parallel):
    if parallel == 0:
        result = []
        for args in zip(*iterables):
            result.append(func(*args))
        return result
    else:
        if parallel == -1:
            max_workers = get_cpu_count()
        else:
            max_workers = parallel
        return parallel_run(func, *iterables, max_workers=max_workers)
        