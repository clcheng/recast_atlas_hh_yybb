#pragma once

// STL include(s):
#include <vector>

// EDM include(s):
#include "AsgTools/AsgMessaging.h"

// ROOT includes
#include "TLorentzVector.h"
#include "TMVA/Reader.h"
#include "TString.h"


// Forward declarations
namespace HG { class EventHandler; class TruthHandler; }

namespace HG {
  /*!
     @class yyHFTool
     @brief The main class for the Higgs+HF measurement.

     This tool, included in the HGamAnalysisFramework, contains all the functions designed
     specifically for the Higgs+HF measurement.
   */
  class yyHFTool : public asg::AsgMessaging {
  public:
    /**
     * @brief Constructor
     * @param eventHandler The HGam eventHandler to write information to EventInfo
     * @param truthHandler The HGam truthHandler to read truth information
     */
    yyHFTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler);

    /*! Destructor, deallocates resources used by class instance. */
    virtual ~yyHFTool();

    /*!
       @fn virtual EL::StatusCode initialize(Config &config)
       @brief Set or fetch default values for members from config
       @param config
     */
    virtual EL::StatusCode initialize(Config &config);

    /*!
       @enum yyHFCutflowEnum
       @brief Latest step in the cutflow that is passed by the event
     */
    enum yyHFCutflowEnum {PHOTONS = 0, MYY = 1, CENJET_MIN = 2, PASS = 3};

    /*!
       @enum yyHFDiJetCat
       @brief yy+HF DiJetCategory
     */

    enum yyHFDiJetCat {BB = 0, BC = 1, BL = 2, CC = 3, CL = 4, LL = 5, NOTTWOJETS = 6};

    /*!
       @enum yyHFCat
       @brief yy+HF JetCategory
     */

    enum yyHFJetCat {B = 0, C = 1, L = 2};


    /**
     * @brief Function to save all the Higgs+HF information
     * @details
     *
     * @param photons Container with all photons
     * @param jets Container with all jets
     * @param jets_noJVT Container with all jets, no JVT cut
     * @param MET Container
     * @param muons Container with all muons
     * @param electron Container with all electrons
     */
    void saveyyHFInfo(const xAOD::PhotonContainer &photons,
                      const xAOD::JetContainer &jets,
                      xAOD::JetContainer &jets_noJVT,
                      const xAOD::MissingETContainer &met,
                      xAOD::MuonContainer &muons,
                      xAOD::ElectronContainer &electrons);

    /**
     * @brief Function to save all the Higgs+HF truth information
     * @details
     *
     * @param photons Container with truth photons
     * @param jets Container with truth jets
     * @param bjet Container with truth jets matched to a b-hadron
     * @param cjet Container with truth jets matched to a c-hadron
     * @param Truth MET Container
     * @param muons Container with truth muons
     * @param electron Container with truth electrons
     */
    void saveyyHFTruthInfo(const xAOD::TruthParticleContainer &photons,
                           const xAOD::JetContainer   &jets,
                           const xAOD::JetContainer   &bjets,
                           const xAOD::JetContainer   &cjets,
                           const xAOD::MissingETContainer &met,
                           xAOD::TruthParticleContainer &muons,
                           xAOD::TruthParticleContainer &electrons);


  private:
    HG::EventHandler *m_eventHandler; //!
    HG::TruthHandler *m_truthHandler; //!

    // User-configurable selection options;
    double m_minMuonPT;             //! Minimum pT of muons for lepton veto (in GeV)
    double m_minElectronPT;         //! Minimum pT of electrons for lepton veto (in GeV)
    float m_centraljet_eta_max;     //! Maximum eta of central jets
    float m_truthjet_rapidity_max;  //! Maximum eta of truth jets
    float m_centraljet_pt_min;      //! Minimum  pt of central jets (in GeV)

    /**
     * @brief Accessor for event handler
     */
    virtual HG::EventHandler *eventHandler();

    /**
     * @brief Accessor for truth handler
     */
    virtual HG::TruthHandler *truthHandler();

    /*!
       \fn void saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString,int> &eventInfoInts)
       \brief Write maps to eventInfo
     */
    void saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);
    void saveMapsToTruthEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);

    /*!
       \fn void performSelection(xAOD::PhotonContainer photons, xAOD::JetContainer& jets, xAOD::JetContainer& jets_noJVT, xAOD::MuonContainer& muons, xAOD::ElectronContainer& electrons, std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);
       \brief Function which performs the Higgs+HF preselection and saves info to info maps (nominal by default)
       \param photons Container with all photons
       \param jets Container with all jets
       \param jets_noJVT Container with all jets and no JVT cut
       \param met Container for MET
       \param muons Container with all muons
       \param electron Container with all electrons
       \param eventInfoFloats Map which has selected float info appended to it for saving to file
       \param eventInfoInts Map which has selected ints info appended to it for saving to file
       \param label String to include in the addtional information
     */
    void performSelection(const xAOD::PhotonContainer &photons,
                          const xAOD::JetContainer &jets,
                          xAOD::JetContainer &jets_noJVT,
                          const xAOD::MissingETContainer &met,
                          xAOD::MuonContainer &muons,
                          xAOD::ElectronContainer &electrons,
                          std::map<TString, float> &eventInfoFloats,
                          std::map<TString, int> &eventInfoInts);

    /*!
       \fn     void performTruthSelection(xAOD::TruthParticleContainer photons, xAOD::JetContainer   &jets,xAOD::JetContainer   &bjets, xAOD::JetContainer   &cjets, const xAOD::MissingETContainer &met, xAOD::TruthParticleContainer &muons, xAOD::TruthParticleContainer &electrons, std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);
       \brief Function which performs the Higgs+HF preselection and saves info to info maps (nominal by default)
       \param photons Container with truth photons
       \param jets Container with truth jets
       \param bjet Container with truth jets matched to a b-hadron
       \param cjet Container with truth jets matched to a c-hadron
       \param met Container with truth MET
       \param muons Container with truth muons
       \param electron Container with truth electrons
       \param eventInfoFloats Map which has selected float info appended to it for saving to file
       \param eventInfoInts Map which has selected ints info appended to it for saving to file
       \param label String to include in the addtional information
     */

    void performTruthSelection(const xAOD::TruthParticleContainer &photons,
                               const xAOD::JetContainer   &jets,
                               const xAOD::JetContainer   &bjets,
                               const xAOD::JetContainer   &cjets,
                               const xAOD::MissingETContainer &met,
                               xAOD::TruthParticleContainer &muons,
                               xAOD::TruthParticleContainer &electrons,
                               std::map<TString, int> &eventInfoInts, std::map<TString, float> &eventInfoFloats);


    /*!
       \fn double weightJVT(yyHFTool::massCatEnum massCat, xAOD::JetContainer &jets_noJVT)
       \brief Fetch JVT (in)efficiency SF
       \param jets_noJVT The jet container for jets before JVT is applied.
     */
    double weightJVT(xAOD::JetContainer &jets_noJVT);

  };

}
