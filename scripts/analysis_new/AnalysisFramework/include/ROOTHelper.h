#pragma once

#include <string>
class TFile;
class TList;

class ROOTHelper {
public:
	static TFile *GetTFile(std::string sample,std::string mcset, std::string fileName);
	static TList *GetListOfFiles(const char *filename);    
	static void setATLASStyle();
};
