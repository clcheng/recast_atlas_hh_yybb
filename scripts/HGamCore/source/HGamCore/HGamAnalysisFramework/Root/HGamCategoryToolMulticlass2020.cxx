#include "HGamAnalysisFramework/HGamCategoryToolMulticlass2020.h"

#include <iterator>
#include <algorithm>

using namespace Coupling2020;

CategorizationMultiClass::CategorizationMultiClass(const std::string &name,
                                                   TTree *tree_multiclass,
                                                   const std::vector<float> &weight_multiclass)
  : asg::AsgMessaging(name),
    m_bdt_multiclass(tree_multiclass),
    m_weight_multiclass(weight_multiclass)
{
  m_nclasses = weight_multiclass.size();
}

std::string CategorizationMultiClass::get_category_name(unsigned int icat) const
{
  if (icat >= m_nclasses) {
    ATH_MSG_FATAL("invalid category index " << icat << " should be in [0, " << m_nclasses << ")");
  }

  return "cat_" + std::to_string(icat);
}

int CategorizationMultiClass::get_category(const CategorizationInputs &inputs) const
{
  //ATH_MSG_INFO("getting inputs for multiclass");
  const std::vector<float> input_values_multiclass = get_input(inputs);

  //ATH_MSG_INFO("getting multi response");
  const std::vector<float> raw_multi_response = m_bdt_multiclass.GetMultiResponse(input_values_multiclass, m_nclasses);
  std::vector<float> multi_response(raw_multi_response.size());

  //ATH_MSG_INFO("multi response before d-optimality: ");

  // for (auto v : raw_multi_response) { ATH_MSG_INFO(v); }

  for (std::size_t i = 0; i != multi_response.size(); ++i) {
    multi_response[i] = raw_multi_response[i] * m_weight_multiclass[i];
  }

  //ATH_MSG_INFO("multi response after d-optimality: ");

  // for (auto v : multi_response) { ATH_MSG_INFO(v); }

  const int max_element_index = std::distance(
                                  std::begin(multi_response),
                                  std::max_element(std::begin(multi_response),
                                                   std::end(multi_response)));

  // ATH_MSG_INFO("multiclass best category: " << max_element_index);

  return max_element_index;
}
