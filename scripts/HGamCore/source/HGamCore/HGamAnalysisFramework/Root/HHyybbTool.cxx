///////////////////////////////////////////
// HHyybb Tool
// Tool performs all yybb selections
// All info decorated to MxAODs
// @author: james.robinson@cern.ch
// @author: alan.james.taylor@cern.ch
///////////////////////////////////////////

// STL include(s):
#include <algorithm>
#include <cmath>
#include <fstream>

// EDM include(s):
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"
#include "hhTruthWeightTools/hhWeightTool.h"
#include "xAODCore/ShallowCopy.h"

// ROOT include(s):
#include "TLorentzVector.h"
#include "TMatrixDSymEigen.h"
#include "TMatrixDSym.h"
#include "TVectorD.h"

// TMVA include(s):
#include "TMVA/Reader.h"
#include "TMVA/Tools.h"
#include "TMVA/Factory.h"
#include "TMVA/Types.h"
#include "TMVA/DataLoader.h"

// XGBoost include(s):
#include "xgboost/c_api.h"

// Local include(s):
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/HGamVariables.h"
#include "HGamAnalysisFramework/HHyybbTool.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"

#include "HGamAnalysisFramework/HGamVariables.h"

#include <numeric>

namespace HG {
  // _______________________________________________________________________________________
  HHyybbTool::HHyybbTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler)
    : asg::AsgMessaging(name)
    , m_eventHandler(eventHandler)
    , m_truthHandler(truthHandler)
    , m_xgboost_yybb_highMass(nullptr)
    , m_xgboost_yybb_lowMass(nullptr)
    , m_xgboost_yybb_noHT_highMass(nullptr)
    , m_xgboost_yybb_noHT_lowMass(nullptr)
    , m_xgboost_yybb_noMbb_highMass(nullptr)
    , m_xgboost_yybb_noMbb_lowMass(nullptr)
    , m_xgboost_yybb_vbf(nullptr)
    , m_j1_pTMin(25.0)
    , m_j2_pTMin(25.0)
    , m_mjj_min(60.0)
    , m_mjj_max(190.0)
    , m_myy_min(120.0)
    , m_myy_max(130.0)
    , m_auxIntsToSave( {"HadronConeExclTruthLabelID", "DL1r_bin"})
  , m_auxFloatsToSave({"OneMu_pt", "OneMu_eta", "OneMu_phi", "OneMu_m", "PtRecoSF", "NNRegPtSF"})
  , m_auxDoublesToSave({"DL1r_discriminant"})
  // default jets where the variables computed are saved to systematics
  , m_jetPreselections({HHyybbTool::WP77, HHyybbTool::WP85})
  , m_Corrections({HHyybbTool::noCor})
  , m_analysisSelections({HHyybbTool::cutBased})
  {}


  // _______________________________________________________________________________________
  // DO NOT MOVE THIS TO THE HEADER - the destructor must be able to see the destructors for all classes which use std::unique_ptr
  HHyybbTool::~HHyybbTool()
  {
    XGBoosterFree(*m_xgboost_yybb_highMass);
    XGBoosterFree(*m_xgboost_yybb_lowMass);
    XGBoosterFree(*m_xgboost_yybb_noHT_highMass);
    XGBoosterFree(*m_xgboost_yybb_noHT_lowMass);
    XGBoosterFree(*m_xgboost_yybb_noMbb_highMass);
    XGBoosterFree(*m_xgboost_yybb_noMbb_lowMass);
    XGBoosterFree(*m_xgboost_yybb_withTop_highMass);
    XGBoosterFree(*m_xgboost_yybb_withTop_lowMass);
    XGBoosterFree(*m_xgboost_yybb_vbf);
  }


  // _______________________________________________________________________________________
  HG::EventHandler *HHyybbTool::eventHandler()
  {
    return m_eventHandler;
  }


  // _______________________________________________________________________________________
  HG::TruthHandler *HHyybbTool::truthHandler()
  {
    return m_truthHandler;
  }

  // _______________________________________________________________________________________
  EL::StatusCode HHyybbTool::initialize(Config &config)
  {
    // Set default cut values etc. here...
    // For MxAOD production, the defaults will be used almost everywhere

    // Check if we are doing a detailed run or not
    m_specificHHyybb = config.getBool("HHyybb.SpecificInfo", false);
    m_detailedHHyybb = config.getBool("HHyybb.DetailedInfo", false);
    ATH_MSG_INFO("Write out specific information?............... " << (m_specificHHyybb ? "YES" : "NO"));
    ATH_MSG_INFO("Write out detailed information?............... " << (m_detailedHHyybb ? "YES" : "NO"));

    m_specificVariables = config.getStrV("HHyybb.SpecificVariables", {});

    // Muon and jet configuration
    m_muon_pTmin = config.getNum("HHyybbTool.MuonPtMin", 4.0); //! all muons to be considered for muon correction must have pT > m_muon_pTmin (in GeV)
    ATH_MSG_INFO("MuonPtMin..................................... " << m_muon_pTmin << " GeV");
    m_muon_DRmax = config.getNum("HHyybbTool.MuonJetDRMax", 0.4); //! all muons to be considered for muon correction must have dR(muon, jet) < m_muon_DRmax
    ATH_MSG_INFO("MuonJetDRMax.................................. " << m_muon_DRmax);
    m_electron_pTmin = config.getNum("HHyybbTool.ElectronPtMin", 4.0); //! all electrons to be considered for electron correction must have pT > m_electron_pTmin (in GeV)
    ATH_MSG_INFO("ElectronPtMin................................. " << m_electron_pTmin << " GeV");
    m_electron_DRmax = config.getNum("HHyybbTool.ElectronJetDRMax", 0.4); //! all electrons to be considered for electron correction must have dR(electron, jet) < m_electron_DRmax
    ATH_MSG_INFO("ElectronJetDRMax.............................. " << m_electron_DRmax);
    m_jet_eta_max = config.getNum("HHyybbTool.JetEtaMax", 2.5); //! only central jets are b-tagged
    ATH_MSG_INFO("JetEtaMax..................................... " << m_jet_eta_max);
    m_massHiggs = config.getNum("HHyybbTool.HiggsMass", 125.0); //! what we consider the Higgs mass to be
    ATH_MSG_INFO("HiggsMass..................................... " << m_massHiggs);

    // Initialise the dihiggs NLO(ish) to NLO tool
    m_hhWeightTool.reset(new xAOD::hhWeightTool("hhWeights"));
    ANA_CHECK(m_hhWeightTool->initialize());

    // If we're in detailed mode then run all combinations of selections
    //if (m_detailedHHyybb) {
    m_Corrections.push_back(HHyybbTool::BJetCal);
    m_Corrections.push_back(HHyybbTool::BJetReg);
    m_Corrections.push_back(HHyybbTool::KF);
    //m_leptonCorrections.push_back(HHyybbTool::allLep);
    // }

    // Setup MVA reader
    m_xgboost_yybb_highMass = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_highMass);
    XGBoosterLoadModel(*m_xgboost_yybb_highMass, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_highMass_bdt.h5").c_str());

    m_xgboost_yybb_lowMass = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_lowMass);
    XGBoosterLoadModel(*m_xgboost_yybb_lowMass, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_lowMass_bdt.h5").c_str());

    m_xgboost_yybb_noHT_highMass = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_noHT_highMass);
    XGBoosterLoadModel(*m_xgboost_yybb_noHT_highMass, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_noHT_highMass_bdt.h5").c_str());

    m_xgboost_yybb_noHT_lowMass = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_noHT_lowMass);
    XGBoosterLoadModel(*m_xgboost_yybb_noHT_lowMass, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_noHT_lowMass_bdt.h5").c_str());

    m_xgboost_yybb_noMbb_highMass = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_noMbb_highMass);
    XGBoosterLoadModel(*m_xgboost_yybb_noMbb_highMass, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_noMbb_highMass_bdt.h5").c_str());

    m_xgboost_yybb_noMbb_lowMass = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_noMbb_lowMass);
    XGBoosterLoadModel(*m_xgboost_yybb_noMbb_lowMass, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_noMbb_lowMass_bdt.h5").c_str());

    m_xgboost_yybb_withTop_highMass = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_withTop_highMass);
    XGBoosterLoadModel(*m_xgboost_yybb_withTop_highMass, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_withTop_highMass_bdt.h5").c_str());

    m_xgboost_yybb_withTop_lowMass = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_withTop_lowMass);
    XGBoosterLoadModel(*m_xgboost_yybb_withTop_lowMass, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_withTop_lowMass_bdt.h5").c_str());

    m_xgboost_yybb_vbf = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_yybb_vbf);
    XGBoosterLoadModel(*m_xgboost_yybb_vbf, PathResolverFindCalibFile("HGamAnalysisFramework/model_yybb_VBF_bdt.dat").c_str());

    // initialize Kinematic Fit Tool
    m_kinematicfitTool = new KinematicFitTool("yybbKinematicFitTool");
    ANA_CHECK(m_kinematicfitTool->setProperty("Jet Collection", "AntiKt4EMPFlow"));
    ANA_CHECK(m_kinematicfitTool->initialize());

    // initialize MVA Reader for resonant bbyy analysis
    m_readerMVA_yy = new TMVA::Reader("Color");
    m_readerMVA_yy->AddVariable("m_bb", &m_bb_ini);
    m_readerMVA_yy->AddVariable("m_yyjj", &m_yyjj_tilde_ini);
    m_readerMVA_yy->AddVariable("pT_yy", &pT_yy_ini);
    m_readerMVA_yy->AddVariable("pT_bb", &pT_bb_ini);
    m_readerMVA_yy->AddVariable("deltaR_yy", &deltaR_yy_ini);
    m_readerMVA_yy->AddVariable("deltaR_bb", &deltaR_bb_ini);
    m_readerMVA_yy->AddVariable("deltaR_yybb", &deltaR_yybb_ini);
    m_readerMVA_yy->AddVariable("deltaPhi_yy", &deltaPhi_yy_ini);
    m_readerMVA_yy->AddVariable("deltaPhi_bb", &deltaPhi_bb_ini);
    m_readerMVA_yy->AddVariable("deltaPhi_bbyy", &deltaPhi_bbyy_ini);
    m_readerMVA_yy->AddVariable("Rap_yy", &Rap_yy_ini);
    m_readerMVA_yy->AddVariable("Rap_bb", &Rap_bb_ini);
    m_readerMVA_yy->AddVariable("deltaRap_bbyy", &deltaRap_bbyy_ini);
    m_readerMVA_yy->AddVariable("N_j", &N_j_ini);
    m_readerMVA_yy->AddVariable("N_j_btag", &N_j_btag_ini);
    m_readerMVA_yy->AddVariable("pT_bbyy", &pT_bbyy_ini);
    m_readerMVA_yy->AddVariable("HT_all", &HTall_30_ini);
    std::string path_MVA_yy_1 = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/TMVAClassification_resonant_mva_train_yy_no_myy_BDTG.weights.xml");
    std::string path_MVA_yy_2 = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/TMVAClassification_resonant_mva_train_yy_no_myy_BDTG_2.weights.xml");
    ATH_MSG_INFO("Reading bbyy resonant BDT XML file:  " << path_MVA_yy_1);
    ATH_MSG_INFO("Reading bbyy resonant BDT XML file:  " << path_MVA_yy_2);
    m_readerMVA_yy->BookMVA("smvaother1_yy", path_MVA_yy_1.c_str());
    m_readerMVA_yy->BookMVA("smvaother2_yy", path_MVA_yy_2.c_str());


    m_readerMVA_ttH = new TMVA::Reader("Color");
    m_readerMVA_ttH->AddVariable("m_bb", &m_bb_ini);
    m_readerMVA_ttH->AddVariable("m_yyjj", &m_yyjj_tilde_ini);
    m_readerMVA_ttH->AddVariable("pT_yy", &pT_yy_ini);
    m_readerMVA_ttH->AddVariable("pT_bb", &pT_bb_ini);
    m_readerMVA_ttH->AddVariable("deltaR_yy", &deltaR_yy_ini);
    m_readerMVA_ttH->AddVariable("deltaR_bb", &deltaR_bb_ini);
    m_readerMVA_ttH->AddVariable("deltaR_yybb", &deltaR_yybb_ini);
    m_readerMVA_ttH->AddVariable("deltaPhi_yy", &deltaPhi_yy_ini);
    m_readerMVA_ttH->AddVariable("deltaPhi_bb", &deltaPhi_bb_ini);
    m_readerMVA_ttH->AddVariable("deltaPhi_bbyy", &deltaPhi_bbyy_ini);
    m_readerMVA_ttH->AddVariable("Rap_yy", &Rap_yy_ini);
    m_readerMVA_ttH->AddVariable("Rap_bb", &Rap_bb_ini);
    m_readerMVA_ttH->AddVariable("deltaRap_bbyy", &deltaRap_bbyy_ini);
    m_readerMVA_ttH->AddVariable("N_j", &N_j_ini);
    m_readerMVA_ttH->AddVariable("N_j_btag", &N_j_btag_ini);
    m_readerMVA_ttH->AddVariable("pT_bbyy", &pT_bbyy_ini);
    m_readerMVA_ttH->AddVariable("HT_all", &HTall_30_ini);
    m_readerMVA_ttH->AddVariable("MET", &MET_ini);
    std::string path_MVA_ttH_1 = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/TMVAClassification_resonant_mva_train_ttH_no_myy_BDTG.weights.xml");
    std::string path_MVA_ttH_2 = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/TMVAClassification_resonant_mva_train_ttH_no_myy_BDTG_2.weights.xml");
    ATH_MSG_INFO("Reading bbyy resonant BDT XML file:  " << path_MVA_ttH_1);
    ATH_MSG_INFO("Reading bbyy resonant BDT XML file:  " << path_MVA_ttH_2);
    m_readerMVA_ttH->BookMVA("smvaother1_ttH", path_MVA_ttH_1.c_str());
    m_readerMVA_ttH->BookMVA("smvaother2_ttH", path_MVA_ttH_2.c_str());

    return EL::StatusCode::SUCCESS;
  }


  // _______________________________________________________________________________________
  // Perform HHyybb selection and write relevant outputs to the output MxAOD
  void HHyybbTool::saveHHyybbInfo(const xAOD::PhotonContainer    &photons,
                                  const xAOD::ElectronContainer  &electrons,
                                  const xAOD::MuonContainer      &muons,
                                  xAOD::JetContainer             &jets,
                                  const xAOD::JetContainer       &jets_no_JVT,
                                  const xAOD::MissingETContainer &met)
  {
    // --------
    // Clear maps containing all event info to decorate to MxAOD
    m_eventFloats.clear();
    m_eventInts.clear();
    m_eventFloatsDetailed.clear();

    // --------
    // Reset the per-event jet quantities to 0 at the start of each event
    m_nJets = 0;
    m_HTJets = 0;
    m_nBJets85 = 0;
    m_nBJets77 = 0;
    m_nCentralJets = 0;
    m_weightBTagging = 0.0;
    m_min_ChiWt = -99.0;

    // --------
    // Apply minimum pT cuts to electrons and muons
    ATH_MSG_DEBUG("About to apply kinematic preselection cuts to leptons");
    applyLeptonPtCuts(electrons, muons);

    // --------
    // Add DL1r bin and other decorations to jets
    ATH_MSG_DEBUG("About to decorate jets with tagging and kinematic information");
    decorateJets(jets);

    // --------
    // Construct one output jet collection for each jet preselection possibility
    ATH_MSG_DEBUG("About to record candidate jet collections to the output");
    recordCandidateJetCollections(jets, jets_no_JVT);

    // --------
    // Calculate jet related quantities used later in analysis selection
    calcJetQuantities(jets);

    // --------
    // Perform selection using discrete DL1r WPs
    ATH_MSG_DEBUG("About to apply HHyybb analysis cuts for each jet preselection choice");

    for (auto jetPreselection : m_jetPreselections) {
      performSelection(photons, jets,  jetPreselection, met);
    }

    // --------
    // Save all the per-event quantities to the eventHandler()
    // Should always be called last, once all of these have been filled
    recordPerEventQuantities();
  }


  // _______________________________________________________________________________________
  // Function which performs full yybb selection for a given jet preselection and for all possible lepton corrections
  void HHyybbTool::performSelection(const xAOD::PhotonContainer &photons, xAOD::JetContainer &jets, const HHyybbTool::jetPreselectionEnum jetPreSelType, const xAOD::MissingETContainer &met)
  {
    // -------
    // Get jet pre-selection label
    // TString jetPreSelLabel = getJetPreselLabel(jetPreSelType);

    // --------
    // Get the candidate jets: best two jets from (un)tagged jets
    // This uses jets from *before* the lepton-correction
    const xAOD::JetContainer *candidateJets = nullptr;

    if (eventHandler()->evtStore()->retrieve(candidateJets, getJetCollectionName(jetPreSelType)).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't retrieve some jets, throwing exception"); }

    // --------
    // Set the four object information
    for (auto Correction : m_Corrections) {
      // Create, store and retrieve lepton-corrected shallow copies of the candidate jets
      const xAOD::JetContainer *correctedCandidateJets = nullptr;

      if (Correction == HHyybbTool::noCor) {
        correctedCandidateJets = candidateJets;
      } else if (Correction != HHyybbTool::KF) {
        if (eventHandler()->evtStore()->retrieve(correctedCandidateJets, shallowCopyCorrectedJets(jetPreSelType, Correction)).isFailure())
        { throw std::runtime_error("HHyybbTool: Couldn't retrieve b-corrected jets, throwing exception"); }
      } else {
        if (eventHandler()->evtStore()->retrieve(correctedCandidateJets, shallowCopyFittedJets(photons, jetPreSelType, Correction)).isFailure())
        { throw std::runtime_error("HHyybbTool: Couldn't retrieve KF jets, throwing exception"); }
      }


      // initialise preselection flag
      m_passpreselection = false;

      // Calculate masses and perform cutflow
      calculateMassesAndCutflow(photons, correctedCandidateJets, jetPreSelType, Correction);

      // Get VBF Jets
      getVBFjets(correctedCandidateJets, &jets, jetPreSelType, Correction);

      // Perform the cut based analysis
      performCutBased(photons, correctedCandidateJets, jetPreSelType, Correction);

      // Perform the XGBoost based analysis
      performXGBoostBDT(&photons, correctedCandidateJets, jetPreSelType, Correction, &met, &jets);


      makeMVAResonantScore(&photons, correctedCandidateJets, &met, jetPreSelType, Correction);

    }
  }


  // _______________________________________________________________________________________
  // Write containers for leptons passing the pT cuts to the event store (transient)
  void HHyybbTool::applyLeptonPtCuts(const xAOD::ElectronContainer &electrons, const xAOD::MuonContainer &muons)
  {
    // --------
    // Apply pT cuts to electrons
    auto selectedElectrons = std::make_unique< ConstDataVector<xAOD::ElectronContainer> >(SG::VIEW_ELEMENTS);

    for (const auto electron : electrons) {
      if (electron && (electron->pt() >= m_electron_pTmin * HG::GeV)) { selectedElectrons->push_back(electron); }
    }

    if (eventHandler()->evtStore()->record(selectedElectrons.release(), "yybb_electrons_passing_pT" + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of electron collection to TStore, throwing exception"); }

    // ... and to muons
    auto selectedMuons = std::make_unique< ConstDataVector<xAOD::MuonContainer> >(SG::VIEW_ELEMENTS);

    for (const auto muon : muons) {
      if (muon && (muon->pt() >= m_muon_pTmin * HG::GeV)) { selectedMuons->push_back(muon); }
    }

    if (eventHandler()->evtStore()->record(selectedMuons.release(), "yybb_muons_passing_pT" + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of muon collection to TStore, throwing exception"); }


  }


  // _______________________________________________________________________________________
  // Write deep-copies of the candidate jets to the output file for the various pre-selection choices
  // Also write central-jets-before-JVT to the event store (transient)
  void HHyybbTool::recordCandidateJetCollections(xAOD::JetContainer &jets, const xAOD::JetContainer &jets_no_JVT)
  {
    // --------
    // Apply central jet requirement (for b-tagging reasons)
    xAOD::JetContainer jets_central(SG::VIEW_ELEMENTS);
    std::copy_if(jets.begin(), jets.end(), std::back_inserter(jets_central), [this](const xAOD::Jet * j) {
      return fabs(j->eta()) <= m_jet_eta_max;
    });

    // ... and do the same for the jets before JVT
    auto jets_no_JVT_passing_cuts = std::make_unique< ConstDataVector<xAOD::JetContainer> >(SG::VIEW_ELEMENTS);
    std::copy_if(jets_no_JVT.begin(), jets_no_JVT.end(), std::back_inserter(*jets_no_JVT_passing_cuts), [this](const xAOD::Jet * j) {
      return fabs(j->eta()) <= m_jet_eta_max;
    });

    if (eventHandler()->evtStore()->record(jets_no_JVT_passing_cuts.release(), "yybb_jets_central_no_JVT" + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection to TStore, throwing exception"); }


    // --------
    // Choose jets using the discrete DL1r bins
    for (auto jetPreselection : m_jetPreselections) {
      recordSingleJetCollection(jets_central, jetPreselection);
    }
  }


  // _______________________________________________________________________________________
  // Record a new deep copied jet collection to the output file
  void HHyybbTool::recordSingleJetCollection(xAOD::JetContainer &jets_central, const HHyybbTool::jetPreselectionEnum jetPreSelCat)
  {
    // Create the new container and its auxiliary store.
    auto selectedJets = std::make_unique<xAOD::JetContainer>();
    auto selectedJetsAux = std::make_unique<xAOD::AuxContainerBase>();
    selectedJets->setStore(selectedJetsAux.get());

    // Get sorted jets
    std::vector<int> sorted_index = sortJets(jets_central);

    // index of the 2 candidate jets
    int idx1 = -1, idx2 = -1;

    if (jets_central.size() >= 2) {
      idx1 = sorted_index[0];
      idx2 = sorted_index[1];

      // Make a new jet which copies the auxdata from the two highest scoring jets
      for (size_t idx = 0; idx < 2; ++idx) {
        xAOD::Jet *selectedJet = new xAOD::Jet();
        selectedJets->push_back(selectedJet);
        // Only copy specific auxdata to reduce the size (otherwise we could use "*selectedJet = *sortedJets.at(idx);" instead of the following lines)
        selectedJet->setJetP4(jets_central.at(sorted_index[idx])->jetP4());

        for (auto name : m_auxIntsToSave) { selectedJet->auxdata<int>(name) = jets_central.at(sorted_index[idx])->auxdata<int>(name); }

        for (auto name : m_auxFloatsToSave) { selectedJet->auxdata<float>(name) = jets_central.at(sorted_index[idx])->auxdata<float>(name); }

        for (auto name : m_auxDoublesToSave) { selectedJet->auxdata<double>(name) = jets_central.at(sorted_index[idx])->auxdata<double>(name); }
      }

      // Sort by pT before returning
      std::sort(selectedJets->begin(), selectedJets->end(), [](const auto & i, const auto & j) { return i->pt() > j->pt(); });
    }

    m_eventInts["yybb_candidate_jet1"] = idx1;
    m_eventInts["yybb_candidate_jet2"] = idx2;

    // Write jet collections to output
    if (eventHandler()->evtStore()->record(selectedJets.release(), getJetCollectionName(jetPreSelCat) + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection (1) to TStore, throwing exception"); }

    if (eventHandler()->evtStore()->record(selectedJetsAux.release(), getJetCollectionName(jetPreSelCat) + HG::VarHandler::getInstance()->getSysName() + "Aux.").isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection (2) to TStore, throwing exception"); }

  }

  // _______________________________________________________________________________________
  // Pre-selection using discrete DL1r bins with pT used as a tie-breaker
  std::vector<int> HHyybbTool::sortJets(xAOD::JetContainer &jets_central)
  {

    std::vector<int> index(jets_central.size());
    int i = 0;
    std::iota(index.begin(), index.end(), i++); //Initializing the index vector

    // first sort jets by pT then do a stable sort by DL1r bin
    std::sort(index.begin(), index.end(), [&](int j, int k) {
      return jets_central.at(j)->pt() > jets_central.at(k)->pt();
    });

    std::stable_sort(index.begin(), index.end(),  [&](int j, int k) {
      return jets_central.at(j)->auxdata<int>("DL1r_bin") > jets_central.at(k)->auxdata<int>("DL1r_bin");
    });

    return index;
  }

  // _______________________________________________________________________________________
  // Decorate jets with lepton corrections, DL1r bin and other quantities
  void HHyybbTool::decorateJets(xAOD::JetContainer &jets)
  {
    for (auto jet : jets) {
      // --------
      // Decorate with |Delta phi| wrt other jets
      TLorentzVector otherjets;

      for (auto otherjet : jets) {
        if (jet != otherjet) { otherjets += otherjet->p4(); }
      }

      jet->auxdata<float>("yybb_abs_delta_phi_in_rad") = jets.size() > 1 ? fabs(otherjets.DeltaPhi(jet->p4()) / M_PI) : -1;

      // --------
      // Decorate with preselection neural net
      std::map<std::string, double> inputs = {
        {"jet_log_E", log(jet->e() / 1000.)},
        {"jet_log_pT", log(jet->pt() / 1000.)},
        {"jet_abs_eta", fabs(jet->eta())},
        {"jet_abs_delta_phi_in_rad", jet->auxdata<float>("yybb_abs_delta_phi_in_rad")},
      };
    }
  }


  // _______________________________________________________________________________________
  // Calculate a number of quantities from jets used later in the analysis selections
  void HHyybbTool::calcJetQuantities(const xAOD::JetContainer &jets)
  {
    for (auto jet : jets) {
      m_nJets++;

      m_HTJets += jet->pt();

      if (jet->auxdata<char>("DL1r_FixedCutBEff_85")) { m_nBJets85++; }

      if (jet->auxdata<char>("DL1r_FixedCutBEff_77")) { m_nBJets77++; }

      if (fabs(jet->eta()) <= m_jet_eta_max) { m_nCentralJets++; }
    }

    m_weightBTagging = calculateBTaggingWeight(jets);

    // --------
    // Get HHyybb weight from JVT, b-tagging and NLO scale factors
    m_eventFloats["yybb_weight"] = weightJVT() * m_weightBTagging * weightNLO();

    // ChiWt variable
    // apply a loose preselection before calculating ChiWt to save some time

    if (m_nCentralJets < 6 && m_nBJets77 >= 1 && m_nBJets77 < 3) {

      // Fill in the 4 vectors of the jets
      std::vector< TLorentzVector > temp_jets;

      for (unsigned int i = 0; i < jets.size(); i++) {
        temp_jets.push_back(jets.at(i)->p4());
      }

      // If there are < 3 jets (min. # required to define ChiWt) fill out the rest with 0, 0, 0, 0 dummy jets
      if (jets.size() < 3) {
        for (unsigned int i = 0; i < 3 - jets.size(); i++) {
          temp_jets.push_back(TLorentzVector(0, 0, 0, 0));
        }
      }

      // Loop over all permutations of jets to calculate ChiWt
      for (unsigned int i = 0; i < temp_jets.size(); i++) {  //i = bjet index
        for (unsigned int j = 0; j < temp_jets.size(); j++) {  //j = ljet1 index
          if (i == j) { continue; }     //indices must be distinct

          for (unsigned int k = 0; k < temp_jets.size(); k++) {    //k = ljet2 index
            if (i == k || k <= j) { continue; }     //indices must be distinct; require k > j since ljet1 <-> ljet2 in the definition

            float ChiWt = sqrt(pow((temp_jets.at(j) + temp_jets.at(k)).M() / 80000.0 - 1, 2) + pow((temp_jets.at(i) + temp_jets.at(j) + temp_jets.at(k)).M() / 173000.0 - 1, 2));

            if (ChiWt < m_min_ChiWt || m_min_ChiWt < 0) { m_min_ChiWt = ChiWt;}

          }
        }

      }
    }

  }


  // _______________________________________________________________________________________
  // Make a shallow copy of the input collection with corrected 4-vectors
  const std::string HHyybbTool::shallowCopyCorrectedJets(const HHyybbTool::jetPreselectionEnum &jetPreSelCat,
                                                         const HHyybbTool::CorrectionEnum &CorrType)
  {
    // Get input jet collection
    const xAOD::JetContainer *jets = nullptr;

    if (eventHandler()->evtStore()->retrieve(jets, getJetCollectionName(jetPreSelCat)).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't retrieve jets in shallowCopyCorrectedJets, throwing exception"); }


    // Make a shallow copy of the candidate jets
    auto correctedJetsShallowCopy = xAOD::shallowCopyContainer(*jets);
    std::unique_ptr<xAOD::JetContainer> correctedJets(correctedJetsShallowCopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> correctedJetsAux(correctedJetsShallowCopy.second);

    if (jets->size() > 0) {
      // Set the 4-vector for each jet
      for (auto jet : *correctedJets) {
        jet->setJetP4(getCorrectedFourVector(jet, CorrType));
      }

      // Sort by pT
      std::sort(correctedJets->begin(), correctedJets->end(), [](const auto & i, const auto & j) { return i->pt() > j->pt(); });
    }

    // Write jet collection to output
    const std::string outputName(getJetCollectionName(jetPreSelCat, CorrType));


    if (eventHandler()->evtStore()->record(correctedJets.release(), outputName + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection to TStore, throwing exception"); }

    if (eventHandler()->evtStore()->record(correctedJetsAux.release(), outputName + HG::VarHandler::getInstance()->getSysName() + "Aux.").isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection to TStore, throwing exception"); }

    return outputName;
  }

  // _______________________________________________________________________________________
  // Make a shallow copy of the input collection with corrected 4-vectors
  const std::string HHyybbTool::shallowCopyFittedJets(const xAOD::PhotonContainer &photon, const HHyybbTool::jetPreselectionEnum &jetPreSelCat,
                                                      const HHyybbTool::CorrectionEnum &CorrType)
  {
    // Get input jet collection
    const xAOD::JetContainer *jets = nullptr;

    if (eventHandler()->evtStore()->retrieve(jets, getJetCollectionName(jetPreSelCat)).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't retrieve jet collection in shallowCopyFittedJets, throwing exception"); }

    // Make a shallow copy of the candidate jets
    auto correctedJetsShallowCopy = xAOD::shallowCopyContainer(*jets);
    std::unique_ptr<xAOD::JetContainer> correctedJets(correctedJetsShallowCopy.first);
    std::unique_ptr<xAOD::ShallowAuxContainer> correctedJetsAux(correctedJetsShallowCopy.second);

    if (jets->size() > 0) {
      // Set the 4-vector for each jet
      for (auto jet : *correctedJets) {
        jet->setJetP4(getCorrectedFourVector(jet, HHyybbTool::BJetCal)); //Get BJetCalibration Correction before the fit
      }
    }

    if (m_kinematicfitTool->applyKF(photon, *correctedJets).isFailure()) //Applying the Fit
    { throw std::runtime_error("HHyybbTool: Couldn't do kinematic fit, throwing exception"); }

    if (jets->size() > 0) {
      std::sort(correctedJets->begin(), correctedJets->end(), [](const auto & i, const auto & j) { return i->pt() > j->pt(); });
    }

    const std::string outputName(getJetCollectionName(jetPreSelCat, CorrType));

    if (eventHandler()->evtStore()->record(correctedJets.release(), outputName + HG::VarHandler::getInstance()->getSysName()).isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection to TStore, throwing exception"); }

    if (eventHandler()->evtStore()->record(correctedJetsAux.release(), outputName + HG::VarHandler::getInstance()->getSysName() + "Aux.").isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't save copy of jet collection to TStore, throwing exception"); }

    return outputName;
  }


  // _______________________________________________________________________________________
  // Calls the selected lepton correction strategy
  xAOD::JetFourMom_t HHyybbTool::getCorrectedFourVector(xAOD::Jet *jet, const HHyybbTool::CorrectionEnum &CorrType)
  {
    if (jet->auxdata<int>("DL1r_bin") > 0) {

      if (CorrType == HHyybbTool::BJetCal) {
        return (jet->jetP4() + jet->jetP4("OneMu")) * jet->auxdata<float>("PtRecoSF");
      }

      if (CorrType == HHyybbTool::BJetReg) {

        xAOD::JetFourMom_t newjet(jet->pt() * jet->auxdata<float>("NNRegPtSF"), jet->eta(), jet->phi(), jet->m());
        return newjet;
      }
    } else {
      return jet->jetP4();
    }

    return jet->jetP4();
  }


  // _______________________________________________________________________________________
  // Set additional event info
  void HHyybbTool::calculateMassesAndCutflow(const xAOD::PhotonContainer             &selPhotons,
                                             const xAOD::JetContainer                *candidate_jets,
                                             const  HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                                             const HHyybbTool::CorrectionEnum  &CorrType)
  {
    TString jetPreSelLabel = getJetPreselLabel(jetPreSelType);
    TString CorrLabel   = getCorrLabel(CorrType);

    // Construct diphoton, jet and dijet 4-vectors
    TLorentzVector y1 = (selPhotons.size() >= 2) ? selPhotons.at(0)->p4() : TLorentzVector(0, 0, 0, 0);
    TLorentzVector y2 = (selPhotons.size() >= 2) ? selPhotons.at(1)->p4() : TLorentzVector(0, 0, 0, 0);
    TLorentzVector yy = y1 + y2;

    TLorentzVector j1 = (candidate_jets->size() >= 2) ? candidate_jets->at(0)->p4() : TLorentzVector(0, 0, 0, 0);
    TLorentzVector j2 = (candidate_jets->size() >= 2) ? candidate_jets->at(1)->p4() : TLorentzVector(0, 0, 0, 0);
    TLorentzVector jj = j1 + j2;

    // ... and the 4-body vector with and without applying the mH constraint
    TLorentzVector jj_cnstrnd = jj * (HHyybbTool::m_massHiggs * HG::GeV / jj.M());
    TLorentzVector yyjj = yy + jj;
    TLorentzVector yyjj_cnstrnd = yy + jj_cnstrnd;

    // Fill output values if there are two photons and two jets
    m_eventFloats["yybb_" + CorrLabel + "m_jj"] = (candidate_jets->size() >= 2) ? jj.M() : -99;
    m_eventFloats["yybb_" + CorrLabel + "m_yyjj"] = (candidate_jets->size() >= 2 && selPhotons.size() >= 2) ? yyjj.M() : -99;
    m_eventFloats["yybb_" + CorrLabel + "m_yyjj_cnstrnd"] = (candidate_jets->size() >= 2 && selPhotons.size() >= 2) ? yyjj_cnstrnd.M() : -99;
    m_eventFloats["yybb_" + CorrLabel + "m_yyjj_tilde"] = (candidate_jets->size() >= 2 && selPhotons.size() >= 2) ? yyjj.M() - yy.M() - jj.M() + 2 * HHyybbTool::m_massHiggs * HG::GeV : -99;


    // Calculate variables that can later be used in performSelection
    // only saved in the case of yybb-Detailed
    m_eventFloatsDetailed["yybb_" + CorrLabel + "scaled_pT_yy"] = (selPhotons.size() >= 2) ? yy.Pt() / yy.M() : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "eta_yy"] = (selPhotons.size() >= 2) ? yy.Eta() : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "phi_yy"] = (selPhotons.size() >= 2) ? yy.Phi() : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "deltaR_yy"] = (selPhotons.size() >= 2) ? y1.DeltaR(y2) : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "pT_jj"] = (candidate_jets->size() >= 2) ? jj.Pt() : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "eta_jj"] = (candidate_jets->size() >= 2) ? jj.Eta() : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "phi_jj"] = (candidate_jets->size() >= 2) ? jj.Phi() : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "deltaR_jj"] = (candidate_jets->size() >= 2) ? j1.DeltaR(j2) : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "deltaR_yyjj"] = (candidate_jets->size() >= 2 && selPhotons.size() >= 2) ? yy.DeltaR(jj) : -99;
    m_eventFloatsDetailed["yybb_" + CorrLabel + "min_ChiWt"] = m_min_ChiWt;

    // Now perform the cutflow
    // =======================

    // Cut 0 - PHOTONS Cut
    // :: at least 2 selected photons
    if (selPhotons.size() < 2) {
      m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "cutFlow"] = PHOTONS;
      return;
    }

    // Cut 1 - LEPVETO Cut
    // :: No lepton
    if (var::N_lep() != 0) {
      m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "cutFlow"] = LEPVETO;
      return;
    }

    // Cut 2 - CENJETSMIN Cut
    // :: at least 2 candidate jets
    if (candidate_jets->size() < 2) {
      m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "cutFlow"] = CENJETSMIN;
      return;
    }

    // Cut 3 - CENJETSVETO Cut
    // :: less than 6 central jets
    if (m_nCentralJets >= 6) {
      m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "cutFlow"] = CENJETSVETO;
      return;
    }

    // Cut 4 - BJETSMIN Cut
    // :: at least 2 btagged jets at 85 % or 2 jet at 77 %
    if ((jetPreSelType == HHyybbTool::WP77 && m_nBJets77 < 1) || (jetPreSelType == HHyybbTool::WP85 && m_nBJets85 < 2)) {
      m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "cutFlow"] = BJETSMIN;
      return;
    }

    // Cut 4 - BJETSVETO Cut
    // :: less than 3 btagged jets at 77 %
    if (m_nBJets77 >= 3) {
      m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "cutFlow"] = BJETSVETO;
      return;
    }

    // If we pass everything
    m_passpreselection = true;
    m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "cutFlow"] = PASSPRESEL;
  }


  // _______________________________________________________________________________________
  // Perform simple cut-based selection
  void HHyybbTool::performCutBased(const xAOD::PhotonContainer             &/*selPhotons*/,
                                   const xAOD::JetContainer                */*candidate_jets*/,
                                   const HHyybbTool::jetPreselectionEnum   &jetPreSelType,
                                   const HHyybbTool::CorrectionEnum  &CorrType)
  {
    TString jetPreSelLabel = getJetPreselLabel(jetPreSelType);
    TString CorrLabel = getCorrLabel(CorrType);
    TString fullLabel = "_cutBased_" + jetPreSelLabel + CorrLabel + "Cat";

    int Cutbased_cat = -99;

    Cutbased_cat = -99;

    // >= 2 central jets, 0 leptons, less than 6 central jets, >= 2 b-tags at 85% or >=1 77% working point, reject 3 b-tags at 77% working point.
    if (m_passpreselection) {

      Cutbased_cat = 0;

      // use variables already calculated by the CalculateMasses function
      float deltaR_yy = m_eventFloatsDetailed["yybb_" + CorrLabel + "deltaR_yy"];
      float deltaR_jj = m_eventFloatsDetailed["yybb_" + CorrLabel + "deltaR_jj"];
      float deltaR_yyjj = m_eventFloatsDetailed["yybb_" + CorrLabel + "deltaR_yyjj"];
      float m_yyjj_tilde = m_eventFloats["yybb_" + CorrLabel + "m_yyjj_tilde"] * HG::invGeV;
      float m_jj = m_eventFloats["yybb_" + CorrLabel + "m_jj"] * HG::invGeV;

      // Require tight m_bb selection and sort into categories
      if (m_jj >= 90.0 && m_jj <= 140.0) {
        // First selection is based on low/high mass
        if (m_yyjj_tilde >= 350.0) {
          if (deltaR_yy < 2.0 && deltaR_jj < 2.0 && deltaR_yyjj < 3.4) {
            if (m_nBJets77 == 2)   {
              Cutbased_cat += 4;
            } else {
              Cutbased_cat += 3;
            }
          }
        } else  {
          if (m_nBJets77 == 2)   {
            Cutbased_cat += 2;
          } else {
            Cutbased_cat += 1;
          }
        }
      }

      // Repeat the selection above with the m_bb selection loosened to [70,180] GeV to enable the study of 2D fit
      if (m_jj >= 70.0 && m_jj <= 180.0) {
        // First selection is based on low/high mass
        if (m_yyjj_tilde >= 350.0) {
          if (deltaR_yy < 2.0 && deltaR_jj < 2.0 && deltaR_yyjj < 3.4) {
            if (m_nBJets77 == 2)   {
              Cutbased_cat += 40;
            } else {
              Cutbased_cat += 30;
            }
          }
        } else  {
          if (m_nBJets77 == 2)   {
            Cutbased_cat += 20;
          } else {
            Cutbased_cat += 10;
          }
        }
      }
    }

    m_eventInts["yybb_nonRes" + fullLabel ] = Cutbased_cat;
  }


  // _______________________________________________________________________________________
  // Perform simple MVA selection
  // Returns categories for 4 ggF MVA scenarios: Nominal + top, Nominal - Mbb, Nominal - HT, Nominal
  // The output is a 4 digit number, where each digit gives the category index for that MVA scenario
  // Possible category indices are (1, 2, 3, 4, 9) = (high mass BDT tight, high mass BDT loose, low mass BDT tight, low mass BDT loose, rejected)
  // Add the categorisation for VBF like events
  void HHyybbTool::performXGBoostBDT(const xAOD::PhotonContainer             *selPhotons,
                                     const xAOD::JetContainer                *candidate_jets,
                                     const HHyybbTool::jetPreselectionEnum   &jetPreSelType,
                                     const HHyybbTool::CorrectionEnum  &CorrType,
                                     const xAOD::MissingETContainer          *met,
                                     const xAOD::JetContainer *allJets)
  {
    TString jetPreSelLabel = getJetPreselLabel(jetPreSelType);
    TString CorrLabel = getCorrLabel(CorrType);
    TString XGBoostCatLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "Cat";
    TString XGBoostHighMassScoreLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "highMass_Score";
    TString XGBoostLowMassScoreLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "lowMass_Score";
    TString XGBoostNoHTHighMassScoreLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "noHT_highMass_Score";
    TString XGBoostNoHTLowMassScoreLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "noHT_lowMass_Score";
    TString XGBoostNoMbbHighMassScoreLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "noMbb_highMass_Score";
    TString XGBoostNoMbbLowMassScoreLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "noMbb_lowMass_Score";
    TString XGBoostWithTopHighMassScoreLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "withTop_highMass_Score";
    TString XGBoostWithTopLowMassScoreLabel = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "withTop_lowMass_Score";

    int   XGBoostCat = -99;
    float XGBoostHighMassScore = -99;
    float XGBoostLowMassScore = -99;
    float XGBoostNoHTHighMassScore = -99;
    float XGBoostNoHTLowMassScore = -99;
    float XGBoostNoMbbHighMassScore = -99;
    float XGBoostNoMbbLowMassScore = -99;
    float XGBoostWithTopHighMassScore = -99;
    float XGBoostWithTopLowMassScore = -99;

    TString vbf_Label = "yybb_nonRes_XGBoost_" + jetPreSelLabel + CorrLabel + "isVBF";

    // >= 2 central jets, 0 leptons, less than 6 central jets, >= 2 b-tags at 85% or >=1 77% working point, reject 3 b-tags at 77% working point.
    // same as cut based
    if (m_passpreselection) {
      // use variables already calculated by the CalculateMasses function
      float m_yyjj_tilde = m_eventFloats["yybb_" + CorrLabel + "m_yyjj_tilde"] * HG::invGeV;
      float *vars = makeXGBoostDMatrixggF(selPhotons, candidate_jets, met, jetPreSelType, CorrType); // build DMatrix
      float *vars_scaleMbb = new float[22];
      std::copy(vars, vars + 22, vars_scaleMbb);

      vars_scaleMbb[8] = vars_scaleMbb[8] / (m_eventFloats["yybb_" + CorrLabel + "m_jj"]);
      vars_scaleMbb[12] = vars_scaleMbb[12] / (m_eventFloats["yybb_" + CorrLabel + "m_jj"]);

      // pass preselection -> prepare to fill in category index
      XGBoostCat = 0;

      // High mass channel Selection
      if (m_yyjj_tilde >= 350.0) {

        m_score = getXGBoostWeightggF(m_xgboost_yybb_highMass, vars, 21); // get score

        if (m_score > 0.968) {
          XGBoostCat += 1;
        } else if (m_score > 0.914) {
          XGBoostCat += 2;
        } else {
          XGBoostCat += 9;
        }

        XGBoostHighMassScore = m_score;

        m_score = getXGBoostWeightggF(m_xgboost_yybb_noHT_highMass, vars, 20); // get score

        if (m_score > 0.969) {
          XGBoostCat += 1 * 10;
        } else if (m_score > 0.918) {
          XGBoostCat += 2 * 10;
        } else {
          XGBoostCat += 9 * 10;
        }

        XGBoostNoHTHighMassScore = m_score;

        m_score = getXGBoostWeightggF(m_xgboost_yybb_noMbb_highMass, vars_scaleMbb, 16); // get score

        if (m_score > 0.962) {
          XGBoostCat += 1 * 100;
        } else if (m_score > 0.892) {
          XGBoostCat += 2 * 100;
        } else {
          XGBoostCat += 9 * 100;
        }

        XGBoostNoMbbHighMassScore = m_score;

        m_score = getXGBoostWeightggF(m_xgboost_yybb_withTop_highMass, vars, 22); // get score

        if (m_score > 0.967) {
          XGBoostCat += 1 * 1000;
        } else if (m_score > 0.857) {
          XGBoostCat += 2 * 1000;
        } else {
          XGBoostCat += 9 * 1000;
        }

        XGBoostWithTopHighMassScore = m_score;

        // Low mass channel Selection
      } else {

        m_score = getXGBoostWeightggF(m_xgboost_yybb_lowMass, vars, 21); // get score

        if (m_score > 0.960) {
          XGBoostCat += 3;
        } else if (m_score > 0.851) {
          XGBoostCat += 4;
        } else {
          XGBoostCat += 9;
        }

        XGBoostLowMassScore = m_score;

        m_score = getXGBoostWeightggF(m_xgboost_yybb_noHT_lowMass, vars, 20); // get score

        if (m_score > 0.961) {
          XGBoostCat += 3 * 10;
        } else if (m_score > 0.860) {
          XGBoostCat += 4 * 10;
        } else {
          XGBoostCat += 9 * 10;
        }

        XGBoostNoHTLowMassScore = m_score;

        m_score = getXGBoostWeightggF(m_xgboost_yybb_noMbb_lowMass, vars_scaleMbb, 16); // get score

        if (m_score > 0.891) {
          XGBoostCat += 3 * 100;
        } else if (m_score > 0.737) {
          XGBoostCat += 4 * 100;
        } else {
          XGBoostCat += 9 * 100;
        }

        XGBoostNoMbbLowMassScore = m_score;

        m_score = getXGBoostWeightggF(m_xgboost_yybb_withTop_lowMass, vars, 22); // get score

        if (m_score > 0.966) {
          XGBoostCat += 3 * 1000;
        } else if (m_score > 0.881) {
          XGBoostCat += 4 * 1000;
        } else {
          XGBoostCat += 9 * 1000;
        }

        XGBoostWithTopLowMassScore = m_score;

      }

      // Do VBF selection
      m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "vbf_selected"] = 0;

      if (allJets->size() >= 4) {
        float *vbf_vars = makeXGBoostDMatrixVBF(selPhotons, candidate_jets, allJets, jetPreSelType, CorrType);
        std::vector<double> vbf_weight = getXGBoostWeightVBF(m_xgboost_yybb_vbf, vbf_vars, 10);

        if (XGBoostCat > 0 &&   // passes any ggF category
            m_eventInts["yybb_vbf_jet1"] > -1 && // vbf jets set
            m_eventInts["yybb_vbf_jet2"] > -1 &&
            vbf_weight[0] > 0.00 && // passes VBF class
            vbf_weight[1] < 0.72 && // fails ggF class
            vbf_weight[2] < 0.06 && // fails yy-continuum class
            vbf_weight[3] < 0.80) { // fails ttH class
          m_eventInts["yybb_" + jetPreSelLabel + CorrLabel + "vbf_selected"] = 1;
        }
      }
    }

    m_eventInts[XGBoostCatLabel] = XGBoostCat ;
    m_eventFloats[XGBoostHighMassScoreLabel] = XGBoostHighMassScore ;
    m_eventFloats[XGBoostLowMassScoreLabel] = XGBoostLowMassScore ;
    m_eventFloats[XGBoostNoHTHighMassScoreLabel] = XGBoostNoHTHighMassScore ;
    m_eventFloats[XGBoostNoHTLowMassScoreLabel] = XGBoostNoHTLowMassScore ;
    m_eventFloats[XGBoostNoMbbHighMassScoreLabel] = XGBoostNoMbbHighMassScore ;
    m_eventFloats[XGBoostNoMbbLowMassScoreLabel] = XGBoostNoMbbLowMassScore ;
    m_eventFloats[XGBoostWithTopHighMassScoreLabel] = XGBoostWithTopHighMassScore ;
    m_eventFloats[XGBoostWithTopLowMassScoreLabel] = XGBoostWithTopLowMassScore ;
  }

  float *HHyybbTool::makeXGBoostDMatrixggF(const xAOD::PhotonContainer            *selPhotons,
                                           const xAOD::JetContainer               *candidate_jets,
                                           const xAOD::MissingETContainer         *met,
                                           const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                                           const HHyybbTool::CorrectionEnum       &CorrType)
  {
    TString jetPreSelLabel = getJetPreselLabel(jetPreSelType);
    TString CorrLabel = getCorrLabel(CorrType);
    static float vars[22] = {0};

    // photons
    float myy = (selPhotons->at(0)->p4() + selPhotons->at(1)->p4()).M();
    vars[0] = selPhotons->at(0)->p4().Pt() / myy;
    vars[1] = selPhotons->at(0)->p4().Eta();
    vars[2] = deltaPhi(selPhotons->at(0)->p4().Phi(), selPhotons->at(0)->p4().Phi());
    vars[3] = selPhotons->at(1)->p4().Pt() / myy;
    vars[4] = selPhotons->at(1)->p4().Eta();
    vars[5] = deltaPhi(selPhotons->at(0)->p4().Phi(), selPhotons->at(1)->p4().Phi());

    // met
    vars[6] = (*met)["TST"]->met();
    vars[7] = deltaPhi(selPhotons->at(0)->p4().Phi(), (*met)["TST"]->phi());

    // jets
    std::vector< std::vector<float> > js;

    for (auto j : *candidate_jets) {

      std::vector<float> ij;
      ij.push_back((float) j->p4().Pt());
      ij.push_back((float) j->p4().Eta());
      ij.push_back(deltaPhi(selPhotons->at(0)->p4().Phi(), j->p4().Phi()));
      int pseudoCont_score = j->auxdata<int>("DL1r_bin") - 1;
      // -9 for non-central jets, otherwise use DL1rbin + 1 (just an arbitrary convention in the BDT model)
      pseudoCont_score = (pseudoCont_score == 0 && fabs(j->p4().Eta()) >= 2.5) ? -9 : pseudoCont_score + 1;
      ij.push_back(pseudoCont_score);
      js.push_back(ij);
    }

    // highest btag jets go first, break ties by pT
    std::sort(js.begin(), js.end(), [](std::vector<float> a, std::vector<float> b) {return a[3] > b[3];});

    int i = 8;

    for (auto j : js) {
      vars[i] = j[0];
      i++;
      vars[i] = j[1];
      i++;
      vars[i] = j[2];
      i++;
      vars[i] = j[3];
      i++;

      // fill in only the first two jets
      if (i >= 16) { break; }
    }

    // jj
    vars[16] = m_eventFloatsDetailed["yybb_" + CorrLabel + "pT_jj"];
    vars[17] = m_eventFloatsDetailed["yybb_" + CorrLabel + "eta_jj"];
    vars[18] = deltaPhi(selPhotons->at(0)->p4().Phi(), m_eventFloatsDetailed["yybb_" + CorrLabel + "phi_jj"]);
    vars[19] = m_eventFloats["yybb_" + CorrLabel + "m_jj"];

    // event level jet variables
    vars[20] = m_HTJets;

    // ChiWt variable
    vars[21] = m_min_ChiWt;

    return vars;
  }

  float HHyybbTool::getXGBoostWeightggF(BoosterHandle *boost, float *vars, int len)
  {
    DMatrixHandle dmat;
    XGDMatrixCreateFromMat(vars, 1, len, -1, &dmat);
    bst_ulong out_len;
    const float *f;
    XGBoosterPredict(*boost, dmat, 0, 0, &out_len, &f);
    XGDMatrixFree(dmat);
    return *f;
  }
  /*_______________________________________________________________________________________
    Generates the DMatrix for input into the VBF BDT
    The indexing is as follows:
    [0] jj_deta - delta eta of vbf jets
    [1] mjj - invariant mass of vbf jet system
    [2] m_yyjj - 4 body mass (tilde def'n)
    [3] dR_yy - dR between higgs candidate photons
    [4] dR_jj - dR between higgs candidate b-jets
    [5] j1_tagbin - best b-tagging WP of leading h->bb jet
    [6] j2_tagbin - best b-tagging WP of subleading h->bb jet
    [7] sphericityT - Event shape
    [8] planarFlow - Event shape
    [9] pt_balance - vector sum of jets / scalar sum of jets
    _______________________________________________________________________________________*/
  float *HHyybbTool::makeXGBoostDMatrixVBF(const xAOD::PhotonContainer            *selPhotons,
                                           const xAOD::JetContainer               *candidate_jets,
                                           const xAOD::JetContainer               *allJets,
                                           const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                                           const HHyybbTool::CorrectionEnum &CorrType)
  {
    TString jetPreSelLabel = getJetPreselLabel(jetPreSelType);
    TString CorrLabel = getCorrLabel(CorrType);
    static float input_variables[10] = {0};

    // This bit assumes getVBFjets has been run already
    int idx1 = m_eventInts["yybb_vbf_jet1"];
    int idx2 = m_eventInts["yybb_vbf_jet2"];
    const xAOD::Jet_v1 *vbf_jet1 = allJets->at(idx1) ;
    const xAOD::Jet_v1 *vbf_jet2 = allJets->at(idx2) ;


    input_variables[0] = TMath::Abs(vbf_jet1->p4().Eta() - vbf_jet2->p4().Eta());
    input_variables[1] = (vbf_jet1->p4() + vbf_jet2->p4()).M() * HG::invGeV;
    input_variables[2] = m_eventFloats["yybb_" + CorrLabel + "m_yyjj_tilde"] * HG::invGeV;
    input_variables[3] = selPhotons->at(0)->p4().DeltaR(selPhotons->at(1)->p4());
    input_variables[4] = candidate_jets->at(0)->p4().DeltaR(candidate_jets->at(1)->p4());

    input_variables[5] = (float)candidate_jets->at(0)->auxdata<int>("DL1r_bin") - 1;
    input_variables[6] = (float)candidate_jets->at(1)->auxdata<int>("DL1r_bin") - 1;

    float *eventShapes = eventShapeVariables(selPhotons, candidate_jets);
    input_variables[7] = eventShapes[4];
    input_variables[8] = eventShapes[7];
    input_variables[9] = (selPhotons->at(0)->p4() + selPhotons->at(1)->p4() + candidate_jets->at(0)->p4() + candidate_jets->at(1)->p4()).Pt() /
                         (selPhotons->at(0)->p4().Pt() + selPhotons->at(1)->p4().Pt() + candidate_jets->at(0)->p4().Pt() + candidate_jets->at(1)->p4().Pt());

    return input_variables;
  }

  std::vector<double> HHyybbTool::getXGBoostWeightVBF(BoosterHandle *boost, float *vars, int len)
  {
    DMatrixHandle dmat;
    XGDMatrixCreateFromMat(vars, 1, len, -1, &dmat);
    bst_ulong out_len;
    const float *f;
    XGBoosterPredict(*boost,  // handle to model
                     dmat, // DMatrix (vbf variables
                     0,
                     0,
                     &out_len,
                     &f);
    std::vector<double> outputVector(f, f + out_len);
    XGDMatrixFree(dmat);
    return outputVector;
  }

  /*_______________________________________________________________________________________
    Evaluates Event Shape Variables and sets the third argument array to the following values
    eventShapes[0] = aplanarity;
    eventShapes[1] = aplanority;
    eventShapes[2] = sphericity;
    eventShapes[3] = spherocity;
    eventShapes[4] = sphericityT;
    eventShapes[5] = planarity;
    eventShapes[6] = circularity;
    eventShapes[7] = planarFlow;
    _______________________________________________________________________________________*/
  float *HHyybbTool::eventShapeVariables(const xAOD::PhotonContainer    *selPhotons,
                                         const xAOD::JetContainer       *candidate_jets)
  {
    TLorentzVector photon1 = selPhotons->at(0)->p4() * HG::invGeV;
    TLorentzVector photon2 = selPhotons->at(1)->p4() * HG::invGeV;
    TLorentzVector jet1 = candidate_jets->at(0)->p4() * HG::invGeV;
    TLorentzVector jet2 = candidate_jets->at(1)->p4() * HG::invGeV;

    TLorentzVector  jj = jet1 + jet2;
    TLorentzVector jj_constrained = jet1 + jet2;
    float jjm = jj.M();
    jj_constrained = jj_constrained * (HHyybbTool::m_massHiggs / jjm);
    TLorentzVector yy = photon1 + photon2;
    TLorentzVector yyjj = yy + jj;
    TLorentzVector yyjjC = yy + jj_constrained;

    TMatrixDSym MomentumTensor = TMatrixDSym(3);
    TMatrixDSym MomentumTensorT = TMatrixDSym(3);
    TMatrixDSym MomentumTensorO = TMatrixDSym(3);

    double Sxx = 0.0, Sxy = 0.0, Sxz = 0.0, Syy = 0.0, Syz = 0.0, Szz = 0.0, normal = 0.0;
    double Oxx = 0.0, Oxy = 0.0, Oxz = 0.0, Oyy = 0.0, Oyz = 0.0, Ozz = 0.0, Onormal = 0.0;

    Sxx += jet1.Px() * jet1.Px();
    Sxx += jet2.Px() * jet2.Px();
    Sxx += photon1.Px() * photon1.Px();
    Sxx += photon2.Px() * photon2.Px();
    Sxy += jet1.Px() * jet1.Py();
    Sxy += jet2.Px() * jet2.Py();
    Sxy += photon1.Px() * photon1.Py();
    Sxy += photon2.Px() * photon2.Py();
    Sxz += jet1.Px() * jet1.Pz();
    Sxz += jet2.Px() * jet2.Pz();
    Sxz += photon1.Px() * photon1.Pz();
    Sxz += photon2.Px() * photon2.Pz();
    Syy += jet1.Py() * jet1.Py();
    Syy += jet2.Py() * jet2.Py();
    Syy += photon1.Py() * photon1.Py();
    Syy += photon2.Py() * photon2.Py();
    Syz += jet1.Py() * jet1.Pz();
    Syz += jet2.Py() * jet2.Pz();
    Syz += photon1.Py() * photon1.Pz();
    Syz += photon2.Py() * photon2.Pz();
    Szz += jet1.Pz() * jet1.Pz();
    Szz += jet2.Pz() * jet2.Pz();
    Szz += photon1.Pz() * photon1.Pz();
    Szz += photon2.Pz() * photon2.Pz();

    normal += jet1.P() * jet1.P();
    normal += jet2.P() * jet2.P();
    normal += photon1.P() * photon1.P();
    normal += photon2.P() * photon2.P();

    Oxx += jet1.Px() * jet1.Px() / jet1.P();
    Oxx += jet2.Px() * jet2.Px() / jet2.P();
    Oxx += photon1.Px() * photon1.Px() / photon1.P();
    Oxx += photon2.Px() * photon2.Px() / photon2.P();
    Oxy += jet1.Px() * jet1.Py() / jet1.P();
    Oxy += jet2.Px() * jet2.Py() / jet2.P();
    Oxy += photon1.Px() * photon1.Py() / photon1.P();
    Oxy += photon2.Px() * photon2.Py() / photon2.P();
    Oxz += jet1.Px() * jet1.Pz() / jet1.P();
    Oxz += jet2.Px() * jet2.Pz() / jet2.P();
    Oxz += photon1.Px() * photon1.Pz() / photon1.P();
    Oxz += photon2.Px() * photon2.Pz() / photon2.P();
    Oyy += jet1.Py() * jet1.Py() / jet1.P();
    Oyy += jet2.Py() * jet2.Py() / jet2.P();
    Oyy += photon1.Py() * photon1.Py() / photon1.P();
    Oyy += photon2.Py() * photon2.Py() / photon2.P();
    Oyz += jet1.Py() * jet1.Pz() / jet1.P();
    Oyz += jet2.Py() * jet2.Pz() / jet2.P();
    Oyz += photon1.Py() * photon1.Pz() / photon1.P();
    Oyz += photon2.Py() * photon2.Pz() / photon2.P();
    Ozz += jet1.Pz() * jet1.Pz() / jet1.P();
    Ozz += jet2.Pz() * jet2.Pz() / jet2.P();
    Ozz += photon1.Pz() * photon1.Pz() / photon1.P();
    Ozz += photon2.Pz() * photon2.Pz() / photon2.P();

    Onormal += jet1.P();
    Onormal += jet2.P();
    Onormal += photon1.P();
    Onormal += photon2.P();


    MomentumTensor[0][0] = Sxx / normal;
    MomentumTensor[0][1] = Sxy / normal;
    MomentumTensor[0][2] = Sxz / normal;
    MomentumTensor[1][0] = MomentumTensor[0][1];
    MomentumTensor[1][1] = Syy / normal;
    MomentumTensor[1][2] = Syz / normal;
    MomentumTensor[2][0] = MomentumTensor[0][2];
    MomentumTensor[2][1] = MomentumTensor[1][2];
    MomentumTensor[2][2] = Szz / normal;

    MomentumTensorT[0][0] = MomentumTensor[0][0];
    MomentumTensorT[0][1] = MomentumTensor[0][1];
    MomentumTensorT[1][1] = MomentumTensor[1][1];
    MomentumTensorT[1][0] = MomentumTensor[1][0];


    MomentumTensorO[0][0] = Oxx / Onormal;
    MomentumTensorO[0][1] = Oxy / Onormal;
    MomentumTensorO[0][2] = Oxz / Onormal;
    MomentumTensorO[1][0] = MomentumTensorO[0][1];
    MomentumTensorO[1][1] = Oyy / Onormal;
    MomentumTensorO[1][2] = Oyz / Onormal;
    MomentumTensorO[2][0] = MomentumTensorO[0][2];
    MomentumTensorO[2][1] = MomentumTensorO[1][2];
    MomentumTensorO[2][2] = Ozz / Onormal;

    TMatrixDSymEigen EigenValues = TMatrixDSymEigen(MomentumTensor);
    TMatrixDSymEigen EigenValuesT = TMatrixDSymEigen(MomentumTensorT);
    TMatrixDSymEigen EigenValuesO = TMatrixDSymEigen(MomentumTensorO);

    TVectorD eigenVec = EigenValues.GetEigenValues();
    TVectorD eigenVecT = EigenValuesT.GetEigenValues();
    TVectorD eigenVecO = EigenValuesO.GetEigenValues();

    float aplanarity = 1.5 * eigenVec[2];
    float aplanority = 1.5 * eigenVecO[2];
    float sphericity = 1.5 * (eigenVec[1] + eigenVec[2]);
    float spherocity = 1.5 * (eigenVecO[1] + eigenVecO[2]);
    float sphericityT = 2.0 * eigenVecT[1] / (eigenVecT[0] + eigenVecT[1]);
    float planarity = eigenVec[1] - eigenVec[2];
    float circularity = -99;

    if ((eigenVec[0] + eigenVec [1]) != 0) {
      circularity = 2.0 * eigenVec[1] / (eigenVec[0] + eigenVec [1]);
    }

    float planarFlow = -99;

    if (((eigenVec[0] + eigenVec [1]) * (eigenVec[0] + eigenVec [1])) != 0) {
      planarFlow = 4.0 * eigenVec[0] * eigenVec[1] / ((eigenVec[0] + eigenVec [1]) * (eigenVec[0] + eigenVec [1]));
    }

    static float eventShapes[8] = {0};
    eventShapes[0] = aplanarity;
    eventShapes[1] = aplanority;
    eventShapes[2] = sphericity;
    eventShapes[3] = spherocity;
    eventShapes[4] = sphericityT;
    eventShapes[5] = planarity;
    eventShapes[6] = circularity;
    eventShapes[7] = planarFlow;
    return eventShapes;
  }

  // _______________________________________________________________________________________
  // Selects highest dijet mass pairing as VBF jets, not including candidate jets
  void HHyybbTool::getVBFjets(const xAOD::JetContainer               *candidateJets,
                              const xAOD::JetContainer               *allJets,
                              const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                              const HHyybbTool::CorrectionEnum &CorrType)
  {
    TString jetPreSelLabel = getJetPreselLabel(jetPreSelType);
    TString CorrLabel = getCorrLabel(CorrType);

    float mjj = -999;
    int idx1 = -1, idx2 = -1;

    // Make sure there's enough jets, if not return the default values
    if ((allJets->size() - candidateJets->size()) < 2 || candidateJets->size() < 2) {
      m_eventInts["yybb_vbf_jet1"] = idx1;
      m_eventInts["yybb_vbf_jet2"] = idx2;
      return;
    }

    for (unsigned int i = 0; i < allJets->size(); i++) {
      const xAOD::Jet_v1 *jet1 = allJets->at(i);

      // Don't consider Higgs jets
      if (jet1->p4().DeltaR(candidateJets->at(0)->p4()) < 0.01 ||
          jet1->p4().DeltaR(candidateJets->at(1)->p4()) < 0.01)
      {continue;}

      // Check all pairings
      for (unsigned int j = 0; j < allJets->size(); j++) {
        const xAOD::Jet_v1 *jet2 = allJets->at(j);

        // Don't consider Higgs or pairings with self
        if (i == j ||
            jet2->p4().DeltaR(candidateJets->at(0)->p4()) < 0.01 ||
            jet2->p4().DeltaR(candidateJets->at(1)->p4()) < 0.01)
        {continue;}

        // Set indicies if better than current best
        TLorentzVector iPair = jet1->p4() + jet2->p4();

        if (iPair.M() > mjj) {
          mjj = iPair.M();
          idx1 = i;
          idx2 = j;
        };

      }
    }

    m_eventInts["yybb_vbf_jet1"] = idx1;
    m_eventInts["yybb_vbf_jet2"] = idx2;
  }

  // _______________________________________________________________________________________
  // Calculate difference in angle and normalize to [-pi, pi]
  float HHyybbTool::deltaPhi(float angle1, float angle2)
  {
    double dPhi = angle2 - angle1;

    if (dPhi > TMath::Pi()) { dPhi -= 2 * TMath::Pi();}
    else if (dPhi < -TMath::Pi()) { dPhi += 2 * TMath::Pi();}

    return dPhi;
  }


  // _______________________________________________________________________________________
  // Write out jet collections to MxAOD
  EL::StatusCode HHyybbTool::writeContainers()
  {
    // Set appropriate return type for ANA_CHECK
    ANA_CHECK_SET_TYPE(EL::StatusCode);

    // only write out the jets if we are in yybb detailed mode
    if (m_detailedHHyybb) {

      // Read jet collections from the event store and write them to the output file
      for (auto jetPreselection : m_jetPreselections) {
        for (auto Correction : m_Corrections) {
          const std::string collectionName = getJetCollectionName(jetPreselection, Correction) + HG::VarHandler::getInstance()->getSysName();

          // Retrieve the jet collection and write it to the output file
          xAOD::JetContainer *selectedJets = nullptr;
          ANA_CHECK(eventHandler()->evtStore()->retrieve(selectedJets, collectionName));
          ANA_CHECK(eventHandler()->outputEvt()->record(selectedJets, collectionName));

          // Now do the same for the aux collection (different type depending on whether this is a deep or shallow copy)
          if (Correction == HHyybbTool::noCor) {
            xAOD::AuxContainerBase *selectedJetsAux = nullptr;
            ANA_CHECK(eventHandler()->evtStore()->retrieve(selectedJetsAux, collectionName + "Aux."));
            ANA_CHECK(eventHandler()->outputEvt()->record(selectedJetsAux, collectionName + "Aux."));
          } else {
            xAOD::ShallowAuxContainer *selectedJetsAux = nullptr;
            ANA_CHECK(eventHandler()->evtStore()->retrieve(selectedJetsAux, collectionName  + "Aux."));
            ANA_CHECK(eventHandler()->outputEvt()->record(selectedJetsAux, collectionName + "Aux."));
          }
        }
      }

    }

    return EL::StatusCode::SUCCESS;
  }


  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HHyybbTool::recordPerEventQuantities()
  {
    if (!m_specificHHyybb) {
      // Floats
      for (auto element : m_eventFloats) {
        eventHandler()->storeVar<float>(element.first.Data(), element.second);
      }

      // Ints
      for (auto element : m_eventInts) {
        eventHandler()->storeVar<int>(element.first.Data(), element.second);
      }

      // In the case of yybbDetailed, record additional quantities
      if (m_detailedHHyybb) {
        // Floats
        for (auto element : m_eventFloatsDetailed) {
          eventHandler()->storeVar<float>(element.first.Data(), element.second);
        }
      }
    } else {
      // Floats
      for (auto element : m_eventFloats) {
        for (TString variable : m_specificVariables) {
          if (element.first.Data() == variable) { eventHandler()->storeVar<float>(element.first.Data(), element.second); }
        }
      }

      // Ints
      for (auto element : m_eventInts) {
        for (TString variable : m_specificVariables) {
          if (element.first.Data() == variable) { eventHandler()->storeVar<int>(element.first.Data(), element.second); }
        }
      }
    }
  }


  // ___________________________________________________________________________________________
  // Multiply all the JVT (in)efficiencies for all jets passing the selection before the JVT cut
  // Use uncorrected, no-JVT jets
  double HHyybbTool::weightJVT()
  {
    // Retrieve jets passing the |eta| cut before JVT
    const xAOD::JetContainer *jets_no_JVT_passing_cuts = nullptr;

    if (eventHandler()->evtStore()->retrieve(jets_no_JVT_passing_cuts, "yybb_jets_central_no_JVT").isFailure())
    { throw std::runtime_error("HHyybbTool: Couldn't retrieve no JVT central jets, throwing exception"); }

    // Return multiplicative combination of JVT efficiencies
    if (jets_no_JVT_passing_cuts->size() > 0) {
      return HG::JetHandler::multiplyJvtWeights(jets_no_JVT_passing_cuts);
    }

    return 1.0;
  }


  // _______________________________________________________________________________________
  // Calculate the b-tagging scale-factor weight from the b-tagging SFs for *all* jets where we look at the DL1r result
  double HHyybbTool::calculateBTaggingWeight(const xAOD::JetContainer &jets)
  {
    // Our default is to use the scale-factor from continuous tagging
    TString SFName("SF_DL1r_Continuous");

    // Retrieve appropriate b-tagging scale-factors
    double jetSF  = 1.0;

    for (auto jet : jets) {
      jetSF *= jet->isAvailable<float>(SFName.Data()) ? jet->auxdata<float>(SFName.Data()) : 0.0;
    }

    return jetSF;
  }


  // _______________________________________________________________________________________
  // Get hh truth weight to reweight signal hh MC samples to full NLO prediction
  double HHyybbTool::weightNLO()
  {
    if (HG::isData()) { return 1.0; }

    // Get truth Higgses and require that there are exactly two
    xAOD::TruthParticleContainer truthHiggses = truthHandler()->getHiggsBosons();

    if (truthHiggses.size() != 2) {
      if (truthHiggses.size() > 2) { ATH_MSG_ERROR("More than two final state Higgses in the event record."); }

      return 1.0;
    }

    ATH_MSG_DEBUG("Found " << truthHiggses.size() << " Higgses which may need an NLO weight.");

    // Hard-code the DSID for SM hh
    if (m_eventHandler->mcChannelNumber() != 342620) { return 1.0; }

    // Pass mHH in MeV
    float mHH = (truthHiggses.at(0)->p4() + truthHiggses.at(1)->p4()).M();
    double weight = m_hhWeightTool->getWeight(mHH);
    ATH_MSG_DEBUG("Found a di-Higgs system with mass " << mHH * HG::invGeV << " GeV => NLO weight: " <<  weight);
    return weight;
  }


  // _______________________________________________________________________________________
  // Get the label for a given jet pre-selection
  TString HHyybbTool::getJetPreselLabel(const HHyybbTool::jetPreselectionEnum &jetPreSelCat)
  {

    if (jetPreSelCat == HHyybbTool::WP77) { return TString("btag77_"); }
    else if (jetPreSelCat == HHyybbTool::WP85) { return TString("btag77_85_"); }

    return TString("");
  }


  // _______________________________________________________________________________________
  // Get the label for a given lepton correction
  TString HHyybbTool::getCorrLabel(const HHyybbTool::CorrectionEnum &CorrType)
  {
    if (CorrType == HHyybbTool::BJetCal) { return TString("BCal_"); }

    if (CorrType == HHyybbTool::BJetReg) { return TString("BReg_"); }

    if (CorrType == HHyybbTool::KF) { return TString("KF_"); }

    return TString("");
  }


  // _______________________________________________________________________________________
  // Get the name for this jet collection
  std::string HHyybbTool::getJetCollectionName(const jetPreselectionEnum &jetPreSelType, const CorrectionEnum &CorrType)
  {
    TString collectionName = "yybbAntiKt4EMPFlowJets_" + getJetPreselLabel(jetPreSelType) + (CorrType == HHyybbTool::noCor ? "" : "_Corr_" + getCorrLabel(CorrType));
    return std::string(TString(collectionName.Strip(TString::kTrailing, '_')).Data());
  }



  void HHyybbTool::makeMVAResonantScore(const xAOD::PhotonContainer            *selPhotons,
                                        const xAOD::JetContainer               *candidate_jets,
                                        const xAOD::MissingETContainer         *met,
                                        const HHyybbTool::jetPreselectionEnum  &jetPreSelType,
                                        const HHyybbTool::CorrectionEnum       &CorrType)
  {
    TString jetPreSelLabel = getJetPreselLabel(jetPreSelType);
    TString CorrLabel = getCorrLabel(CorrType);

    const xAOD::EventInfo *evt = 0;
    int eventNum = 0;

    if (m_eventHandler->outputEvt()->retrieve(evt, "EventInfo")) {
      eventNum = evt->eventNumber();
    }

    // Construct diphoton, jet and dijet 4-vectors
    TLorentzVector y1 = (selPhotons->size() >= 2) ? selPhotons->at(0)->p4() : TLorentzVector(0, 0, 0, 0);
    TLorentzVector y2 = (selPhotons->size() >= 2) ? selPhotons->at(1)->p4() : TLorentzVector(0, 0, 0, 0);
    TLorentzVector yy = y1 + y2;

    TLorentzVector j1 = (candidate_jets->size() >= 2) ? candidate_jets->at(0)->p4() : TLorentzVector(0, 0, 0, 0);
    TLorentzVector j2 = (candidate_jets->size() >= 2) ? candidate_jets->at(1)->p4() : TLorentzVector(0, 0, 0, 0);
    TLorentzVector jj = j1 + j2;

    // ... and the 4-body vector with and without applying the mH constraint
    TLorentzVector jj_cnstrnd = jj * (HHyybbTool::m_massHiggs * HG::GeV / jj.M());
    TLorentzVector yyjj = yy + jj;
    TLorentzVector yyjj_cnstrnd = yy + jj_cnstrnd;

    // photons
    pT_yy_ini = (selPhotons->size() >= 2) ? yy.Pt() * HG::invGeV : -99;
    deltaR_yy_ini = (selPhotons->size() >= 2) ? y1.DeltaR(y2) : -99;
    deltaPhi_yy_ini = (selPhotons->size() >= 2) ? fabs(y1.DeltaPhi(y2)) : -99;
    Rap_yy_ini = (selPhotons->size() >= 2) ? yy.Rapidity() : -99;

    // jets
    m_bb_ini = (candidate_jets->size() >= 2) ? jj.M() * HG::invGeV : -99;
    pT_bb_ini = (candidate_jets->size() >= 2) ? jj.Pt() * HG::invGeV : -99;
    deltaR_bb_ini = (candidate_jets->size() >= 2) ? j1.DeltaR(j2) : -99;
    deltaPhi_bb_ini = (candidate_jets->size() >= 2) ? fabs(j1.DeltaPhi(j2)) : -99;
    Rap_bb_ini = (candidate_jets->size() >= 2) ? jj.Rapidity() : -99;

    N_j N_j_class;
    N_j_ini = N_j_class.calculateValue(0);

    N_j_btag N_j_btag_class;
    N_j_btag_ini = N_j_btag_class.calculateValue(0);

    // diHiggs
    m_yyjj_tilde_ini = (candidate_jets->size() >= 2 && selPhotons->size() >= 2) ? (yyjj.M() - yy.M() - jj.M() + 2 * HHyybbTool::m_massHiggs * HG::GeV) * HG::invGeV : -99;
    deltaR_yybb_ini = (candidate_jets->size() >= 2 && selPhotons->size() >= 2) ? yy.DeltaR(jj) : -99;
    deltaPhi_bbyy_ini = (candidate_jets->size() >= 2 && selPhotons->size() >= 2) ? fabs(jj.DeltaPhi(yy)) : -99;
    deltaRap_bbyy_ini = (candidate_jets->size() >= 2 && selPhotons->size() >= 2) ? fabs(Rap_bb_ini - Rap_yy_ini) : -99;
    pT_bbyy_ini = (candidate_jets->size() >= 2 && selPhotons->size() >= 2) ? yyjj.Pt() * 0.001 : -99;

    // MET
    MET_ini = (*met)["TST"]->met() * HG::invGeV;

    //HT
    HTall_30 HTall_30_class;

    HTall_30_ini = HTall_30_class.calculateValue(0) * HG::invGeV;

    std::vector<float> variablesValues_yy;
    variablesValues_yy.push_back(m_bb_ini);
    variablesValues_yy.push_back(m_yyjj_tilde_ini);
    variablesValues_yy.push_back(pT_yy_ini);
    variablesValues_yy.push_back(pT_bb_ini);
    variablesValues_yy.push_back(deltaR_yy_ini);
    variablesValues_yy.push_back(deltaR_bb_ini);
    variablesValues_yy.push_back(deltaR_yybb_ini);
    variablesValues_yy.push_back(deltaPhi_yy_ini);
    variablesValues_yy.push_back(deltaPhi_bb_ini);
    variablesValues_yy.push_back(deltaPhi_bbyy_ini);
    variablesValues_yy.push_back(Rap_yy_ini);
    variablesValues_yy.push_back(Rap_bb_ini);
    variablesValues_yy.push_back(deltaRap_bbyy_ini);
    variablesValues_yy.push_back(N_j_ini);
    variablesValues_yy.push_back(N_j_btag_ini);
    variablesValues_yy.push_back(pT_bbyy_ini);
    variablesValues_yy.push_back(HTall_30_ini);

    std::vector<float> variablesValues_ttH;
    variablesValues_ttH.push_back(m_bb_ini);
    variablesValues_ttH.push_back(m_yyjj_tilde_ini);
    variablesValues_ttH.push_back(pT_yy_ini);
    variablesValues_ttH.push_back(pT_bb_ini);
    variablesValues_ttH.push_back(deltaR_yy_ini);
    variablesValues_ttH.push_back(deltaR_bb_ini);
    variablesValues_ttH.push_back(deltaR_yybb_ini);
    variablesValues_ttH.push_back(deltaPhi_yy_ini);
    variablesValues_ttH.push_back(deltaPhi_bb_ini);
    variablesValues_ttH.push_back(deltaPhi_bbyy_ini);
    variablesValues_ttH.push_back(Rap_yy_ini);
    variablesValues_ttH.push_back(Rap_bb_ini);
    variablesValues_ttH.push_back(deltaRap_bbyy_ini);
    variablesValues_ttH.push_back(N_j_ini);
    variablesValues_ttH.push_back(N_j_btag_ini);
    variablesValues_ttH.push_back(pT_bbyy_ini);
    variablesValues_ttH.push_back(HTall_30_ini);
    variablesValues_ttH.push_back(MET_ini);

    Double_t score_yy = -2.;
    Double_t score_ttH = -2.;

    if (eventNum % 2 == 0) {
      score_yy = m_readerMVA_yy->EvaluateMVA(variablesValues_yy, "smvaother1_yy");
      score_ttH = m_readerMVA_ttH->EvaluateMVA(variablesValues_ttH, "smvaother1_ttH");
    } else if (eventNum % 2 == 1) {
      score_yy = m_readerMVA_yy->EvaluateMVA(variablesValues_yy, "smvaother2_yy");
      score_ttH = m_readerMVA_ttH->EvaluateMVA(variablesValues_ttH, "smvaother2_ttH");
    }

    m_eventFloats["yybb_Res_BDT_" + CorrLabel + "yy_Score"] = score_yy;
    m_eventFloats["yybb_Res_BDT_" + CorrLabel + "ttH_Score"] = score_ttH;
    variablesValues_yy.clear();
    variablesValues_ttH.clear();

  }

}
