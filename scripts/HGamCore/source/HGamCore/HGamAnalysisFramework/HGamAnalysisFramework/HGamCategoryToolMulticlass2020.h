#ifndef HGAMCATEGORYTOOLMULTICLASS2020_H
#define HGAMCATEGORYTOOLMULTICLASS2020_H

#include "AsgTools/AsgMessaging.h"
#include "MVAUtils/BDT.h"

#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODMissingET/MissingETContainer.h"

#include "HGamAnalysisFramework/EventHandler.h"

#include <vector>
#include <string>

namespace Coupling2020 {

  struct CategorizationInputs {
    const xAOD::ElectronContainer *electrons;
    const xAOD::MuonContainer *muons;
    const xAOD::JetContainer *jets;
    const xAOD::JetContainer *jetsJVT;
    const xAOD::MissingETContainer *met;
    const HG::EventHandler *eventHandler;
  };

  /**
   * Base class for the multiclass + d-optimality. It is supposed to be based on lgbm.
   **/
  class CategorizationMultiClass : public asg::AsgMessaging {
  public:
    /**
    * name: an unique string
    * tree_multiclass: the TTree containing the BDT weights for the multiclass.
    * weight_multiclass: the weights for the d-optimality
    **/
    CategorizationMultiClass(const std::string &name,
                             TTree *tree_multiclass,
                             const std::vector<float> &weight_multiclass);

    unsigned int get_nclasses() const { return m_nclasses; }
    int get_category(const CategorizationInputs &inputs) const;
    virtual std::string get_category_name(unsigned int icat) const;

  private:
    virtual std::vector<float> get_input(const CategorizationInputs &inputs) const = 0;

    unsigned int m_nclasses;
    MVAUtils::BDT m_bdt_multiclass;
    std::vector<float> m_weight_multiclass;
  };

} // namespace Coupling2020

#endif