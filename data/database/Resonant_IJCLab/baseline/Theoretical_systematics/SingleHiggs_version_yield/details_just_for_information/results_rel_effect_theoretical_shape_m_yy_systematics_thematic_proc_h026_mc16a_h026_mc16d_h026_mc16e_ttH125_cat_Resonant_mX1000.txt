\newcolumntype{C}{>{\centering\arraybackslash}p{2cm}}
\begin{table}[h!]
\begin{center}
\scriptsize
\begin{tabular}{|l|l|C|C|}
\hline
\multicolumn{2}{|l|}{Source of systematic uncertainty}    &\multicolumn{2}{c|}{\% effect wrt nominal}\\
\multicolumn{2}{|l|}{                                }   &\multicolumn{2}{c|}{ttH125}\\
\multicolumn{2}{|l|}{                                } &$\mu$   &$\sigma$\\
\hline
\hline
\multirow{3}{*}{Theory}\rule[-1.ex]{0pt}{3.5ex}&\verb|QCD|	&$<0.1$ &-	\\
                                 \rule[-1.ex]{0pt}{3.5ex}&\verb|PDF_alpha_s|	&$<0.1$ &-	\\
\hline
\end{tabular}
\end{center}
\vspace{-0.5cm}
\caption{Summary of dominant theoretical systematic uncertainties affecting expected shape after the selection of the category \textrm{Resonant\_mX1000}. Sources marked ~-~ are not significant.}
\label{table_theoretical_systematics_thematic_shape_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_Resonant_mX1000}
\end{table}
