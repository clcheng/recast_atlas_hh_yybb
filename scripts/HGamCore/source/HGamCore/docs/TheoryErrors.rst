Theoretical errors
==================

Theoretical uncertainties about QCD scales, PDF and α\ :sub:`s` can be computed just changing the MC weights used during the analysis.

In general:
   * use generator weights provided by Powheg
   * PDF: variation of eigenvectors (~30)
   * QCD scales:
      - for most production modes, compute all (μ\ :sub:`F`,μ\ :sub:`R`)-variations with μ\ :sub:`F/R` = 0 or 1; take envelope of variations from final histograms
      - dedicated scheme for ggF (and maybe more production modes in future): 9 nuisance parameters split in more "physical" sources: jet-multiplicity bins, p\ :sub:`T`, ...

It is suggested to create a new algorithm, creating a new class as explained in https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HggDerivationAnalysisFramework#Make_HgammaAnalysis_skeleton

Get variations from :code:`eventHandler()->higgsWeights()` object. This reads the weight vector from the MxAOD. 

.. code:: c++

   xAOD::HiggsWeights hw = eventHandler()->higgsWeights();

The idea is to divide the nominal initial event weight (:code:`w_ini`) by the nominal MC weight (:code:`hw.nominal`) and to multiply by the systematic one to get the total event weight

.. code:: c++

   double w_ini var::weightInitial() * m_lumiWeight;
   // where m_lumiWeight should be defined by hand


Obtain the varied event weights for the different systematics sources listed below.

QCD scales for ggF samples
--------------------------

A dedicated detailed nuisance parameter list is applied. They correspond to:

.. code:: c++ 

    const std::vector<TString> NP_QCDggH = {"mu", "res", "mig01", "mig12", "vbf2j", "vbf3j", "pTH60", "pTH120", "qm_t"};

    for (int i = 0; i < NP_QCDggH.size(); ++i) {
      w_QCDggFvar[i] = w_ini / hw.nominal * hw.ggF_qcd_2017[i];
      // ...
    }

QCD scales for VBF, VH and ttH samples
--------------------------------------

These are variations corresponding to the case μ\ :sub:`F/R` = 0 or 1

.. code:: c++ 

   for(int i = 0; i < hw.qcd.size(); ++i) {
     w_QCDvar[i] = w_ini / hw.nominal * hw.qcd[i];
     // ...
    }

PDF variations
---------------

Here use the 30 eigenvectors:

.. code:: c++ 

    for(int i = 0; i < 30; ++i) {
      w_pdfVar[i] =  w_ini / hw.nominal * hw.pdf4lhc_unc[i]);
      // ...
    }

α\ :sub:`s` variations
-----------------------

Here just an up and down variation is considered

.. code:: c++ 

   double w_varAlphaS_up = w_ini / hw.nominal * hw.alphaS_up;
   double w_varAlphaS_down = w_ini / hw.nominal * hw.alphaS_down;
