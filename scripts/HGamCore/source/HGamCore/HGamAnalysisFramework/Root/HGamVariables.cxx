// Local include(s):
#include "HGamAnalysisFramework/HGamVariables.h"

namespace HG {

  //____________________________________________________________________________
  double tauJet(const xAOD::IParticle *g1, const xAOD::IParticle *g2, const xAOD::IParticle *jet)
  {
    return sqrt(pow(jet->pt(), 2) + pow(jet->m(), 2))
           / (2.0 * cosh(jet->rapidity() - (g1->p4() + g2->p4()).Rapidity()));
  }

  // Functions for calculating the leptonic W four-vector
  double W_m_trans(TLorentzVector vec, double px, double py)
  {
    double pt = sqrt(px * px + py * py);
    return sqrt(2 * (pt * vec.Pt() - px * vec.Px() - py * vec.Py()));
  }

  double py_nu(double px, TLorentzVector lep_vec, bool plus)
  {
    double M_W = 80.385;
    double A, B;

    A = (M_W * M_W * lep_vec.Py() + 2 * lep_vec.Px() * lep_vec.Py() * px) / (2 * pow(lep_vec.Px(), 2));

    B = (M_W * lep_vec.Pt() / (2 * pow(lep_vec.Px(), 2))) * sqrt(M_W * M_W + 4 * lep_vec.Px() * px);

    if (plus == 1)
    { return A + B; }
    else
    { return A - B; }
  }

  std::vector<double> pxpy_corr(TLorentzVector lep_vec, double met_x, double met_y)
  {

    std::vector<double> output;
    output.push_back(met_x);
    output.push_back(met_y);

    double M_W = 80.385;

    if (W_m_trans(lep_vec, met_x, met_y) < M_W) {
      return output;
    }

    double iter = std::max(1.0, fabs((M_W * M_W) / (4 * lep_vec.Px())) / 100);
    double iter_pos;
    const int n_iters = 1000;
    double del_pos[n_iters], del_neg[n_iters];
    double tmp_px[n_iters], tmp_py_pos[n_iters], tmp_py_neg[n_iters];
    int min_pos_index, min_neg_index;

    if (lep_vec.Px() < 0) { iter_pos = -1 * iter; }
    else { iter_pos = iter; }

    tmp_px[0] = (-(M_W * M_W) / (4 * lep_vec.Px())) + iter_pos / 4.0;
    tmp_py_pos[0] = py_nu(tmp_px[0], lep_vec, 1);
    tmp_py_neg[0] = py_nu(tmp_px[0], lep_vec, 0);

    //setting up initial deltas
    del_pos[0] = sqrt(pow(tmp_px[0] - met_x, 2) + pow(tmp_py_pos[0] - met_y, 2));
    del_neg[0] = sqrt(pow(tmp_px[0] - met_x, 2) + pow(tmp_py_neg[0] - met_y, 2));

    //minimum delta for this iteration and last
    double del_min_current = 0;
    double del_min_last = -100;

    //Do we increase or decrease px to move away from imaginary py solutions
    //loop until delta meet requirement
    while (del_min_current - del_min_last > 1) {
      del_min_last = del_min_current;

      for (int ii = 1; ii < n_iters; ii++) {
        tmp_px[ii] = tmp_px[ii - 1] + iter_pos;
        tmp_px[ii] = tmp_px[ii - 1] + iter_pos;
        //Check positive
        tmp_py_pos[ii] = py_nu(tmp_px[ii], lep_vec, 1);
        del_pos[ii] = sqrt(pow(tmp_px[ii] - met_x, 2) + pow(tmp_py_pos[ii] - met_y, 2));

        //now check neg
        tmp_py_neg[ii] = py_nu(tmp_px[ii], lep_vec, 0);
        del_neg[ii] = sqrt(pow(tmp_px[ii] - met_x, 2) + pow(tmp_py_neg[ii] - met_y, 2));
      }

      min_pos_index = std::min_element(del_pos, del_pos + n_iters) - del_pos;
      min_neg_index = std::min_element(del_neg, del_neg + n_iters) - del_neg;

      //the index of minnum delta
      int min_index = 0;

      //in addition to output assignment, assigin the minimum of delta and min_index.
      if (fabs(del_neg[min_neg_index]) > fabs(del_pos[min_pos_index])) {
        output.at(0) = tmp_px[min_pos_index];
        output.at(1) = tmp_py_pos[min_pos_index];
        del_min_current = del_pos[min_pos_index];
        min_index = min_pos_index;
      } else {
        output.at(0) = tmp_px[min_neg_index];
        output.at(1) = tmp_py_neg[min_neg_index];
        del_min_current = del_neg[min_neg_index];
        min_index = min_neg_index;
      }

      //judge if the improvment is significant and decide if next iteration is needed
      iter_pos = (tmp_px[min_index + 1] - tmp_px[min_index - 1]) / 1000.;
      tmp_px[0] = tmp_px[min_index - 1];
    }

    return output;
  }

  TLorentzVector leptonic_W(TLorentzVector lep_vec, double met_x, double met_y, double met)
  {

    double Mass_W = 80.385;
    TLorentzVector nu_trans;
    TLorentzVector out_vec;
    double pz_nu;

    std::vector<double> output = pxpy_corr(lep_vec, met_x, met_y);

    bool C_fail = true;

    if (output.at(0) == met_x && output.at(1) == met_y) {
      C_fail = false;
    }

    met_x = output.at(0);
    met_y = output.at(1);

    double pos_sol, neg_sol;
    nu_trans.SetPxPyPzE(met_x, met_y, 0., met);
    double mu = (pow(Mass_W, 2) / 2) + (lep_vec.Pt() * nu_trans.Pt() * cos(lep_vec.DeltaPhi(nu_trans)));

    // Type I events where the pz is simply reco'd with no need for additional corrections
    if (C_fail == 0) {
      if (pow(mu, 2) < pow(lep_vec.Pt() * nu_trans.Pt(), 2))
      { out_vec.SetPxPyPzE(0, 0, 0, 0); }
      else {
        double term1 = (mu * lep_vec.Pz()) / (lep_vec.Pt() * lep_vec.Pt());
        double term2 = pow((mu * lep_vec.Pz()) / (lep_vec.Pt() * lep_vec.Pt()), 2);
        double term3 = (pow(lep_vec.E() * nu_trans.Pt(), 2) - mu * mu) / (lep_vec.Pt() * lep_vec.Pt());

        if (term2 > term3) {
          pos_sol = term1 + sqrt(term2 - term3);
          neg_sol = term1 - sqrt(term2 - term3);
          pz_nu = (fabs(pos_sol) > fabs(neg_sol) ? neg_sol : pos_sol);
        } else {
          pz_nu = term1;
        }

        out_vec.SetPxPyPzE(lep_vec.Px() + met_x,
                           lep_vec.Py() + met_y,
                           lep_vec.Pz() + pz_nu,
                           lep_vec.E() + sqrt(met_x * met_x + met_y * met_y + pz_nu * pz_nu));
      }
    }
    // Type II events where the px and py of neutrino are found by scanning for a min
    // under the condition that the lepton neutrino system has mT = m_W
    else if (C_fail == 1) {
      pz_nu = (mu * lep_vec.Pz()) / (lep_vec.Pt() * lep_vec.Pt());
      out_vec.SetPxPyPzE(lep_vec.Px() + met_x,
                         lep_vec.Py() + met_y,
                         lep_vec.Pz() + pz_nu,
                         lep_vec.E() + sqrt(met_x * met_x + met_y * met_y + pz_nu * pz_nu));
    }

    return out_vec;
  }

}

namespace var {
  HG::pT_h1 pT_h1;
  HG::pT_h2 pT_h2;
  HG::y_h1 y_h1;
  HG::y_h2 y_h2;
  HG::m_h1 m_h1;
  HG::m_h2 m_h2;
  HG::yAbs_yy yAbs_yy;
  HG::pTt_yy pTt_yy;
  HG::m_yy m_yy;
  HG::passMeyCut passMeyCut;
  HG::pT_yy pT_yy;
  HG::pT_y1 pT_y1;
  HG::pT_y2 pT_y2;
  HG::E_y1 E_y1;
  HG::E_y2 E_y2;
  HG::sumpT_y_y sumpT_y_y;
  HG::DpT_y_y DpT_y_y;
  HG::pT_hard pT_hard;
  HG::cosTS_yy cosTS_yy;
  HG::phiStar_yy phiStar_yy;
  HG::Dphi_y_y Dphi_y_y;
  HG::Dy_y_y Dy_y_y;
  HG::N_e N_e;
  HG::N_mu N_mu;
  HG::N_lep N_lep;
  HG::N_lep_15 N_lep_15;
  HG::weightN_lep weightN_lep;
  HG::weightN_lep_15 weightN_lep_15;
  HG::N_j N_j;
  HG::N_j_30 N_j_30;
  HG::N_j_50 N_j_50;
  HG::N_j_central N_j_central;
  HG::N_j_central30 N_j_central30;
  HG::N_j_btag N_j_btag;
  HG::N_j_btag30 N_j_btag30;
  HG::pT_j1_30 pT_j1_30;
  HG::pT_j1 pT_j1;
  HG::pT_j2_30 pT_j2_30;
  HG::pT_j2 pT_j2;
  HG::pT_j3_30 pT_j3_30;
  HG::yAbs_j1 yAbs_j1;
  HG::yAbs_j1_30 yAbs_j1_30;
  HG::yAbs_j2 yAbs_j2;
  HG::yAbs_j2_30 yAbs_j2_30;
  HG::pT_jj pT_jj;
  HG::pT_yyj pT_yyj;
  HG::pT_yyj_30 pT_yyj_30;
  HG::m_yyj m_yyj;
  HG::m_yyj_30 m_yyj_30;
  HG::m_jj m_jj;
  HG::m_jj_30 m_jj_30;
  HG::m_jj_50 m_jj_50;
  HG::Dy_j_j Dy_j_j;
  HG::Deta_j_j Deta_j_j;
  HG::Dy_j_j_30 Dy_j_j_30;
  HG::Dy_yy_jj Dy_yy_jj;
  HG::Dy_yy_jj_30 Dy_yy_jj_30;
  HG::Dphi_j_j Dphi_j_j;
  HG::Dphi_j_j_30 Dphi_j_j_30;
  HG::Dphi_j_j_50 Dphi_j_j_50;
  HG::Dphi_j_j_30_signed Dphi_j_j_30_signed;
  HG::Dphi_j_j_50_signed Dphi_j_j_50_signed;
  HG::Dphi_yy_jj Dphi_yy_jj;
  HG::Dphi_yy_jj_30 Dphi_yy_jj_30;
  HG::Dphi_yy_jj_50 Dphi_yy_jj_50;
  HG::m_yyjj m_yyjj;
  HG::pT_yyjj pT_yyjj;
  HG::pT_yyjj_30 pT_yyjj_30;
  HG::pT_yyjj_50 pT_yyjj_50;
  HG::m_ee m_ee;
  HG::pt_ee pt_ee;
  HG::pt_mumu pt_mumu;
  HG::pt_llmax pt_llmax;
  HG::m_mumu m_mumu;
  HG::DRmin_y_j DRmin_y_j;
  HG::DRmin_y_j_2 DRmin_y_j_2;
  HG::DR_y_y DR_y_y;
  HG::Zepp Zepp;
  HG::cosTS_yyjj cosTS_yyjj;
  HG::sumTau_yyj_30 sumTau_yyj_30;
  HG::maxTau_yyj_30 maxTau_yyj_30;
  HG::met_TST met_TST;
  HG::met_Sig met_Sig;
  HG::sumet_TST sumet_TST;
  HG::phi_TST phi_TST;
  HG::N_conv N_conv;
  HG::HT_30 HT_30;
  HG::HTall_30 HTall_30;
  HG::massTrans massTrans;
  HG::pTlepMET pTlepMET;
  HG::etalepMET etalepMET;
  HG::philepMET philepMET;
  HG::m_alljet_30 m_alljet_30;
  HG::m_alljet m_alljet;
  HG::pT_alljet pT_alljet;
  HG::eta_alljet eta_alljet;
  HG::score_recotop1 score_recotop1;
  HG::pT_recotop1 pT_recotop1;
  HG::eta_recotop1 eta_recotop1;
  HG::phi_recotop1 phi_recotop1;
  HG::m_recotop1 m_recotop1;
  HG::score_recotop2 score_recotop2;
  HG::pT_recotop2 pT_recotop2;
  HG::eta_recotop2 eta_recotop2;
  HG::phi_recotop2 phi_recotop2;
  HG::m_recotop2 m_recotop2;
  HG::pT_hybridtop2 pT_hybridtop2;
  HG::eta_hybridtop2 eta_hybridtop2;
  HG::phi_hybridtop2 phi_hybridtop2;
  HG::m_hybridtop2 m_hybridtop2;
  HG::score_hadtop1 score_hadtop1;
  HG::pT_hadtop1 pT_hadtop1;
  HG::eta_hadtop1 eta_hadtop1;
  HG::phi_hadtop1 phi_hadtop1;
  HG::m_hadtop1 m_hadtop1;
  HG::score_hadtop2 score_hadtop2;
  HG::pT_hadtop2 pT_hadtop2;
  HG::eta_hadtop2 eta_hadtop2;
  HG::phi_hadtop2 phi_hadtop2;
  HG::m_hadtop2 m_hadtop2;
  HG::pT_had_hybrid_top2 pT_had_hybrid_top2;
  HG::eta_had_hybrid_top2 eta_had_hybrid_top2;
  HG::phi_had_hybrid_top2 phi_had_hybrid_top2;
  HG::m_had_hybrid_top2 m_had_hybrid_top2;
  HG::score_leptop1 score_leptop1;
  HG::pT_leptop1 pT_leptop1;
  HG::eta_leptop1 eta_leptop1;
  HG::phi_leptop1 phi_leptop1;
  HG::m_leptop1 m_leptop1;
  HG::pT_lep_hybrid_top2 pT_lep_hybrid_top2;
  HG::eta_lep_hybrid_top2 eta_lep_hybrid_top2;
  HG::phi_lep_hybrid_top2 phi_lep_hybrid_top2;
  HG::m_lep_hybrid_top2 m_lep_hybrid_top2;
  HG::idx_jets_recotop1 idx_jets_recotop1;
  HG::idx_jets_recotop2 idx_jets_recotop2;
  HG::index_hadtop1 index_hadtop1;
  HG::index_hadtop2 index_hadtop2;
  HG::index_leptop1 index_leptop1;
  HG::isPassedBasic isPassedBasic;
  HG::isPassed isPassed;
  HG::isPassedJetEventClean isPassedJetEventClean;
  HG::isFiducial isFiducial;
  HG::isFiducialKinOnly isFiducialKinOnly;
  HG::isDalitzEvent isDalitzEvent;
  HG::cutFlow cutFlow;
  HG::weightInitial weightInitial;
  HG::weight weight;
  HG::weightSF weightSF;
  HG::weightTrigSF weightTrigSF;
  HG::vertexWeight vertexWeight;
  HG::pileupWeight pileupWeight;
  HG::weightCatCoup_Moriond2017 weightCatCoup_Moriond2017;
  HG::weightCatCoup_SFMoriond2017 weightCatCoup_SFMoriond2017;
  HG::catCoup_Moriond2017 catCoup_Moriond2017;
  HG::weightCatCoup_Moriond2017BDT_qqH2jet weightCatCoup_Moriond2017BDT_qqH2jet;
  HG::weightCatCoup_SFMoriond2017BDT_qqH2jet weightCatCoup_SFMoriond2017BDT_qqH2jet;
  HG::catCoup_Moriond2017BDT_qqH2jet catCoup_Moriond2017BDT_qqH2jet;
  HG::weightCatCoup_Moriond2017BDT weightCatCoup_Moriond2017BDT;
  HG::weightCatCoup_SFMoriond2017BDT weightCatCoup_SFMoriond2017BDT;
  HG::catCoup_Moriond2017BDT catCoup_Moriond2017BDT;
  HG::weightCatCoup_Moriond2017BDTlep weightCatCoup_Moriond2017BDTlep;
  HG::weightCatCoup_SFMoriond2017BDTlep weightCatCoup_SFMoriond2017BDTlep;
  HG::catCoup_GlobalICHEP catCoup_GlobalICHEP;
  HG::weightCatCoup_SFGlobalICHEP weightCatCoup_SFGlobalICHEP;
  HG::weightCatCoup_GlobalICHEP weightCatCoup_GlobalICHEP;
  HG::scoreBinaryCatCoup_GlobalICHEP scoreBinaryCatCoup_GlobalICHEP;
  HG::multiClassCatCoup_GlobalICHEP multiClassCatCoup_GlobalICHEP;
  HG::catCoup_HybridICHEP catCoup_HybridICHEP;
  HG::weightCatCoup_SFHybridICHEP weightCatCoup_SFHybridICHEP;
  HG::weightCatCoup_HybridICHEP weightCatCoup_HybridICHEP;
  HG::scoreBinaryCatCoup_HybridICHEP scoreBinaryCatCoup_HybridICHEP;
  HG::multiClassCatCoup_HybridICHEP multiClassCatCoup_HybridICHEP;
  HG::catCoup_Moriond2017BDTlep catCoup_Moriond2017BDTlep;
  HG::catMass_Run1 catMass_Run1;
  HG::catMass_pT catMass_pT;
  HG::catMass_eta catMass_eta;
  HG::catMass_conv catMass_conv;
  HG::catMass_mu catMass_mu;
  HG::catXS_VBF catXS_VBF;
  HG::numberOfPrimaryVertices numberOfPrimaryVertices;
  HG::selectedVertexSumPt2 selectedVertexSumPt2;
  HG::selectedVertexZ selectedVertexZ;
  HG::selectedVertexPhi selectedVertexPhi;
  HG::hardestVertexSumPt2 hardestVertexSumPt2;
  HG::hardestVertexZ hardestVertexZ;
  HG::hardestVertexPhi hardestVertexPhi;
  HG::pileupVertexSumPt2 pileupVertexSumPt2;
  HG::pileupVertexZ pileupVertexZ;
  HG::pileupVertexPhi pileupVertexPhi;
  HG::zCommon zCommon;
  HG::vertexZ vertexZ;
  HG::catCoup catCoup;
  HG::procCoup procCoup;
  HG::mu mu;
  HG::isVyyOverlap isVyyOverlap;

  HG::weightCatCoup_SFXGBoost_ttH weightCatCoup_SFXGBoost_ttH;
  HG::weightCatCoup_XGBoost_ttH weightCatCoup_XGBoost_ttH;
  HG::catCoup_XGBoost_ttH catCoup_XGBoost_ttH;
  HG::score_ttH score_ttH;

  HG::weightCatCoup_SFXGBoost_ttHCP weightCatCoup_SFXGBoost_ttHCP;
  HG::weightCatCoup_XGBoost_ttHCP weightCatCoup_XGBoost_ttHCP;
  HG::catCoup_XGBoost_ttHCP catCoup_XGBoost_ttHCP;
  HG::score_ttHCP score_ttHCP;

  HG::score_ttH_ICHEP2020 score_ttH_ICHEP2020;
  HG::score_tHjb_ICHEP2020 score_tHjb_ICHEP2020;
  HG::score_tWH_ICHEP2020 score_tWH_ICHEP2020;
  HG::score_WH_ICHEP2020 score_WH_ICHEP2020;
  HG::score_ZH_ICHEP2020 score_ZH_ICHEP2020;

  HG::weightCatCoup_SFMonoH_2var weightCatCoup_SFMonoH_2var;
  HG::weightCatCoup_MonoH_2var weightCatCoup_MonoH_2var;
  HG::catCoup_MonoH_2var catCoup_MonoH_2var;
  HG::score_MonoH_2var score_MonoH_2var;
}
