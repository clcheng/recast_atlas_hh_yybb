# source this script after setting up HGamAnalysis environment
# probably save output to make sure all got submitted properly
[[ -z "$1" ]] && echo Please enter an htag as and argument! E.G source getAllMxAODs.sh h011 && return
htag=$1

CONFIGS=(MxAODAllSys MxAODPhotonSys)

$ROOTCOREBIN/user_scripts/HGamTools/submitMxAOD.sh allMC $htag MxAOD.config -official
#$ROOTCOREBIN/user_scripts/HGamTools/submitMxAOD.sh periodAll25ns $htag MxAOD.config -official

#for config in ${CONFIGS[@]}; do
#  $ROOTCOREBIN/user_scripts/HGamTools/submitMxAOD.sh nominalMC $htag ${config}.config -official
#done
