// STL include(s):
#include <stdexcept>

// EDM include(s):
#include "xAODBTaggingEfficiency/BTaggingEfficiencyTool.h"
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "xAODBTagging/BTaggingContainer.h"
#include "xAODBTagging/BTagging.h"

// Local include(s):
#include "HGamAnalysisFramework/JetHandler.h"

#include "PhotonVertexSelection/PhotonVertexHelpers.h"

#include "BJetCalibrationTool/BJetMuInJetPtRecoTool.h"

#include "TSystem.h"

namespace HG {
  SG::AuxElement::Accessor<float> JetHandler::DetectorEta("DetectorEta");
  SG::AuxElement::Accessor<std::vector<float> > JetHandler::JVF("JVF");
  SG::AuxElement::Accessor<float> JetHandler::Jvf("Jvf");
  SG::AuxElement::Accessor<float> JetHandler::CorrJvf("CorrJvf");
  SG::AuxElement::Accessor<float> JetHandler::Jvt("Jvt");
  SG::AuxElement::Accessor<float> JetHandler::Rpt("Rpt");
  SG::AuxElement::Accessor<float> JetHandler::hardVertexJvt("hardVertexJvt");
  SG::AuxElement::Accessor<char>  JetHandler::isClean("isClean");
  SG::AuxElement::Accessor<float> JetHandler::scaleFactor("scaleFactor");

  // ______________________________________________________________________________
  JetHandler::JetHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store)
    : HgammaHandler(name, event, store)
  {}

  // ______________________________________________________________________________
  JetHandler::~JetHandler()
  {}

  // ______________________________________________________________________________
  EL::StatusCode JetHandler::initialize(Config &config)
  {
    // Set appropriate return type for ANA_CHECK
    ANA_CHECK_SET_TYPE(EL::StatusCode);

    ANA_CHECK(HgammaHandler::initialize(config));

    // General options
    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(m_event->retrieve(eventInfo, "EventInfo"));

    // Read in configuration information
    m_containerName = config.getStr(m_name + ".ContainerName");
    m_correctVertex = config.getBool(m_name + ".Calibration.CorrectVertex");
    m_truthName     = config.getStr(m_name + ".TruthContainerName", "AntiKt4TruthJets");

    //
    m_pvxName = config.getStr("PrimaryVertices.ContainerName", "HggPrimaryVertices");

    m_rapidityCut = config.getNum(m_name + ".Selection.MaxAbsRapidity", 4.4);
    m_ptCut       = config.getNum(m_name + ".Selection.PtPreCutGeV", 25.0) * GeV;
    m_jvf         = config.getNum(m_name + ".Selection.JVF", 0.25);
    m_jvt         = config.getNum(m_name + ".Selection.JVT", 0.64);
    m_fjvt         = config.getNum(m_name + ".Selection.fJVT", 0.53);
    m_doFJVT      = config.getBool(m_name + ".Selection.DoFJVT", true);
    m_applyFJVT   = config.getBool(m_name + ".Selection.ApplyFJVT", false);
    m_recomputeJvt = config.getBool(m_name + ".Selection.RecomputeJVT", false);

    if (m_applyFJVT & !m_doFJVT) {
      ANA_MSG_FATAL("Cannot perform fJVT cut without Selection.DoFJVT option set to YES. exiting.");
    }

    TString rhoKey = "";
    TString bcontainerName = m_containerName;

    // Configure JetCalibrationTool, including origin correction from derivations if requested
    if (m_containerName == "AntiKt4EMTopoJets") {
      bcontainerName = "AntiKt4EMTopoJets_BTagging201810";
    } else if (m_containerName == "AntiKt4EMPFlowJets") {
      bcontainerName = "AntiKt4EMPFlowJets_BTagging201810";
    }

    if (m_containerName.Contains("PFlowCustomVtxHgg")) {
      m_jetAlg       = "AntiKt4EMPFlow";
      bcontainerName = "AntiKt4EMPFlowJets_BTagging201903";
      rhoKey         = "Kt4PFlowCustomVtxEventShape";
    } else
    { m_jetAlg = TString(m_containerName).ReplaceAll("_BTagging201903", "").ReplaceAll("_BTagging201810", "").ReplaceAll("Jets", ""); }

    // B-tagging
    m_enableBTagging = config.getBool(m_name + ".EnableBTagging", false);

    if (m_enableBTagging) {

      TString relS = TString(gSystem->Getenv("AnalysisBase_VERSION"));
      int relV = relS.ReplaceAll("21.2.", "").Atoi();
      Info("JetHandler::init", "Using AnalysisBase release version 21.2 : %i", relV);

      TString bTagCDI, bTagCDIFullPath;

      if (HG::isMC()) {
        bTagCDI = config.getStr(m_name + ".BTagging.ScaleFactorFileName");
      } else {
        bTagCDI = config.getStr(m_name + ".BTagging.DataWorkingPointFileName");
      }

      bTagCDIFullPath = PathResolverFindCalibFile(bTagCDI.Data());

      m_defaultBJetWP     = config.getStr(m_name + ".BTagging.DefaultWP", "FixedCutBEff_77");
      m_bJetEtaCut        = config.getNum(m_name + ".BTagging.MaxAbsEta", 2.5);
      m_bTagNames         = config.getStrV(m_name + ".BTagging.TaggerNames");
      m_bTagDiscriminants = config.getStrV(m_name + ".BTagging.DiscriminantsToStore");
      // for (auto &discriminant : config.getStrV(m_name + ".BTagging.DiscriminantsToStore")) { m_bTagDiscriminants.push_back(discriminant.Data()); }
      m_bTagEVReduction   = config.getStr(m_name + ".BTagging.EVReduction");

      for (const auto &taggerName : m_bTagNames) {
        // Use the operating points defined by the Tagging tool.
        m_bTagOPs[taggerName] = config.getStrV(m_name + "." + taggerName + ".OperatingPoints");

        for (const auto &op : m_bTagOPs[taggerName]) {

          // Configure efficiency tool
          asg::AnaToolHandle<IBTaggingEfficiencyTool> effTool;
          effTool.setTypeAndName(("BTaggingEfficiencyTool/" + m_name + "_Eff_" + taggerName + "_" + op).Data());
          ANA_CHECK(effTool.setProperty("TaggerName", taggerName.Data()));
          ANA_CHECK(effTool.setProperty("OperatingPoint", op.Data()));

          if (relV >= 114)
          { ANA_CHECK(effTool.setProperty("MinPt", m_ptCut)); }

          ANA_CHECK(effTool.setProperty("JetAuthor", bcontainerName.Data()));
          ANA_CHECK(effTool.setProperty("ScaleFactorFileName", bTagCDIFullPath.Data()));
          ANA_CHECK(effTool.setProperty("EigenvectorReductionB", m_bTagEVReduction.Data()));
          ANA_CHECK(effTool.setProperty("EigenvectorReductionC", m_bTagEVReduction.Data()));
          ANA_CHECK(effTool.setProperty("EigenvectorReductionLight", m_bTagEVReduction.Data()));

          // Set calibration for MCMC scale factors.
          // In order, these DSIDs are PowHeg + Pythia8, Sherpa 2.2, PowHeg + Herwig7
          TString btagCalibs = "410470;410250;410558;410464";

          // Not all MCMC scale factors are available for all taggers so we override the default here as necessary
          // CDI file only has two options for PFlow jets and/or jets with continuous b-tagging
          if ((m_jetAlg == "AntiKt4EMPFlow")) {
            //btagCalibs = "410470;default;410558;default;default;default";
            btagCalibs = "410470;410250;410558"; // The CDI file has the same working points as Topo 2017-21-13TeV-MC16-CDI-2018-10-19_v1.root For PFlow

            if ((op == "Continuous")) {
              btagCalibs = "410470;default;410558"; // The CDI file has two working points if PFlow 2017-21-13TeV-MC16-CDI-2018-10-19_v1.root
            }
          }

          // Now set the calibration appropriately
          ANA_CHECK(effTool.setProperty("EfficiencyBCalibrations", btagCalibs.Data()));
          ANA_CHECK(effTool.setProperty("EfficiencyCCalibrations", btagCalibs.Data()));
          ANA_CHECK(effTool.setProperty("EfficiencyTCalibrations", btagCalibs.Data()));
          ANA_CHECK(effTool.setProperty("EfficiencyLightCalibrations", btagCalibs.Data()));
          ANA_CHECK(effTool.retrieve());

          // Configure selection tool
          asg::AnaToolHandle<IBTaggingSelectionTool> selTool;
          selTool.setTypeAndName(("BTaggingSelectionTool/" + m_name + "_Sel_" + taggerName + "_" + op).Data());
          ANA_CHECK(selTool.setProperty("MaxEta", m_bJetEtaCut));
          ANA_CHECK(selTool.setProperty("MinPt", m_ptCut));
          ANA_CHECK(selTool.setProperty("TaggerName", taggerName.Data()));
          ANA_CHECK(selTool.setProperty("OperatingPoint", op.Data()));
          ANA_CHECK(selTool.setProperty("JetAuthor", bcontainerName.Data()));
          ANA_CHECK(selTool.setProperty("FlvTagCutDefinitionsFileName", bTagCDIFullPath.Data()));
          ANA_CHECK(selTool.retrieve());

          // Add both tools to the map
          m_bTagEffTools[taggerName].push_back(effTool);
          m_bTagSelTools[taggerName].push_back(selTool);
        }
      }
    }

    TString jetConfig = config.getStr(m_name + ".Calibration.ConfigFile" + (HG::isAFII() ? "AFII" : ""));
    TString jetCalibSeq = config.getStr(m_name + ".Calibration.CalibSeq");

    //JB : why is Smear not applied for AFII ? I do not see this in the Twiki
    //if (HG::isMC() && !HG::isAFII()) { jetCalibSeq += "_Smear"; }
    if (HG::isMC()) {
      jetCalibSeq += "_Smear";
    } else {
      jetCalibSeq += "_Insitu";
    }

    const bool applyOriginCorrection = config.getBool("HgammaAnalysis.SelectVertex", false) && !config.getBool("HgammaAnalysis.UseHardestVertex", true);

    if (m_correctVertex && applyOriginCorrection) {
      jetCalibSeq = jetCalibSeq.ReplaceAll("Residual", "Residual_Origin");
    }

    // Initialise JetCalibrationTool
    ANA_MSG_INFO("Using jet calibration sequence: " << jetCalibSeq.Data());
    m_jetCalibTool.setTypeAndName(("JetCalibrationTool/" + m_name + "_JetCalibTool").Data());
    ANA_CHECK(m_jetCalibTool.setProperty("JetCollection", m_jetAlg.Data()));
    ANA_CHECK(m_jetCalibTool.setProperty("ConfigFile", jetConfig.Data()));
    ANA_CHECK(m_jetCalibTool.setProperty("CalibSequence", jetCalibSeq.Data()));
    ANA_CHECK(m_jetCalibTool.setProperty("IsData", HG::isData()));
    ANA_CHECK(m_jetCalibTool.setProperty("CalibArea", config.getStr(m_name + ".Calibration.CalibArea").Data()));

    if (rhoKey.Length()) {
      ANA_CHECK(m_jetCalibTool.setProperty("RhoKey", rhoKey.Data()));
      ANA_MSG_INFO("Using " << rhoKey << " for area-based pileup correction");
    }

    if (m_correctVertex && applyOriginCorrection) {
      ANA_CHECK(m_jetCalibTool.setProperty("OriginScale", "Hgg_JetOriginConstitScaleMomentum"));
      ANA_MSG_INFO("Using jet origin scale: Hgg_JetOriginConstitScaleMomentum");
    }

    //ANA_CHECK(m_jetCalibTool.setProperty("OutputLevel", MSG::VERBOSE));
    ANA_CHECK(m_jetCalibTool.retrieve());

    // Add eventually Bjet energy correction
    m_doBJetEcorr = config.getBool(m_name + ".DoBJetEnergyCorrection", false);

    if (m_doBJetEcorr) {
      m_doBJetEcorrPtRec = config.getBool(m_name + ".DoBJetEnergyCorrectionPtReco", false);

      if (m_doBJetEcorrPtRec) {
        m_doPtReco      = config.getBool(m_name + ".DoPtReco", true);
        m_BJetEcorrTool.setTypeAndName("BJetMuInJetPtRecoTool/BJetEcorrTool");
        ANA_CHECK(m_BJetEcorrTool.setProperty("JetCollection", m_jetAlg.Data()));
        ANA_CHECK(m_BJetEcorrTool.setProperty("doPtReco", m_doPtReco));

        if (config.getBool(m_name + ".DebugEnergyCorrection", false))
        { ANA_CHECK(m_BJetEcorrTool.setProperty("OutputLevel", MSG::VERBOSE)); }

        ANA_CHECK(m_BJetEcorrTool.retrieve());
      }

      m_doBJetEcorrNNReg = config.getBool(m_name + ".DoBJetEnergyCorrectionNNReg", false);

      if (m_doBJetEcorrNNReg) {
        m_NNScaleName = "NN" + config.getStr(m_name + ".NNScaleName", "Reg");
        m_BJetEcorrNNRegTool.setTypeAndName("BJetNNRegressionTool/BJetEcorrNNRegTool");
        ANA_CHECK(m_BJetEcorrNNRegTool.setProperty("JetCollection", m_jetAlg.Data()));
        ANA_CHECK(m_BJetEcorrNNRegTool.setProperty("scaleName",     m_NNScaleName.Data()));

        if (config.getBool(m_name + ".DebugEnergyCorrection", false))
        { ANA_CHECK(m_BJetEcorrNNRegTool.setProperty("OutputLevel", MSG::VERBOSE)); }

        ANA_CHECK(m_BJetEcorrNNRegTool.retrieve());
      }
    }

    // Cleaning tool
    m_doCleaning  = config.getBool(m_name + ".Selection.DoCleaning", true);
    m_jetCleaningTool.setTypeAndName("JetCleaningTool/JetCleaningTool");
    ANA_CHECK(m_jetCleaningTool.setProperty("CutLevel", config.getStr(m_name + ".Selection.CutLevel").Data()));
    ANA_CHECK(m_jetCleaningTool.setProperty("DoUgly", config.getBool(m_name + ".Selection.DoUgly")));
    ANA_CHECK(m_jetCleaningTool.retrieve());

    // Jet uncertainty tool (both JES and JER)
    TString mctype = config.getStr(m_name + ".Uncertainty.MCType");

    if (HG::isAFII()) { mctype = "AFII"; };

    m_jetUncTool.setTypeAndName(("JetUncertaintiesTool/" + m_name + "_JetUncertaintiesTool").Data());

    ANA_CHECK(m_jetUncTool.setProperty("JetDefinition", m_jetAlg.Data()));

    ANA_CHECK(m_jetUncTool.setProperty("MCType", mctype.Data()));

    ANA_CHECK(m_jetUncTool.setProperty("ConfigFile", config.getStr(m_name + ".Uncertainty.ConfigFile").Data()));

    bool uncForceData = config.getBool(m_name + ".Uncertainty.ForceData", false);

    ANA_CHECK(m_jetUncTool.setProperty("IsData", (uncForceData ? uncForceData : HG::isData())));

    if (config.isDefined(m_name + ".Uncertainty.CalibArea"))
    { ANA_CHECK(m_jetUncTool.setProperty("CalibArea", config.getStr(m_name + ".Uncertainty.CalibArea").Data())); }

    ANA_CHECK(m_jetUncTool.retrieve());

    // Track selection tool
    m_trackTool.setTypeAndName("InDet::InDetTrackSelectionTool/TrackSelectionTool");
    ANA_CHECK(m_trackTool.setProperty("CutLevel", "Loose"));
    ANA_CHECK(m_trackTool.retrieve());

    // JVT likelihood histogram
    m_jvtLikelihoodHist = HG::getHistogramPtrFromFile<TH2F>("JetMomentTools/JVTlikelihood_20140805.root", "JVTRootCore_kNN100trim_pt20to50_Likelihood");
    ANA_CHECK(m_jvtLikelihoodHist != nullptr);

    // JVT re-scaling tool
    m_jvtTool.setTypeAndName("JetVertexTaggerTool/JVTTool");
    ANA_CHECK(m_jvtTool.setProperty("VertexContainer", m_pvxName.Data()));
    ANA_CHECK(m_jvtTool.retrieve());

    // Forward JVT tool
    // For PFlow, this tool just computes the decision from the variable computed at derivation level (in JetForwardPFlowJvtTool)
    bool usefJvtTightOP = config.getStr(m_name + ".fJVT.WorkingPoint") == "Tight" ? true : false;
    m_fjvtTool.setTypeAndName("JetForwardJvtTool/fJVTTool");
    ANA_CHECK(m_fjvtTool.setProperty("UseTightOP", usefJvtTightOP));
    ANA_CHECK(m_fjvtTool.setProperty("ForwardMaxPt", config.getNum(m_name + ".fJVT.maxPtGeV", 60) * HG::GeV)); // do not understand from Twiki what is recommended
    ANA_CHECK(m_fjvtTool.retrieve());

    // JVT efficiency SF
    m_jvtSFTool.setTypeAndName("CP::JetJvtEfficiency/JVTSFTool");

    // make the JVT working point configurable, since we use Medium for EM Topo and Tight for PFlow
    ANA_CHECK(m_jvtSFTool.setProperty("WorkingPoint", config.getStr(m_name + ".JVT.WorkingPoint")));
    ANA_CHECK(m_jvtSFTool.setProperty("SFFile", config.getStr(m_name + ".JVT.ConfigFile").Data()));
    ANA_CHECK(m_jvtSFTool.setProperty("ScaleFactorDecorationName", "SF_jvt"));
    ANA_CHECK(m_jvtSFTool.setProperty("JetJvtMomentName", "Jvt"));
    ANA_CHECK(m_jvtSFTool.setProperty("JetEtaName", "DetectorEta"));
    ANA_CHECK(m_jvtSFTool.retrieve());

    // fJVT efficiency SF
    m_fjvtSFTool.setTypeAndName("CP::JetJvtEfficiency/fJVTSFTool");
    ANA_CHECK(m_fjvtSFTool.setProperty("WorkingPoint", config.getStr(m_name + ".fJVT.WorkingPoint")));
    ANA_CHECK(m_fjvtSFTool.setProperty("SFFile", config.getStr(m_name + ".fJVT.ConfigFile").Data()));
    ANA_CHECK(m_fjvtSFTool.setProperty("ScaleFactorDecorationName", "SF_fjvt"));
    ANA_CHECK(m_fjvtSFTool.setProperty("UseMuSFFormat", true)); // To account for new SF binning
    ANA_CHECK(m_fjvtSFTool.setProperty("MaxPtForJvt", config.getNum(m_name + ".fJVT.maxPtGeV", 60.) * HG::GeV)); // do not understand from Twiki what is recommended
    ANA_CHECK(m_fjvtSFTool.setProperty("JetEtaName", "DetectorEta"));
    ANA_CHECK(m_fjvtSFTool.retrieve());

    return EL::StatusCode::SUCCESS;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::getCorrectedContainer()
  {
    // Get shallow copy from TEvent/TStore
    bool calib = false;
    xAOD::JetContainer shallowContainer = getShallowContainer(calib);

    if (calib) { return shallowContainer; }

    // Setting links for the shallow copies to the objects of the original container, needed for METMaker to use the association maps
    std::string defaultContainerName = (m_jetAlg + "Jets").Data();

    if (!m_containerName.Contains("CustomVtx") && m_containerName != defaultContainerName && shallowContainer.size() > 0) {

      const xAOD::JetContainer *defaultJetContainer = nullptr;

      if (m_event->retrieve(defaultJetContainer, defaultContainerName).isFailure()) {
        ANA_MSG_FATAL("Failed to retrieve the default jet container " + defaultContainerName + ", exiting.");
        throw std::invalid_argument("Failed to retrieve the default jet container " + defaultContainerName + ", exiting.");
      }

      if (!xAOD::setOriginalObjectLink(*defaultJetContainer, shallowContainer)) {
        fatal("Failed to add original object links to shallow copy");
      }
    }

    // This is to decorate the original correction with mu-in-jet/ptreco
    const xAOD::JetContainer *origJetContainer = nullptr;

    if (m_event->retrieve(origJetContainer, m_containerName.Data()).isFailure()) {
      ANA_MSG_FATAL("Failed to retrieve the original jet container " + m_containerName + ", exiting.");
      throw std::invalid_argument("Failed to retrieve the original jet container " + m_containerName + ", exiting.");
    }

    // Otherwise start calibrating...
    if (m_jetAlg.Contains("EM")) {
      ANA_MSG_DEBUG("Setting jet constituent state to UNCALIBRATED.");

      for (auto jet : shallowContainer) {
        jet->setConstituentsSignalState(xAOD::UncalibratedJetConstituent);
      }
    }

    // Calibrate and decorate jets
    for (auto jet : shallowContainer) {
      scaleFactor(*jet) = 1.0;

      // Apply initial decorations necessary for MxAOD
      decorateJVF(jet);

      if (m_jetAlg.Contains("PFlow")) {
        ANA_MSG_DEBUG("PFlow jets cannot be cleaned. Skipping.");
        isClean(*jet) = true;
      } else {
        // isClean(*jet) = m_jetCleaning->accept(*jet);
        isClean(*jet) = m_jetCleaningTool->keep(*jet);
      }

      if (HG::isMC()) { decorateBJetTruth(jet); } // add truth-tagging for BJES unc.

      DetectorEta(*jet) = jet->getAttribute<xAOD::JetFourMom_t>("JetConstitScaleMomentum").eta();

      // Since calibrateJet applies systematic uncertainties, do this after decorations are applied
      calibrateJet(jet);

      // Reco b-tagging and JVT recalculation rely on the jet being fully calibrated
      recalculateJVT(*jet);

      if (m_enableBTagging) { decorateBJetReco(jet); }

      // b-jet energy corrections : this only compute it and store the diff wrt the uncorrected scale in the jet stat OneMu, and ptRecoSF
      if (m_doBJetEcorr) {
        if (m_sysName.Length() == 0) {

          if (m_doBJetEcorrPtRec) {
            if (m_BJetEcorrTool->applyBJetCalibration(*jet).isFailure())
            { fatal("Cannot apply mu-in-jet (+pTreco) correction"); }

            // Also decorate the original jets. So that all shallow copies have the correction
            // For pTreco, from N. Morange, VHbb use the same for all JES/JER
            origJetContainer->at(jet->index())->auxdecor<float>("OneMu_pt")  = jet->jetP4("OneMu").Pt();
            origJetContainer->at(jet->index())->auxdecor<float>("OneMu_eta") = jet->jetP4("OneMu").Eta();
            origJetContainer->at(jet->index())->auxdecor<float>("OneMu_phi") = jet->jetP4("OneMu").Phi();
            origJetContainer->at(jet->index())->auxdecor<float>("OneMu_m")   = jet->jetP4("OneMu").M();
            origJetContainer->at(jet->index())->auxdecor<float>("PtRecoSF")  = jet->auxdata<float>("PtRecoSF");
          }

          if (m_doBJetEcorrNNReg) {
            if (m_BJetEcorrNNRegTool->applyBJetCalibration(*jet).isFailure())
            { fatal("Cannot apply NN b-jet energy correction"); }

            // For the regression, I am not sure of how to treat the interplay JES vs regression : should one recompute the new pT for all sys ?
            // Here the correction factor on pT is stored
            origJetContainer->at(jet->index())->auxdecor<float>((m_NNScaleName + "PtSF").Data()) = jet->auxdata<float>((m_NNScaleName + "PtSF").Data());
          }

        }
      }

    }

    if (m_doBJetEcorr) {
      if (m_doBJetEcorrPtRec)
      { m_BJetEcorrTool->reset(); }

      if (m_doBJetEcorrNNReg)
      { m_BJetEcorrNNRegTool->reset(); }
    }

    // Add forward passFJVT flag, and JVT scale factors
    decorateJVTExtras(shallowContainer);

    // Some further corrections rely on the initial calibrated energy
    decorateRawCalib(shallowContainer);

    // Sort the Jets
    shallowContainer.sort(comparePt);

    return shallowContainer;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelectionNoCleaning(xAOD::JetContainer &container)
  {
    bool saveCleaning = m_doCleaning;
    m_doCleaning = false;

    xAOD::JetContainer sel = applySelection(container);

    m_doCleaning = saveCleaning;
    return sel;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelectionNoJvt(xAOD::JetContainer &container)
  {
    double saveJvt = m_jvt;
    m_jvt = -1.0;

    xAOD::JetContainer sel = applySelection(container);

    m_jvt = saveJvt;
    return sel;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applySelection(xAOD::JetContainer &container)
  {
    xAOD::JetContainer selected(SG::VIEW_ELEMENTS);

    for (auto jet : container) {
      // Apply pT and eta selection cuts
      if (!passPtEtaCuts(jet)) { continue; }

      // Apply cleaning cuts. MxAODs do not have isAvailable as cut is already applied
      if (m_doCleaning && isClean.isAvailable(*jet) && !isClean(*jet)) { continue; }

      // JVF cuts
      if (!passJVFCut(jet)) { continue; }

      // JVT cuts
      if (!passJVTCut(jet)) { continue; }

      // fJVT cuts
      if (!passFJVTCut(jet)) { continue; }

      // All the cuts are passed so we keep this jet
      selected.push_back(jet);
    }

    return selected;
  }

  // ______________________________________________________________________________
  xAOD::JetContainer JetHandler::applyBJetSelection(xAOD::JetContainer &container, TString wp)
  {
    if (wp == "") { wp = m_defaultBJetWP; }

    xAOD::JetContainer selected(SG::VIEW_ELEMENTS);

    if (!m_enableBTagging) { return selected; }

    for (auto jet : container) {
      if (jet->auxdata<char>(wp.Data())) { selected.push_back(jet); }
    }

    return selected;
  }

  // ______________________________________________________________________________
  CP::SystematicCode JetHandler::applySystematicVariation(const CP::SystematicSet &sys)
  {
    // Set appropriate return type for ANA_CHECK
    ANA_CHECK_SET_TYPE(CP::SystematicCode);

    setVertexCorrected(false);

    bool isAffected = false;

    for (auto var : sys) {
      if (m_jetUncTool->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (m_jvtSFTool->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (m_fjvtSFTool->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      if (HG::isMC()) {
        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            if (tool->isAffectedBySystematic(var)) {
              isAffected = true;
              break;
            }
          }
        }
      }
    }

    // This should mean that the jets can be shifted by this systematic
    if (isAffected) {
      ANA_CHECK(m_jetUncTool->applySystematicVariation(sys));
      ANA_CHECK(m_jvtSFTool->applySystematicVariation(sys));
      ANA_CHECK(m_fjvtSFTool->applySystematicVariation(sys));

      if (HG::isMC()) {
        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            ANA_CHECK(tool->applySystematicVariation(sys));
          }
        }
      }

      m_sysName = (sys.name() == "" ? "" : "_" + sys.name());

      // Jets are not affected by this systematic
    } else {
      ANA_CHECK(m_jetUncTool->applySystematicVariation(CP::SystematicSet()));
      ANA_CHECK(m_jvtSFTool->applySystematicVariation(CP::SystematicSet()));
      ANA_CHECK(m_fjvtSFTool->applySystematicVariation(CP::SystematicSet()));

      if (HG::isMC()) {
        for (auto name : m_bTagNames) {
          for (auto tool : m_bTagEffTools[name]) {
            ANA_CHECK(tool->applySystematicVariation(CP::SystematicSet()));
          }
        }
      }

      m_sysName = "";
    }

    return CP::SystematicCode::Ok;
  }

  // ______________________________________________________________________________
  void JetHandler::calibrateJet(xAOD::Jet *jet)
  {
    // applies calibration to a jet
    double E_before = jet->e();

    m_jetCalibTool->applyCalibration(*jet).ignore();

    // Uncertainty shifting
    if ((jet->pt() > 20.0 * HG::GeV) && (fabs(jet->rapidity()) < 4.4)) {
      m_jetUncTool->applyCorrection(*jet).ignore();
    }

    jet->setJetP4("DefScale", jet->jetP4());

    // decorate the photon with the calibration factor
    jet->auxdata<float>("Ecalib_ratio") = jet->e() / E_before;
  }

  // ______________________________________________________________________________
  bool JetHandler::passPtEtaCuts(const xAOD::Jet *jet)
  {
    // eta cuts
    if (fabs(jet->rapidity()) > m_rapidityCut) { return false; }

    // pt cuts
    if (jet->pt() < m_ptCut) { return false; }

    return true;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateRescaledJVT(xAOD::Jet &jet)
  {
    Jvt(jet) = m_jvtTool->updateJvt(jet);
  }

  // ______________________________________________________________________________
  void JetHandler::recalculateJVT(xAOD::Jet &jet)
  {

    // Rpt is sumPtTrk500[pv] / pT, update will simply update with the latest pT calib : can easily have it w.r.t. any vtx
    // Rpt stored is the one with original pT calib, not even the one used for DFCommonJets_Jvt
    // JVFCorr : a single value, w.r.t. pv choice decided at derivation level because = pTsumPV / (pTsumPV + 100/npu*pTsumPU, pTsumP, npu not stored)
    static SG::AuxElement::ConstAccessor<float> DFCommonJets_Jvt("DFCommonJets_Jvt");
    static SG::AuxElement::ConstAccessor<float> JvtRpt("JvtRpt");
    static SG::AuxElement::ConstAccessor<float> JVFCorr("JVFCorr");

    // Always decorate the hardVertex value (used by MET maker)
    double defaultJvt  = m_jvtTool->updateJvt(jet);
    hardVertexJvt(jet) = defaultJvt;

    // Just get the original decoration
    if (!m_recomputeJvt) {

      CorrJvf(jet) = JVFCorr.isAvailable(jet) ? JVFCorr(jet) : -99;
      Jvt(jet)     = defaultJvt;

      const xAOD::VertexContainer *vertexCont = 0;

      if (m_event->retrieve(vertexCont, m_pvxName.Data()).isFailure()) {
        ANA_MSG_WARNING("Failed to retrieve vertices, no decoration for Rpt");
        Rpt(jet) = -99;
        return;
      }

      // All these lines just to decorate with Rpt !
      const xAOD::Vertex *pvx = xAOD::PVHelpers::getHardestVertex(vertexCont);
      int ind = pvx ? pvx->index() : -1;
      double spt = -99.;

      if (ind >= 0) {
        std::vector<float> sumpttrkpt500;
        bool hasSumPtTrkPt500 = jet.getAttribute("SumPtTrkPt500", sumpttrkpt500);

        if (hasSumPtTrkPt500)
        { spt = sumpttrkpt500[ind] / jet.pt(); }
      }

      Rpt(jet) = spt;

    } else {

      // Retrieve HGamVertices, using default JVT if this fails
      ConstDataVector<xAOD::VertexContainer> *vertices = nullptr;

      if (m_store->retrieve(vertices, "HGamVertices").isFailure()) {
        ANA_MSG_WARNING("Failed to retrieve HGamVertices from TStore. Using default JVT value.");
        Jvt(jet) = defaultJvt;
        return;
      }

      // Find selected vertex, using default JVT if this fails
      const xAOD::Vertex *diphoton_vertex = nullptr;

      if (vertices && (vertices->size() > 0)) { diphoton_vertex = vertices->at(0); }

      if (diphoton_vertex == nullptr) {
        ANA_MSG_WARNING("Selected diphoton vertex could not be determined. Using default JVT value.");
        Jvt(jet) = defaultJvt;
        return;
      }

      // Get track container
      const xAOD::TrackParticleContainer *tracks = nullptr;

      if (m_event->retrieve(tracks, "InDetTrackParticles").isFailure()) {
        ANA_MSG_FATAL("Failed to retrieve InDetTrackParticles, exiting.");
        throw std::invalid_argument("Failed to retrieve InDetTrackParticles, exiting.");
      }

      // Check tracks, using default JVT if this fails
      if (tracks == nullptr) {
        ANA_MSG_WARNING("InDetTrackParticles retrieved as a nullptr! Using default JVT value.");
        Jvt(jet) = defaultJvt;
        return;
      }

      // Count the number of pileup tracks
      int nPileupTracks = 0;

      for (auto track : *tracks) {
        if (track == nullptr) { continue; }

        if (m_trackTool->accept(*track, diphoton_vertex) && track->vertex() && track->vertex()->index() != diphoton_vertex->index() && (track->pt() < 30e3)) {
          nPileupTracks++;
        }
      }

      if (nPileupTracks == 0) { nPileupTracks = 1; }

      // Get all tracks ghost-associated to the jet
      std::vector<const xAOD::IParticle *> jetTracks;
      jet.getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack, jetTracks);

      // Iterate over ghost-tracks, rejecting invalid ones and calculating sum(pT)
      double ptSum_all(0.0), ptSum_pv(0.0), ptSum_pu(0.0);

      for (auto jetTrack : jetTracks) {
        if (jetTrack == nullptr) { continue; }

        const xAOD::TrackParticle *track = static_cast<const xAOD::TrackParticle *>(jetTrack);

        // Add accepted tracks to pT sums for all, primary vertex, pileup
        if (m_trackTool->accept(*track, diphoton_vertex) && track->pt() > 500) {
          ptSum_all += track->pt();

          if ((track->vertex() && track->vertex()->index() == diphoton_vertex->index()) || (!track->vertex() && (fabs((track->z0() + track->vz() - diphoton_vertex->z()) * sin(track->theta())) < 3.0))) {
            ptSum_pv += track->pt();
          }

          if (track->vertex() && track->vertex()->index() != diphoton_vertex->index()) {
            ptSum_pu += track->pt();
          }
        }
      }

      // Perform explicit JVT calculation
      double _Rpt     = ptSum_pv / jet.pt();
      double _CorrJVF = ptSum_pv + ptSum_pu > 0 ? ptSum_pv / (ptSum_pv + 100 * ptSum_pu / nPileupTracks) : -1;
      double _JVT     = _CorrJVF >= 0 ? m_jvtLikelihoodHist->Interpolate(_CorrJVF, std::min(_Rpt, 1.0)) : -0.1;
      Rpt(jet) = _Rpt;
      CorrJvf(jet) = _CorrJVF;
      Jvt(jet) = _JVT;

    }
  }

  //______________________________________________________________________________
  double JetHandler::multiplyJvtWeights(const xAOD::JetContainer *jets_noJvtCut)
  {
    // Static function so can't use asg::Messaging
    static SG::AuxElement::Decorator<float> SF_jvt("SF_jvt");

    double weight = 1.0;

    for (auto jet : *jets_noJvtCut) {
      float current_sf = SF_jvt(*jet);

      if (current_sf <= 0.0) {
        Warning("JetHandler::multiplyJvtWeights", "Found a jet which failed JVT calibration, skipping!");
        continue;
      }

      weight *= current_sf;
    }

    return weight;
  }

  //______________________________________________________________________________
  double JetHandler::multiplyFJvtWeights(const xAOD::JetContainer *jets_noJvtCut)
  {
    // Static function so can't use asg::Messaging
    static SG::AuxElement::Decorator<float> SF_fjvt("SF_fjvt");

    double weight = 1.0;

    for (auto jet : *jets_noJvtCut) {
      float current_sf = SF_fjvt(*jet);

      if (current_sf <= 0.0) {
        Warning("JetHandler::multiplyFJvtWeights", "Found a jet which failed fJVT calibration, skipping!");
        continue;
      }

      weight *= current_sf;
    }

    return weight;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateJVTExtras(xAOD::JetContainer &jets)
  {
    // Now decorate JVT and fJVT scale factors
    static SG::AuxElement::Decorator<float> SF_jvt("SF_jvt");
    static SG::AuxElement::Decorator<float> SF_fjvt("SF_fjvt");
    float _unused;

    // Calling this decorates isJvtHS and SF_jvt
    if (HG::isMC()) {
      if (CP::CorrectionCode::Ok != m_jvtSFTool->applyAllEfficiencyScaleFactor(&jets, _unused))
      { fatal("jvtSFTool failure in decorateJVTExtras") ; }
    } else {
      for (auto jet : jets) { SF_jvt(*jet) = 1.0; }
    }

    // Tag jets with "passFJVT" forward JVT decoration
    // For PFlow, this only compute the decision from the variables stored in DAOD (Timing and DFCommon_fJvt)
    if (m_doFJVT) { m_fjvtTool->modify(jets); }

    // Calling this decorates SF_fjvt
    if (HG::isMC() && m_doFJVT) {
      if (CP::CorrectionCode::Ok != m_fjvtSFTool->applyAllEfficiencyScaleFactor(&jets, _unused))
      { fatal("fjvtSFTool failure in decorateJVTExtras") ; }
    } else {
      for (auto jet : jets) { SF_fjvt(*jet) = 1.0; }
    }
  }

  // ______________________________________________________________________________
  float JetHandler::weightJvt(const xAOD::JetContainer &jets)
  {
    if (HG::isData()) { return 1.0; }

    float weight = 1.0;

    if (CP::CorrectionCode::Ok != m_jvtSFTool->applyAllEfficiencyScaleFactor(&jets, weight))
    { fatal("jvtSFTool failure in weightJvt()"); }

    return weight;
  }

  // ______________________________________________________________________________
  float JetHandler::weightFJvt(const xAOD::JetContainer &jets)
  {
    if (HG::isData()) { return 1.0; }

    float weight = 1.0;

    if (CP::CorrectionCode::Ok != m_fjvtSFTool->applyAllEfficiencyScaleFactor(&jets, weight))
    { fatal("fjvtSFTool failure in weightFJvt()"); }

    return weight;
  }

  // ______________________________________________________________________________
  bool JetHandler::passJVTCut(const xAOD::Jet *jet)
  {
    // Normal jet check
    if (m_jvt < 0) {
      return true;
    }

    if (m_jvtSFTool->passesJvtCut(*jet)) {
      return true;
    }

    return false;
  }

  // ______________________________________________________________________________
  bool JetHandler::passFJVTCut(const xAOD::Jet *jet)
  {
    // check if decorations exist
    if (!m_doFJVT) {
      return true;
    }

    // check if cut required
    if (!m_applyFJVT)
    { return true ;}

    static SG::AuxElement::ConstAccessor<char> passFJVT("passFJVT");
    return passFJVT(*jet);
  }

  // ______________________________________________________________________________
  bool JetHandler::passJVFCut(const xAOD::Jet *jet, bool useBTagCut)
  {
    // If the cut is disabled, just return true
    if (m_jvf < 0) { return true; }

    float jvf = useBTagCut ? m_bJetJvfCut : m_jvf;

    if ((jet->pt() < 50.0 * HG::GeV) && (fabs(jet->eta()) < 2.4) && (fabs(Jvf(*jet)) < jvf)) {
      return false;
    }

    return true;
  }

  // ______________________________________________________________________________
  void JetHandler::decorateJVF(xAOD::Jet *jet)
  {
    // JVF decoration
    const xAOD::VertexContainer *vertices = 0;

    if (!m_event->retrieve(vertices, m_pvxName.Data()).isSuccess()) {
      ANA_MSG_FATAL("Failed to retrieve primary vertices " << m_pvxName << " exiting.");
      throw std::invalid_argument("Failed to retrieve PrimaryVertices, exiting.");
    }

    double ajvf = -1.;
    const xAOD::Vertex *pvx = xAOD::PVHelpers::getHardestVertex(vertices);
    int pv = pvx ? pvx->index() : -1;

    if (pv >= 0) {
      ajvf = JVF.isAvailable(*jet) ? JVF(*jet).at(pv) : -99;
    }

    Jvf(*jet) = ajvf;

  }

  // ______________________________________________________________________________
  TString JetHandler::getFlavorLabel(xAOD::Jet *jet)
  {
    static SG::AuxElement::ConstAccessor<int> PartonTruthLabelID("PartonTruthLabelID");
    int truthID = PartonTruthLabelID(*jet);

    if (truthID == 5)  { return "B"; }

    if (truthID == 4)  { return "C"; }

    if (truthID == 15) { return "T"; }

    return "Light";
  }


  // ______________________________________________________________________________
  void JetHandler::setMCGen(TString sampleName)
  {
    m_mcIndex = 0;  // Default: Pythia8

    if (sampleName.Contains("Pythia"))          { m_mcIndex = 0; }
    else if (sampleName.Contains("Py8"))        { m_mcIndex = 0; }
    else if (sampleName.Contains("PowhegPy"))   { m_mcIndex = 0; }
    else if (sampleName.Contains("Sherpa"))     { m_mcIndex = 1; }
    else if (sampleName.Contains("Hwpp"))       { m_mcIndex = 2; }
    else if (sampleName.Contains("PowhegH7"))   { m_mcIndex = 2; }
    else if (sampleName.Contains("PowhegHw7"))  { m_mcIndex = 2; }
    // else if (sampleName.Contains("Sherpa"))     { m_mcIndex = 3; } // Sherpa 2.1 dijets -- not recommended by flavour tagging group
    // else if (sampleName.Contains("Pythia8"))    { m_mcIndex = 2; } // Pythia 8 dijets -- not recommended by flavour tagging group
    // else if (sampleName.Contains("Herwig7"))    { m_mcIndex = 4; } // Herwig 7 dijets -- not recommended by flavour tagging group
    else {
      ANA_MSG_WARNING("Could not identify generator (or identified an unsupported generator) for MCMC SFs. Will use Pythia8.");
    }

    // Print message identifying showering
    if (m_mcIndex == 0)  { ANA_MSG_INFO(sampleName.Data() << " identified as Pythia8 showering."); }
    else if (m_mcIndex == 1)  { ANA_MSG_INFO(sampleName.Data() << " identified as Sherpa showering."); }
    else if (m_mcIndex == 2)  { ANA_MSG_INFO(sampleName.Data() << " identified as Herwig++/Herwig7 showering."); }
  }


  // ______________________________________________________________________________
  void JetHandler::decorateBJetReco(xAOD::Jet *jet)
  {
    // Jets always need to be decorated with something, for writting to ROOT files
    for (auto name : m_bTagNames) {
      jet->auxdata<int>((name + "_bin").Data()) = -2;

      for (auto op : m_bTagOPs[name]) {
        jet->auxdata<char>((name + "_" + op).Data()) = false;
        jet->auxdata<float>(("Eff_" + name + "_" + op).Data()) = 1.0;
        jet->auxdata<float>(("InEff_" + name + "_" + op).Data()) = 1.0;
        jet->auxdata<float>(("SF_" + name + "_" + op).Data()) = 1.0;
      }
    }

    // Fill the discriminants
    for (const auto &discriminantName : m_bTagDiscriminants) {
      double tagging_discriminant(-99);

      // Get DL1 discriminant from the selection tool
      if (discriminantName.Contains("DL1") && (m_bTagSelTools.find(discriminantName) != m_bTagSelTools.end())) {
        if (CP::CorrectionCode::Ok != m_bTagSelTools[discriminantName].at(0)->getTaggerWeight(*jet, tagging_discriminant))
        { fatal("Could not get the tagger weight"); }

        //Also store the "flavour probilities". Store only pb and pc, as pb + pc + pu = 1. This is for c-tagging
        if (jet->btagging() != 0) {
          double val = -99;

          if (!jet->btagging()->pc(discriminantName.Data(), val))
          { ANA_MSG_WARNING("Couldn't retrieve value c probability of " + discriminantName + " discriminant!"); }

          jet->auxdata<double>((discriminantName + "_pc").Data()) = val;
          val = -99;

          if (!jet->btagging()->pb(discriminantName.Data(), val))
          { ANA_MSG_WARNING("Couldn't retrieve value b probability of " + discriminantName + " discriminant!"); }

          jet->auxdata<double>((discriminantName + "_pb").Data()) = val;
          val = -99;

          if (!jet->btagging()->pu(discriminantName.Data(), val))
          { ANA_MSG_WARNING("Couldn't retrieve value u probability of " + discriminantName + " discriminant!"); }

          jet->auxdata<double>((discriminantName + "_pu").Data()) = val;
        }

        // Get MV2 discriminant from the dedicated MVx_discriminant function
      } else {
        if (jet->btagging() != 0) { jet->btagging()->MVx_discriminant(discriminantName.Data(), tagging_discriminant); }
      }

      if (tagging_discriminant == -99) {
        ANA_MSG_WARNING("Couldn't retrieve value of " + discriminantName + " discriminant!");
      }

      jet->auxdata<double>((discriminantName + "_discriminant").Data()) = tagging_discriminant;
    }

    // Ensure the jet is inside the rapidity region
    if (fabs(jet->eta()) >= m_bJetEtaCut) { return; }

    // Require JVT cut for MC15 tagging
    if (!passJVTCut(jet)) { return; }

    // Calculate b-tagging scale factors
    for (auto taggerName : m_bTagNames) {

      for (size_t iOP = 0; iOP < m_bTagOPs[taggerName].size(); ++iOP) {

        if (m_bTagOPs[taggerName].at(iOP).Contains("Continuous")) {
          jet->auxdata<int>((taggerName + "_bin").Data()) = m_bTagSelTools[taggerName].at(iOP)->getQuantile(*jet);
        }

        // Check accept/reject (when using continuous b-tagging all jets are accepted)
        const bool isAccepted(m_bTagOPs[taggerName].at(iOP).Contains("Continuous") || m_bTagSelTools[taggerName].at(iOP)->accept(*jet));

        // Decorate with decision for this b-tagging configuration
        const std::string bTagConfigName((taggerName + "_" + m_bTagOPs[taggerName].at(iOP)).Data());
        jet->auxdata<char>(bTagConfigName) = isAccepted;

        if (HG::isMC()) {
          // Set MCMC appropriately
          if (not m_bTagEffTools[taggerName].at(iOP)->setMapIndex(getFlavorLabel(jet).Data(), m_mcIndex)) {
            ANA_MSG_WARNING("Couldn't set MC/MC index properly. Results will be biased.");
          }

          // Decorate with efficiency - we check against Error so as not to throw away values when OutOfValidity
          float btagEff(1.0);

          if (CP::CorrectionCode::Error != m_bTagEffTools[taggerName].at(iOP)->getEfficiency(*jet, btagEff)) {
            jet->auxdata<float>("Eff_" + bTagConfigName) = btagEff;
          } else {
            ANA_MSG_WARNING("Couldn't retrieve efficiency for " + bTagConfigName);
          }

          // Decorate with inefficiency - we check against Error so as not to throw away values when OutOfValidity
          float btagInEff(1.0);

          if (CP::CorrectionCode::Error != m_bTagEffTools[taggerName].at(iOP)->getInefficiency(*jet, btagInEff)) {
            jet->auxdata<float>("InEff_" + bTagConfigName) = btagInEff;
          } else {
            ANA_MSG_WARNING("Couldn't retrieve inefficiency for " + bTagConfigName);
          }

          // Decorate with the correct SF depending on whether the jet is accepted or not - we check against Error so as not to throw away values when OutOfValidity
          float btagSF(1.0);

          if (CP::CorrectionCode::Error != (isAccepted ?
                                            m_bTagEffTools[taggerName].at(iOP)->getScaleFactor(*jet, btagSF) :
                                            m_bTagEffTools[taggerName].at(iOP)->getInefficiencyScaleFactor(*jet, btagSF))) {
            jet->auxdata<float>("SF_" + bTagConfigName) = btagSF;
          } else {
            ANA_MSG_WARNING("Couldn't retrieve scale factor for " + bTagConfigName);
          }
        }
      }
    }
  }

  // ______________________________________________________________________________
  void JetHandler::decorateBJetTruth(xAOD::Jet *jet)
  {
    if (HG::isData()) {
      ANA_MSG_FATAL("Only MC jets can be decorated with truth-b-tagging information!");
      throw std::invalid_argument("Only MC jets can be decorated with truth-b-tagging information!");
    }

    // If this is MC, then add the isBjet decoration for use by BJES
    SG::AuxElement::Accessor<char> accIsBjet("IsBjet");
    accIsBjet(*jet) = (jet->auxdata<int>("HadronConeExclTruthLabelID") == 5);
  }

  // ______________________________________________________________________________
  void JetHandler::printJet(const xAOD::Jet *jet, TString comment)
  {
    // Static function so need to use ROOT::Info instead of use asg::Messaging
    Info("JetHandler::printJet", "Jet %2zu  %s", jet->index(), comment.Data());
    Info("JetHandler::printJet", "   (pT,eta,phi,m) = (%5.1f GeV,%6.3f,%6.3f,%4.1f GeV)", jet->pt() / GeV, jet->eta(), jet->phi(), jet->m() / GeV);

    // print some more information
    TString str;

    if (jet->isAvailable<float>("Ecalib_ratio")) {
      str += Form("   calibFactor = %.3f", jet->auxdata<float>("Ecalib_ratio"));
    }

    Info("JetHandler::printJet", str);
  }

  // ______________________________________________________________________________
  EL::StatusCode
  JetHandler::writeContainerAndBjetContainer(xAOD::JetContainer &container, TString name)
  {
    // Check for name of collection to write
    if (name == "") {
      name = m_MxAODname + m_containerName + m_sysName;
    }

    xAOD::BTaggingContainer *btoutput(nullptr);
    xAOD::AuxContainerBase *btoutputAux(nullptr);
    TString bname, bnameAux;

    // Create new b-tagging containers
    btoutput    = new xAOD::BTaggingContainer();
    btoutputAux = new xAOD::AuxContainerBase();
    btoutput->setStore(btoutputAux);

    bname = "BTagging_";
    bname += m_MxAODname + m_containerName + m_sysName;
    bname.ReplaceAll("Jets", "");

    bnameAux = bname;
    bnameAux += "Aux.";

    // Create the new container and its auxilliary store
    xAOD::JetContainer     *output    = new xAOD::JetContainer();
    xAOD::AuxContainerBase *outputAux = new xAOD::AuxContainerBase();
    output->setStore(outputAux);

    for (auto part : container) {
      // Copy to output container
      xAOD::Jet *save = new xAOD::Jet();
      output->push_back(save);
      *save = *part;

      if (part->btagging()) {
        xAOD::BTagging *btag = new xAOD::BTagging();
        btoutput->push_back(btag);
        *btag = *(part->btagging());
      } else {
        fatal("this jet does not have a valid btagging object, exiting");
      }
    }

    if (m_event->record(btoutput, bname.Data()).isFailure())
    { return EL::StatusCode::FAILURE; }

    if (m_event->record(btoutputAux, bnameAux.Data()).isFailure())
    { return EL::StatusCode::FAILURE; }

    for (auto save : *output) {
      size_t index = save->index();
      xAOD::BTagging *btag = btoutput->at(index);
      ElementLink<xAOD::BTaggingContainer> linkBTagger(*btoutput, btag->index());
      save->setBTaggingLink(linkBTagger);
    }

    if (m_event->record(output, name.Data()).isFailure())
    { return EL::StatusCode::FAILURE; }

    name += "Aux.";

    if (m_event->record(outputAux, name.Data()).isFailure())
    { return EL::StatusCode::FAILURE; }

    return EL::StatusCode::SUCCESS;
  }
}
