// STL include(s):
#include <cmath>
#include <algorithm>
#include <iostream>
#include <fstream>

// EDM include(s):
#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"
#include "PATInterfaces/SystematicVariation.h"
#include "PhotonVertexSelection/PhotonVertexHelpers.h"
#include "PhotonVertexSelection/PhotonPointingTool.h"

// ROOT include(s):
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"

// XGBoost include(s):
#include "xgboost/c_api.h"

// Local include(s):
#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "HGamAnalysisFramework/HGamVariables.h"
#include "HGamAnalysisFramework/HGamCategoryTool.h"
#include "HGamAnalysisFramework/VarHandler.h"


namespace HG {

  //______________________________________________________________________________
  HGamCategoryTool::HGamCategoryTool(const char *name, HG::EventHandler *eventHandler)
    : asg::AsgMessaging(name)
    , m_applyFJvtSF(false)
    , m_eventHandler(eventHandler)
    , m_readerVBF_low(nullptr)
    , m_readerVBF_high(nullptr)
    , m_readerVH_had_Moriond2017(nullptr)
    , m_reader_ttHhad(nullptr)
    , m_reader_ttHlep(nullptr)
    , m_xgboost_ttHhad(nullptr)
    , m_xgboost_ttHlep(nullptr)
    , m_xgboost_topreco(nullptr)
    , m_xgboost_ttHCPhad(nullptr)
    , m_xgboost_ttHCPlep(nullptr)
    , m_xgboost_htophad_Moriond2020(nullptr)
    , m_xgboost_htoplep_Moriond2020(nullptr)
    , m_xgboost_ttHhad_Moriond2020(nullptr)
    , m_xgboost_ttHlep_Moriond2020(nullptr)
    , m_xgboost_tHhad_Moriond2020(nullptr)
    , m_xgboost_tHlep_Moriond2020(nullptr)
    , m_xgboost_VHMET_Moriond2020(nullptr)
    , m_xgboost_VHlep_Moriond2020(nullptr)
    , m_xgboost_ttH_ICHEP2020(nullptr)
    , m_xgboost_tHjb_ICHEP2020(nullptr)
    , m_xgboost_tWH_ICHEP2020(nullptr)
    , m_xgboost_WH_ICHEP2020(nullptr)
    , m_xgboost_ZH_ICHEP2020(nullptr)
    , m_xgboost_MonoH_2var(nullptr)
  { }

  //______________________________________________________________________________
  HGamCategoryTool::~HGamCategoryTool()
  {
    SafeDelete(m_readerVBF_high);
    SafeDelete(m_readerVBF_low);
    SafeDelete(m_readerVH_had_Moriond2017);
    SafeDelete(m_reader_ttHhad);
    SafeDelete(m_reader_ttHlep);
    SafeDelete(m_xgboost_ttHhad);
    SafeDelete(m_xgboost_ttHlep);
    SafeDelete(m_xgboost_topreco);
    SafeDelete(m_xgboost_ttHCPhad);
    SafeDelete(m_xgboost_ttHCPlep);
    SafeDelete(m_xgboost_htophad_Moriond2020);
    SafeDelete(m_xgboost_htoplep_Moriond2020);
    SafeDelete(m_xgboost_ttHhad_Moriond2020);
    SafeDelete(m_xgboost_ttHlep_Moriond2020);
    SafeDelete(m_xgboost_tHhad_Moriond2020);
    SafeDelete(m_xgboost_tHlep_Moriond2020);
    SafeDelete(m_xgboost_VHMET_Moriond2020);
    SafeDelete(m_xgboost_VHlep_Moriond2020);
    SafeDelete(m_xgboost_ttH_ICHEP2020);
    SafeDelete(m_xgboost_tHjb_ICHEP2020);
    SafeDelete(m_xgboost_tWH_ICHEP2020);
    SafeDelete(m_xgboost_WH_ICHEP2020);
    SafeDelete(m_xgboost_ZH_ICHEP2020);
    SafeDelete(m_xgboost_MonoH_2var);
  }

  //______________________________________________________________________________
  EL::StatusCode HGamCategoryTool::initialize(Config &config)
  {
    m_applyFJvtSF = config.getBool("JetHandler.Selection.ApplyFJVT", false);

    // Setup MVA reader

    // VBF_high
    m_readerVBF_high = new TMVA::Reader("!Color:!Silent");
    m_readerVBF_high->AddVariable("pTt_yy",      &t_pTt_yy);
    m_readerVBF_high->AddVariable("m_jj",        &t_m_jj);
    m_readerVBF_high->AddVariable("jj_DeltaEta", &t_dEta_jj);
    m_readerVBF_high->AddVariable("Dphi_yy_jj",  &t_dPhi_yy_jj);
    m_readerVBF_high->AddVariable("abs(Zepp)",   &t_Zepp);
    m_readerVBF_high->AddVariable("DRmin_y_j",   &t_Drmin_y_j2);
    TString readerPathVBF_high = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_VBF_high.weights.xml");
    m_readerVBF_high->BookMVA("BDTG", readerPathVBF_high);

    // VBF low
    m_readerVBF_low = new TMVA::Reader("!Color:!Silent");
    m_readerVBF_low->AddVariable("pTt_yy",      &t_pTt_yy);
    m_readerVBF_low->AddVariable("m_jj",        &t_m_jj);
    m_readerVBF_low->AddVariable("jj_DeltaEta", &t_dEta_jj);
    m_readerVBF_low->AddVariable("Dphi_yy_jj",  &t_dPhi_yy_jj);
    m_readerVBF_low->AddVariable("abs(Zepp)",   &t_Zepp);
    m_readerVBF_low->AddVariable("DRmin_y_j",   &t_Drmin_y_j2);
    TString readerPathVBF_low = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_VBF_low.weights.xml");
    m_readerVBF_low->BookMVA("BDTG", readerPathVBF_low);

    // Setup VH_had MVA reader
    m_readerVH_had_Moriond2017 = new TMVA::Reader("!Color:!Silent");
    m_readerVH_had_Moriond2017->AddVariable("m_jj",       &t_m_jjGeV);
    m_readerVH_had_Moriond2017->AddVariable("pTt_yy",     &t_pTt_yyGeV);
    m_readerVH_had_Moriond2017->AddVariable("Dy_yy_jj",   &t_Dy_yy_jj);
    m_readerVH_had_Moriond2017->AddVariable("cosTS_yyjj", &t_cosTS_yy_jj);
    TString readerPathVH = PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_config_VH_had_Moriond2017.xml");
    m_readerVH_had_Moriond2017->BookMVA("BDTG", readerPathVH);

    // ttH hadronic BDT
    m_reader_ttHhad = new TMVA::Reader("!Color:!Silent");
    m_reader_ttHhad->AddVariable("m_alljet",      &t_m_alljet_30);
    m_reader_ttHhad->AddVariable("HT_30",         &t_HT_30);
    m_reader_ttHhad->AddVariable("N_j_30",        &t_N_j30);
    m_reader_ttHhad->AddVariable("N_j_btag30",    &t_N_bjet30);
    m_reader_ttHhad->AddVariable("N_j_central30", &t_N_j_central30);
    m_reader_ttHhad->BookMVA("BDTG", PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_config_ttHhad_Moriond2017.xml").c_str());

    // ttH leptonic BDT
    m_reader_ttHlep = new TMVA::Reader("!Color:!Silent");
    m_reader_ttHlep->AddVariable("N_j_central30",      &t_N_j_central30);
    m_reader_ttHlep->AddVariable("HT_30",              &t_HT_30);
    m_reader_ttHlep->AddVariable("m_pTlepEtmiss",      &t_pTlepMET);
    m_reader_ttHlep->AddVariable("m_mT",               &t_massT);
    m_reader_ttHlep->BookMVA("BDTG", PathResolverFindCalibFile("HGamAnalysisFramework/BDT/MVA_config_ttHlep_Moriond2017.xml").c_str());

    // ttH hadronic XGBoost BDT
    m_xgboost_ttHhad = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHhad);
    XGBoosterLoadModel(*m_xgboost_ttHhad, PathResolverFindCalibFile("HGamAnalysisFramework/model_hadronic_bdt.h5").c_str());

    // ttH leptonic XGBoost BDT
    m_xgboost_ttHlep = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHlep);
    XGBoosterLoadModel(*m_xgboost_ttHlep, PathResolverFindCalibFile("HGamAnalysisFramework/model_leptonic_bdt.h5").c_str());

    // ttH top recoXGBoost BDT
    m_xgboost_topreco = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_topreco);
    XGBoosterLoadModel(*m_xgboost_topreco, PathResolverFindCalibFile("HGamAnalysisFramework/model_topRecoBDT.h5").c_str());

    // ttH CP hadronic XGBoost BDT
    m_xgboost_ttHCPhad = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHCPhad);
    XGBoosterLoadModel(*m_xgboost_ttHCPhad, PathResolverFindCalibFile("HGamAnalysisFramework/model_hadcp_bdt.h5").c_str());

    // ttH CP leptonic XGBoost BDT
    m_xgboost_ttHCPlep = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHCPlep);
    XGBoosterLoadModel(*m_xgboost_ttHCPlep, PathResolverFindCalibFile("HGamAnalysisFramework/model_lepcp_bdt.h5").c_str());

    // htop hadronic XGBoost BDT (Moriond2020)
    m_xgboost_htophad_Moriond2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_htophad_Moriond2020);
    XGBoosterLoadModel(*m_xgboost_htophad_Moriond2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_htop_hadronic_bdt_Moriond2020.h5").c_str());

    // htop leptonic XGBoost BDT (Moriond2020)
    m_xgboost_htoplep_Moriond2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_htoplep_Moriond2020);
    XGBoosterLoadModel(*m_xgboost_htoplep_Moriond2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_htop_leptonic_bdt_Moriond2020.h5").c_str());

    // ttH hadronic XGBoost BDT (Moriond2020)
    m_xgboost_ttHhad_Moriond2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHhad_Moriond2020);
    XGBoosterLoadModel(*m_xgboost_ttHhad_Moriond2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_ttH_hadronic_bdt_Moriond2020.h5").c_str());

    // ttH leptonic XGBoost BDT (Moriond2020)
    m_xgboost_ttHlep_Moriond2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttHlep_Moriond2020);
    XGBoosterLoadModel(*m_xgboost_ttHlep_Moriond2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_ttH_leptonic_bdt_Moriond2020.h5").c_str());

    // tH hadronic XGBoost BDT (Moriond2020)
    m_xgboost_tHhad_Moriond2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_tHhad_Moriond2020);
    XGBoosterLoadModel(*m_xgboost_tHhad_Moriond2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_tH_hadronic_bdt_Moriond2020.h5").c_str());

    // tH leptonic XGBoost BDT (Moriond2020)
    m_xgboost_tHlep_Moriond2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_tHlep_Moriond2020);
    XGBoosterLoadModel(*m_xgboost_tHlep_Moriond2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_tH_leptonic_bdt_Moriond2020.h5").c_str());

    // VH MET XGBoost BDT (Moriond2020)
    m_xgboost_VHMET_Moriond2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_VHMET_Moriond2020);
    XGBoosterLoadModel(*m_xgboost_VHMET_Moriond2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_VH_MET_bdt_Moriond2020.h5").c_str());

    // VH lep XGBoost BDT (Moriond2020)
    m_xgboost_VHlep_Moriond2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_VHlep_Moriond2020);
    XGBoosterLoadModel(*m_xgboost_VHlep_Moriond2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_VH_lep_bdt_Moriond2020.h5").c_str());

    // ttH XGBoost BDT (ICHEP2020)
    m_xgboost_ttH_ICHEP2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ttH_ICHEP2020);
    XGBoosterLoadModel(*m_xgboost_ttH_ICHEP2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_ttH_bdt_ICHEP2020.h5").c_str());

    // tHjb XGBoost BDT (ICHEP2020)
    m_xgboost_tHjb_ICHEP2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_tHjb_ICHEP2020);
    XGBoosterLoadModel(*m_xgboost_tHjb_ICHEP2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_tHjb_bdt_ICHEP2020.h5").c_str());

    // tWH XGBoost BDT (ICHEP2020)
    m_xgboost_tWH_ICHEP2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_tWH_ICHEP2020);
    XGBoosterLoadModel(*m_xgboost_tWH_ICHEP2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_tWH_bdt_ICHEP2020.h5").c_str());

    // WH XGBoost BDT (ICHEP2020)
    m_xgboost_WH_ICHEP2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_WH_ICHEP2020);
    XGBoosterLoadModel(*m_xgboost_WH_ICHEP2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_WH_bdt_ICHEP2020.h5").c_str());

    // ZH XGBoost BDT (ICHEP2020)
    m_xgboost_ZH_ICHEP2020 = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_ZH_ICHEP2020);
    XGBoosterLoadModel(*m_xgboost_ZH_ICHEP2020, PathResolverFindCalibFile("HGamAnalysisFramework/model_ZH_bdt_ICHEP2020.h5").c_str());

    // MonoH XGBoost BDT (2var)
    m_xgboost_MonoH_2var = new BoosterHandle();
    XGBoosterCreate(0, 0, * &m_xgboost_MonoH_2var);
    XGBoosterLoadModel(*m_xgboost_MonoH_2var, PathResolverFindCalibFile("HGamAnalysisFramework/model_MonoH_2var_bdt.h5").c_str());

    // run make HGamAnalysisFrameworkDataInstall
    // global + binaries BDTs
    {
      const std::string folder_multiclass = "HGamAnalysisFramework/multiclass_plus_binary_coupling_stxs1.2_ICHEP_2020";
      const std::string path_multiclass = PathResolverFindCalibDirectory(folder_multiclass);

      if (path_multiclass.empty()) {
        ATH_MSG_FATAL("cannot find directory " << folder_multiclass);
        return EL::StatusCode::FAILURE;
      }

      ATH_MSG_INFO("initializing multiclass + binary (global) using folder: " << path_multiclass);
      m_cat_tool_global_ICHEP = Coupling2020::CategorizationGlobalICHEP::factory("ToolGlobalICHEPCat", path_multiclass);

      ATH_MSG_INFO("initializing multiclass + binary (hybrid) using folder: " << path_multiclass);
      m_cat_tool_hybrid_ICHEP = Coupling2020::CategorizationHybridICHEP::factory("ToolHybridlICHEPCat", path_multiclass);

    }

    return EL::StatusCode::SUCCESS;
  }

  //______________________________________________________________________________
  void HGamCategoryTool::saveHGamCategoryInfo(const xAOD::PhotonContainer    *photons,
                                              const xAOD::ElectronContainer  *electrons,
                                              const xAOD::MuonContainer      *muons,
                                              const xAOD::JetContainer       *jets,
                                              const xAOD::MissingETContainer *mets,
                                              const xAOD::JetContainer       *jetsNoJvt)
  {

    // Clean the vector for top-reco (important for sys loop)
    static SG::AuxElement::Accessor<float> bdt_leptop1("bdt_leptop1");
    static SG::AuxElement::Accessor<float> bdt_hadtop1("bdt_hadtop1");
    static SG::AuxElement::Accessor<float> bdt_hadtop2("bdt_hadtop2");

    for (auto jet : *jets) {
      bdt_hadtop1(*const_cast<xAOD::Jet *>(jet)) = 0;
      bdt_hadtop2(*const_cast<xAOD::Jet *>(jet)) = 0;
      bdt_leptop1(*const_cast<xAOD::Jet *>(jet)) = 0;
    }

    // Decoration for reconstructed top candidates
    decorateSleptopCandidate(jets, electrons, muons, mets);
    decorateHadtopCandidate(jets, 1);
    decorateHadtopCandidate(jets, 2);

    // Determine the category and weight
    if (!var::weightCatCoup_SFMoriond2017BDT.exists()) {
      std::pair<int, float> catCoup_Moriond2017BDT;
      catCoup_Moriond2017BDT = getCategoryAndWeightMoriond2017_ttHBDT(photons, electrons, muons, jets, jetsNoJvt, mets);
      var::catCoup_Moriond2017BDT.setValue(catCoup_Moriond2017BDT.first);
      var::weightCatCoup_SFMoriond2017BDT.setValue(catCoup_Moriond2017BDT.second);
    }

    var::weightCatCoup_Moriond2017BDT.setValue(var::weightCatCoup_SFMoriond2017BDT()*var::weight());

    // Determine the category and weight
    if (!var::weightCatCoup_SFXGBoost_ttH.exists()) {
      float *catCoup_XGBoost_ttH;
      catCoup_XGBoost_ttH = getCategoryAndWeightXGBoost_ttH(photons, electrons, muons, jets, jetsNoJvt, mets);
      var::catCoup_XGBoost_ttH.setValue((int)catCoup_XGBoost_ttH[0]);
      var::weightCatCoup_SFXGBoost_ttH.setValue(catCoup_XGBoost_ttH[1]);
      var::score_ttH.setValue(catCoup_XGBoost_ttH[2]);
    }

    var::weightCatCoup_XGBoost_ttH.setValue(var::weightCatCoup_SFXGBoost_ttH()*var::weight());

    // Determine the category and weight
    if (!var::weightCatCoup_SFXGBoost_ttHCP.exists()) {
      float *catCoup_XGBoost_ttHCP;
      catCoup_XGBoost_ttHCP = getCategoryAndWeightXGBoost_ttHCP(photons, electrons, muons, jets, jetsNoJvt, mets);
      var::catCoup_XGBoost_ttHCP.setValue((int)catCoup_XGBoost_ttHCP[0]);
      var::weightCatCoup_SFXGBoost_ttHCP.setValue(catCoup_XGBoost_ttHCP[1]);
      var::score_ttHCP.setValue(catCoup_XGBoost_ttHCP[2]);
    }

    var::weightCatCoup_XGBoost_ttHCP.setValue(var::weightCatCoup_SFXGBoost_ttHCP()*var::weight());

    // Determine the binary BDT scores
    if (!var::score_ttH_ICHEP2020.exists()) {
      float *score_ICHEP2020;
      score_ICHEP2020 = getScoreICHEP2020(photons, electrons, muons, jets, jetsNoJvt, mets);
      var::score_ttH_ICHEP2020.setValue(score_ICHEP2020[0]);
      var::score_tHjb_ICHEP2020.setValue(score_ICHEP2020[1]);
      var::score_tWH_ICHEP2020.setValue(score_ICHEP2020[2]);
      var::score_WH_ICHEP2020.setValue(score_ICHEP2020[3]);
      var::score_ZH_ICHEP2020.setValue(score_ICHEP2020[4]);
    }

    // multiclass + binaries (global) ICHEP 2020
    eventHandler()->mu();  // stupid line, needed to fill var::mu()
    eventHandler()->numberOfPrimaryVertices(); // stupid line, needed to fill var::numberOfPrimaryVertices()

    if (!var::weightCatCoup_SFGlobalICHEP.exists()) {
      // ATH_MSG_INFO("------> runNumber: " << eventHandler()->runNumber() << " mcChannelNumber: " << eventHandler()->mcChannelNumber());

      const Coupling2020::CategorizationInputs inputs{electrons, muons, jets, jetsNoJvt, mets, eventHandler()};
      Coupling2020::CategorizationMultiClassPlusBinaries::Result r = m_cat_tool_global_ICHEP->get_category(inputs, m_applyFJvtSF);
      var::weightCatCoup_SFGlobalICHEP.setValue(r.weight_SF);
      var::catCoup_GlobalICHEP.setValue(r.index);
      var::scoreBinaryCatCoup_GlobalICHEP.setValue(r.score_binary);
      var::multiClassCatCoup_GlobalICHEP.setValue(r.index_multiclass);
    }

    var::weightCatCoup_GlobalICHEP.setValue(var::weight()*var::weightCatCoup_SFGlobalICHEP());

    // multiclass + binaries (hybrid) ICHEP 2020

    if (!var::weightCatCoup_SFHybridICHEP.exists()) {
      // ATH_MSG_INFO("------> runNumber: " << eventHandler()->runNumber() << " mcChannelNumber: " << eventHandler()->mcChannelNumber());

      const Coupling2020::CategorizationInputs inputs{electrons, muons, jets, jetsNoJvt, mets, eventHandler()};
      Coupling2020::CategorizationMultiClassPlusBinaries::Result r = m_cat_tool_hybrid_ICHEP->get_category(inputs, m_applyFJvtSF);
      var::weightCatCoup_SFHybridICHEP.setValue(r.weight_SF);
      var::catCoup_HybridICHEP.setValue(r.index);
      var::scoreBinaryCatCoup_HybridICHEP.setValue(r.score_binary);
      var::multiClassCatCoup_HybridICHEP.setValue(r.index_multiclass);
    }

    var::weightCatCoup_HybridICHEP.setValue(var::weight()*var::weightCatCoup_SFHybridICHEP());

    // Determine the category and weight
    if (!var::weightCatCoup_SFMonoH_2var.exists()) {
      float *catCoup_MonoH_2var;
      catCoup_MonoH_2var = getCategoryAndWeightMonoH_2var(photons, electrons, muons, jets, jetsNoJvt, mets);
      var::catCoup_MonoH_2var.setValue((int)catCoup_MonoH_2var[0]);
      var::weightCatCoup_SFMonoH_2var.setValue(catCoup_MonoH_2var[1]);
      var::score_MonoH_2var.setValue(catCoup_MonoH_2var[2]);
    }

    var::weightCatCoup_MonoH_2var.setValue(var::weightCatCoup_SFMonoH_2var()*var::weight());

    if (!var::catMass_Run1.exists()) {
      var::catMass_Run1.setValue(getMassCategoryRun1(photons));
    }

    if (!var::catMass_eta.exists() && photons != nullptr) {
      var::catMass_eta.setValue(getEtaMassCategory(photons));
    }

    if (!var::catMass_conv.exists() && photons != nullptr) {
      var::catMass_conv.setValue(getConvMassCategory(photons));
    }

    if (!var::catMass_pT.exists() && photons != nullptr) {
      var::catMass_pT.setValue(getPtMassCategory(photons));
    }

    if (!var::catMass_mu.exists() && photons != nullptr) {
      var::catMass_mu.setValue(getMuMassCategory(eventHandler()->mu()));
    }

    return;
  }

  //______________________________________________________________________________
  void HGamCategoryTool::saveHGamTruthCategoryInfo(const xAOD::TruthParticleContainer */*photons*/,
                                                   const xAOD::TruthParticleContainer */*electrons*/,
                                                   const xAOD::TruthParticleContainer */*muons*/,
                                                   const xAOD::JetContainer           */*jets*/,
                                                   const xAOD::MissingETContainer     */*mets*/)
  {

    if (!var::catCoup_Moriond2017BDT.exists()) {
      var::catCoup_Moriond2017BDT.setValue(0);
      var::weightCatCoup_Moriond2017BDT.setValue(var::weightInitial());
      var::weightCatCoup_SFMoriond2017BDT.setValue(1.0);
    }

    if (!var::catCoup_XGBoost_ttH.exists()) {
      var::catCoup_XGBoost_ttH.setValue(0);
      var::weightCatCoup_XGBoost_ttH.setValue(var::weightInitial());
      var::weightCatCoup_SFXGBoost_ttH.setValue(1.0);
      var::score_ttH.setValue(-1.0);
    }

    if (!var::catCoup_XGBoost_ttHCP.exists()) {
      var::catCoup_XGBoost_ttHCP.setValue(0);
      var::weightCatCoup_XGBoost_ttHCP.setValue(var::weightInitial());
      var::weightCatCoup_SFXGBoost_ttHCP.setValue(1.0);
      var::score_ttHCP.setValue(-1.0);
    }

    if (!var::score_ttH_ICHEP2020.exists()) {
      var::score_ttH_ICHEP2020.setValue(-1.0);
      var::score_tHjb_ICHEP2020.setValue(-1.0);
      var::score_tWH_ICHEP2020.setValue(-1.0);
      var::score_WH_ICHEP2020.setValue(-1.0);
      var::score_ZH_ICHEP2020.setValue(-1.0);
    }

    if (!var::catCoup_MonoH_2var.exists()) {
      var::catCoup_MonoH_2var.setValue(0);
      var::weightCatCoup_MonoH_2var.setValue(var::weightInitial());
      var::weightCatCoup_SFMonoH_2var.setValue(1.0);
      var::score_MonoH_2var.setValue(-1.0);
    }

    return;
  }

  //___________________________________________________________________________________________________________
  float HGamCategoryTool::getMVAWeight(TMVA::Reader *xReader)
  {
    //resetReader();  // redundant to call so many times, only done once now.
    float BDTG_weight = xReader->EvaluateMVA("BDTG") ;
    return BDTG_weight;
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VBF_Moriond2020(const xAOD::JetContainer  *jetsJVT)
  {
    m_weight = 1.0;

    // Check Basic selection
    if (var::N_j_30() < 2) { return 0 ; }

    if (var::Deta_j_j() < 2) { return 0; }

    if (fabs(var::Zepp()) > 5) { return 0; }

    // Get JVT weight and return category
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) {
        m_weight *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
      } // JVT SFs

    if (var::pT_yyjj() > 25 * HG::GeV) { return 2; }
    else { return 1; }
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VBF_Moriond2017(const xAOD::JetContainer  *jetsJVT)
  {
    m_weight = 1.0;

    // Check Basic selection
    if (var::N_j_30() < 2) { return 0 ; }

    if (var::Deta_j_j() < 2) { return 0; }

    if (fabs(var::Zepp()) > 5) { return 0; }

    // Get JVT weight and return category
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) {
        m_weight *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
      } // JVT SFs

    if (var::pT_yyjj() > 25 * HG::GeV) { return 2; }
    else { return 1; }
  }


  //___________________________________________________________________________________________________________
  bool HGamCategoryTool::Passes_VH_hadronic_Moriond2020(const xAOD::JetContainer *jetsJVT)
  {
    m_weight = 1.0;

    // Check basic selection
    if (var::N_j_30() < 2) { return 0; }

    if (var::m_jj() <  60 * HG::GeV) { return 0; }

    if (var::m_jj() > 120 * HG::GeV) { return 0; }

    // Get JVT weight and return passed
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) {
        m_weight *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
      } // JVT SFs

    return 1;
  }


  //___________________________________________________________________________________________________________
  bool HGamCategoryTool::Passes_VH_hadronic_Moriond2017(const xAOD::JetContainer *jetsJVT)
  {
    m_weight = 1.0;

    // Check basic selection
    if (var::N_j_30() < 2) { return 0; }

    if (var::m_jj() <  60 * HG::GeV) { return 0; }

    if (var::m_jj() > 120 * HG::GeV) { return 0; }

    // Get JVT weight and return passed
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) {
        m_weight *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
      } // JVT SFs

    return 1;
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_MET_Moriond2020(const xAOD::PhotonContainer *photons,
                                                  const xAOD::ElectronContainer  *electrons,
                                                  const xAOD::MuonContainer      *muons,
                                                  const xAOD::MissingETContainer *met)
  {
    m_weight = 1.0;

    int m_Nelectrons = electrons->size();
    int m_Nmuons = muons->size();

    if ((m_Nelectrons + m_Nmuons) != 0) { return 0; }

    float TST_met = -99;//, TST_sumet = -99;

    if ((*met)["TST"] != nullptr) {
      TST_met   = (*met)["TST"]->met();
      //TST_sumet = (*met)["TST"]->sumet();
    }

    if (TST_met < 75 * HG::GeV) { return 0; }

    float *vars = make_XGBoost_DMatrix_VHMET_Moriond2020(photons, met); // build DMatrix
    float score_VH = get_XGBoost_Weight(m_xgboost_VHMET_Moriond2020, vars, 20); // get VH score

    if (TST_met >= 250 * HG::GeV && score_VH < 0.074) {
      return 4;
    } else if (TST_met >= 150 * HG::GeV && TST_met < 250 * HG::GeV && score_VH < 0.012) {
      return 3;
    } else if (TST_met >= 75 * HG::GeV && TST_met < 150 * HG::GeV && score_VH < 0.008) {
      return 2;
    } else if (TST_met >= 75 * HG::GeV && TST_met < 150 * HG::GeV && score_VH < 0.038) {
      return 1;
    }

    // should only reach here if failed selection
    return 0;
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_MET_Moriond2017(const xAOD::PhotonContainer *   /*photons*/,
                                                  const xAOD::MissingETContainer *met)
  {
    m_weight = 1.0;
    float TST_met = -99, TST_sumet = -99;

    if ((*met)["TST"] != nullptr) {
      TST_met   = (*met)["TST"]->met();
      TST_sumet = (*met)["TST"]->sumet();
    }

    float m_MET_signi = -99999;

    if (TST_met != 0) { m_MET_signi = TST_met * 0.001 / sqrt(TST_sumet * 0.001); }

    if (TST_met < 150 * HG::GeV && TST_met >= 80 * HG::GeV && m_MET_signi > 8) { return 1; }
    else if (TST_met < 250 * HG::GeV && TST_met >= 150 * HG::GeV && m_MET_signi > 9) { return 2; }
    else if (TST_met >= 250 * HG::GeV) { return 3; }
    else { return 0; }
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_leptonic_Moriond2020(
    const xAOD::PhotonContainer    *photons,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::MissingETContainer *met)
  {

    m_weight = 1.0;
    int m_Nelectrons = electrons->size();
    int m_Nmuons = muons->size();
    m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs

    if ((m_Nelectrons + m_Nmuons) != 1) { return 0; }

    bool passZey(true);

    for (auto el : *electrons) {
      double mey1 = (el->p4() + photons->at(0)->p4()).M() * HG::invGeV;
      double mey2 = (el->p4() + photons->at(1)->p4()).M() * HG::invGeV;

      if ((fabs(mey1 - 89) < 5) || (fabs(mey2 - 89) < 5)) { passZey = false; }
    }

    if (!passZey) { return 0; }

    float *vars = make_XGBoost_DMatrix_VHlep_Moriond2020(photons, electrons, muons, met); // build DMatrix
    float score_VH = get_XGBoost_Weight(m_xgboost_VHlep_Moriond2020, vars, 25); // get VH score

    if (var::pTlepMET() >= 250 * HG::GeV && score_VH < 0.106) {
      return 6;
    } else if (var::pTlepMET() >= 150 * HG::GeV && var::pTlepMET() < 250 * HG::GeV && score_VH < 0.074) {
      return 5;
    } else if (var::pTlepMET() >= 75 * HG::GeV && var::pTlepMET() < 150 * HG::GeV && score_VH < 0.036) {
      return 4;
    } else if (var::pTlepMET() >= 75 * HG::GeV && var::pTlepMET() < 150 * HG::GeV && score_VH < 0.094) {
      return 3;
    } else if (var::pTlepMET() >= 0 * HG::GeV && var::pTlepMET() < 75 * HG::GeV && score_VH < 0.062) {
      return 2;
    } else if (var::pTlepMET() >= 0 * HG::GeV && var::pTlepMET() < 75 * HG::GeV && score_VH < 0.262) {
      return 1;
    }

    // should only reach here if failed selection
    return 0;

  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_leptonic_Moriond2017(
    const xAOD::PhotonContainer    *photons,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::MissingETContainer *met)
  {

    m_weight = 1.0;
    int m_Nelectrons = electrons->size();
    int m_Nmuons = muons->size();
    m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs

    if ((m_Nelectrons + m_Nmuons) != 1) { return 0; }

    bool passZey(true);

    for (auto el : *electrons) {
      double mey1 = (el->p4() + photons->at(0)->p4()).M() * HG::invGeV;
      double mey2 = (el->p4() + photons->at(1)->p4()).M() * HG::invGeV;

      if ((fabs(mey1 - 89) < 5) || (fabs(mey2 - 89) < 5)) { passZey = false; }
    }

    if (!passZey) { return 0; }

    float TST_met = -99, TST_sumet = -99;

    if ((*met)["TST"] != nullptr) {
      TST_met   = (*met)["TST"]->met();
      TST_sumet = (*met)["TST"]->sumet();
    }

    float m_MET_signi = -99999;

    if (TST_met != 0) { m_MET_signi = TST_met * HG::invGeV / sqrt(TST_sumet * HG::invGeV); }

    TLorentzVector metVec;
    metVec.SetPtEtaPhiM((*met)["TST"]->met(), 0, (*met)["TST"]->phi(), 0);

    bool pTVlt150 = true;

    if (((m_Nelectrons == 1) && ((electrons->at(0)->p4() + metVec).Pt() > 150 * HG::GeV)) ||
        ((m_Nmuons == 1) && ((muons    ->at(0)->p4() + metVec).Pt() > 150 * HG::GeV)))
    { pTVlt150 = false; }

    if (pTVlt150 && m_MET_signi > 1) { return 1; }
    else if (!pTVlt150) { return 2; }
    else { return 0; }
  }


  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_dileptons_Moriond2020(const xAOD::PhotonContainer *    /*photons*/,
                                                        const xAOD::ElectronContainer  *electrons,
                                                        const xAOD::MuonContainer      *muons)
  {
    m_weight = 1.0;
    int m_Nelectrons = electrons->size();
    int m_Nmuons = muons->size();
    m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
    float m_m_mumu = 0, m_m_ee = 0, pT_m_ee = 0, pT_m_mumu = 0;

    if (m_Nelectrons > 1) {
      m_m_ee = (electrons->at(0)->p4() + electrons->at(1)->p4()).M();
      pT_m_ee = (electrons->at(0)->p4() + electrons->at(1)->p4()).Pt();
    }

    if (m_Nmuons > 1) {
      m_m_mumu = (muons->at(0)->p4() + muons->at(1)->p4()).M();
      pT_m_mumu = (muons->at(0)->p4() + muons->at(1)->p4()).Pt();
    }

    bool passMuSel = (m_Nmuons >= 2 && m_m_mumu > 70 * HG::GeV && m_m_mumu < 110 * HG::GeV);
    bool passElSel = (m_Nelectrons >= 2 && m_m_ee > 70 * HG::GeV && m_m_ee < 110 * HG::GeV);


    if ((passMuSel && pT_m_mumu >= 150 * HG::GeV) || (passElSel && pT_m_ee >= 150 * HG::GeV)) { return 3; }
    else if ((passMuSel && pT_m_mumu >= 75 * HG::GeV) || (passElSel && pT_m_ee >= 75 * HG::GeV)) { return 2; }
    else if ((passMuSel && pT_m_mumu >= 0 * HG::GeV) || (passElSel && pT_m_ee >= 0 * HG::GeV)) { return 1; }
    else { return 0; }
  }

  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_VH_dileptons_Moriond2017(const xAOD::PhotonContainer *    /*photons*/,
                                                        const xAOD::ElectronContainer  *electrons,
                                                        const xAOD::MuonContainer      *muons)
  {
    m_weight = 1.0;
    int m_Nelectrons = electrons->size();
    int m_Nmuons = muons->size();
    m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
    float m_m_mumu = 0, m_m_ee = 0, pT_m_ee = 0, pT_m_mumu = 0;

    if (m_Nelectrons > 1) {
      m_m_ee = (electrons->at(0)->p4() + electrons->at(1)->p4()).M();
      pT_m_ee = (electrons->at(0)->p4() + electrons->at(1)->p4()).Pt();
    }

    if (m_Nmuons > 1) {
      m_m_mumu = (muons->at(0)->p4() + muons->at(1)->p4()).M();
      pT_m_mumu = (muons->at(0)->p4() + muons->at(1)->p4()).Pt();
    }

    bool passMuSel = (m_Nmuons >= 2 && m_m_mumu > 70 * HG::GeV && m_m_mumu < 110 * HG::GeV);
    bool passElSel = (m_Nelectrons >= 2 && m_m_ee > 70 * HG::GeV && m_m_ee < 110 * HG::GeV);
    bool pTVlt150 = ((passMuSel && pT_m_mumu < 150 * HG::GeV) || (passElSel && pT_m_ee < 150 * HG::GeV));
    bool pTVgt150 = ((passMuSel && pT_m_mumu >= 150 * HG::GeV) || (passElSel && pT_m_ee >= 150 * HG::GeV));


    if (pTVlt150) { return 1; }
    else if (pTVgt150) { return 2; }
    else { return 0; }
  }

  //___________________________________________________________________________________________________________
  bool HGamCategoryTool::Passes_qqH_BSM_2jet_Moriond2020(const xAOD::JetContainer *jetsJVT)
  {
    m_weight = 1.0;

    if (var::pT_yy() <= 200 * HG::GeV || var::m_jj() <=  350 * HG::GeV || var::N_j_30() < 2) { return 0; }

    // get JVT weight and return passed
    for (auto jet : *jetsJVT) {
      m_weight *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
    } // JVT SFs

    return 1;
  }


  //___________________________________________________________________________________________________________
  bool HGamCategoryTool::Passes_qqH_BSM_2jet(const xAOD::JetContainer *jetsJVT)
  {
    m_weight = 1.0;

    if (var::pT_j1_30() <= 200 * HG::GeV || var::N_j_30() < 2) { return 0; }

    // get JVT weight and return passed
    for (auto jet : *jetsJVT) {
      m_weight *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
    } // JVT SFs

    return 1;
  }


  //___________________________________________________________________________________________________________
  bool HGamCategoryTool::Passes_qqH_BSM(const xAOD::JetContainer *jetsJVT)
  {
    m_weight = 1.0;

    if (var::pT_j1_30() <= 200 * HG::GeV) { return 0; }

    // get JVT weight and return passed
    for (auto jet : *jetsJVT) {
      m_weight *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
    } // JVT SFs

    return 1;
  }


  //___________________________________________________________________________________________________________
  // ----------------------------------------------------------------------
  //  STXS ggF Categories (jared.vasquez@yale.edu)
  //  Updated splitting for Moriond 2020 analysis (STXS measurements)
  // ----------------------------------------------------------------------
  int HGamCategoryTool::split_ggH_STXS_Moriond2020(const xAOD::PhotonContainer *photons,
                                                   const xAOD::JetContainer    *jetsJVT)
  {
    m_weight = 1.0;

    // BSM bins
    if (var::pT_yy() >  650 * HG::GeV) { return M20_ggH_BSM4; }

    if (var::pT_yy() >  450 * HG::GeV) { return M20_ggH_BSM3; }

    if (var::pT_yy() >  300 * HG::GeV) { return M20_ggH_BSM2; }

    if (var::pT_yy() >  200 * HG::GeV) { return M20_ggH_BSM1; }

    // Get JVT weight from collection of 30 GeV jets w/ out JVT cut
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) {
        m_weight *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
      }

    // Are photons forward or central?
    bool isFwd = (fabs(photons->at(0)->eta()) > 0.95 || fabs(photons->at(1)->eta()) > 0.95);

    int pTbin = 999;

    if (var::pT_yy() <=  10 * HG::GeV) { pTbin = 0; }
    else if (var::pT_yy() <=  60 * HG::GeV) { pTbin = 1; }
    else if (var::pT_yy() <= 120 * HG::GeV) { pTbin = 2; }
    else if (var::pT_yy() <= 200 * HG::GeV) { pTbin = 3; }

    // >= 2 Jets
    if ((var::N_j_30() >= 2) && (pTbin == 3)) { return M20_ggH_2J_HIGH; }

    if ((var::N_j_30() >= 2) && (pTbin == 2)) { return M20_ggH_2J_MED; }

    if ((var::N_j_30() >= 2) && (pTbin <= 1)) { return M20_ggH_2J_LOW; }

    // == 1 Jets
    if ((var::N_j_30() == 1) && (pTbin == 3)) { return M20_ggH_1J_HIGH; }

    if ((var::N_j_30() == 1) && (pTbin == 2)) { return M20_ggH_1J_MED; }

    if ((var::N_j_30() == 1) && (pTbin <= 1)) { return M20_ggH_1J_LOW; }

    // ggH+0j, split Cen/Fwd regions
    if (var::N_j_30() == 0 && pTbin != 0 &&  isFwd) { return M20_ggH_0J_HIGH_Fwd; }

    if (var::N_j_30() == 0 && pTbin != 0 && !isFwd) { return M20_ggH_0J_HIGH_Cen; }

    if (var::N_j_30() == 0 && pTbin == 0 &&  isFwd) { return M20_ggH_0J_LOW_Fwd; }

    if (var::N_j_30() == 0 && pTbin == 0 && !isFwd) { return M20_ggH_0J_LOW_Cen; }

    Error("HGamCategoryTool::split_ggH_STXS()", "Please Investigate. Could not find proper inclusive category!");
    return 0; // Should never get here.
  }


  //___________________________________________________________________________________________________________
  // ----------------------------------------------------------------------
  //  STXS ggF Categories (jared.vasquez@yale.edu)
  //  Updated splitting for Moriond analysis (STXS measurements)
  // ----------------------------------------------------------------------
  int HGamCategoryTool::split_ggH_STXS(const xAOD::PhotonContainer *photons,
                                       const xAOD::JetContainer    *jetsJVT)
  {
    m_weight = 1.0;

    // Get JVT weight from collection of 30 GeV jets w/ out JVT cut
    for (auto jet : *jetsJVT)
      if (jet->pt() > 30 * HG::GeV) {
        m_weight *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { m_weight *= jet->auxdata<float>("SF_fjvt"); }
      }

    // Are photons forward or central?
    bool isFwd = (fabs(photons->at(0)->eta()) > 0.95 || fabs(photons->at(1)->eta()) > 0.95);

    // ggH+0j, split Cen/Fwd regions
    if (var::N_j_30() == 0 &&  isFwd) { return M17_ggH_0J_Fwd; }

    if (var::N_j_30() == 0 && !isFwd) { return M17_ggH_0J_Cen; }

    int pTbin = -999;

    if (var::pT_yy() <=  60 * HG::GeV) { pTbin = 0; }
    else if (var::pT_yy() <= 120 * HG::GeV) { pTbin = 1; }
    else if (var::pT_yy() <= 200 * HG::GeV) { pTbin = 2; }
    else                                    { pTbin = 3; }

    // >= 1 Jets
    if ((var::N_j_30() == 1) && (pTbin == 3)) { return M17_ggH_1J_BSM; }

    if ((var::N_j_30() == 1) && (pTbin == 2)) { return M17_ggH_1J_HIGH; }

    if ((var::N_j_30() == 1) && (pTbin == 1)) { return M17_ggH_1J_MED; }

    if ((var::N_j_30() == 1) && (pTbin == 0)) { return M17_ggH_1J_LOW; }

    // >= 2 Jets
    if ((var::N_j_30() >= 2) && (pTbin == 3)) { return M17_ggH_2J_BSM; }

    if ((var::N_j_30() >= 2) && (pTbin == 2)) { return M17_ggH_2J_HIGH; }

    if ((var::N_j_30() >= 2) && (pTbin == 1)) { return M17_ggH_2J_MED; }

    if ((var::N_j_30() >= 2) && (pTbin == 0)) { return M17_ggH_2J_LOW; }

    Error("HGamCategoryTool::split_ggH_STXS()", "Please Investigate. Could not find proper inclusive category!");
    return 0; // Should never get here.
  }


  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   ttH Category Moriond 2017, Cut Based Selection  (jared.vasquez@yale.edu)
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_ttH_Moriond(
    const xAOD::PhotonContainer *   /*photons*/,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer * /*met*/)
  {
    m_weight = 1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jetsCen25(0), n_jetsCen35(0), n_jetsFwd25(0);
    int n_tags25_70(0), n_tags35_70(0);

    double bweight25_70(1.0), bweight35_70(1.0);

    // count only central jets s.t. |eta| < 2.5
    for (auto jet : *jets) {
      if (fabs(jet->eta()) > 2.5) { n_jetsFwd25++; continue; }

      // Count 25 GeV Jets and Tags
      n_jetsCen25++;
      bweight25_70 *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_70");

      if (jet->auxdata<char>("DL1r_FixedCutBEff_70")) { n_tags25_70++; }

      // Count 35 GeV Jets and Tags
      if (jet->pt() >= 35.e3) {
        n_jetsCen35++;
        bweight35_70 *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_70");

        if (jet->auxdata<char>("DL1r_FixedCutBEff_70")) { n_tags35_70++; }
      }
    }

    // use other jet collection for JVT weights
    double alljetWeight25(1.0), jetWeight25(1.0), jetWeight35(1.0);

    for (auto jet : *jetsJVT) {
      alljetWeight25  *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { alljetWeight25 *= jet->auxdata<float>("SF_fjvt"); }

      if (fabs(jet->eta()) > 2.5) { continue; }

      jetWeight25  *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { jetWeight25 *= jet->auxdata<float>("SF_fjvt"); }

      if (jet->pt() >= 35 * HG::GeV) {
        jetWeight35  *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { jetWeight35 *= jet->auxdata<float>("SF_fjvt"); }
      }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {

      double mll = -999.; // require OS dileptons in future?

      if (n_muons >= 2) { mll = (muons->at(0)->p4() + muons->at(1)->p4()).M() * HG::invGeV; }

      if (n_electrons >= 2) { mll = (electrons->at(0)->p4() + electrons->at(1)->p4()).M() * HG::invGeV; }

      if (fabs(mll - 91) < 10) { return 0; } // fail selection

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (alljetWeight25 * bweight25_70); // jvt & b-tag SFs

      bool passPreselTH = ((n_leptons == 1) && (n_tags25_70 >= 1));

      if (passPreselTH && (n_jetsCen25 <= 3) && (n_jetsFwd25 == 0))  { return 9; }
      else if (passPreselTH && (n_jetsCen25 <= 4) && (n_jetsFwd25 >= 1))  { return 8; }
      else if ((n_jetsCen25 >= 2) && (n_tags25_70 >= 1))  { return 7; }

      // Hadronic Channel Selection
    } else {

      if ((n_jetsCen35 == 4) && (n_tags35_70 == 1)) {
        m_weight *= (jetWeight35 * bweight35_70);
        return 6;

      } else if ((n_jetsCen35 == 4) && (n_tags35_70 >= 2)) {
        m_weight *= (jetWeight35 * bweight35_70);
        return 5;

      } else if ((n_jetsCen25 == 5) && (n_tags25_70 == 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 4;

      } else if ((n_jetsCen25 == 5) && (n_tags25_70 >= 2)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 3;

      } else if ((n_jetsCen25 >= 6) && (n_tags25_70 == 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 2;

      } else if ((n_jetsCen25 >= 6) && (n_tags25_70 >= 2)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 1;
      }
    }

    // should only reach here if failed selection
    return 0;
  }


  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   ttH Category Moriond 2017, BDT Based Selection  (jared.vasquez@yale.edu)
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_ttHBDT_Moriond(
    const xAOD::PhotonContainer *    /*photons*/,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer * /*met*/)
  {
    m_weight = 1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jetsCen25(0), n_jetsCen30(0), n_jetsFwd25(0);
    int n_tags25_70(0), n_tags30_70(0);

    double bweight25_70(1.0), bweight30_70(1.0);

    // count only central jets s.t. |eta| < 2.5
    for (auto jet : *jets) {
      if (fabs(jet->eta()) > 2.5) { n_jetsFwd25++; continue; }

      n_jetsCen25++;
      bweight25_70 *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_70");

      if (jet->auxdata<char>("DL1r_FixedCutBEff_70")) { n_tags25_70++; }

      // Count 30 GeV Jets and Tags
      if (jet->pt() >= 30 * HG::GeV) {
        n_jetsCen30++;
        bweight30_70 *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_70");

        if (jet->auxdata<char>("DL1r_FixedCutBEff_70")) { n_tags30_70++; }
      }
    }

    // use other jet collection for JVT weights
    double jetWeight25(1.0), jetWeight30(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { jetWeight25 *= jet->auxdata<float>("SF_fjvt"); }

      if (jet->pt() >= 30 * HG::GeV) {
        jetWeight30 *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { jetWeight30 *= jet->auxdata<float>("SF_fjvt"); }
      }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {

      double mll = -999.; // require OS dileptons in future?

      if (n_muons >= 2) { mll = (muons->at(0)->p4() + muons->at(1)->p4()).M() * HG::invGeV; }

      if (n_electrons >= 2) { mll = (electrons->at(0)->p4() + electrons->at(1)->p4()).M() * HG::invGeV; }

      if (fabs(mll - 91) < 10) { return 0; } // fail selection

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (jetWeight25 * bweight25_70); // jvt & b-tag SFs

      bool passPreselTH = ((n_leptons == 1) && (n_tags25_70 >= 1));

      if (passPreselTH && (n_jetsCen25 <= 3) && (n_jetsFwd25 == 0))  { return 9; }
      else if (passPreselTH && (n_jetsCen25 <= 4) && (n_jetsFwd25 >= 1))  { return 8; }
      else if ((n_jetsCen25 >= 2) && (n_tags25_70 >= 1))  { return 7; }

      // Hadronic Channel Selection
    } else {

      if (n_leptons > 0) { return 0; } // dumb check

      // Get some features not in HGamVariables
      t_N_bjet30    = n_tags30_70;

      bool preselBDT = ((n_tags30_70 > 0) && (var::N_j_30() >= 3));
      float MVA_weight_ttHhad = (!preselBDT) ? -1.0 : getMVAWeight(m_reader_ttHhad);

      // BDT hadronic selection
      if (preselBDT && (MVA_weight_ttHhad > 0.92)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 6;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.83)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 5;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.79)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 4;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.52)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 3;

        // tH hadronic selection
      } else if ((n_jetsCen25 == 4) && (n_tags25_70 == 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 2;
      } else if ((n_jetsCen25 == 4) && (n_tags25_70 >= 2)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 1;
      }

    }

    // should only reach here if failed selection
    return 0;
  }


  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   ttH Category Moriond 2017, BDT Based Selection  (jared.vasquez@yale.edu)
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_ttHBDTlep_Moriond(
    const xAOD::PhotonContainer *    /*photons*/,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer * /*met*/)
  {
    m_weight = 1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jetsCen25(0), n_jetsCen30(0), n_jetsFwd25(0);
    int n_tags25_70(0), n_tags30_70(0);

    double bweight25_70(1.0), bweight30_70(1.0);

    // count only central jets s.t. |eta| < 2.5
    for (auto jet : *jets) {
      if (fabs(jet->eta()) > 2.5) { n_jetsFwd25++; continue; }

      n_jetsCen25++;
      bweight25_70 *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_70");

      if (jet->auxdata<char>("DL1r_FixedCutBEff_70")) { n_tags25_70++; }

      // Count 30 GeV Jets and Tags
      if (jet->pt() >= 30 * HG::GeV) {
        n_jetsCen30++;
        bweight30_70 *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_70");

        if (jet->auxdata<char>("DL1r_FixedCutBEff_70")) { n_tags30_70++; }
      }
    }

    // use other jet collection for JVT weights
    double jetWeight25(1.0), jetWeight30(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { jetWeight25 *= jet->auxdata<float>("SF_fjvt"); }

      if (jet->pt() >= 30 * HG::GeV) {
        jetWeight30 *= jet->auxdata<float>("SF_jvt");

        if (m_applyFJvtSF) { jetWeight30 *= jet->auxdata<float>("SF_fjvt"); }
      }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs

      // Get some features not in HGamVariables
      t_N_bjet30    = n_tags30_70;
      t_pTlepMET    = var::pTlepMET();
      t_massT       = var::massTrans();

      bool preselBDT = (n_tags30_70 >= 1);
      float MVA_weight_ttHlep = (!preselBDT) ? -1.0 : getMVAWeight(m_reader_ttHlep);

      if (preselBDT && (MVA_weight_ttHlep > 0.83)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 9;
      } else if (preselBDT && (MVA_weight_ttHlep > 0.30)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 8;
      }

      bool passPreselTH = ((n_leptons == 1) && (n_tags25_70 >= 1));

      if (passPreselTH && (n_jetsCen25 <= 3) && (n_jetsFwd25 == 0)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 7;
      } else if (passPreselTH && (n_jetsCen25 <= 4) && (n_jetsFwd25 >= 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 6;
      }

      // Hadronic Channel Selection
    } else {

      if (n_leptons > 0) { return 0; } // dumb check

      // Get some features not in HGamVariables
      t_N_bjet30    = n_tags30_70;

      bool preselBDT = ((n_tags30_70 > 0) && (var::N_j_30() >= 3));
      float MVA_weight_ttHhad = (!preselBDT) ? -1.0 : getMVAWeight(m_reader_ttHhad);

      // BDT hadronic selection
      if (preselBDT && (MVA_weight_ttHhad > 0.92)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 5;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.83)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 4;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.79)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 3;
      } else if (preselBDT && (MVA_weight_ttHhad > 0.52)) {
        m_weight *= (jetWeight30 * bweight30_70);
        return 2;

        // tH hadronic selection
      } else if ((n_jetsCen25 == 4) && (n_tags25_70 >= 1)) {
        m_weight *= (jetWeight25 * bweight25_70);
        return 1;
      }

    }

    // should only reach here if failed selection
    return 0;
  }


  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   XGBoost ttH
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_XGBoost_ttH(
    const xAOD::PhotonContainer    *photons,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer *met)
  {
    m_weight = 1.0;
    m_score = -1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jets25(0);
    int n_tags25_77(0);

    double bweight(1.0);

    for (auto jet : *jets) {
      n_jets25++;

      if (jet->auxdata<char>("DL1r_FixedCutBEff_77")) { n_tags25_77++; }

      bweight *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_77");
    }

    // use other jet collection for JVT weights
    double jetWeight25(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { jetWeight25 *= jet->auxdata<float>("SF_fjvt"); }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {
      if (!(n_tags25_77 > 0)) { return 0; }

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (jetWeight25 * bweight); // jvt & b-tag SFs

      float *vars = make_XGBoost_DMatrix_ttHlep(photons, electrons, muons, jets, met); // build DMatrix
      m_score = get_XGBoost_Weight(m_xgboost_ttHlep, vars, 45); // get score

      if (m_score < 0.013)  { return 9; }
      else if (m_score < 0.058)  { return 8; }
      else if (m_score < 0.295)  { return 7; }

      // Hadronic Channel Selection
    } else {
      if (!((n_tags25_77 > 0) && (n_jets25 >= 3))) { return 0; }   // preselection

      float *vars = make_XGBoost_DMatrix_ttHhad(photons, jets, met); // build DMatrix
      m_score = get_XGBoost_Weight(m_xgboost_ttHhad, vars, 45); // get score

      if (m_score < 0.004) {
        m_weight *= (jetWeight25 * bweight);
        return 6;
      } else if (m_score < 0.009) {
        m_weight *= (jetWeight25 * bweight);
        return 5;
      } else if (m_score < 0.029) {
        m_weight *= (jetWeight25 * bweight);
        return 4;
      } else if (m_score < 0.089) {
        m_weight *= (jetWeight25 * bweight);
        return 3;
      }
    }

    // should only reach here if failed selection
    return 0;
  }

  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   XGBoost ttH CP
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_XGBoost_ttHCP(
    const xAOD::PhotonContainer    *photons,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer *met)
  {

    m_weight = 1.0;
    m_score = -1.0;

    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jets25(0);
    int n_tags25_77(0);

    double bweight(1.0);

    std::vector<int> v;
    v = var::idx_jets_recotop1();
    v = var::idx_jets_recotop2();

    for (auto jet : *jets) {
      n_jets25++;

      if (jet->auxdata<char>("DL1r_FixedCutBEff_77")) { n_tags25_77++; }

      bweight *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_77");
    }

    // use other jet collection for JVT weights
    double jetWeight25(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { jetWeight25 *= jet->auxdata<float>("SF_fjvt"); }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {
      if (!(n_tags25_77 > 0)) { return 0; }

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (jetWeight25 * bweight); // jvt & b-tag SFs

      double m_score_a = var::score_ttH();

      float *vars = make_XGBoost_DMatrix_ttHCPlep(photons, jets, met); // build DMatrix
      double m_score_b = get_XGBoost_Weight(m_xgboost_ttHCPlep, vars, 20); // get score

      m_score = m_score_b;

      // Boundaries
      double a1 = 0.012, a2 = 0.085, a3 = 0.748;
      double b11 = 0.91;
      double b21 = 0.82, b22 = 0.93;
      double b31 = 0.72, b32 = 0.86;
      int a_index = 0;

      if (m_score_a < a1) { a_index = 1; }
      else if (m_score_a < a2) { a_index = 2; }
      else if (m_score_a < a3) { a_index = 3; }

      if (a_index == 1) {
        if (m_score_b < b11) { return 13; }
        else { return 14; }
      } else if (a_index == 2) {
        if (m_score_b < b21) { return 15; }
        else if (m_score_b < b22) { return 16; }
        else { return 17; }
      } else if (a_index == 3) {
        if (m_score_b < b31) { return 18; }
        else if (m_score_b < b32) { return 19; }
        else { return 20; }
      } else { return 0; }
    }
    // Hadronic Channel Selection
    else {
      if (!((n_tags25_77 > 0) && (n_jets25 >= 3))) { return 0; }   // preselection

      m_weight *= (jetWeight25 * bweight);

      double m_score_a = var::score_ttH();
      float *vars = make_XGBoost_DMatrix_ttHCPhad(photons, jets, met); // build DMatrix
      double m_score_b = get_XGBoost_Weight(m_xgboost_ttHCPhad, vars, 20); // get score
      m_score = m_score_b;

      // Boundaries
      double a1 = 0.005, a2 = 0.009, a3 = 0.019, a4 = 0.091;
      double b11 = 0.90, b12 = 0.97;
      double b21 = 0.88, b22 = 0.96;
      double b31 = 0.84, b32 = 0.96;
      double b41 = 0.61, b42 = 0.86;
      int a_index = 0;

      if (m_score_a < a1) { a_index = 1; }
      else if (m_score_a < a2) { a_index = 2; }
      else if (m_score_a < a3) { a_index = 3; }
      else if (m_score_a < a4) { a_index = 4; }

      if (a_index == 1) {
        if (m_score_b < b11) { return 1; }
        else if (m_score_b < b12) { return 2; }
        else {return 3;}
      } else if (a_index == 2) {
        if (m_score_b < b21) { return 4; }
        else if (m_score_b < b22) { return 5; }
        else { return 6; }
      } else if (a_index == 3) {
        if (m_score_b < b31) { return 7; }
        else if (m_score_b < b32) { return 8; }
        else { return 9; }
      } else if (a_index == 4) {
        if (m_score_b < b41) { return 10; }
        else if (m_score_b < b42) { return 11; }
        else { return 12; }
      } else { return 0; }
    }

    // should only reach here if failed selection
    return 0;
  }

  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   XGBoost ttH for Moriond2020
  // ---------------------------------------------------------------------------------
  int HGamCategoryTool::Passes_XGBoost_ttH_Moriond2020(
    const xAOD::PhotonContainer    *photons,
    const xAOD::ElectronContainer  *electrons,
    const xAOD::MuonContainer      *muons,
    const xAOD::JetContainer       *jets,
    const xAOD::JetContainer       *jetsJVT,
    const xAOD::MissingETContainer *met)
  {

    m_weight = 1.0;
    m_score = -1.0;
    int n_electrons = electrons->size();
    int n_muons = muons->size();
    int n_leptons = (n_electrons + n_muons);

    int n_jets25(0);
    int n_tags25_77(0);

    double bweight(1.0);

    for (auto jet : *jets) {
      n_jets25++;

      if (jet->auxdata<char>("DL1r_FixedCutBEff_77")) { n_tags25_77++; }

      bweight *= jet->auxdata<float>("SF_DL1r_Continuous");
    }

    // use other jet collection for JVT weights
    double jetWeight25(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { jetWeight25 *= jet->auxdata<float>("SF_fjvt"); }
    }

    // Leptonic Channel Selection
    if (n_leptons >= 1) {
      if (!(n_tags25_77 > 0)) { return 0; }

      m_weight *= getLeptonSFs(electrons, muons);   // lepton SFs
      m_weight *= (jetWeight25 * bweight); // jvt & b-tag SFs

      float *vars_multi = make_XGBoost_DMatrix_ttHlep_multiclass(photons, jets, met); // build DMatrix
      const float *score_multi = get_XGBoost_Weight_multiclass(m_xgboost_htoplep_Moriond2020, vars_multi, 18); // get htop score

      float score_multi_bkg = score_multi[0];
      float score_multi_tH = score_multi[1];
      float score_multi_ttH = score_multi[2];
      m_score = score_multi_bkg;

      // Background class
      if (score_multi_bkg > score_multi_tH &&
          score_multi_bkg > score_multi_ttH) {
        return 0;
      }

      if (var::pT_yy() >= 200 * HG::GeV) {
        return 18;
      }

      float *vars = make_XGBoost_DMatrix_ttHlep_Moriond2020(photons, electrons, muons, jets, met); // build DMatrix

      // ttH class
      if (score_multi_ttH > score_multi_tH) {
        float score_ttH = get_XGBoost_Weight(m_xgboost_ttHlep_Moriond2020, vars, 55); // get ttH score

        if (var::pT_yy() >= 120 * HG::GeV && var::pT_yy() < 200 * HG::GeV && score_ttH < 0.578) {
          return 17;
        } else if (var::pT_yy() >= 60 * HG::GeV && var::pT_yy() < 120 * HG::GeV && score_ttH < 0.312) {
          return 16;
        } else if (var::pT_yy() >= 60 * HG::GeV && var::pT_yy() < 120 * HG::GeV && score_ttH < 0.793) {
          return 15;
        } else if (var::pT_yy() >= 0 * HG::GeV && var::pT_yy() < 60 * HG::GeV && score_ttH < 0.465) {
          return 14;
        } else if (var::pT_yy() >= 0 * HG::GeV && var::pT_yy() < 60 * HG::GeV && score_ttH < 0.903) {
          return 13;
        }
      }
      // tH class
      else {
        float score_tH = get_XGBoost_Weight(m_xgboost_tHlep_Moriond2020, vars, 55); // get tH score

        if (score_tH < 0.319) {
          return 12;
        } else if (score_tH < 0.581) {
          return 11;
        }
      }

      // Hadronic Channel Selection
    } else {
      if (!((n_tags25_77 > 0) && (n_jets25 >= 3))) { return 0; }   // preselection

      m_weight *= (jetWeight25 * bweight);

      float *vars_multi = make_XGBoost_DMatrix_ttHhad_multiclass(photons, jets, met); // build DMatrix
      const float *score_multi = get_XGBoost_Weight_multiclass(m_xgboost_htophad_Moriond2020, vars_multi, 18); // get htop score

      float score_multi_bkg = score_multi[0];
      float score_multi_tH = score_multi[1];
      float score_multi_ttH = score_multi[2];
      m_score = score_multi_bkg;

      // Background class
      if (score_multi_bkg > score_multi_tH &&
          score_multi_bkg > score_multi_ttH) {
        return 0;
      }

      float *vars = make_XGBoost_DMatrix_ttHhad_Moriond2020(photons, jets, met); // build DMatrix

      if (score_multi_ttH > score_multi_tH) {
        float score_ttH = get_XGBoost_Weight(m_xgboost_ttHhad_Moriond2020, vars, 55); // get ttH score

        if (var::pT_yy() >= 300 * HG::GeV && score_ttH < 0.052) {
          return 10;
        } else if (var::pT_yy() >= 200 * HG::GeV && var::pT_yy() < 300 * HG::GeV && score_ttH < 0.041) {
          return 9;
        } else if (var::pT_yy() >= 120 * HG::GeV && var::pT_yy() < 200 * HG::GeV && score_ttH < 0.019) {
          return 8;
        } else if (var::pT_yy() >= 120 * HG::GeV && var::pT_yy() < 200 * HG::GeV && score_ttH < 0.062) {
          return 7;
        } else if (var::pT_yy() >= 60 * HG::GeV && var::pT_yy() < 120 * HG::GeV && score_ttH < 0.030) {
          return 6;
        } else if (var::pT_yy() >= 60 * HG::GeV && var::pT_yy() < 120 * HG::GeV && score_ttH < 0.181) {
          return 5;
        } else if (var::pT_yy() >= 0 * HG::GeV && var::pT_yy() < 60 * HG::GeV && score_ttH < 0.034) {
          return 4;
        } else if (var::pT_yy() >= 0 * HG::GeV && var::pT_yy() < 60 * HG::GeV && score_ttH < 0.188) {
          return 3;
        }
      } else {
        float score_tH = get_XGBoost_Weight(m_xgboost_tHhad_Moriond2020, vars, 55); // get tH score

        if (score_tH < 0.025) {
          return 2;
        } else if (score_tH < 0.139) {
          return 1;
        }
      }
    }

    // should only reach here if failed selection
    return 0;
  }

  //___________________________________________________________________________________________________________
  int HGamCategoryTool::Passes_MonoH_2var(const xAOD::PhotonContainer *photons,
                                          const xAOD::ElectronContainer  *electrons,
                                          const xAOD::MuonContainer      *muons,
                                          const xAOD::JetContainer       * /*jets*/,
                                          const xAOD::JetContainer       *jetsJVT,
                                          const xAOD::MissingETContainer *met)
  {
    m_weight = 1.0;
    m_score = -1.0;

    int m_Nelectrons = electrons->size();
    int m_Nmuons = muons->size();

    if ((m_Nelectrons + m_Nmuons) != 0) { return 0; }

    float TST_met = -99;//, TST_sumet = -99;

    if ((*met)["TST"] != nullptr) {
      TST_met   = (*met)["TST"]->met();
      //TST_sumet = (*met)["TST"]->sumet();
    }

    if (TST_met < 90 * HG::GeV) { return 0; }

    float hardVertexTST_met = -99;

    if ((*met)["hardVertexTST"] != nullptr) {
      hardVertexTST_met   = (*met)["hardVertexTST"]->met();
    }

    if ((TST_met - hardVertexTST_met) > 30 * HG::GeV) { return 0; }

    // use other jet collection for JVT weights
    double jetWeight25(1.0);

    for (auto jet : *jetsJVT) {
      jetWeight25   *= jet->auxdata<float>("SF_jvt");

      if (m_applyFJvtSF) { jetWeight25 *= jet->auxdata<float>("SF_fjvt"); }
    }

    m_weight *= jetWeight25; // jvt SFs

    float *vars = make_XGBoost_DMatrix_MonoH_2var(photons, met); // build DMatrix
    float score_MonoH = get_XGBoost_Weight(m_xgboost_MonoH_2var, vars, 5); // get MonoH score
    m_score = score_MonoH;

    if (TST_met >= 150 * HG::GeV && score_MonoH < 0.050) {
      return 6;
    } else if (TST_met >= 150 * HG::GeV && score_MonoH < 0.306) {
      return 5;
    } else if (TST_met >= 150 * HG::GeV) {
      return 4;
    } else if (TST_met >= 90 * HG::GeV && TST_met < 150 * HG::GeV && score_MonoH < 0.136) {
      return 3;
    } else if (TST_met >= 90 * HG::GeV && TST_met < 150 * HG::GeV && score_MonoH < 0.614) {
      return 2;
    } else if (TST_met >= 90 * HG::GeV && TST_met < 150 * HG::GeV) {
      return 1;
    }

    // should only reach here if failed selection
    return 0;
  }


  //___________________________________________________________________________________________________________
  double HGamCategoryTool::getLeptonSFs(const xAOD::ElectronContainer *electrons, const xAOD::MuonContainer *muons)
  {
    double lepSF = 1.0;

    for (auto el : *electrons) { lepSF *= el->auxdata< float >("scaleFactor"); }

    for (auto mu : *muons)     { lepSF *= mu->auxdata< float >("scaleFactor"); }

    return lepSF;
  }


  //___________________________________________________________________________________________________________
  void HGamCategoryTool::resetReader()
  {
    t_pTt_yyGeV     = var::pTt_yy() * HG::invGeV; // VH had uses GeV
    t_m_jjGeV       = var::m_jj() * HG::invGeV; // VH had uses GeV
    t_pTt_yy        = var::pTt_yy();
    t_m_jj          = var::m_jj();
    t_cosTS_yy_jj   = var::cosTS_yyjj();
    t_dEta_jj       = var::Deta_j_j();
    t_dPhi_yy_jj    = var::Dphi_yy_jj();
    t_Zepp          = fabs(var::Zepp());
    //t_Drmin_y_j     = var::DRmin_y_j();
    t_Drmin_y_j2    = var::DRmin_y_j_2();
    t_Dy_yy_jj      = var::Dy_yy_jj();
    t_N_j30         = var::N_j_30();
    t_N_j_central30 = var::N_j_central30();
    t_m_alljet_30   = var::m_alljet_30();
    t_HT_30         = var::HT_30();
  }


  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2017 Categories ( Cut Based ttH )
  // -----------------------------------------------
  std::pair<int, float> HGamCategoryTool::getCategoryAndWeightMoriond2017(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) { return std::make_pair(-1, 0.0); }

    if (photons->size() < 2)     { return std::make_pair(M17_FailDiphoton, 0.0); }

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_ttH_Moriond(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > 0)               { return std::make_pair(M17_ttH + CtthCat - 1, m_weight); }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                { return std::make_pair(M17_VHdilep_HIGH, m_weight); }

    if (CVH2l == 1)                { return std::make_pair(M17_VHdilep_LOW,  m_weight); }

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               { return std::make_pair(M17_VHlep_HIGH,   m_weight); }

    if (CVHlep == 1)               { return std::make_pair(M17_VHlep_LOW,    m_weight); }

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               { return std::make_pair(M17_VHMET_BSM,    m_weight); }

    if (CVHMET == 2)               { return std::make_pair(M17_VHMET_HIGH,   m_weight); }

    if (CVHMET == 1)               { return std::make_pair(M17_VHMET_LOW,    m_weight); }

    //   qq->H 1 category
    // --------------------------------
    if (Passes_qqH_BSM(jetsJVT)) { return std::make_pair(M17_qqH_BSM,      m_weight); }

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    { return std::make_pair(M17_VHhad_tight,  m_weight); }

      if (wMVA_VH >  0.18)    { return std::make_pair(M17_VHhad_loose,  m_weight); }
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    { return std::make_pair(M17_VBF_HjjHIGH_tight, m_weight); }

      if (wMVA_VBF > -0.50)    { return std::make_pair(M17_VBF_HjjHIGH_loose, m_weight); }
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    { return std::make_pair(M17_VBF_HjjLOW_tight,  m_weight); }

      if (wMVA_VBF > -0.39)    { return std::make_pair(M17_VBF_HjjLOW_loose,  m_weight); }
    }

    //   gg->H Categories
    // --------------------------------
    return std::make_pair(split_ggH_STXS(photons, jetsJVT), m_weight);
  }



  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2017 Categories ( BDT Based ttH )
  // -----------------------------------------------
  std::pair<int, float> HGamCategoryTool::getCategoryAndWeightMoriond2017_ttHBDT_qqH2jet(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) { return std::make_pair(-1, 0.0); }

    if (photons->size() < 2)     { return std::make_pair(M17_FailDiphoton, 0.0); }

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_ttHBDT_Moriond(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > 0)               { return std::make_pair(M17_ttH + CtthCat - 1, m_weight); }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                { return std::make_pair(M17_VHdilep_HIGH, m_weight); }

    if (CVH2l == 1)                { return std::make_pair(M17_VHdilep_LOW,  m_weight); }

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               { return std::make_pair(M17_VHlep_HIGH,   m_weight); }

    if (CVHlep == 1)               { return std::make_pair(M17_VHlep_LOW,    m_weight); }

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               { return std::make_pair(M17_VHMET_BSM,    m_weight); }

    if (CVHMET == 2)               { return std::make_pair(M17_VHMET_HIGH,   m_weight); }

    if (CVHMET == 1)               { return std::make_pair(M17_VHMET_LOW,    m_weight); }

    //   qq->H 1 category
    // --------------------------------
    if (Passes_qqH_BSM_2jet(jetsJVT)) { return std::make_pair(M17_qqH_BSM,      m_weight); }

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    { return std::make_pair(M17_VHhad_tight,  m_weight); }

      if (wMVA_VH >  0.18)    { return std::make_pair(M17_VHhad_loose,  m_weight); }
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    { return std::make_pair(M17_VBF_HjjHIGH_tight, m_weight); }

      if (wMVA_VBF > -0.50)    { return std::make_pair(M17_VBF_HjjHIGH_loose, m_weight); }
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    { return std::make_pair(M17_VBF_HjjLOW_tight,  m_weight); }

      if (wMVA_VBF > -0.39)    { return std::make_pair(M17_VBF_HjjLOW_loose,  m_weight); }
    }


    //   gg->H Categories
    // --------------------------------
    return std::make_pair(split_ggH_STXS(photons, jetsJVT), m_weight);
  }



  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2017 Categories ( BDT Based ttH )
  // -----------------------------------------------
  std::pair<int, float> HGamCategoryTool::getCategoryAndWeightMoriond2017_ttHBDT(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) { return std::make_pair(-1, 0.0); }

    if (photons->size() < 2)     { return std::make_pair(M17_FailDiphoton, 0.0); }

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_ttHBDT_Moriond(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > 0)               { return std::make_pair(M17_ttH + CtthCat - 1, m_weight); }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                { return std::make_pair(M17_VHdilep_HIGH, m_weight); }

    if (CVH2l == 1)                { return std::make_pair(M17_VHdilep_LOW,  m_weight); }

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               { return std::make_pair(M17_VHlep_HIGH,   m_weight); }

    if (CVHlep == 1)               { return std::make_pair(M17_VHlep_LOW,    m_weight); }

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               { return std::make_pair(M17_VHMET_BSM,    m_weight); }

    if (CVHMET == 2)               { return std::make_pair(M17_VHMET_HIGH,   m_weight); }

    if (CVHMET == 1)               { return std::make_pair(M17_VHMET_LOW,    m_weight); }

    //   qq->H 1 category
    // --------------------------------
    if (Passes_qqH_BSM(jetsJVT)) { return std::make_pair(M17_qqH_BSM,      m_weight); }

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    { return std::make_pair(M17_VHhad_tight,  m_weight); }

      if (wMVA_VH >  0.18)    { return std::make_pair(M17_VHhad_loose,  m_weight); }
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    { return std::make_pair(M17_VBF_HjjHIGH_tight, m_weight); }

      if (wMVA_VBF > -0.50)    { return std::make_pair(M17_VBF_HjjHIGH_loose, m_weight); }
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    { return std::make_pair(M17_VBF_HjjLOW_tight,  m_weight); }

      if (wMVA_VBF > -0.39)    { return std::make_pair(M17_VBF_HjjLOW_loose,  m_weight); }
    }


    //   gg->H Categories
    // --------------------------------
    return std::make_pair(split_ggH_STXS(photons, jetsJVT), m_weight);
  }



  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2017 Categories ( BDT Based ttH )
  // -----------------------------------------------
  std::pair<int, float> HGamCategoryTool::getCategoryAndWeightMoriond2017_ttHBDTlep(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) { return std::make_pair(-1, 0.0); }

    if (photons->size() < 2)     { return std::make_pair(M17_FailDiphoton, 0.0); }

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_ttHBDTlep_Moriond(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > 0)               { return std::make_pair(M17_ttH + CtthCat - 1, m_weight); }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                { return std::make_pair(M17_VHdilep_HIGH, m_weight); }

    if (CVH2l == 1)                { return std::make_pair(M17_VHdilep_LOW,  m_weight); }

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               { return std::make_pair(M17_VHlep_HIGH,   m_weight); }

    if (CVHlep == 1)               { return std::make_pair(M17_VHlep_LOW,    m_weight); }

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               { return std::make_pair(M17_VHMET_BSM,    m_weight); }

    if (CVHMET == 2)               { return std::make_pair(M17_VHMET_HIGH,   m_weight); }

    if (CVHMET == 1)               { return std::make_pair(M17_VHMET_LOW,    m_weight); }

    //   qq->H 1 category
    // --------------------------------
    if (Passes_qqH_BSM(jetsJVT)) { return std::make_pair(M17_qqH_BSM,      m_weight); }

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    { return std::make_pair(M17_VHhad_tight,  m_weight); }

      if (wMVA_VH >  0.18)    { return std::make_pair(M17_VHhad_loose,  m_weight); }
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    { return std::make_pair(M17_VBF_HjjHIGH_tight, m_weight); }

      if (wMVA_VBF > -0.50)    { return std::make_pair(M17_VBF_HjjHIGH_loose, m_weight); }
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    { return std::make_pair(M17_VBF_HjjLOW_tight,  m_weight); }

      if (wMVA_VBF > -0.39)    { return std::make_pair(M17_VBF_HjjLOW_loose,  m_weight); }
    }


    //   gg->H Categories
    // --------------------------------
    return std::make_pair(split_ggH_STXS(photons, jetsJVT), m_weight);
  }

  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  XGBoost ttH
  // -----------------------------------------------
  float *HGamCategoryTool::getCategoryAndWeightXGBoost_ttH(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    static float results[3] = {0, 0, 0};

    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) {results[0] = -1; return results;}

    if (photons->size() < 2)     {results[0] = M17_FailDiphoton; return results;}

    resetReader(); // Preps variables for TMVA reader

    //    ttH Categories
    // --------------------------------
    int CtthCat = Passes_XGBoost_ttH(photons, electrons, muons, jets, jetsJVT, met);

    //std::cout << m_score << std::endl;
    if (CtthCat > 0)               {results[0] = M17_ttH + CtthCat - 1; results[1] = m_weight; results[2] = m_score; return results;}

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2017(photons, electrons, muons);

    if (CVH2l >= 2)                {results[0] = M17_VHdilep_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVH2l == 1)                {results[0] = M17_VHdilep_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    int CVHlep = Passes_VH_leptonic_Moriond2017(photons, electrons, muons, met);

    if (CVHlep >= 2)               {results[0] = M17_VHlep_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHlep == 1)               {results[0] = M17_VHlep_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    int CVHMET = Passes_VH_MET_Moriond2017(photons, met);

    if (CVHMET >= 3)               {results[0] = M17_VHMET_BSM;  results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 2)               {results[0] = M17_VHMET_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 1)               {results[0] = M17_VHMET_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    //   qq->H 1 category
    // --------------------------------
    // if (Passes_qqH_BSM(jetsJVT)) {results[0] = M17_qqH_BSM; results[1] = m_weight; results[2] = m_score; return results;}
    if (Passes_qqH_BSM_2jet(jetsJVT)) {results[0] = M17_qqH_BSM; results[1] = m_weight; results[2] = m_score; return results;}

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2017(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    {results[0] = M17_VHhad_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VH >  0.18)    {results[0] = M17_VHhad_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2017(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    {results[0] = M17_VBF_HjjHIGH_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VBF > -0.50)    {results[0] = M17_VBF_HjjHIGH_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    {results[0] = M17_VBF_HjjLOW_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VBF > -0.39)    {results[0] = M17_VBF_HjjLOW_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }


    //   gg->H Categories
    // --------------------------------
    results[0] = split_ggH_STXS(photons, jetsJVT);
    results[1] = m_weight;
    results[2] = m_score;
    return results;
  }

  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  XGBoost ttH + top reco
  // -----------------------------------------------
  float *HGamCategoryTool::getCategoryAndWeightXGBoost_ttHCP(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    static float results[3] = {0, 0, 0};

    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) {results[0] = -1; return results;}

    if (photons->size() < 2)     {results[0] = M17_FailDiphoton; return results;}

    resetReader(); // Preps variables for TMVA reader

    int CtthCat = Passes_XGBoost_ttHCP(photons, electrons, muons, jets, jetsJVT, met);

    if (CtthCat > -1) {results[0] = CtthCat; results[1] = m_weight; results[2] = m_score; return results; }

    return results;
  }

  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  Moriond 2020 Categories
  // -----------------------------------------------
  float *HGamCategoryTool::getCategoryAndWeightMoriond2020(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    static float results[3] = {0, 0, 0};

    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) {results[0] = -1; return results;}

    if (photons->size() < 2)     {results[0] = M20_FailDiphoton; return results;}

    resetReader(); // Preps variables for TMVA reader

    int CtthCat = Passes_XGBoost_ttH_Moriond2020(photons, electrons, muons, jets, jetsJVT, met);

    //std::cout << m_score << std::endl;
    if (CtthCat > 0) {results[0] = M20_ttH + CtthCat - 1; results[1] = m_weight; results[2] = m_score; return results; }

    //   qq->VH Categories  3+2+2
    // --------------------------------
    int CVH2l  = Passes_VH_dileptons_Moriond2020(photons, electrons, muons);

    if (CVH2l >= 3)                {results[0] = M20_VHdilep_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVH2l == 2)                {results[0] = M20_VHdilep_MED;  results[1] = m_weight; results[2] = m_score; return results;}

    if (CVH2l == 1)                {results[0] = M20_VHdilep_LOW;  results[1] = m_weight; results[2] = m_score; return results;}

    int CVHlep = Passes_VH_leptonic_Moriond2020(photons, electrons, muons, met);

    if (CVHlep >= 6)               {results[0] = M20_VHlep_BSM;  results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHlep == 5)               {results[0] = M20_VHlep_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHlep == 4)               {results[0] = M20_VHlep_MED_tight; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHlep == 3)               {results[0] = M20_VHlep_MED_loose; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHlep == 2)               {results[0] = M20_VHlep_LOW_tight; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHlep == 1)               {results[0] = M20_VHlep_LOW_loose; results[1] = m_weight; results[2] = m_score; return results;}

    int CVHMET = Passes_VH_MET_Moriond2020(photons, electrons, muons, met);

    if (CVHMET >= 4)               {results[0] = M20_VHMET_BSM;  results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 3)               {results[0] = M20_VHMET_HIGH; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 2)               {results[0] = M20_VHMET_MED_tight; results[1] = m_weight; results[2] = m_score; return results;}

    if (CVHMET == 1)               {results[0] = M20_VHMET_MED_loose; results[1] = m_weight; results[2] = m_score; return results;}


    //   qq->H 1 category
    // --------------------------------

    if (Passes_qqH_BSM_2jet_Moriond2020(jetsJVT)) { results[0] = M20_qqH_BSM; results[1] = m_weight; results[2] = m_score; return results;}

    //   VH hadronic 2 categories
    // --------------------------------
    if (Passes_VH_hadronic_Moriond2020(jetsJVT)) {
      float wMVA_VH = getMVAWeight(m_readerVH_had_Moriond2017);

      if (wMVA_VH >  0.73)    {results[0] = M20_VHhad_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VH >  0.18)    {results[0] = M20_VHhad_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    //   VBF categories 2+2
    // --------------------------------
    int CVBF = Passes_VBF_Moriond2020(jetsJVT);

    if (CVBF == 2)  { // VBF, High pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_high);

      if (wMVA_VBF >  0.48)    {results[0] = M20_VBF_HjjHIGH_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VBF > -0.50)    {results[0] = M20_VBF_HjjHIGH_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    if (CVBF == 1)  { // VBF, Low pT Hjj
      float wMVA_VBF = getMVAWeight(m_readerVBF_low);

      if (wMVA_VBF >  0.87)    {results[0] = M20_VBF_HjjLOW_tight; results[1] = m_weight; results[2] = m_score; return results;}

      if (wMVA_VBF > -0.39)    {results[0] = M20_VBF_HjjLOW_loose; results[1] = m_weight; results[2] = m_score; return results;}
    }

    //   gg->H Categories
    // --------------------------------
    results[0] = split_ggH_STXS_Moriond2020(photons, jetsJVT);
    results[1] = m_weight;
    results[2] = m_score;
    return results;
  }

  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  ICHEP 2020 Scores
  // -----------------------------------------------
  float *HGamCategoryTool::getScoreICHEP2020(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    static float results[5] = {0, 0, 0, 0, 0};

    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) {results[0] = -1; results[1] = -1; results[2] = -1; results[3] = -1; results[4] = -1; return results;}

    if (photons->size() < 2)     {results[0] = -1; results[1] = -1; results[2] = -1; results[3] = -1; results[4] = -1; return results;}

    resetReader(); // Preps variables for TMVA reader

    float *vars_ttH = make_XGBoost_DMatrix_ttH_ICHEP2020(photons, electrons, muons, jets, met); // build ttH DMatrix
    float *vars_VH = make_XGBoost_DMatrix_VH_ICHEP2020(photons, electrons, muons, met); // build VH DMatrix

    results[0] = get_XGBoost_Weight(m_xgboost_ttH_ICHEP2020, vars_ttH, 65); // get ttH score
    results[1] = get_XGBoost_Weight(m_xgboost_tHjb_ICHEP2020, vars_ttH, 65); // get tHjb score
    results[2] = get_XGBoost_Weight(m_xgboost_tWH_ICHEP2020, vars_ttH, 65); // get tWH score
    results[3] = get_XGBoost_Weight(m_xgboost_WH_ICHEP2020, vars_VH, 30); // get WH score
    results[4] = get_XGBoost_Weight(m_xgboost_ZH_ICHEP2020, vars_VH, 30); // get ZH score

    //std::cout << m_score << std::endl;

    return results;
  }

  //___________________________________________________________________________________________________________
  // -----------------------------------------------
  //  MonoH Categories (2var)
  // -----------------------------------------------
  float *HGamCategoryTool::getCategoryAndWeightMonoH_2var(
    const xAOD::PhotonContainer     *photons,
    const xAOD::ElectronContainer   *electrons,
    const xAOD::MuonContainer       *muons,
    const xAOD::JetContainer        *jets,
    const xAOD::JetContainer        *jetsJVT,
    const xAOD::MissingETContainer  *met)
  {
    static float results[3] = {0, 0, 0};

    if (not photons || not electrons || not muons || not jets || not jetsJVT || not met) {results[0] = -1; return results;}

    if (photons->size() < 2)     {results[0] = -1; return results;}

    int CMonoHCat = Passes_MonoH_2var(photons, electrons, muons, jets, jetsJVT, met);

    //std::cout << m_score << std::endl;
    if (CMonoHCat > -1) {results[0] = CMonoHCat; results[1] = m_weight; results[2] = m_score; return results; }

    return results;
  }

  // RUN1 Mass category: https://cds.cern.ch/record/1642851/files/ATL-COM-PHYS-2014-018.pdf, p.18
  int HGamCategoryTool::getMassCategoryRun1(const xAOD::PhotonContainer *photons)
  {
    double low_pTt = 70;

    if (photons != nullptr && photons->size() > 1) {
      static SG::AuxElement::ConstAccessor<float> eta_s2("eta_s2");

      double abs_etas2_y1 = fabs(eta_s2(*(*photons)[0]));
      double abs_etas2_y2 = fabs(eta_s2(*(*photons)[1]));

      int conv_y1 = (*photons)[0]->conversionType();
      int conv_y2 = (*photons)[1]->conversionType();

      double pTt_yy = var::pTt_yy() / 1000.0;

      const bool double_unconverted = (conv_y1 + conv_y2) == 0;
      const bool isGood = (abs_etas2_y1 < 0.75  && abs_etas2_y2 < 0.75);
      const bool isBad = ((abs_etas2_y1 >= 1.3 && abs_etas2_y1 <= 1.75) || (abs_etas2_y2 >= 1.3 && abs_etas2_y2 <= 1.75));

      if (double_unconverted) {
        if (isGood) {
          if (pTt_yy < low_pTt) { return 1; }
          else { return 2; }
        } else if (!isBad) {
          if (pTt_yy < low_pTt) { return 3; }
          else { return 4; }
        } else { return 5; }
      } else {
        if (isGood) {
          if (pTt_yy < low_pTt) { return 6; }
          else { return 7; }
        } else if (!isBad) {
          if (pTt_yy < low_pTt) { return 8; }
          else { return 9; }
        } else { return 10; }
      }
    } else { return -1; }
  }

  int HGamCategoryTool::getEtaMassCategory(const xAOD::PhotonContainer *photons)
  {
    if (photons->size() > 1) {
      static SG::AuxElement::ConstAccessor<float> eta_s2("eta_s2");
      double abs_etas2_y1 = fabs(eta_s2(*(*photons)[0]));
      double abs_etas2_y2 = fabs(eta_s2(*(*photons)[1]));
      const bool Barrel = (abs_etas2_y1 < 1.37 && abs_etas2_y2 < 1.37);
      const bool Endcap = ((abs_etas2_y1 > 1.52 && abs_etas2_y1 < 2.37) && (abs_etas2_y2 > 1.52 && abs_etas2_y2 < 2.37));

      if (Barrel) { return 1; }
      else if (Endcap) { return 3; }
      else { return 2; }

    } else { return -1; }
  }



  int HGamCategoryTool::getConvMassCategory(const xAOD::PhotonContainer *photons)
  {
    if (photons->size() > 1) {
      int conv_y1 = (*photons)[0]->conversionType() != 0;
      int conv_y2 = (*photons)[1]->conversionType() != 0;
      return conv_y1 + conv_y2 + 1;
    } else { return -1; }
  }



  int HGamCategoryTool::getPtMassCategory(const xAOD::PhotonContainer *photons)
  {
    double low_pT = 58.;

    if (photons->size() > 1) {
      double pT_y1 = (*photons)[0]->pt() / 1000.;
      double pT_y2 = (*photons)[1]->pt() / 1000.;

      if (pT_y1 < low_pT && pT_y2 < low_pT) { return 1; }
      else if (pT_y1 < low_pT || pT_y2 < low_pT) { return 2; }
      else { return 3; }
    } else { return -1; }
  }



  int HGamCategoryTool::getMuMassCategory(float mu)
  {
    if (mu >= 0 && mu < 16) { return 1; }
    else if (mu >= 16 && mu < 20) { return 2; }
    else if (mu >= 20 && mu < 24) { return 3; }
    else if (mu >= 24 && mu < 28) { return 4; }
    else if (mu >= 28) { return 5; }
    else { return -1; }
  }

  //float HGamCategoryTool::get_pseudoCont_score(double btagscore)
  //{
  //  if (btagscore > 0.94) { return 5.; }
  //  else if (btagscore > 0.83) { return 4.; }
  //  else if (btagscore > 0.64) { return 3.; }
  //  else if (btagscore > 0.11) { return 2.; }
  //  else if (btagscore > -1.000000) { return 1.; }
  //  else { return -9.; }
  //}

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHhad(const xAOD::PhotonContainer    *photons,
                                                       const xAOD::JetContainer       *jets,
                                                       const xAOD::MissingETContainer *met)
  {
    static float vars[45] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = photons->at(0)->p4().E() / myy;
    vars[4] = 22;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = photons->at(1)->p4().E() / myy;
    vars[9] = 22;

    // met
    vars[10] = (*met)["TST"]->met();
    vars[11] = 0;
    vars[12] = (*met)["TST"]->phi();
    vars[13] = 0;
    vars[14] = 16;

    // jets
    std::vector< std::vector<float> > js;

    for (auto j : *jets) {
      std::vector<float> ij;
      ij.push_back((float) j->p4().Pt());
      ij.push_back((float) j->p4().Eta());
      ij.push_back((float) j->p4().Phi());
      ij.push_back((float) j->p4().E());

      if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_77")) { ij.push_back(1.); }
      else { ij.push_back(0.); }

      //ij.push_back(get_pseudoCont_score(j->auxdata<double>("DL1r_discriminant")));

      js.push_back(ij);
    }

    std::sort(js.begin(), js.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    while (js.size() < 6) {
      std::vector<float> ij;

      for (int i = 0; i < 5; i++) { ij.push_back(-999.); }

      js.push_back(ij);
    }

    int i = 15;

    for (auto j : js) {
      vars[i] = j[0];
      i++;
      vars[i] = j[1];
      i++;
      vars[i] = j[2];
      i++;
      vars[i] = j[3];
      i++;
      vars[i] = j[4];
      i++;

      if (i >= 45) { break; }
    }

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHlep(const xAOD::PhotonContainer    *photons,
                                                       const xAOD::ElectronContainer  *electrons,
                                                       const xAOD::MuonContainer      *muons,
                                                       const xAOD::JetContainer       *jets,
                                                       const xAOD::MissingETContainer *met)
  {
    static float vars[45] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = photons->at(0)->p4().E() / myy;
    vars[4] = 22;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = photons->at(1)->p4().E() / myy;
    vars[9] = 22;

    // lepton
    std::vector< std::vector<float> > ls;

    for (auto e : *electrons) {
      std::vector<float> ie;
      ie.push_back((float) e->p4().Pt());
      ie.push_back((float) e->p4().Eta());
      ie.push_back((float) e->p4().Phi());
      ie.push_back((float) e->p4().E());
      ie.push_back(12.);
      ls.push_back(ie);
    }

    for (auto m : *muons) {
      std::vector<float> im;
      im.push_back((float) m->p4().Pt());
      im.push_back((float) m->p4().Eta());
      im.push_back((float) m->p4().Phi());
      im.push_back((float) m->p4().E());
      im.push_back(12.);
      ls.push_back(im);
    }

    std::sort(ls.begin(), ls.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});
    vars[10] = ls[0][0];
    vars[11] = ls[0][1];
    vars[12] = ls[0][2];
    vars[13] = ls[0][3];
    vars[14] = ls[0][4];

    if (ls.size() > 1) {
      vars[15] = ls[1][0];
      vars[16] = ls[1][1];
      vars[17] = ls[1][2];
      vars[18] = ls[1][3];
      vars[19] = ls[1][4];
    } else {
      vars[15] = -999.;
      vars[16] = -999.;
      vars[17] = -999.;
      vars[18] = -999.;
      vars[19] = -999.;
    }

    // met
    vars[20] = (*met)["TST"]->met();
    vars[21] = 0;
    vars[22] = (*met)["TST"]->phi();
    vars[23] = 0;
    vars[24] = 16;

    // jets
    std::vector< std::vector<float> > js;

    for (auto j : *jets) {
      std::vector<float> ij;
      ij.push_back((float) j->p4().Pt());
      ij.push_back((float) j->p4().Eta());
      ij.push_back((float) j->p4().Phi());
      ij.push_back((float) j->p4().E());
      ij.push_back(1.);
      js.push_back(ij);
    }

    std::sort(js.begin(), js.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    while (js.size() < 4) {
      std::vector<float> ij;

      for (int i = 0; i < 5; i++) { ij.push_back(-999.); }

      js.push_back(ij);
    }

    int i = 25;

    for (auto j : js) {
      vars[i] = j[0];
      i++;
      vars[i] = j[1];
      i++;
      vars[i] = j[2];
      i++;
      vars[i] = j[3];
      i++;
      vars[i] = j[4];
      i++;

      if (i >= 45) { break; }
    }

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHCPhad(const xAOD::PhotonContainer *photons,
                                                         const xAOD::JetContainer *jets,
                                                         const xAOD::MissingETContainer *met)
  {
    static float vars[20] = {0};

    // Possible underflow
    for (int i = 0; i < 20; i++) {
      vars[i] = -999;
    }

    // pt_H, eta_H
    // t1_score, t1_pt, t1_eta, t1_phi0
    // t2_score, hy_pt, hy_eta, hy_phi0
    // delta_eta_t1hy, delta_phi_t1hy
    // m_HT
    // m_njet, m_nbjet_fixed80
    // m_met_sig
    // mt1_hy, m_t1H
    // delta_Rmin_yj, delta_Rmin_yj2

    const xAOD::Photon *ph1 = photons->at(0);
    const xAOD::Photon *ph2 = photons->at(1);
    TLorentzVector y1 = ph1->p4() * HG::invGeV;
    TLorentzVector y2 = ph2->p4() * HG::invGeV;
    TLorentzVector H1 = y1 + y2;

    vars[0] = H1.Pt();
    vars[1] = H1.Eta();

    // had1 top
    double pt_hadtop1 = var::pT_recotop1();
    double eta_hadtop1 = var::eta_recotop1();
    double phi_hadtop1 = var::phi_recotop1();
    double m_hadtop1 = var::m_recotop1();

    TLorentzVector t1;
    t1.SetPtEtaPhiM(pt_hadtop1, eta_hadtop1, phi_hadtop1, m_hadtop1);

    vars[2] = var::score_recotop1();
    vars[3] = pt_hadtop1;
    vars[4] = eta_hadtop1;
    vars[5] = H1.DeltaPhi(t1);

    // had2 top
    double score_recotop2 = var::score_recotop2();

    if (score_recotop2 > 0) { vars[6] = score_recotop2; }

    // Hybrid second top (had channel)
    double pt_hy_had = var::pT_hybridtop2();
    double eta_hy_had = var::eta_hybridtop2();
    double phi_hy_had = var::phi_hybridtop2();
    double m_hy_had = var::m_hybridtop2();

    vars[16] = m_hadtop1;

    if (pt_hy_had > 0) {
      vars[7] = pt_hy_had;
      vars[8] = eta_hy_had;

      TLorentzVector t2;
      t2.SetPtEtaPhiM(pt_hy_had, eta_hy_had, phi_hy_had, m_hy_had);
      vars[9] = H1.DeltaPhi(t2);
      vars[10] = abs(t1.Eta() - t2.Eta());
      vars[11] = abs(t1.DeltaPhi(t2));
      vars[16] = (t1 + t2).M();
    }

    double HT = 0;
    int n_jets = 0;
    int n_bjets = 0;

    double delta_Rmin_yj = 999;
    double delta_Rmin_yj2 = 999;

    // Loop over jets
    for (auto jet : *jets) {
      n_jets++;

      if ((bool)jet->auxdata<char>("DL1r_FixedCutBEff_77")) { n_bjets++; }

      TLorentzVector j;
      j = jet->p4() * HG::invGeV;

      HT += j.Pt();

      if (j.DeltaR(y1) < delta_Rmin_yj) {
        delta_Rmin_yj2 = delta_Rmin_yj;
        delta_Rmin_yj = j.DeltaR(y1);
      } else if (j.DeltaR(y1) < delta_Rmin_yj2) {
        delta_Rmin_yj2 = j.DeltaR(y1);
      }

      if (j.DeltaR(y2) < delta_Rmin_yj) {
        delta_Rmin_yj2 = delta_Rmin_yj;
        delta_Rmin_yj = j.DeltaR(y2);

      } else if (j.DeltaR(y2) < delta_Rmin_yj2) {
        delta_Rmin_yj2 = j.DeltaR(y2);
      }

    } // End loop over jets

    vars[12] = HT;
    vars[13] = n_jets;
    vars[14] = n_bjets;
    vars[15] = ((*met)["TST"]->met() * HG::invGeV) / sqrt(HT);
    vars[17] = (t1 + H1).M();
    vars[18] = delta_Rmin_yj;
    vars[19] = delta_Rmin_yj2;

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHCPlep(const xAOD::PhotonContainer *photons,
                                                         const xAOD::JetContainer *jets,
                                                         const xAOD::MissingETContainer *met)
  {

    static float vars[20] = {0};

    // Possible underflow
    for (int i = 0; i < 20; i++) {
      vars[i] = -999;
    }

    // pt_H, eta_H
    // t1_score, t1_pt, t1_eta, t1_phi0
    // t2_score, hy_pt, hy_eta, hy_phi0
    // delta_eta_t1hy, delta_phi_t1hy
    // m_HT
    // m_njet, m_nbjet_fixed80
    // m_met_sig
    // mt1_hy, m_t1H
    // delta_Rmin_yj, delta_Rmin_yj2

    const xAOD::Photon *ph1 = photons->at(0);
    const xAOD::Photon *ph2 = photons->at(1);
    TLorentzVector y1 = ph1->p4() * HG::invGeV;
    TLorentzVector y2 = ph2->p4() * HG::invGeV;

    TLorentzVector H1 = y1 + y2;

    vars[0] = H1.Pt();
    vars[1] = H1.Eta();

    // lep1 top
    double pt_leptop1 = var::pT_recotop1();
    double eta_leptop1 = var::eta_recotop1();
    double phi_leptop1 = var::phi_recotop1();
    double m_leptop1 = var::m_recotop1();

    TLorentzVector t1;
    t1.SetPtEtaPhiM(pt_leptop1, eta_leptop1, phi_leptop1, m_leptop1);

    vars[2] = var::score_recotop1();
    vars[3] = pt_leptop1;
    vars[4] = eta_leptop1;
    vars[5] = H1.DeltaPhi(t1);

    // had1 top
    double score_recotop2 = var::score_recotop2();

    if (score_recotop2 > 0) { vars[6] = score_recotop2; }

    // Hybrid second top (lep channel)
    double pt_hy_lep = var::pT_hybridtop2();
    double eta_hy_lep = var::eta_hybridtop2();
    double phi_hy_lep = var::phi_hybridtop2();
    double m_hy_lep = var::m_hybridtop2();

    vars[16] = m_leptop1;

    if (pt_hy_lep > 0) {
      vars[7] = pt_hy_lep;
      vars[8] = eta_hy_lep;

      TLorentzVector t2;
      t2.SetPtEtaPhiM(pt_hy_lep, eta_hy_lep, phi_hy_lep, m_hy_lep);
      vars[9] = H1.DeltaPhi(t2);
      vars[10] = abs(t1.Eta() - t2.Eta());
      vars[11] = abs(t1.DeltaPhi(t2));
      vars[16] = (t1 + t2).M();
    }

    double HT = 0;
    int n_jets = 0;
    int n_bjets = 0;

    double delta_Rmin_yj = 999;
    double delta_Rmin_yj2 = 999;

    // Loop over jets

    for (auto jet : *jets) {
      n_jets++;

      if ((bool)jet->auxdata<char>("DL1r_FixedCutBEff_77")) { n_bjets++; }

      TLorentzVector j;
      j = jet->p4() * HG::invGeV;

      HT += j.Pt();

      if (j.DeltaR(y1) < delta_Rmin_yj) {
        delta_Rmin_yj2 = delta_Rmin_yj;
        delta_Rmin_yj = j.DeltaR(y1);
      } else if (j.DeltaR(y1) < delta_Rmin_yj2) {
        delta_Rmin_yj2 = j.DeltaR(y1);
      }

      if (j.DeltaR(y2) < delta_Rmin_yj) {
        delta_Rmin_yj2 = delta_Rmin_yj;
        delta_Rmin_yj = j.DeltaR(y2);
      } else if (j.DeltaR(y2) < delta_Rmin_yj2) {
        delta_Rmin_yj2 = j.DeltaR(y2);
      }

    } // End loop over jets

    vars[12] = HT;
    vars[13] = n_jets;
    vars[14] = n_bjets;
    vars[15] = ((*met)["TST"]->met() * HG::invGeV) / sqrt(HT);
    vars[17] = (t1 + H1).M();
    vars[18] = delta_Rmin_yj;
    vars[19] = delta_Rmin_yj2;

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHhad_Moriond2020(const xAOD::PhotonContainer    *photons,
      const xAOD::JetContainer       *jets,
      const xAOD::MissingETContainer *met)
  {
    static float vars[55] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = -999.;
    vars[4] = -999.;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = -999.;
    vars[9] = -999.;

    // met
    vars[10] = (*met)["TST"]->met() * HG::invGeV;
    vars[11] = 0.;
    vars[12] = (*met)["TST"]->phi();
    vars[13] = -999.;
    vars[14] = -999.;

    // jets
    std::vector< std::vector<float> > js;

    for (auto j : *jets) {
      std::vector<float> ij;
      ij.push_back((float) j->p4().Pt() * HG::invGeV);
      ij.push_back((float) j->p4().Eta());
      ij.push_back((float) j->p4().Phi());
      ij.push_back(-999.);

      int PCb = -1;

      if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_60"))  { PCb = 5; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_70")) { PCb = 4; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_77")) { PCb = 3; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_85")) { PCb = 2; }
      else if (fabs(j->p4().Eta()) < 2.5) { PCb = 1; }

      ij.push_back(PCb);

      js.push_back(ij);
    }

    std::sort(js.begin(), js.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    while (js.size() < 6) {
      std::vector<float> ij;

      for (int i = 0; i < 5; i++) { ij.push_back(-999.); }

      js.push_back(ij);
    }

    int i = 15;

    for (auto j : js) {
      vars[i] = j[0];
      i++;
      vars[i] = j[1];
      i++;
      vars[i] = j[2];
      i++;
      vars[i] = j[3];
      i++;
      vars[i] = j[4];
      i++;

      if (i >= 45) { break; }
    }

    // tops
    if (var::pT_recotop1() >= 0) {
      vars[45] = var::pT_recotop1();
      vars[46] = var::eta_recotop1();
      vars[47] = var::phi_recotop1();
    } else {
      vars[45] = -999.;
      vars[46] = -999.;
      vars[47] = -999.;
    }

    vars[48] = -999.;
    vars[49] = var::score_recotop1();

    if (var::pT_hybridtop2() >= 0) {
      vars[50] = var::pT_hybridtop2();
      vars[51] = var::eta_hybridtop2();
      vars[52] = var::phi_hybridtop2();
    } else {
      vars[50] = -999.;
      vars[51] = -999.;
      vars[52] = -999.;
    }

    vars[53] = -999.;
    vars[54] = var::score_recotop2();

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHlep_Moriond2020(const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::JetContainer       *jets,
      const xAOD::MissingETContainer *met)
  {
    static float vars[55] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = -999.;
    vars[4] = -999.;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = -999.;
    vars[9] = -999.;

    // lepton
    std::vector< std::vector<float> > ls;

    for (auto e : *electrons) {
      std::vector<float> ie;
      ie.push_back((float) e->p4().Pt() * HG::invGeV);
      ie.push_back((float) e->p4().Eta());
      ie.push_back((float) e->p4().Phi());
      ie.push_back(-999.);
      ie.push_back(-999.);
      ls.push_back(ie);
    }

    for (auto m : *muons) {
      std::vector<float> im;
      im.push_back((float) m->p4().Pt() * HG::invGeV);
      im.push_back((float) m->p4().Eta());
      im.push_back((float) m->p4().Phi());
      im.push_back(-999.);
      im.push_back(-999.);
      ls.push_back(im);
    }

    std::sort(ls.begin(), ls.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});
    vars[10] = ls[0][0];
    vars[11] = ls[0][1];
    vars[12] = ls[0][2];
    vars[13] = ls[0][3];
    vars[14] = ls[0][4];

    if (ls.size() > 1) {
      vars[15] = ls[1][0];
      vars[16] = ls[1][1];
      vars[17] = ls[1][2];
      vars[18] = ls[1][3];
      vars[19] = ls[1][4];
    } else {
      vars[15] = -999.;
      vars[16] = -999.;
      vars[17] = -999.;
      vars[18] = -999.;
      vars[19] = -999.;
    }

    // met
    vars[20] = (*met)["TST"]->met() * HG::invGeV;
    vars[21] = 0.;
    vars[22] = (*met)["TST"]->phi();
    vars[23] = -999.;
    vars[24] = -999.;

    // jets
    std::vector< std::vector<float> > js;

    for (auto j : *jets) {
      std::vector<float> ij;
      ij.push_back((float) j->p4().Pt() * HG::invGeV);
      ij.push_back((float) j->p4().Eta());
      ij.push_back((float) j->p4().Phi());
      ij.push_back(-999.);

      int PCb = -1;

      if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_60"))  { PCb = 5; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_70")) { PCb = 4; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_77")) { PCb = 3; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_85")) { PCb = 2; }
      else if (fabs(j->p4().Eta()) < 2.5) { PCb = 1; }

      ij.push_back(PCb);

      js.push_back(ij);
    }

    std::sort(js.begin(), js.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    while (js.size() < 4) {
      std::vector<float> ij;

      for (int i = 0; i < 5; i++) { ij.push_back(-999.); }

      js.push_back(ij);
    }

    int i = 25;

    for (auto j : js) {
      vars[i] = j[0];
      i++;
      vars[i] = j[1];
      i++;
      vars[i] = j[2];
      i++;
      vars[i] = j[3];
      i++;
      vars[i] = j[4];
      i++;

      if (i >= 45) { break; }
    }

    // tops
    if (var::pT_recotop1() >= 0) {
      vars[45] = var::pT_recotop1();
      vars[46] = var::eta_recotop1();
      vars[47] = var::phi_recotop1();
    } else {
      vars[45] = -999.;
      vars[46] = -999.;
      vars[47] = -999.;
    }

    vars[48] = -999.;
    vars[49] = var::score_recotop1();

    if (var::pT_hybridtop2() >= 0) {
      vars[50] = var::pT_hybridtop2();
      vars[51] = var::eta_hybridtop2();
      vars[52] = var::phi_hybridtop2();
    } else {
      vars[50] = -999.;
      vars[51] = -999.;
      vars[52] = -999.;
    }

    vars[53] = -999.;
    vars[54] = var::score_recotop2();

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttH_ICHEP2020(const xAOD::PhotonContainer    *photons,
                                                              const xAOD::ElectronContainer  *electrons,
                                                              const xAOD::MuonContainer      *muons,
                                                              const xAOD::JetContainer       *jets,
                                                              const xAOD::MissingETContainer *met)
  {
    static float vars[65] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = -999.;
    vars[4] = -999.;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = -999.;
    vars[9] = -999.;

    // lepton
    std::vector< std::vector<float> > ls;

    for (auto e : *electrons) {
      std::vector<float> ie;
      ie.push_back((float) e->p4().Pt());
      ie.push_back((float) e->p4().Eta());
      ie.push_back((float) e->p4().Phi());
      ie.push_back(-999.);
      ie.push_back(-999.);
      ls.push_back(ie);
    }

    for (auto m : *muons) {
      std::vector<float> im;
      im.push_back((float) m->p4().Pt());
      im.push_back((float) m->p4().Eta());
      im.push_back((float) m->p4().Phi());
      im.push_back(-999.);
      im.push_back(-999.);
      ls.push_back(im);
    }

    std::sort(ls.begin(), ls.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    if (ls.size() > 0) {
      vars[10] = ls[0][0];
      vars[11] = ls[0][1];
      vars[12] = ls[0][2];
      vars[13] = ls[0][3];
      vars[14] = ls[0][4];
    } else {
      vars[10] = -999.;
      vars[11] = -999.;
      vars[12] = -999.;
      vars[13] = -999.;
      vars[14] = -999.;
    }

    if (ls.size() > 1) {
      vars[15] = ls[1][0];
      vars[16] = ls[1][1];
      vars[17] = ls[1][2];
      vars[18] = ls[1][3];
      vars[19] = ls[1][4];
    } else {
      vars[15] = -999.;
      vars[16] = -999.;
      vars[17] = -999.;
      vars[18] = -999.;
      vars[19] = -999.;
    }

    // met
    float TST_MET_signi = -999.;

    if ((*met)["TST"]->met() != 0) { TST_MET_signi = (*met)["TST"]->met() * HG::invGeV / sqrt((*met)["TST"]->sumet() * HG::invGeV); }

    vars[20] = (*met)["TST"]->met();
    vars[21] = 0.;
    vars[22] = (*met)["TST"]->phi();
    vars[23] = TST_MET_signi;
    vars[24] = -999.;

    // jets
    std::vector< std::vector<float> > js;

    for (auto j : *jets) {
      std::vector<float> ij;
      ij.push_back((float) j->p4().Pt());
      ij.push_back((float) j->p4().Eta());
      ij.push_back((float) j->p4().Phi());
      ij.push_back(-999.);

      int PCb = -1;

      if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_60"))  { PCb = 5; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_70")) { PCb = 4; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_77")) { PCb = 3; }
      else if ((bool)j->auxdata<char>("DL1r_FixedCutBEff_85")) { PCb = 2; }
      else if (fabs(j->p4().Eta()) < 2.5) { PCb = 1; }

      ij.push_back(PCb);

      js.push_back(ij);
    }

    std::sort(js.begin(), js.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    while (js.size() < 6) {
      std::vector<float> ij;

      for (int i = 0; i < 5; i++) { ij.push_back(-999.); }

      js.push_back(ij);
    }

    int i = 25;

    for (auto j : js) {
      vars[i] = j[0];
      i++;
      vars[i] = j[1];
      i++;
      vars[i] = j[2];
      i++;
      vars[i] = j[3];
      i++;
      vars[i] = j[4];
      i++;

      if (i >= 55) { break; }
    }

    // tops
    if (var::pT_recotop1() >= 0) {
      vars[55] = var::pT_recotop1();
      vars[56] = var::eta_recotop1();
      vars[57] = var::phi_recotop1();
    } else {
      vars[55] = -999.;
      vars[56] = -999.;
      vars[57] = -999.;
    }

    vars[58] = -999.;

    if (var::score_recotop1() >= 0) {
      vars[59] = var::score_recotop1();
    } else {
      vars[59] = -999.;
    }

    if (var::pT_hybridtop2() >= 0) {
      vars[60] = var::pT_hybridtop2();
      vars[61] = var::eta_hybridtop2();
      vars[62] = var::phi_hybridtop2();
    } else {
      vars[60] = -999.;
      vars[61] = -999.;
      vars[62] = -999.;
    }

    vars[63] = -999.;

    if (var::score_recotop2() >= 0) {
      vars[64] = var::score_recotop2();
    } else {
      vars[64] = -999.;
    }

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHhad_multiclass(const xAOD::PhotonContainer *photons,
      const xAOD::JetContainer *jets,
      const xAOD::MissingETContainer *met)
  {

    static float vars[18] = {0};

    // pt_H, eta_H
    // t1_score, t1_pt, t1_eta, t1_phi0
    // t2_score, hy_pt, hy_eta, hy_phi0
    // delta_eta_t1hy, delta_phi_t1hy
    // m_HT
    // m_njet, m_nbjet_fixed80
    // m_met_sig, m_t1hy, m_t1H

    const xAOD::Photon *ph1 = photons->at(0);
    const xAOD::Photon *ph2 = photons->at(1);
    TLorentzVector y1 = ph1->p4() * HG::invGeV;
    TLorentzVector y2 = ph2->p4() * HG::invGeV;
    TLorentzVector H1 = y1 + y2;

    vars[0] = H1.Pt();
    vars[1] = H1.Eta();

    // had1 top
    double pt_hadtop1 = var::pT_recotop1();
    double eta_hadtop1 = var::eta_recotop1();
    double phi_hadtop1 = var::phi_recotop1();
    double m_hadtop1 = var::m_recotop1();

    TLorentzVector t1;
    t1.SetPtEtaPhiM(pt_hadtop1, eta_hadtop1, phi_hadtop1, m_hadtop1);

    vars[2] = var::score_recotop1();
    vars[3] = pt_hadtop1;
    vars[4] = eta_hadtop1;
    vars[5] = H1.DeltaPhi(t1);

    // had2 top
    vars[6] = var::score_recotop2();

    // Hybrid second top (had channel)
    double pt_hy_had = var::pT_hybridtop2();
    double eta_hy_had = var::eta_hybridtop2();
    double phi_hy_had = var::phi_hybridtop2();
    double m_hy_had = var::m_hybridtop2();

    vars[7] = pt_hy_had;
    vars[8] = eta_hy_had;
    // Possible underflow
    vars[9] = -999;
    vars[10] = -999;
    vars[11] = -999;
    vars[16] = m_hadtop1;

    if (pt_hy_had > 0) {
      TLorentzVector t2;
      t2.SetPtEtaPhiM(pt_hy_had, eta_hy_had, phi_hy_had, m_hy_had);
      vars[9] = H1.DeltaPhi(t2);
      vars[10] = abs(t1.Eta() - t2.Eta());
      vars[11] = abs(t1.DeltaPhi(t2));
      vars[16] = (t1 + t2).M();
    }

    double HT = 0;
    int n_jets = 0;
    int n_bjets = 0;

    // Loop over jets
    for (auto jet : *jets) {
      n_jets++;

      if ((bool)jet->auxdata<char>("DL1r_FixedCutBEff_77")) { n_bjets++; }

      TLorentzVector j;
      j = jet->p4() * HG::invGeV;
      HT += j.Pt();

    } // End loop over jets

    vars[12] = HT;
    vars[13] = n_jets;
    vars[14] = n_bjets;
    vars[15] = ((*met)["TST"]->met() * HG::invGeV) / sqrt(HT);

    vars[17] = (t1 + H1).M();

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_ttHlep_multiclass(const xAOD::PhotonContainer *photons,
      const xAOD::JetContainer       *jets,
      const xAOD::MissingETContainer *met)
  {

    static float vars[18] = {0};

    // pt_H, eta_H
    // t1_score, t1_pt, t1_eta, t1_phi0
    // t2_score, hy_pt, hy_eta, hy_phi0
    // delta_eta_t1hy, delta_phi_t1hy
    // m_HT
    // m_njet, m_nbjet_fixed80
    // m_met_sig, m_t1hy, m_t1H

    const xAOD::Photon *ph1 = photons->at(0);
    const xAOD::Photon *ph2 = photons->at(1);
    TLorentzVector y1 = ph1->p4() * HG::invGeV;
    TLorentzVector y2 = ph2->p4() * HG::invGeV;

    TLorentzVector H1 = y1 + y2;

    vars[0] = H1.Pt();
    vars[1] = H1.Eta();

    // lep1 top
    double pt_leptop1 = var::pT_recotop1();
    double eta_leptop1 = var::eta_recotop1();
    double phi_leptop1 = var::phi_recotop1();
    double m_leptop1 = var::m_recotop1();

    TLorentzVector t1;
    t1.SetPtEtaPhiM(pt_leptop1, eta_leptop1, phi_leptop1, m_leptop1);

    vars[2] = var::score_recotop1();
    vars[3] = pt_leptop1;
    vars[4] = eta_leptop1;
    vars[5] = H1.DeltaPhi(t1);

    // had1 top
    vars[6] = var::score_recotop2();

    // Hybrid second top (lep channel)
    double pt_hy_lep = var::pT_hybridtop2();
    double eta_hy_lep = var::eta_hybridtop2();
    double phi_hy_lep = var::phi_hybridtop2();
    double m_hy_lep = var::m_hybridtop2();

    vars[7] = pt_hy_lep;
    vars[8] = eta_hy_lep;
    // Possible underflow
    vars[9] = -999;
    vars[10] = -999;
    vars[11] = -999;
    vars[16] = m_leptop1;

    if (pt_hy_lep > 0) {
      TLorentzVector t2;
      t2.SetPtEtaPhiM(pt_hy_lep, eta_hy_lep, phi_hy_lep, m_hy_lep);
      vars[9] = H1.DeltaPhi(t2);
      vars[10] = abs(t1.Eta() - t2.Eta());
      vars[11] = abs(t1.DeltaPhi(t2));
      vars[16] = (t1 + t2).M();
    }

    double HT = 0;
    int n_jets = 0;
    int n_bjets = 0;

    // Loop over jets
    for (auto jet : *jets) {
      n_jets++;

      if ((bool)jet->auxdata<char>("DL1r_FixedCutBEff_77")) { n_bjets++; }

      TLorentzVector j;
      j = jet->p4() * HG::invGeV;
      HT += j.Pt();

    } // End loop over jets

    vars[12] = HT;
    vars[13] = n_jets;
    vars[14] = n_bjets;
    vars[15] = ((*met)["TST"]->met() * HG::invGeV) / sqrt(HT);
    vars[17] = (t1 + H1).M();

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_VHMET_Moriond2020(const xAOD::PhotonContainer    *photons,
      const xAOD::MissingETContainer *met)
  {
    static float vars[20] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = -999.;
    vars[4] = -999.;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = -999.;
    vars[9] = -999.;

    // met
    float TST_MET_signi = -999.;

    if ((*met)["TST"]->met() != 0) { TST_MET_signi = (*met)["TST"]->met() * HG::invGeV / sqrt((*met)["TST"]->sumet() * HG::invGeV); }

    vars[10] = (*met)["TST"]->met();
    vars[11] = 0.;
    vars[12] = (*met)["TST"]->phi();
    vars[13] = TST_MET_signi;
    vars[14] = -999.;

    vars[15] = (*met)["hardVertexTST"]->met() - (*met)["TST"]->met();
    vars[16] = 0.;
    vars[17] = 0.;
    vars[18] = 0.;
    vars[19] = 0.;

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_VHlep_Moriond2020(const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::MissingETContainer *met)
  {
    static float vars[25] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = -999.;
    vars[4] = -999.;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = -999.;
    vars[9] = -999.;

    // lepton
    std::vector< std::vector<float> > ls;

    for (auto e : *electrons) {
      std::vector<float> ie;
      ie.push_back((float) e->p4().Pt());
      ie.push_back((float) e->p4().Eta());
      ie.push_back((float) e->p4().Phi());
      ie.push_back(-999.);
      ie.push_back(-999.);
      ls.push_back(ie);
    }

    for (auto m : *muons) {
      std::vector<float> im;
      im.push_back((float) m->p4().Pt());
      im.push_back((float) m->p4().Eta());
      im.push_back((float) m->p4().Phi());
      im.push_back(-999.);
      im.push_back(-999.);
      ls.push_back(im);
    }

    std::sort(ls.begin(), ls.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});
    vars[10] = ls[0][0];
    vars[11] = ls[0][1];
    vars[12] = ls[0][2];
    vars[13] = ls[0][3];
    vars[14] = ls[0][4];

    // met
    float TST_MET_signi = -999.;

    if ((*met)["TST"]->met() != 0) { TST_MET_signi = (*met)["TST"]->met() * HG::invGeV / sqrt((*met)["TST"]->sumet() * HG::invGeV); }

    vars[15] = (*met)["TST"]->met();
    vars[16] = 0.;
    vars[17] = (*met)["TST"]->phi();
    vars[18] = TST_MET_signi;
    vars[19] = -999.;

    // vector bosons
    vars[20] = var::pTlepMET();
    vars[21] = var::etalepMET();
    vars[22] = var::philepMET();
    vars[23] = 0;
    vars[24] = 0;

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_VH_ICHEP2020(const xAOD::PhotonContainer    *photons,
                                                             const xAOD::ElectronContainer  *electrons,
                                                             const xAOD::MuonContainer      *muons,
                                                             const xAOD::MissingETContainer *met)
  {
    static float vars[30] = {0};

    // photons
    float myy = (photons->at(0)->p4() + photons->at(1)->p4()).M();
    vars[0] = photons->at(0)->p4().Pt() / myy;
    vars[1] = photons->at(0)->p4().Eta();
    vars[2] = photons->at(0)->p4().Phi();
    vars[3] = -999.;
    vars[4] = -999.;
    vars[5] = photons->at(1)->p4().Pt() / myy;
    vars[6] = photons->at(1)->p4().Eta();
    vars[7] = photons->at(1)->p4().Phi();
    vars[8] = -999.;
    vars[9] = -999.;

    // lepton
    std::vector< std::vector<float> > ls;

    for (auto e : *electrons) {
      std::vector<float> ie;
      ie.push_back((float) e->p4().Pt());
      ie.push_back((float) e->p4().Eta());
      ie.push_back((float) e->p4().Phi());
      ie.push_back(-999.);
      ie.push_back(-999.);
      ls.push_back(ie);
    }

    for (auto m : *muons) {
      std::vector<float> im;
      im.push_back((float) m->p4().Pt());
      im.push_back((float) m->p4().Eta());
      im.push_back((float) m->p4().Phi());
      im.push_back(-999.);
      im.push_back(-999.);
      ls.push_back(im);
    }

    std::sort(ls.begin(), ls.end(), [](std::vector<float> a, std::vector<float> b) {return a[0] > b[0];});

    if (ls.size() > 0) {
      vars[10] = ls[0][0];
      vars[11] = ls[0][1];
      vars[12] = ls[0][2];
      vars[13] = ls[0][3];
      vars[14] = ls[0][4];
    } else {
      vars[10] = -999.;
      vars[11] = -999.;
      vars[12] = -999.;
      vars[13] = -999.;
      vars[14] = -999.;
    }

    if (ls.size() > 1) {
      vars[15] = ls[1][0];
      vars[16] = ls[1][1];
      vars[17] = ls[1][2];
      vars[18] = ls[1][3];
      vars[19] = ls[1][4];
    } else {
      vars[15] = -999.;
      vars[16] = -999.;
      vars[17] = -999.;
      vars[18] = -999.;
      vars[19] = -999.;
    }

    // met
    float TST_MET_signi = -999.;

    if ((*met)["TST"]->met() != 0) { TST_MET_signi = (*met)["TST"]->met() * HG::invGeV / sqrt((*met)["TST"]->sumet() * HG::invGeV); }

    vars[20] = (*met)["TST"]->met();
    vars[21] = 0.;
    vars[22] = (*met)["TST"]->phi();
    vars[23] = TST_MET_signi;
    vars[24] = -999.;

    // vector bosons
    float m_ll = -999.;

    if (var::m_ee() >= 0 || var::m_mumu() >= 0) { m_ll = std::max(var::m_ee(), var::m_mumu()); }

    float mT_lepMET = -999.;

    if (vars[10] >= 0) { mT_lepMET = sqrt(2 * vars[10] * vars[20] * (1 - cos(vars[12] - vars[22]))); }

    vars[25] = m_ll;
    vars[26] = mT_lepMET;

    if (((float)(*met)["TST"]->met() - (float)(*met)["hardVertexTST"]->met()) > 30 * HG::GeV) {
      vars[27] = 1;
    } else {
      vars[27] = -1;
    }

    vars[28] = 0;
    vars[29] = 0;

    return vars;
  }

  float *HGamCategoryTool::make_XGBoost_DMatrix_MonoH_2var(const xAOD::PhotonContainer    *photons,
                                                           const xAOD::MissingETContainer *met)
  {
    static float vars[5] = {0};

    // photons
    float ptyy = (photons->at(0)->p4() + photons->at(1)->p4()).Pt();

    // met
    float TST_MET_signi = -999.;

    if ((*met)["TST"]->met() != 0) { TST_MET_signi = (*met)["TST"]->met() * HG::invGeV / sqrt((*met)["TST"]->sumet() * HG::invGeV); }

    vars[0] = ptyy;
    vars[1] = 0.;
    vars[2] = 0.;
    vars[3] = TST_MET_signi;
    vars[4] = 0.;

    return vars;
  }

  float HGamCategoryTool::get_XGBoost_Weight(BoosterHandle *boost, float *vars, int len)
  {
    DMatrixHandle dmat;
    XGDMatrixCreateFromMat(vars, 1, len, -1, &dmat);
    bst_ulong out_len;
    const float *f;
    XGBoosterPredict(*boost, dmat, 0, 0, &out_len, &f);
    XGDMatrixFree(dmat);
    return *f;
  }

  const float *HGamCategoryTool::get_XGBoost_Weight_multiclass(BoosterHandle *boost, float *vars, int len)
  {
    DMatrixHandle dmat;
    XGDMatrixCreateFromMat(vars, 1, len, -1, &dmat);
    bst_ulong out_len;
    const float *f;
    XGBoosterPredict(*boost, dmat, 0, 0, &out_len, &f);
    XGDMatrixFree(dmat);

    return f;
  }

  //___________________________________________________________________________________________________________
  // ---------------------------------------------------------------------------------
  //   XGBoost top reconstruction
  // ---------------------------------------------------------------------------------
  void HGamCategoryTool::decorateHadtopCandidate(const xAOD::JetContainer *jets, int ntop)
  {

    static SG::AuxElement::Accessor<float> bdt_leptop1("bdt_leptop1");
    static SG::AuxElement::Accessor<float> bdt_hadtop1("bdt_hadtop1");
    static SG::AuxElement::Accessor<float> bdt_hadtop2("bdt_hadtop2");

    float best_score = -1;

    const xAOD::Jet *recojet1_t = 0;
    const xAOD::Jet *recojet2_t = 0;
    const xAOD::Jet *recojet3_t = 0;

    TLorentzVector k1, k2, k3;

    std::map<std::string, double> vbls;
    std::vector<std::string> vbl_names = {"W_pt", "W_eta", "W_phi", "W_m",
                                          "b_pt", "b_eta", "b_phi", "b_E",
                                          "Wjj_dR", "tWb_dR",
                                          "btag1", "btag2", "btag3", "t_m"
                                         };
    const int len = 14;

    // Loop over all sets of 3 reco jets
    int index_1 = -1;

    for (auto recojet1 : *jets) {
      index_1++;

      bdt_hadtop2(*const_cast<xAOD::Jet *>(recojet1)) = 0;

      // Skip if hadtop1 already selected
      if (bdt_hadtop1.isAvailable(*recojet1)) {
        if (bdt_hadtop1(*recojet1) > 0) { continue; }
      } else {
        bdt_hadtop1(*const_cast<xAOD::Jet *>(recojet1)) = 0;
      }

      // Skip if selected by leptop1
      if (bdt_leptop1.isAvailable(*recojet1)) {
        if (bdt_leptop1(*recojet1) > 0) { continue; }
      }

      k1.SetPtEtaPhiM(recojet1->pt()*HG::invGeV, recojet1->eta(), recojet1->phi(), recojet1->m()*HG::invGeV);

      double index_2 = -1;

      for (auto recojet2 : *jets) {
        index_2++;

        if (index_2 == index_1) { continue; }

        if (bdt_hadtop1.isAvailable(*recojet2)) {
          if (bdt_hadtop1(*recojet2) > 0) { continue; }
        }

        if (bdt_leptop1.isAvailable(*recojet2)) {
          if (bdt_leptop1(*recojet2) > 0) { continue; }
        }

        if (index_2 == index_1) { continue; }

        if (bdt_hadtop1.isAvailable(*recojet2)) {
          if (bdt_hadtop1(*recojet2) > 0) { continue; }
        }

        k2.SetPtEtaPhiM(recojet2->pt()*HG::invGeV, recojet2->eta(), recojet2->phi(), recojet2->m()*HG::invGeV);

        double index_3 = -1;

        for (auto recojet3 : *jets) {
          index_3++;

          if (index_3 >= index_2 || index_3 == index_1) { continue; }

          if (bdt_hadtop1.isAvailable(*recojet3)) {
            if (bdt_hadtop1(*recojet3) > 0) { continue; }
          }

          if (bdt_leptop1.isAvailable(*recojet3)) {
            if (bdt_leptop1(*recojet3) > 0) { continue; }
          }

          if (index_3 >= index_2 || index_3 == index_1) { continue; }

          if (bdt_hadtop1.isAvailable(*recojet3)) {
            if (bdt_hadtop1(*recojet3) > 0) { continue; }
          }

          k3.SetPtEtaPhiM(recojet3->pt()*HG::invGeV, recojet3->eta(), recojet3->phi(), recojet3->m()*HG::invGeV);

          vbls["W_pt"] = (k2 + k3).Pt();
          vbls["W_eta"] = (k2 + k3).Eta();
          vbls["W_phi"] = (k2 + k3).Phi();
          vbls["W_m"] = (k2 + k3).M();
          vbls["b_pt"] = (k1).Pt();
          vbls["b_eta"] = (k1).Eta();
          vbls["b_phi"] = (k1).Phi();
          vbls["b_E"] = (k1).E();
          vbls["Wjj_dR"] = k2.DeltaR(k3);
          vbls["tWb_dR"] = (k2 + k3).DeltaR(k1);
          vbls["t_m"] = (k1 + k2 + k3).M();

          double PCb1 = 1 ;

          if ((bool)recojet1->auxdata<char>("DL1r_FixedCutBEff_60"))  { PCb1 = 5; }
          else if ((bool)recojet1->auxdata<char>("DL1r_FixedCutBEff_70")) { PCb1 = 4; }
          else if ((bool)recojet1->auxdata<char>("DL1r_FixedCutBEff_77")) { PCb1 = 3; }
          else if ((bool)recojet1->auxdata<char>("DL1r_FixedCutBEff_85")) { PCb1 = 2; }

          double PCb2 = 1 ;

          if ((bool)recojet2->auxdata<char>("DL1r_FixedCutBEff_60"))  { PCb2 = 5; }
          else if ((bool)recojet2->auxdata<char>("DL1r_FixedCutBEff_70")) { PCb2 = 4; }
          else if ((bool)recojet2->auxdata<char>("DL1r_FixedCutBEff_77")) { PCb2 = 3; }
          else if ((bool)recojet2->auxdata<char>("DL1r_FixedCutBEff_85")) { PCb2 = 2; }

          double PCb3 = 1 ;

          if ((bool)recojet3->auxdata<char>("DL1r_FixedCutBEff_60"))  { PCb3 = 5; }
          else if ((bool)recojet3->auxdata<char>("DL1r_FixedCutBEff_70")) { PCb3 = 4; }
          else if ((bool)recojet3->auxdata<char>("DL1r_FixedCutBEff_77")) { PCb3 = 3; }
          else if ((bool)recojet3->auxdata<char>("DL1r_FixedCutBEff_85")) { PCb3 = 2; }

          vbls["btag1"] = PCb1;
          vbls["btag2"] = PCb2;
          vbls["btag3"] = PCb3;

          static float vars[len] = {0};

          for (int j = 0; j < len; j++) {
            vars[j] = vbls[vbl_names.at(j)];
          }

          float m_score = get_XGBoost_Weight(m_xgboost_topreco, vars, len);

          if (m_score > best_score) {
            best_score = m_score;
            recojet1_t = recojet1;
            recojet2_t = recojet2;
            recojet3_t = recojet3;
          }

        } // End loop over reco jets
      } // End loop over reco jets
    } // End loop over reco jets

    if (best_score > 0) {
      if (ntop == 1) {
        bdt_hadtop1(*const_cast<xAOD::Jet *>(recojet1_t)) = best_score;
        bdt_hadtop1(*const_cast<xAOD::Jet *>(recojet2_t)) = best_score;
        bdt_hadtop1(*const_cast<xAOD::Jet *>(recojet3_t)) = best_score;
      } else if (ntop == 2) {
        bdt_hadtop2(*const_cast<xAOD::Jet *>(recojet1_t)) = best_score;
        bdt_hadtop2(*const_cast<xAOD::Jet *>(recojet2_t)) = best_score;
        bdt_hadtop2(*const_cast<xAOD::Jet *>(recojet3_t)) = best_score;
      }
    }

    return;
  }

  void HGamCategoryTool::decorateSleptopCandidate(const xAOD::JetContainer       *jets,
                                                  const xAOD::ElectronContainer  *electrons,
                                                  const xAOD::MuonContainer      *muons,
                                                  const xAOD::MissingETContainer *mets)
  {

    static SG::AuxElement::Accessor<float> bdt_hadtop1("bdt_hadtop1");
    static SG::AuxElement::Accessor<float> bdt_leptop1("bdt_leptop1");

    float best_score = -1;

    const xAOD::Jet *recojet1_t = 0;

    TLorentzVector k1, k2;
    std::map<std::string, double> vbls;
    std::vector<std::string> vbl_names = {"W_pt", "W_eta", "W_phi", "W_m",
                                          "b_pt", "b_eta", "b_phi", "b_E",
                                          "Wjj_dR", "tWb_dR",
                                          "btag1", "btag2", "btag3", "t_m"
                                         };

    const int len = 14;

    if (electrons->size() + muons->size() != 1) { return; }

    TLorentzVector W1 = lepW(electrons, muons, mets);

    TLorentzVector l1, l2;

    for (auto el : *electrons) {
      l1.SetPtEtaPhiM(el->pt()*HG::invGeV, el->eta(), el->phi(), el->m()*HG::invGeV);
    }

    for (auto mu : *muons) {
      l1.SetPtEtaPhiM(mu->pt()*HG::invGeV, mu->eta(), mu->phi(), mu->m()*HG::invGeV);
    }

    l2 = W1 - l1;

    // Loop over all jets
    int index_1 = -1;

    for (auto recojet1 : *jets) {
      index_1++;

      // Set all jet BDT scores to zero. To be updated later
      bdt_leptop1(*const_cast<xAOD::Jet *>(recojet1)) = 0;

      if (bdt_hadtop1.isAvailable(*recojet1)) {
        if (bdt_hadtop1(*recojet1) > 0) { continue; }
      }

      k1.SetPtEtaPhiM(recojet1->pt()*HG::invGeV, recojet1->eta(), recojet1->phi(), recojet1->m()*HG::invGeV);

      vbls["W_pt"] = W1.Pt();
      vbls["W_eta"] = W1.Eta();
      vbls["W_phi"] = W1.Phi();
      vbls["W_m"] = W1.M();
      vbls["b_pt"] = (k1).Pt();
      vbls["b_eta"] = (k1).Eta();
      vbls["b_phi"] = (k1).Phi();
      vbls["b_E"] = (k1).E();
      vbls["Wjj_dR"] = l1.DeltaR(l2);
      vbls["tWb_dR"] = (l1 + l2).DeltaR(k1);
      vbls["t_m"] = (k1 + W1).M();

      double PCb1 = 1 ;

      if ((bool)recojet1->auxdata<char>("DL1r_FixedCutBEff_60"))  { PCb1 = 5; }
      else if ((bool)recojet1->auxdata<char>("DL1r_FixedCutBEff_70")) { PCb1 = 4; }
      else if ((bool)recojet1->auxdata<char>("DL1r_FixedCutBEff_77")) { PCb1 = 3; }
      else if ((bool)recojet1->auxdata<char>("DL1r_FixedCutBEff_85")) { PCb1 = 2; }

      vbls["btag1"] = PCb1;
      vbls["btag2"] = 1;
      vbls["btag3"] = 1;

      static float vars[len] = {0};

      for (int j = 0; j < len; j++) {

        vars[j] = vbls[vbl_names.at(j)];
      }

      float m_score = get_XGBoost_Weight(m_xgboost_topreco, vars, len);

      if (m_score > best_score) {
        best_score = m_score;
        recojet1_t = recojet1;
      }

    } // End loop over reco jets

    if (best_score > 0) {
      bdt_leptop1(*const_cast<xAOD::Jet *>(recojet1_t)) = best_score;
    }

    return;
  }

}
