// EDM include(s):
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/AuxInfoBase.h"

// Local include(s):
#include "HGamAnalysisFramework/VarHandler.h"
// #include "HGamAnalysisFramework/HgammaUtils.h"

namespace HG {

  //____________________________________________________________________________
  VarHandler *VarHandler::m_ptr = nullptr;

  //____________________________________________________________________________
  VarHandler::VarHandler()
    : m_sysName("")
    , m_MxAODName("HGam")
    , m_event(nullptr)
    , m_store(nullptr)
    , m_recoContAvail(false)
    , m_recoPhotonsAvail(false)
    , m_recoElectronsAvail(false)
    , m_recoMuonsAvail(false)
    , m_recoTausAvail(false)
    , m_recoJetsAvail(false)
    , m_recoMetsAvail(false)
    , m_truthContAvail(false)
    , m_truthPhotonsAvail(false)
    , m_truthElectronsAvail(false)
    , m_truthMuonsAvail(false)
    , m_truthTausAvail(false)
    , m_truthJetsAvail(false)
    , m_truthMetsAvail(false)
    , m_higgsBosonsAvail(false)
  { }

  //____________________________________________________________________________
  VarHandler::~VarHandler()
  { }

  //____________________________________________________________________________
  VarHandler *VarHandler::getInstance()
  {
    if (m_ptr == nullptr)
    { m_ptr = new VarHandler(); }

    return m_ptr;
  }

  //____________________________________________________________________________
  CP::SystematicCode VarHandler::applySystematicVariation(const CP::SystematicSet &sys)
  {
    TString sysName = sys.name() == "" ? "" : ("_" + sys.name()).c_str();
    sysName.ReplaceAll(" ", "_");

    m_MxAODName = "HGam";
    m_sysName = sysName.Data();

    this->clearContainers();

    return CP::SystematicCode::Ok;
  }

  //____________________________________________________________________________
  const xAOD::EventInfo *VarHandler::getEventInfoFromEvent()
  {
    std::string name = getEventInfoName();

    const xAOD::EventInfo *eventInfo = nullptr;

    if (!m_event->contains<xAOD::EventInfo>(name))
    { return nullptr; }

    if (m_event->retrieve(eventInfo, name).isFailure())
    { return nullptr; }

    return eventInfo;
  }

  //____________________________________________________________________________
  std::string VarHandler::getSysName() const
  { return m_sysName; }

  //____________________________________________________________________________
  xAOD::EventInfo *VarHandler::getEventInfoFromStore(bool createInfo)
  {
    std::string name = getEventInfoName();

    xAOD::EventInfo *eventInfo = nullptr;

    if (m_store->contains<xAOD::EventInfo>(name)) {
      if (m_store->retrieve(eventInfo, name).isFailure()) {
        return nullptr;
      }

      return eventInfo;
    }

    if (!createInfo) { return nullptr; }

    const xAOD::EventInfo *constInfo = nullptr;

    if (m_event->contains<xAOD::EventInfo>(name)) {
      if (m_event->retrieve(constInfo, name).isFailure())
      { return nullptr; }
    }

    eventInfo = new xAOD::EventInfo();
    xAOD::AuxInfoBase *eventInfoAux = new xAOD::AuxInfoBase();
    eventInfo->setStore(eventInfoAux);

    if (constInfo != nullptr)
    { *eventInfo = *constInfo; }

    if (m_store->record(eventInfo, name).isFailure())
    { return nullptr; }

    name += "Aux";

    if (m_store->record(eventInfoAux, name).isFailure())
    { return nullptr; }

    return eventInfo;
  }

  //____________________________________________________________________________
  const xAOD::EventInfo *VarHandler::getTruthEventInfoFromEvent()
  {
    // Save reco names
    std::string sysName   = m_sysName;
    std::string MxAODName = m_MxAODName;

    // Set true names
    m_sysName = "";
    m_MxAODName = "HGamTruth";

    // Retrieve eventInfo
    const xAOD::EventInfo *eventInfo =  getEventInfoFromEvent();

    // Reset reco names
    m_sysName = sysName;
    m_MxAODName = MxAODName;

    return eventInfo;
  }

  //____________________________________________________________________________
  xAOD::EventInfo *VarHandler::getTruthEventInfoFromStore(bool createInfo)
  {
    // Save reco names
    std::string sysName   = m_sysName;
    std::string MxAODName = m_MxAODName;

    // Set true names
    m_sysName = "";
    m_MxAODName = "HGamTruth";

    // Retrieve eventInfo
    xAOD::EventInfo *eventInfo =  getEventInfoFromStore(createInfo);

    // Reset reco names
    m_sysName = sysName;
    m_MxAODName = MxAODName;

    return eventInfo;
  }

  //____________________________________________________________________________
  std::string VarHandler::getEventInfoName() const
  {
    return m_MxAODName + "EventInfo" + m_sysName;
  }

  //____________________________________________________________________________
  const xAOD::IParticleContainer *VarHandler::getPhotons(bool truth) const
  {
    if (truth) {
      if (!m_truthPhotonsAvail)
      { throw std::runtime_error("Truth photon container requested but not set in VarHandler, throwing exception"); }

      return &m_truthPhotons;
    }

    if (!m_recoPhotonsAvail)
    { throw std::runtime_error("Reco photon container requested but not set in VarHandler, throwing exception"); }

    return &m_photons;
  }

  //____________________________________________________________________________
  const xAOD::IParticleContainer *VarHandler::getJets(bool truth) const
  {
    if (truth) {
      if (!m_truthJetsAvail)
      { throw std::runtime_error("Truth jet container requested but not set in VarHandler, throwing exception"); }

      return &m_truthJets;
    }

    if (!m_recoJetsAvail)
    { throw std::runtime_error("Reco jet container requested but not set in VarHandler, throwing exception"); }

    return &m_jets;
  }

  //____________________________________________________________________________
  const xAOD::IParticleContainer *VarHandler::getElectrons(bool truth) const
  {
    if (truth) {
      if (!m_truthElectronsAvail)
      { throw std::runtime_error("Truth electron container requested but not set in VarHandler, throwing exception"); }

      return &m_truthElectrons;
    }

    if (!m_recoElectronsAvail)
    { throw std::runtime_error("Reco electron container requested but not set in VarHandler, throwing exception"); }

    return &m_electrons;
  }

  //____________________________________________________________________________
  const xAOD::IParticleContainer *VarHandler::getMuons(bool truth) const
  {
    if (truth) {
      if (!m_truthMuonsAvail)
      { throw std::runtime_error("Truth muon container requested but not set in VarHandler, throwing exception"); }

      return &m_truthMuons;
    }

    if (!m_recoMuonsAvail)
    { throw std::runtime_error("Reco muon container requested but not set in VarHandler, throwing exception"); }

    return &m_muons;
  }

  //____________________________________________________________________________
  const xAOD::IParticleContainer *VarHandler::getTaus(bool truth) const
  {
    if (truth) {
      if (!m_truthTausAvail)
      { throw std::runtime_error("Truth taujet container requested but not set in VarHandler, throwing exception"); }

      return &m_truthTaus;
    }

    if (!m_recoTausAvail)
    { throw std::runtime_error("Reco taujet container requested but not set in VarHandler, throwing exception"); }

    return &m_taus;
  }

  //____________________________________________________________________________
  const xAOD::MissingETContainer *VarHandler::getMissingETs(bool truth) const
  {
    if (truth) {
      if (!m_truthMetsAvail)
      { throw std::runtime_error("Truth MET container requested but not set in VarHandler, throwing exception"); }

      return &m_truthMets;
    }

    if (!m_recoMetsAvail)
    { throw std::runtime_error("Reco MET container requested but not set in VarHandler, throwing exception"); }

    return &m_mets;
  }

  //____________________________________________________________________________
  const xAOD::IParticleContainer *VarHandler::getHiggsBosons() const
  {
    if (!m_higgsBosonsAvail)
    { throw std::runtime_error("Higgs boson container requested but not set in VarHandler, throwing exception"); }

    return &m_higgsBosons;
  }

  //____________________________________________________________________________
  void VarHandler::setEventAndStore(xAOD::TEvent *event, xAOD::TStore *store)
  {
    m_event = event;
    m_store = store;
  }

  //____________________________________________________________________________
  void VarHandler::setContainers(const xAOD::IParticleContainer *photons,
                                 const xAOD::IParticleContainer *electrons,
                                 const xAOD::IParticleContainer *muons,
                                 const xAOD::IParticleContainer *taus,
                                 const xAOD::IParticleContainer *jets,
                                 const xAOD::MissingETContainer *mets)
  {
    if (photons) { m_photons   = *photons;   m_recoPhotonsAvail   = true; }

    if (electrons) { m_electrons = *electrons; m_recoElectronsAvail = true; }

    if (muons) { m_muons     = *muons;     m_recoMuonsAvail     = true; }

    if (taus) { m_taus     = *taus;     m_recoTausAvail     = true; }

    if (jets) { m_jets      = *jets;      m_recoJetsAvail      = true; }

    if (mets) { m_mets      = *mets;      m_recoMetsAvail      = true; }

    m_recoContAvail = true;
  }

  //____________________________________________________________________________
  bool VarHandler::checkContainers() const
  { return m_recoContAvail; }

  //____________________________________________________________________________
  void VarHandler::setTruthContainers(const xAOD::IParticleContainer *photons,
                                      const xAOD::IParticleContainer *electrons,
                                      const xAOD::IParticleContainer *muons,
                                      const xAOD::IParticleContainer *taus,
                                      const xAOD::IParticleContainer *jets,
                                      const xAOD::MissingETContainer *mets)
  {
    if (photons) { m_truthPhotons   = *photons;   m_truthPhotonsAvail   = true; }

    if (electrons) { m_truthElectrons = *electrons; m_truthElectronsAvail = true; }

    if (muons) { m_truthMuons     = *muons;     m_truthMuonsAvail     = true; }

    if (taus) { m_truthTaus     = *taus;     m_truthTausAvail     = true; }

    if (jets) { m_truthJets      = *jets;      m_truthJetsAvail      = true; }

    if (mets) { m_truthMets      = *mets;      m_truthMetsAvail      = true; }

    m_truthContAvail = true;
  }

  //____________________________________________________________________________
  void VarHandler::setHiggsBosons(const xAOD::IParticleContainer *higgs)
  {
    if (higgs) { m_higgsBosons = *higgs; m_higgsBosonsAvail = true; }
  }

  //____________________________________________________________________________
  bool VarHandler::checkTruthContainers() const
  { return m_truthContAvail; }

  //____________________________________________________________________________
  void VarHandler::clearContainers()
  {
    // Clear reco containers
    m_photons.clear();
    m_electrons.clear();
    m_muons.clear();
    m_taus.clear();
    m_jets.clear();
    m_mets.clear();
    m_recoContAvail = false;
    m_recoPhotonsAvail = m_recoJetsAvail = m_recoElectronsAvail = m_recoMuonsAvail = m_recoTausAvail = m_recoMetsAvail = false;

    // Clear truth containers
    m_truthPhotons.clear();
    m_truthElectrons.clear();
    m_truthMuons.clear();
    m_truthTaus.clear();
    m_truthJets.clear();
    m_truthMets.clear();
    m_higgsBosons.clear();
    m_truthContAvail = false;
    m_truthPhotonsAvail = m_truthJetsAvail = m_truthElectronsAvail = m_truthMuonsAvail = m_truthTausAvail = m_truthMetsAvail = m_higgsBosonsAvail = false;
  }

  //____________________________________________________________________________
  EL::StatusCode VarHandler::write()
  {
    xAOD::EventInfo *eventInfo = getEventInfoFromStore();

    if (eventInfo == nullptr)
    { return EL::StatusCode::FAILURE; }

    xAOD::EventInfo *copy = new xAOD::EventInfo();
    xAOD::AuxInfoBase *copyAux = new xAOD::AuxInfoBase();
    copy->setStore(copyAux);
    *copy = *eventInfo;

    std::string name = getEventInfoName();

    if (m_event->record(copy, name).isFailure())
    { return EL::StatusCode::FAILURE; }

    name += "Aux.";

    if (m_event->record(copyAux, name).isFailure())
    { return EL::StatusCode::FAILURE; }

    return EL::StatusCode::SUCCESS;
  }

  //____________________________________________________________________________
  EL::StatusCode VarHandler::writeTruth()
  {
    // Save reco names
    std::string sysName   = m_sysName;
    std::string MxAODName = m_MxAODName;

    // Set true names
    m_sysName = "";
    m_MxAODName = "HGamTruth";

    // Retrieve eventInfo
    EL::StatusCode code = write();

    // Reset reco names
    m_sysName = sysName;
    m_MxAODName = MxAODName;

    return code;
  }

}
