#!/bin/bash
if [ "$#" -ge 1 ];
then
	mode=$1
else
	mode="default"
fi

if [ "$mode" == "default" ];
then
	source /release_setup.sh
elif [ "$mode" == "hgamcore" ];
then
	source /release_setup.sh
	source /scripts/HGamCore/build/x86_64-centos7-gcc8-opt/setup.sh   
elif [ "$mode" == "python2"];
then
	if [ "$DEFAULT_PYTHONPATH" != "" ];
	then
		export PYTHONPATH=$DEFAULT_PYTHONPATH
	fi
	if [ "$DEFAULT_PYTHONHOME" != "" ];
	then
		export PYTHONHOME=$DEFAULT_PYTHONHOM
	fi
	alias python=python2
elif [ "$mode" == "python3"];
then
	if [ "$DEFAULT_PYTHONPATH" == "" ];
	then
		export DEFAULT_PYTHONPATH=$PYTHONPATH
	fi
	if [ "$DEFAULT_PYTHONHOME" == "" ];
	then
		export DEFAULT_PYTHONHOME=$PYTHONHOME
	fi
	unset PYTHONHOME
	unset PYTHONPATH	
	alias python=${PYTHON_EXE}
elif [ "$mode" == "test" ];
then
	if [ "$DEFAULT_PATH" != "" ];
	then
		export PATH=$DEFAULT_PATH
	fi
	if [ "$DEFAULT_LD_LIBRARY_PATH" != "" ];
	then
		export LD_LIBRARY_PATH=$DEFAULT_LD_LIBRARY_PATH
	fi
	if [ "$DEFAULT_ROOT_INCLUDE_PATH" != "" ];
	then
		export ROOT_INCLUDE_PATH=$DEFAULT_ROOT_INCLUDE_PATH
	fi
	if [ "$DEFAULT_PYTHONPATH" != "" ];
	then
		export PYTHONPATH=$DEFAULT_PYTHONPATH
	fi
	if [ "$DEFAULT_PYTHONHOME" != "" ];
	then
		export PYTHONHOME=$DEFAULT_PYTHONHOME
	fi		
	source /release_setup.sh
	source /scripts/analysis/HGamCore/build/x86_64-centos7-gcc8-opt/setup.sh
	if [ "$CONDA_PATH" != "" ];
	then
		export DEFAULT_PYTHONPATH=$PYTHONPATH
		export DEFAULT_PYTHONHOME=$PYTHONHOME
		export DEFAULT_PATH=$PATH
		export DEFAULT_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
		export DEFAULT_ROOT_INCLUDE_PATH=$ROOT_INCLUDE_PATH
		unset PYTHONHOME
		unset PYTHONPATH
		# for using python3
		export PATH=$CONDA_PATH/bin:$PATH
	fi
elif [ "$mode" == "pyROOT" ];
then
	export DEFAULT_PYTHONPATH=$PYTHONPATH
	export DEFAULT_PYTHONHOME=$PYTHONHOME
	export DEFAULT_PATH=$PATH
	export DEFAULT_LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	export DEFAULT_ROOT_INCLUDE_PATH=$ROOT_INCLUDE_PATH	
	unset PYTHONHOME
	unset PYTHONPATH
	unset LD_LIBRARY_PATH
	unset ROOT_INCLUDE_PATH
	export PATH=$CONDA_PATH/bin:$PATH
	export LD_LIBRARY_PATH=$CONDA_PATH/lib
fi


