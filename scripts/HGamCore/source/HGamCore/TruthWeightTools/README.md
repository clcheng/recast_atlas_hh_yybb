# TruthWeightTools

This tool provides easy access to the theoretical uncertainties relevant to Higgs MC samples (HiggsWeightTool would have been a more appropriate name, but we are stuck with this).

**Using this tool doesn't guarantee that all systematics relevant to your analysis are considered. This will only cover systematics related to STXS bins boundary. It is up to the analyses groups to make sure that any additional systematics are evaluated appropriately and included in their workspace**

This tool provides access to the PDF and QCD scale uncertainties. At the simplest level, it provides access to the PDF4LHC  and the muR/muF variations that are included as weights in the MC. At the second level, it also evaluate uncertainties related to each production mode which are designed specifically for the STXS analyses.

Note: That you only have to use the muR/muF **or** the STXS uncertainties. If you use both, you will double count the uncertainties.

Shower uncertainties are not provided at the moment are the responsibility of the analysis group. 

The tool usage instructions are provided below. (I know the output object is messy... but this is to keep backwards compatibility).

## Usage:
To initialize
```bash
 m_higgsMCtool = new TruthWeightTools::HiggsWeightTool( "HiggsWeightTool" );
 if (ggF) 
 {
	 double weightMax=100.0;
	 m_higgsMCtool->setProperty( "RequireFinite", true );
	 // This is designed to flatten the MC weight for large weighted events
	 // Due to the limited number of stats generated in on EVNT job. Issues and fix by DAG
	 m_higgsMCtool->setProperty( "WeightCutOff", weightMax );
	 m_higgsMCtool->setProperty( "ProdMode", "ggF" );
 }
 if (VBF) m_higgsMCtool->setProperty( "ProdMode", "VBF" );
 if (WpH || WmH) m_higgsMCtool->setProperty( "ProdMode", "WH" );
 if (qqZH) m_higgsMCtool->setProperty( "ProdMode", "qqZH" );
 if (ggZH) m_higgsMCtool->setProperty( "ProdMode", "ggZH" );
 
 m_higgsMCtool->initialize().ignore();
```

For every event:
```bash
 int HTXS_Njets30        = eventInfo->auxdata<int>("HTXS_Njets_pTjet30");
 int HTXS_Stage1         = eventInfo->auxdata<int>("HTXS_Stage1_Category_pTjet30");
 double HTXS_pTH         = eventInfo->auxdata<float>("HTXS_Higgs_pt"); // Need to be in MeV
 int HTXS_Stage1p1       = eventInfo->auxdata<int>("HTXS_Stage1_1_Category_pTjet30");
 int HTXS_Stage1p1Fine   = m_eventCont->eventInfo->auxdata<int>("HTXS_Stage1_1_Fine_Category_pTjet30");

 // Access all Higgs weights
 TruthWeightTools::HiggsWeights hw = m_higgsMCtool->getHiggsWeights(HTXS_Njets30, HTXS_pTH, HTXS_Stage1, HTXS_Stage1p1, HTXS_Stage1p1Fine);

// hw.nominal -> nominal MC weight
// hw.pdf4lhc_unc -> 30 Eigen variation for PHD4LHC
// hw.alphaS_up -> up alpha_s variaton for PHD4LHC
// hw.alphaS_down -> down alpha_s variaton for PHD4LHC
// hw.qcd -> muR/muF variation for the given MC

// Sample dependant QCD scale variations:
if (ggF)
{
     var_th_qcd_2017_mu       = hw.ggF_qcd_2017[0];
     var_th_qcd_2017_res      = hw.ggF_qcd_2017[1];
     var_th_qcd_2017_mig01    = hw.ggF_qcd_2017[2];
     var_th_qcd_2017_mig12    = hw.ggF_qcd_2017[3];
     var_th_qcd_2017_vbf2j    = hw.ggF_qcd_2017[4];
     var_th_qcd_2017_vbf3j    = hw.ggF_qcd_2017[5];
     var_th_qcd_2017_pTH60    = hw.ggF_qcd_2017[6];
     var_th_qcd_2017_pTH120   = hw.ggF_qcd_2017[7];
     var_th_qcd_2017_qm_t     = hw.ggF_qcd_2017[8];
     var_th_qcd_pTH_nJ0       = hw.ggF_qcd_pTH_nJ0;
}
if (VBF)
{
    var_th_qcd_VBF_mu         = hw.qq2Hqq_VBF_scheme[0];
    var_th_qcd_VBF_PTH200     = hw.qq2Hqq_VBF_scheme[1];
    var_th_qcd_VBF_Mjj60      = hw.qq2Hqq_VBF_scheme[2];
    var_th_qcd_VBF_Mjj120     = hw.qq2Hqq_VBF_scheme[3];
    var_th_qcd_VBF_Mjj350     = hw.qq2Hqq_VBF_scheme[4];
    var_th_qcd_VBF_Mjj700     = hw.qq2Hqq_VBF_scheme[5];
    var_th_qcd_VBF_Mjj1000    = hw.qq2Hqq_VBF_scheme[6];
    var_th_qcd_VBF_Mjj1500    = hw.qq2Hqq_VBF_scheme[7];
    var_th_qcd_VBF_pTjH25     = hw.qq2Hqq_VBF_scheme[8];
    var_th_qcd_VBF_JET01      = hw.qq2Hqq_VBF_scheme[9];
}
if ( WmH | WpH | qqZH | ggZH )
{
    var_th_qcd_qqVH_mu        = hw.qq2Hll_scheme[0];
    var_th_qcd_qqVH_PTV75     = hw.qq2Hll_scheme[1];
    var_th_qcd_qqVH_PTV150    = hw.qq2Hll_scheme[2];
    var_th_qcd_qqVH_PTV250    = hw.qq2Hll_scheme[3];
    var_th_qcd_qqVH_PTV400    = hw.qq2Hll_scheme[4];
    var_th_qcd_qqVH_mig01     = hw.qq2Hll_scheme[5];
    var_th_qcd_qqVH_mig12     = hw.qq2Hll_scheme[6];
                                                   
    var_th_qcd_ggVH_mu        = hw.gg2Hll_scheme[0];
    var_th_qcd_ggVH_PTV75     = hw.gg2Hll_scheme[1];
    var_th_qcd_ggVH_PTV150    = hw.gg2Hll_scheme[2];
    var_th_qcd_ggVH_PTV250    = hw.gg2Hll_scheme[3];
    var_th_qcd_ggVH_PTV400    = hw.gg2Hll_scheme[4];
    var_th_qcd_ggVH_mig01     = hw.gg2Hll_scheme[5];
    var_th_qcd_ggVH_mig12     = hw.gg2Hll_scheme[6];
}
```

## TODO:
Right now, this tool is not fully updated for the Stage 1.2 scheme. If you have any missing numbers, please feel free to include them into this tool for the wider Higgs community! :)

 The missing issues are listed below

 1. ggF: There are no systematics to cover the mJJ bins or pTHjj sub-bins in the nJ >= 2 phase space. Until official recommendations are in place, a possible way would be to compare FxFx vs NNLOPS to derive any analysis dependant systematics.
Probably, we need dedicated systematics for pTH > 200 bins. Right now, the only uncertainties provided are those related to finite top mass.
 3. VH-Had: For the VH-had bins, there are no dedicated STXS uncertainties. These is work in progress in the LHC-XS-WG2. Until official recommendations are in place, a possible way to derive systematics would be an envelope of muR/muF variations.
 4. ttH: No systematics are provided for this at the moment.

 

## References:
[STXS implementation](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/STXSImplementation) - HComb reference on how to implement systematics in analysis workspace.

[HComb Reference on theory sys](https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HiggsPropertiesTheorySystematics) - This is the HComb reference on the systematics related for combination.

All of the QCD scale systematics basically follow the scheme outlined at [https://arxiv.org/pdf/1803.07977.pdf](https://arxiv.org/pdf/1803.07977.pdf). This scheme essentially is a multi-step factorization process.


### ggF QCD scale:
1. https://indico.cern.ch/event/618048/ In this meeting (first and last talks) two different ggF uncertainty schemes (WG1 and STXS) are discussed and the final recommendation, called the "2017 uncertainty scheme" is the result of combining the "best parts" of the two.
2. https://indico.cern.ch/event/617739/ This shows example usage for the old tool in SVN, but more importantly, many uncertainty results.
3. https://indico.cern.ch/event/616388/ Initial talk. Less complete than the above one.
### VBF QCD scale:
The uncertainity numbers come from [LHCHXSWG2 - GitLab](https://gitlab.cern.ch/LHCHIGGSXS/LHCHXSWG2/STXS/VBF-Uncertainties/blob/master/qq2Hqq_uncert_scheme.cpp) with description of the methodology at 
[LHC XS WG meeting](https://indico.cern.ch/event/796065/contributions/3307675/attachments/1799797/2935242/VBF-STSX-HXSWG-WG1-FEB.pdf)
### VH-lep QCD scale:
These are derived by ATLAS's Hbb group. The corresponding CONF note is at [ATL-PHYS-PUB-2018-035](https://cds.cern.ch/record/2649241)



