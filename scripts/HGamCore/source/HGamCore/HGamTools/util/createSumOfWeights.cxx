// STL include(s):
#include <iostream>
#include <map>
#include <fstream>

// ROOT include(s):
#include "TFile.h"
#include "TH1F.h"
#include "TKey.h"
#include "TList.h"
#include "TString.h"
#include "TSystem.h"
#include "TTree.h"
#include "TTreeReader.h"
#include "TTreeReaderValue.h"

// forward declarations
void getSumOfWeightsAndDSID(std::vector<TString> files, double &sumOfWeights, int &DSID);

int main(int argc, char *argv[])
{
  if (argc != 2) {
    std::cout << "Usage: " << argv[0] << " <dir with MxAODs>" << std::endl;
    std::cout << "  e.g: " << argv[0] << " root://eosatlas//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h014/mc15c" << std::endl << std::endl;
    return 0;
  }


  // TString dirname = "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h014/mc15c";
  TString dirname = argv[1];
  void *dir = gSystem->OpenDirectory(dirname.Data());
  TString mcT = "SumOfWeightsMC16";

  if (dirname.Contains("mc16a"))
  { mcT += "a."; }
  else if (dirname.Contains("mc16d"))
  { mcT += "d."; }
  else if (dirname.Contains("mc16e"))
  { mcT += "e."; }
  else {
    std::cout << "Unknown campaign" << std::endl;
    return 0;
  }

  TString outputN = mcT + "txt";
  std::ofstream sumOfWFile(outputN.Data());

  std::map<int, float> weights;
  std::map<int, int> multi;

  const char *ent = nullptr;

  while ((ent = gSystem->GetDirEntry(dir))) {
    TString sub = TString::Format("%s/%s", dirname.Data(), ent);

    if (sub.Contains("diphoton_myy_175_2000"))
    { continue; }

    if (!sub.Contains(".root"))
    { continue; }

    Long_t id, size, flags, modtime;
    gSystem->GetPathInfo(sub.Data(), &id, &size, &flags, &modtime);

    std::vector<TString> files;

    //std::cout << sub << std::endl;

    if (flags & 0b10) {
      // It's a directory!
      void *subdir = gSystem->OpenDirectory(sub.Data());

      while ((ent = gSystem->GetDirEntry(subdir))) {
        TString subfile = TString::Format("%s/%s", sub.Data(), ent);

        if (!subfile.EndsWith("root"))
        { continue; }

        if (subfile.Contains(".sys"))
        { continue; }

        files.push_back(subfile);
      }

      gSystem->FreeDirectory(subdir);
    } else {
      // It's a single file!
      files.push_back(sub);
    }

    double sumOfWeights = -99.0;
    int DSID = -99;
    getSumOfWeightsAndDSID(files, sumOfWeights, DSID);

    char buf[50];
    sprintf(buf, "%s%i: %13.3f\n", mcT.Data(), DSID, sumOfWeights);

    if (weights.find(DSID) == weights.end()) {
      std::cout << mcT << DSID << ": " << sumOfWeights << " in nF = " << files.size() << std::endl;
      //sumOfWFile << mcT << DSID << ": " << sumOfWeights << std::endl;
      weights[DSID] = sumOfWeights;
      multi[DSID] = 1;
    } else {
      std::cout << mcT << DSID << ": " << sumOfWeights << " ***** multiple samples! *****" << std::endl;
      //sumOfWFile << mcT << DSID << ": " << sumOfWeights << std::endl;
      multi[DSID]++;
    }

    if (files.size() > 1)
    { sumOfWFile << std::string(buf); }
  }

  std::cout << "Read " << multi.size() << " datasets" << std::endl;

  for (auto m : multi) {
    if (m.second > 1)
    { std::cout << "DSID " << m.first << " appears " << m.second << " times" << std::endl; }
  }

  gSystem->FreeDirectory(dir);

  return 0;
}

void getSumOfWeightsAndDSID(std::vector<TString> files, double &sumOfWeights, int &DSID)
{
  TH1F *cutflow = nullptr;
  TTree *CollectionTree = nullptr;
  TFile *file = nullptr;

  for (TString filename : files) {
    file = TFile::Open(filename.Data(), "READ");

    // Get sumOfWeights
    TH1F *temp = nullptr;
    TIter next(file->GetListOfKeys());
    TObject *keyAsObj = nullptr;

    while ((keyAsObj = next())) {
      auto key = (TKey *)keyAsObj;
      TString name = key->GetName();

      if (name.Contains("_noDalitz_weighted")) {
        file->GetObject(key->GetName(), temp);
        break;
      }
    }

    if (temp == nullptr) {
      std::cout << "ERROR: CutFlow histogram not found? Won't be considered..." << std::endl;
    } else {
      if (cutflow == nullptr) {
        cutflow = (TH1F *)temp->Clone();
      } else {
        cutflow->Add(temp);
      }
    }

    // Get DSID
    if (CollectionTree) { continue; } // only for first file

    file->GetObject("CollectionTree", CollectionTree);

    if (CollectionTree == nullptr) {
      std::cout << "ERROR: CollectionTree is nullptr? Returning DSID = -99" << std::endl;
      DSID = -99;
    } else {
      TTreeReader myReader(CollectionTree);
      TTreeReaderValue<unsigned int> mcChannelNumber(myReader, "EventInfoAuxDyn.mcChannelNumber");

      while (myReader.Next()) {
        DSID = *mcChannelNumber;
        // only need first entry
        break;
      }
    }
  }

  if (cutflow) { sumOfWeights = cutflow->GetBinContent(3) * cutflow->GetBinContent(1) / cutflow->GetBinContent(2); }

  //std::cout << DSID << " " << sumOfWeights << std::endl;
  // if (cutflow) sumOfWeights = cutflow->GetBinContent(1);
}






















