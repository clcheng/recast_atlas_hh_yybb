# H125 samples
#------------------------------

PowhegPy8_ggH125
PowhegPy8_NNLOPS_ggH125
PowhegPy8_VBFH125
PowhegPy8_NNPDF30_VBFH125
Pythia8_WH125
PowhegPy8_WmH125J
PowhegPy8_WpH125J
PowhegPy8_ZH125J
PowhegPy8_ggZH125
Pythia8_ZH125
aMCnloPy8_ttH125

aMCnloPy8_bbH125_yb2
aMCnloPy8_bbH125_ybyt

MGHwpp_tHjb125_yt_minus1
MGHwpp_tHjb125_yt_plus1
MGHwpp_tHjb125_yt_plus2

MGPy8_tHjb125_yt_minus1
MGPy8_tHjb125_yt_plus1
MGPy8_tHjb125_yt_plus2

aMCnloHwpp_tWH125_yt_plus2
aMCnloHwpp_tWH125_yt_plus1
aMCnloHwpp_tWH125_yt_minus1

