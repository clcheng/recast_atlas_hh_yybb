#ifndef HGAMCATEGORYTOOLICHEPGLOBAL__H
#define HGAMCATEGORYTOOLICHEPGLOBAL__H

#include "HGamAnalysisFramework/HGamCategoryToolMulticlass2020.h"
#include "HGamAnalysisFramework/HGamCategoryToolBinaryDiscriminator2020.h"
#include "HGamAnalysisFramework/HGamCategoryToolMulticlassPlusBinary2020.h"

#include <vector>
#include <string>
#include <memory>

class TTree; // forward

namespace Coupling2020 {

  namespace VariablesICHEP {

    enum class InputVariable {
      pT_yy, yAbs_yy, Dphi_y_y, Dy_y_y, pTt_yy, phiStar_yy,
      N_j_central,  N_j, N_j_btag, m_jj, pT_yyjj,
      N_j_central30,  N_j_30, N_j_btag30, m_jj_30, pT_yyjj_30,
      pT_jj, pT_j1_30, Dy_j_j, Dphi_j_j, Deta_j_j, pT_yyj, m_yyj, m_yyjj, Dphi_yy_jj, DRmin_y_j, Dy_yy_jj, m_alljet, cosTS_yyjj,
      N_lep, N_e, N_mu, m_ee, m_mumu,
      met_TST, sumet_TST, MET_sig, phi_TST, pT_ll, pTlepMET,
      met_hardVertexTST, sumet_hardVertexTST, MET_sig_hard, phi_hardVertexTST,
      HT_30, Zepp,
      mu, numberOfPrimaryVertices,
      score_recotop, recotop_m, recotop_phi, recotop_eta, recotop_pT,
      score_hybrtop, hybrtop_m, hybrtop_phi, hybrtop_eta, hybrtop_pT,
    };

    const std::vector<std::string> input_variable_names = {
      "pT_yy", "yAbs_yy", "Dphi_y_y", "Dy_y_y", "pTt_yy", "phiStar_yy",
      "N_j_central", "N_j", "N_j_btag", "m_jj", "pT_yyjj",
      "N_j_central30", "N_j_30", "N_j_btag30", "m_jj_30", "pT_yyjj_30",
      "pT_jj", "pT_j1_30", "Dy_j_j", "Dphi_j_j", "Deta_j_j", "pT_yyj", "m_yyj", "m_yyjj", "Dphi_yy_jj", "DRmin_y_j", "Dy_yy_jj", "m_alljet", "cosTS_yyjj",
      "N_lep", "N_e", "N_mu", "m_ee", "m_mumu",
      "met_TST", "sumet_TST", "MET_sig", "phi_TST", "pT_ll", "pTlepMET",
      "met_hardVertexTST", "sumet_hardVertexTST", "MET_sig_hard", "phi_hardVertexTST",
      "HT_30", "Zepp",
      "mu", "numberOfPrimaryVertices",
      "score_recotop", "recotop_m", "recotop_phi", "recotop_eta", "recotop_pT",
      "score_hybrtop", "hybrtop_m", "hybrtop_phi", "hybrtop_eta", "hybrtop_pT"
    };

    std::vector<float> get_input_vars(const CategorizationInputs &inputs, const std::vector<InputVariable> &var_idx);

  } // namespace VariablesICHEP


  /**
   * Concrete implementation for a BinaryDiscriminator
   * This used LGBM
   **/
  class BinaryDiscriminatorICHEPDavide : public BinaryDiscriminatorLGBM {
  public:
    /**
     * tree: the TTree containing the BDT weights. This is computed with MVAUtils from the output of LGBM
     * vars: id of the vars to be used as input to the BDT. The input is build from CategorizationInputs
     * and global HGam variables
     **/
    BinaryDiscriminatorICHEPDavide(TTree *tree, const std::vector<VariablesICHEP::InputVariable> &vars)
      : BinaryDiscriminatorLGBM(tree),
        m_var_ids(vars) {};

  private:
    std::vector<float> get_input(const CategorizationInputs &inputs) const final;
    std::vector<VariablesICHEP::InputVariable> m_var_ids;
  };


  /**
   * Concrete implementation for the multiclass plus the d-optimality layer
   */
  class CategorizationMultiClassICHEP : public CategorizationMultiClass {
  public:
    /**
     * name: an unique string
     * tree_multiclass: the TTree containing the BDT weights. This is computed with MVAUtils from the output of LGBM
     * weight_multiclass: the weights for the d-optimality
     *
     * for implementation details of MVAUtils the only way to know how many categories there are is
     * from weight_multiclass
     **/
    CategorizationMultiClassICHEP(const std::string &name, TTree *tree_multiclass)
      : CategorizationMultiClass(name, tree_multiclass, get_weight_multiclass()) {}

    static std::vector<std::string> get_multiclass_names()
    {
      return {/* 0 */ "GG2H_0J_PTH_0_10", "GG2H_0J_PTH_GT10",
                      /* 2 */ "GG2H_1J_PTH_0_60", "GG2H_1J_PTH_60_120", "GG2H_1J_PTH_120_200",
                      /* 5 */ "GG2H_GE2J_MJJ_0_350_PTH_0_60", "GG2H_GE2J_MJJ_0_350_PTH_60_120", "GG2H_GE2J_MJJ_0_350_PTH_120_200",
                      /* 8 */ "GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25", "GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25",
                      /* 10 */ "GG2H_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_0_25", "GG2H_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_GT25",
                      /* 12 */ "GG2H_PTH_200_300", "GG2H_PTH_300_450", "GG2H_PTH_450_650", "GG2H_PTH_GT650",
                      /* 16 */ "QQ2HQQ_0J", "QQ2HQQ_1J",
                      /* 18 */ "QQ2HQQ_GE2J_MJJ_0_60", "QQ2HQQ_GE2J_MJJ_60_120", "QQ2HQQ_GE2J_MJJ_120_350",
                      /* 21 */ "QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25", "QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25",
                      /* 23 */ "QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_0_25", "QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_GT25",
                      /* 25 */ "QQ2HQQ_GE2J_MJJ_350_700_PTH_GT200", "QQ2HQQ_GE2J_MJJ_GT700_PTH_GT200",
                      /* 27 */ "QQ2HLNU_PTV_0_75", "QQ2HLNU_PTV_75_150", "QQ2HLNU_PTV_150_250_0J", "QQ2HLNU_PTV_150_250_GE1J", "QQ2HLNU_PTV_GT250",
                      /* 32 */ "HLL_PTV_0_75", "HLL_PTV_75_150", "HLL_PTV_150_250_0J", "HLL_PTV_150_250_GE1J", "HLL_PTV_GT250",
                      /* 37 */ "TTH_PTH_0_60", "TTH_PTH_60_120", "TTH_PTH_120_200", "TTH_PTH_200_300", "TTH_PTH_GT300",
                      /* 42 */ "THJB", "TWH"};
    }

    virtual std::string get_category_name(unsigned int icat) const override
    {
      if (icat >= get_nclasses()) {
        ATH_MSG_FATAL("invalid category index " << icat << " should be in [0, " << get_nclasses() << ")");
      }

      return get_multiclass_names()[icat];
    }


    static std::vector<float> get_weight_multiclass()
    {
      return {3.91288510, 13.4040514, 10.3235904, 14.6708345, 1.55010941, 46.3211099, 34.9929362, 4.668618, 1.8552676, 3.55443031,
              1.29144023, 2.79796169, 1.67950253, 3.41451438, 0.618637359, 1.27567482, 1.34517196, 2.616089, 2.45252166, 2.32629517,
              10.3605383, 0.738009079, 0.83007188, 0.623738822, 1.0327143, 0.120109879, 0.508756273, 2.26484666, 0.571406305, 0.195747628,
              0.838104878, 0.311243191, 0.0108758093, 1.70634766, 0.145551286, 0.462998622, 0.683913952, 0.535483172, 0.356482813, 0.366720373,
              0.35964831, 0.873269261, 0.257618326, 0.619404883};
    }

  private:
    virtual std::vector<float> get_input(const CategorizationInputs &inputs) const final;
  };


  /**
   * Final categorization for ICHEP (multiclass + binaries from Davide)
   *
   * This is not the hybrid
   **/
  class CategorizationGlobalICHEP : public CategorizationMultiClassPlusBinaries {

  public:
    /**
      * name: an unique string
      * tree_multiclass: the TTree containing the BDT weights for the multiclass.
      * trees_binary: all the TTree containing the BDT weights for the binary.
      *
      * Note that all the other parameters (weights for the d-optimality, ...) are hard-coded
      * It is suggested to don't use this constructor, but the factory method
      **/
    CategorizationGlobalICHEP(const std::string &name,
                              TTree *tree_multiclass,
                              const std::vector<TTree *> &trees_binary);

    static std::vector<std::string> get_multiclass_names() { return CategorizationMultiClassICHEP::get_multiclass_names(); }


    /**
     * Build the object just from the folder where the ROOT files containing the trees for all the BDTs are
     *
     * name: an unique string
     * folder: the folder containing the ROOT files. The file for the multiclass is "model1.root", while the
     *         ones for the binary BDTs are "model2_<cat>" where <cat> is the name of the multiclass-category
     *         (i.e. the stxs-bin)
     **/
    static std::unique_ptr<CategorizationGlobalICHEP> factory(const std::string &name, const std::string &folder);


  private:
    static std::vector<std::unique_ptr<const BinaryDiscriminator>> create_binaries(const std::vector<TTree *> &trees_binary);

  public:
    static std::vector<std::vector<float>> get_boundaries()
    {
      // high threshould means very pure
      // (0) <<-- less pure | -->> more pure (1)

      return {
        {0.550549903826583087},                         // GG2H_0J_PTH_0_10,
        {},                                             // GG2H_0J_PTH_GT10,
        {0.470723736513157853},                         // GG2H_1J_PTH_0_60,
        {0.419736999506578945},                         // GG2H_1J_PTH_60_120,
        {0.377415976819490129},                         // GG2H_1J_PTH_120_200,
        {0.424710960667186832, 0.685689124813637974},   // GG2H_GE2J_MJJ_0_350_PTH_0_60,
        {0.384276765068987725, 0.607896398615586309},   // GG2H_GE2J_MJJ_0_350_PTH_60_120,
        {0.429050325367495933},                         // GG2H_GE2J_MJJ_0_350_PTH_120_200,
        {0.356714342834171583, 0.484657274835125107},   // GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25,
        {0.341296448601795643, 0.534685881238043725},   // GG2H_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25,
        {0.365845794739403096, 0.476773882455851006},   // GG2H_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_0_25,
        {0.302930198203829937, 0.535413948299139686},   // GG2H_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_GT25,
        {0.350926713810855329, 0.534434939851690771},   // GG2H_PTH_200_300,
        {0.452568861995886196, 0.631740337876358860},   // GG2H_PTH_300_450,
        {0.371731351048519676},                         // GG2H_PTH_450_650,
        {0.163816499342105265},                         // GG2H_PTH_GT650,
        {0.370867863589638025},                         // QQ2HQQ_0J,
        {0.374571280079826652, 0.505986757059966319},   // QQ2HQQ_1J,
        {0.362134996385147789, 0.582564257031269905},   // QQ2HQQ_GE2J_MJJ_0_60,
        {0.399260018143503204},                         // QQ2HQQ_GE2J_MJJ_60_120,
        {0.384135480504437465, 0.717393223008045200},   // QQ2HQQ_GE2J_MJJ_120_350,
        {0.537520505458470521},                         // QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_0_25,
        {0.247122390111019802},                         // QQ2HQQ_GE2J_MJJ_350_700_PTH_0_200_PTHJJ_GT25,
        {0.557283044600637378},                         // QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_0_25,
        {0.413476978559411346, 0.614226362587848462},   // QQ2HQQ_GE2J_MJJ_GT700_PTH_0_200_PTHJJ_GT25,
        {0.317701850914884898},                         // QQ2HQQ_GE2J_MJJ_350_700_PTH_GT200,
        {0.506219110562294405},                         // QQ2HQQ_GE2J_MJJ_GT700_PTH_GT200,
        {0.553373413408349002, 0.711087119391870526},   // QQ2HLNU_PTV_0_75,
        {0.385763022255345422},                         // QQ2HLNU_PTV_75_150,
        {0.056579909210526316},                         // QQ2HLNU_PTV_150_250_0J,
        {0.508737613599917649},                         // QQ2HLNU_PTV_150_250_GE1J,
        {0.075823657401315775},                         // QQ2HLNU_PTV_GT250,
        {},                                             // HLL_PTV_0_75,
        {0.682683756152220766, 0.839283510425641754},   // HLL_PTV_75_150,
        {0.322697744901315753},                         // HLL_PTV_150_250_0J,
        {0.482588972206517197},                         // HLL_PTV_150_250_GE1J,
        {0.231250635937500004},                         // HLL_PTV_GT250,
        {0.490979239155678870, 0.699971553905687882},   // TTH_PTH_0_60,
        {0.321403266028710666, 0.601460608144558373},   // TTH_PTH_60_120,
        {0.534796410336143113},                         // TTH_PTH_120_200,
        {0.548725274074835623},                         // TTH_PTH_200_300,
        {0.260896851192434220},                         // TTH_PTH_GT300,
        {0.277447045456414521},                         // THJB,
        {0.345395139802631534}                         // TWH
      };
    }
  };

  class CategorizationHybridICHEP : public CategorizationMultiClassPlusBinaries {

  public:
    /**
      * name: an unique string
      * tree_multiclass: the TTree containing the BDT weights for the multiclass.
      * trees_binary: all the TTree containing the BDT weights for the binary.
      *
      * Note that all the other parameters (weights for the d-optimality, ...) are hard-coded
      * It is suggested to don't use this constructor, but the factory method
      **/
    CategorizationHybridICHEP(const std::string &name,
                              TTree *tree_multiclass,
                              const std::vector<TTree *> &trees_binary);

    static std::vector<std::string> get_multiclass_names() { return CategorizationMultiClassICHEP::get_multiclass_names(); }

    /**
     * Build the object just from the folder where the ROOT files containing the trees for all the BDTs are
     *
     * name: an unique string
     * folder: the folder containing the ROOT files. The file for the multiclass is "model1.root", while the
     *         ones for the binary BDTs are "model2_<cat>" where <cat> is the name of the multiclass-category
     *         (i.e. the stxs-bin)
     **/
    static std::unique_ptr<CategorizationHybridICHEP> factory(const std::string &name, const std::string &folder);

  private:
    static std::vector<std::unique_ptr<const BinaryDiscriminator>> create_binaries(const std::vector<TTree *> &trees_binary);

    static const int n_davide = 27; // multiclass categories in range [0-26] are done with binaries from Davide
    // the rest with binaries from Chen

  public:
    static std::vector<std::vector<float>> get_boundaries()
    {
      // high threshould means very pure
      // (0) <<-- less pure | -->> more pure (1)

      // copy the first n_davide boundaries for binaries from the global
      const std::vector<std::vector<float>> boundaries_global = CategorizationGlobalICHEP::get_boundaries();
      std::vector<std::vector<float>> boundaries(boundaries_global.begin(), boundaries_global.begin() + n_davide);

      // high threshold means very pure
      // (0) <<-- less pure | -->> more pure (1)
      std::vector<std::vector<float>> boundaries_chen = {
        {0.749, 0.940},           // WH
        {0.796, 0.927},
        {0.403},
        {0.870},
        {0.716},
        {0.932},                  // ZH
        {0.751, 0.962},
        {0.638},
        {0.769},
        {0.724},
        {0.441, 0.835},           // TTH
        {0.588, 0.869},
        {0.611, 0.879},
        {0.861},
        {0.679},
        {0.942},                  // THJB
        {0.625}                   // TWH
      };

      boundaries.insert(boundaries.end(), boundaries_chen.begin(), boundaries_chen.end());

      return boundaries;
    }
  };

} // namespace Coupling2020

#endif
