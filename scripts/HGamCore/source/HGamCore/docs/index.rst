.. HGamCore documentation master file, created by
   sphinx-quickstart on Fri Jun  1 13:26:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to HGamCore's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Intro
   Installation
   Examples
   Weights
   TheoryErrors
   Development
   Samples
   Signal
   SSCode
   Documentation

Full APIs
=========

Check out the full APIs `here <apis/>`_ (probably only developers care about this).
