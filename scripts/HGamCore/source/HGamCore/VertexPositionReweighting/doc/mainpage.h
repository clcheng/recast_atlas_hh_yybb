// $Id$
namespace CP { 
/**
@mainpage

Tool to reweight the <i>z</i>-position of the primary vertex.<br/>
See the documentation of the VertexPositionReweightingTool class for <a href="classCP_1_1VertexPositionReweightingTool.html#details">details</a>.

Example usage (RootCore):
-------------------------

@code
CP::VertexPositionReweightingTool vtxTool( "VertexPosition" );
vtxTool.setProperty("DataTag", "IndetBeampos-14TeV-SigmaXY12um-SigmaZ50mm-001");
// or:
// vtxTool.setProperty("DataMean", 0.0)
// vtxTool.setProperty("DataSigma", 53.0);


// For each event:

double vtxWeight = 1.0;
switch ( vtxTool.getWeight( vtxWeight ) ) {
   case CP::CorrectionCode::Error:
      Error( APP_NAME, "Error getting vtx correction" );
      break;
   case CP::CorrectionCode::OutOfValidityRange:
      Info( APP_NAME, "Warning, out of validity range" );
      break;
   case CP::CorrectionCode::Ok:
      break;
}
@endcode

Using plain ntuples:
--------------------

It is also possible to use this tool with "plain ntuples", i.e. plain Root trees without xAOD.  To do this, just create an instance of the tool as usual.  Make sure that you also set \ref VertexPositionReweightingTool::McMean "McMean" and \ref VertexPositionReweightingTool::McSigma "McSigma", since these are usually read from the xAOD.  Then call \ref VertexPositionReweightingTool::getWeight(const double vxp_z, double& weight).  To get `vxp_z`, you can use \ref VertexPositionReweightingTool::getVertexPosition() when creating the ntuple from an xAOD.

Todo / Limitations
--------------------
 - Currently, the MC beamspot is determined only once per job, in the first event.  If multiple files with different generated beamspots are processed, this has to be changed.
 - The tool only takes a conditions tag for data, but not a run number.  The conditions database has per-run-number beamspot info for certain tags, though.

Further Information
--------------------
\author Jason Mansour <jmansour@cern.ch>

@htmlinclude used_packages.html

@include requirements

*/
// vim: filetype=doxygen
}
