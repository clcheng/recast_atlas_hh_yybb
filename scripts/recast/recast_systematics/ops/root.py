import numpy as np

import ROOT

macro_get_cumulative_data = \
"""
std::pair<std::vector<double>, std::vector<double>> GetCulmulativeData(TH1D* h)
{
    const int n_bin = h->GetNbinsX();
    std::vector<double> bin_content_vec;
    std::vector<double> bin_error_vec;
    bin_content_vec.reserve(n_bin);
    bin_error_vec.reserve(n_bin);
    double content_sum = 0.;
    double error2_sum = 0.;
    for (int bin_index = 1; bin_index <= n_bin; bin_index++){
        content_sum += h->GetBinContent(bin_index);
        error2_sum += pow(h->GetBinError(bin_index), 2);
        bin_content_vec.push_back(content_sum);
        bin_error_vec.push_back(std::sqrt(error2_sum));
    }
    std::pair<std::vector<double>, std::vector<double>> result(bin_content_vec, bin_error_vec);
    return result;
}
"""

macro_get_bin_error_array = \
"""
std::vector<double> GetBinErrorArray(TH1* h)
{
    const int n_bin = h->GetNbinsX();
    std::vector<double> result;
    result.reserve(n_bin);
    for (int bin_index = 1; bin_index <= n_bin; bin_index++)
        result.push_back(h->GetBinError(bin_index));
    return result;
}
"""

macro_get_bin_center_array = \
"""
std::vector<double> GetBinCenterArray(TH1* h)
{
    const int n_bin = h->GetNbinsX();
    std::vector<double> result;
    result.reserve(n_bin);
    for (int bin_index = 1; bin_index <= n_bin; bin_index++)
        result.push_back(h->GetBinCenter(bin_index));
    return result;
}
"""

macro_get_bin_content_array = \
"""
std::vector<double> GetBinContentArray(TH1* h)
{
    const int n_bin = h->GetNbinsX();
    std::vector<double> result;
    result.reserve(n_bin);
    for (int bin_index = 1; bin_index <= n_bin; bin_index++)
        result.push_back(h->GetBinContent(bin_index));
    return result;
}
"""

macros_th1 = {
    'GetCulmulativeData': macro_get_cumulative_data,
    'GetBinContentArray': macro_get_bin_content_array,
    'GetBinErrorArray': macro_get_bin_error_array,
    'GetBinCenterArray': macro_get_bin_center_array
}

            
class TH1(object):
    @staticmethod
    def load_macros():
        for macro_name, macro_str in macros_th1.items():
            if not hasattr(ROOT, macro_name):
                ROOT.gInterpreter.Declare(macro_str)
    @staticmethod       
    def GetCulmulativeData(h):
        result = ROOT.GetCulmulativeData(h)
        bin_content = np.array(result.first.data())
        bin_error = np.array(result.second.data())
        return bin_content, bin_error
    @staticmethod
    def GetBinErrorArray(h):
        result = ROOT.GetBinErrorArray(h)
        bin_error = np.array(result.data())
        return bin_error
    @staticmethod
    def GetBinCenterArray(h):
        result = ROOT.GetBinCenterArray(h)
        bin_center = np.array(result.data())
        return bin_center
