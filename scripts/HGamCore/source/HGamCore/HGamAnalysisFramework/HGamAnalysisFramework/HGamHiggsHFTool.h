#pragma once

// STL include(s):
#include <vector>

// EDM include(s):
#include "AsgTools/AsgMessaging.h"

// ROOT includes
#include "TLorentzVector.h"
#include "TMVA/Reader.h"
#include "TString.h"


// Forward declarations
namespace HG { class EventHandler; class TruthHandler; }

namespace HG {
  /*!
     @class HGamHiggsHFTool
     @brief The main class for the Higgs+HF measurement.

     This tool, included in the HGamAnalysisFramework, contains all the functions designed
     specifically for the Higgs+HF measurement.
   */
  class HGamHiggsHFTool : public asg::AsgMessaging {
  public:
    /**
     * @brief Constructor
     * @param eventHandler The HGam eventHandler to write information to EventInfo
     * @param truthHandler The HGam truthHandler to read truth information
     */
    HGamHiggsHFTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler);

    /*! Destructor, deallocates resources used by class instance. */
    virtual ~HGamHiggsHFTool();

    /*!
       @fn virtual EL::StatusCode initialize(Config &config)
       @brief Set or fetch default values for members from config
       @param config
     */
    virtual EL::StatusCode initialize(Config &config);

    /*!
       @enum HiggsHFCutflowEnum
       @brief Latest step in the cutflow that is passed by the event
     */
    enum HiggsHFCutflowEnum {PHOTONS = 0, LEPVETO = 1, CENJET_MIN = 2, CENJET_MAX = 3, BTAG = 4, PASS = 5};

    /**
     * @brief Function to save all the Higgs+HF information
     * @details
     *
     * @param photons Container with all photons
     * @param jets Container with all jets
     * @param jets_noJVT Container with all jets, no JVT cut
     * @param muons Container with all muons
     * @param electron Container with all electrons
     */
    void saveHGamHiggsHFInfo(const xAOD::PhotonContainer &photons,
                             const xAOD::JetContainer &jets,
                             xAOD::JetContainer &jets_noJVT,
                             xAOD::MuonContainer &muons,
                             xAOD::ElectronContainer &electrons);

    /**
     * @brief Function to save all the Higgs+HF truth information
     * @details
     *
     * @param photons Container with truth photons
     * @param jets Container with truth jets
     * @param bjet Container with truth jets matched to a b-hadron
     * @param cjet Container with truth jets matched to a c-hadron
     * @param muons Container with truth muons
     * @param electron Container with truth electrons
     */
    void saveHGamHiggsHFTruthInfo(const xAOD::TruthParticleContainer &photons,
                                  const xAOD::JetContainer   &jets,
                                  const xAOD::JetContainer   &bjets,
                                  const xAOD::JetContainer   &cjets,
                                  xAOD::TruthParticleContainer &muons,
                                  xAOD::TruthParticleContainer &electrons);


  private:
    HG::EventHandler *m_eventHandler; //!
    HG::TruthHandler *m_truthHandler; //!
    TMVA::Reader  *m_TMVAreader_HiggsHF; //!


    // User-configurable selection options;
    double m_minMuonPT;             //! Minimum pT of muons for lepton veto (in GeV)
    double m_minElectronPT;         //! Minimum pT of electrons for lepton veto (in GeV)
    float m_centraljet_eta_max;     //! Maximum eta of central jets
    float m_truthjet_rapidity_max;  //! Maximum eta of truth jets
    float m_centraljet_pt_min;      //! Minimum  pt of central jets (in GeV)

    // BDT input variables
    float t_lead_btag_jet_bin;
    float t_deltaR_y2_j1;
    float t_deltaR_y1_j1;
    float t_deltaR_y2_j2;
    float t_deltaR_y1_j2;
    float t_deltaR_yy;
    float t_deltaEta_yy;
    float t_effmass_all;
    float t_foxwolfram1_all;

    static constexpr float m_massHiggs = 125.09; //! what we consider the exact Higgs mass to be (in GeV)

    /**
     * @brief Accessor for event handler
     */
    virtual HG::EventHandler *eventHandler();

    /**
     * @brief Accessor for truth handler
     */
    virtual HG::TruthHandler *truthHandler();

    /*!
       \fn void saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString,int> &eventInfoInts)
       \brief Write maps to eventInfo
     */
    void saveMapsToEventInfo(std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);
    void saveMapsToTruthEventInfo(std::map<TString, int> &eventInfoInts);

    /*!
       \fn void performSelection(xAOD::PhotonContainer photons, xAOD::JetContainer& jets, xAOD::JetContainer& jets_noJVT, xAOD::MuonContainer& muons, xAOD::ElectronContainer& electrons, std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);
       \brief Function which performs the Higgs+HF preselection and saves info to info maps (nominal by default)
       \param photons Container with all photons
       \param jets Container with all jets
       \param jets_noJVT Container with all jets and no JVT cut
       \param muons Container with all muons
       \param electron Container with all electrons
       \param eventInfoFloats Map which has selected float info appended to it for saving to file
       \param eventInfoInts Map which has selected ints info appended to it for saving to file
       \param label String to include in the addtional information
     */
    void performSelection(const xAOD::PhotonContainer &photons,
                          const xAOD::JetContainer &jets,
                          xAOD::JetContainer &jets_noJVT,
                          xAOD::MuonContainer &muons,
                          xAOD::ElectronContainer &electrons,
                          std::map<TString, float> &eventInfoFloats,
                          std::map<TString, int> &eventInfoInts);

    /*!
       \fn     void performTruthSelection(xAOD::TruthParticleContainer photons, xAOD::JetContainer   &jets,xAOD::JetContainer   &bjets, xAOD::JetContainer   &cjets, xAOD::TruthParticleContainer &muons, xAOD::TruthParticleContainer &electrons, std::map<TString, float> &eventInfoFloats, std::map<TString, int> &eventInfoInts);
       \brief Function which performs the Higgs+HF preselection and saves info to info maps (nominal by default)
       \param photons Container with truth photons
       \param jets Container with truth jets
       \param bjet Container with truth jets matched to a b-hadron
       \param cjet Container with truth jets matched to a c-hadron
       \param muons Container with truth muons
       \param electron Container with truth electrons
       \param eventInfoFloats Map which has selected float info appended to it for saving to file
       \param eventInfoInts Map which has selected ints info appended to it for saving to file
       \param label String to include in the addtional information
     */

    void performTruthSelection(const xAOD::TruthParticleContainer &photons,
                               const xAOD::JetContainer   &jets,
                               const xAOD::JetContainer   &bjets,
                               const xAOD::JetContainer   &cjets,
                               xAOD::TruthParticleContainer &muons,
                               xAOD::TruthParticleContainer &electrons,
                               std::map<TString, int> &eventInfoInts);


    /*!
       \fn double weightJVT(HGamHiggsHFTool::massCatEnum massCat, xAOD::JetContainer &jets_noJVT)
       \brief Fetch JVT (in)efficiency SF
       \param jets_noJVT The jet container for jets before JVT is applied.
     */
    double weightJVT(xAOD::JetContainer &jets_noJVT);

    // Get BDT weight
    float getMVAWeight(TMVA::Reader *xReader);

    // Functions which calculate different event shape variables
    float GetEffectiveMass(std::vector<TLorentzVector> fPart);
    float GetFoxWolframMoment(std::vector<TLorentzVector> fPart, int order);

  };

}
