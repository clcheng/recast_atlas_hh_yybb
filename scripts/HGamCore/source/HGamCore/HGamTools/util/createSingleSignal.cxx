////////////////////////////////////////////////////////////////////////////////
//                                                                            //
//  createSingleSignal.cxx                                                    //
//                                                                            //
//  Author: Andrew Hard                                                       //
//  Date: 29/01/2016                                                          //
//  Email: ahard@cern.ch                                                      //
//                                                                            //
//  This main method provides a tool for performing individual fits to the    //
//  resonance Monte Carlo. Settings for the utility are provided in           //
//  singleSigFitExample.cfg.                                                  //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

// ROOT include(s):
#include "TCanvas.h"
#include "TChain.h"
#include "TF1.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TLatex.h"
#include "TPad.h"
#include "TRegexp.h"
#include "TString.h"

// Local include(s):
#include "HGamAnalysisFramework/Config.h"
#include "HGamTools/AtlasStyle.h"
#include "HGamTools/RooTwoSidedCBShape.h"
#include "HGamTools/SigParam.h"

/**
   -----------------------------------------------------------------------------
   A simple method for choosing which category a value should fall into, based
   on a specified binning.
   @param value - The value of the parameter.
   @param bining - A list of bin edges into which the value will fall.
   @return - An integer category index starting at zero.
*/
int getCateFromVar(double value, std::vector<double> binning)
{
  for (int i_b = 0; i_b < (int)binning.size() - 1; i_b++) {
    if ((value >= binning[i_b]) && (value < binning[i_b + 1])) { return i_b; }
  }

  return -1;
}

/**
   -----------------------------------------------------------------------------
   Retrieve the total number of events in the file for event normalization:
   @param file - The current MxAOD file in the TChain.
   @param histName - The name of the cutflow
   @param decayMode - The decay mode (noDalitz / onlyDalitz)
   @param binForNorm - The cutflow bin for overall event normalization.
   @param maxCutFlowIndex - An int to store the number of cutflow hist bins.
   @return - The total number of weighted events in the file. Also, the max
   cutFlowIndex passed by reference.
*/
double getNTotEvtFromHist(TFile *file, TString histName, TString decayMode,
                          int binForNorm, int &maxCutFlowIndex)
{
  // Find the cutflow histograms from the file based on limited name info:
  TIter next(file->GetListOfKeys());
  TObject *currObj;

  while ((currObj = (TObject *)next())) {
    TString currName = currObj->GetName();

    if (histName.EqualTo("")) {
      if (currName.Contains("CutFlow") && currName.Contains("weighted")
          && currName.Contains(decayMode)) {
        maxCutFlowIndex = ((TH1F *)file->Get(currName))->GetNbinsX();
        return (((TH1F *)file->Get(currName))->GetBinContent(binForNorm));
      }
    } else {
      TRegexp expression(histName);

      if (currName.Contains(expression)) {
        maxCutFlowIndex = ((TH1F *)file->Get(currName))->GetNbinsX();
        return (((TH1F *)file->Get(currName))->GetBinContent(binForNorm));
      }
    }
  }

  std::cout << "createSignalParameterization: ERROR! MxAOD doesn't have cutflow"
            << std::endl;
  exit(0);
}

/**
   -----------------------------------------------------------------------------
   Copy files from a slow resource (e.g. EOS) to the local disk for faster
   processing.
   @param fileNames - The original file names.
   @return - An updated list of file names.
*/
std::vector<TString> makeLocalFileCopies(std::vector<TString> fileNames)
{
  std::cout << "createSingleSignal: Making local copies of inputs."
            << std::endl;
  std::vector<TString> result;
  result.clear();

  for (int i_f = 0; i_f < (int)fileNames.size(); i_f++) {
    TString newName = Form("tempFile%d.root", i_f);

    if (fileNames[i_f].Contains("root://eosatlas/")) {
      system(Form("xrdcp %s %s", fileNames[i_f].Data(), newName.Data()));
    } else if (fileNames[i_f].Contains("/eos/atlas/")) {
      system(Form("eos cp %s %s", fileNames[i_f].Data(), newName.Data()));
    } else {
      system(Form("cp %s %s", fileNames[i_f].Data(), newName.Data()));
    }

    result.push_back(newName);
  }

  return result;
}

/**
   -----------------------------------------------------------------------------
   Remove any files that were copied over for speed.
   @param fileNames - The original file names.
*/
void removeLocalFileCopies(std::vector<TString> fileNames)
{
  std::cout << "createSingleSignal: Removing local copies of inputs."
            << std::endl;

  for (int i_f = 0; i_f < (int)fileNames.size(); i_f++) {
    system(Form("rm %s", fileNames[i_f].Data()));
  }
}

/**
   -----------------------------------------------------------------------------
   Prints a progress bar to screen to provide elapsed time and remaining time
   information to the user. This is useful when processing large datasets.
   @param index - The current event index.
   @param total - The total number of events.
*/
void PrintProgressBar(int index, int total)
{
  if (index % 10000 == 0) {
    TString print_bar = " [";

    for (int bar = 0; bar < 20; bar++) {
      double current_fraction = double(bar) / 20.0;

      if (double(index) / double(total) > current_fraction) { print_bar.Append("/"); }
      else { print_bar.Append("."); }
    }

    print_bar.Append("] ");
    double percent = 100.0 * (double(index) / double(total));
    TString text = Form("%s %2.2f ", print_bar.Data(), percent);
    std::cout << text << "%\r" << std::flush;
  }
}

/**
   -----------------------------------------------------------------------------
   The main method for this utility. Provide 1 argument - the location of the
   config (.cfg) file, which should be stored in the data/ directory. The main()
   method runs over the samples provided, performs the fits requests, and gives
   comparisons of parameterized and non-parameterized fits.
*/
int main(int argc, char *argv[])
{
  // Check that the config file location is provided.
  if (argc < 2) { HG::fatal("No arguemnts provided"); }

  //HG::Config settings(TString(argv[1]));
  HG::Config *settings = new HG::Config(TString(argv[1]));

  // Print configuration for benefit of user:
  std::cout << "createSingleSignal will run with parameters:"
            << std::endl;
  settings->printDB();

  // Set the function type:
  TString function = settings->getStr("SignalFunctionalForm");

  // Check that output directory exists:
  TString outputDir = settings->getStr("OutputDir");
  system(Form("mkdir -vp %s", outputDir.Data()));

  // Set the ATLAS Style for plots:
  SetAtlasStyle();

  // Instantiate SigParam class for individual & parameterized fits:
  SigParam *sps
    = new SigParam(settings->getStr("SampleName"), outputDir + "/Individual");
  // Set the print level of the signal parameterization tool:
  sps->verbosity(settings->getBool("Verbose"));

  // Also set some of the plot formatting for the signal tool:
  sps->setPlotFormat(settings->getStr("PlotFileFormat"));
  sps->setPlotATLASLabel(settings->getStr("ATLASLabel"));


  // calculate the total luminosity
  float multiLuminosity = 0.0;
  std::vector<double> luminosities = settings->getNumV("Luminosities");

  for (int i = 0; i < (int)luminosities.size(); i++) {
    multiLuminosity += luminosities[i] / 1000.0 ;
  }

  sps->setPlotLuminosity(Form("%2.2f fb^{-1}", multiLuminosity));


  sps->setPlotXAxisTitle(settings->getStr("XAxisTitle"));

  // Use custom initial values and ranges for certain fit parameters:
  if (settings->isDefined("DefinedParams")) {
    std::vector<TString> params = settings->getStrV("DefinedParams");

    for (int i_p = 0; i_p < (int)params.size(); i_p++) {
      sps->setParamState(params[i_p], settings->getStr("Param_" + params[i_p]));
    }
  }

  // Prepare for loop over input MxAOD/TTree:
  std::vector<TString> fileNames = settings->getStrV("InputFile");

  // Make local copies of files if requested, to improve speed:
  if (settings->getBool("MakeLocalCopies")) {
    fileNames = makeLocalFileCopies(fileNames);
  }

  // Create TChain of input files:
  TChain *chain = new TChain(settings->getStr("TreeName"));

  for (int i_f = 0; i_f < (int)fileNames.size(); i_f++) {
    chain->AddFile(fileNames[i_f]);
  }


  double nTotEvt = 1000.0;
  TString currFileName = "";

  // Index set by index of last bin in MxAOD cutflow hist
  int cutFlowIndex = 0;

  // Get the single value of the resonance mass from file:
  float resMass = (double)settings->getNum("ResonanceMass") / 1000.0;

  // In case the cutflow histogram is unorthodox:
  TString decayMode = "noDalitz";
  TString histName = "";

  if (settings->isDefined("DecayMode")) {
    decayMode = settings->getStr("DecayMode");
  }

  if (settings->isDefined("CutFlowHistName")) {
    histName = settings->getStr("CutFlowHistName",
                                "^CutFlow_.+_" + decayMode + "_weighted$");
  }

  // Assign the MxAOD/TTree branches to variables:
  float v_mass;
  float v_weight;
  float v_xsbreff;
  int v_cutFlow;
  float v_cate;
  int v_cateIndex;
  chain->SetBranchAddress(settings->getStr("MassBranchName"), &v_mass);
  chain->SetBranchAddress(settings->getStr("WeightBranchName"), &v_weight);
  chain->SetBranchAddress(settings->getStr("XSBREffBranchName"), &v_xsbreff);
  chain->SetBranchAddress(settings->getStr("CutFlowBranchName"), &v_cutFlow);

  if (settings->isDefined("CategoryBranchName")) {
    chain->SetBranchAddress(settings->getStr("CategoryBranchName"),
                            &v_cateIndex);
  } else if (settings->isDefined("VariableForCategories")) {
    chain->SetBranchAddress(settings->getStr("VariableForCategories"), &v_cate);
  } else {
    std::cout << "createSingleSignal: ERROR! Must provide VariableForCategories"
              << " or CategoryBranchName." << std::endl;
    exit(0);
  }

  int nEvents = chain->GetEntries();
  int nCategories = 0;

  //--------------------------------------//
  // Loop over events to build dataset for signal shape fitting:
  std::cout << "There are " << nEvents << " events to process." << std::endl;

  std::vector<TString> mcTypes = settings->getStrV("MCtype");

  if (mcTypes.size() != luminosities.size()) {
    std::cout << "number of mc types is not equal to the number of luminosities specified! exiting.. " << std::endl;
    exit(0);
  }

  // initialize luminosity to the first value
  float luminosity = luminosities[0];

  for (int index = 0; index < nEvents; index++) {
    chain->GetEntry(index);
    PrintProgressBar(index, nEvents);

    // Change the nTotEvt normalization factor for each new file:
    if (!currFileName.EqualTo(chain->GetFile()->GetName())) {
      currFileName = chain->GetFile()->GetName();


      if (settings->isDefined("CutFlowIndexForNorm")) {
        nTotEvt = getNTotEvtFromHist(chain->GetFile(), histName, decayMode,
                                     settings->getInt("CutFlowIndexForNorm"),
                                     cutFlowIndex);
      } else {
        nTotEvt = getNTotEvtFromHist(chain->GetFile(), histName, decayMode,
                                     3, cutFlowIndex);
      }


      for (int i = 0; i < (int)mcTypes.size(); i++) {

        if (currFileName.Contains(mcTypes[i])) {
          luminosity = luminosities[i];
        }

      }
    }


    // Only use events passing the full selection:
    if ((settings->isDefined("CutFlowIndex") &&
         v_cutFlow < settings->getInt("CutFlowIndex")) ||
        (!settings->isDefined("CutFlowIndex") &&
         v_cutFlow < cutFlowIndex)) { continue; }

    // The category index fed into the SigParam tool should start at 0.
    int currCate = 0;

    if (settings->isDefined("CategoryBranchName")) {
      currCate = v_cateIndex - 1;
    } else if (settings->isDefined("VariableForCategories")) {
      currCate = getCateFromVar(v_cate, settings->getNumV("VariableBins"));
    }

    if (currCate < 0) { continue; }

    if (currCate >= nCategories) { nCategories = currCate + 1; }

    // The observed mass fed into the SigParam tool should be in GeV:
    double massToUse = (settings->getStr("MassBranchUnits") == "GeV") ?
                       ((double)v_mass) : ((double)v_mass / 1000.0);

    if (settings->getBool("AdditionalWeight")) {
      float additional_weight;
      chain->SetBranchAddress(settings->getStr("AdditionalWeightName"), &additional_weight);
      v_weight = v_weight * additional_weight;
    }

    // Calculate the weight to use:
    double weightToUse = luminosity * v_xsbreff * v_weight / nTotEvt;

    // Add the mass and weight values to the datasets for fitting:
    sps->addMassPoint(resMass, currCate, massToUse, weightToUse);
  }

  //--------------------------------------//
  // Now fit and plot the resonance shapes!
  std::cout << "createSingleSignal: Start fitting and plotting!"
            << std::endl;

  // Check which fits failed or succeeded:
  std::vector<TString> fitFailures;
  fitFailures.clear();
  std::vector<TString> fitSuccesses;
  fitSuccesses.clear();

  // Set the category names for output plots and tables:
  sps->nameTheCategories(settings->getStrV("CategoryNames"));

  // Loop over the analysis categories:
  for (int i_c = 0; i_c < nCategories; i_c++) {
    // The command to perform the fit returns "true" iff. the fit succeeds:
    if (sps->makeSingleResonance(resMass, i_c, function)) {
      sps->plotSingleResonance(resMass, i_c);
      fitSuccesses.push_back(Form("mass=%2.2f GeV in category %d", resMass, i_c));
    } else {
      std::cout << "createSingleSignal: Fit at mRes=" << resMass
                << ", cate=" << i_c << " did not converge :(" << std::endl;
      fitFailures.push_back(Form("mass=%2.2f GeV in category %d", resMass, i_c));
    }
  }

  // Save the fit results:
  sps->saveAll();

  // Print LaTex table:
  sps->printResTable(resMass);

  // Print some values for category 0 at the first defined mass point:
  std::cout << "\ncreateSingleSignal: Printing parameters for cat 0, mH = "
            << resMass << " GeV" << std::endl;
  std::vector<TString> names = sps->getVariableNames(resMass, 0);

  for (int i_n = 0; i_n < (int)names.size(); i_n++) {
    std::cout << "\t" << names[i_n] << " = "
              << sps->getParameterValue(names[i_n], resMass, 0) << std::endl;
  }

  // Remove any files copied locally for speed:
  if (settings->getBool("MakeLocalCopies")) { removeLocalFileCopies(fileNames); }

  // Print out a list of good and bad fits:
  std::cout << "createSingleSignal: Printing Summary" << std::endl;
  std::cout << "\tFits that succeeded (" << (int)fitSuccesses.size()
            << " total)" << std::endl;

  for (int i_s = 0; i_s < (int)fitSuccesses.size(); i_s++) {
    std::cout << "\t\t" << fitSuccesses[i_s] << std::endl;
  }

  std::cout << "\tFits that failed (" << (int)fitFailures.size() << " total)"
            << std::endl;

  for (int i_f = 0; i_f < (int)fitFailures.size(); i_f++) {
    std::cout << "\t\t" << fitFailures[i_f] << std::endl;
  }

  /*
  // New! Create Asimov data from the fitted PDF, or toy MC from either the
  // fitted PDF or the input dataset:
  std::cout << "createSingleSignal: Generating Asimov, pdf toy, and mc toy data"
      << std::endl;
  std::cout << "For reference, in data:" << std::endl;
  std::cout << "\tMean(MC)=" << sps->getMeanOrStdDevInData("Mean",resMass,0)
      << std::endl;
  std::cout << "\tRes(MC)=" << sps->getMeanOrStdDevInData("StdDev",resMass,0)
      << std::endl;

  TString generatedDataTypes[3] = {"asimov", "pdftoy", "mctoy"};
  for (int i_g = 0; i_g < 3; i_g++) {
    int seed = 23857; // A random number for toy MC generation
    if (sps->generateAndFitData(resMass, 0, generatedDataTypes[i_g], seed)) {
      std::cout << "createSingleSignal: " << generatedDataTypes[i_g]
    << " data has been created and fitted. Results:" << std::endl;
      std::cout << "\tMean=" << sps->getMeanOrStdDev("Mean",resMass,0)
    << std::endl;
      std::cout << "\tRes=" << sps->getMeanOrStdDev("StdDev",resMass,0)
    << std::endl;
      sps->plotSingleResonance(resMass, 0, generatedDataTypes[i_g]);
    }
  }
  */

  // Then print the signal yield from parameterization at resonance mass:
  std::cout << "The inclusive signal yield at mH=" << resMass << " GeV is: "
            << sps->getYieldTotal(resMass) << " for "
            << multiLuminosity << " fb-1" << std::endl;

  return 0;
}
