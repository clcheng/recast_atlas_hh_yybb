#include "HGamAnalysisFramework/BJetCalib.h"
#include "AthContainers/AuxElement.h"

namespace HG {

  BJetCalib *BJetCalib::m_ptr = nullptr;

  BJetCalib *BJetCalib::getInstance()
  {
    if (m_ptr == nullptr)
    { m_ptr = new BJetCalib(); }

    return m_ptr;
  }

  void BJetCalib::applyPtRecoEnergyCorrection(xAOD::Jet *jet, bool debug)
  {
    static SG::AuxElement::Accessor<float> m_ptRecoScale("PtRecoSF");
    double ptSF = m_ptRecoScale(*jet);

    if (debug > 0) {
      printf("Retrieving original four vector pt = %6.2f eta = %3.2f phi = %3.2f, m = %6.2f, PtRecoSF = %4.3f\n",
             jet->jetP4().Pt() * 1e-3,
             jet->jetP4().Eta(), jet->jetP4().Phi(),
             jet->jetP4().M() * 1e-3,
             ptSF);
    }

    jet->setJetP4((jet->jetP4())*ptSF);

    if (debug > 0) {
      printf("Corrected four vector pt = %6.2f eta = %3.2f phi = %3.2f, m = %6.2f\n",
             jet->jetP4().Pt() * 1e-3,
             jet->jetP4().Eta(), jet->jetP4().Phi(),
             jet->jetP4().M() * 1e-3);
    }
  }

  void BJetCalib::applyBJetEnergyCorrection(xAOD::Jet *jet, TString scaleName, bool applypTreco, bool debug)
  {
    static SG::AuxElement::Accessor<float> m_ptRecoScale("PtRecoSF");
    double ptSF   = 1;

    if (debug > 0) {
      printf("Retrieving original four vector pt = %6.2f eta = %3.2f phi = %3.2f, m = %6.2f\n",
             jet->jetP4().Pt() * 1e-3,
             jet->jetP4().Eta(), jet->jetP4().Phi(),
             jet->jetP4().M() * 1e-3);
    }

    if (!scaleName.Contains("NN")) {
      if (applypTreco)
      { ptSF = m_ptRecoScale(*jet); }

      jet->setJetP4((jet->jetP4() + jet->jetP4(scaleName.Data()))*ptSF);
    } else {
      ptSF = jet->auxdata<float>((scaleName + "PtSF").Data());
      xAOD::JetFourMom_t new_jet(ptSF * jet->pt(), jet->eta(), jet->phi(), jet->m());
      jet->setJetP4(new_jet);
    }

    if (debug > 0) {
      if (!scaleName.Contains("NN")) {
        printf("Retrieving correction four vector pt = %6.2f eta = %3.2f phi = %3.2f, m = %6.2f, pTrecoSF = %5.4f\n",
               jet->jetP4(scaleName.Data()).Pt() * 1e-3,
               jet->jetP4(scaleName.Data()).Eta(), jet->jetP4(scaleName.Data()).Phi(),
               jet->jetP4(scaleName.Data()).M() * 1e-3, ptSF);
      } else {
        printf("Retrieving correction factor ptSF = %5.4f\n", ptSF);
      }

      printf("Corrected four vector %s pt = %6.2f eta = %3.2f phi = %3.2f, m = %6.2f\n",
             scaleName.Data(),
             jet->jetP4().Pt() * 1e-3,
             jet->jetP4().Eta(), jet->jetP4().Phi(),
             jet->jetP4().M() * 1e-3);
    }
  }

  void BJetCalib::undoBJetEnergyCorrection(xAOD::Jet *jet)
  {
    jet->setJetP4((jet->jetP4("DefScale")));
  }
  void BJetCalib::undoBJetEnergyCorrection(xAOD::JetContainer &jetC)
  {
    for (auto jet : jetC)
    { this->undoBJetEnergyCorrection(jet); }
  }
}
