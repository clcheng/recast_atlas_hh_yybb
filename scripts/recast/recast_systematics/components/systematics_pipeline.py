import os
import re
import json
import shutil

from recast_systematics.utils.root import load_macro, is_corrupt
from multiprocessing import Pool

import ROOT

# ugly fix for multiprocessing.Pool pickle issue
def create_trees(args):
    sample_name = args[0]
    syst_theme = args[1]
    input_path = args[2]
    outdir = args[3]
    version = args[4]
    executable = "runtool_Experimental_Systematics"
    cmd = ("{executable} InputFileList: {input_path}/{sample_name}.txt SampleName: {sample_name} " + \
          "OutputDir: {outdir}/{sample_name}/ {input_path}/tool_Experimental_Systematics_{syst_theme}_{version}.config").format(
          executable=executable, input_path=input_path, outdir=outdir, version=version, syst_theme=str(syst_theme), 
          sample_name=str(sample_name))
    os.system(cmd)

class SystematicsPipeline:
    _NON_RES_CATEGORIES_ = ["XGBoost_btag77_withTop_BCal_tightScore_HMass",
                            "XGBoost_btag77_withTop_BCal_looseScore_HMass",
                            "XGBoost_btag77_withTop_BCal_tightScore_LMass",
                            "XGBoost_btag77_withTop_BCal_looseScore_LMass"]
    _RES_MASS_ = [251, 260, 280, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000]
    _RES_CATEGORIES_ = ["Resonant_mX{}".format(m) for m in _RES_MASS_]
    
    _MAX_SYST_PROC_ = ["gg_HH_resonant", "ggH_in_HH_resonant", "ZH_in_HH_resonant", "ttH_in_resonant", "gg_HH_non_resonant", 
                       "vbf_HH_non_resonant", "gg_HH_non_resonant_in_HH_resonant", "vbf_HH_non_resonant_in_HH_resonant"]
    

    @property
    def processes(self):
        return self._processes
    
    @processes.setter
    def processes(self, vals):
        if vals is None:
            self._processes = self.all_processes
        elif isinstance(vals, (str, list)):
            if isinstance(vals, str):
                vals = vals.split(',')
            for proc in vals:
                if proc not in self.all_processes:
                    raise ValueError('process `{}` not found in systematics config'.format(proc))
            self._processes = vals
        else:
            raise ValuError("unknown input`{}`".format(vals))
    
    
    @property
    def systematics_themes(self):
        return self._systematics_themes
    
    @systematics_themes.setter
    def systematics_themes(self, vals):
        if vals is None:
            self._systematics_themes = self.all_systematics_themes
        elif isinstance(vals, (str, list)):
            if isinstance(vals, str):
                vals = vals.split(',')
            for theme in vals:
                if theme not in self.all_systematics_themes:
                    raise ValueError('systematics theme `{}` not found in systematics config'.format(proc))
            self._systematics_themes = vals
        else:
            raise ValuError("unknown input`{}`".format(vals)) 

    def __init__(self, base_path="./", input_path="inputs", 
                 processes=None, syst_themes=None, merged_prefix="h026_mc16a_h026_mc16d_h026_mc16e",
                 version="h026"):
        self.version = version
        self.base_path = base_path
        self.input_path = input_path
        if not os.path.exists(input_path):
            raise RuntimeError('input path "{}" does not exist'.format(input_path))
        if not os.path.isdir(input_path):
            raise RuntimeError('input path "{}" is not a directory'.format(input_path))
        syst_config_path = os.path.join(input_path, "systematics_config.json")
        if not os.path.exists(syst_config_path):
            raise RuntimeError('systematics configuration file `{}` does not exist'.format(syst_config_path))
        self.syst_config = json.load(open(syst_config_path))
        self.all_processes = sorted(list(self.syst_config["processes"]))
        self.all_systematics_themes = sorted(list(self.syst_config["systematics"]))
        self.systematics_maps = self.syst_config["systematics"]
        self.process_map = self.syst_config["processes"]
        self.input_maps = self.syst_config["inputs"]
        self.processes = processes
        self.systematics_themes = syst_themes
        self.merged_prefix = merged_prefix
        print('INFO: Initialized pipeline for evaluating systematics from MxAODs.')
        print('INFO: Processes to evaluate: {}'.format(','.join(self.processes)))
        print('INFO: Systametics to evaluate: {}'.format(','.join(self.systematics_themes)))
        
    def check_tree_status(self, sample_name, syst_theme, check_corrupt=False):
        tree_base_path = self.get_tree_base_path(sample_name)
        # tree production not started
        if not os.path.exists(tree_base_path):
            return 0
        tree_path = self.get_tree_path(sample_name, syst_theme)
        # tree production aborted midway
        if not os.path.exists(tree_path):
            return -1
        if check_corrupt and ROOT.TFile(tree_path).IsZombie():
            return -1        
        # tree produced
        return 1
    
    def get_tree_path(self, sample_name, syst_theme):
        tree_base_path = self.get_tree_base_path(sample_name)
        tree_path = os.path.join(tree_base_path, "trees_and_hist", syst_theme, "{}.root".format(sample_name))
        return tree_path
    
    def get_tree_base_path(self, sample_name):
        return os.path.join(self.base_path, sample_name)    
    
    def remove_tree_base_path(self, sample_name):
        if re.match(r'^\w+$', str(sample_name)):
            tree_base_path = self.get_tree_base_path(sample_name)
            print('WARNING: Removing directory "{}"'.format(tree_base_path))
            shutil.rmtree(tree_base_path)
        else:
            raise ValueError("Invalid sample name: {}".format(sample_name))
    
    def get_tree_tasks(self, check_corrupt=False):
        finished = []
        unfinished = []
        if check_corrupt:
            print("INFO: Checking for corrupted files...")
        for process in self.processes:
            if process not in self.input_maps:
                continue
            for syst_theme in self.systematics_themes:
                if syst_theme not in self.input_maps[process]:
                    continue
                for sample_name in self.input_maps[process][syst_theme]:
                    tree_status = self.check_tree_status(sample_name, syst_theme, 
                                                         check_corrupt=check_corrupt)
                    if tree_status in [0, -1]:
                        unfinished.append((sample_name, syst_theme))
                    else:
                        finished.append((sample_name, syst_theme))
        return finished, unfinished
    
    def get_merged_tree_name(self, process, syst_theme):
        if process not in self.process_map:
            raise ValueError('process "{}" not found in the merge list'.format(process))
        merged_tree_name = "{}_{}_{}.root".format(self.merged_prefix, self.process_map[process], syst_theme)
        return merged_tree_name
    
    def hadd_trees(self, process, syst_theme, cache=True):
        if process not in self.input_maps:
            raise ValueError('process "{}" not found in the input list'.format(process))
        if syst_theme not in self.input_maps[process]:
            print('INFO: No specification for the "{}" systematics for the process "{}" is found in '
                             'the input list. Skipped.'.format(syst_theme, process))
            return None
        samples = self.input_maps[process][syst_theme]
        tree_paths = [self.get_tree_path(s, syst_theme) for s in samples]
        unfinished_samples = [s for s in samples if self.check_tree_status(s, syst_theme) != 1]
        print('INFO: Merging trees for the "{}" systematics for the process "{}"'.format(syst_theme, process))
        if len(unfinished_samples) > 0:
            print('Tree missing for the following sample(s):')
            print("\n".join(unfinished_samples))
            print("Skip merging.")
            return None
        merged_tree_name = self.get_merged_tree_name(process, syst_theme)
        merged_tree_dir = os.path.join(self.base_path, 'trees', syst_theme)
        if not os.path.exists(merged_tree_dir):
            os.makedirs(merged_tree_dir)
        merged_tree_path = os.path.join(self.base_path, 'trees', syst_theme, merged_tree_name)
        if os.path.exists(merged_tree_path):
            if cache:
                print("INFO: Merged tree already exists. Skipped.")
                return None
            else:
                print('INFO: Removing existing merged tree "{}"'.format(merged_tree_path))
                os.remove(merged_tree_path)
        cmd = "hadd -f {} {}".format(merged_tree_path, " ".join(tree_paths))
        print(cmd)
        os.system(cmd)
        
    def hadd_all_trees(self, cache=True):
        for process in self.processes:
            for syst_theme in self.systematics_themes:
                self.hadd_trees(process, syst_theme, cache=cache)

    def create_all_trees(self, parallel=True, cache=True):
        task_list = []
        finished, unfinished = self.get_tree_tasks(check_corrupt=True)
        for task in unfinished:
            tree_status = self.check_tree_status(*task)
            sample_name = task[0]
            if (tree_status == -1):
                print("INFO: Found unfinished run for the sample {}.".format(sample_name))
                self.remove_tree_base_path(sample_name)
        if not cache:
            for task in finished:
                sample_name = task[0]
                print("INFO: Found finished run for sample {}. "
                      "It will be rerun since cache is disabled".format(sample_name))                
                self.remove_tree_base_path(sample_name)
        finished, unfinished = self.get_tree_tasks()
        for task in unfinished:
            sample_name = task[0]
            syst_theme = task[1]
            task_args = (sample_name, syst_theme, self.input_path, self.base_path, self.version)
            task_list.append(task_args)
        if parallel:
            nProc = os.cpu_count()
            Pool(nProc).map(create_trees, task_list)
        else:
            for task in task_list:
                create_trees(task)