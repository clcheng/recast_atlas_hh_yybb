#ifndef HGAMCATEGORYTOOLBINARYDISCRIMINATOR2020_H
#define HGAMCATEGORYTOOLBINARYDISCRIMINATOR2020_H

#include "HGamAnalysisFramework/VarHandler.h"
#include "MVAUtils/BDT.h"

#include <vector>
#include <ostream>

class TTree; // forward

namespace Coupling2020 {

  struct CategorizationInputs; // forward

  /**
     * Base class for a binary (signal/background) disciminator.
     * It compute the value of the discrimination, usually expected between 0 and 1
     * 0 is background like, 1 is signal like
     **/
  class BinaryDiscriminator {
  public:
    virtual float get_result(const CategorizationInputs &inputs) const = 0;

  protected:
    virtual std::ostream &format(std::ostream &out) const
    {
      out << "<Unknown binary>";
      return out;
    }
    friend std::ostream &operator<<(std::ostream &out, const BinaryDiscriminator &instance);
  };

  std::ostream &operator<<(std::ostream &out, const BinaryDiscriminator &instance);

  /**
     * Implementation of a BinaryDiscriminator using LGBM
     * The class is still virtual since you need to specify how to compute
     * the input variables to the internal BDT from the generic input to the method
     **/
  class BinaryDiscriminatorLGBM : public BinaryDiscriminator {
  public:
    BinaryDiscriminatorLGBM(TTree *tree);
    virtual float get_result(const CategorizationInputs &inputs) const final;

  protected:
    virtual std::ostream &format(std::ostream &out) const override
    {
      out << "<binary LGBM>";
      return out;
    }

  private:
    MVAUtils::BDT m_bdt;
    virtual std::vector<float> get_input(const CategorizationInputs &inputs) const = 0;
  };

  /**
     * Implementation of a BinaryDiscriminator for the hybrid multiclass+binary
     * In this case the discriminator is already computed, you just need to specify how to
     * get it
     **/
  class BinaryDiscriminatorFromVar : public BinaryDiscriminator {
  public:
    BinaryDiscriminatorFromVar(HG::VarBase<float> &var_score); // HG::VarVase::operator()() is not const
    virtual float get_result(const CategorizationInputs &inputs) const final;

  protected:
    virtual std::ostream &format(std::ostream &out) const override
    {
      out << "<binary from var>";
      return out;
    }

  private:
    HG::VarBase<float> &m_var_score;
  };

  /**
     * Implementation of a BinaryDiscriminator for the hybrid multiclass+binary
     * In this case the discriminator is already computed, you just need to specify how to
     * get it. 1 - the value is returned
     **/
  class BinaryDiscriminatorFromVarComplementary : public BinaryDiscriminator {
  public:
    BinaryDiscriminatorFromVarComplementary(HG::VarBase<float> &var_score); // HG::VarBase::operator()() is not const
    virtual float get_result(const CategorizationInputs &inputs) const final;

  protected:
    virtual std::ostream &format(std::ostream &out) const override
    {
      out << "<binary from var complementary>";
      return out;
    }

  private:
    HG::VarBase<float> &m_var_score;
  };

} // namespace Coupling2020

#endif