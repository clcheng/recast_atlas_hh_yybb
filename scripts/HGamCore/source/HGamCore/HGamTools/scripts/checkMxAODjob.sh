
scriptName=$0

setupIssue() {
  echo
  echo "$1"
  echo
  echo "To setup do:"
  echo "  export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
  echo "  source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
  echo "  localSetupFAX"
  echo "  rcSetup -q"
  echo "  localSetupPandaClient --noAthenaCheck"
  exit 1
}

usage() {
  printf "\n%s\n" "$1"
  printf "\nUsage:\n   %s INPUT HTAG CONFIGFILE\n" "$scriptName"
  printf "\nExample:\n  %s periodJ h010 MxAOD.config\n" "$scriptName"
  printf "\nCONFIGS, see HGamTools/data/  examples:\n  MxAOD.config, MxAODAllSys.config\n"
  printf "\nINPUT, see HGamTools/data/input/  examples:\n  periodC, allMC, PowhegPy8_ggH125, PowhegPy8_VBF145 ...\n\n"
  exit 1
}

sample=$1
htag=$2
cfg=$ROOTCOREBIN/data/HGamTools/$3
[[ ! -e $cfg ]] && usage "Cannot find $3 in this location: $config"
cfgName=${3%.*}
config=HGamTools/$3
pbookOutputFile=pbook.out
pbookOutput=$(pbook <<< $'show()\nquit()\n' &> $pbookOutputFile)

[ "$4" == "-official" ] && official="true"

getOutputDSnameData() {
  ds=$(echo $dataset | cut -f2 -d:)
  ds2=$(echo $ds | awk -F '.merge' {'print $1'})
  ds3=$(echo $ds2 | awk -F '.PhysCont' {'print $1'})
  ptag=${dataset: -6}
  outputDS=user.${RUCIO_ACCOUNT}.${ds3}.${cfgName}.${ptag%?}.${htag}
  [[ "$official" == "true" ]] && outputDS=group.phys-higgs.${ds3}.${cfgName}.${ptag%?}.${htag}
}
getOutputDSnameMC() {
  ptag=${dataset: -6}
  outputDS=user.${RUCIO_ACCOUNT}.${sample}.${cfgName}.${ptag%?}.${htag}
  [[ "$official" == "true" ]] && outputDS=group.phys-higgs.${sample}.${cfgName}.${ptag%?}.${htag}
}

testData() {
getOutputDSnameData
testPanda || return 1
}
testMC() {
getOutputDSnameMC
testPanda || return 1
}

testPanda() {
  jobstatus=$(grep -B 10 "\-\-outDS\=${outputDS} " $pbookOutputFile | \
    grep "taskStatus" | awk '{print $3}')
  [ -z "$jobstatus" ] && jobstatus=UNKNOWN
  echo outputDS:  $outputDS
  echo jobStatus: $jobstatus
  if [ "$jobstatus" == "finished" ] || [ "$jobstatus" == "done" ] ; then
    return 0
  else
    return 1
  fi
}

list=$ROOTCOREBIN/data/HGamTools/input/mc.txt
[[ $sample = period* ]] && list=$ROOTCOREBIN/data/HGamTools/input/data.txt
if [[ $sample = periodAll25ns ]]; then
    n=$(grep -c "^period. " $list)
    [[ $n = 0 ]] && usage "No samples of type $sample in $list"
    datasets=$(grep "^period. " $list | awk '{print $2}')
    AllDone="YES"
    for dataset in $datasets ; do
      sample=$(grep "$dataset" $list | awk '{print $1}')
      testData || AllDone="NO" 
    done
    [[ "$AllDone" == "NO" ]] && exit 1

elif [[ $sample = period* ]] ; then
  n=$(grep -c "^$sample " $list)
  [[ $n = 0 ]] && usage "No samples of type $sample in $list"
  datasets=$(grep "^$sample " $list | awk '{print $2}')
  AllDone="YES"
  for dataset in $datasets ; do
    testData || AllDone="NO" 
  done
  [[ "$AllDone" == "NO" ]] && exit 1
elif [[ $sample = allMC ]] ; then
  samples=$(cat $list | grep -v ^\# | awk '{print $1}')
  AllDone="YES"
  for sample in $samples ; do
    n=$(grep -c "^$sample " $list)
    [[ $n -gt 1 ]] && usage "$n samples of type $sample in $list ?"
    dataset=$(grep "^$sample " $list | awk '{print $2}')
    testMC || AllDone="NO"
  done
  [[ "$AllDone" == "NO" ]] && exit 1
else
  n=$(grep -c "^$sample " $list)
  [[ $n = 0   ]] && usage "No samples of type $sample in $list"
  [[ $n -gt 1 ]] && usage "$n samples of type $sample in $list ?"
  dataset=$(grep "^$sample " $list | awk '{print $2}')
  testMC || exit 1
fi
exit 0
