#include "HGamAnalysisFramework/HGamCategoryToolICHEPGlobal.h"
#include "HGamAnalysisFramework/HGamVariables.h"

#include <TFile.h>
#include <TTree.h>

#include <limits>
#include <stdexcept>

using namespace MVAUtils;
using namespace Coupling2020;

namespace Coupling2020::VariablesICHEP {

  template<typename T>
  T var_or_nan(HG::VarBase<T> &var)
  {
    const T value = var();

    if (value == var.get_default()) { return std::numeric_limits<T>::quiet_NaN();}

    return value;
  }

  std::vector<float> get_input_vars(const CategorizationInputs &inputs, const std::vector<InputVariable> &var_ids)
  {
    std::vector<float> result;
    result.reserve(var_ids.size());

    for (const auto var_id : var_ids) {
      switch (var_id) {
        case InputVariable::pT_yy:
          result.push_back(var::pT_yy());
          break;

        case InputVariable::yAbs_yy:
          result.push_back(var::yAbs_yy());
          break;

        case InputVariable::Dphi_y_y:
          result.push_back(var::Dphi_y_y());
          break;

        case InputVariable::Dy_y_y:
          result.push_back(var::Dy_y_y());
          break;

        case InputVariable::pTt_yy:
          result.push_back(var::pTt_yy());
          break;

        case InputVariable::phiStar_yy:
          result.push_back(var::phiStar_yy());
          break;

        case InputVariable::N_j_central:
          result.push_back(var::N_j_central());
          break;

        case InputVariable::N_j:
          result.push_back(var::N_j());
          break;

        case InputVariable::N_j_btag:
          result.push_back(std::min(var::N_j_btag(), 3));
          break;

        case InputVariable::m_jj:
          result.push_back(var_or_nan(var::m_jj));
          break;

        case InputVariable::pT_yyjj:
          result.push_back(var_or_nan(var::pT_yyjj));
          break;

        case InputVariable::N_j_central30:
          result.push_back(var::N_j_central30());
          break;

        case InputVariable::N_j_30:
          result.push_back(var::N_j_30());
          break;

        case InputVariable::N_j_btag30:
          result.push_back(std::min(var::N_j_btag30(), 3));
          break;

        case InputVariable::m_jj_30:
          result.push_back(var_or_nan(var::m_jj_30));
          break;

        case InputVariable::pT_yyjj_30:
          result.push_back(var_or_nan(var::pT_yyjj_30));
          break;

        case InputVariable::pT_jj:
          result.push_back(var_or_nan(var::pT_jj));
          break;

        case InputVariable::pT_j1_30:
          result.push_back(var_or_nan(var::pT_j1_30));
          break;

        case InputVariable::Dy_j_j:
          result.push_back(var_or_nan(var::Dy_j_j));
          break;

        case InputVariable::Dphi_j_j:
          result.push_back(var_or_nan(var::Dphi_j_j));
          break;

        case InputVariable::Deta_j_j:
          result.push_back(var_or_nan(var::Deta_j_j));
          break;

        case InputVariable::pT_yyj:
          result.push_back(var_or_nan(var::pT_yyj));
          break;

        case InputVariable::m_yyj:
          result.push_back(var_or_nan(var::m_yyj));
          break;

        case InputVariable::m_yyjj:
          result.push_back(var_or_nan(var::m_yyjj));
          break;

        case InputVariable::Dphi_yy_jj:
          result.push_back(var_or_nan(var::Dphi_yy_jj));
          break;

        case InputVariable::DRmin_y_j:
          result.push_back(var_or_nan(var::DRmin_y_j));
          break;

        case InputVariable::Dy_yy_jj:
          result.push_back(var_or_nan(var::Dy_yy_jj));
          break;

        case InputVariable::m_alljet:
          result.push_back(var::N_j() >= 3 ? var::m_alljet() : std::numeric_limits<float>::quiet_NaN());
          break;

        case InputVariable::cosTS_yyjj:
          result.push_back(var_or_nan(var::cosTS_yyjj));
          break;

        case InputVariable::N_lep:
          result.push_back(var::N_lep());
          break;

        case InputVariable::N_e:
          result.push_back(var::N_e());
          break;

        case InputVariable::N_mu:
          result.push_back(var::N_mu());
          break;

        case InputVariable::m_ee:
          result.push_back(var_or_nan(var::m_ee));
          break;

        case InputVariable::m_mumu:
          result.push_back(var_or_nan(var::m_mumu));
          break;

        case InputVariable::met_TST:
          result.push_back(var_or_nan(var::met_TST));
          break;

        case InputVariable::sumet_TST:
          result.push_back(var_or_nan(var::sumet_TST));
          break;

        case InputVariable::MET_sig:
          result.push_back(var::met_TST() / 1000. / sqrt(var::sumet_TST() / 1000.));
          break;

        case InputVariable::phi_TST:
          result.push_back(var_or_nan(var::phi_TST));
          break;

        case InputVariable::pT_ll:
          result.push_back(var_or_nan(var::pt_llmax));
          break;

        case InputVariable::pTlepMET:
          result.push_back(var_or_nan(var::pTlepMET));
          break;

        case InputVariable::met_hardVertexTST:
          result.push_back((*inputs.met)["hardVertexTST"]->met());
          break;

        case InputVariable::sumet_hardVertexTST:
          result.push_back((*inputs.met)["hardVertexTST"]->sumet());
          break;

        case InputVariable::MET_sig_hard: {
          const float met_hard = (*inputs.met)["hardVertexTST"]->met();
          const float sumet_hard = (*inputs.met)["hardVertexTST"]->sumet();
          result.push_back(met_hard / 1000. / sqrt(sumet_hard / 1000.));
          break;
        }

        case InputVariable::phi_hardVertexTST:
          result.push_back((*inputs.met)["hardVertexTST"]->phi());
          break;

        case InputVariable::HT_30:
          result.push_back(var_or_nan(var::HT_30));
          break;

        case InputVariable::Zepp:
          result.push_back(var_or_nan(var::Zepp));
          break;

        case InputVariable::mu:
          result.push_back(var::mu());
          break;

        case InputVariable::numberOfPrimaryVertices:
          result.push_back(var::numberOfPrimaryVertices());
          break;

        case InputVariable::score_recotop:
          result.push_back(var_or_nan(var::score_recotop1));
          break;

        case InputVariable::recotop_m:
          result.push_back(var_or_nan(var::m_recotop1));
          break;

        case InputVariable::recotop_phi:
          result.push_back(var_or_nan(var::phi_recotop1));
          break;

        case InputVariable::recotop_eta:
          result.push_back(var_or_nan(var::eta_recotop1));
          break;

        case InputVariable::recotop_pT:
          result.push_back(var_or_nan(var::pT_recotop1));
          break;

        case InputVariable::score_hybrtop:
          result.push_back(var_or_nan(var::score_recotop2));
          break;

        case InputVariable::hybrtop_m:
          result.push_back(var_or_nan(var::m_hybridtop2));
          break;

        case InputVariable::hybrtop_phi:
          result.push_back(var_or_nan(var::phi_hybridtop2));
          break;

        case InputVariable::hybrtop_eta:
          result.push_back(var_or_nan(var::eta_hybridtop2));
          break;

        case InputVariable::hybrtop_pT:
          result.push_back(var_or_nan(var::pT_hybridtop2));
          break;

        default:
          throw std::runtime_error(std::string("variable with code") + std::to_string(static_cast<std::underlying_type_t<InputVariable>>(var_id)) + " not implemented in CategorizationGlobalICHEP");
      }
    }

    /*  for (int ivar = 0; ivar != var_ids.size(); ++ivar) {
        auto index = static_cast<std::underlying_type_t<InputVariable>>(var_ids[ivar]);
        ATH_MSG_INFO("variable: " << input_variable_names[index] << " = " << result[ivar]);
      }
    */

    return result;
  }

}

std::vector<float> CategorizationMultiClassICHEP::get_input(const CategorizationInputs &inputs) const
{
  using namespace Coupling2020::VariablesICHEP;
  static const std::vector<InputVariable> var_ids_multiclass = {
    InputVariable::pT_yy, InputVariable::yAbs_yy,
    InputVariable::N_j_central, InputVariable::N_j, InputVariable::N_j_btag, InputVariable::m_jj, InputVariable::pT_yyjj,
    InputVariable::N_j_central30, InputVariable::N_j_30, InputVariable::N_j_btag30, InputVariable::m_jj_30, InputVariable::pT_yyjj_30,
    InputVariable::pT_jj, InputVariable::pT_j1_30, InputVariable::Dy_j_j, InputVariable::Dphi_j_j, InputVariable::Deta_j_j, InputVariable::pT_yyj, InputVariable::m_yyj, InputVariable::m_yyjj, InputVariable::Dphi_yy_jj, InputVariable::DRmin_y_j, InputVariable::Dy_yy_jj, InputVariable::m_alljet,
    InputVariable::N_lep, InputVariable::m_ee, InputVariable::m_mumu, InputVariable::met_TST, InputVariable::sumet_TST, InputVariable::MET_sig, InputVariable::pT_ll, InputVariable::pTlepMET,
    InputVariable::HT_30,
    InputVariable::mu, InputVariable::numberOfPrimaryVertices,
    InputVariable::score_recotop, InputVariable::recotop_m, InputVariable::recotop_phi, InputVariable::recotop_eta, InputVariable::recotop_pT,
    InputVariable::score_hybrtop, InputVariable::hybrtop_m, InputVariable::hybrtop_phi, InputVariable::hybrtop_eta, InputVariable::hybrtop_pT
  };

  // ATH_MSG_INFO("computing input vars multiclass");
  return get_input_vars(inputs, var_ids_multiclass);

}

std::vector<float> BinaryDiscriminatorICHEPDavide::get_input(const CategorizationInputs &inputs) const
{
  return get_input_vars(inputs, m_var_ids);
}



// comment: can be done simpler with incapsulation instead of inheritance
CategorizationGlobalICHEP::CategorizationGlobalICHEP(const std::string &name,
                                                     TTree *tree_multiclass,
                                                     const std::vector<TTree *> &trees_binary)
  : CategorizationMultiClassPlusBinaries(
      name,
      std::make_unique<CategorizationMultiClassICHEP>(name + "_multiclass", tree_multiclass),
      CategorizationGlobalICHEP::create_binaries(trees_binary),
      CategorizationGlobalICHEP::get_boundaries()
    ) { }

std::vector<std::unique_ptr<const BinaryDiscriminator>>
                                                     CategorizationGlobalICHEP::create_binaries(const std::vector<TTree *> &trees_binary)
{
  using namespace Coupling2020::VariablesICHEP;
  const std::vector<std::vector<InputVariable>> vars_ids_binaries = {
#include "training_vars_multiclass_ichep_binary.def"
  };

  std::vector<std::unique_ptr<const BinaryDiscriminator>> binaries;

  for (unsigned i = 0; i != trees_binary.size(); ++i) {
    binaries.emplace_back(new BinaryDiscriminatorICHEPDavide(trees_binary[i], vars_ids_binaries[i]));
  }

  return binaries;
}

std::unique_ptr<CategorizationGlobalICHEP> CategorizationGlobalICHEP::factory(const std::string &name, const std::string &folder)
{
  const std::string filename_multiclass = folder + "/model1.root";
  const std::vector<std::string> suffixes = CategorizationGlobalICHEP::get_multiclass_names();

  TFile file_multiclass(filename_multiclass.c_str());
  TTree *tree_multiclass = nullptr;
  file_multiclass.GetObject("lgbm", tree_multiclass);

  if (not tree_multiclass) { throw std::runtime_error("cannot find tree lgbm in file " + filename_multiclass); }

  std::vector<TTree *> tree_binaries;
  std::vector<std::unique_ptr<TFile>> file_binaries;  // TFile is not copiable, cannot be used in std::vector

  for (const std::string &suffix : suffixes) {
    const std::string filename_binary = folder + "/model2_" + suffix + ".root";
    file_binaries.emplace_back(new TFile(filename_binary.c_str()));
    TTree *tree_binary = nullptr;
    file_binaries.back()->GetObject("lgbm", tree_binary);

    if (not tree_binary) { throw std::runtime_error("cannot find tree lgbm in file " + filename_binary); }

    tree_binaries.push_back(tree_binary);
  }

  return std::make_unique<CategorizationGlobalICHEP>(
           name,
           tree_multiclass,
           tree_binaries);
}



CategorizationHybridICHEP::CategorizationHybridICHEP(const std::string &name,
                                                     TTree *tree_multiclass,
                                                     const std::vector<TTree *> &trees_binary)
  : CategorizationMultiClassPlusBinaries(
      name,
      std::make_unique<CategorizationMultiClassICHEP>(name + "_multiclass", tree_multiclass),
      CategorizationHybridICHEP::create_binaries(trees_binary),
      CategorizationHybridICHEP::get_boundaries()
    ) { }


std::vector<std::unique_ptr<const BinaryDiscriminator>> CategorizationHybridICHEP::create_binaries(const std::vector<TTree *> &trees_binary)
{
  using namespace Coupling2020::VariablesICHEP;

  if (trees_binary.size() != n_davide) {
    throw std::runtime_error("the number of binary trees should be " + std::to_string(n_davide) + " while they are " + std::to_string(trees_binary.size()));
  }

  const std::vector<std::vector<InputVariable>> vars_ids_binaries = {
#include "training_vars_multiclass_ichep_binary.def"
  };

  std::vector<std::unique_ptr<const BinaryDiscriminator>> binaries;

  // Davide's part
  for (unsigned i = 0; i != trees_binary.size(); ++i) {
    binaries.emplace_back(new BinaryDiscriminatorICHEPDavide(trees_binary[i], vars_ids_binaries[i]));
  }

  // Chen's part
  // use the complementary version since the variable is 1 - score
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_WH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_WH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_WH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_WH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_WH_ICHEP2020));

  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ZH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ZH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ZH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ZH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ZH_ICHEP2020));

  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ttH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ttH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ttH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ttH_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_ttH_ICHEP2020));

  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_tHjb_ICHEP2020));
  binaries.emplace_back(new BinaryDiscriminatorFromVarComplementary(var::score_tWH_ICHEP2020));

  return binaries;
}

std::unique_ptr<CategorizationHybridICHEP> CategorizationHybridICHEP::factory(const std::string &name, const std::string &folder)
{
  const std::string filename_multiclass = folder + "/model1.root";
  const std::vector<std::string> suffixes = CategorizationHybridICHEP::get_multiclass_names();

  TFile file_multiclass(filename_multiclass.c_str());
  TTree *tree_multiclass = nullptr;
  file_multiclass.GetObject("lgbm", tree_multiclass);

  if (not tree_multiclass) { throw std::runtime_error("cannot find tree lgbm in file " + filename_multiclass); }

  std::vector<TTree *> tree_binaries;
  std::vector<std::unique_ptr<TFile>> file_binaries;  // TFile is not copiable, cannot be used in std::vector

  for (const std::string &suffix : suffixes) {
    const std::string filename_binary = folder + "/model2_" + suffix + ".root";
    file_binaries.emplace_back(new TFile(filename_binary.c_str()));
    TTree *tree_binary = nullptr;
    file_binaries.back()->GetObject("lgbm", tree_binary);

    if (not tree_binary) { throw std::runtime_error("cannot find tree lgbm in file " + filename_binary); }

    tree_binaries.push_back(tree_binary);

    if (tree_binaries.size() >= n_davide) { break; }
  }

  return std::make_unique<CategorizationHybridICHEP>(
           name,
           tree_multiclass,
           tree_binaries);
}