// LHAPDF include(s):
#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/PDF.h"
#include "LHAPDF/PDFSet.h"
#include "LHAPDF/Reweighting.h"

// Local include(s):
#include "HGamTools/PDFTool.h"
#include "HGamAnalysisFramework/HGamVariables.h"

// this is needed to distribute the algorithm to the workers
ClassImp(PDFTool)



PDFTool::PDFTool(const char *name)
  : HgammaAnalysis(name)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



PDFTool::~PDFTool()
{
  // Here you delete any memory you allocated during your analysis.
}



EL::StatusCode PDFTool::createOutput()
{
  // In future make this configurable
  int nCats(33); //, nBins(42), nIndex(53);

  // protect the user from running on data
  m_pdfs.clear();

  if (HG::isData()) {
    HG::fatal("Sample identified as data. Tool should only be ran on MC samples.");
  }

  // initialize LHAPDF tool for reweighting
  int mcid = eventInfo()->mcChannelNumber();
  int lhapdfID = config()->getInt(TString::Format("LHAPDFID.%d", mcid), -999);

  if (lhapdfID == -999) {
    HG::fatal(TString::Format("No LHAPDF ID defined for DSID %d", mcid));
  }

  TString pdfSet = config()->getStr(TString::Format("PDFSet.%d", lhapdfID), "None");

  if (pdfSet == "None") {
    HG::fatal(TString::Format("Must specify PDF set for LHAPDF ID %d", lhapdfID));
  }

  const LHAPDF::PDFSet nompdf(pdfSet.Data());
  m_pdfsnom = nompdf.mkPDFs(); // pointers to PDF set members

  const LHAPDF::PDFSet pdfset("PDF4LHC15_nlo_30_pdfas");
  m_pdfs = pdfset.mkPDFs(); // pointers to PDF set members

  // Make one histogram for each variation
  for (unsigned int ipdf(0); ipdf < m_pdfs.size(); ipdf++) {
    TString suffix = TString::Format("_PDFVar%d", ipdf);
    histoStore()->createTH1F("h_category" + suffix,  nCats, 0.5, nCats + 0.5);
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode PDFTool::execute()
{
  HgammaAnalysis::execute(); // DO NOT REMOVE

  // Get things working for couplings first, then make categorization and weight configurable.
  double w = (HG::isData()) ? 1.0 : var::weightCatCoup_Moriond2017BDT() * lumiXsecWeight();

  if (w == 0.) { return EL::StatusCode::SUCCESS; }

  // get category
  int category = var::catCoup_Moriond2017BDT();

  // loop and fill for each variation
  const xAOD::TruthEventContainer *truthEvts = 0;
  static TString s_truthEvtName = config()->getStr("TruthEvents.ContainerName", "TruthEvents");
  event()->retrieve(truthEvts, s_truthEvtName.Data()) ;

  std::vector<double> weights;

  if (truthEvts->size() && m_pdfs.size() > 0) {
    double Q   = truthEvts->at(0)->pdfInfo().Q;
    double x1  = truthEvts->at(0)->pdfInfo().x1;
    double x2  = truthEvts->at(0)->pdfInfo().x2;
    int pdgId1 = truthEvts->at(0)->pdfInfo().pdgId1;
    int pdgId2 = truthEvts->at(0)->pdfInfo().pdgId2;

    for (size_t imem = 0; imem <= m_pdfs.size() - 1; imem++) {
      double wQ2 = w * LHAPDF::weightxxQ2(pdgId1, pdgId2, x1, x2, Q * Q, m_pdfsnom[0], m_pdfs[imem]) ;

      if (not std::isfinite(wQ2)) {
        std::cout << "WARNING: Systematic weight not finite, using nominal." << std::endl;
        wQ2 = w;
      }

      TString suffix = TString::Format("_PDFVar%d", (int)imem);
      histoStore()->fillTH1F("h_category" + suffix, category, wQ2);
      weights.push_back(wQ2);   // one event weight for each error in the set including the nominal
    }
  }

  return EL::StatusCode::SUCCESS;
}
