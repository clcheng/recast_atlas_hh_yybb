// Tool to perform tests of bkg models
// Nicolas Berger, nicolas.berger@cern.ch

// STL include(s):
#include <iostream>

// ROOT include(s):
#include "TEnv.h"

// Local include(s):
#include "HGamTools/BkgParam.h"


int main(int argc, char *argv[])
{
  if (argc < 3) {
    std::cout << "Usage: " << argv[0] << " <configuration_file> <output directory>" <<  std::endl;
    return 1;
  }

  BkgTool::SpuriousSignalSelector selector(argv[1], argv[2]);

  if (!selector.setup()) { return 2; }

  if (!selector.run()) { return 3; }

  return 0;
}