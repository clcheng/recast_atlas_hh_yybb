import sys

is_python3 = sys.version_info > (3, 0)

from recast_systematics.ops.root import TH1
TH1.load_macros()