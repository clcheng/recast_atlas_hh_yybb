#include "HGamAnalysisFramework/HGamCategoryToolMulticlassPlusBinary2020.h"

#include <TFile.h>
#include <TTree.h>

#include <limits>
#include <utility>
#include <stdexcept>

using namespace MVAUtils;
using namespace Coupling2020;

CategorizationMultiClassPlusBinaries::CategorizationMultiClassPlusBinaries(
  const std::string &name,
  std::unique_ptr<const CategorizationMultiClass> multiclass,
  std::vector<std::unique_ptr<const BinaryDiscriminator>> binaries,
  const std::vector<std::vector<float>> &boundaries)
  : asg::AsgMessaging(name),
    m_multiclass(std::move(multiclass)),
    m_binaries(std::move(binaries)),
    m_boundaries(boundaries)
{

  if (m_boundaries.size() != m_binaries.size()) {
    ATH_MSG_FATAL("number of boundaries set (" << m_boundaries.size()
                  << ") different from the number of classes (" << m_binaries.size() << ")");
    throw std::runtime_error("raise on FATAL");  // seems FATAL doesn't throw
  }

  m_nclasses = 0;

  for (const auto &b : boundaries) {
    m_offset_cat.push_back(m_nclasses);
    m_nclasses += b.size() + 1;
  }

  ATH_MSG_INFO("multiclass created with " << m_binaries.size() << " classes and " << m_nclasses << " categories");

  for (unsigned int i = 0; i != m_binaries.size(); ++i) {
    std::string boundaries_string;

    for (unsigned int j = 0; j != m_boundaries[i].size(); ++j) {
      boundaries_string += " " + std::to_string(m_boundaries[i][j]);
    }

    ATH_MSG_INFO("binary " << i << " (for " << m_multiclass->get_category_name(i) << "): " << * (m_binaries[i]) << ", boundaries: [" << boundaries_string << "]");
  }

  ATH_MSG_INFO("category mapping for " << name);
  unsigned int icat = 0;

  for (unsigned int i = 0; i != m_binaries.size(); ++i) {
    for (unsigned int j = 0; j != m_boundaries[i].size() + 1; ++j) {
      ATH_MSG_INFO(icat << ": " << m_multiclass->get_category_name(i) + "__" << j);
      ++icat;
    }
  }
}

CategorizationMultiClassPlusBinaries::Result CategorizationMultiClassPlusBinaries::get_category(const CategorizationInputs &inputs, bool add_weight_fjvt) const
{
  const int multiclass_category = m_multiclass->get_category(inputs);

  // ATH_MSG_INFO("multiclass best category: " << multiclass_category);

  float weight = 1.;
  // TODO: tune here depending on the category
  // TODO: delegate weight computation to multiclass

  bool add_weight_leptons = true;
  bool add_weight_btagging = true;
  bool add_weight_jvt = true;

  if (add_weight_leptons) {
    for (const auto &el : *inputs.electrons) {
      weight *= el->auxdata<float>("scaleFactor");
    }

    for (const auto &mu : *inputs.muons) {
      weight *= mu->auxdata<float>("scaleFactor");
    }
  }

  if (add_weight_btagging) {
    for (const auto &jet : *inputs.jets) {
      weight *= jet->auxdata<float>("SF_DL1r_FixedCutBEff_77");
    }
  }

  if (add_weight_jvt) {
    for (const auto &jet : *inputs.jets)
      if (jet->pt() > 30 * HG::GeV) {
        weight *= jet->auxdata<float>("SF_jvt");

        if (add_weight_fjvt) { weight *= jet->auxdata<float>("SF_fjvt"); }
      }
  }

  int binary_bin = 0;
  float output_binary = 0;

  if (m_boundaries[multiclass_category].size() != 0) {
    // if there is a binary BDT
    // ATH_MSG_INFO("computing input for binary bdt");

    output_binary = m_binaries[multiclass_category]->get_result(inputs);
    const auto boundaries = m_boundaries[multiclass_category];
    // remember that pure bins (higher value) has lower index. Extremes (0, 1) and not inside boundaries
    binary_bin = std::distance(std::upper_bound(std::begin(boundaries), std::end(boundaries), output_binary),
                               std::end(boundaries));
  }

  // ATH_MSG_INFO("binary output: " << output_binary);
  // ATH_MSG_INFO("category: " << binary_bin);
  return Result({binary_bin + m_offset_cat[multiclass_category],
                 multiclass_category,
                 output_binary,
                 weight
                });
}
