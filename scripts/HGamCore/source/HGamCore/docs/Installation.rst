.. _Install:

Installation
============

Create a working directory

.. code-block:: bash

  mkdir hgam-your_label && cd $_
  mkdir build source run

Find the recommended tag for analysis or MxAOD production and the corresponding AnalysisBase release with which it should be set up.

HGamCore tag and AnalysisBase release
-------------------------------------

.. csv-table::
   :header: "HGamCore tag", "Base release", "MxAOD h-tag working with this tag", "Results based on this version"

   "master", "21.2.113", "ONLY for development (via merge request)", " "
   "v1.9.10-h025", "21.2.113", "For h025 production and analysis", "Used for ICHEP 2020 analysis"
   "v1.8.1-h024-HighLowMass ", "21.2.56", "Reprocessed data with updated calibration", "For high-mass search only" 
   "v1.8.1-h024-CouplingsSTXS1_2", "21.2.90", "Test production", "Used for couplings optimization studies"
   "v1.8.52-h024-ttHcp", "21.2.56", "For h024 production and analysis", "Used for 2019 ttH CP analysis"
   "v1.8.33-h024", "21.2.56", "For h024 production and analysis", "Used for 2019 results (Moriond and EPS)"

For h025, a good pair used in these instructions is the one below, but generally use the latest available in the recommendations.

.. code-block:: bash

  21.2.113 -- v1.9.10-h025

Setup the analysis release

.. code-block:: bash

  cd source
  setupATLAS
  lsetup git
  asetup AnalysisBase,21.2.113,here

Setup the code to compile with the release used above

.. code-block:: bash

  cd $TestArea # this should be the "source" directory from above
  git clone --recursive https://:@gitlab.cern.ch:8443/atlas-hgam-sw/HGamCore.git # Assumes Kerberos checkout
  cd HGamCore
  git checkout v1.9.xx-h025 # Generally the most recently recommended version is best
  git submodule update --init --recursive # Get proper dependencies
  cd -

Compile everything

.. code-block:: bash

  cd $TestArea/../build
  cmake ../source
  make -j4 # Compile using 4 cores; tweak as appropriate for your system
  source $AnalysisBase_PLATFORM/setup.sh

Run a test in the interactive mode

.. code-block:: bash
   
   cd $TestArea/../run
   echo /eos/atlas/user/y/yanlin/tutorials/mc16_13TeV/DAOD_HIGG1D1.20695145._000001.pool.root.1 > input.txt
   runHGamCutflowAndMxAOD HGamTools/MxAODDetailed.config InputFileList: input.txt OutputDir: outdir NumEvents: 1000
