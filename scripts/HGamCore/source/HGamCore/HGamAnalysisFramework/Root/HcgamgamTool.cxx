///////////////////////////////////////////
// H+c selection Tool
// Tool performs all the yy+c selection
//  with looser dR and jetpT cuts
// All info decorated to MxAODs
// @author: anna.ivina@cern.ch
///////////////////////////////////////////

// STL includes
#include <algorithm>

// ATLAS/ROOT includes
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"



// Local includes
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/JetHandler.h"
#include "HGamAnalysisFramework/ElectronHandler.h"
#include "HGamAnalysisFramework/MuonHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"
#include "HGamAnalysisFramework/HcgamgamTool.h"

namespace HG {
  // _______________________________________________________________________________________
  HcgamgamTool::HcgamgamTool(const char *name, HG::EventHandler *eventHandler, HG::TruthHandler *truthHandler)
    : asg::AsgMessaging(name)
    , m_eventHandler(eventHandler)
    , m_truthHandler(truthHandler)
    , m_minMuonPT(10)
    , m_minElectronPT(10)
    , m_jet_eta_max(2.5)
    , m_truthjet_rapidity_max(2.5)
    , m_jet_pt(25)
    , m_Njets(3)
    , m_dRcut(0.5)
  {}

  // _______________________________________________________________________________________
  HcgamgamTool::~HcgamgamTool()
  {
  }


  // _______________________________________________________________________________________
  HG::EventHandler *HcgamgamTool::eventHandler()
  {
    return m_eventHandler;
  }


  // _______________________________________________________________________________________
  HG::TruthHandler *HcgamgamTool::truthHandler()
  {
    return m_truthHandler;
  }


  // _______________________________________________________________________________________
  EL::StatusCode HcgamgamTool::initialize(Config &config)
  {
    // Set default cut values etc here...
    // Almost all of these are set hard-coded because this will be used by the nominal MxAOD production
    // Also safer :)

    m_minMuonPT = config.getNum("HcgamgamTool.MinMuonPt", 10.0); //! minimum muon pt (in GeV!!! ) for lepton veto - not usi ng for now
    ATH_MSG_INFO("MinMuonPt.................................. " << m_minMuonPT);

    m_minElectronPT = config.getNum("HcgamgamTool.MinElectronPt", 10.0); //! minimum electron pt (in GeV!!!) for lepton veto - not using for now
    ATH_MSG_INFO("MinElectronPt.............................. " << m_minElectronPT);

    m_jet_eta_max = config.getNum("HcgamgamTool.MaxJetEta", 2.5); //! Max jet eta in abs
    ATH_MSG_INFO("MaxJetEta........................... " << m_jet_eta_max);

    m_jet_pt     = config.getNum("HcgamgamTool.JetpT", 25); //! Loose jet pT cut in GeV - leave this for now - loose
    ATH_MSG_INFO("JetpT........................... " << m_jet_pt);

    m_Njets = config.getNum("HcgamgamTool.NJetInEvent", 3); //! How many jets you want to use in your analysis - loose
    ATH_MSG_INFO("NJetInEvent........................... " << m_Njets);

    m_dRcut = config.getNum("HcgamgamTool.dRcutJetPho", 0.5); //! Dr cut between a leading jet and a lead(sublead)photon - loose
    ATH_MSG_INFO("dRCutJetPhotons........................... " << m_dRcut);

    m_tagger = config.getStr("HcgamgamTool.Tagger", "DL1");//The name of the Tagger for Ctagging - DL1 is default by now
    ATH_MSG_INFO("TaggerName........................... " << m_tagger);


    //In current version CDI files - 2019-21-13TeV-MC16-CDI-2019-10-07_v1.root in DL1/AntiKt4EMTopoJets_BTagging201810 we have 3 WP for c-tagging
    //CTag_Loose - DL1 cut 0.61456, fb - 0.18
    //CTag_Tight - DL1 cut 1.3, fb - 0.08
    //Ctag_Tight_Veto_MV2c10_FixedBEff_70 - require jets !b @ 70% with MV2c10_FixedBEff_70 and DL1 cut > 1.3 and fb - 0.08


    return EL::StatusCode::SUCCESS;
  }

  void HcgamgamTool::ClearTrue()
  {
    //Set the values
    //Ints
    //truth
    m_eventInfoTrueInts["Hc_N_bjets25"] = 0;
    m_eventInfoTrueInts["Hc_N_cjets25"] = 0;
    m_eventInfoTrueInts["Hc_N_cjets30"] = 0;
  }

  void HcgamgamTool::Clear()
  {

    //clear vectors for each events and setting to some default value;
    //Ints
    m_eventInfoVInts["Hc_jet_truthlabel"].clear();

    //Floats
    m_eventInfoVFloats["Hc_y1_j_dr"].clear();
    m_eventInfoVFloats["Hc_y2_j_dr"].clear();

    //reco
    m_eventInfoInts["Hc_m_yy_cat"] = -1;
    m_eventInfoInts["Hc_leadJet_truthLabel"] = -99.;

    //m_eventInfoFloats["Hc_y1leadJet_dr"] = -1;
    //m_eventInfoFloats["Hc_y2leadJet_dr"] = -1;


  }
  // _______________________________________________________________________________________
  void HcgamgamTool::saveHCTruthInfo(const xAOD::TruthParticleContainer &photons,
                                     const xAOD::JetContainer   &jets,
                                     const xAOD::JetContainer   &bjets,
                                     const xAOD::JetContainer   &cjets,
                                     xAOD::TruthParticleContainer &muons,
                                     xAOD::TruthParticleContainer &electrons)
  {
    // --------
    // Maps containing all event info to decorate to MxAOD
    // Floats for all decimal values
    // Chars for all bools
    // Ints for all counters (Category and multiplicities)


    ClearTrue();
    // Perform truth selection
    performTruthSelection(photons, jets, bjets, cjets, muons, electrons);


    // Save all the eventInfoMaps to the eventHandler()
    // Should always be called last, once all of these have been filled
    saveMapsToTruthEventInfo();
  }



  // _______________________________________________________________________________________
  // Function which performs full HiggsHF truth selection and saves info to info maps (nominal by default)
  void HcgamgamTool::performTruthSelection(const xAOD::TruthParticleContainer &photons,
                                           const xAOD::JetContainer   &jets,
                                           const xAOD::JetContainer   &bjets,
                                           const xAOD::JetContainer   &cjets,
                                           xAOD::TruthParticleContainer &/*muons*/,
                                           xAOD::TruthParticleContainer &/*electrons*/)
  {

    // First apply photon selection
    if (!truthHandler()->passFiducial(&photons)) {
      m_eventInfoTrueInts["Hc_cutFlowAllJet"] = HCPHOTONS;
      return;
    }


    // Number of central jets
    int Njet_central = 0;
    int Njet_b25 = 0;
    int Njet_c25 = 0;
    int Njet_c30 = 0;



    //Loop over jets
    for (auto jet : jets) {
      if (fabs(jet->eta()) > m_truthjet_rapidity_max || jet->pt() < m_jet_pt * HG::GeV) { continue; }

      Njet_central++;
    }

    //Loop over bjets
    for (auto bjet : bjets) {
      if (fabs(bjet->eta()) > m_truthjet_rapidity_max || bjet->pt() < m_jet_pt * HG::GeV) { continue; }

      Njet_b25++;
    }

    //Loop over cjets, reject jets that are already matched to a b-jet
    for (auto cjet : cjets) {
      if (fabs(cjet->eta()) > m_truthjet_rapidity_max || cjet->pt() < m_jet_pt * HG::GeV) { continue; }

      Njet_c25++;

      if (fabs(cjet->eta()) > m_truthjet_rapidity_max || cjet->pt() < 30 * HG::GeV) { continue; }

      Njet_c30++;
    }


    // Minimum requirement on the number of truth jets
    if (Njet_central == 0) { m_eventInfoTrueInts["Hc_cutFlowAllJet"] = HCATLEAST1JET; return;}

    // Require at least one jet matched to a c-hadron
    if (Njet_c25 == 0) { m_eventInfoTrueInts["Hc_cutFlowAllJet"] = HCCTAG; return;}

    // Save events that pass all requirements
    m_eventInfoTrueInts["Hc_cutFlowAllJet"] = HCPASS;
    m_eventInfoTrueInts["Hc_N_bjets25"] = Njet_b25;
    m_eventInfoTrueInts["Hc_N_cjets25"] = Njet_c25;
    m_eventInfoTrueInts["Hc_N_cjets30"] = Njet_c30;
  }


  // _______________________________________________________________________________________
  void HcgamgamTool::saveHCInfo(xAOD::PhotonContainer   photons,
                                xAOD::JetContainer  &jets,
                                xAOD::MuonContainer &muons,
                                xAOD::ElectronContainer &electrons)
  {
    // --------
    // Maps containing all event info to decorate to MxAOD
    // Floats for all decimal values
    // Chars for all bools
    // Ints for all counters (Category and multiplicities)


    Clear();
    // Perform selection
    performSelection(photons, jets, muons, electrons);


    // Save all the eventInfoMaps to the eventHandler()
    // Should always be called last, once all of these have been filled
    saveMapsToEventInfo();
  }

  // _______________________________________________________________________________________
  // Function which performs full h+c selection and saves info to info maps (nominal by default)
  void HcgamgamTool::performSelection(xAOD::PhotonContainer photons,
                                      xAOD::JetContainer &jets,
                                      xAOD::MuonContainer &/*muons*/,
                                      xAOD::ElectronContainer &/*electrons*/)
  {


    // First apply photon selection
    if (photons.size() < 2) {
      m_eventInfoInts["Hc_cutFlowLeadJet"] = HCPHOTONS;
      m_eventInfoInts["Hc_cutFlowAllJet"]  = HCPHOTONS;
      return;
    }



    //----------------- Selecting the jets -----------------//

    //Disregard events with no jets
    if (jets.size() < 1) {
      m_eventInfoInts["Hc_cutFlowLeadJet"] = HCATLEAST1JET;
      m_eventInfoInts["Hc_cutFlowAllJet"] = HCATLEAST1JET;
      return;
    }

    std::vector<int> centraljets;
    int counts = -1; //-1 no jets are found


    for (auto jet : jets) {
      counts++;

      if (fabs(jet->eta()) > m_jet_eta_max || jet->pt() < m_jet_pt * HG::GeV) { continue; }

      centraljets.push_back(counts);

      if ((int)centraljets.size() == m_Njets) { break; }
    }


    //Define Higgs ------
    //-------------------------------
    TLorentzVector pho1 = (photons)[0]->p4();
    TLorentzVector pho2 = (photons)[1]->p4();


    //Perform the selection for the Leading jet only
    performleadjetCutflow(pho1, pho2, jets, centraljets);

    //Perform the selection for the jets min=1 max=3
    performjetCutflow(pho1, pho2, jets, centraljets);


    //Save some common variables
    TLorentzVector Vyy = pho1 + pho2;

    //Sunt in case include the categories of the m_yy
    if (Vyy.M() >= 122.5 * HG::GeV && Vyy.M() <= 127.5 * HG::GeV) {
      m_eventInfoInts["Hc_m_yy_cat"] = SR;
    } else if (Vyy.M() <= 120 * HG::GeV || Vyy.M() >= 130 * HG::GeV) {
      m_eventInfoInts["Hc_m_yy_cat"] = SIDEBANDS;
    } else {
      m_eventInfoInts["Hc_m_yy_cat"] = VR;
    }



  }//end of performSelection




  void HcgamgamTool::performleadjetCutflow(TLorentzVector ph1,
                                           TLorentzVector ph2,
                                           xAOD::JetContainer &jets,
                                           std::vector<int> centjets)
  {

    //Require to have at least one central jet
    if (centjets.size() <= 0) { m_eventInfoInts["Hc_cutFlowLeadJet"] = HCJETPTETA; return; }

    //Use the leading jet to perform the measurements
    if (centjets.at(0) > 0) { m_eventInfoInts["Hc_cutFlowLeadJet"] = HCLEADJETPTETA; return; }

    //Lead jet
    const xAOD::Jet *leadjet = (jets)[0];
    TLorentzVector jet  = leadjet->p4();


    //perform the dR on the leading jet - tight dR cut
    if ((jet.DeltaR(ph1) < m_dRcut) || (jet.DeltaR(ph2) < m_dRcut))
    {m_eventInfoInts["Hc_cutFlowLeadJet"] = DRJETGAM; return;}


    //Save the events that passed all requirements
    m_eventInfoInts["Hc_cutFlowLeadJet"] = HCPASS;


    //Vars to store
    //Floats

    //m_eventInfoFloats["Hc_y1leadJet_dr"] = ph1.DeltaR(jet);
    //m_eventInfoFloats["Hc_y2leadJet_dr"] = ph2.DeltaR(jet);


    //Truth info
    //int truthLabel = HG::isMC() ? leadjet->auxdata<int>("HadronConeExclTruthLabelID") : -99;
    //Ints
    //m_eventInfoInts["Hc_leadJet_truthLabel"] = truthLabel;


  }


  void HcgamgamTool::performjetCutflow(TLorentzVector ph1,
                                       TLorentzVector ph2,
                                       xAOD::JetContainer &jets,
                                       std::vector<int> centjets)

  {


    //int isLoose_index = -1, isTight_index = -1;//index of tagged jets -1 no jets are found
    //bool isLoose = false, isTight = false;//the flag to select the tagged jets
    int isPassdR;
    bool ispassed1 = false;
    TLorentzVector jet;
    int truthLabel;


    //Require to have at least one jet in the container
    if (centjets.size() <= 0) {m_eventInfoInts["Hc_cutFlowAllJet"] = HCJETPTETA; return;}


    //Selecting first 3 central jets
    for (long unsigned int i = 0; i < centjets.size(); i++) {
      //Check dR
      isPassdR = -1;
      jet  = jets.at(centjets.at(i))->p4();
      float dR_y1_j = jet.DeltaR(ph1);
      float dR_y2_j = jet.DeltaR(ph2);

      if ((dR_y1_j > m_dRcut) && (dR_y2_j > m_dRcut)) {ispassed1 = true;}
      else { continue; }

      if (isPassdR == 1) {truthLabel = HG::isMC() ? jets.at(i)->auxdata<int>("HadronConeExclTruthLabelID") : -99;}


      m_eventInfoVInts["Hc_jet_truthlabel"].push_back(truthLabel);
      m_eventInfoVFloats["Hc_y1_j_dr"].push_back(dR_y1_j);
      m_eventInfoVFloats["Hc_y2_j_dr"].push_back(dR_y2_j);

    }//end of for loop

    if (ispassed1 == false) {m_eventInfoInts["Hc_cutFlowAllJet"] = DRJETGAM; return;}

    //Save the events that passed all requirements
    m_eventInfoInts["Hc_cutFlowAllJet"] = HCPASS;


  }




  // ___________________________________________________________________________________________
  // Multiply all the JVT (in)efficiencies for all jets passing the selection before the JVT cut
  double HcgamgamTool::weightJVT(xAOD::JetContainer &jets_noJVT)
  {

    // Construct output jet container
    xAOD::JetContainer jets_noJVT_passing_cuts(SG::VIEW_ELEMENTS);

    // Apply central jet requirement (for b-tagging reasons) and appropriate pT cuts
    for (auto jet : jets_noJVT) {
      if (fabs(jet->eta()) > m_jet_eta_max) { continue; }

      if (jet->pt() < m_jet_pt * HG::GeV) { continue; }

      jets_noJVT_passing_cuts.push_back(jet);
    }

    // Return multiplicative combination of JVT efficiencies
    if (jets_noJVT_passing_cuts.size() > 0) {
      return HG::JetHandler::multiplyJvtWeights(&jets_noJVT_passing_cuts);
    }

    return 1.0;
  }



  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HcgamgamTool::saveMapsToEventInfo()
  {
    // Floats
    for (auto element : m_eventInfoFloats) {
      eventHandler()->storeVar<float>(element.first.Data(), element.second);
    }

    // Ints
    for (auto element : m_eventInfoInts) {
      eventHandler()->storeVar<int>(element.first.Data(), element.second);
    }

    //Vectors of Ints
    for (auto element : m_eventInfoVInts) {
      eventHandler()->storeVar<std::vector<int>>(element.first.Data(), element.second);
    }

    //Vectors of floats
    for (auto element : m_eventInfoVFloats) {
      eventHandler()->storeVar<std::vector<float>>(element.first.Data(), element.second);
    }


  }

  // _______________________________________________________________________________________
  // Save event info from maps to eventInfo
  void HcgamgamTool::saveMapsToTruthEventInfo()
  {
    // Floats
    //for (auto element : eventInfoFloats) {
    // eventHandler()->storeTruthVar<float>(element.first.Data(), element.second);
    //}

    // Ints
    for (auto element : m_eventInfoTrueInts) {
      eventHandler()->storeTruthVar<int>(element.first.Data(), element.second);
    }
  }


  //--------------------------------------------------------------------------------------------------
  //Discriminant function//very easy
  double HcgamgamTool::discriminant(double pc, double pb, double pu, float fraction)
  {
    double ctag_discriminant = log(pc / ((pb * fraction) + (1 - fraction) * pu));
    return ctag_discriminant;
  }





  bool HcgamgamTool::isCTagLoose(const xAOD::Jet &jet)
  {
    static SG::AuxElement::Accessor<char> isDL1CtagLoose("DL1_CTag_Loose");


    if (isDL1CtagLoose.isAvailable(jet)) { return isDL1CtagLoose(jet); }
    else {
      if (jet.btagging()) {
        static const float fb = 0.18;
        static const double dl1_cut = 0.61456;
        double pb = -99;
        jet.btagging()->pb(m_tagger.Data(), pb);
        double pc = -99;
        jet.btagging()->pc(m_tagger.Data(), pc);
        double pu = -99;
        jet.btagging()->pu(m_tagger.Data(), pu);

        if (pb > -99. && pu > -99. && pc > -99.) {
          double dl1 = discriminant(pc, pb, pu, fb);
          return dl1 > dl1_cut;
        } else { return false; }

      } else { return false; }
    }

  }


  bool HcgamgamTool::isCTagTight(const xAOD::Jet &jet)
  {
    static SG::AuxElement::Accessor<char> isDL1CtagTight("DL1_CTag_Tight");


    if (isDL1CtagTight.isAvailable(jet)) { return isDL1CtagTight(jet); }
    else {
      if (jet.btagging()) {
        static const float fb = 0.08;
        static const double dl1_cut = 1.3;
        double pb = -99;
        jet.btagging()->pb(m_tagger.Data(), pb);
        double pc = -99;
        jet.btagging()->pc(m_tagger.Data(), pc);
        double pu = -99;
        jet.btagging()->pu(m_tagger.Data(), pu);

        if (pb > -99. && pu > -99. && pc > -99.) {
          double dl1 = discriminant(pc, pb, pu, fb);
          return dl1 > dl1_cut;
        } else { return false; }

      } else { return false; }
    }

  }

  bool HcgamgamTool::isGoodJet(const xAOD::Jet *jet)
  {
    if (jet->pt() < m_jet_pt * HG::GeV && fabs(jet->eta()) > m_jet_eta_max) { return false; }
    else
    { return true; }
  }

  // _______________________________________________________________________________________
  // Fetch the ctagging SF weight
  double HcgamgamTool::weightCTagging(const xAOD::Jet *jet)
  {

    TString SFName = TString("SF_DL1_CTag_Loose");
    return jet->isAvailable<float>(SFName.Data()) ? jet->auxdata<float>(SFName.Data()) : -1;
  }



}//end of the hctool


