Running the spurious signal code
--------------------------------

In order to get the background functions and the corresponding uncertainty, one has first to produce workspaces to build the signal shape based on signal MC. In a second step, for each analysis selection (each bin if done differentially), one then run the spurious signal code.

Spurious signal evaluation
--------------------------

Check the example config `signalParam <https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/tree/master/HGamTools/data/signalParam/exp_spuriousSignal_ptH_0.cfg>` in the HGamCore package, which uses the workspace from the H->yy for the leading bin of ptH (see Documentation **Building the signal workspace**).

As input, you need the signal workspaces as created in the previous step and the background templates for your analysis. The config has to be adapted to use the directory and naming structure as used for the inputs, for this you have to change the following lines (here as an additional example for the inclusive selection)

.. code-block:: bash

  Scan.Var: muCBNom_SM_incl_0_m125000_c0 (variable name as stored in the signal workspace)
  RefSignalYieldVar.Name: sigYield_SM_incl_0_m125000_c0 (signal yield as stored in the workspace)
  Signal.PDF.Name: sigPdf_SM_incl_0_m125000_c0 (as stored in the workspace)
  Signal.PDF.File: path to the root file of the signal workspace
  RefSignalYieldVar.File: path to the root file of the signal workspace
  Dataset.File: path to the background template file
  Dataset.HistogramName: variable name as used in the background templae
  

You can specify the functions which should be tested via the **Background.PDFs** entry. Below is the detailed list of functions used for the Hyy differential analysis (CONF with 140/fb)

.. code-block:: bash

  #PDF expressions are written with a generic observable m_yy (replaced in code with the observable)
  # Expression for the PDF form, either as in RooWorkspace::factory syntax or as a a formula expression to use with RooGenericPdf
  Exponential.Expression:                         exp(xi*m_yy_m125000_c0);
  # For the case of a RooGenericPdf expression above, provides the comma-separated list of PDF parameters to use
  Exponential.Parameters:                         xi[0,-20,20]
  # If set to True, the initial values of the parameters will be set from a B-only fit to the dataset above (recommended)
  Exponential.SetInitialValuesFromDataset:        True

  ExpPoly2.Expression:                         exp((m_yy_m125000_c0 - 100)/100*(a1 + a2*(m_yy_m125000_c0 - 100)/100))
  ExpPoly2.Parameters:                         a1[0,-10,10] a2[0,-10,10]
  ExpPoly2.SetInitialValuesFromDataset:        True

  ExpPoly3.Expression:                         exp((m_yy_m125000_c0 - 100)/100*(a1 + a2*(m_yy_m125000_c0 - 100)/100 + a3*(m_yy_m125000_c0 - 100)/100*(m_yy_m125000_c0 - 100)/100))
  ExpPoly3.Parameters:                         a1[0,-20,20] a2[0,-20,20]  a3[0,-200,200]
  ExpPoly3.SetInitialValuesFromDataset:        True

  Bern3.Expression:                            RooBernstein(m_yy_m125000_c0, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], 1})
  Bern3.SetInitialValuesFromDataset:           True

  Bern4.Expression:                            RooBernstein(m_yy_m125000_c0, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], a4[0,-10,10],1})
  Bern4.SetInitialValuesFromDataset:           True

  Bern5.Expression:                            RooBernstein(m_yy_m125000_c0, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], a4[0,-10,10], a5[0,-10,10],1})
  Bern5.SetInitialValuesFromDataset:           True

  Bern6.Expression:                            RooBernstein(m_yy_m125000_c0, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], a4[0,-10,10], a5[0,-10,10],a6[0,-10,10],1})
  Bern6.SetInitialValuesFromDataset:           True

  Bern7.Expression:                            RooBernstein(m_yy_m125000_c0, { a1[0,-10,10], a2[0,-10,10], a3[0,-10,10], a4[0,-10,10], a5[0,-10,10],a6[0,-10,10],a7[0,-10,10],1})
  Bern7.SetInitialValuesFromDataset:           True


  Pow.Expression: pow(m_yy_m125000_c0/100., p1)
  Pow.Parameters: p1[0,-10,0]
  Pow.SetInitialValuesFromDataset: True

  Pow2.Expression: pow(m_yy_m125000_c0/100.,p1) + p2*pow(m_yy_m125000_c0/100.,p3)
  Pow2.Parameters: p1[0,-10,0] p2[0,-2,2] p3[0,-10,10]
  Pow2.SetInitialValuesFromDataset: True


To run the actual code, you call

.. code-block:: bash

  checkBkgModel <signalParamName>.cfg <outputDir>

This creates a directory including a summary table for the spurious signal result where the tested functions are sorted according to the selected sorting and if they passes the selected criteria. Furthermore there are plots showing the fitted signal and the spurious signal uncertainty. Those plots are provided for a combination of all functions with and without an uncertainty band and for all functions separetly.
