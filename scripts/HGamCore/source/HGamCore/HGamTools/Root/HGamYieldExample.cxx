// EDM include(s):
#include "EventLoop/Worker.h"

// Local include(s):
#include "HGamTools/HGamYieldExample.h"
#include "HGamAnalysisFramework/HGamVariables.h"

// this is needed to distribute the algorithm to the workers
ClassImp(HGamYieldExample)



HGamYieldExample::HGamYieldExample(const char *name)
  : HgammaAnalysis(name) {}

// Here you delete any memory you allocated during your analysis.
HGamYieldExample::~HGamYieldExample() { }



EL::StatusCode HGamYieldExample::createOutput()
{
  // Here you setup the histograms needed for you analysis. This method
  // gets called after the Handlers are initialized, so that the systematic
  // registry is already filled.

  return EL::StatusCode::SUCCESS;
}

TH1F *HGamYieldExample::createAndRegisterTH1F(TString name, int Nbins, double min, double max, TString title)
{
  histoStore()->createTH1F(name, Nbins, min, max, title);
  wk()->addOutput(histoStore()->getTH1F(name));
  return histoStore()->getTH1F(name);
}

EL::StatusCode HGamYieldExample::execute()
{
  // Important to keep this, so that internal tools / event variables
  // are filled properly.
  HgammaAnalysis::execute();

  if (!HG::isMAOD()) { HG::fatal("This code only works on MxAOD input."); }

  TString prefix = HG::isData() ? "data" : getMCSampleName();

  // number of events per MCID
  int id = HG::isData() ? 0 : eventInfo()->mcChannelNumber();

  if (m_nEvents[id] == 0) {
    // New sample!
    createAndRegisterTH1F(prefix + "_m_yy", 300, 0, 300, prefix + ";#it{m}_{#gamma#gamma} [GeV]");

    if (HG::isMC()) {
      createAndRegisterTH1F(prefix + "_truth_pT_y1", 300, 0, 300, prefix + ";#it{p}_{T#gamma1} [GeV]");
    }
  }

  m_nEvents[id]++;

  // final weight
  double w = HG::isData() ? 1.0 : lumiXsecWeight() * weight();

  // reco myy, fill if we pass full seelection
  if (var::isPassed()) {
    histoStore()->fillTH1F(prefix + "_m_yy", var::m_yy() / HG::GeV, w);
  }

  if (HG::isMC()) {
    xAOD::TruthParticleContainer truth_gams  = truthHandler()->getPhotons();

    if (truth_gams.size()) { histoStore()->fillTH1F(prefix + "_truth_pT_y1", truth_gams[0]->pt() / HG::GeV, w); }
  }

  return EL::StatusCode::SUCCESS;
}
