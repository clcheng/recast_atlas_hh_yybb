import os
import json
import glob
from collections import OrderedDict, defaultdict

import oyaml as yaml
import numpy as np
import click

#now using values from Marc
ggF_HH_params = {'SM_1': (1.56276, -0.803479, 0.12113),
'SM_2': (0.695215, -0.398999, 0.0646446),
'BSM_1': (0.195258, -0.206064, 0.0578087),
'BSM_2': (0.32765, -0.36127, 0.106953)}

VBF_HH_params = {'SM_1': (0.0460971, -0.0470876, 0.0137609),
'SM_2': (0.0387746, -0.0304524, 0.00811418), 
'BSM_1': (0.0188482, -0.0244384, 0.00888752),
'BSM_2': (0.0357513, -0.0457254, 0.0171165)}

kl_reweighting_syst = {'SM_1': 0.0271,
'SM_2': 0.0461,
'BSM_1': 0.0387,
'BSM_2': 0.0313}

category_map = {
'SM_1' : 'tightScore_HMass',
'SM_2' : 'looseScore_HMass',
'BSM_1' : 'tightScore_LMass',
'BSM_2' : 'looseScore_LMass',
}

nested_dict = lambda: defaultdict(nested_dict)

SIG_PRUNE_THRESHOLD = 0 #0.1% #0.002
SYST_PRUNE_THRESHOLD = 0    #1% #0.001
SYST_SCALE_PRUNE_THRESHOLD = 0
SYST_RES_PRUNE_THRESHOLD = 0
XS_OR_MU = 'XS' #treatment of theory uncertainties

if XS_OR_MU == 'XS': DEFF_OR_DNRT = 'dEff'
elif XS_OR_MU == 'MU': DEFF_OR_DNRT = 'dNrt'

#convert HComb name to production mode
#outputs: ggH, VBF, ZH, WH, ggZH, ttH, bbH, tHW, tHqb
def HComb_to_prodmode(process_name):
    if process_name.startswith('ggF_'): return 'ggH'
    if process_name.startswith('VBF_') or process_name.startswith('ZH_') or process_name.startswith('WH_') or process_name.startswith('ggZH_') or process_name.startswith('ttH') or process_name.startswith('bbH') or process_name.startswith('tHW') or process_name.startswith('tHqb'): return process_name.split('_', 1)[0]
    if process_name.startswith('qq2Hlnu'): return 'WH'
    if process_name.startswith('qq2Hll'): return 'ZH'
    if process_name.startswith('gg2Hll'): return 'ggZH'

def HComb_to_STXS1p2(process_name):
    if process_name.startswith('ggF_') or process_name.startswith('VBF_') or process_name.startswith('ZH_') or process_name.startswith('WH_') or process_name.startswith('ggZH_'):
        return process_name.split('_', 1)[1]
    else: 
        return process_name

def HComb_to_STXS0(process_name):
    process_stage0_names = ['ggF_gg2H', 'VBF_qq2Hqq', 'WH_qq2Hqq', 'ZH_qq2Hqq', 'qq2Hlnu', 'qq2Hll', 'ggZH_gg2H', 'gg2Hll', 'ttH', 'bbH', 'tHqb_tH', 'tHW_tH']
    for process_stage0_name in process_stage0_names:
        if process_name.startswith(process_stage0_name): return process_stage0_name
    return 'MISSING'

def isHH(process_name):
    return ('HH' in process_name or 'hh' in process_name)

def isH(process_name):
    return (not isHH(process_name) and ('ggH' in process_name or 'VBF' in process_name or 'WH' in process_name or 'ZH' in process_name or 'ttH' in process_name or 'tHjb' in process_name or 'tWH' in process_name))

def process_Marc_name(name):
    if name == 'PDF_alpha_s': return 'THEO_ACC_PDFalphas'
    if name == 'QCD': return 'THEO_ACC_SCALE'
    if name == 'PartonShowering': return 'THEO_ACC_PS'
    return 'ATLAS_' + name

def load_Marc_txt(dict_name, cat_name, process, file):
    try:
        f = open(file)
        lines = f.readlines()
        for x in lines:
            if x.startswith('#'): continue
            nvals = len(x.split(' '))
            if nvals == 3:
                dict_name[cat_name][process][process_Marc_name(x.split(' ')[0])]['up'] = float(x.split(' ')[1])/100.
                dict_name[cat_name][process][process_Marc_name(x.split(' ')[0])]['down'] = float(x.split(' ')[2].replace('\n', ''))/100.
            elif nvals == 2:
                dict_name[cat_name][process][process_Marc_name(x.split(' ')[0])]['up'] = float(x.split(' ')[1].replace('\n', ''))/100.

    except IOError:
        print(file, "not found; intentional?")
    except IndexError:
        print(file, "missing some syst, intentional?")

        #TODO properly fix missing files or missing entries

def load_Alex_txt(dict_name, cat_name, process, file):
    try:
        f = open(file)
        lines = f.readlines()
        for x in lines:
            nvals = len(x.split(' '))
            if nvals == 3:
                dict_name[cat_name][process][x.split(' ')[0].split('_' + process)[0]]['up'] = float(x.split(' ')[1])
                dict_name[cat_name][process][x.split(' ')[0].split('_' + process)[0]]['down'] = float(x.split(' ')[2].replace('\n', ''))
            elif nvals == 2:
                dict_name[cat_name][process][x.split(' ')[0].split('_' + process)[0]]['up'] = float(x.split(' ')[1].replace('\n', ''))

    except IOError:
        print(file, "not found; intentional?")
    except IndexError:
        print(file, "missing some syst, intentional?")

        #TODO properly fix missing files or missing entries

def printXML(yield_dict, syst_res_dict, syst_scale_dict, syst_exp_yield_dict, syst_th_res_dict, syst_th_xs_dict, syst_th_acc_dict, syst_th_yield_dict, syst_th_PS_dict, ss_dict, output_dir, kl, param = 0, mu = 0):
    #print yields in format useful for Hongtaoworkspacebuilder
    for category_name, process_dict in yield_dict.items():

        #get total yield in category to help with pruning
        total_yield = 0
        total_yield_pruned = 0
        for process_name, process_yield in yield_dict[category_name].items():
            if not process_name == 'data':
                total_yield += process_yield
        #print 'Total yield is', total_yield, 'for', category_name

    #1 workspace for each klambda
        if param == 0: filename = '%s/category_%s%s_%s.xml' %(output_dir, kl, '_mu' if mu == 1 else '', category_name)
        else: filename = '%s/category_%s%s_%s.xml' %(output_dir, 'param', '_mu' if mu == 1 else '', category_name)
            
        if not os.path.exists(os.path.dirname(filename)): os.makedirs(os.path.dirname(filename))
        with open(filename, 'w') as outfile:
            outfile.write('<!DOCTYPE Channel SYSTEM \'AnaWSBuilder.dtd\'>\n')
            outfile.write('<Channel Name="' + category_name + '" Type="shape" Lumi="1">\n')

            #data or MC input for background
            outfile.write('  <Data InputFile="config/data/mass_points_data_' + category_name + '.txt" Observable="atlas_invMass_' + category_name + '[105,160]" Binning="550" InjectGhost="1" BlindRange="120,130"/>\n\n')

            outfile.write('  <Systematic Name="ATLAS_LHCmass" Constr="gaus" CentralValue="1" Mag="0.00191861859461" WhereTo="shape"/>\n')
            outfile.write('  <Systematic Name="ATLAS_lumi_run2" Constr="logn" CentralValue="1" Mag="0.017" WhereTo="yield"/>\n\n')
            outfile.write('  <Systematic Name="THEO_BR_Hyy" Constr="asym" CentralValue="1" Mag="0.0290,0.0284" WhereTo="yield"/>\n\n')

            #Impact on HH shape
            #Resolution
            for shape_group in ['HH_ggF', 'HH_VBF', 'ttH', 'ggH', 'ZH']:
                for sys_name, sys_val in syst_res_dict[category_name][shape_group].items():
                    if sys_name != 'ATLAS_EG_RESOLUTION_ALL': continue
                    if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_RES_PRUNE_THRESHOLD: continue
                    outfile.write('  <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape" Process="%s"/>\n' %(sys_name, sys_val['up'], sys_val['down'], shape_group))
                for sys_name, sys_val in syst_scale_dict[category_name][shape_group].items():
                    if sys_name != 'ATLAS_EG_SCALE_ALL': continue
                    if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_SCALE_PRUNE_THRESHOLD: continue
                    outfile.write('  <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape" Process="%s"/>\n' %(sys_name, sys_val['up'], sys_val['down'], shape_group))

            outfile.write('\n')

            for process_name, process_eff in process_dict.items():

                if process_name == 'data' or 'fwdH' in process_name: continue
                if (isHH(process_name)) and not kl in process_name: continue
                process_name_short = process_name #for parametrized workspace 
                #if (isHH(process_name)) and param == 1: process_name_short = process_name.split('_kl')[0]
                if (isHH(process_name)): process_name_short = process_name.split('_kl')[0]
                if process_eff == 0: continue
                if isHH(process_name) and 'ggF' in process_name: 
                    outfile.write('  <Sample Name="' + process_name_short + '" XSection="1" SelectionEff="1" InputFile="config/models/signal_HH_ggF_' + category_name + '.xml" ImportSyst=":common:,HH_ggF" MultiplyLumi="1">\n')
                elif isHH(process_name) and 'VBF' in process_name: 
                    outfile.write('  <Sample Name="' + process_name_short + '" XSection="1" SelectionEff="1" InputFile="config/models/signal_HH_VBF_' + category_name + '.xml" ImportSyst=":common:,HH_VBF" MultiplyLumi="1">\n')
                elif process_name == 'ttH' or process_name == 'ggH' or process_name == 'ZH':
                    outfile.write('  <Sample Name="' + process_name_short + '" XSection="1" SelectionEff="1" InputFile="config/models/signal_' + process_name_short + '_' + category_name + '.xml" ImportSyst=":common:,%s" MultiplyLumi="1">\n' %(process_name_short))
                else:
                    outfile.write('  <Sample Name="' + process_name_short + '" XSection="1" SelectionEff="1" InputFile="config/models/signal_H_' + category_name + '.xml" ImportSyst=":common:" MultiplyLumi="1">\n')

                if yield_dict[category_name][process_name] > total_yield * SIG_PRUNE_THRESHOLD:
                        total_yield_pruned += yield_dict[category_name][process_name]
                        #HF uncertainty
                        if process_name == 'ggH':
                            outfile.write('    <Systematic Name="ATLAS_HF_ggH" Constr="logn" CentralValue="1" Mag="1.0" WhereTo="yield"/>\n')
                        if process_name == 'VBF':                                                                                              
                            outfile.write('    <Systematic Name="ATLAS_HF_VBF" Constr="logn" CentralValue="1" Mag="1.0" WhereTo="yield"/>\n')
                        elif process_name == 'WH':
                            outfile.write('    <Systematic Name="ATLAS_HF_WH" Constr="logn" CentralValue="1" Mag="1.0" WhereTo="yield"/>\n')

                        #PS systematic

                        #QCDScale and PDFAlphaS and FxFx
                        if isHH(process_name): 
                            outfile.write('    <Systematic Name="THEO_BR_Hbb" Constr="asym" CentralValue="1" Mag="0.0170,0.0173" WhereTo="yield"/>\n')
                            #XS uncertainties on signal
                            if mu == 1:
                                if 'ggF' in process_name:
                                    outfile.write('    <Systematic Name="THEO_XS_SCALE_%s" Constr="asym" CentralValue="1" Mag="0.0478,-0.0846" WhereTo="yield"/>\n' %process_name_short)
                                    outfile.write('    <Systematic Name="THEO_XS_PDFalphas_%s" Constr="logn" CentralValue="1" Mag="0.03" WhereTo="yield"/>\n' %process_name_short)
                                    outfile.write('    <Systematic Name="THEO_XS_Mtop_%s" Constr="logn" CentralValue="1" Mag="0.026" WhereTo="yield"/>\n' %process_name_short)
                                elif 'VBF' in process_name: 
                                    outfile.write('    <Systematic Name="THEO_XS_SCALE_%s" Constr="asym" CentralValue="1" Mag="0.0003,-0.0004" WhereTo="yield"/>\n' %process_name_short)
                                    outfile.write('    <Systematic Name="THEO_XS_PDFalphas_%s" Constr="logn" CentralValue="1" Mag="0.021" WhereTo="yield"/>\n' %process_name_short)
                            if 'ggF' in process_name and (param or (not kl == 'kl1p0')):
                                outfile.write('    <Systematic Name="ATLAS_KLAMBDA_REWEIGHTING_%s" Constr="logn" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(process_name_short, kl_reweighting_syst[category_name]))
                        #effects on eff or (yield for single H)
                        category_dict = []
                        if isHH(process_name) and 'ggF' in process_name: category_dict = syst_th_acc_dict[category_name]['HH']
                        elif process_name == 'ZH': category_dict = syst_th_yield_dict[category_name][process_name_short] #to also include ttH when those are available 
                        elif process_name == 'ttH': category_dict = syst_th_yield_dict[category_name][process_name_short] #done!!
                        #print syst_th_acc_dict
                        if not category_dict == []:
                            for syst_name, syst_value in category_dict.items():  #format as [down, up, type]
                                if abs(syst_value['up']) < SYST_PRUNE_THRESHOLD: continue
                                if process_name == 'ZH' or process_name == 'ttH':
                                    outfile.write('    <Systematic Name="%s_%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name.replace('ACC', 'YIELD'), process_name_short, 'logn', syst_value['up']))
                                else:
                                    outfile.write('    <Systematic Name="%s_%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name, process_name_short, 'logn', syst_value['up']))

                        #Parton showering
                        category_dict = []
                        if isHH(process_name) and 'ggF' in process_name: category_dict = syst_th_PS_dict[category_name]['HH']
                        elif process_name == 'ZH': category_dict = syst_th_PS_dict[category_name][process_name_short] 
                        elif process_name == 'ttH': category_dict = syst_th_PS_dict[category_name][process_name_short] 
                        #print syst_th_acc_dict
                        if not category_dict == []:
                            for syst_name, syst_value in category_dict.items():  #format as [down, up, type]
                                if abs(syst_value['up']) < SYST_PRUNE_THRESHOLD: continue
                                outfile.write('    <Systematic Name="%s_%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name, process_name_short, 'logn', syst_value['up']))
                                
                        #Experimental yield systematics
                        if isHH(process_name) and 'ggF' in process_name: outfile.write('    <ImportItems FileName="config/systs/syst_%s_%s.xml" />\n' %('HH_ggF', category_name))
                        if isHH(process_name) and 'VBF' in process_name: outfile.write('    <ImportItems FileName="config/systs/syst_%s_%s.xml" />\n' %('HH_VBF', category_name))
                        elif process_name == 'ttH' or process_name == 'ggH' or process_name == 'ZH': outfile.write('    <ImportItems FileName="config/systs/syst_%s_%s.xml" />\n' %(process_name_short, category_name))

                #Efficiency and XSBR
                #outfile.write('    <NormFactor Name="eff_' + process_name + '_yy[' +  str(process_eff)  + ']" />\n')
                #outfile.write('    <NormFactor Name="XS13_' + process_name + '_yy[' +  str(xsbr_dict[process_name])  + ']" />\n')
                if ((isHH(process_name)) and 'ggF' in process_name and param == 1):
                    # ggF HH 
                    outfile.write('    <NormFactor Name="expr::yield_HH_ggF(\'@0 + @1*@3 + @2*@3*@3\', HH_ggF_param_p0[%s], HH_ggF_param_p1[%s], HH_ggF_param_p2[%s], klambda[1,-10,10])" />\n' %(ggF_HH_params[category_name][0], ggF_HH_params[category_name][1], ggF_HH_params[category_name][2]))
                elif ((isHH(process_name)) and 'VBF' in process_name and param == 1):
                    # VBF HH 
                    outfile.write('    <NormFactor Name="expr::yield_HH_VBF(\'@0 + @1*@3 + @2*@3*@3\', HH_VBF_param_p0[%s], HH_VBF_param_p1[%s], HH_VBF_param_p2[%s], klambda[1,-10,10])" />\n' %(VBF_HH_params[category_name][0], VBF_HH_params[category_name][1], VBF_HH_params[category_name][2]))
                else:
                    outfile.write('    <NormFactor Name="yield_' + process_name_short + '[' +  str(process_eff)  + ']" />\n')

                #disable by default
                #if 'mzp' in process_name or ((isHH(process_name)) and process_name != 'HH_ggF_kl1p0' and process_name != 'HH_VBF_kl1p0cvv1'):
                #        outfile.write('    <NormFactor Name="mu_XS_' + process_name + '[0]" />\n')
                #else:
                #        outfile.write('    <NormFactor Name="mu_XS_' + process_name + '[1]" />\n')
                outfile.write('    <NormFactor Name="mu_XS_' + process_name_short + '[1]" />\n')


                #Add the 5 POIs
                if process_name.startswith('ZH') or process_name.startswith('WH'):
                    outfile.write('    <NormFactor Name="mu_XS_VH[1]" />\n')
                if process_name.startswith('tHjb') or process_name.startswith('tWH'):
                    outfile.write('    <NormFactor Name="mu_XS_tH[1]" />\n')
                if process_name.startswith('ttH') or process_name.startswith('tHjb') or process_name.startswith('tWH'):
                    outfile.write('    <NormFactor Name="mu_XS_ttHtH[1]" />\n')

                if isHH(process_name):
                    outfile.write('    <NormFactor Name="mu_XS_HH_%s[1]" />\n' %category_name)
                    outfile.write('    <NormFactor Name="mu_XS_HH[1]" />\n')
                if isH(process_name):
                    outfile.write('    <NormFactor Name="mu_XS_H[1]" />\n')

                outfile.write('    <NormFactor Name="mu_yy[1]" />\n')
                outfile.write('    <NormFactor Name="mu[1]" />\n')
                outfile.write('  </Sample>\n\n')


            nss = ss_dict[category_name]
            outfile.write('  <Sample Name="spurious" XSection="1" SelectionEff="1" InputFile="config/models/signal_HH_ggF_' + category_name + '.xml" ImportSyst=":self:" MultiplyLumi="0">\n')
            outfile.write('    <Systematic Name="SPURIOUS_' + category_name + '" Constr="gaus" CentralValue="0" Mag="' + str(nss) + '" WhereTo="yield"/>\n')
            outfile.write('  </Sample>\n\n')

            nbkg = yield_dict[category_name]['data']
            outfile.write('  <Sample Name="background" XSection="1" SelectionEff="1" InputFile="config/models/background_' + category_name + '.xml" ImportSyst=":self:" MultiplyLumi="0">\n')
            outfile.write('    <NormFactor Name="nbkg_' + category_name + '[' + str(nbkg) + ',0,10000000]"/>\n')
            outfile.write('  </Sample>\n\n')

            outfile.write('</Channel>')

            #print 'Category', category_name, 'total pruned yield: ', total_yield_pruned, 'vs total yield: ', total_yield, '(', total_yield_pruned/total_yield * 100, ')'

def printXMLbkg(output_dir):
    #print the background cards
    for category_name in category_map.keys():
        if not os.path.exists(output_dir): os.makedirs(output_dir)
        file = open('%s/background_%s.xml' %(output_dir, category_name), "w")
        file.write('<!DOCTYPE Model SYSTEM \'AnaWSBuilder.dtd\'>\n')
        file.write('<Model Type=\"UserDef\">\n')
        file.write('  <ModelItem Name=\"EXPR::bkgPdf_%s(\'exp((@0-100)*@1/100)\', :observable:,  BKG_p0_%s[-1,-1000,1000])\"/>\n' %(category_name, category_name))
        file.write("</Model>\n")
        file.close()

DEFAULT_MODEL_PARAMETERS = {
                                'SM_1': {
                                    "muCBNom" : 125.09,
                                    "sigmaCBNom" : 1.3257,
                                    "alphaCBLo" : 1.3995,
                                    "nCBLo" : 13.428,
                                    "alphaCBHi" : 1.4617,
                                    "nCBHi" : 38.479,
                                },
                                'SM_2': {
                                    "muCBNom" : 125.13,
                                    "sigmaCBNom" : 1.4654,
                                    "alphaCBLo" : 1.4004,
                                    "nCBLo" : 11.227,
                                    "alphaCBHi" : 1.3981,
                                    "nCBHi" : 35.703,
                                },
                                'BSM_1': {
                                    "muCBNom" : 125.15,
                                    "sigmaCBNom" : 1.4995,
                                    "alphaCBLo" : 1.2834,
                                    "nCBLo" : 33.399,
                                    "alphaCBHi" : 1.3437,
                                    "nCBHi" : 37.929,
                                },
                                'BSM_2': {
                                    "muCBNom" : 125.08,
                                    "sigmaCBNom" : 1.6436,
                                    "alphaCBLo" : 1.5120,
                                    "nCBLo" : 16.392,
                                    "alphaCBHi" : 1.3689,
                                    "nCBHi" : 118.83,
                                }
                            }

def printXMLsig(output_dir, model_parameters=None):
    if model_parameters is None:
        model_parameters = DEFAULT_MODEL_PARAMETERS
    for sig in ['H', 'HH_ggF', 'HH_VBF', 'ggH', 'ttH', 'ZH']: 
        for category_name in category_map.keys():
            if not os.path.exists(output_dir): os.makedirs(output_dir)
            file = open('%s/signal_%s_%s.xml' %(output_dir, sig, category_name), "w")
            file.write('<!DOCTYPE Model SYSTEM \'AnaWSBuilder.dtd\'>\n')
            file.write('<Model Type=\"UserDef\" CacheBinning="100000">\n')
            file.write('  <Item Name=\"mHNom[125.09]\"/>\n')
            file.write('  <Item Name=\"prod::mH(mHNom, response::ATLAS_LHCmass)\"/>\n')

            if sig == 'H':
                file.write('  <Item Name=\"expr::muCB_%s(\'%s-125+@0\',mH)\"/>\n' %(sig, model_parameters[category_name]["muCBNom"]))
                file.write('  <Item Name=\"expr::sigmaCB_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["sigmaCBNom"]))
            else: 
                file.write('  <Item Name=\"expr::muCBNom_%s(\'%s-125+@0\',mH)\"/>\n' %(sig, model_parameters[category_name]["muCBNom"]))
                file.write('  <Item Name=\"prod::muCB_%s(muCBNom_%s, response::ATLAS_EG_SCALE_ALL_%s)\"/>\n' %(sig, sig, sig))
                file.write('  <Item Name=\"expr::sigmaCBNom_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["sigmaCBNom"]))
                file.write('  <Item Name=\"prod::sigmaCB_%s(sigmaCBNom_%s, response::ATLAS_EG_RESOLUTION_ALL_%s)\"/>\n' %(sig, sig, sig))

            file.write('  <Item Name=\"expr::alphaCBLo_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["alphaCBLo"]))
            file.write('  <Item Name=\"expr::alphaCBHi_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["alphaCBHi"]))
            file.write('  <Item Name=\"expr::nCBLo_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["nCBLo"]))
            file.write('  <Item Name=\"expr::nCBHi_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["nCBHi"]))
            file.write('  <ModelItem Name=\"RooTwoSidedCBShape::signal(:observable:, muCB_%s, sigmaCB_%s, alphaCBLo_%s, nCBLo_%s, alphaCBHi_%s, nCBHi_%s)"/>\n' %(sig, sig, sig, sig, sig, sig))
            file.write('</Model>')
            file.close()

def printXMLsystexpyield(syst_exp_yield_dict, output_dir):
    #print the experimental yield systematics
    for category_name in category_map.keys():
        category_dict = syst_exp_yield_dict[category_name]
        for process_name, process_dict in category_dict.items():
            if not os.path.exists(output_dir): os.makedirs(output_dir)
            file = open('%s/syst_%s_%s.xml' %(output_dir, process_name, category_name), "w")
            file.write('<!DOCTYPE SampleItems SYSTEM \'AnaWSBuilder.dtd\'>\n')
            file.write('<SampleItems>\n')
            for syst_name, syst_value in process_dict.items(): 
                #if syst_value['constr'] == 'asym':
                if max(abs(syst_value['up']), abs(syst_value['down'])) < SYST_PRUNE_THRESHOLD: continue
                file.write('  <Systematic Name="%s" Constr="%s" CentralValue="1" Mag="%s,%s" WhereTo="yield"/>\n' %(syst_name, 'asym', syst_value['up'], syst_value['down']))
                #else: #gaus or logn
                #    if abs(syst_value['up']) < SYST_PRUNE_THRESHOLD: continue
                #    file.write('  <Systematic Name="%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name, syst_value['constr'], syst_value['up']))
            file.write('</SampleItems>\n')
            file.close()

def printXMLtop(output_dir, kl, param = 0, mu = 0):
    if param: kl = 'param'
    filename = '%s/input_%s%s.xml' %(output_dir, kl, '_mu' if mu == 1 else '')
    if not os.path.exists(output_dir): os.makedirs(output_dir)
    file = open(filename, "w")
    file.write('<!DOCTYPE Combination  SYSTEM \'AnaWSBuilder.dtd\'>\n')
    file.write('<Combination WorkspaceName="combWS" ModelConfigName="ModelConfig" DataName="combData" OutputFile="workspace/WS-yybb-non-resonant_%s%s.root" Blind="0">\n' %(kl, '_mu' if mu == 1 else ''))
    file.write('\n')
    file.write('  <Input>config/categories/category_%s_SM_1.xml</Input>\n' %(kl + '_mu' if mu == 1 else kl))
    file.write('  <Input>config/categories/category_%s_SM_2.xml</Input>\n' %(kl + '_mu' if mu == 1 else kl))
    file.write('  <Input>config/categories/category_%s_BSM_1.xml</Input>\n' %(kl + '_mu' if mu == 1 else kl))
    file.write('  <Input>config/categories/category_%s_BSM_2.xml</Input>\n' %(kl + '_mu' if mu == 1 else kl))
    file.write('\n')

    if param:
        file.write('  <POI>mu_XS_HH,mu_XS_HH_SM_1,mu_XS_HH_SM_2,mu_XS_HH_BSM_1,mu_XS_HH_BSM_2,mu_XS_HH_ggF,mu_XS_HH_VBF,mu,mu_yy,mu_XS_H,mu_XS_VH,mu_XS_ttHtH,mu_XS_tH,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH,klambda</POI> \n')
        file.write('\n')
        file.write('  <Asimov Name="setup"   Setup="klambda=1,mu_XS_HH_ggF=1,mu_XS_HH_VBF=1"     Action=""/> \n')
    elif kl == 'kl1p0' or kl == 'kl10p0' or kl == 'kl0p0' or kl == 'kl2p0':
        file.write('  <POI>mu_XS_HH,mu_XS_HH_SM_1,mu_XS_HH_SM_2,mu_XS_HH_BSM_1,mu_XS_HH_BSM_2,mu_XS_HH_ggF,mu_XS_HH_VBF,mu,mu_yy,mu_XS_H,mu_XS_VH,mu_XS_ttHtH,mu_XS_tH,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH</POI> \n')
        file.write('\n')
        file.write('  <Asimov Name="setup"   Setup="mu_XS_HH_ggF=1,mu_XS_HH_VBF=1"     Action=""/> \n')
    else: 
        file.write('  <POI>mu_XS_HH,mu_XS_HH_SM_1,mu_XS_HH_SM_2,mu_XS_HH_BSM_1,mu_XS_HH_BSM_2,mu_XS_HH_ggF,mu,mu_yy,mu_XS_H,mu_XS_VH,mu_XS_ttHtH,mu_XS_tH,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH</POI> \n')
        file.write('\n')
        file.write('  <Asimov Name="setup"   Setup="mu_XS_HH_ggF=1"     Action=""/> \n')

    file.write('  <Asimov Name="POISnap"  Setup=""  Action="savesnapshot" SnapshotPOI="nominalPOI"/> \n')
    file.write('  <Asimov Name="NPSnap"   Setup="mu=1"     Action="fixsyst:fit:float:savesnapshot:nominalPOI" SnapshotNuis="nominalNuis" SnapshotGlob="nominalGlobs"/> \n')
    file.write('  <Asimov Name="asimovData_0" Setup="mu=1,mu_XS_HH=0"      Action="fit:genasimov:matchglob:savesnapshot:nominalGlobs" SnapshotGlob="conditionalGlobs_0" SnapshotNuis="conditionalNuis_0"/> \n')
    file.write('  <Asimov Name="UCMLES0Snap" Setup=""         Action="savesnapshot" SnapshotAll="ucmles_0"/> \n')
    file.write('  <Asimov Name="asimovData_1" Setup="mu=1,mu_XS_HH=1"      Action="fit:genasimov:matchglob:savesnapshot:nominalGlobs" SnapshotGlob="conditionalGlobs_1" SnapshotNuis="conditionalNuis_1"/> \n')
    file.write('  <Asimov Name="UCMLES1Snap" Setup=""         Action="savesnapshot" SnapshotAll="ucmles_1"/> \n')
    file.write('  <Asimov Name="setup2" Setup="mu_XS_HH=0" Action="nominalNuis:nominalGlobs"/> \n')
    file.write('\n')
    file.write('</Combination>')

def create_non_res_xmls(yield_path, model_parameters_path, database_path, outdir="yybb_non_resonant"):
    
    #prepare the inputs
    print('INFO: Loading information')

    #load yields
    with open(yield_path, 'r') as yield_file:
        yield_dict = yaml.safe_load(yield_file)
        
    #load signal model parameters
    with open(model_parameters_path, 'r') as param_file:
        model_parameters = json.load(param_file) 
        
    marc_syst_path = os.path.join(database_path, 'NonResonant_IJCLab', 'h026_baseline_4categories_ggH')
    #load experimental systematics
    syst_res_dict = nested_dict()
    syst_scale_dict = nested_dict()
    syst_exp_yield_dict = nested_dict()
    exp_syst_path = os.path.join(marc_syst_path, 'Experimental_systematics')
    for category in category_map: #to be added: VBFHH
        load_Marc_txt(syst_res_dict, category, 'HH_ggF', '%s/version_with_gg_HH_from_Py8/datacard_experimental_spread_shape_m_yy_proc_gg_HH_non_resonant_Max_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'HH_ggF', '%s/version_with_gg_HH_from_Py8/datacard_experimental_position_shape_m_yy_proc_gg_HH_non_resonant_Max_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'HH_ggF', '%s/version_with_gg_HH_from_Py8/datacard_experimental_yield_proc_gg_HH_non_resonant_Max_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))

        load_Marc_txt(syst_res_dict, category, 'HH_VBF', '%s/datacard_experimental_spread_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_vbf_HH_non_resonant_kappa_lambda_01_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'HH_VBF', '%s/datacard_experimental_position_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_vbf_HH_non_resonant_kappa_lambda_01_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'HH_VBF', '%s/datacard_experimental_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_vbf_HH_non_resonant_kappa_lambda_01_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))

        load_Marc_txt(syst_res_dict, category, 'ttH', '%s/datacard_experimental_spread_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'ttH', '%s/datacard_experimental_position_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'ttH', '%s/datacard_experimental_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_res_dict, category, 'ggH', '%s/datacard_experimental_spread_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_NNLOPS_ggH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'ggH', '%s/datacard_experimental_position_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_NNLOPS_ggH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'ggH', '%s/datacard_experimental_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_NNLOPS_ggH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_res_dict, category, 'ZH', '%s/datacard_experimental_spread_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_ZH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'ZH', '%s/datacard_experimental_position_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_ZH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'ZH', '%s/datacard_experimental_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_ZH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))

    #theoretical
    theo_syst_path = os.path.join(marc_syst_path, 'Theoretical_systematics')
    syst_th_res_dict = nested_dict()
    syst_th_xs_dict = nested_dict()
    syst_th_acc_dict = nested_dict()
    syst_th_yield_dict = nested_dict()
    syst_th_PS_dict = nested_dict()

    for category in category_map: 
        load_Marc_txt(syst_th_acc_dict, category, 'HH', '%s/HH_version_efficiency/version_gg_HH_Py8/datacard_theory_yield_proc_gg_HH_non_resonant_Max_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        load_Marc_txt(syst_th_yield_dict, category, 'ttH', '%s/SingleHiggs_version_yield/datacard_theory_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        load_Marc_txt(syst_th_yield_dict, category, 'ZH', '%s/SingleHiggs_version_yield/datacard_theory_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_ZH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        load_Marc_txt(syst_th_PS_dict, category, 'HH', '%s/datacard_theory_yield_parton_showering_proc_HH_non_resonant_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))

    ps_syst_path = os.path.join(database_path, 'NonResonant_Wisc', 'SingleHiggs_PS')
    for category in category_map: 
        load_Alex_txt(syst_th_PS_dict, category, 'ttH', '%s/datacard_yield_ttH_%s' %(ps_syst_path, category_map[category]))
        load_Alex_txt(syst_th_PS_dict, category, 'ZH', '%s/datacard_yield_ZH_%s' %(ps_syst_path, category_map[category]))

    ss_syst_path = os.path.join(marc_syst_path, 'BkgModelling_SpuriousSignal')
    ss_dict = {}
    for category in category_map: 
        f = open('%s/version_where_gg_part_of_shape_is_Py8/spurious_signal_spurious_signal_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(ss_syst_path, category_map[category]))
        lines = f.readlines()
        for x in lines:
            ss_dict[category] = float(x.replace('\n', ''))
    print('INFO: Printing category cards')

    lambdas = np.linspace(-10, 10, 101)
    xml_path = os.path.join(outdir, 'config')
    category_xml_path = os.path.join(xml_path, 'categories')
    # mu workspace (including theoretical uncertainties on signal XS)
    from pdb import set_trace
    set_trace()
    printXML(yield_dict, syst_res_dict, syst_scale_dict, syst_exp_yield_dict, syst_th_res_dict, syst_th_xs_dict, syst_th_acc_dict, syst_th_yield_dict, syst_th_PS_dict, ss_dict, category_xml_path, 'kl1p0', param = 0, mu = 0)
    printXMLtop(xml_path, 'kl1p0', param = 0, mu = 0)    

    # and for the parametrized workspace: 
    printXML(yield_dict, syst_res_dict, syst_scale_dict, syst_exp_yield_dict, syst_th_res_dict, syst_th_xs_dict, syst_th_acc_dict, syst_th_yield_dict, syst_th_PS_dict, ss_dict, category_xml_path, 'kl1p0', param = 1, mu = 0)
    printXMLtop(xml_path, 'kl1p0', param = 1, mu = 0)

    print('INFO: Printing background model cards')
    model_xml_path = os.path.join(xml_path, 'models')
    printXMLbkg(model_xml_path)

    print('INFO: Printing signal model cards')
    printXMLsig(model_xml_path, model_parameters)

    print('INFO: Printing experimental systematic yield cards')
    syst_xml_path = os.path.join(xml_path, 'systs')
    printXMLsystexpyield(syst_exp_yield_dict, syst_xml_path)

    print('INFO: Done')
    
    
    
    
def create_non_resonant_xmls(yields_path, model_parameters_path, database_path, 
    experimental_systematics_path=None, outdir='yybb_non_resonant'):
    
    non_resonant_path = os.path.join(database_path, 'NonResonant_IJCLab', 'h026_baseline_4categories_ggH')
    exp_syst_path = os.path.join(non_resonant_path, "Experimental_systematics")
    spurious_signal_path = os.path.join(non_resonant_path , "BkgModelling_SpuriousSignal")
    
    if experimental_systematics_path is not None:
        experimental_systematics = json.load(open(experimental_systematics_path))
        append_ATLAS_label(experimental_systematics)
    else:
        # extract systematics information from database
        experimental_systematics = extract_experimental_systematics(exp_syst_path)
    spurious_signal_systematics = extract_spurious_signal_systematics(spurious_signal_path)
    theory_systematics = extract_theory_systematics()
    systematics = merge_systematics([experimental_systematics, theory_systematics])
    
    # extract yield, efficiency and signal model_parameters from user input
    yields = yaml.safe_load(open(yields_path))
    efficiency = yaml.safe_load(open(efficiency_path))
    model_parameters = json.load(open(model_parameters_path))
    
    input_outdir = os.path.join(outdir, "config")
    category_outdir = os.path.join(outdir, "config", "categories")
    model_outdir = os.path.join(outdir, "config", "models")
    makedirs([outdir, input_outdir, category_outdir, model_outdir])
    
    processes = list(process_map['resonant'].keys())
    for mass in mass_points:
        mass_str = mass.replace('.', 'p')
        xml = create_category_xml(yields[mass_str], efficiency[mass_str], systematics, 
                                  processes, mass=mass_str,
                                  spurious_signal=spurious_signal_systematics[mass_str],
                                  br="0.002637286", background="10")
        xml.save(os.path.join(category_outdir, 'category_{}.xml'.format(mass_str)))
        model_xmls = create_model_xml(model_parameters[mass_str], systematics, processes)
        for key in model_xmls:
            model_xmls[key].save(os.path.join(model_outdir, "{}_{}.xml".format(key, mass_str)))
        input_xml = create_input_xml(mass_str, processes)
        input_xml.save(os.path.join(input_outdir, "input_{}.xml".format(mass_str)))    