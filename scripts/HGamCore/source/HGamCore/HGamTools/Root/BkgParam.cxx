#include "HGamTools/BkgParam.h"

#include "TCanvas.h"
#include "TFile.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TH1.h"
#include "TChain.h"
#include "TSystem.h"
#include "TLine.h"
#include "TLegend.h"

#include "RooDataHist.h"

#include <iostream>
#include <fstream>
#include <sstream>
using std::cout;
using std::endl;
using std::ofstream;

using namespace BkgTool;


/////////////////////////////////////////////
//
// Spurious Signal Selection
//
/////////////////////////////////////////////


bool SpuriousSignalSelector::setup()
{
  if (!Selector::setup()) { return false; }

  // Setup the selection
  m_maxSigOverError = m_reader.GetValue(GetName() + ".MaxSignalOverError", -1.0);
  m_maxSigOverRef   = m_reader.GetValue(GetName() + ".MaxSignalOverRef", -1.0);
  m_oneSigmaSigOverError = m_reader.GetValue(GetName() + ".MaxOneSigmaSignalOverError", -1.0);
  m_oneSigmaSigOverRef   = m_reader.GetValue(GetName() + ".MaxOneSigmaSignalOverRef", -1.0);
  m_twoSigmaSigOverError = m_reader.GetValue(GetName() + ".MaxTwoSigmaSignalOverError", -1.0);
  m_twoSigmaSigOverRef   = m_reader.GetValue(GetName() + ".MaxTwoSigmaSignalOverRef", -1.0);
  m_minChi2Pvalue = m_reader.GetValue(GetName() + ".MinChiSquarePvalue", -1.0);

  if (m_maxSigOverError < 0 && m_maxSigOverRef < 0 && m_oneSigmaSigOverError < 0 && m_oneSigmaSigOverRef < 0
      && m_twoSigmaSigOverError < 0 && m_twoSigmaSigOverRef < 0) {
    cout << Form("ERROR: must specify at least one selection criterion using %s.MaxSignalOverError or %s.MaxSignalOverRefSignal statement", GetName().Data(), GetName().Data()) << endl;
    return false;
  }

  if (m_maxSigOverRef > 0 || m_oneSigmaSigOverRef > 0 || m_twoSigmaSigOverRef > 0) {
    if (!m_nSignalRef.setup(m_reader, m_scanVar->GetName(), m_targetLumi)) {
      cout << "ERROR: must set a reference signal for selection using the RefSignalYield or RefSignalCrossSection syntax." << endl;
      return false;
    }
  }

  if (verbosity(1)) {
    if (m_maxSigOverError > 0) { cout << Form("INFO: will apply the selection Spurious_Signal < %.1f%% Signal_Error", m_maxSigOverError * 100) << endl; }

    if (m_maxSigOverRef   > 0) { cout << Form("INFO: will apply the selection Spurious_Signal < %.1f%% Ref_Signal", m_maxSigOverRef * 100) << endl; }

    if (m_oneSigmaSigOverError > 0) { cout << Form("INFO: will apply the selection Spurious_Signal_OneSigmaCompatibility < %.1f%% Signal_Error", m_oneSigmaSigOverError * 100) << endl; }

    if (m_oneSigmaSigOverRef   > 0) { cout << Form("INFO: will apply the selection Spurious_Signal_OneSigmaCompatibility < %.1f%% Ref_Signal", m_oneSigmaSigOverRef * 100) << endl; }

    if (m_twoSigmaSigOverError > 0) { cout << Form("INFO: will apply the selection Spurious_Signal_TwoSigmaCompatibility < %.1f%% Signal_Error", m_twoSigmaSigOverError * 100) << endl; }

    if (m_twoSigmaSigOverRef   > 0) { cout << Form("INFO: will apply the selection Spurious_Signal_TwoSigmaCompatibility < %.1f%% Ref_Signal", m_twoSigmaSigOverRef * 100) << endl; }

    if (m_minChi2Pvalue > 0) { cout << Form("INFO: will apply the selection p(chi2) > %.1f%%", m_minChi2Pvalue * 100) << endl; }

    std::string tmp_sortingOption = m_reader.GetValue(GetName() + ".SortingOption", "Default");

    if (tmp_sortingOption != "Default" && tmp_sortingOption != "TotalError" && tmp_sortingOption != "TieredTotalError" && tmp_sortingOption != "TieredDefault") {
      cout << "ERROR: " << GetName() + ".SortingOption " << tmp_sortingOption << " is unknown! Exiting." << endl;
      return false;
    }

    cout << "INFO: Using sorting option (" << GetName() + ".SortingOption" << ") " << tmp_sortingOption << endl;
  }

  return true;
}


bool SpuriousSignalSelector::performFits(Parameterization &pdf)
{
  RooArgSet *initialState = (RooArgSet *)pdf.parameters().snapshot();

  if (verbosity(1)) cout << Form("INFO: performing fits for PDF '%s' in %d steps from %g to %g %s",
                                   pdf.GetName().Data(), nSteps(),  m_scanVar->getMin(), m_scanVar->getMax(), m_scanVar->getUnit()) << endl;

  TCanvas *c1 = (m_perPointPlots ? new TCanvas("perPoint", "", 800, 600) : 0);

  for (unsigned int i = 0; i < nSteps(); i++) {
    double val = m_scanVar->getMin() + i * m_scanStep;

    if (!mkdir(pdf.GetName(), pointDir(i))) {
      if (verbosity(2)) { cout << Form("INFO: skipping already processed point %d (val = %g)", i, val) << endl; }

      continue;
    }

    if (verbosity(2)) cout << "INFO: performing S+B fit for PDF '" << pdf.GetName() << "' at point " << i << ", "
                             << m_scanVar->GetName() << " = " << val << endl;

    pdf.parameters() = *initialState;
    pdf.scanVar()->setVal(val);

    if (!pdf.fit(*m_data.data(), m_fitOptionsF, m_fitOptionsT, path(pdf.GetName(), pointDir(i)), c1)) { return false; }
  }

  // Now do background-only fit
  if (!mkdir(pdf.GetName(), "BkgOnly")) {
    if (verbosity(2)) { cout << "INFO: skipping already processed bkg-only fit." << endl; }
  } else {
    if (verbosity(2)) { cout << "INFO: performing Bkg-only fit for PDF '" << pdf.GetName() << "'." << std::endl; }

    if (!pdf.fit(*m_data.data(), m_fitOptionsBkgF, m_fitOptionsBkgT, path(pdf.GetName(), "BkgOnly"), c1, true, m_data.data_forChi2())) { return false; }
  }

  delete initialState;

  if (c1) { delete c1; }

  return true;
}


bool SpuriousSignalSelector::makeResults(std::vector<Result *> &results)
{
  // Compute spurious signal over specified ranges and apply selection
  TString outputFile = path("spurious_signal.root");

  if (verbosity(2)) { cout << "INFO: creating spurious signal selection results" << endl; }

  TFile *f = TFile::Open(outputFile, "RECREATE");
  TCanvas *c1 = new TCanvas("c1", "", 800, 600);
  double maxZ  = (m_maxSigOverError > 0 ? m_maxSigOverError : -9999);
  double maxMu = (m_maxSigOverRef   > 0 ? m_maxSigOverRef   : -9999);
  double maxOneSigmaZ  = (m_oneSigmaSigOverError > 0 ? m_oneSigmaSigOverError : -9999);
  double maxOneSigmaMu = (m_oneSigmaSigOverRef   > 0 ? m_oneSigmaSigOverRef   : -9999);
  double maxTwoSigmaZ  = (m_twoSigmaSigOverError > 0 ? m_twoSigmaSigOverError : -9999);
  double maxTwoSigmaMu = (m_twoSigmaSigOverRef   > 0 ? m_twoSigmaSigOverRef   : -9999);
  std::vector<TGraphErrors *> all_nSignal, all_nSignal_sumW2Errors, all_signalZ, all_signalMu, all_signalZ_winMax, all_signalMu_winMax;
  TLegend *l = new TLegend(0.6, 0.9, 0.9, 0.75);

  for (auto pdf : m_candidates) {
    if (verbosity(2)) { cout << Form("INFO: processing spurious signal results for PDF '%s'", pdf.GetName().Data()) << endl; }

    if (!performFits(pdf)) { return false; }

    TGraphErrors *nSignal = fitValues("nSignal", pdf, false, pdf.GetName(), "Spurious Signal");
    TGraphErrors *nSignal_sumW2Errors = fitValues("nSignal", pdf, true, pdf.GetName(), "Spurious Signal");

    if (!nSignal || !nSignal_sumW2Errors) {
      cout << Form("ERROR : invalid fit results for function '%s', please delete the output and re-run the computation", pdf.GetName().Data()) << endl;
      return false;
    }

    TGraphErrors *signalZ = Z(*nSignal, *nSignal_sumW2Errors, pdf.GetName() + "_Z", "S/#deltaS");
    TGraphErrors *nSignalRef = m_nSignalRef.hasValue() ? functionValues(m_nSignalRef, pdf.GetName() + "_nSignalRef", "S_{Ref}") : 0;
    TGraphErrors *signalMu = nSignalRef ? ratio(*nSignal_sumW2Errors, *nSignalRef, pdf.GetName() + "_Mu", "S/S_{Ref}") : 0;
    TGraphErrors *signalZ_af2_1sigma  = Z_af2_1sigma(*nSignal, *nSignal_sumW2Errors, pdf.GetName() + "_ZAF2_1sigma", "(S#pm#sigma)/#deltaS");
    TGraphErrors *signalMu_af2_1sigma = nSignalRef ? ratio_af2_1sigma(*nSignal_sumW2Errors, *nSignalRef, pdf.GetName() + "_MuAF2_1sigma", "(S#pm#sigma)/S_{Ref}") : 0;
    TGraphErrors *signalZ_af2_2sigma  = Z_af2_2sigma(*nSignal, *nSignal_sumW2Errors, pdf.GetName() + "_ZAF2_2sigma", "(S#pm2#sigma)/#deltaS");
    TGraphErrors *signalMu_af2_2sigma = nSignalRef ? ratio_af2_2sigma(*nSignal_sumW2Errors, *nSignalRef, pdf.GetName() + "_MuAF2_2sigma", "(S#pm2#sigma)/S_{Ref}") : 0;
    TGraphErrors *nSignal_winMax = winMax(*nSignal);
    TGraphErrors *signalZ_winMax = winMax(*signalZ);
    TGraphErrors *signalMu_winMax = signalMu ? winMax(*signalMu) : 0;
    TGraphErrors *signalZ_af2_1sigma_winMax = signalZ_af2_1sigma ? winMax(*signalZ_af2_1sigma) : 0;
    TGraphErrors *signalMu_af2_1sigma_winMax = signalMu_af2_1sigma ? winMax(*signalMu_af2_1sigma) : 0;
    TGraphErrors *signalZ_af2_2sigma_winMax = signalZ_af2_2sigma ? winMax(*signalZ_af2_2sigma) : 0;
    TGraphErrors *signalMu_af2_2sigma_winMax = signalMu_af2_2sigma ? winMax(*signalMu_af2_2sigma) : 0;
    f->cd();
    nSignal->Write();
    nSignal_sumW2Errors->Write();
    signalZ->Write();
    nSignal_winMax->Write();
    signalZ_winMax->Write();

    if (signalMu) {
      signalMu->Write();
      signalMu_winMax->Write();
      nSignalRef->Write();
    }

    if (m_oneSigmaSigOverError > 0) {
      signalZ_af2_1sigma->Write();
      signalZ_af2_1sigma_winMax->Write();
    }

    if (m_oneSigmaSigOverRef > 0) {
      signalMu_af2_1sigma->Write();
      signalMu_af2_1sigma_winMax->Write();
    }

    if (m_twoSigmaSigOverError > 0) {
      signalZ_af2_2sigma->Write();
      signalZ_af2_2sigma_winMax->Write();
    }

    if (m_twoSigmaSigOverRef > 0) {
      signalMu_af2_2sigma->Write();
      signalMu_af2_2sigma_winMax->Write();
    }

    drawGraph(*nSignal_sumW2Errors,  "3A", 1, 4, 0);
    c1->Print(path(pdf.GetName() + "_nSignal_sumW2Errors.pdf"));
    drawGraph(*nSignal_sumW2Errors, "LAX", 1, 4, 0);
    c1->Print(path(pdf.GetName() + "_nSignal_sumW2Errors_noErr.pdf"));
    drawGraph(*signalZ,              "3A", 1, 4, -maxZ,  maxZ);
    c1->Print(path(pdf.GetName() + "_Z.pdf"));
    drawGraph(*signalZ,             "LAX", 1, 4, -maxZ,  maxZ);
    c1->Print(path(pdf.GetName() + "_Z_noErr.pdf"));
    drawGraph(*nSignal,              "3A", 1, 4, 0);
    c1->Print(path(pdf.GetName() + "_nSignal.pdf"));
    drawGraph(*nSignal,             "LAX", 1, 4, 0);
    c1->Print(path(pdf.GetName() + "_nSignal_noErr.pdf"));
    drawGraph(*nSignal_winMax,       "3A", 1, 4, 0);
    c1->Print(path(pdf.GetName() + "_nSignal_winMax.pdf"));
    drawGraph(*nSignal_winMax,      "LAX", 1, 4, 0);
    c1->Print(path(pdf.GetName() + "_nSignal_winMax_noErr.pdf"));
    drawGraph(*signalZ_winMax,       "3A", 1, 4, -maxZ,  maxZ);
    c1->Print(path(pdf.GetName() + "_Z_winMax.pdf"));
    drawGraph(*signalZ_winMax,      "LAX", 1, 4, -maxZ,  maxZ);
    c1->Print(path(pdf.GetName() + "_Z_winMax_noErr.pdf"));

    if (m_oneSigmaSigOverError > 0) {
      drawGraph(*signalZ_af2_1sigma,  "3A", 1, 4, -maxOneSigmaZ, maxOneSigmaZ);
      c1->Print(path(pdf.GetName() + "_Z_af2_1sigma.pdf"));
      drawGraph(*signalZ_af2_1sigma_winMax, "3A", 1, 4, -maxOneSigmaZ, maxOneSigmaZ);
      c1->Print(path(pdf.GetName() + "_Z_af2_1sigma_winMax.pdf"));
    }

    if (m_oneSigmaSigOverRef > 0) {
      drawGraph(*signalMu_af2_1sigma,  "3A", 1, 4, -maxOneSigmaMu, maxOneSigmaMu);
      c1->Print(path(pdf.GetName() + "_Mu_af2_1sigma.pdf"));
      drawGraph(*signalMu_af2_1sigma_winMax, "3A", 1, 4, -maxOneSigmaMu, maxOneSigmaMu);
      c1->Print(path(pdf.GetName() + "_Mu_af2_1sigma_winMax.pdf"));
    }

    if (m_twoSigmaSigOverError > 0) {
      drawGraph(*signalZ_af2_2sigma,  "3A", 1, 4, -maxTwoSigmaZ, maxTwoSigmaZ);
      c1->Print(path(pdf.GetName() + "_Z_af2_2sigma.pdf"));
      drawGraph(*signalZ_af2_2sigma_winMax, "3A", 1, 4, -maxTwoSigmaZ, maxTwoSigmaZ);
      c1->Print(path(pdf.GetName() + "_Z_af2_2sigma_winMax.pdf"));
    }

    if (m_twoSigmaSigOverRef > 0) {
      drawGraph(*signalMu_af2_2sigma,  "3A", 1, 4, -maxTwoSigmaMu, maxTwoSigmaMu);
      c1->Print(path(pdf.GetName() + "_Mu_af2_2sigma.pdf"));
      drawGraph(*signalMu_af2_2sigma_winMax, "3A", 1, 4, -maxTwoSigmaMu, maxTwoSigmaMu);
      c1->Print(path(pdf.GetName() + "_Mu_af2_2sigma_winMax.pdf"));
    }

    all_nSignal.push_back(nSignal);
    all_nSignal_sumW2Errors.push_back(nSignal_sumW2Errors);
    all_signalZ.push_back(signalZ);
    all_signalZ_winMax.push_back(signalZ_winMax);

    if (signalMu) {
      drawGraph(*signalMu,           "3A", 1, 4, -maxMu, maxMu);
      c1->Print(path(pdf.GetName() + "_Mu.pdf"));
      drawGraph(*signalMu,          "LAX", 1, 4, -maxMu, maxMu);
      c1->Print(path(pdf.GetName() + "_Mu_noErr.pdf"));
      drawGraph(*signalMu_winMax,    "3A", 1, 4, -maxMu, maxMu);
      c1->Print(path(pdf.GetName() + "_Mu_winMax.pdf"));
      drawGraph(*signalMu_winMax,   "LAX", 1, 4, -maxMu, maxMu);
      c1->Print(path(pdf.GetName() + "_Mu_winMax_noErr.pdf"));
      all_signalMu.push_back(signalMu);
      all_signalMu_winMax.push_back(signalMu_winMax);
    }

    l->AddEntry(nSignal, pdf.GetName(), "L");

    // Total error (at mH = 125)
    double relative_stat_error_125 = 1000;

    for (int i = 0; i < nSignalRef->GetN(); i++) {
      if (nSignalRef->GetX()[i] == 125. && nSignal->GetX()[i] == 125.) {
        relative_stat_error_125 = nSignal->GetEY()[i] / nSignalRef->GetY()[i];
      }
    }

    // Chi2 and its associated p-value
    double chi2_over_ndof = 0, pvalue_chi2 = 0;
    int ndof = 0;
    (void)ndof;
    // Retrieve the chi2 from txt file (see Parameterization::writeBkgOnlyChiSquare)
    std::ifstream chi2_if(path(pdf.GetName(), "BkgOnly", "chi2.txt"));
    std::string store;

    while (std::getline(chi2_if, store)) {
      std::stringstream line(store);
      std::string token;

      // These values are written in a particular format in Parameterization::writeBkgOnlyChiSquare
      if (line >> token) { chi2_over_ndof = atof(token.c_str()); }

      if (line >> token) { ndof = stoi(token); }

      if (line >> token) { pvalue_chi2 = atof(token.c_str()); }
    }

    chi2_if.close();

    // Final results
    SpuriousSignalResult *result = new SpuriousSignalResult(pdf.GetName());
    std::string sort_option = m_reader.GetValue(GetName() + ".SortingOption", "Default");
    result->SetSortingOption(sort_option);
    result->add("max(S/deltaS) [%]", max_val(*signalZ_winMax) * 100);

    if (maxOneSigmaZ  > 0) { result->add("max(1sigma/deltaS) [%]", max_val(*signalZ_af2_1sigma_winMax) * 100); }

    if (maxTwoSigmaZ  > 0) { result->add("max(2sigma/deltaS) [%]", max_val(*signalZ_af2_2sigma_winMax) * 100); }

    if (signalMu) { result->add("max(S/Sref) [%]", max_val(*signalMu_winMax) * 100); }

    if (maxOneSigmaMu > 0) { result->add("max(1sigma/Sref) [%]", max_val(*signalMu_af2_1sigma_winMax) * 100); }

    if (maxTwoSigmaMu > 0) { result->add("max(2sigma/Sref) [%]", max_val(*signalMu_af2_2sigma_winMax) * 100); }

    result->add("max(S)", max_val(*nSignal_winMax));
    result->add("nPars", pdf.bkgParameters().getSize());
    result->add("chi2/ndof", chi2_over_ndof);
    result->add("Prob(chi2) [%]", pvalue_chi2 * 100.);
    result->add("Stat Err [%]", relative_stat_error_125 * 100.);
    result->add("Relative Tot Err [%]", sqrt(pow(result->value("max(S/Sref) [%]"), 2) + pow(result->value("Stat Err [%]"), 2)));

    if (verbosity(2)) {
      double z = fabs(result->value("max(S/deltaS) [%]") / 100), mu = fabs(result->value("max(S/Sref) [%]") / 100);
      double oneSigmaZ  = fabs(result->value("max(1sigma/deltaS) [%]") / 100);
      double oneSigmaMu = fabs(result->value("max(1sigma/Sref) [%]") / 100);
      double twoSigmaZ  = fabs(result->value("max(2sigma/deltaS) [%]") / 100);
      double twoSigmaMu = fabs(result->value("max(2sigma/Sref) [%]") / 100);

      if (maxZ  > 0) { cout << Form("INFO: applying max(S/deltaS) > %g selection on %g ==> %s", maxZ,   z,  z > maxZ  ? "FAIL" : "PASS") << endl; }

      if (maxMu > 0) { cout << Form("INFO: applying max(S/Sref)   > %g selection on %g ==> %s", maxMu, mu, mu > maxMu ? "FAIL" : "PASS") << endl; }

      if (maxOneSigmaZ  > 0) { cout << Form("INFO: applying max(1sigma/deltaS) > %g selection on %g ==> %s", maxOneSigmaZ, oneSigmaZ, oneSigmaZ  > maxOneSigmaZ  ? "FAIL" : "PASS") << endl; }

      if (maxOneSigmaMu > 0) { cout << Form("INFO: applying max(1sigma/Sref)   > %g selection on %g ==> %s", maxOneSigmaMu, oneSigmaMu, oneSigmaMu > maxOneSigmaMu ? "FAIL" : "PASS") << endl; }

      if (maxTwoSigmaZ  > 0) { cout << Form("INFO: applying max(2sigma/deltaS) > %g selection on %g ==> %s", maxTwoSigmaZ, twoSigmaZ, twoSigmaZ  > maxTwoSigmaZ  ? "FAIL" : "PASS") << endl; }

      if (maxTwoSigmaMu > 0) { cout << Form("INFO: applying max(2sigma/Sref)   > %g selection on %g ==> %s", maxTwoSigmaMu, twoSigmaMu, twoSigmaMu > maxTwoSigmaMu ? "FAIL" : "PASS") << endl; }
    }

    if (sort_option.find("Tiered") != std::string::npos) {
      if (m_minChi2Pvalue > 0)
      { result->add("passT0", m_minChi2Pvalue <= 0 || result->value("Prob(chi2) [%]") / 100. > m_minChi2Pvalue); }

      if (maxZ > 0 || maxMu > 0)
        result->add("passT1", (maxZ > 0 && fabs(result->value("max(S/deltaS) [%]") / 100) < maxZ) ||
                    (maxMu > 0 && fabs(result->value("max(S/Sref) [%]") / 100) < maxMu));

      if (maxOneSigmaZ > 0 || maxTwoSigmaZ > 0)
        result->add("passT2", (maxOneSigmaZ > 0  && fabs(result->value("max(1sigma/deltaS) [%]") / 100) < maxOneSigmaZ) ||
                    (maxOneSigmaMu > 0 && fabs(result->value("max(1sigma/Sref) [%]") / 100) < maxOneSigmaMu));

      if (maxTwoSigmaZ > 0 || maxTwoSigmaZ > 0)
        result->add("passT3", (maxTwoSigmaZ > 0  && fabs(result->value("max(2sigma/deltaS) [%]") / 100) < maxTwoSigmaZ) ||
                    (maxTwoSigmaMu > 0 && fabs(result->value("max(2sigma/Sref) [%]") / 100) < maxTwoSigmaMu));
    }

    result->setPassing(((maxZ > 0 && fabs(result->value("max(S/deltaS) [%]") / 100) < maxZ) ||
                        (maxMu > 0 && fabs(result->value("max(S/Sref) [%]") / 100) < maxMu) ||
                        (maxOneSigmaZ > 0  && fabs(result->value("max(1sigma/deltaS) [%]") / 100) < maxOneSigmaZ) ||
                        (maxOneSigmaMu > 0 && fabs(result->value("max(1sigma/Sref) [%]") / 100) < maxOneSigmaMu) ||
                        (maxTwoSigmaZ > 0  && fabs(result->value("max(2sigma/deltaS) [%]") / 100) < maxTwoSigmaZ) ||
                        (maxTwoSigmaMu > 0 && fabs(result->value("max(2sigma/Sref) [%]") / 100) < maxTwoSigmaMu))
                       && (m_minChi2Pvalue <= 0 || result->value("Prob(chi2) [%]") / 100. > m_minChi2Pvalue)
                      );
    results.push_back(result);
  }

  drawGraphs(all_nSignal, "LX", 0);
  c1->Print(path("all_nSignal_noErr.pdf"));
  drawGraphs(all_nSignal, "3", 0);
  c1->Print(path("all_nSignal.pdf"));
  l->Draw();
  c1->Print(path("all_nSignal_legend.pdf"));
  drawGraphs(all_nSignal_sumW2Errors, "L", 0);
  c1->Print(path("all_nSignal_sumW2Errors_noErr.pdf"));
  drawGraphs(all_nSignal_sumW2Errors, "3", 0);
  c1->Print(path("all_nSignal_sumW2Errors.pdf"));
  l->Draw();
  c1->Print(path("all_nSignal_sumW2Errors_legend.pdf"));
  drawGraphs(all_signalZ, "LX", -maxZ,  maxZ);
  c1->Print(path("all_Z_noErr.pdf"));
  drawGraphs(all_signalZ, "3", -maxZ,  maxZ);
  c1->Print(path("all_Z.pdf"));
  l->Draw();
  c1->Print(path("all_Z_legend.pdf"));

  if (all_signalMu.size()) { drawGraphs(all_signalMu, "LX", -maxMu, maxMu); c1->Print(path("all_Mu_noErr.pdf")); }

  if (all_signalMu.size()) { drawGraphs(all_signalMu, "3", -maxMu, maxMu); c1->Print(path("all_Mu.pdf")); }

  if (all_signalMu.size()) { l->Draw(); c1->Print(path("all_Mu_legend.pdf")); }

  drawGraphs(all_signalZ_winMax, "LX", -maxZ,  maxZ);
  c1->Print(path("all_Z_winMax_noErr.pdf"));
  drawGraphs(all_signalZ_winMax, "3", -maxZ,  maxZ);
  c1->Print(path("all_Z_winMax.pdf"));
  l->Draw();
  c1->Print(path("all_Z_winMax_legend.pdf"));

  if (all_signalMu.size()) { drawGraphs(all_signalMu_winMax, "LX", -maxMu, maxMu); c1->Print(path("all_Mu_winMax_noErr.pdf")); }

  if (all_signalMu.size()) { drawGraphs(all_signalMu_winMax, "3", -maxMu, maxMu); c1->Print(path("all_Mu_winMax.pdf")); }

  if (all_signalMu.size()) { l->Draw(); c1->Print(path("all_Mu_winMax_legend.pdf")); }

  delete c1;
  delete l;
  delete f;
  return true;
}


bool SpuriousSignalResult::operator<(const Result &other) const // "less" means "worse result"
{
  // sorting option is set using "Selection.SortingOption" in config file.
  if (m_sortingOption == "TotalError") {
    if (isPassing() != other.isPassing()) { return !isPassing(); }

    return (value("Relative Tot Err [%]") > other.value("Relative Tot Err [%]"));
  }

  // sorting option is set using "Selection.SortingOption" in config file.
  if (m_sortingOption == "TieredTotalError") {
    if (isPassing() != other.isPassing()) { return !isPassing(); }

    if (value("passT0") != other.value("passT0")) { return !value("passT0"); }

    if (value("passT1") != other.value("passT1")) { return !value("passT1"); }

    if (value("passT2") != other.value("passT2")) { return !value("passT2"); }

    if (value("passT3") != other.value("passT3")) { return !value("passT3"); }

    return (value("Relative Tot Err [%]") > other.value("Relative Tot Err [%]"));
  }

  // sorting option is set using "Selection.SortingOption" in config file.
  if (m_sortingOption == "TieredDefault") {
    if (isPassing() != other.isPassing()) { return !isPassing(); }

    if (value("passT0") != other.value("passT0")) { return !value("passT0"); }

    if (value("passT1") != other.value("passT1")) { return !value("passT1"); }

    if (value("passT2") != other.value("passT2")) { return !value("passT2"); }

    if (value("passT3") != other.value("passT3")) { return !value("passT3"); }

    if (value("nPars") != other.value("nPars")) { return (value("nPars") > other.value("nPars")); }

    return fabs(value("max(S)")) > fabs(other.value("max(S)"));
  }

  // default option.
  if (isPassing() != other.isPassing()) { return !isPassing(); }

  if (value("nPars") != other.value("nPars")) { return (value("nPars") > other.value("nPars")); }

  return fabs(value("max(S)")) > fabs(other.value("max(S)"));

}


/////////////////////////////////////////////
//
// Mass Bias Selection
//
/////////////////////////////////////////////


bool MassBiasSelector::setup()
{
  if (!Selector::setup()) { return false; }

  // Setup the selection
  m_maxDeltaMOverM = m_reader.GetValue(GetName() + ".MaxDeltaMOverM", -1.0);

  if (!m_nSignalRef.setup(m_reader, m_scanVar->GetName(), m_targetLumi)) {
    cout << "ERROR: must set a reference signal for selection using the RefSignalYield or RefSignalCrossSection syntax." << endl;
    return false;
  }

  if (m_maxDeltaMOverM < 0) {
    cout << Form("ERROR: must specify a mass bias selection using %s.MaxDeltaMOverM  = ...", GetName().Data()) << endl;
    return false;
  }

  if (verbosity(1)) { cout << Form("INFO: will apply the selection |DeltaM/M| < %.1f%%", m_maxDeltaMOverM * 100) << endl; }

  for (auto pdf : m_candidates) { pdf.scanVar()->setConstant(0); }

  return true;
}


bool MassBiasSelector::performFits(Parameterization &pdf)
{
  RooArgSet *initialState = (RooArgSet *)pdf.parameters().snapshot();

  if (verbosity(1)) cout << Form("INFO: performing fits for PDF '%s' in %d steps from %g to %g %s",
                                   pdf.GetName().Data(), nSteps(),  m_scanVar->getMin(), m_scanVar->getMax(), m_scanVar->getUnit()) << endl;

  TCanvas *c1 = (m_perPointPlots ? new TCanvas("c1", "", 800, 600) : 0);

  for (unsigned int i = 0; i < nSteps(); i++) {
    double val = m_scanVar->getMin() + i * m_scanStep;

    if (!mkdir(pdf.GetName(), pointDir(i))) {
      if (verbosity(2)) { cout << Form("INFO: skipping already processed point %d (val = %g)", i, val) << endl; }

      continue;
    }

    if (verbosity(2)) cout << "INFO: performing S+B fit for PDF '" << pdf.GetName() << "' at point " << i << ", "
                             << m_scanVar->GetName() << " = " << val << endl;

    pdf.parameters() = *initialState;
    pdf.scanVar()->setVal(val);

    if (verbosity(2)) { cout << Form("INFO: generating %g signal events", m_nSignalRef.value(val)) << endl; }

    RooDataHist *data = pdf.sigPdf()->generateBinned(*pdf.obsVar(), m_nSignalRef.value(val), true); // true: "expected data" -> Asimov
    data->add(*m_data.data());

    if (!pdf.fit(*data, m_fitOptionsF, m_fitOptionsT, path(pdf.GetName(), pointDir(i)), c1)) { return false; }

    delete data;
  }

  delete initialState;

  if (c1) { delete c1; }

  return true;
}


bool MassBiasSelector::makeResults(std::vector<Result *> &results)
{
  // Compute bias over specified ranges and apply selection
  TString outputFile = path("mass_bias.root");

  if (verbosity(2)) { cout << "INFO: creating mass bias selection results" << endl; }

  TFile *f = TFile::Open(outputFile, "RECREATE");
  TCanvas *c1 = new TCanvas("c1", "", 800, 600);
  std::vector<TGraphErrors *> all_nSignal, all_deltaM, all_deltaMOverM_sumW2Errors, all_deltaMOverM, all_deltaMOverM_sumW2Errors_winMax;
  TLegend *l = new TLegend(0.6, 0.9, 0.9, 0.75);

  for (auto pdf : m_candidates) {
    if (verbosity(2)) { cout << Form("INFO: processing mass bias results for PDF '%s'", pdf.GetName().Data()) << endl; }

    if (!performFits(pdf)) { return false; }

    TGraphErrors *nSignal = fitValues("nSignal", pdf, false, pdf.GetName(), "Fitted N_{Signal}");
    TGraphErrors *mass = fitValues(m_scanVar->GetName(), pdf, false, pdf.GetName() + "_" + m_scanVar->GetName(), m_scanVar->GetTitle());
    TGraphErrors *mass_sumW2Errors = fitValues(m_scanVar->GetName(), pdf, true, pdf.GetName() + "_" + m_scanVar->GetName(), m_scanVar->GetTitle());

    if (!nSignal || !mass || !mass_sumW2Errors) {
      cout << Form("ERROR : invalid fit results for function '%s', please delete the output and re-run the computation", pdf.GetName().Data()) << endl;
      return false;
    }

    TGraph *refMass = new TGraph(nSteps());

    for (unsigned int i = 0; i < nSteps(); i++) { refMass->SetPoint(i, mass->GetX()[i], mass->GetX()[i]); }

    TGraphErrors *deltaMOverM = ratio(*mass, *refMass, pdf.GetName() + "_deltaM", "#Delta M / M", true);
    TGraphErrors *deltaMOverM_sumW2Errors = ratio(*mass_sumW2Errors, *refMass, pdf.GetName() + "_deltaMOverM_sumW2Errors", "#Delta M / M", true);
    TGraphErrors *deltaMOverM_sumW2Errors_winMax = winMax(*deltaMOverM_sumW2Errors);

    f->cd();
    nSignal->Write();
    mass->Write();
    mass_sumW2Errors->Write();
    deltaMOverM->Write();
    deltaMOverM_sumW2Errors->Write();
    deltaMOverM_sumW2Errors_winMax->Write();
    drawGraph(*nSignal,                         "3A", 1, 4, 0);
    c1->Print(path(pdf.GetName() + "_nSignal.pdf"));
    drawGraph(*nSignal,                        "LAX", 1, 4, 0);
    c1->Print(path(pdf.GetName() + "_nSignal_noErr.pdf"));
    drawGraph(*deltaMOverM_sumW2Errors,         "3A", 1, 4, -m_maxDeltaMOverM, m_maxDeltaMOverM);
    c1->Print(path(pdf.GetName() + "_DMoverM.pdf"));
    drawGraph(*deltaMOverM_sumW2Errors,        "LAX", 1, 4, -m_maxDeltaMOverM, m_maxDeltaMOverM);
    c1->Print(path(pdf.GetName() + "_DMoverM_noErr.pdf"));
    drawGraph(*deltaMOverM_sumW2Errors_winMax,  "3A", 1, 4, -m_maxDeltaMOverM, m_maxDeltaMOverM);
    c1->Print(path(pdf.GetName() + "_DMoverM_winMax.pdf"));
    drawGraph(*deltaMOverM_sumW2Errors_winMax, "LAX", 1, 4, -m_maxDeltaMOverM, m_maxDeltaMOverM);
    c1->Print(path(pdf.GetName() + "_DMoverM_winMax_noErr.pdf"));
    all_nSignal.push_back(nSignal);
    all_deltaMOverM.push_back(deltaMOverM);
    all_deltaMOverM_sumW2Errors.push_back(deltaMOverM_sumW2Errors);
    all_deltaMOverM_sumW2Errors_winMax.push_back(deltaMOverM_sumW2Errors_winMax);
    l->AddEntry(nSignal, pdf.GetName(), "L");
    Result *result = new MassBiasResult(pdf.GetName());
    double maxVal = max_val(*deltaMOverM_sumW2Errors_winMax);
    result->add("max(dM/M) [%]", maxVal * 100);

    if (verbosity(2)) { cout << Form("INFO: appling max(deltaM/M) > %g selection on %g ==> %s", m_maxDeltaMOverM, maxVal, maxVal > m_maxDeltaMOverM ? "FAIL" : "PASS") << endl; }

    result->setPassing(maxVal < m_maxDeltaMOverM);
    results.push_back(result);
  }

  drawGraphs(all_nSignal, "LX", 0);
  c1->Print(path("all_nSignal_noErr.pdf"));
  drawGraphs(all_nSignal, "3", 0);
  c1->Print(path("all_nSignal.pdf"));
  l->Draw();
  c1->Print(path("all_nSignal_legend.pdf"));
  drawGraphs(all_deltaMOverM_sumW2Errors, "LX", -m_maxDeltaMOverM, m_maxDeltaMOverM);
  c1->Print(path("all_deltaM_sumW2Errors_noErr.pdf"));
  drawGraphs(all_deltaMOverM_sumW2Errors,  "3", -m_maxDeltaMOverM, m_maxDeltaMOverM);
  c1->Print(path("all_deltaM_sumW2Errors.pdf"));
  l->Draw();
  c1->Print(path("all_deltaM_sumW2Errors_legend.pdf"));
  delete c1;
  delete l;
  delete f;
  return true;
}


bool MassBiasResult::operator<(const Result &other) const // "less" means "worse result"
{
  if (isPassing() != other.isPassing()) { return !isPassing(); }

  return fabs(value("max(deltaM/M) [%]")) > fabs(other.value("max(deltaM/M) [%]"));

}

/////////////////////////////////////////////
//
// Tools for building templates
//
/////////////////////////////////////////////

bool BkgTool::makeHistogram(const std::vector<TString> &inputFiles, const TString &treeName, const TString &obsName,
                            const TString &weightVarName, const TString &outputFile, const TString &outputHistName,
                            unsigned int histBins, double histMin, double histMax, bool convertToGeV)
{
  if (!gSystem->AccessPathName(outputFile)) {
    cout << "ERROR: output file " << outputFile << " already exists, will not overwrite." << endl;
    return false;
  }

  TChain chain(treeName);

  for (std::vector<TString>::const_iterator file = inputFiles.begin(); file != inputFiles.end(); file++) {
    unsigned int n = chain.Add(*file);

    if (n == 0)
    { cout << "WARNING: could not add file " << *file << endl; }
    else
    { cout << "INFO: added " << n << " file(s) from pattern " << *file << endl; }
  }

  chain.LoadTree(0); // needed for branch operations for some reason
  chain.SetBranchStatus("*", false);

  Float_t obs = 0, weight = 1; // assume float not double...

  if (chain.SetBranchAddress(obsName, &obs) != 0) {
    cout << "ERROR: could not connect observable (type: Float_t) to branch " << obsName << endl;
    cout << chain.SetBranchAddress(obsName, &obs) << endl;
    return false;
  }

  if (weightVarName != "" && chain.SetBranchAddress(weightVarName, &weight) != 0) {
    cout << "ERROR: could not connect weight variable (type: Float_t) to branch " << weightVarName << endl;
    return false;
  }

  TFile *output = TFile::Open(outputFile, "RECREATE");
  TH1D *hist = new TH1D(outputHistName, "", histBins, histMin, histMax);
  long long n = chain.GetEntries();

  for (unsigned int i = 0; i < n; i++) {
    if (i % 100000 == 0) { cout << "INFO: processing entry " << i << " of " << n << endl; }

    chain.GetEntry(i);
    hist->Fill(convertToGeV ? obs / 1000 : obs, weight);
  }

  output->cd();
  hist->Write();
  delete output;
  return true;
}

bool BkgTool::makeTree(const std::vector<TString> &inputFiles, const TString &inputTreeName, const TString &inputObsName,
                       const TString &outputFile, const TString &outputTreeName, const TString &outputObsName,
                       const TString &cutFlag, double obsMin, double obsMax, bool convertToGeV, const TString &catName, int cat)
{
  if (!gSystem->AccessPathName(outputFile)) {
    cout << "ERROR: output file " << outputFile << " already exists, will not overwrite." << endl;
    return false;
  }

  TChain chain(inputTreeName);

  for (std::vector<TString>::const_iterator file = inputFiles.begin(); file != inputFiles.end(); file++) {
    unsigned int n = chain.Add(*file);

    if (n == 0)
    { cout << "WARNING: could not add file " << *file << endl; }
    else
    { cout << "INFO: added " << n << " file(s) from pattern " << *file << endl; }
  }

  chain.LoadTree(0); // needed for branch operations for some reason

  Float_t obsIn = 0;
  Double_t obsOut = 0;

  if (chain.SetBranchAddress(inputObsName, &obsIn) != 0) {
    cout << "ERROR: could not connect observable (type: Float_t) to branch " << inputObsName << endl;
    cout << chain.SetBranchAddress(inputObsName, &obsIn) << endl;
    return false;
  }

  Char_t flags[99];
  std::vector<TString> flagBranches;

  if (cutFlag != "") {
    TObjArray *flagNames = cutFlag.Tokenize(",");

    for (int k = 0; k < flagNames->GetEntries(); k++) {
      flagBranches.push_back(flagNames->At(k)->GetName());

      if (chain.SetBranchAddress(flagBranches[k], &flags[k]) != 0) {
        cout << "ERROR: could not connect cut variable (type: Char_t) to branch " << flagBranches[k] << endl;
        return false;
      }

      cout << "Using flag '" << flagBranches[k] << "'." << endl;
    }

    delete flagNames;
  }

  chain.SetBranchStatus("*", false);
  chain.SetBranchStatus(inputObsName, true);

  for (unsigned int k = 0; k < flagBranches.size(); k++) { chain.SetBranchStatus(flagBranches[k], true); }

  TFile *output = TFile::Open(outputFile, "RECREATE");
  TTree *tree = new TTree(outputTreeName, "");
  tree->Branch(outputObsName, &obsOut);

  if (catName != "" && cat >= 0) { tree->Branch(catName, &cat); }

  unsigned int nPassed = 0;

  long long n = chain.GetEntries();

  for (unsigned int i = 0; i < n; i++) {
    if (i % 100000 == 0) { cout << "INFO: processing entry " << i << " of " << n << " (passed so far : " << nPassed << ")" << endl; }

    chain.GetEntry(i);
    bool pass = true;

    for (unsigned int k = 0; k < flagBranches.size(); k++) if (!flags[k]) { pass = false; break; }

    if (!pass) { continue; }

    if (convertToGeV) { obsIn /= 1000; }

    if (obsMin > -DBL_MAX / 2 && obsIn < obsMin) { continue; }

    if (obsMax > -DBL_MAX / 2 && obsIn > obsMax) { continue; }

    obsOut = obsIn;
    tree->Fill();
    nPassed++;
  }

  output->cd();
  tree->Write();
  delete output;
  return true;
}
