#include "VariablePlotter.h"
#include "JSONDoc.h"
#include "Controller.h"
#include "ROOTHelper.h"
#include "TSystemFile.h"
#include "TList.h"
#include "TCollection.h"
#include "TROOT.h"
#include <memory>
#include <chrono>

DECLARE_ALGORITHM(VariablePlotter, VariablePlotter)

void ReplaceAll(std::string& str, const std::string& from, const std::string& to)
{

    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
}

bool hasEnding(std::string const& fullString, std::string const& ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    }
    else {
        return false;
    }
}



void VariablePlotter::execute()
{

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    // Fetch the JSON
    mytest::JSONData& document = Controller::GetDocument();
    std::string outdir = document.outdir;

    bool dumpNtuple = false; //ntuple dumper is off by default, add a line in the JSON file if you want to activate it

    // First extract selections from the JSON
    // Full parsing is handled by the serializer.h
    std::vector<std::string> cutFlows = {};
    for (auto is : document.selections.selMap) {
        std::cout << " new selection being added to the list " << is.first << std::endl;
        cutFlows.push_back(is.first);
    }

    // Extract the extra weights you want to use sample reweighting                                                                              
    std::vector<std::string> reweight_name = { "" };
    for (auto is : document.reweight.reweightMap) {
        std::cout << " Extra weight being added to the list " << is.first << std::endl;
        reweight_name.push_back(is.first);
    }


    // Now extract the desired MC campaigns to run on
    std::vector<std::string> mcCampaigns = {};
    for (auto is : document.luminosity.lumiMap)
    {
        std::cout << " MC campaigns being added to the list " << is.first << std::endl;
        mcCampaigns.push_back(is.first);
    }

    // For logging purposes, used later
    std::string logging;

    //std::map<std::string, TH1F*, std::less<std::string> > sumhistoMap;

    float theXStimesBR = 1.0;
    // Add a map for the resonant signal x-section * BR
    std::map<std::string, float> XStimesBR; // in fb, and normalised to 1 fb^-1
    XStimesBR["X251toHH"] = 0.960;
    XStimesBR["X260toHH"] = 0.960;
    XStimesBR["X280toHH"] = 1.039;
    XStimesBR["X300toHH"] = 0.964;
    XStimesBR["X325toHH"] = 0.718;
    XStimesBR["X350toHH"] = 0.553;
    XStimesBR["X400toHH"] = 0.411;
    XStimesBR["X450toHH"] = 0.228;
    XStimesBR["X500toHH"] = 0.182;
    XStimesBR["X550toHH"] = 0.156;
    XStimesBR["X600toHH"] = 0.104;
    XStimesBR["X700toHH"] = 0.068;
    XStimesBR["X800toHH"] = 0.052;
    XStimesBR["X900toHH"] = 0.041;
    XStimesBR["X1000toHH"] = 0.033;
    XStimesBR["X2000toHH"] = 0.009;
    XStimesBR["X3000toHH"] = 0.008;

    //variables you want to save in the flat ntuple - this could be generalised and read in from the json
    /*
    std::vector< std::string > treeList = { "SF", "weight", "total_weight", "m_yy", "EventNumber", "HGamEventInfoAuxDyn.yybb_Res_BDT_BCal_yy_Score", "HGamEventInfoAuxDyn.yybb_Res_BDT_BCal_ttH_Score",
                                          "HGamEventInfoAuxDyn.yybb_BCal_m_yyjj_tilde",
                                          "HGamEventInfoAuxDyn.yybb_btag77_cutFlow",
                                          "HGamEventInfoAuxDyn.cutFlow"};
    */
    std::vector< std::string > treeList = { "SF", "weight", "total_weight", "m_yy", "EventNumber" };
    std::string truthMatch = "";

    int MC_counter = 0;

    TFile* outfile;
    std::vector< std::string > outName;
    std::vector< std::string > outNameMerge;
    ROOT::RDF::RSnapshotOptions opts;
    //opts.fLazy = true;
    opts.fMode = "RECREATE";
    using SnapRet_t = ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager>>;

    DIR* dir = opendir(outdir.c_str());
    if (!dir) const int dir_err = mkdir(outdir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    for (auto iSample : document.samples.samples) {
        mytest::aSample thisSample = document.samples.samples[iSample.first];
        std::string sampleName = iSample.first;
        const std::string basic_sampleName = iSample.first;

        for (auto iWeight = reweight_name.begin(); iWeight != reweight_name.end(); ++iWeight) { //Remaking this into\                                   

            std::string weight_name = *iWeight;
            if (weight_name.empty() and reweight_name.size() > 1) { sampleName = basic_sampleName + "_unweighted"; }
            if (!weight_name.empty()) { sampleName = basic_sampleName + "_" + weight_name; } //Add reweighting to sample name                         

            std::cout << " \n " << sampleName << " \n " << std::endl;


            std::map<std::string, TH1F*, std::less<std::string> > sumhistoMap;

            for (auto iCut : cutFlows) {
                for (auto ikk : document.variables.varMap) {
                    std::string variableName = ikk.first;
                    std::string variableValue = (ikk.second).first;
                    int nbins = (ikk.second).second.nBins;
                    double lowerBin = (ikk.second).second.lowerBin;
                    double upperBin = (ikk.second).second.upperBin;
                    std::string his = "sumHisto_" + variableName + "_" + iCut;
                    TH1F* old_histo = (TH1F*)gROOT->FindObject(his.c_str()); //Fixing memory leak 
                    if (old_histo) delete old_histo; //Fixing memory leak
                    TH1F* sumHisto = new TH1F(his.c_str(), his.c_str(), nbins, lowerBin, upperBin);
                    sumhistoMap[his] = sumHisto;
                }
            }

            for (auto iMC : mcCampaigns) {
                const std::string mc = iMC;
                MC_counter++;
                std::cout << iMC << std::endl;
                for (auto iCut : cutFlows) {
                    outfile = new TFile((outdir + "/" + sampleName + "_" + iCut + ".root").c_str(), "RECREATE");
                    std::cout << iCut << std::endl;
                    if (sampleName == "yybb") truthMatch = " && HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.HadronConeExclTruthLabelID[0]==5 && HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.HadronConeExclTruthLabelID[1]==5";
                    if (sampleName == "yyrr") truthMatch = " && (HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.HadronConeExclTruthLabelID[0]!=5 || HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.HadronConeExclTruthLabelID[1]!=5)";

                    logging = sampleName + "_" + mc + "_" + iCut;

                    // Pick up the selections for each of the different categories
                    std::string select = document.selections.selMap[iCut];
                    //std::cout<<"ZIHANG select= "<<select<<std::endl;
                    if (select.find("HGamEventInfoAuxDyn.isPassed") != std::string::npos) {
                        select = select.std::string::replace(select.find("HGamEventInfoAuxDyn.isPassed"), std::string("HGamEventInfoAuxDyn.isPassed").length(), "HGamEventInfoAuxDyn.isPassed" + truthMatch);
                    }
                    if (sampleName == "data") select = document.selections.dataSel;

                    // Specify weight
                    std::string weight = document.selections.weight;
                    // File name
                    std::string fileName = thisSample.sampleMap[iMC];
                    TString file_name = TString(fileName);
                    // new Pythia8 signal samples no longer has btag77 in the category variable name
                    // use all events instead of test events
                    // use yybb_btag77_cutFlow instead of yybb_btag77_BCal_cutFlow and use >=6 instead of == 6
                    if ((file_name.Contains("PowhegPy8_HHbbyy_cHHH01d0.MxAODDetailedNoSkim") &&
                        file_name.EndsWith("h026.root")) ||
                        (file_name.Contains("MGPy8_hh_bbyy_vbf_l1cvv1cv1.MxAODDetailedNoSkim") &&
                            file_name.EndsWith("h026.root"))) {
                        select = (std::string) TString(select).ReplaceAll("HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat",
                            "HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_Cat");
                        select = (std::string) TString(select).ReplaceAll("yybb_btag77_BCal_cutFlow == 6", "yybb_btag77_cutFlow >= 6");
                        select = (std::string) TString(select).ReplaceAll("yybb_btag77_BCal_cutFlow", "yybb_btag77_cutFlow");
                        select = (std::string) TString(select).ReplaceAll("&& EventInfoAux.eventNumber % 4 == 3", "");
                        weight = (std::string) TString(weight).ReplaceAll("*4", "");
                    }
                    // newly added resonant mass points use yybb_btag77_cutFlow instead of yybb_btag77_BCal_cutFlow
                    // and use >=6 instead of == 6
                    if (file_name.Contains("tohh_bbyy_AF2.MxAODDetailedNoSkim") &&
                        (file_name.Contains("MGH7_X270tohh") || file_name.Contains("MGH7_X290tohh") || file_name.Contains("MGH7_X3125tohh") ||
                            file_name.Contains("MGH7_X3375tohh") || file_name.Contains("MGH7_X375tohh") || file_name.Contains("MGH7_X425tohh") ||
                            file_name.Contains("MGH7_X475tohh"))) {
                        select = (std::string) TString(select).ReplaceAll("yybb_btag77_BCal_cutFlow == 6", "yybb_btag77_cutFlow >= 6");
                        select = (std::string) TString(select).ReplaceAll("yybb_btag77_BCal_cutFlow", "yybb_btag77_cutFlow");
                    }
                    // do not use isPassed for resonant signal due to a bug in the MxAOD
                    // https://its.cern.ch/jira/browse/HGAMSW-893
                    if (file_name.Contains("tohh_bbyy_AF2.MxAODDetailedNoSkim")) {
                        select = (std::string) TString(select).ReplaceAll("HGamEventInfoAuxDyn.isPassed == 1", "HGamEventInfoAuxDyn.cutFlow == 15");
                    }
                    if (file_name.Contains("mc16a")) {
                        select = "HGamEventInfoAuxDyn.passCrackVetoCleaning && " + select;
                    }

                    if (!weight_name.empty()) { weight += "*" + document.reweight.reweightMap[weight_name]; } //Add weight for reweighting                    
                    std::string weighted_selection = "(" + select + ")*(" + weight + ")";

                    // Pick up the luminosity
                    double lumi = document.luminosity.lumiMap[iMC];

                    //Decide if you want to dump the ntuple
                    dumpNtuple = document.dumper.dumperMap[iMC];

                    // Data and MC directories from JSON
                    std::string dataDir = document.directories.dirMap[iMC];
                    if (sampleName == "data") dataDir = document.directories.dataDir;



                    // This part is a place holder until we have all MC. We are duplicating mc16d with the luminosity of mc16d and mc16e!!
                    // Only used for h024, but kept in for backward compatibility
                    if (fileName == "mc16d.PowhegPy8_NNPDF30_VBFH125.MxAODDetailed.e6636_s3126_r10201_p3665.h024.root")
                        dataDir = "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16d/Nominal/";
                    if (fileName == "mc16d.aMCnloHwpp_hh_yybb_AF2.MxAODDetailed.e4419_a875_r10201_p3629.h024.root")
                        dataDir = "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16d/Nominal/";
                    if (fileName == "mc16d.MGPy8_ttgamma_nonallhadronic_AF2.MxAODDetailed.e6155_a875_r10201_p3703.h024.root")
                        dataDir = "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16d/Nominal/";
                    if (fileName == "mc16a_hh_yybb_NLO.root" || fileName == "mc16d_hh_yybb_NLO.root" || fileName == "mc16e_hh_yybb_NLO.root")
                        dataDir = "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/yybb/skimmed_samples/";
                    if (fileName == "15_16_data.root" || fileName == "17_data.root" || fileName == "18_data.root")
                        dataDir = "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-hdbs/diHiggs/yybb/skimmed_samples/";
                    ////////////////

                    std::string histoName = thisSample.histoName;
                    double sum1 = 0, sum2 = 0, sum3 = 0;
                    TList* files = ROOTHelper::GetListOfFiles((dataDir + fileName).c_str());
                    if (files) {
                        TSystemFile* file_obj;
                        TString fname;
                        TIter next_file(files);
                        // get weights 
                        while ((file_obj = (TSystemFile*)next_file())) {
                            fname.Form("%s/%s", file_obj->GetTitle(), file_obj->GetName());
                            if (!fname.EndsWith(".root"))
                                continue;
                            std::cout << "Processing file: " << fname << std::endl;
                            TFile* file = new TFile(fname.Data());
                            TH1* histo = dynamic_cast<TH1*>(file->Get(histoName.c_str()));
                            if (histo) {
                                sum1 += histo->GetBinContent(1);//“N_{xAOD}
                                //sum1=histo->GetBinContent(4);//No_Duplicate -- this is a temporary solution to avoid usage 
                                //of duplicate events which are present both in h024 and h025
                                sum2 += histo->GetBinContent(2);//N_{DxAOD}
                                sum3 += histo->GetBinContent(3);//AllEvents
                            }
                            file->Close();
                            delete file;
                        }
                        double sum_weights = (sum1 / sum2) * sum3;

                        next_file = TIter(files);
                        int count = 0;
                        // iterate over sub-files
                        while ((file_obj = (TSystemFile*)next_file())) {
                            fname.Form("%s/%s", file_obj->GetTitle(), file_obj->GetName());
                            if (!fname.EndsWith(".root"))
                                continue;
                            count++;
                            TFile* file = new TFile(fname.Data());
                            TTree* tree = (TTree*)file->Get("CollectionTree");
                            for (auto iVar : document.variables.varMap) {
                                std::string var = iVar.second.first;
                                std::string varName = iVar.first;
                                std::string hName = varName + "_" + logging + "_" + std::to_string(count);
                                std::string hN = "sumHisto_" + varName + "_" + iCut;
                                int nbins = (iVar.second).second.nBins;
                                double lowerBin = (iVar.second).second.lowerBin;
                                double upperBin = (iVar.second).second.upperBin;
                                std::shared_ptr<TH1F> his = std::make_shared<TH1F>(hName.c_str(), hName.c_str(), nbins, lowerBin, upperBin);

                                if (var.find("*FJvt") != std::string::npos) {
                                    var = var.std::string::replace(var.find("*FJvt"), var.find("*FJvt") + std::string("*FJvt").length(), "*HGamEventInfoAuxDyn.weightFJvt"); // / Avoiding error when plotting weight variable using using the alias *FJvt. This alias is used to prevent clash between weight and weightFJvt when producing the trees, bug in ROOT version <= 6.20 when using variables names that are substrings with a dot in them                                
                                }
                                std::string vvar = var + " >> " + hName;
                                std::cout << "Drawing with selection: " << select.c_str() << std::endl;
                                tree->Draw(vvar.c_str(), weighted_selection.c_str(), "HIST");
                                his->Scale(lumi * theXStimesBR / sum_weights);
                                sumhistoMap[hN]->Add(his.get());
                            }
                            delete tree;
                            file->Close();
                            delete file;
                        }

                        if (dumpNtuple) {
                            next_file = TIter(files);
                            count = 0;
                            // iterate over sub-files
                            while ((file_obj = (TSystemFile*)next_file())) {
                                fname.Form("%s/%s", file_obj->GetTitle(), file_obj->GetName());
                                if (!fname.EndsWith(".root"))
                                    continue;
                                count++;
                                TFile* file = new TFile(fname.Data());
                                TTree* tree = (TTree*)file->Get("CollectionTree");
                                ROOT::RDataFrame df(*tree);

                                std::string select_clean;
                                select_clean = select;

                                ReplaceAll(select_clean, "@", "");
                                ReplaceAll(select_clean, "$", "");

                                auto df_filter = df.Filter(select_clean);
                                df.Alias("FJvt", "HGamEventInfoAuxDyn.weightFJvt"); // Using alias to prevent clash between weight and weightFJvt, bug in ROOT version <= 6.20 when using variables names that are substrings with a dot in them
                                df_filter = df_filter.Alias("EventNumber", "EventInfoAux.eventNumber");
                                double df_SF = lumi * theXStimesBR / sum_weights;
                                auto df_out = df_filter.Define("SF", std::to_string(df_SF));

                                ROOT::RDF::RNode df_with_defines(df_out);
                                for (auto iVar : document.variables.varMap) {
                                    std::string var = iVar.second.first;
                                    std::string varName = iVar.first;
                                    for (auto j : treeList) {
                                        if (varName == j) {
                                            if (j == "weight" and !weight_name.empty()) {
                                                var = var + "*" + document.reweight.reweightMap[weight_name];
                                            }

                                            df_with_defines = df_with_defines.Define(varName, var);
                                            if (j == "weight") {
                                                auto df_total_weight = std::to_string(df_SF) + "*" + var;
                                                df_with_defines = df_with_defines.Define("total_weight", df_total_weight);
                                            }
                                        }
                                    }
                                }


                                df_with_defines.Snapshot(tree->GetName(), outdir + "/" + sampleName + "_" + mc + "_" + iCut + "_tree_" + std::to_string(count) + ".root", treeList, opts);

                                if (MC_counter == 1) {
                                    outName.push_back(sampleName + "_" + iCut + "_tree.root");
                                    outNameMerge.push_back(sampleName + "_*_" + iCut + "_tree_*.root");
                                }
                            }
                        }
                    }

                    outfile->cd();
                    for (auto iy : sumhistoMap) {
                        if (hasEnding(iy.first, iCut))
                            iy.second->Write();
                    }
                    if (MC_counter == 1) outfile->ls();
                    outfile->Close();

                }//CutFlows
            } // MCCampaigns
            MC_counter = 0;
        }//extra-weight              
    }//samples                                                                                                           


    if (dumpNtuple) {
        for (int i = 0; i < outName.size(); i++) {
            std::string hadd = "hadd -f " + outdir + "/" + outName.at(i) + " " + outdir + "/" + outNameMerge.at(i);
            system(hadd.c_str());
        }
        //system(("rm " + outdir + "/*mc16*_tree_*.root").c_str());
    }

    outName.clear();
    outNameMerge.clear();
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Execution time difference = " << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << "[s]" << std::endl;
    std::cout << std::endl << std::endl << " VariablePlotter::execute() done !!!! " << std::endl << std::endl;
}
