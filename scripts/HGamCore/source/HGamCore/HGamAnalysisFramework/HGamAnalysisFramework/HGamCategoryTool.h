#pragma once

// Local include(s):
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "HGamAnalysisFramework/PhotonHandler.h"
#include "HGamAnalysisFramework/HGamCategoryToolICHEPGlobal.h"

#include <memory>

namespace TMVA {
  class Reader;
}

// Forward declarations
namespace HG { class EventHandler; }

// Taken from xgboost/c_api.h, mut be identical!
typedef void *BoosterHandle;

namespace HG {

  class HGamCategoryTool : public asg::AsgMessaging {
  private:
    std::unique_ptr<Coupling2020::CategorizationGlobalICHEP> m_cat_tool_global_ICHEP;
    std::unique_ptr<Coupling2020::CategorizationHybridICHEP> m_cat_tool_hybrid_ICHEP;
    bool m_applyFJvtSF;

  protected:

    HG::EventHandler *m_eventHandler; //!

    TMVA::Reader              *m_readerVBF_low; //!
    TMVA::Reader              *m_readerVBF_high; //!
    TMVA::Reader              *m_readerVH_had_Moriond2017; //!
    TMVA::Reader              *m_reader_ttHhad; //!
    TMVA::Reader              *m_reader_ttHlep; //!

    BoosterHandle             *m_xgboost_ttHhad; //!
    BoosterHandle             *m_xgboost_ttHlep; //!
    BoosterHandle             *m_xgboost_topreco; //!
    BoosterHandle             *m_xgboost_ttHCPhad; //!
    BoosterHandle             *m_xgboost_ttHCPlep; //!
    BoosterHandle             *m_xgboost_htophad_Moriond2020; //!
    BoosterHandle             *m_xgboost_htoplep_Moriond2020; //!
    BoosterHandle             *m_xgboost_ttHhad_Moriond2020; //!
    BoosterHandle             *m_xgboost_ttHlep_Moriond2020; //!
    BoosterHandle             *m_xgboost_tHhad_Moriond2020; //!
    BoosterHandle             *m_xgboost_tHlep_Moriond2020; //!
    BoosterHandle             *m_xgboost_VHMET_Moriond2020; //!
    BoosterHandle             *m_xgboost_VHlep_Moriond2020; //!
    BoosterHandle             *m_xgboost_ttH_ICHEP2020; //!
    BoosterHandle             *m_xgboost_tHjb_ICHEP2020; //!
    BoosterHandle             *m_xgboost_tWH_ICHEP2020; //!
    BoosterHandle             *m_xgboost_WH_ICHEP2020; //!
    BoosterHandle             *m_xgboost_ZH_ICHEP2020; //!
    BoosterHandle             *m_xgboost_MonoH_2var; //!

    float t_pTt_yyGeV;
    float t_m_jjGeV;
    float t_pTt_yy;
    float t_m_jj;
    float t_dEta_jj;
    float t_dPhi_yy_jj;
    float t_Zepp;
    float t_Drmin_y_j;
    float t_Drmin_y_j2;
    float t_Dy_yy_jj;
    float t_cosTS_yy_jj;
    float t_eta_y1;
    float t_eta_y2;
    float t_m_alljet_30;
    float t_HT_30;
    float t_METsig;
    float t_N_j30;
    float t_N_j_central30;
    float t_N_bjet30;
    float t_pTlepMET;
    float t_massT;

    double m_weight;
    double m_score;

  protected:

    virtual HG::EventHandler *eventHandler() { return m_eventHandler; }

    double getLeptonSFs(const xAOD::ElectronContainer *electrons,
                        const xAOD::MuonContainer     *muons);

    int  Passes_VBF_Moriond2017(const xAOD::JetContainer *jets);
    int  Passes_VBF_Moriond2020(const xAOD::JetContainer *jets);

    bool Passes_VH_hadronic_Moriond2017(const xAOD::JetContainer *jetsJVT);
    bool Passes_VH_hadronic_Moriond2020(const xAOD::JetContainer *jetsJVT);

    int Passes_VH_MET_Moriond2017(const xAOD::PhotonContainer    *photons,
                                  const xAOD::MissingETContainer *met);
    int Passes_VH_MET_Moriond2020(const xAOD::PhotonContainer    *photons,
                                  const xAOD::ElectronContainer  *electrons,
                                  const xAOD::MuonContainer      *muons,
                                  const xAOD::MissingETContainer *met);

    int  Passes_VH_leptonic_Moriond2017(const xAOD::PhotonContainer    *photons,
                                        const xAOD::ElectronContainer  *electrons,
                                        const xAOD::MuonContainer      *muons,
                                        const xAOD::MissingETContainer *met);
    int  Passes_VH_leptonic_Moriond2020(const xAOD::PhotonContainer    *photons,
                                        const xAOD::ElectronContainer  *electrons,
                                        const xAOD::MuonContainer      *muons,
                                        const xAOD::MissingETContainer *met);

    int  Passes_VH_dileptons_Moriond2017(const xAOD::PhotonContainer    *photons,
                                         const xAOD::ElectronContainer  *electrons,
                                         const xAOD::MuonContainer      *muons);
    int  Passes_VH_dileptons_Moriond2020(const xAOD::PhotonContainer    *photons,
                                         const xAOD::ElectronContainer  *electrons,
                                         const xAOD::MuonContainer      *muons);


    int split_ggH_STXS(const xAOD::PhotonContainer *photons, const xAOD::JetContainer *jetsJVT);
    int split_ggH_STXS_Moriond2020(const xAOD::PhotonContainer *photons, const xAOD::JetContainer *jetsJVT);

    int Passes_ttH_Moriond(const xAOD::PhotonContainer     *photons,
                           const xAOD::ElectronContainer   *electrons,
                           const xAOD::MuonContainer       *muons,
                           const xAOD::JetContainer        *jets,
                           const xAOD::JetContainer        *jetsJVT,
                           const xAOD::MissingETContainer  *met);

    int Passes_ttHBDT_Moriond(const xAOD::PhotonContainer     *photons,
                              const xAOD::ElectronContainer   *electrons,
                              const xAOD::MuonContainer       *muons,
                              const xAOD::JetContainer        *jets,
                              const xAOD::JetContainer        *jetsJVT,
                              const xAOD::MissingETContainer  *met);

    int Passes_ttHBDTlep_Moriond(const xAOD::PhotonContainer     *photons,
                                 const xAOD::ElectronContainer   *electrons,
                                 const xAOD::MuonContainer       *muons,
                                 const xAOD::JetContainer        *jets,
                                 const xAOD::JetContainer        *jetsJVT,
                                 const xAOD::MissingETContainer  *met);

    int Passes_XGBoost_ttH(const xAOD::PhotonContainer     *photons,
                           const xAOD::ElectronContainer   *electrons,
                           const xAOD::MuonContainer       *muons,
                           const xAOD::JetContainer        *jets,
                           const xAOD::JetContainer        *jetsJVT,
                           const xAOD::MissingETContainer  *met);


    int Passes_XGBoost_ttHCP(const xAOD::PhotonContainer    *photons,
                             const xAOD::ElectronContainer  *electrons,
                             const xAOD::MuonContainer      *muons,
                             const xAOD::JetContainer       *jets,
                             const xAOD::JetContainer       *jetsJVT,
                             const xAOD::MissingETContainer *met);

    int Passes_XGBoost_ttH_Moriond2020(const xAOD::PhotonContainer    *photons,
                                       const xAOD::ElectronContainer  *electrons,
                                       const xAOD::MuonContainer      *muons,
                                       const xAOD::JetContainer       *jets,
                                       const xAOD::JetContainer       *jetsJVT,
                                       const xAOD::MissingETContainer *met);

    bool Passes_qqH_BSM(const xAOD::JetContainer  *jetsJVT);
    bool Passes_qqH_BSM_2jet(const xAOD::JetContainer  *jetsJVT);
    bool Passes_qqH_BSM_2jet_Moriond2020(const xAOD::JetContainer  *jetsJVT);

    int Passes_MonoH_2var(const xAOD::PhotonContainer    *photons,
                          const xAOD::ElectronContainer  *electrons,
                          const xAOD::MuonContainer      *muons,
                          const xAOD::JetContainer       *jets,
                          const xAOD::JetContainer       *jetsJVT,
                          const xAOD::MissingETContainer *met);

    float getMVAWeight(TMVA::Reader *XReader);
    void resetReader();

    float *make_XGBoost_DMatrix_ttHhad(const xAOD::PhotonContainer    *photons,
                                       const xAOD::JetContainer       *jets,
                                       const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHlep(const xAOD::PhotonContainer    *photons,
                                       const xAOD::ElectronContainer  *electrons,
                                       const xAOD::MuonContainer      *muons,
                                       const xAOD::JetContainer       *jets,
                                       const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHCPhad(const xAOD::PhotonContainer *photons,
                                         const xAOD::JetContainer *jets,
                                         const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHCPlep(const xAOD::PhotonContainer    *photons,
                                         const xAOD::JetContainer       *jets,
                                         const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHhad_multiclass(const xAOD::PhotonContainer *photons,
                                                  const xAOD::JetContainer *jets,
                                                  const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHlep_multiclass(const xAOD::PhotonContainer    *photons,
                                                  const xAOD::JetContainer       *jets,
                                                  const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHhad_Moriond2020(const xAOD::PhotonContainer *photons,
                                                   const xAOD::JetContainer *jets,
                                                   const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttHlep_Moriond2020(const xAOD::PhotonContainer    *photons,
                                                   const xAOD::ElectronContainer  *electrons,
                                                   const xAOD::MuonContainer      *muons,
                                                   const xAOD::JetContainer       *jets,
                                                   const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_ttH_ICHEP2020(const xAOD::PhotonContainer    *photons,
                                              const xAOD::ElectronContainer  *electrons,
                                              const xAOD::MuonContainer      *muons,
                                              const xAOD::JetContainer       *jets,
                                              const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_VHMET_Moriond2020(const xAOD::PhotonContainer *photons,
                                                  const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_VHlep_Moriond2020(const xAOD::PhotonContainer    *photons,
                                                  const xAOD::ElectronContainer  *electrons,
                                                  const xAOD::MuonContainer      *muons,
                                                  const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_VH_ICHEP2020(const xAOD::PhotonContainer    *photons,
                                             const xAOD::ElectronContainer  *electrons,
                                             const xAOD::MuonContainer      *muons,
                                             const xAOD::MissingETContainer *met);
    float *make_XGBoost_DMatrix_MonoH_2var(const xAOD::PhotonContainer *photons,
                                           const xAOD::MissingETContainer *met);

    float get_XGBoost_Weight(BoosterHandle *boost, float *vars, int len);
    const float *get_XGBoost_Weight_multiclass(BoosterHandle *boost, float *vars, int len);
    //float get_pseudoCont_score(double btagscore);

    void decorateHadtopCandidate(const xAOD::JetContainer *jets, int ntop);
    void decorateSleptopCandidate(const xAOD::JetContainer       *jets,
                                  const xAOD::ElectronContainer  *electrons,
                                  const xAOD::MuonContainer      *muons,
                                  const xAOD::MissingETContainer *mets);

  public:
    HGamCategoryTool(const char *name, HG::EventHandler *eventHandler);
    virtual ~HGamCategoryTool();

    void saveHGamCategoryInfo(const xAOD::PhotonContainer    *photons   = nullptr,
                              const xAOD::ElectronContainer  *electrons = nullptr,
                              const xAOD::MuonContainer      *muons     = nullptr,
                              const xAOD::JetContainer       *jets      = nullptr,
                              const xAOD::MissingETContainer *met       = nullptr,
                              const xAOD::JetContainer       *jetsNoJvt = nullptr);

    void saveHGamTruthCategoryInfo(const xAOD::TruthParticleContainer *photons   = nullptr,
                                   const xAOD::TruthParticleContainer *electrons = nullptr,
                                   const xAOD::TruthParticleContainer *muons     = nullptr,
                                   const xAOD::JetContainer           *jets      = nullptr,
                                   const xAOD::MissingETContainer     *mets      = nullptr);

    virtual EL::StatusCode initialize(Config &config);

    // Use below for splitting ggH categories
    //enum HGamCatIndex { FailDiphoton=-1, ggH_CenLow=1, ggH_CenHigh=2, ggH_FwdLow=3, ggH_FwdHigh=4, VBFloose=5, VBFtight=6, VHhad_loose=7, VHhad_tight=8, VHMET=9, VHlep=10, VHdilep=11, ttHhad=12, ttHlep=13 };

    enum HGamM17Index { M17_FailDiphoton = -1, M17_ggH_0J_Cen = 1, M17_ggH_0J_Fwd = 2, M17_ggH_1J_LOW = 3, M17_ggH_1J_MED = 4, M17_ggH_1J_HIGH = 5, M17_ggH_1J_BSM = 6, M17_ggH_2J_LOW = 7, M17_ggH_2J_MED = 8, M17_ggH_2J_HIGH = 9, M17_ggH_2J_BSM = 10, M17_VBF_HjjLOW_loose = 11, M17_VBF_HjjLOW_tight = 12, M17_VBF_HjjHIGH_loose = 13, M17_VBF_HjjHIGH_tight = 14, M17_VHhad_loose = 15, M17_VHhad_tight = 16, M17_qqH_BSM = 17, M17_VHMET_LOW = 18, M17_VHMET_HIGH = 19, M17_VHMET_BSM = 20, M17_VHlep_LOW = 21, M17_VHlep_HIGH = 22, M17_VHdilep_LOW = 23, M17_VHdilep_HIGH = 24, M17_ttH = 25 };

    enum HGamM20Index { M20_FailDiphoton = -1, M20_ggH_0J_LOW_Cen = 1, M20_ggH_0J_LOW_Fwd = 2, M20_ggH_0J_HIGH_Cen = 3, M20_ggH_0J_HIGH_Fwd = 4, M20_ggH_1J_LOW = 5, M20_ggH_1J_MED = 6, M20_ggH_1J_HIGH = 7, M20_ggH_2J_LOW = 8, M20_ggH_2J_MED = 9, M20_ggH_2J_HIGH = 10, M20_ggH_BSM1 = 11, M20_ggH_BSM2 = 12, M20_ggH_BSM3 = 13, M20_ggH_BSM4 = 14, M20_VBF_HjjLOW_loose = 15, M20_VBF_HjjLOW_tight = 16, M20_VBF_HjjHIGH_loose = 17, M20_VBF_HjjHIGH_tight = 18, M20_VHhad_loose = 19, M20_VHhad_tight = 20, M20_qqH_BSM = 21, M20_VHMET_MED_loose = 22, M20_VHMET_MED_tight = 23, M20_VHMET_HIGH = 24, M20_VHMET_BSM = 25, M20_VHlep_LOW_loose = 26, M20_VHlep_LOW_tight = 27, M20_VHlep_MED_loose = 28, M20_VHlep_MED_tight = 29, M20_VHlep_HIGH = 30, M20_VHlep_BSM = 31, M20_VHdilep_LOW = 32, M20_VHdilep_MED = 33, M20_VHdilep_HIGH = 34, M20_ttH = 35 };

    std::pair<int, float> getCategoryAndWeightMoriond2017_ttHBDT_qqH2jet(
      const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::JetContainer       *jets,
      const xAOD::JetContainer       *jetsJVT,
      const xAOD::MissingETContainer *met);

    std::pair<int, float> getCategoryAndWeightMoriond2017_ttHBDT(
      const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::JetContainer       *jets,
      const xAOD::JetContainer       *jetsJVT,
      const xAOD::MissingETContainer *met);

    std::pair<int, float> getCategoryAndWeightMoriond2017_ttHBDTlep(
      const xAOD::PhotonContainer    *photons,
      const xAOD::ElectronContainer  *electrons,
      const xAOD::MuonContainer      *muons,
      const xAOD::JetContainer       *jets,
      const xAOD::JetContainer       *jetsJVT,
      const xAOD::MissingETContainer *met);

    std::pair<int, float> getCategoryAndWeightMoriond2017(const xAOD::PhotonContainer    *photons,
                                                          const xAOD::ElectronContainer  *electrons,
                                                          const xAOD::MuonContainer      *muons,
                                                          const xAOD::JetContainer       *jets,
                                                          const xAOD::JetContainer       *jetsJVT,
                                                          const xAOD::MissingETContainer *met);

    float *getCategoryAndWeightXGBoost_ttH(const xAOD::PhotonContainer    *photons,
                                           const xAOD::ElectronContainer  *electrons,
                                           const xAOD::MuonContainer      *muons,
                                           const xAOD::JetContainer       *jets,
                                           const xAOD::JetContainer       *jetsJVT,
                                           const xAOD::MissingETContainer *met);

    float *getCategoryAndWeightXGBoost_ttHCP(const xAOD::PhotonContainer    *photons,
                                             const xAOD::ElectronContainer  *electrons,
                                             const xAOD::MuonContainer      *muons,
                                             const xAOD::JetContainer       *jets,
                                             const xAOD::JetContainer       *jetsJVT,
                                             const xAOD::MissingETContainer *met);

    float *getCategoryAndWeightMoriond2020(const xAOD::PhotonContainer    *photons,
                                           const xAOD::ElectronContainer  *electrons,
                                           const xAOD::MuonContainer      *muons,
                                           const xAOD::JetContainer       *jets,
                                           const xAOD::JetContainer       *jetsJVT,
                                           const xAOD::MissingETContainer *met);

    float *getScoreICHEP2020(const xAOD::PhotonContainer    *photons,
                             const xAOD::ElectronContainer  *electrons,
                             const xAOD::MuonContainer      *muons,
                             const xAOD::JetContainer       *jets,
                             const xAOD::JetContainer       *jetsJVT,
                             const xAOD::MissingETContainer *met);

    float *getCategoryAndWeightMonoH_2var(const xAOD::PhotonContainer    *photons,
                                          const xAOD::ElectronContainer  *electrons,
                                          const xAOD::MuonContainer      *muons,
                                          const xAOD::JetContainer       *jets,
                                          const xAOD::JetContainer       *jetsJVT,
                                          const xAOD::MissingETContainer *met);

    int getMassCategoryRun1(const xAOD::PhotonContainer * /*photons*/);
    int getEtaMassCategory(const xAOD::PhotonContainer * /*photons*/);
    int getConvMassCategory(const xAOD::PhotonContainer * /*photons*/);
    int getPtMassCategory(const xAOD::PhotonContainer * /*photons*/);
    int getMuMassCategory(float mu);

  };

}
