/* Add your custom classes in this file */
/* Remember also to modify inc/rooCommon.h */
#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ nestedtypedef;

#pragma link C++ class utils+;
#pragma link C++ class auxUtils+;

#pragma link C++ class fitTool+;
#pragma link C++ class asimovTool+;
#pragma link C++ class limitTool+;

/* Custom classes (other than those in RooFitExtensions) */
#pragma link C++ class FlexibleInterpVarExt+;
#pragma link C++ class FlexibleInterpVarMkII+;
#pragma link C++ class HggMG5aMCNLOLineShapePdf+;
#pragma link C++ class HggBernstein+;

#pragma link C++ class std::list<RooAbsData*>::iterator;

#endif
