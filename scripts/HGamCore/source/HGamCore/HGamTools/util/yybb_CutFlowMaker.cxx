// STL include(s):
#include <iostream>
#include <sstream>

// ROOT include(s):
#include "TChain.h"
#include "TFile.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TString.h"
#include "TH1F.h"

// Local include(s):
#include "HGamAnalysisFramework/Config.h"
#include "HGamAnalysisFramework/HGamCommon.h"


// Cutflow levels
enum CutEnum {NOCUT = -1, NxAOD = 0, NDxAOD = 1, ALLEVTS = 2, DUPLICATE = 3, GRL = 4, TRIGGER = 5, DQ = 6, VERTEX = 7, TWO_LOOSE_GAM = 8, AMBIGUITY = 9,
              TRIG_MATCH = 10, GAM_TIGHTID = 11, GAM_ISOLATION = 12, RELPTCUTS = 13, MASSCUT = 14,
              CENJETS = 15, TAGGING = 16, BPT = 17, BBMASS = 18, SR = 19
             };

// Cutflow names
const std::vector<TString> m_CutLabels = {"#it{N}_{xAOD}", "#it{N}_{DxAOD}", "All events", "No duplicates", "GRL", "Pass trigger", "Detector DQ", "Has PV",
                                          "2 loose photons", "e-#gamma ambiguity", "Trigger match", "tight ID", "isolation", "rel. #it{p}_{T} cuts", "#it{m}_{#gamma#gamma} #in [105,160] GeV", "2 Cen Jets", "#it{b}-tagging", "#it{b}Jet p_{T} Cuts", "m_{#it{bb}} Cut", "#gamma#gamma SR"
                                         };

bool isData = true;

// Number of cuts
int Ncuts = m_CutLabels.size();

// BTag enumeration
enum bTagCatEnum {noTag = -1, zeroTag = 0, oneTag = 1, twoTag = 2};

//Function to return btag suffix
TString getBTagSuffix(int btagCat);
//Fetch cutflows for each category. Fill cutflows from MxAOD below MASSCUT (Slimming)
std::map<TString, TH1F *> fetchCutflowsFromMxAOD(TFile *inFile);

void PrintProgressBar(int index, int total);

int main(int argc, char *argv[])
{
  // Check that the config file location is provided.
  if (argc < 2) { HG::fatal("Please supply the config file as a config parameter. I.E runyybbCutFlowMaker yybbCutFlowMaker.config"); }

  HG::Config *settings = new HG::Config(TString(argv[1]));

  // Print configuration for benefit of user:
  std::cout << "Running cutflow maker using config parameters:" << std::endl;
  settings->printDB();

  bool mergeFiles   = settings->getBool("MergeInputFiles", false);
  bool normCutflows = settings->getBool("NormToXSec", false);
  double intLumi    = settings->getNum("Luminosity", 3209);
  bool alreadyMerged = false;
  bool lowMass   = settings->getBool("lowMass", false);

  // Prepare for loop over input MxAOD/TTree:
  std::vector<TString> fileNames = settings->getStrV("InputFile");


  for (auto fullFileName : fileNames) {
    if (alreadyMerged) { break; }

    std::map<TString, TH1F *> cutflowHistos;
    TFile *inFile = TFile::Open(fullFileName.Data());

    //----------------------------------------
    // Fetch all cutflows and fill from MxAOD
    //----------------------------------------
    cutflowHistos = fetchCutflowsFromMxAOD(inFile);

    TObjArray *splitFileName = fullFileName.Tokenize("/");
    TObjString *outputFileName = (TObjString *)splitFileName->Last();
    TString fileName = outputFileName->GetString().Data();
    fileName.ReplaceAll(".root", "");
    fileName.ReplaceAll("MxAOD-", "");

    TChain *chain = new TChain("CollectionTree");
    chain->AddFile(fullFileName.Data());

    if (mergeFiles) {
      //            cutflowHistos[fileName+getBTagSuffix(zeroTag)]->Clear();
      //            cutflowHistos[fileName+getBTagSuffix(oneTag)]->Clear();
      //            cutflowHistos[fileName+getBTagSuffix(twoTag)]->Clear();
      bool firstFile = true;

      for (auto samples : settings->getStrV("InputFile")) {
        if (firstFile) {
          firstFile = false;
          continue;
        }

        TObjArray *splitFileName = samples.Tokenize("/");
        TObjString *outputFileName = (TObjString *)splitFileName->Last();
        TString tempFileName = outputFileName->GetString().Data();
        tempFileName.ReplaceAll(".root", "");
        tempFileName.ReplaceAll("MxAOD-", "");

        chain->AddFile(samples.Data());
        TFile *tempFile = TFile::Open(samples.Data());
        std::map<TString, TH1F *> tempCutflowHistos = fetchCutflowsFromMxAOD(tempFile);
        std::cout << "Trying to fetch these mapped string: " << tempFileName + getBTagSuffix(zeroTag) << std::endl;
        cutflowHistos[fileName + getBTagSuffix(zeroTag)]->Add(tempCutflowHistos[tempFileName + getBTagSuffix(zeroTag)]);
        cutflowHistos[fileName + getBTagSuffix(oneTag)]->Add(tempCutflowHistos[tempFileName + getBTagSuffix(oneTag)]);
        cutflowHistos[fileName + getBTagSuffix(twoTag)]->Add(tempCutflowHistos[tempFileName + getBTagSuffix(twoTag)]);
      }

      alreadyMerged = true;
    }

    std::cout << "\n\n######################################################" << std::endl;
    std::cout << "Starting cutflow generation for: " << fileName << std::endl;
    std::cout << "######################################################\n\n" << std::endl;


    //IF YOU WANT TO ADD VARIABLES THEN DO THAT HERE!!
    //Create a map that allows you to track that variable for a given systematic

    bool yy_isPassed;
    int yy_cutFlow, yybb_cutFlow, yybb_bTagCat;
    float yy_weight, yybb_weight, xSecBrFe;
    double totalWeight;
    xSecBrFe = 1.0;

    TString eventInfoBranch     = "HGamEventInfoAuxDyn.";
    TString yy_isPassedBranch   = eventInfoBranch + "isPassed";
    TString yy_cutFlowBranch    = eventInfoBranch + "cutFlow";
    TString yy_WeightBranch     = eventInfoBranch + "weight";
    TString yy_WeightInitBranch = eventInfoBranch + "weightInitial";
    TString yybb_bTagCatBranch  = eventInfoBranch + "yybb_bTagCat";
    TString mass;
    TString yybb_cutFlowBranch;
    TString yybb_WeightBranch;

    if (lowMass) {
      mass = "yybb_lowMass_cutFlow";
      yybb_cutFlowBranch = eventInfoBranch + "yybb_lowMass_cutFlow";
      yybb_WeightBranch = eventInfoBranch + "yybb_lowMass_weight";
      mass = "yybb_lowMass_cutFlow";
    } else {
      mass = "yybb_highMass_cutFlow";
      yybb_cutFlowBranch = eventInfoBranch + "yybb_highMass_cutFlow";
      yybb_WeightBranch = eventInfoBranch + "yybb_highMass_weight";
      mass = "yybb_highMass_cutFlow";
    }

    TString xSecBrFeBranch      = eventInfoBranch + "crossSectionBRfilterEff";


    chain->SetBranchAddress(yy_isPassedBranch.Data(), &yy_isPassed);
    chain->SetBranchAddress(yy_cutFlowBranch.Data(), &yy_cutFlow);
    chain->SetBranchAddress(yy_WeightBranch.Data(), &yy_weight);
    chain->SetBranchAddress(yybb_bTagCatBranch.Data(), &yybb_bTagCat);
    chain->SetBranchAddress(yybb_cutFlowBranch.Data(), &yybb_cutFlow);
    chain->SetBranchAddress(yybb_WeightBranch.Data(), &yybb_weight);

    if (not isData)
    { chain->SetBranchAddress(xSecBrFeBranch.Data(), &xSecBrFe); }

    int nEvents = chain->GetEntries();

    //------------------------------------
    //Fill BTag Categories Evnt by Evnt
    //------------------------------------

    std::cout << "There are " << nEvents << " events to process." << std::endl;

    for (int index = 0; index < nEvents; index++) {
      chain->GetEntry(index);
      PrintProgressBar(index, nEvents);

      //Cut on events that Pass the entire yy selection
      if (not yy_isPassed) { continue; }

      //Fill the mass cut bin. This is useful to compare with HGam
      cutflowHistos[fileName + getBTagSuffix(zeroTag)]->Fill(MASSCUT, yy_weight);
      cutflowHistos[fileName + getBTagSuffix(oneTag)]->Fill(MASSCUT, yy_weight);
      cutflowHistos[fileName + getBTagSuffix(twoTag)]->Fill(MASSCUT, yy_weight);

      totalWeight = yybb_weight * yy_weight;

      if (yybb_cutFlow > 1) {
        cutflowHistos[fileName + getBTagSuffix(zeroTag)]->Fill(CENJETS, yy_weight);
        cutflowHistos[fileName + getBTagSuffix(oneTag)]->Fill(CENJETS, yy_weight);
        cutflowHistos[fileName + getBTagSuffix(twoTag)]->Fill(CENJETS, yy_weight);
      }

      if (yybb_cutFlow > 1 && yybb_bTagCat != noTag) { cutflowHistos[fileName + getBTagSuffix(yybb_bTagCat)]->Fill(TAGGING, totalWeight); }

      if (yybb_cutFlow > 2 && yybb_bTagCat != noTag) { cutflowHistos[fileName + getBTagSuffix(yybb_bTagCat)]->Fill(BPT, totalWeight); }

      if (yybb_cutFlow > 3 && yybb_bTagCat != noTag) { cutflowHistos[fileName + getBTagSuffix(yybb_bTagCat)]->Fill(BBMASS, totalWeight); }

      if (yybb_cutFlow > 4 && yybb_bTagCat != noTag) { cutflowHistos[fileName + getBTagSuffix(yybb_bTagCat)]->Fill(SR, totalWeight); }

    }

    //------------------------------------
    //Normalise histograms
    //------------------------------------
    if (normCutflows && not isData) {
      for (auto cutflowElement : cutflowHistos) {
        double sumW = cutflowElement.second->GetBinContent(ALLEVTS + 1);
        double normScale = intLumi * xSecBrFe / sumW;

        std::cout << "\n\nNormalising " << cutflowElement.first << " using the following paramters:" << std::endl;
        std::cout << "nEvents       : " << sumW << std::endl;
        std::cout << "intLumi (pb-1): " << intLumi << std::endl;
        std::cout << "xSecBrFe      : " << xSecBrFe << std::endl;

        for (int bin = 1; bin <= Ncuts; ++bin) {
          cutflowElement.second->SetBinContent(bin, normScale * cutflowElement.second->GetBinContent(bin));
          cutflowElement.second->SetBinError(bin, normScale * cutflowElement.second->GetBinError(bin));
        }

        std::cout << "Done! " << std::endl;
      }
    }

    //------------------------------------
    //Prepare saving results to text file
    //------------------------------------
    TString outFileName = fileName;
    outFileName += "_Cutflows";
    outFileName += ".root";

    TFile *file = new TFile(outFileName.Data(), "RECREATE");
    file->cd();

    for (auto cutflowElement : cutflowHistos)
    { cutflowElement.second->Write(); }

    file->Write();


    //------------------------------------
    //Prepare saving results to latex file
    //------------------------------------


    FILE *tex_file;
    tex_file = fopen(fileName + "_Cutflows.tex", "w");

    double bin_content = 0;
    double bin_error = 0;
    float first_bin = 0;

    fprintf(tex_file, "\\documentclass[handout, 10pt]{beamer}\n \\usepackage[UKenglish]{babel}\n\\usepackage[UKenglish]{isodate}\n\\usepackage{tabularx}\n\\usetheme{Dresden}\n\\begin{document}\n\n");

    for (int c = 0; c < 3; c++) {
      std::string cat;

      if (c == 0) { cat = "zeroTag"; }

      if (c == 1) { cat = "oneTag"; }

      if (c == 2) { cat = "twoTag"; }

      std::string thename = (std::string) fileName + "-" + cat;

      if (c == 0) { fprintf(tex_file, "\\begin{table}\\tiny\n\\begin{center} \n\\caption{Cutflow for $%s$} \n\\begin{tabular}{|c|c|c|c|} \n \\hline \nCuts& \\multicolumn{3}{c|}{0 b-tag} \\\\ \\hline \n &Yield&Error&Efficiency\\\\ \\hline \n", thename.c_str()); }

      if (c == 1) { fprintf(tex_file, "\\begin{table}\\tiny\n\\begin{center} \n\\caption{Cutflow for $%s$} \n\\begin{tabular}{|c|c|c|c|} \n \\hline \nCuts& \\multicolumn{3}{c|}{1 b-tag} \\\\ \\hline \n &Yield&Error&Efficiency\\\\ \\hline \n", thename.c_str()); }

      if (c == 2) { fprintf(tex_file, "\\begin{table}\\tiny\n\\begin{center} \n\\caption{Cutflow for $%s$} \n\\begin{tabular}{|c|c|c|c|} \n \\hline \nCuts& \\multicolumn{3}{c|}{2 b-tag} \\\\ \\hline \n &Yield&Error&Efficiency\\\\ \\hline \n", thename.c_str()); }

      for (int index_cutflow = 1; index_cutflow < (cutflowHistos[fileName + getBTagSuffix(c)]->GetNbinsX() + 1); index_cutflow ++) {
        bin_content = cutflowHistos[fileName + getBTagSuffix(c)]->GetBinContent(index_cutflow);
        bin_error = cutflowHistos[fileName + getBTagSuffix(c)]->GetBinError(index_cutflow);

        if (index_cutflow == 3) { first_bin = bin_content; }


        TString title_axis = cutflowHistos[fileName + getBTagSuffix(c)]->GetXaxis()->GetBinLabel(index_cutflow);
        title_axis.ReplaceAll("#", "\\");
        title_axis.ReplaceAll(" ", "\\ ");

        std::string title = (std::string) title_axis;
        std::string line = "-";

        if (normCutflows && not isData) {
          if (index_cutflow < 3) {
            fprintf(tex_file, "$%s$ & %4.3f&%4.3f &$%s$ \\\\ \n \\hline \n", title.c_str(), bin_content, bin_error, line.c_str());
          } else {
            fprintf(tex_file, "$%s$ & %4.3f&%4.3f &%4.1f \\\\ \n \\hline \n", title.c_str(), bin_content, bin_error, bin_content * 100 / first_bin);
          }
        } else {
          if (index_cutflow < 3) {
            fprintf(tex_file, "$%s$ & %4.0f&%4.0f &$%s$ \\\\ \n \\hline \n", title.c_str(), bin_content, bin_error, line.c_str());
          } else {
            fprintf(tex_file, "$%s$ & %4.0f&%4.0f &%4.1f \\\\ \n \\hline \n", title.c_str(), bin_content, bin_error, bin_content * 100 / first_bin);
          }
        }
      }

      fprintf(tex_file, "\\end{tabular}\n\\end{center}\n\\end{table}\n\\pagebreak\n");
    }

    fprintf(tex_file, "\\end{document}");

    fclose(tex_file);


    //------------------------------------
    //Going from latex to pdf
    //------------------------------------
    //In the directory where you have created your tex file, do:
    //latex (name_of_your_sample).tex (e.g. latex X275_Cutflows.tex ) -> A dvi file is created
    //dvipdf (name_of_your_sample).dvi (e.g. dvipdf X275_Cutflows.dvi )

    //        std::ofstream myfile;
    //        myfile.open (outFileName.Data());
    //        myfile << output.Data();
    //        myfile.close();

  }

  return 0;
}

TString getBTagSuffix(int btagCat)
{
  TString suffix = "";

  switch (btagCat) {
    case zeroTag :
      return suffix = "_zeroTag";
      break;

    case oneTag :
      return suffix = "_oneTag";
      break;

    case twoTag :
      return suffix = "_twoTag";
      break;
  }

  return suffix;
}

void PrintProgressBar(int index, int total)
{
  if (index % 100 == 0) {
    TString print_bar = " [";

    for (int bar = 0; bar < 20; bar++) {
      double current_fraction = double(bar) / 20.0;

      if (double(index) / double(total) > current_fraction) { print_bar.Append("/"); }
      else { print_bar.Append("."); }
    }

    print_bar.Append("] ");
    double percent = 100.0 * (double(index) / double(total));
    TString text = Form("%s %2.2f ", print_bar.Data(), percent);
    std::cout << text << "%\r" << std::flush;
  }
}

//Fetch cutflows for each category. Fill cutflows from MxAOD below MASSCUT (Slimming)
std::map<TString, TH1F *> fetchCutflowsFromMxAOD(TFile *inFile)
{
  TIter next(inFile->GetListOfKeys());
  TKey *key;
  std::vector<TH1F *> cutflows;
  std::map<TString, TH1F *> cutflowHistos;

  TString fullFileName = inFile->GetName();
  TObjArray *splitFileName = fullFileName.Tokenize("/");
  TObjString *outputFileName = (TObjString *)splitFileName->Last();
  TString fileName = outputFileName->GetString().Data();
  fileName.ReplaceAll(".root", "");
  fileName.ReplaceAll("MxAOD-", "");

  //If no cutflow has weighted in name then we have a data input
  std::cout << "Searching input file for cutflows..." << std::endl;
  isData = true;

  while ((key = (TKey *)next())) {
    TString objType = (TString)key->GetClassName();
    TString keyName = (TString)key->GetName();

    if (objType == "TH1F" && keyName.Contains("noDalitz_weighted")) {
      std::cout << "Found MC cutflow: " << keyName << std::endl;
      cutflows.push_back((TH1F *)inFile->Get(keyName.Data()));
      isData = false;
    }
  }

  //If we have data then we need to add all cutflows together for each run number
  if (isData) {
    Ncuts = Ncuts - 1; //we don't want to unblind
    TIter next(inFile->GetListOfKeys());

    std::cout << "No weighted cutflow found. Assuming data input for file: " << fullFileName << std::endl;

    if (cutflows.size() > 0)
    { HG::fatal("Issue in fetching cutflows. No cutflows found but cutflow vector non-zero."); }

    while ((key = (TKey *)next())) {
      TString objType = (TString)key->GetClassName();
      TString keyName = (TString)key->GetName();

      // Cutflow for data should have many period numbers
      if (objType == "TH1F" && keyName.Contains("CutFlow")) {
        std::cout << "Found data cutflow: " << keyName << std::endl;
        cutflows.push_back((TH1F *)inFile->Get(keyName.Data()));
      }
    }
  }

  TH1F *totalCutflow = cutflows[0];
  totalCutflow->Sumw2();

  //Add all cutflows together
  if (isData) {
    std::cout << "\n\nMerging cutflows together (Multiple data run numbers):" << std::endl;
    std::cout << cutflows[0]->GetName() << std::endl;

    for (unsigned int i = 1; i < cutflows.size(); i++) {
      std::cout << cutflows[i]->GetName() << std::endl;
      totalCutflow->Add(cutflows[i]);
    }

  }

  std::cout << "Setting initial cut values below MASSCUT for:" << std::endl;

  for (int cat = zeroTag; cat <= twoTag; cat++) {
    TString suffix = getBTagSuffix(cat);
    TString histName = fileName + suffix;
    std::cout << "Hist: " << histName << std::endl;

    TH1F *cutflow = new TH1F(histName.Data(), histName.Data(), Ncuts, 0, Ncuts);
    TAxis *axis = cutflow->GetXaxis();

    for (int bin = 1; bin <= Ncuts; ++bin) {
      axis->SetBinLabel(bin, m_CutLabels[bin - 1]);

      if (bin > MASSCUT) { continue; } //Only fill cutflow up to Rel. pT.

      cutflow->SetBinContent(bin, totalCutflow->GetBinContent(bin));
      cutflow->SetBinError(bin, totalCutflow->GetBinError(bin));
    }

    cutflowHistos[histName.Data()] = cutflow;
    std::cout << "Name of hist for " << histName << std::endl;
    std::cout << cutflowHistos[histName.Data()]->GetName() << std::endl;
  }

  std::cout << "Done...\n\nAll cutflows set!\n\n" << std::endl;
  return cutflowHistos;
}
