/**********************************************************************
 * AsgTool: hhWeightTool
 *
 * Authors:
 *      John Alison <john.alison@cern.ch>
 *
 * Description:
 *      Base tool for hh reweigting
 **********************************************************************/
#include "hhTruthWeightTools/hhWeightTool.h"
#include <AsgTools/MessageCheck.h>
#include "PathResolver/PathResolver.h"

#include "TFile.h"
#include "TH1.h"

//**********************************************************************

using namespace xAOD;


hhWeightTool::hhWeightTool( const std::string& name )
  : AsgTool(name)
{
  declareProperty("Verbosity",            m_verbosity=0);

  //
  // Reweight file for SMhh
  //      "hhTruthWeightTools/data/SMhh_mhh_kfactor.root"
  // See (arXiv:1604.06447, arXiv:1608.04798) for details
  // 
  declareProperty("ReweightFile",         m_reweightFile="hhTruthWeightTools/SMhh_mhh_ReWeight.root");
}



//**********************************************************************

StatusCode hhWeightTool::initialize() {
  
  ATH_MSG_INFO( "Initialising tool " << name() );

  loadFile(PathResolverFindCalibFile(m_reweightFile),"mhh_kfactor");

  return StatusCode::SUCCESS;
}


float hhWeightTool::getWeight(float mhh){
  
  float mhh_in_GeV = mhh*0.001;
  
  for(unsigned int iBin = 0; iBin < m_nMHHBins; ++iBin){

    if(mhh_in_GeV < m_mHHUpperBinEdge.at(iBin))
      return m_kFactor.at(iBin);
  }

  return m_kFactor.back();
}


//!==========================================================================
StatusCode hhWeightTool::finalize() {
  
  ATH_MSG_INFO( "Finalising tool " << name() );
  
  return StatusCode::SUCCESS;
}

void hhWeightTool::addMHHBin(float upperMhhBinEdge, float kFactor, float kFactorError){
 
  m_mHHUpperBinEdge.push_back(upperMhhBinEdge);
  m_kFactor        .push_back(kFactor        );
  m_kFactorErr     .push_back(kFactorError   );
}


void hhWeightTool::loadFile(std::string fileName, std::string histName){
 
  TFile* inFile = new TFile(fileName.c_str(),"READ");
  TH1* mhh_hist = (TH1*)inFile->Get(histName.c_str());

  //
  // Underflow + all bins
  //
  for(int iBin =0; iBin < mhh_hist->GetNbinsX()+1; ++iBin){
    addMHHBin((mhh_hist->GetBinLowEdge(iBin)+mhh_hist->GetBinWidth(iBin)),  
	      mhh_hist->GetBinContent(iBin),
	      mhh_hist->GetBinError  (iBin));
  }

  //
  // Overflow
  //
  addMHHBin(1e9,
	    mhh_hist->GetBinContent(mhh_hist->GetNbinsX()+1),
	    mhh_hist->GetBinError  (mhh_hist->GetNbinsX()+1));

  m_nMHHBins = m_mHHUpperBinEdge.size();

  inFile->Close();
  return;
}
