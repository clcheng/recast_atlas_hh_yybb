[XGBoost_btag77_withTop_BCal_looseScore_HMass]

muCB_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_HMass =  125.13 +/- 0.013831 L(123.5 - 126.5) // [GeV]
sigmaCB_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_HMass =  1.4654 +/- 0.022151 L(0 - 6.36943) // [GeV]
alphaCB_Low_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_HMass =  1.4004 +/- 0.057851 L(0 - 5) 
nCB_Low_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_HMass =  11.227 +/- 2.6010 L(0 - 200) 
alphaCB_High_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_HMass =  1.3981 +/- 0.11753 L(0 - 5) 
nCB_High_m_yy_proc_gg_vbf_HH_cat_XGBoost_btag77_withTop_BCal_looseScore_HMass =  35.703 +/- 51.412 L(0 - 200) 
