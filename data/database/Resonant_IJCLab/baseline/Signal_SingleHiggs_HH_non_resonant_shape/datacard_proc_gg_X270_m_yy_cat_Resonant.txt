[Resonant]

muCB_m_yy_proc_gg_X270_cat_Resonant_mX270 =  125.13 +/- 0.014699 L(123.5 - 126.5) // [GeV]
sigmaCB_m_yy_proc_gg_X270_cat_Resonant_mX270 =  1.7950 +/- 0.019816 L(0 - 6.36943) // [GeV]
alphaCB_Low_m_yy_proc_gg_X270_cat_Resonant_mX270 =  1.6262 +/- 0.071002 L(0 - 5) 
nCB_Low_m_yy_proc_gg_X270_cat_Resonant_mX270 =  4.5155 +/- 0.86368 L(0 - 200) 
alphaCB_High_m_yy_proc_gg_X270_cat_Resonant_mX270 =  1.4653 +/- 0.10200 L(0 - 5) 
nCB_High_m_yy_proc_gg_X270_cat_Resonant_mX270 =  9.8245 +/- 4.7062 L(0 - 200) 
