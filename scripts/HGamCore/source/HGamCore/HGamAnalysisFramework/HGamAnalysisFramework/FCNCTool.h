#pragma once

// Local include(s):
#include "HGamAnalysisFramework/EventHandler.h"
#include "HGamAnalysisFramework/TruthHandler.h"

typedef std::vector<short> vshort;
typedef std::vector<int> vint;
typedef std::vector<float> vfloat;

namespace TMVA {
  class Reader;
}

namespace CP {
  class IJetJvtEfficiency;
}

namespace HG {

  class FCNCTool {
  public:

    FCNCTool(HG::EventHandler *eventHandler);

    virtual ~FCNCTool() {};

    virtual EL::StatusCode initialize(Config &config);

    void saveFCNCInfo(xAOD::PhotonContainer    &photons,
                      xAOD::MuonContainer      &muons,
                      xAOD::ElectronContainer  &electrons,
                      xAOD::MissingETContainer &met,
                      xAOD::JetContainer       &jets,
                      const xAOD::EventInfo   *ei);

    void truthHandler(HG::TruthHandler *th) { m_truthHandler = th; }

  private:

    HG::EventHandler         *m_eventHandler; //!
    virtual HG::EventHandler *eventHandler();

    HG::TruthHandler         *m_truthHandler; //!

    void saveMapsToEventInfo();

    unsigned int performSelection(xAOD::PhotonContainer    &photons,
                                  xAOD::MuonContainer      &muons,
                                  xAOD::ElectronContainer  &electrons,
                                  xAOD::MissingETContainer &met,
                                  xAOD::JetContainer       &jets);

    unsigned int LeptonAnaSel(xAOD::MissingETContainer &met,
                              xAOD::JetContainer       &jets);

    unsigned int HadronAnaSel(xAOD::JetContainer       &jets);

    int getChannelNumber() { return m_eventHandler->mcChannelNumber(); };

    bool m_writeTruthInfo;
    bool findTrueTopEx();
    bool findTrueTopSM();
    const xAOD::TruthParticle *ThisParticleFinal(const xAOD::TruthParticle *p);

    void GetDaughter(const xAOD::TruthParticle *tp, std::vector<const xAOD::TruthParticle *> &vecDaughter);
    std::vector<double> m_mcidForMatch;

    std::vector<double> getNuPz(TLorentzVector &lep, const xAOD::MissingET *met, double &mT);

    bool isGoodJ(const xAOD::Jet *);
    bool isCt(const xAOD::Jet &);
    bool isBt(const xAOD::Jet &);

    double m_mgg, m_sumpTj;
    TLorentzVector m_p4H, m_p4g1, m_p4g2;
    struct selLep {
      TLorentzVector p4;
      double sf;
      short id;
    } m_lep;

    // jetInd is for easy retrieval of STORED jets participating to the combination.
    // seljetInd is for the deep-copied (not stored) jets particpating to the combination.
    struct selComb {
      double mTop1 = -1., mTop2 = -1.;
      int cat = -1, jetInd = -1, seljetInd = -1, nbTop2 = 0, ncTop1 = -1; // I put 0 for nb given I will estimate it via ++...
      bool hasmu = false;
    };
    std::vector<selComb> m_comb;

    // for fJvt and MET correction
    double m_spxfJvtRejJet, m_spyfJvtRejJet, m_sptfJvtRejJet;
    double m_fJvtSF;

    // Configuration
    unsigned short m_maxNjet, m_maxNjetLep;
    double m_jptCC, m_jptCF;
    bool m_doLooseJ, m_dofJVT, m_AddfJvtRejJetToMET, m_usefJvtDecision, m_FTagUseDL1, m_doBJetEneCor, m_WriteDetails;
    double m_fJvtC;
    TString m_btagger;
    asg::AnaToolHandle<CP::IJetJvtEfficiency> m_fjvtSFTool; //!


    double m_mTop1Central, m_mTop2Central;
    double m_mTop1HadLow, m_mTop1HadHigh, m_mTop1LepLow, m_mTop1LepHigh;
    double m_mTop2HadLow, m_mTop2HadHigh, m_mTop2LepLow, m_mTop2LepHigh;
    double m_ptelC, m_ptmuC;
    double m_meyCutLow, m_meyCutHigh;
    double m_mTC;

    int m_nEligibleJets;

    // usefull for BDT
    unsigned long int m_evt;
    std::map<TString, float> m_bdtIn;
    // the training of the 4 categories is done in a single run,
    // these variables are needed as spectator to define the category
    float m_bdtInCat, m_bdtInmTop2;
    float m_bdtInEvt2;
    EL::StatusCode InitBDT(Config &config);
    std::map<TString, TMVA::Reader *> m_reader; //!
    std::vector<float> LeptHBDT();
    std::vector<float> HadtHBDT(xAOD::JetContainer &jets);
    std::vector<float> HadttBDT(xAOD::JetContainer &jets);

    // debug
    int m_eventCounter, m_debug;

    // The info I want to store...
    std::map<TString, float>  m_eventInfoFloats;
    std::map<TString, int>    m_eventInfoInts;
    std::map<TString, vfloat> m_eventInfoVFloats;
    std::map<TString, vint>   m_eventInfoVInts;
    std::map<TString, vshort> m_eventInfoVShorts;
    bool m_declared;
    void InitMap();


  };

}
