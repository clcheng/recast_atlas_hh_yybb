# RooFitExtensions

This package contains a wide variety of extensions to RooFit that have
been developed in the context of the ATLAS Higgs analyses and Higgs
combination efforts.

## Setup

This package supports setup with CMake based AMG
releases as well as standalone compilation with ROOT.

In order to compile with CMake, type

    mkdir build
    cd build
    cmake ..
    make -j4
    cd ..
    source build/setup.sh

Now, you are ready to use the package. Don't forget to 

    source build/setup.sh

every time you create a new shell.
