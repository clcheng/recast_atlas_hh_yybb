/*
   Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
 */

// EDM include(s):
// #include "PathResolver/PathResolver.h"
// #include "xAODEventInfo/EventInfo.h"

// Local include(s):
#include "TruthWeightTools/HiggsWeights.h"


// ROOT include(s):
#include "TString.h"


namespace TruthWeightTools
{

  /// WG1 proposed uncertainty scheme
  std::vector<double> HiggsWeights::ggF_qcd_wg1()
  {
    return {ggF_qcd_wg1_mu, ggF_qcd_wg1_res, ggF_qcd_wg1_mig01, ggF_qcd_wg1_mig12, ggF_qcd_wg1_vbf2j, ggF_qcd_wg1_vbf3j, ggF_qcd_wg1_pTH, ggF_qcd_wg1_qm_t};
  }

  /// methods to print weights to the screen
  char *HiggsWeights::uncStr(double var, double nom)
  {
    return var == 0 ? Form("  N/A") : Form("%s%.1f%%", var >= nom ? "+" : "", (var - nom) / nom * 100);
  }

  void HiggsWeights::print()
  {
    double n = nominal;

    printf("\n------\n  Higgs MC weights of current event, pTH = %.1f GeV, Njets = %i\n", pTH, Njets30);
    printf("    Nominal weight: %.3f\n", nominal);
    printf("    Weight 0:       %.3f\n", weight0);

    printf("\n   There are %lu PDF4LHC NLO uncertainty variations\n", pdf4lhc_unc.size());

    if (pdf4lhc_unc.size() == 30) {
      printf("    PDF unc  1-10:");

      for (size_t i = 0; i < 10; ++i) { printf(" %s", uncStr(pdf4lhc_unc[i], n)); }

      printf("\n    PDF unc 11-20:");

      for (size_t i = 10; i < 20; ++i) { printf(" %s", uncStr(pdf4lhc_unc[i], n)); }

      printf("\n    PDF unc 21-30:");

      for (size_t i = 20; i < 30; ++i) { printf(" %s", uncStr(pdf4lhc_unc[i], n)); }

      printf("\n    alphaS up: %s, down: %s\n", uncStr(alphaS_up, n), uncStr(alphaS_dn, n));
    }

    printf("\n   There are %lu NNPDF 3.0 NLO uncertainty variations\n", nnpdf30_unc.size());

    // NNLOPS Specific stuff
    if (ggF_qcd_nnlops.size()) {
      printf("\n    ggF Powheg NNLOPS with %lu QCD uncertainty variations\n",
             ggF_qcd_nnlops.size());

      printf("\n    Quark mass variations  (m_top=inf): %s  (m_b minlo): %s\n",
             uncStr(ggF_mt_inf, n), uncStr(ggF_mb_minlo, n));
      printf("\n    WG1 proposed QCD uncertainty scheme\n");
      printf("      mu: %s,   res: %s,   mig01: %s,   mig12: %s\n",
             uncStr(ggF_qcd_wg1_mu, n), uncStr(ggF_qcd_wg1_res, n), uncStr(ggF_qcd_wg1_mig01, n), uncStr(ggF_qcd_wg1_mig12, n));
      printf("      pTH: %s,   quark-mass, b: %s, t: %s\n",
             uncStr(ggF_qcd_wg1_pTH, n), uncStr(ggF_qcd_wg1_qm_b, n), uncStr(ggF_qcd_wg1_qm_b, n));

    }

    if (qcd.size()) {
      printf("\n    %lu Powheg QCD scale variations\n   ", qcd.size());

      for (auto q : qcd) { printf(" %s", uncStr(q, n)); }

      printf("\n");
    }

    printf("\n   PDF central values\n");
    printf("     PDF4LHC_nlo:  %s, PDF4LHC_nnlo: %s\n", uncStr(pdf4lhc_nlo, n), uncStr(pdf4lhc_nnlo, n));
    printf("     NNPDF30_nlo:  %s, NNPDF30_nnlo: %s\n", uncStr(nnpdf30_nlo, n), uncStr(nnpdf30_nnlo, n));
    printf("     CT10nlo:      %s, CT14nlo:      %s\n", uncStr(ct10nlo, n), uncStr(ct14nlo, n));
    printf("     CT10nlo_0118: %s, CT14nlo_0118: %s\n", uncStr(ct10nlo_0118, n), uncStr(ct14nlo_0118, n));
    printf("     MMHT2014nlo:  %s\n", uncStr(mmht2014nlo, n));
    printf("\n------\n");
  }

} // namespace TruthWeightTools
