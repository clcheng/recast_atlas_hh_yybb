#pragma once

// EDM include(s):
#include "FourMomUtils/xAODP4Helpers.h"

// Local include(s):
#include "HGamAnalysisFramework/HGamCommon.h"
#include "HGamAnalysisFramework/VarHandler.h"

#include <stdexcept>

namespace HG {

  //____________________________________________________________________________
  class pT_h1 : public VarBase<float> {
  public:
    pT_h1() : VarBase("pT_h1") { m_default = -99; m_truthOnly = true; }
    ~pT_h1() { }

    float calculateValue(bool truth)
    {
      if (not truth)
      { return m_default; }

      const xAOD::IParticleContainer *higgs = HG::VarHandler::getInstance()->getHiggsBosons();

      if (higgs->size() < 1)
      { return m_default; }

      return (*higgs)[0]->pt();
    }
  };

  //____________________________________________________________________________
  class pT_h2 : public VarBase<float> {
  public:
    pT_h2() : VarBase("pT_h2") { m_default = -99; m_truthOnly = true; }
    ~pT_h2() { }

    float calculateValue(bool truth)
    {
      if (not truth)
      { return m_default; }

      const xAOD::IParticleContainer *higgs = HG::VarHandler::getInstance()->getHiggsBosons();

      if (higgs->size() < 2)
      { return m_default; }

      return (*higgs)[1]->pt();
    }
  };

  //____________________________________________________________________________
  class y_h1 : public VarBase<float> {
  public:
    y_h1() : VarBase("y_h1") { m_default = -99; m_truthOnly = true; }
    ~y_h1() { }

    float calculateValue(bool truth)
    {
      if (not truth)
      { return m_default; }

      const xAOD::IParticleContainer *higgs = HG::VarHandler::getInstance()->getHiggsBosons();

      if (higgs->size() < 1)
      { return m_default; }

      return (*higgs)[0]->rapidity();
    }
  };

  //____________________________________________________________________________
  class y_h2 : public VarBase<float> {
  public:
    y_h2() : VarBase("y_h2") { m_default = -99; m_truthOnly = true; }
    ~y_h2() { }

    float calculateValue(bool truth)
    {
      if (not truth)
      { return m_default; }

      const xAOD::IParticleContainer *higgs = HG::VarHandler::getInstance()->getHiggsBosons();

      if (higgs->size() < 2)
      { return m_default; }

      return (*higgs)[1]->rapidity();
    }
  };

  //____________________________________________________________________________
  class m_h1 : public VarBase<float> {
  public:
    m_h1() : VarBase("m_h1") { m_default = -99; m_truthOnly = true; }
    ~m_h1() { }

    float calculateValue(bool truth)
    {
      if (not truth)
      { return m_default; }

      const xAOD::IParticleContainer *higgs = HG::VarHandler::getInstance()->getHiggsBosons();

      if (higgs->size() < 1)
      { return m_default; }

      return (*higgs)[0]->m();
    }
  };

  //____________________________________________________________________________
  class m_h2 : public VarBase<float> {
  public:
    m_h2() : VarBase("m_h2") { m_default = -99; m_truthOnly = true; }
    ~m_h2() { }

    float calculateValue(bool truth)
    {
      if (not truth)
      { return m_default; }

      const xAOD::IParticleContainer *higgs = HG::VarHandler::getInstance()->getHiggsBosons();

      if (higgs->size() < 2)
      { return m_default; }

      return (*higgs)[1]->m();
    }
  };

  //____________________________________________________________________________
  class yAbs_yy : public VarBase<float> {
  public:
    yAbs_yy() : VarBase("yAbs_yy") { m_default = -99; }
    ~yAbs_yy() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      return fabs(((*photons)[0]->p4() + (*photons)[1]->p4()).Rapidity());
    }
  };

  //____________________________________________________________________________
  class pTt_yy : public VarBase<float> {
  public:
    pTt_yy() : VarBase("pTt_yy") { m_default = -99; }
    ~pTt_yy() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      TLorentzVector g1 = (*photons)[0]->p4(), g2 = (*photons)[1]->p4();
      return fabs(g1.Px() * g2.Py() - g2.Px() * g1.Py()) / (g1 - g2).Pt() * 2.0;
    }
  };

  //____________________________________________________________________________
  class m_yy : public VarBase<float> {
  public:
    m_yy() : VarBase("m_yy") { m_default = -99; }
    ~m_yy() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      return ((*photons)[0]->p4() + (*photons)[1]->p4()).M();
    }
  };

  //____________________________________________________________________________
  class pT_y1 : public VarBase<float> {
  public:
    pT_y1() : VarBase("pT_y1") { m_default = -99; }
    ~pT_y1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 1)
      { return m_default; }

      return (*photons)[0]->pt();
    }
  };

  //____________________________________________________________________________
  class pT_y2 : public VarBase<float> {
  public:
    pT_y2() : VarBase("pT_y2") { m_default = -99; }
    ~pT_y2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      return (*photons)[1]->pt();
    }
  };

  //____________________________________________________________________________
  class E_y1 : public VarBase<float> {
  public:
    E_y1() : VarBase("E_y1") { m_default = -99; }
    ~E_y1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 1)
      { return m_default; }

      return (*photons)[0]->e();
    }
  };

  //____________________________________________________________________________
  class E_y2 : public VarBase<float> {
  public:
    E_y2() : VarBase("E_y2") { m_default = -99; }
    ~E_y2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      return (*photons)[1]->e();
    }
  };

  //____________________________________________________________________________
  class sumpT_y_y : public VarBase<float> {
  public:
    sumpT_y_y() : VarBase("sumpT_y_y") { m_default = -99; }
    ~sumpT_y_y() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      return (*photons)[0]->pt() + (*photons)[1]->pt();
    }
  };

  //____________________________________________________________________________
  class DpT_y_y : public VarBase<float> {
  public:
    DpT_y_y() : VarBase("DpT_y_y") { m_default = -99; }
    ~DpT_y_y() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      return (*photons)[0]->pt() - (*photons)[1]->pt();
    }
  };

  //____________________________________________________________________________
  class pT_hard : public VarBase<float> {
  public:
    pT_hard() : VarBase("pT_hard") { m_default = -99; }
    ~pT_hard() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (photons->size() == 0 && jets->size() == 0)
      { return m_default; }

      TLorentzVector all;

      for (auto gam : *photons) { all += gam->p4(); }

      for (auto jet : *jets) { all += jet->p4(); }

      return all.Pt();
    }
  };

  //____________________________________________________________________________
  class massTrans : public VarBase<float> {
  public:
    massTrans() : VarBase("massTrans") { m_default = -999999; }
    ~massTrans() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() == 0 && els->size() == 0) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      const xAOD::MissingET *met = truth ? (*mets)["NonInt"] : (*mets)["TST"];

      TLorentzVector metvec;
      metvec.SetPtEtaPhiE(met->met(), 0, met->phi(), 0);

      double ETsquare = m_default;
      double PTsquare = m_default;

      bool useEl = (els->size() > 0);

      if (useEl && (mus->size() > 0)) {
        useEl = (els->at(0)->pt() > mus->at(0)->pt());
      }

      double mT = m_default;

      if (useEl) {
        ETsquare  = pow(met->met() + els->at(0)->pt(), 2) ;
        PTsquare  = pow(met->mpx() + els->at(0)->pt() * cos(els->at(0)->phi()), 2);
        PTsquare += pow(met->mpy() + els->at(0)->pt() * sin(els->at(0)->phi()), 2);
      } else {
        ETsquare  = pow(met->met() + mus->at(0)->pt(), 2) ;
        PTsquare  = pow(met->mpx() + mus->at(0)->pt() * cos(mus->at(0)->phi()), 2);
        PTsquare += pow(met->mpy() + mus->at(0)->pt() * sin(mus->at(0)->phi()), 2);
      }

      mT = sqrt(ETsquare - PTsquare);
      return mT;
    }
  };

  //____________________________________________________________________________
  class pTlepMET : public VarBase<float> {
  public:
    pTlepMET() : VarBase("pTlepMET") { m_default = -99; }
    ~pTlepMET() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() == 0 && els->size() == 0) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      const xAOD::MissingET *met = truth ? (*mets)["NonInt"] : (*mets)["TST"];

      TLorentzVector metvec;
      metvec.SetPtEtaPhiM(met->met(), 0, met->phi(), 0);

      bool useEl = (els->size() > 0);

      if (useEl && (mus->size() > 0)) {
        useEl = (els->at(0)->pt() > mus->at(0)->pt());
      }

      double pTlv = m_default;

      if (useEl)  { pTlv = (metvec + els->at(0)->p4()).Pt(); }
      else        { pTlv = (metvec + mus->at(0)->p4()).Pt(); }

      return pTlv;
    }
  };

  //____________________________________________________________________________
  class etalepMET : public VarBase<float> {
  public:
    etalepMET() : VarBase("etalepMET") { m_default = -99; }
    ~etalepMET() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() == 0 && els->size() == 0) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      const xAOD::MissingET *met = truth ? (*mets)["NonInt"] : (*mets)["TST"];

      TLorentzVector metvec;
      metvec.SetPtEtaPhiM(met->met(), 0, met->phi(), 0);

      bool useEl = (els->size() > 0);

      if (useEl && (mus->size() > 0)) {
        useEl = (els->at(0)->pt() > mus->at(0)->pt());
      }

      double pTlv = m_default;

      if (useEl)  { pTlv = (metvec + els->at(0)->p4()).Eta(); }
      else        { pTlv = (metvec + mus->at(0)->p4()).Eta(); }

      return pTlv;
    }
  };

  //____________________________________________________________________________
  class philepMET : public VarBase<float> {
  public:
    philepMET() : VarBase("philepMET") { m_default = -99; }
    ~philepMET() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() == 0 && els->size() == 0) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      const xAOD::MissingET *met = truth ? (*mets)["NonInt"] : (*mets)["TST"];

      TLorentzVector metvec;
      metvec.SetPtEtaPhiM(met->met(), 0, met->phi(), 0);

      bool useEl = (els->size() > 0);

      if (useEl && (mus->size() > 0)) {
        useEl = (els->at(0)->pt() > mus->at(0)->pt());
      }

      double pTlv = m_default;

      if (useEl)  { pTlv = (metvec + els->at(0)->p4()).Phi(); }
      else        { pTlv = (metvec + mus->at(0)->p4()).Phi(); }

      return pTlv;
    }
  };

  //____________________________________________________________________________
  class HTall_30 : public VarBase<float> {
  public:
    HTall_30() : VarBase("HTall_30") { m_default = -99; }
    ~HTall_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      double HT = 0.0;

      for (auto ph : *photons) { HT += ph->pt(); }

      for (auto jet : *jets) if (jet->pt() >= 30 * HG::GeV) { HT += jet->pt(); }

      for (auto mu : *mus) if (mu->pt() >= 15 * HG::GeV) { HT += mu->pt(); }

      for (auto el : *els) if (el->pt() >= 15 * HG::GeV) { HT += el->pt(); }

      return HT;
    }
  };

  //____________________________________________________________________________
  class HT_30 : public VarBase<float> {
  public:
    HT_30() : VarBase("HT_30") { m_default = -99; }
    ~HT_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      double HT = 0.0;

      for (auto jet : *jets) {
        if (jet->pt() < 30 * HG::GeV) { continue; }

        HT += jet->pt();
      }

      return HT;
    }
  };

  //____________________________________________________________________________
  class pT_alljet : public VarBase<float> {
  public:
    pT_alljet() : VarBase("pT_alljet") { m_default = -99; }
    ~pT_alljet() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() == 0)
      { return m_default; }

      TLorentzVector all;

      for (auto jet : *jets) { all += jet->p4(); }

      return all.Pt();
    }
  };

  //____________________________________________________________________________

  class eta_alljet : public VarBase<float> {
  public:
    eta_alljet() : VarBase("eta_alljet") { m_default = -99; }
    ~eta_alljet() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() == 0)
      { return m_default; }

      TLorentzVector all;

      for (auto jet : *jets) { all += jet->p4(); }

      return all.Eta();
    }
  };

  //____________________________________________________________________________
  class m_alljet : public VarBase<float> {
  public:
    m_alljet() : VarBase("m_alljet") { m_default = -99; }
    ~m_alljet() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() == 0)
      { return m_default; }

      TLorentzVector all;

      for (auto jet : *jets) { all += jet->p4(); }

      return all.M();
    }
  };

  //____________________________________________________________________________
  class m_alljet_30 : public VarBase<float> {
  public:
    m_alljet_30() : VarBase("m_alljet_30") { m_default = -99; }
    ~m_alljet_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() == 0)
      { return m_default; }

      TLorentzVector all;

      for (auto jet : *jets) {
        if (jet->pt() < 30.0 * HG::GeV) { continue; }

        all += jet->p4();
      }

      return all.M();
    }
  };

  //____________________________________________________________________________
  //  //  //
  class pT_yy : public VarBase<float> {
  public:
    pT_yy() : VarBase("pT_yy") { m_default = -99; }
    ~pT_yy() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      return ((*photons)[0]->p4() + (*photons)[1]->p4()).Pt();
    }
  };

  //____________________________________________________________________________
  class cosTS_yy : public VarBase<float> {
  public:
    cosTS_yy() : VarBase("cosTS_yy") { m_default = -99; }
    ~cosTS_yy() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *gams = HG::VarHandler::getInstance()->getPhotons(truth);

      if (gams->size() < 2)
      { return m_default; }

      const TLorentzVector &y1 = (*gams)[0]->p4(), &y2 = (*gams)[1]->p4();
      return std::abs(((y1.E() + y1.Pz()) * (y2.E() - y2.Pz()) - (y1.E() - y1.Pz()) * (y2.E() + y2.Pz()))
                      / ((y1 + y2).M() * sqrt(pow((y1 + y2).M(), 2) + pow((y1 + y2).Pt(), 2))));
    }
  };

  //____________________________________________________________________________
  double tauJet(const xAOD::IParticle *g1, const xAOD::IParticle *g2, const xAOD::IParticle *jet);

  //____________________________________________________________________________
  class maxTau_yyj_30 : public VarBase<float> {
  public:
    maxTau_yyj_30() : VarBase("maxTau_yyj_30") { m_default = -99; }
    ~maxTau_yyj_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *gams = HG::VarHandler::getInstance()->getPhotons(truth);

      if (gams->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 1 || (*jets)[0]->pt() < 30.0 * HG::GeV)
      { return m_default; }

      float max = m_default, temp = m_default;

      for (auto jet : *jets) {
        if (jet->pt() < 30.0 * HG::GeV) { continue; }

        temp = tauJet((*gams)[0], (*gams)[1], jet);
        max = temp > max ? temp : max;
      }

      return max;
    }
  };

  //____________________________________________________________________________
  class sumTau_yyj_30 : public VarBase<float> {
  public:
    sumTau_yyj_30() : VarBase("sumTau_yyj_30") { m_default = -99; }
    ~sumTau_yyj_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *gams = HG::VarHandler::getInstance()->getPhotons(truth);

      if (gams->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 1 || (*jets)[0]->pt() < 30.0 * HG::GeV)
      { return m_default; }

      float sum = 0.0, temp = m_default;

      for (auto jet : *jets) {
        if (jet->pt() < 30.0 * HG::GeV) { continue; }

        temp = tauJet((*gams)[0], (*gams)[1], jet);
        sum += temp > 5 * HG::GeV ? temp : 0.0;
      }

      return sum;
    }
  };



  //____________________________________________________________________________
  class phiStar_yy : public VarBase<float> {
  public:
    phiStar_yy() : VarBase("phiStar_yy") { m_default = -99; }
    ~phiStar_yy() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      const TLorentzVector &g1 = (*photons)[0]->p4(), &g2 = (*photons)[1]->p4();
      return tan((TMath::Pi() - fabs(g1.DeltaPhi(g2))) / 2.0) *
             sqrt(1.0 - pow(tanh((g1.Eta() - g2.Eta()) / 2.0), 2.0)); // FIXME?
    }
  };

  //____________________________________________________________________________
  //
  class N_conv : public VarBase<float> {
  public:
    N_conv() : VarBase("N_conv") { m_default = 0; m_recoOnly = true; }
    ~N_conv() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2) { return m_default; }

      const static SG::AuxElement::ConstAccessor<int> acc("conversionType");

      const int conv_ph1 = bool(acc(*(*photons)[0]));
      const int conv_ph2 = bool(acc(*(*photons)[1]));

      return conv_ph1 + conv_ph2;
    }
  };

  //____________________________________________________________________________
  //
  class Dphi_y_y : public VarBase<float> {
  public:
    Dphi_y_y() : VarBase("Dphi_y_y") { m_default = -99; }
    ~Dphi_y_y() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      TLorentzVector g1 = (*photons)[0]->p4(), g2 = (*photons)[1]->p4();
      return fabs(g1.DeltaPhi(g2));
    }
  };

  //____________________________________________________________________________
  class Dy_y_y : public VarBase<float> {
  public:
    Dy_y_y() : VarBase("Dy_y_y") { m_default = -99; }
    ~Dy_y_y() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);

      if (photons->size() < 2)
      { return m_default; }

      return fabs((*photons)[0]->rapidity() - (*photons)[1]->rapidity());
    }
  };

  //____________________________________________________________________________
  class N_e : public VarBase<int> {
  public:
    N_e() : VarBase("N_e") { m_default = -99; }
    ~N_e() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      return els->size();
    }
  };

  //____________________________________________________________________________
  class N_mu : public VarBase<int> {
  public:
    N_mu() : VarBase("N_mu") { m_default = -99; }
    ~N_mu() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      return mus->size();
    }
  };

  //____________________________________________________________________________
  class N_lep : public VarBase<int> {
  public:
    N_lep() : VarBase("N_lep") { m_default = -99; }
    ~N_lep() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      return mus->size() + els->size();
    }
  };

  //____________________________________________________________________________
  class N_lep_15 : public VarBase<int> {
  public:
    N_lep_15() : VarBase("N_lep_15") { m_default = -99; }
    ~N_lep_15() { }

    int calculateValue(bool truth)
    {
      int nmus = 0, nels = 0;
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);

      for (auto mu : *mus) if (mu->pt() >= 15 * HG::GeV) { nmus++; }

      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      for (auto el : *els) if (el->pt() >= 15 * HG::GeV) { nels++; }

      return nels + nmus;
    }
  };

  //____________________________________________________________________________
  class weightN_lep : public VarBase<float> {
  public:
    weightN_lep() : VarBase("weightN_lep") { m_default = 1.0; }
    ~weightN_lep() { }

    float calculateValue(bool truth)
    {
      double weight = 1.0;
      SG::AuxElement::ConstAccessor<float> SF("scaleFactor");
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);

      for (auto mu : *mus) { weight *= SF(*mu); } //->auxdata<float>("scaleFactor");

      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      for (auto el : *els) { weight *= SF(*el); } //->auxdata<float>("scaleFactor");

      return weight;
    }
  };

  //____________________________________________________________________________
  class weightN_lep_15 : public VarBase<float> {
  public:
    weightN_lep_15() : VarBase("weightN_lep_15") { m_default = 1.0; }
    ~weightN_lep_15() { }

    float calculateValue(bool truth)
    {
      double weight = 1.0;
      SG::AuxElement::ConstAccessor<float> SF("scaleFactor");
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);

      for (auto mu : *mus) if (mu->pt() >= 15 * HG::GeV) { weight *= SF(*mu); } //->auxdata<float>("scaleFactor");

      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      for (auto el : *els) if (el->pt() >= 15 * HG::GeV) { weight *= SF(*el); } //->auxdata<float>("scaleFactor");

      return weight;
    }
  };

  //____________________________________________________________________________
  class N_j : public VarBase<int> {
  public:
    N_j() : VarBase("N_j") { m_default = -99; }
    ~N_j() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      return jets->size();
    }
  };

  //____________________________________________________________________________
  class N_j_30 : public VarBase<int> {
  public:
    N_j_30() : VarBase("N_j_30") { m_default = -99; }
    ~N_j_30() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      int njets = 0;

      for (auto jet : *jets)
        if (jet->pt() >= 30 * HG::GeV) { njets++; }

      return njets;
    }
  };

  //____________________________________________________________________________
  class N_j_50 : public VarBase<int> {
  public:
    N_j_50() : VarBase("N_j_50") { m_default = -99; }
    ~N_j_50() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      int njets = 0;

      for (auto jet : *jets)
        if (jet->pt() >= 50 * HG::GeV) { njets++; }

      return njets;
    }
  };

  //____________________________________________________________________________
  class N_j_central : public VarBase<int> {
  public:
    N_j_central() : VarBase("N_j_central") { m_default = -99; }
    ~N_j_central() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      int njets = 0;

      for (auto jet : *jets)
        if (fabs(jet->eta()) < 2.5) { njets++; }

      return njets;
    }
  };

  //____________________________________________________________________________
  class N_j_central30 : public VarBase<int> {
  public:
    N_j_central30() : VarBase("N_j_central30") { m_default = -99; }
    ~N_j_central30() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      int njets = 0;

      for (auto jet : *jets)
        if (fabs(jet->eta()) < 2.5 && jet->pt() >= 30 * HG::GeV) { njets++; }

      return njets;
    }
  };

  struct N_j_btag : public VarBase<int> {
  public:
    N_j_btag() : VarBase("N_j_btag") { m_default = -99; }
    ~N_j_btag() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->empty()) { return 0; }

      int njets = 0;
      static SG::AuxElement::ConstAccessor<char> acc_DL1r_FixedCutBEff_70("DL1r_FixedCutBEff_70");

      if (not acc_DL1r_FixedCutBEff_70.isAvailable(*(*jets)[0])) {
        throw std::runtime_error("DL1r_FixedCutBEff_70 is not available");
      }

      for (auto jet : *jets) {
        if (acc_DL1r_FixedCutBEff_70(*jet)) { ++njets; }
      }

      return njets;
    }
  };


  //____________________________________________________________________________
  class N_j_btag30 : public VarBase<int> {
  public:
    N_j_btag30() : VarBase("N_j_btag30") { m_default = -99; }
    ~N_j_btag30() { }

    int calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->empty()) { return 0; }

      int njets = 0;
      static SG::AuxElement::ConstAccessor<char> acc_DL1r_FixedCutBEff_70("DL1r_FixedCutBEff_70");

      if (not acc_DL1r_FixedCutBEff_70.isAvailable(*(*jets)[0])) {
        throw std::runtime_error("DL1r_FixedCutBEff_70 is not available");
      }

      for (auto jet : *jets) {
        if (acc_DL1r_FixedCutBEff_70(*jet) && jet->pt() > 30 * HG::GeV) { ++njets; }
      }

      return njets;
    }
  };

  //____________________________________________________________________________
  class pT_j1_30 : public VarBase<float> {
  public:
    pT_j1_30() : VarBase("pT_j1_30") { m_default = -99; }
    ~pT_j1_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 1)
      { return m_default; }

      return (*jets)[0]->pt() < 30 * HG::GeV ? m_default : (*jets)[0]->pt();
    }
  };

  //____________________________________________________________________________
  class pT_j1 : public VarBase<float> {
  public:
    pT_j1() : VarBase("pT_j1") { m_default = -99; }
    ~pT_j1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 1)
      { return m_default; }

      return (*jets)[0]->pt();
    }
  };

  //____________________________________________________________________________
  class yAbs_j1_30 : public VarBase<float> {
  public:
    yAbs_j1_30() : VarBase("yAbs_j1_30") { m_default = -99; }
    ~yAbs_j1_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 1)
      { return m_default; }

      return (*jets)[0]->pt() < 30 * HG::GeV ? m_default : fabs((*jets)[0]->rapidity());
    }
  };

  //____________________________________________________________________________
  class yAbs_j1 : public VarBase<float> {
  public:
    yAbs_j1() : VarBase("yAbs_j1") { m_default = -99; }
    ~yAbs_j1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 1)
      { return m_default; }

      return fabs((*jets)[0]->rapidity());
    }
  };

  //____________________________________________________________________________
  class pT_jj : public VarBase<float> {
  public:
    pT_jj() : VarBase("pT_jj") { m_default = -99; }
    ~pT_jj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return ((*jets)[0]->p4() + (*jets)[1]->p4()).Pt();
    }
  };

  //____________________________________________________________________________
  class pT_yyj : public VarBase<float> {
  public:
    pT_yyj() : VarBase("pT_yyj") { m_default = -99; }
    ~pT_yyj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (photons->size() < 2)
      { return m_default; }

      if (jets->size() < 1)
      { return m_default; }

      return ((*photons)[0]->p4() + (*photons)[1]->p4() + (*jets)[0]->p4()).Pt();
    }
  };

  /** pT of the yyj system. j = leading selected jet with pT >= 30 GeV */
  class pT_yyj_30 : public VarBase<float> {
  public:
    pT_yyj_30() : VarBase("pT_yyj_30") { m_default = -99; }
    ~pT_yyj_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (photons->size() < 2)
      { return m_default; }

      if (jets->size() < 1)
      { return m_default; }

      if ((*jets)[0]->pt() < 30 * HG::GeV)
      { return m_default; }

      return ((*photons)[0]->p4() + (*photons)[1]->p4() + (*jets)[0]->p4()).Pt();
    }
  };

  //____________________________________________________________________________
  class m_yyj : public VarBase<float> {
  public:
    m_yyj() : VarBase("m_yyj") { m_default = -99; }
    ~m_yyj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (photons->size() < 2)
      { return m_default; }

      if (jets->size() < 1)
      { return m_default; }

      return ((*photons)[0]->p4() + (*photons)[1]->p4() + (*jets)[0]->p4()).M();
    }
  };

  /** mass of the yyj system. j = leading selected jet with pT >= 30 GeV */
  class m_yyj_30 : public VarBase<float> {
  public:
    m_yyj_30() : VarBase("m_yyj_30") { m_default = -99; }
    ~m_yyj_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *photons = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (photons->size() < 2)
      { return m_default; }

      if (jets->size() < 1)
      { return m_default; }

      if ((*jets)[0]->pt() < 30 * HG::GeV)
      { return m_default; }

      return ((*photons)[0]->p4() + (*photons)[1]->p4() + (*jets)[0]->p4()).M();
    }
  };

  //____________________________________________________________________________
  class passMeyCut : public VarBase<char> {
  public:
    passMeyCut() : VarBase("passMeyCut") { m_default = false; }
    ~passMeyCut() { }

    char calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *es = HG::VarHandler::getInstance()->getElectrons(truth);

      for (auto ph : *ps) {
        TLorentzVector ph4 = ph->p4();

        for (auto el : *es) {
          double mass = (ph4 + el->p4()).M();

          if (84 * HG::GeV <= mass && mass < 94 * HG::GeV) { return false; }
        }
      }

      return true;
    }
  };

  //____________________________________________________________________________
  class pT_j2_30 : public VarBase<float> {
  public:
    pT_j2_30() : VarBase("pT_j2_30") { m_default = -99; }
    ~pT_j2_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 1)
      { return m_default; }

      if (jets->size() < 2)
      { return 0; }

      return (*jets)[1]->pt() < 30 * HG::GeV ? m_default : (*jets)[1]->pt();
    }
  };

  //____________________________________________________________________________
  class pT_j3_30 : public VarBase<float> {
  public:
    pT_j3_30() : VarBase("pT_j3_30") { m_default = -99; }
    ~pT_j3_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      if (jets->size() < 3)
      { return 0; }

      return (*jets)[2]->pt() < 30 * HG::GeV ? m_default : (*jets)[2]->pt();
    }
  };

  //____________________________________________________________________________
  class pT_j2 : public VarBase<float> {
  public:
    pT_j2() : VarBase("pT_j2") { m_default = -99; }
    ~pT_j2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return (*jets)[1]->pt();
    }
  };

  //____________________________________________________________________________
  class yAbs_j2_30 : public VarBase<float> {
  public:
    yAbs_j2_30() : VarBase("yAbs_j2_30") { m_default = -99; }
    ~yAbs_j2_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return (*jets)[1]->pt() < 30 * HG::GeV ? m_default : fabs((*jets)[1]->rapidity());
    }
  };

  //____________________________________________________________________________
  class yAbs_j2 : public VarBase<float> {
  public:
    yAbs_j2() : VarBase("yAbs_j2") { m_default = -99; }
    ~yAbs_j2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return fabs((*jets)[1]->rapidity());
    }
  };

  //____________________________________________________________________________
  class m_jj_30 : public VarBase<float> {
  public:
    m_jj_30() : VarBase("m_jj_30") { m_default = -99; }
    ~m_jj_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return (*jets)[1]->pt() < 30 * HG::GeV ? m_default : ((*jets)[0]->p4() + (*jets)[1]->p4()).M();
    }
  };

  /** mass of the jj system. j = leading or subleading selected jet with pT >= 50 GeV */
  class m_jj_50 : public VarBase<float> {
  public:
    m_jj_50() : VarBase("m_jj_50") { m_default = -99; }
    ~m_jj_50() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return (*jets)[1]->pt() < 50 * HG::GeV  ? m_default : ((*jets)[0]->p4() + (*jets)[1]->p4()).M();
    }
  };

  //____________________________________________________________________________
  class m_jj : public VarBase<float> {
  public:
    m_jj() : VarBase("m_jj") { m_default = -99; }
    ~m_jj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return ((*jets)[0]->p4() + (*jets)[1]->p4()).M();
    }
  };

  //____________________________________________________________________________
  class Dy_j_j_30 : public VarBase<float> {
  public:
    Dy_j_j_30() : VarBase("Dy_j_j_30") { m_default = -99; }
    ~Dy_j_j_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return (*jets)[1]->pt() < 30 * HG::GeV ? m_default : fabs((*jets)[0]->rapidity() - (*jets)[1]->rapidity());
    }
  };

  //____________________________________________________________________________
  class Dy_j_j : public VarBase<float> {
  public:
    Dy_j_j() : VarBase("Dy_j_j") { m_default = -99; }
    ~Dy_j_j() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return fabs((*jets)[0]->rapidity() - (*jets)[1]->rapidity());
    }
  };
  //____________________________________________________________________________

  class Deta_j_j : public VarBase<float> {
  public:
    Deta_j_j() : VarBase("Deta_j_j") { m_default = -99; }
    ~Deta_j_j() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return fabs((*jets)[0]->eta() - (*jets)[1]->eta());
    }
  };

  //____________________________________________________________________________
  class Dy_yy_jj_30 : public VarBase<float> {
  public:
    Dy_yy_jj_30() : VarBase("Dy_yy_jj_30") { m_default = -99; }
    ~Dy_yy_jj_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      if ((*js)[1]->pt() < 30 * HG::GeV)
      { return m_default; }

      return fabs(((*ps)[0]->p4() + (*ps)[1]->p4()).Rapidity() - ((*js)[0]->p4() + (*js)[1]->p4()).Rapidity());
    }
  };

  /** abs delta rapidity between yy system and jj system. j = leading or subleading selected jet with pT >= 50 GeV */
  class Dy_yy_jj_50 : public VarBase<float> {
  public:
    Dy_yy_jj_50() : VarBase("Dy_yy_jj_50") { m_default = -99; }
    ~Dy_yy_jj_50() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      if ((*js)[1]->pt() < 50 * HG::GeV)
      { return m_default; }

      return fabs(((*ps)[0]->p4() + (*ps)[1]->p4()).Rapidity() - ((*js)[0]->p4() + (*js)[1]->p4()).Rapidity());
    }
  };

  //____________________________________________________________________________
  class Dy_yy_jj : public VarBase<float> {
  public:
    Dy_yy_jj() : VarBase("Dy_yy_jj") { m_default = -99; }
    ~Dy_yy_jj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      return fabs(((*ps)[0]->p4() + (*ps)[1]->p4()).Rapidity() - ((*js)[0]->p4() + (*js)[1]->p4()).Rapidity());
    }
  };

  //____________________________________________________________________________
  class Dphi_j_j : public VarBase<float> {
  public:
    Dphi_j_j() : VarBase("Dphi_j_j") { m_default = -99; }
    ~Dphi_j_j() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return fabs((*jets)[0]->p4().DeltaPhi((*jets)[1]->p4()));
    }
  };

  //____________________________________________________________________________
  class Dphi_j_j_30_signed : public VarBase<float> {
  public:
    Dphi_j_j_30_signed() : VarBase("Dphi_j_j_30_signed") { m_default = -99; }
    ~Dphi_j_j_30_signed() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      if ((*jets)[1]->pt() < 30.0 * HG::GeV)
      { return m_default; }

      double phi1 = (*jets)[0]->phi(), phi2 = (*jets)[1]->phi();

      if ((*jets)[0]->rapidity() < (*jets)[1]->rapidity())
      { std::swap(phi1, phi2); }

      return TVector2::Phi_mpi_pi(phi1 - phi2);
    }
  };

  /** phi1 - phi2, where 1, 2 are leading and subleading jets with pT >= 50 GeV. The difference is reported in the range -pi, pi */
  class Dphi_j_j_50_signed : public VarBase<float> {
  public:
    Dphi_j_j_50_signed() : VarBase("Dphi_j_j_50_signed") { m_default = -99; }
    ~Dphi_j_j_50_signed() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      if ((*jets)[1]->pt() < 50.0 * HG::GeV)
      { return m_default; }

      double phi1 = (*jets)[0]->phi(), phi2 = (*jets)[1]->phi();

      if ((*jets)[0]->rapidity() < (*jets)[1]->rapidity())
      { std::swap(phi1, phi2); }

      return TVector2::Phi_mpi_pi(phi1 - phi2);
    }
  };

  //____________________________________________________________________________
  class Dphi_j_j_30 : public VarBase<float> {
  public:
    Dphi_j_j_30() : VarBase("Dphi_j_j_30") { m_default = -99; }
    ~Dphi_j_j_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return (*jets)[1]->pt() < 30.0 * HG::GeV ? m_default : fabs((*jets)[0]->p4().DeltaPhi((*jets)[1]->p4()));
    }
  };

  /** delta phi jj, where j = leading or subpleading jet with pT >= 50 GeV. The absolute difference is reported in the range 0, pi */
  class Dphi_j_j_50 : public VarBase<float> {
  public:
    Dphi_j_j_50() : VarBase("Dphi_j_j_50") { m_default = -99; }
    ~Dphi_j_j_50() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (jets->size() < 2)
      { return m_default; }

      return (*jets)[1]->pt() < 50.0 * HG::GeV ? m_default : fabs((*jets)[0]->p4().DeltaPhi((*jets)[1]->p4()));
    }
  };

  //____________________________________________________________________________
  class Dphi_yy_jj : public VarBase<float> {
  public:
    Dphi_yy_jj() : VarBase("Dphi_yy_jj") { m_default = -99; }
    ~Dphi_yy_jj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      return fabs(((*ps)[0]->p4() + (*ps)[1]->p4()).DeltaPhi((*js)[0]->p4() + (*js)[1]->p4()));
    }
  };

  //____________________________________________________________________________
  class Dphi_yy_jj_30 : public VarBase<float> {
  public:
    Dphi_yy_jj_30() : VarBase("Dphi_yy_jj_30") { m_default = -99; }
    ~Dphi_yy_jj_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      if ((*js)[1]->pt() < 30 * HG::GeV) { return m_default; }

      return fabs(((*ps)[0]->p4() + (*ps)[1]->p4()).DeltaPhi((*js)[0]->p4() + (*js)[1]->p4()));
    }
  };

  /** Delta phi between yy and jj system. j = leading or subleading selected jet with pT >= 50 GeV. The absolute difference is reported in the range 0, pi */
  class Dphi_yy_jj_50 : public VarBase<float> {
  public:
    Dphi_yy_jj_50() : VarBase("Dphi_yy_jj_50") { m_default = -99; }
    ~Dphi_yy_jj_50() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      if ((*js)[1]->pt() < 50 * HG::GeV) { return m_default; }

      return fabs(((*ps)[0]->p4() + (*ps)[1]->p4()).DeltaPhi((*js)[0]->p4() + (*js)[1]->p4()));
    }
  };

  //____________________________________________________________________________
  class m_yyjj : public VarBase<float> {
  public:
    m_yyjj() : VarBase("m_yyjj") { m_default = -99; }
    ~m_yyjj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      return ((*ps)[0]->p4() + (*ps)[1]->p4() + (*js)[0]->p4() + (*js)[1]->p4()).M();
    }
  };

  //____________________________________________________________________________
  class pT_yyjj_30 : public VarBase<float> {
  public:
    pT_yyjj_30() : VarBase("pT_yyjj_30") { m_default = -99; }
    ~pT_yyjj_30() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      if ((*js)[1]->pt() < 30 * HG::GeV)
      { return m_default; }

      return ((*ps)[0]->p4() + (*ps)[1]->p4() + (*js)[0]->p4() + (*js)[1]->p4()).Pt();
    }
  };

  /** pT of the yyjj system. j = leading or subleading selected jet with pT >= 50 GeV */
  class pT_yyjj_50 : public VarBase<float> {
  public:
    pT_yyjj_50() : VarBase("pT_yyjj_50") { m_default = -99; }
    ~pT_yyjj_50() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      if ((*js)[1]->pt() < 50 * HG::GeV)
      { return m_default; }

      return ((*ps)[0]->p4() + (*ps)[1]->p4() + (*js)[0]->p4() + (*js)[1]->p4()).Pt();
    }
  };

  //____________________________________________________________________________
  class pT_yyjj : public VarBase<float> {
  public:
    pT_yyjj() : VarBase("pT_yyjj") { m_default = -99; }
    ~pT_yyjj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      return ((*ps)[0]->p4() + (*ps)[1]->p4() + (*js)[0]->p4() + (*js)[1]->p4()).Pt();
    }
  };

  //____________________________________________________________________________
  class m_ee : public VarBase<float> {
  public:
    m_ee() : VarBase("m_ee") { m_default = -99; }
    ~m_ee() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *es = HG::VarHandler::getInstance()->getElectrons(truth);

      if (es->size() < 2)
      { return m_default; }

      return ((*es)[0]->p4() + (*es)[1]->p4()).M();
    }
  };

  //____________________________________________________________________________
  class m_mumu : public VarBase<float> {
  public:
    m_mumu() : VarBase("m_mumu") { m_default = -99; }
    ~m_mumu() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);

      if (mus->size() < 2)
      { return m_default; }

      return ((*mus)[0]->p4() + (*mus)[1]->p4()).M();
    }
  };

  struct pt_ee : public VarBase<float> {
    pt_ee() : VarBase("pt_ee") { m_default = -99; }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *es = HG::VarHandler::getInstance()->getElectrons(truth);

      if (es->size() < 2) { return m_default; }

      return ((*es)[0]->p4() + (*es)[1]->p4()).Pt();
    }
  };

  struct pt_mumu : public VarBase<float> {
    pt_mumu() : VarBase("pt_mumu") { m_default = -99; }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);

      if (mus->size() < 2) { return m_default; }

      return ((*mus)[0]->p4() + (*mus)[1]->p4()).Pt();
    }
  };

  struct pt_llmax : public VarBase<float> {
    pt_llmax() : VarBase("pt_llmax") { m_default = -99; }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *es = HG::VarHandler::getInstance()->getElectrons(truth);

      float ptee = -99;

      if (es->size() >= 2) {
        ptee = ((*es)[0]->p4() + (*es)[1]->p4()).Pt();
      }

      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      float ptmumu = -99;

      if (mus->size() >= 2) {
        ptmumu = ((*mus)[0]->p4() + (*mus)[1]->p4()).Pt();
      }

      if (ptee < 0 and ptmumu < 0) { return m_default; }

      return std::max(ptee, ptmumu);
    }
  };


  //____________________________________________________________________________
  class DRmin_y_j : public VarBase<float> {
  public:
    DRmin_y_j() : VarBase("DRmin_y_j") { m_default = -99; }
    ~DRmin_y_j() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *gams = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      double dR2min = 99.0, dR2 = 0.0, eta = 0.0, phi = 0.0;

      for (auto gam : *gams) {
        eta = gam->eta();
        phi = gam->phi();

        for (auto jet : *jets) {
          dR2 = xAOD::P4Helpers::deltaR2(*jet, eta, phi, false);

          if (dR2 < dR2min) { dR2min = dR2; }
        }
      }

      if (dR2min == 99) { return m_default; }

      return sqrt(dR2min);
    }
  };

  //____________________________________________________________________________
  class DRmin_y_j_2 : public VarBase<float> {
  public:
    DRmin_y_j_2() : VarBase("DRmin_y_j_2") { m_default = -99; }
    ~DRmin_y_j_2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *gams = HG::VarHandler::getInstance()->getPhotons(truth);
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      if (gams->size() < 2) { return m_default; }

      if (jets->size() < 2) { return m_default; }

      double dR2min = 99.0, dR2 = 0.0, eta = 0.0, phi = 0.0;

      for (int i = 0; i < 2; i++) {
        auto gam = gams->at(i);
        eta = gam->eta();
        phi = gam->phi();

        for (int j = 0; j < 2; j++) {
          auto jet = jets->at(j) ;
          dR2 = xAOD::P4Helpers::deltaR2(*jet, eta, phi, false);

          if (dR2 < dR2min) { dR2min = dR2; }
        }
      }

      if (dR2min == 99) { return m_default; }

      return sqrt(dR2min);
    }
  };

  //____________________________________________________________________________
  //
  class DR_y_y : public VarBase<float> {
  public:
    DR_y_y() : VarBase("DR_y_y") { m_default = -99; }
    ~DR_y_y() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *gams = HG::VarHandler::getInstance()->getPhotons(truth);

      if (gams->size() < 2)
      { return m_default; }

      return sqrt(xAOD::P4Helpers::deltaR2(*(*gams)[0], *(*gams)[1], false));
    }
  };

  //____________________________________________________________________________
  class Zepp : public VarBase<float> {
  public:
    Zepp() : VarBase("Zepp") { m_default = -99; }
    ~Zepp() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      return ((*ps)[0]->p4() + (*ps)[1]->p4()).Eta() - ((*js)[0]->eta() + (*js)[1]->eta()) / 2.0;
    }
  };

  //____________________________________________________________________________
  class cosTS_yyjj : public VarBase<float> {
  public:
    cosTS_yyjj() : VarBase("cosTS_yyjj") { m_default = -99; }
    ~cosTS_yyjj() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      TLorentzVector vH, vZ, vg, vl1, vl2;

      vg = (*js)[0]->p4() + (*js)[1]->p4();
      vl1 = (*ps)[0]->p4();
      vl2 = (*ps)[1]->p4();
      vZ = (*ps)[0]->p4() + (*ps)[1]->p4();
      vH = vZ + vg;

      TVector3 boost = -vH.BoostVector();
      vH.Boost(boost);
      vZ.Boost(boost);
      vg.Boost(boost);
      vl1.Boost(boost);
      vl2.Boost(boost);

      TLorentzVector q, qbar;
      q.SetPxPyPzE(0, 0, vH.M() / 2, vH.M() / 2);
      qbar.SetPxPyPzE(0, 0, -vH.M() / 2, vH.M() / 2);

      return (q - qbar).Dot(vZ) / (vH.M() * vZ.P());
    }
  };

  //____________________________________________________________________________
  class met_TST : public VarBase<float> {
  public:
    met_TST() : VarBase("met_TST") { m_default = -99; }
    ~met_TST() { }

    float calculateValue(bool truth)
    {
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if ((*mets)["TST"] == nullptr) { return m_default; }

      return (*mets)["TST"]->met();
    }
  };

  //____________________________________________________________________________
  class met_Sig : public VarBase<float> {
  public:
    met_Sig() : VarBase("met_Sig") { m_default = -99; }
    ~met_Sig() { }

    float calculateValue(bool truth)
    {
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if ((*mets)["metSig"] == nullptr) { return m_default; }

      return (*mets)["metSig"]->met();
    }
  };

  //____________________________________________________________________________
  class sumet_TST : public VarBase<float> {
  public:
    sumet_TST() : VarBase("sumet_TST") { m_default = -99; }
    ~sumet_TST() { }

    float calculateValue(bool truth)
    {
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if ((*mets)["TST"] == nullptr) { return m_default; }

      return (*mets)["TST"]->sumet();
    }
  };

  //____________________________________________________________________________
  class phi_TST : public VarBase<float> {
  public:
    phi_TST() : VarBase("phi_TST") { m_default = -99; }
    ~phi_TST() { }

    float calculateValue(bool truth)
    {
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if ((*mets)["TST"] == nullptr) { return m_default; }

      return (*mets)["TST"]->phi();
    }
  };

  // Helper functions to determine had and lep channels for ttH
  inline bool is_hadronictop_preselection(int nlep, int njet) { return nlep == 0 && njet > 2; }
  inline bool is_leptonictop_preselection(int nlep, int njet) { return nlep == 1 && njet > 0; }

  //____________________________________________________________________________
  // Returns a vector of indexes of the selected jets in top candidate
  class idx_jets_recotop1 : public VarBase<std::vector<int>> {
  public:
    idx_jets_recotop1() : VarBase("idx_jets_recotop1") { m_recoOnly = true;  }
    ~idx_jets_recotop1() { }
    std::vector<int> calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      const int njet = jets->size();
      const int nlep = els->size() + mus->size();

      std::vector<int> selected_jets;

      if (is_hadronictop_preselection(nlep, njet)) {

        static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

        if (not bdt_top1.isAvailable(*(*jets)[0])) {
          throw std::runtime_error("Jet decoration bdt_hadtop1 is not available");
        }

        for (unsigned int i = 0; i < jets->size(); i++) {
          auto jet = jets->at(i);

          if (bdt_top1(*jet) > 0) { selected_jets.push_back(i); }
        }
      }

      if (is_leptonictop_preselection(nlep, njet)) {

        static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

        if (not bdt_top1.isAvailable(*(*jets)[0])) {
          throw std::runtime_error("Jet decoration bdt_leptop1 is not available");
        }

        for (unsigned int i = 0; i < jets->size(); i++) {
          auto jet = jets->at(i);

          if (bdt_top1(*jet) > 0) { selected_jets.push_back(i); }
        }
      }

      return selected_jets;
    }
  };

  //____________________________________________________________________________
  // Returns a vector of indexes of the selected jets in top candidate
  class idx_jets_recotop2 : public VarBase<std::vector<int>> {
  public:
    idx_jets_recotop2() : VarBase("idx_jets_recotop2") { m_recoOnly = true; }
    ~idx_jets_recotop2() { }
    std::vector<int> calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      const int njet = jets->size();
      const int nlep = els->size() + mus->size();

      std::vector<int> selected_jets;

      if (is_hadronictop_preselection(nlep, njet - 3)) {

        static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");

        if (not bdt_top2.isAvailable(*(*jets)[0])) {
          throw std::runtime_error("Jet decoration bdt_hadtop2 is not available");
        }

        for (unsigned int i = 0; i < jets->size(); i++) {
          auto jet = jets->at(i);

          if (bdt_top2(*jet) > 0) { selected_jets.push_back(i); }
        }
      }

      if (is_leptonictop_preselection(nlep, njet - 3)) {

        static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop1");

        if (not bdt_top2.isAvailable(*(*jets)[0])) {
          throw std::runtime_error("Jet decoration bdt_hadtop1 is not available");
        }

        for (unsigned int i = 0; i < jets->size(); i++) {
          auto jet = jets->at(i);

          if (bdt_top2(*jet) > 0) { selected_jets.push_back(i); }
        }
      }

      return selected_jets;
    }
  };

  //____________________________________________________________________________
  class score_recotop1 : public VarBase<float> {
  public:
    score_recotop1() : VarBase("score_recotop1") { m_default = -1; m_recoOnly = true; }
    ~score_recotop1() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      const int njet = jets->size();
      const int nlep = els->size() + mus->size();

      double bdt_t1 = m_default;

      if (is_hadronictop_preselection(nlep, njet)) {

        static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

        if (not bdt_top1.isAvailable(*(*jets)[0])) {
          throw std::runtime_error("Jet decoration bdt_hadtop1 is not available");
        }

        for (auto jet : *jets) {
          const double bdt_t1_this_jet = bdt_top1(*jet);

          if (bdt_t1_this_jet > 0) {
            bdt_t1 = bdt_t1_this_jet;
            break;
          }
        }
      }

      if (is_leptonictop_preselection(nlep, njet)) {

        static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

        if (not bdt_top1.isAvailable(*(*jets)[0])) {
          throw std::runtime_error("Jet decoration bdt_leptop1 is not available");
        }

        for (auto jet : *jets) {
          const double bdt_t1_this_jet = bdt_top1(*jet);

          if (bdt_t1_this_jet > 0) {
            bdt_t1 = bdt_t1_this_jet;
            break;
          }
        }
      }

      return bdt_t1;
    }
  };

  //____________________________________________________________________________
  class score_recotop2 : public VarBase<float> {
  public:
    score_recotop2() : VarBase("score_recotop2") { m_default = -1; m_recoOnly = true; }
    ~score_recotop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      const int njet = jets->size();
      const int nlep = els->size() + mus->size();

      double bdt_t2 = m_default;

      if (is_hadronictop_preselection(nlep, njet - 3)) {
        static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");

        if (not bdt_top2.isAvailable(*(*jets)[0])) {
          throw std::runtime_error("Jet decoration bdt_hadtop2 is not available");
        }

        for (auto jet : *jets) {
          const double bdt_t2_this_jet = bdt_top2(*jet);

          if (bdt_t2_this_jet > 0) {
            bdt_t2 = bdt_t2_this_jet;
            break;
          }
        }
      }

      if (is_leptonictop_preselection(nlep, njet - 3)) {

        static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop1");

        if (not bdt_top2.isAvailable(*(*jets)[0])) {
          throw std::runtime_error("Jet decoration bdt_hadtop1 is not available");
        }

        for (auto jet : *jets) {
          const double bdt_t2_this_jet = bdt_top2(*jet);

          if (bdt_t2_this_jet > 0) {
            bdt_t2 = bdt_t2_this_jet;
            break;
          }
        }
      }

      return bdt_t2;
    }
  };

  //____________________________________________________________________________
  class index_hadtop1 : public VarBase<std::vector<int>> {
  public:
    index_hadtop1() : VarBase("index_hadtop1") { }
    ~index_hadtop1() { }

    std::vector<int> calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      std::vector<int> hadjets;
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      for (unsigned int i = 0; i < jets->size(); i++) {
        auto jet = jets->at(i);

        if (bdt_top1.isAvailable(*jet)) {
          if (bdt_top1(*jet) > 0) {
            hadjets.push_back(i);
          }
        }
      }

      return hadjets;
    }
  };

  //____________________________________________________________________________
  class score_hadtop1 : public VarBase<float> {
  public:
    score_hadtop1() : VarBase("score_hadtop1") { m_default = -1; }
    ~score_hadtop1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");
      double bdt_t1 = m_default;

      for (auto jet : *jets) {
        if (bdt_top1.isAvailable(*jet)) {
          if (bdt_top1(*jet) > 0) {
            bdt_t1 = bdt_top1(*jet);
            break;
          }
        }
      }

      return bdt_t1;
    }
  };

  // Helper function for retrieving reco top four-vector
  inline TLorentzVector sumpt_if(const xAOD::IParticleContainer &jets,
                                 std::function<bool(const xAOD::IParticle &)> &&predicate)
  {
    TLorentzVector vt;

    for (const auto jet : jets) {
      if (predicate(*jet)) { vt += jet->p4(); }
    }

    return vt;
  }

  // Helper functions for calculating the leptonic W four-vector
  double W_m_trans(TLorentzVector vec, double px, double py);
  double py_nu(double px, TLorentzVector lep_vec, bool plus);
  std::vector<double> pxpy_corr(TLorentzVector lep_vec, double met_x, double met_y);
  TLorentzVector leptonic_W(TLorentzVector lep_vec, double met_x, double met_y, double met);

  inline TLorentzVector lepW(const xAOD::IParticleContainer *els,
                             const xAOD::IParticleContainer *mus,
                             const xAOD::MissingETContainer *mets)
  {
    if (els->size() + mus->size() != 1) { return TLorentzVector(); }

    TLorentzVector vl;

    if (!mus->empty()) {
      const auto mu = (*mus)[0];
      vl.SetPtEtaPhiM(mu->pt() * HG::invGeV, mu->eta(), mu->phi(), mu->m() * HG::invGeV);
    } else if (!els->empty()) {
      const auto el = (*els)[0];
      vl.SetPtEtaPhiM(el->pt() * HG::invGeV, el->eta(), el->phi(), el->m() * HG::invGeV);
    }

    const xAOD::MissingET *met = (*mets)["TST"];
    const double m_met =  met->met() * HG::invGeV;
    const double m_met_x = met->mpx() * HG::invGeV;
    const double m_met_y = met->mpy() * HG::invGeV;

    return leptonic_W(vl, m_met_x, m_met_y, m_met);
  }

  inline TLorentzVector p4_recotop1(const xAOD::IParticleContainer *jets,
                                    const xAOD::IParticleContainer *els,
                                    const xAOD::IParticleContainer *mus,
                                    const xAOD::MissingETContainer *mets)
  {

    const int njet = jets->size();
    const int nlep = els->size() + mus->size();

    if (is_hadronictop_preselection(nlep, njet)) {

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      if (not bdt_top1.isAvailable(*(*jets)[0])) {
        throw std::runtime_error("Jet decoration bdt_hadtop1 is not available");
      }

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1(jet) > 0);
      }) * HG::invGeV;

      return vt;
    }

    if (is_leptonictop_preselection(nlep, njet)) {

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      if (not bdt_top1.isAvailable(*(*jets)[0])) {
        throw std::runtime_error("Jet decoration bdt_leptop1 is not available");
      }

      TLorentzVector W1 = lepW(els, mus, mets);
      TLorentzVector k1 = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1(jet) > 0);
      }) * HG::invGeV;

      return (k1 + W1);
    }

    return TLorentzVector();
  }

  inline TLorentzVector p4_recotop2(const xAOD::IParticleContainer *jets,
                                    const xAOD::IParticleContainer *els,
                                    const xAOD::IParticleContainer *mus)
  {

    const int njet = jets->size();
    const int nlep = els->size() + mus->size();

    if (is_hadronictop_preselection(nlep, njet - 3)) {

      static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");

      if (not bdt_top2.isAvailable(*(*jets)[0])) {
        throw std::runtime_error("Jet decoration bdt_hadtop2 is not available");
      }

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top2(jet) > 0);
      }) * HG::invGeV;

      return vt;
    }

    if (is_leptonictop_preselection(nlep, njet - 3)) {

      static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop1");

      if (not bdt_top2.isAvailable(*(*jets)[0])) {
        throw std::runtime_error("Jet decoration bdt_hadtop1 is not available");
      }

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top2(jet) > 0);
      }) * HG::invGeV;

      return vt;
    }

    return TLorentzVector();
  }

  inline TLorentzVector p4_hybridtop2(const xAOD::IParticleContainer *jets,
                                      const xAOD::IParticleContainer *els,
                                      const xAOD::IParticleContainer *mus)
  {


    // Fully reconstruct second top if possible
    double score_top2 = score_recotop2().calculateValue(0);

    // Return the fully reconstructed top if it exists
    if (score_top2 > 0) {
      return p4_recotop2(jets, els, mus);
    }

    const int njet = jets->size();
    const int nlep = els->size() + mus->size();

    if (is_hadronictop_preselection(nlep, njet - 1)) {

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      if (not bdt_top1.isAvailable(*(*jets)[0])) {
        throw std::runtime_error("Jet decoration bdt_hadtop1 is not available");
      }

      // Skip jets used to build the first top
      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      return vEE;
    }

    if (is_leptonictop_preselection(nlep, njet - 1)) {

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      if (not bdt_top1.isAvailable(*(*jets)[0])) {
        throw std::runtime_error("Jet decoration bdt_leptop1 is not available");
      }

      // Skip jets used to build the first top
      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      return vEE;
    }

    return TLorentzVector();
  }

  //____________________________________________________________________________
  class pT_recotop1 : public VarBase<float> {
  public:
    pT_recotop1() : VarBase("pT_recotop1") { m_default = -99; m_recoOnly = true; }
    ~pT_recotop1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if ((*mets)["TST"] == nullptr) { return m_default; }

      TLorentzVector vt = p4_recotop1(jets, els, mus, mets);

      if (vt.Pt() > 0) { return vt.Pt(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class eta_recotop1 : public VarBase<float> {
  public:
    eta_recotop1() : VarBase("eta_recotop1") { m_default = -99; m_recoOnly = true; }
    ~eta_recotop1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if ((*mets)["TST"] == nullptr) { return m_default; }

      TLorentzVector vt = p4_recotop1(jets, els, mus, mets);

      if (vt.Pt() > 0) { return vt.Eta(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class phi_recotop1 : public VarBase<float> {
  public:
    phi_recotop1() : VarBase("phi_recotop1") { m_default = -99; m_recoOnly = true; }
    ~phi_recotop1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if ((*mets)["TST"] == nullptr) { return m_default; }

      TLorentzVector vt = p4_recotop1(jets, els, mus, mets);

      if (vt.Pt() > 0) { return vt.Phi(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class m_recotop1 : public VarBase<float> {
  public:
    m_recotop1() : VarBase("m_recotop1") { m_default = -99; m_recoOnly = true; }
    ~m_recotop1() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if ((*mets)["TST"] == nullptr) { return m_default; }

      TLorentzVector vt = p4_recotop1(jets, els, mus, mets);

      if (vt.Pt() > 0) { return vt.M(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class pT_recotop2 : public VarBase<float> {
  public:
    pT_recotop2() : VarBase("pT_recotop2") { m_default = -99; m_recoOnly = true; }
    ~pT_recotop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      TLorentzVector vt = p4_recotop2(jets, els, mus);

      if (vt.Pt() > 0) { return vt.Pt(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class eta_recotop2 : public VarBase<float> {
  public:
    eta_recotop2() : VarBase("eta_recotop2") { m_default = -99; m_recoOnly = true; }
    ~eta_recotop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      TLorentzVector vt = p4_recotop2(jets, els, mus);

      if (vt.Pt() > 0) { return vt.Eta(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class phi_recotop2 : public VarBase<float> {
  public:
    phi_recotop2() : VarBase("phi_recotop2") { m_default = -99; m_recoOnly = true; }
    ~phi_recotop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      TLorentzVector vt = p4_recotop2(jets, els, mus);

      if (vt.Pt() > 0) { return vt.Phi(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class m_recotop2 : public VarBase<float> {
  public:
    m_recotop2() : VarBase("m_recotop2") { m_default = -99; m_recoOnly = true; }
    ~m_recotop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      TLorentzVector vt = p4_recotop2(jets, els, mus);

      if (vt.Pt() > 0) { return vt.M(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class pT_hybridtop2 : public VarBase<float> {
  public:
    pT_hybridtop2() : VarBase("pT_hybridtop2") { m_default = -99; m_recoOnly = true; }
    ~pT_hybridtop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      TLorentzVector vt = p4_hybridtop2(jets, els, mus);

      if (vt.Pt() > 0) { return vt.Pt(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class eta_hybridtop2 : public VarBase<float> {
  public:
    eta_hybridtop2() : VarBase("eta_hybridtop2") { m_default = -99; m_recoOnly = true; }
    ~eta_hybridtop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      TLorentzVector vt = p4_hybridtop2(jets, els, mus);

      if (vt.Pt() > 0) { return vt.Eta(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class phi_hybridtop2 : public VarBase<float> {
  public:
    phi_hybridtop2() : VarBase("phi_hybridtop2") { m_default = -99; m_recoOnly = true; }
    ~phi_hybridtop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      TLorentzVector vt = p4_hybridtop2(jets, els, mus);

      if (vt.Pt() > 0) { return vt.Phi(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class m_hybridtop2 : public VarBase<float> {
  public:
    m_hybridtop2() : VarBase("m_hybridtop2") { m_default = -99; m_recoOnly = true; }
    ~m_hybridtop2() { }

    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);

      TLorentzVector vt = p4_hybridtop2(jets, els, mus);

      if (vt.Pt() > 0) { return vt.M(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class pT_hadtop1 : public VarBase<float> {
  public:
    pT_hadtop1() : VarBase("pT_hadtop1") { m_default = -99; }
    ~pT_hadtop1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) > 0);
      }) * HG::invGeV;


      if (vt.Pt() > 0) { return vt.Pt(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class eta_hadtop1 : public VarBase<float> {
  public:
    eta_hadtop1() : VarBase("eta_hadtop1") { m_default = -99; }
    ~eta_hadtop1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) > 0);
      }) * HG::invGeV;

      if (vt.Pt() > 0) { return vt.Eta(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class phi_hadtop1 : public VarBase<float> {
  public:
    phi_hadtop1() : VarBase("phi_hadtop1") { m_default = -99; }
    ~phi_hadtop1() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) > 0);
      }) * HG::invGeV;

      if (vt.Pt() > 0) { return vt.Phi(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class m_hadtop1 : public VarBase<float> {
  public:
    m_hadtop1() : VarBase("m_hadtop1") { m_default = -99; }
    ~m_hadtop1() { }
    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) > 0);
      }) * HG::invGeV;;

      if (vt.Pt() > 0) { return vt.M(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class index_hadtop2 : public VarBase<std::vector<int>> {
  public:
    index_hadtop2() : VarBase("index_hadtop2") { }
    ~index_hadtop2() { }

    std::vector<int> calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      std::vector<int> hadjets;
      static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");

      for (unsigned int i = 0; i < jets->size(); i++) {
        auto jet = jets->at(i);

        if (bdt_top2.isAvailable(*jet)) {
          if (bdt_top2(*jet) > 0) {
            hadjets.push_back(i);
          }
        }
      }

      return hadjets;
    }
  };

  //____________________________________________________________________________
  class score_hadtop2 : public VarBase<float> {
  public:
    score_hadtop2() : VarBase("score_hadtop2") { m_default = -1; }
    ~score_hadtop2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);

      static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");
      double bdt_t2 = m_default;

      for (auto jet : *jets) {
        if (bdt_top2.isAvailable(*jet)) {
          if (bdt_top2(*jet) > 0) {
            bdt_t2 = bdt_top2(*jet);
            break;
          }
        }
      }

      return bdt_t2;
    }
  };

  //____________________________________________________________________________
  class pT_hadtop2 : public VarBase<float> {
  public:
    pT_hadtop2() : VarBase("pT_hadtop2") { m_default = -99; }
    ~pT_hadtop2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top2.isAvailable(jet)) and (bdt_top2(jet) > 0);
      }) * HG::invGeV;

      if (vt.Pt() > 0) { return vt.Pt(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class eta_hadtop2 : public VarBase<float> {
  public:
    eta_hadtop2() : VarBase("eta_hadtop2") { m_default = -99; }
    ~eta_hadtop2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top2.isAvailable(jet)) and (bdt_top2(jet) > 0);
      }) * HG::invGeV;

      if (vt.Pt() > 0) { return vt.Eta(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class phi_hadtop2 : public VarBase<float> {
  public:
    phi_hadtop2() : VarBase("phi_hadtop2") { m_default = -99; }
    ~phi_hadtop2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top2.isAvailable(jet)) and (bdt_top2(jet) > 0);
      }) * HG::invGeV;

      if (vt.Pt() > 0) { return vt.Phi(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class m_hadtop2 : public VarBase<float> {
  public:
    m_hadtop2() : VarBase("m_hadtop2") { m_default = -99; }
    ~m_hadtop2() { }

    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top2("bdt_hadtop2");

      TLorentzVector vt = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top2.isAvailable(jet)) and (bdt_top2(jet) > 0);
      }) * HG::invGeV;

      if (vt.Pt() > 0) { return vt.M(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class pT_had_hybrid_top2 : public VarBase<float> {
  public:
    pT_had_hybrid_top2() : VarBase("pT_had_hybrid_top2") { m_default = -99; }
    ~pT_had_hybrid_top2() { }

    float calculateValue(bool truth)
    {
      // If there are enough jets to fully reconstruct the second top
      if (pT_hadtop2().calculateValue(truth) != m_default) {
        return pT_hadtop2().calculateValue(truth);
      }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      if (vEE.Pt() > 0) { return vEE.Pt(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class eta_had_hybrid_top2 : public VarBase<float> {
  public:
    eta_had_hybrid_top2() : VarBase("eta_had_hybrid_top2") { m_default = -99; }
    ~eta_had_hybrid_top2() { }

    float calculateValue(bool truth)
    {
      // If there are enough jets to fully reconstruct the second top
      if (eta_hadtop2().calculateValue(truth) != m_default) {
        return eta_hadtop2().calculateValue(truth);
      }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      if (vEE.Pt() > 0) { return vEE.Eta(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class phi_had_hybrid_top2 : public VarBase<float> {
  public:
    phi_had_hybrid_top2() : VarBase("phi_had_hybrid_top2") { m_default = -99; }
    ~phi_had_hybrid_top2() { }

    float calculateValue(bool truth)
    {
      // If there are enough jets to fully reconstruct the second top
      if (phi_hadtop2().calculateValue(truth) != m_default) {
        return phi_hadtop2().calculateValue(truth);
      }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      if (vEE.Pt() > 0) { return vEE.Phi(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class m_had_hybrid_top2 : public VarBase<float> {
  public:
    m_had_hybrid_top2() : VarBase("m_had_hybrid_top2") { m_default = -99; }
    ~m_had_hybrid_top2() { }

    float calculateValue(bool truth)
    {
      // If there are enough jets to fully reconstruct the second top
      if (m_hadtop2().calculateValue(truth) != m_default) {
        return m_hadtop2().calculateValue(truth);
      }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_hadtop1");

      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      if (vEE.Pt() > 0) { return vEE.M(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class index_leptop1 : public VarBase<std::vector<int>> {
  public:
    index_leptop1() : VarBase("index_leptop1") { }
    ~index_leptop1() { }

    std::vector<int> calculateValue(bool truth)
    {

      std::vector<int> lepjets;

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() + els->size() != 1) { return lepjets; }

      if (!truth && (*mets)["TST"] == nullptr) { return lepjets; }

      if (truth && (*mets)["NonInt"] == nullptr) { return lepjets; }

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      for (unsigned int i = 0; i < jets->size(); i++) {
        auto jet = jets->at(i) ;

        if (bdt_top1.isAvailable(*jet)) {
          if (bdt_top1(*jet) > 0) {
            lepjets.push_back(i);
            break;
          }
        }
      }

      return lepjets;
    }
  };

  //____________________________________________________________________________
  class score_leptop1 : public VarBase<float> {
  public:
    score_leptop1() : VarBase("score_leptop1") { m_default = -1; }
    ~score_leptop1() { }
    float calculateValue(bool truth)
    {

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() + els->size() != 1) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");
      double bdt_t1 = m_default;

      for (auto jet : *jets) {
        if (bdt_top1.isAvailable(*jet)) {
          if (bdt_top1(*jet) > 0) {
            bdt_t1 = bdt_top1(*jet);
            break;
          }
        }
      }

      return bdt_t1;
    }
  };

  //____________________________________________________________________________
  class pT_leptop1 : public VarBase<float> {
  public:
    pT_leptop1() : VarBase("pT_leptop1") { m_default = -99; }
    ~pT_leptop1() { }
    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() + els->size() != 1) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      TLorentzVector W1 = lepW(els, mus, mets);

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      TLorentzVector k1 = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) > 0);
      }) * HG::invGeV;

      if (k1.Pt() > 0) { return (k1 + W1).Pt(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class eta_leptop1 : public VarBase<float> {
  public:
    eta_leptop1() : VarBase("eta_leptop1") { m_default = -99; }
    ~eta_leptop1() { }
    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() + els->size() != 1) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      TLorentzVector W1 = lepW(els, mus, mets);

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      TLorentzVector k1 = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) > 0);
      }) * HG::invGeV;

      if (k1.Pt() > 0) { return (k1 + W1).Eta(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class phi_leptop1 : public VarBase<float> {
  public:
    phi_leptop1() : VarBase("phi_leptop1") { m_default = -99; }
    ~phi_leptop1() { }
    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() + els->size() != 1) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      TLorentzVector W1 = lepW(els, mus, mets);

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      TLorentzVector k1 = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) > 0);
      }) * HG::invGeV;

      if (k1.Pt() > 0) { return (k1 + W1).Phi(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class m_leptop1 : public VarBase<float> {
  public:
    m_leptop1() : VarBase("m_leptop1") { m_default = -99; }
    ~m_leptop1() { }
    float calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      const xAOD::IParticleContainer *mus = HG::VarHandler::getInstance()->getMuons(truth);
      const xAOD::IParticleContainer *els = HG::VarHandler::getInstance()->getElectrons(truth);
      const xAOD::MissingETContainer *mets = HG::VarHandler::getInstance()->getMissingETs(truth);

      if (mus->size() + els->size() != 1) { return m_default; }

      if (!truth && (*mets)["TST"] == nullptr) { return m_default; }

      if (truth && (*mets)["NonInt"] == nullptr) { return m_default; }

      TLorentzVector W1 = lepW(els, mus, mets);

      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      TLorentzVector k1 = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) > 0);
      }) * HG::invGeV;

      if (k1.Pt() > 0) { return (k1 + W1).M(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class pT_lep_hybrid_top2 : public VarBase<float> {
  public:
    pT_lep_hybrid_top2() : VarBase("pT_lep_hybrid_top2") { m_default = -99; }
    ~pT_lep_hybrid_top2() { }

    float calculateValue(bool truth)
    {
      // If there are enough jets to fully reconstruct the second top
      if (pT_hadtop1().calculateValue(truth) != m_default) {
        return pT_hadtop1().calculateValue(truth);
      }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      if (vEE.Pt() > 0) { return vEE.Pt(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class eta_lep_hybrid_top2 : public VarBase<float> {
  public:
    eta_lep_hybrid_top2() : VarBase("eta_lep_hybrid_top2") { m_default = -99; }
    ~eta_lep_hybrid_top2() { }

    float calculateValue(bool truth)
    {
      // If there are enough jets to fully reconstruct the second top
      if (eta_hadtop1().calculateValue(truth) != m_default) {
        return eta_hadtop1().calculateValue(truth);
      }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      if (vEE.Pt() > 0) { return vEE.Eta(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class phi_lep_hybrid_top2 : public VarBase<float> {
  public:
    phi_lep_hybrid_top2() : VarBase("phi_lep_hybrid_top2") { m_default = -99; }
    ~phi_lep_hybrid_top2() { }

    float calculateValue(bool truth)
    {
      // If there are enough jets to fully reconstruct the second top
      if (phi_hadtop1().calculateValue(truth) != m_default) {
        return phi_hadtop1().calculateValue(truth);
      }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      if (vEE.Pt() > 0) { return vEE.Phi(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class m_lep_hybrid_top2 : public VarBase<float> {
  public:
    m_lep_hybrid_top2() : VarBase("m_lep_hybrid_top2") { m_default = -99; }
    ~m_lep_hybrid_top2() { }

    float calculateValue(bool truth)
    {
      // If there are enough jets to fully reconstruct the second top
      if (m_hadtop1().calculateValue(truth) != m_default) {
        return m_hadtop1().calculateValue(truth);
      }

      const xAOD::IParticleContainer *jets = HG::VarHandler::getInstance()->getJets(truth);
      static SG::AuxElement::ConstAccessor<float> bdt_top1("bdt_leptop1");

      TLorentzVector vEE = sumpt_if(*jets, [](const xAOD::IParticle & jet) {
        return (bdt_top1.isAvailable(jet)) and (bdt_top1(jet) <= 0);
      }) * HG::invGeV;

      if (vEE.Pt() > 0) { return vEE.M(); }

      return m_default;
    }
  };

  //____________________________________________________________________________
  class isPassedBasic : public VarBase<char> {
  public:
    isPassedBasic() : VarBase("isPassedBasic") { m_default = false; }
    ~isPassedBasic() { }
  };

  class isDalitzEvent : public VarBase<char> {
  public:
    isDalitzEvent() : VarBase("isDalitz") { m_default = false; }
    ~isDalitzEvent() { }
  };


  //____________________________________________________________________________
  class isPassed : public VarBase<char> {
  public:
    isPassed() : VarBase("isPassed") { m_default = false; }
    ~isPassed() { }
  };

  //____________________________________________________________________________
  class isPassedJetEventClean : public VarBase<char> {
  public:
    isPassedJetEventClean() : VarBase("isPassedJetEventClean") { m_default = false; }
    ~isPassedJetEventClean() { }
  };

  //____________________________________________________________________________
  class isFiducial : public VarBase<char> {
  public:
    isFiducial() : VarBase("isFiducial") { m_default = false; m_truthOnly = true; }
    ~isFiducial() { }
  };

  //____________________________________________________________________________
  class isFiducialKinOnly : public VarBase<char> {
  public:
    isFiducialKinOnly() : VarBase("isFiducialKinOnly") { m_default = false; m_truthOnly = true; }
    ~isFiducialKinOnly() { }
  };

  //____________________________________________________________________________
  class cutFlow : public VarBase<int> {
  public:
    cutFlow() : VarBase("cutFlow") { m_default = -99; m_recoOnly = true; }
    ~cutFlow() { }
  };

  //____________________________________________________________________________
  class pileupWeight : public VarBase<float> {
  public:
    pileupWeight() : VarBase("pileupWeight") { m_default = 1.0; m_recoOnly = true; }
    ~pileupWeight() { }
  };

  //____________________________________________________________________________
  class vertexWeight : public VarBase<float> {
  public:
    vertexWeight() : VarBase("vertexWeight") { m_default = 1.0; m_recoOnly = true; }
    ~vertexWeight() { }
  };

  //____________________________________________________________________________
  class weightInitial : public VarBase<float> {
  public:
    weightInitial() : VarBase("weightInitial") { m_default = 1.0; m_recoOnly = true; }
    ~weightInitial() { }
  };

  //____________________________________________________________________________
  class weightSF : public VarBase<float> {
  public:
    weightSF() : VarBase("weightSF") { m_default = 1.0; m_recoOnly = true; }
    ~weightSF() { }
  };

  //____________________________________________________________________________
  class weightTrigSF : public VarBase<float> {
  public:
    weightTrigSF() : VarBase("weightTrigSF") { m_default = 1.0; m_recoOnly = true; }
    ~weightTrigSF() { }
  };

  //____________________________________________________________________________
  class weight : public VarBase<float> {
  public:
    weight() : VarBase("weight") { m_default = 1.0; m_recoOnly = true; }
    ~weight() { }
  };

  //____________________________________________________________________________
  class weightCatCoup_SFMoriond2017 : public VarBase<float> {
  public:
    weightCatCoup_SFMoriond2017() : VarBase("weightCatCoup_SFMoriond2017") { m_default = 1.0; m_recoOnly = true; }
    ~weightCatCoup_SFMoriond2017() { }
  };
  //____________________________________________________________________________
  class weightCatCoup_Moriond2017 : public VarBase<float> {
  public:
    weightCatCoup_Moriond2017() : VarBase("weightCatCoup_Moriond2017") { m_default = 1.0; m_recoOnly = true; }
    ~weightCatCoup_Moriond2017() { }
  };
  //____________________________________________________________________________
  class catCoup_Moriond2017 : public VarBase<int> {
  public:
    catCoup_Moriond2017() : VarBase("catCoup_Moriond2017") { m_default = -99; m_recoOnly = true; }
    ~catCoup_Moriond2017() { }
  };

  //____________________________________________________________________________
  class weightCatCoup_SFMoriond2017BDT_qqH2jet : public VarBase<float> {
  public:
    weightCatCoup_SFMoriond2017BDT_qqH2jet() : VarBase("weightCatCoup_SFMoriond2017BDT_qqH2jet") { m_default = 1.0; m_recoOnly = true; }
    ~weightCatCoup_SFMoriond2017BDT_qqH2jet() { }
  };
  //____________________________________________________________________________
  class weightCatCoup_Moriond2017BDT_qqH2jet : public VarBase<float> {
  public:
    weightCatCoup_Moriond2017BDT_qqH2jet() : VarBase("weightCatCoup_Moriond2017BDT_qqH2jet") { m_default = 1.0; m_recoOnly = true; }
    ~weightCatCoup_Moriond2017BDT_qqH2jet() { }
  };
  //____________________________________________________________________________
  class catCoup_Moriond2017BDT_qqH2jet : public VarBase<int> {
  public:
    catCoup_Moriond2017BDT_qqH2jet() : VarBase("catCoup_Moriond2017BDT_qqH2jet") { m_default = -99; m_recoOnly = true; }
    ~catCoup_Moriond2017BDT_qqH2jet() { }
  };

  //____________________________________________________________________________
  class weightCatCoup_SFMoriond2017BDT : public VarBase<float> {
  public:
    weightCatCoup_SFMoriond2017BDT() : VarBase("weightCatCoup_SFMoriond2017BDT") { m_default = 1.0; m_recoOnly = true; }
    ~weightCatCoup_SFMoriond2017BDT() { }
  };
  //____________________________________________________________________________
  class weightCatCoup_Moriond2017BDT : public VarBase<float> {
  public:
    weightCatCoup_Moriond2017BDT() : VarBase("weightCatCoup_Moriond2017BDT") { m_default = 1.0; m_recoOnly = true; }
    ~weightCatCoup_Moriond2017BDT() { }
  };
  //____________________________________________________________________________
  class catCoup_Moriond2017BDT : public VarBase<int> {
  public:
    catCoup_Moriond2017BDT() : VarBase("catCoup_Moriond2017BDT") { m_default = -99; m_recoOnly = true; }
    ~catCoup_Moriond2017BDT() { }
  };

  //____________________________________________________________________________
  struct catCoup_GlobalICHEP : public VarBase<int> {
    catCoup_GlobalICHEP() : VarBase("catCoup_GlobalICHEP") { m_default = -99; m_recoOnly = true; }
  };

  struct weightCatCoup_SFGlobalICHEP : public VarBase<float> {
    weightCatCoup_SFGlobalICHEP() : VarBase("weightCatCoup_SFGlobalICHEP") { m_default = 1.; m_recoOnly = true; }
  };

  struct weightCatCoup_GlobalICHEP : public VarBase<float> {
    weightCatCoup_GlobalICHEP() : VarBase("weightCatCoup_GlobalICHEP") { m_default = 1.; m_recoOnly = true; }
  };

  struct scoreBinaryCatCoup_GlobalICHEP : public VarBase<float> {
    scoreBinaryCatCoup_GlobalICHEP() : VarBase("scoreBinaryCatCoup_GlobalICHEP") { m_default = -999; m_recoOnly = true; }
  };

  struct multiClassCatCoup_GlobalICHEP : public VarBase<int> {
    multiClassCatCoup_GlobalICHEP() : VarBase("multiClassCatCoup_GlobalICHEP") { m_default = -999; m_recoOnly = true; }
  };

  //____________________________________________________________________________
  struct catCoup_HybridICHEP : public VarBase<int> {
    catCoup_HybridICHEP() : VarBase("catCoup_HybridICHEP") { m_default = -99; m_recoOnly = true; }
  };

  struct weightCatCoup_SFHybridICHEP : public VarBase<float> {
    weightCatCoup_SFHybridICHEP() : VarBase("weightCatCoup_SFHybridICHEP") { m_default = 1.; m_recoOnly = true; }
  };

  struct weightCatCoup_HybridICHEP : public VarBase<float> {
    weightCatCoup_HybridICHEP() : VarBase("weightCatCoup_HybridICHEP") { m_default = 1.; m_recoOnly = true; }
  };

  struct scoreBinaryCatCoup_HybridICHEP : public VarBase<float> {
    scoreBinaryCatCoup_HybridICHEP() : VarBase("scoreBinaryCatCoup_HybridICHEP") { m_default = -999; m_recoOnly = true; }
  };

  struct multiClassCatCoup_HybridICHEP : public VarBase<int> {
    multiClassCatCoup_HybridICHEP() : VarBase("multiClassCatCoup_HybridICHEP") { m_default = -999; m_recoOnly = true; }
  };


  //____________________________________________________________________________
  class weightCatCoup_SFMoriond2017BDTlep : public VarBase<float> {
  public:
    weightCatCoup_SFMoriond2017BDTlep() : VarBase("weightCatCoup_SFMoriond2017BDTlep") { m_default = 1.0; m_recoOnly = true; }
    ~weightCatCoup_SFMoriond2017BDTlep() { }
  };
  //____________________________________________________________________________
  class weightCatCoup_Moriond2017BDTlep : public VarBase<float> {
  public:
    weightCatCoup_Moriond2017BDTlep() : VarBase("weightCatCoup_Moriond2017BDTlep") { m_default = 1.0; m_recoOnly = true; }
    ~weightCatCoup_Moriond2017BDTlep() { }
  };
  //____________________________________________________________________________
  class catCoup_Moriond2017BDTlep : public VarBase<int> {
  public:
    catCoup_Moriond2017BDTlep() : VarBase("catCoup_Moriond2017BDTlep") { m_default = -99; m_recoOnly = true; }
    ~catCoup_Moriond2017BDTlep() { }
  };

  //____________________________________________________________________________
  class catMass_Run1 : public VarBase<int> {
  public:
    catMass_Run1() : VarBase("catMass_Run1") { m_default = -99; m_recoOnly = true; }
    ~catMass_Run1() { }
  };

  //____________________________________________________________________________
  class catMass_eta : public VarBase<int> {
  public:
    catMass_eta() : VarBase("catMass_eta") { m_default = -99; m_recoOnly = true; }
    ~catMass_eta() { }
  };

  //____________________________________________________________________________
  class catMass_pT : public VarBase<int> {
  public:
    catMass_pT() : VarBase("catMass_pT") { m_default = -99; m_recoOnly = true; }
    ~catMass_pT() { }
  };

  //____________________________________________________________________________
  class catMass_conv : public VarBase<int> {
  public:
    catMass_conv() : VarBase("catMass_conv") { m_default = -99; m_recoOnly = true; }
    ~catMass_conv() { }
  };

  //____________________________________________________________________________
  class catMass_mu : public VarBase<int> {
  public:
    catMass_mu() : VarBase("catMass_mu") { m_default = -99; m_recoOnly = true; }
    ~catMass_mu() { }
  };

  //____________________________________________________________________________
  class catXS_VBF : public VarBase<char> {
  public:
    catXS_VBF() : VarBase("catXS_VBF") { m_default = false; }
    ~catXS_VBF() { }

    char calculateValue(bool truth)
    {
      const xAOD::IParticleContainer *js = HG::VarHandler::getInstance()->getJets(truth);

      if (js->size() < 2)
      { return m_default; }

      if ((*js)[1]->pt() < 30.0 * HG::GeV)
      { return m_default; }

      if (((*js)[0]->p4() + (*js)[1]->p4()).M() < 400000.0)
      { return m_default; }

      if (fabs((*js)[0]->rapidity() - (*js)[1]->rapidity()) < 2.8)
      { return m_default; }

      const xAOD::IParticleContainer *ps = HG::VarHandler::getInstance()->getPhotons(truth);

      if (ps->size() < 2)
      { return m_default; }

      if (fabs(((*ps)[0]->p4() + (*ps)[1]->p4()).DeltaPhi((*js)[0]->p4() + (*js)[1]->p4())) < 2.6)
      { return m_default; }

      return true;
    }
  };

  //____________________________________________________________________________
  class numberOfPrimaryVertices : public VarBase<int> {
  public:
    numberOfPrimaryVertices() : VarBase("numberOfPrimaryVertices") { m_default = -99; m_recoOnly = true; }
    ~numberOfPrimaryVertices() { }
  };

  //____________________________________________________________________________
  class selectedVertexSumPt2 : public VarBase<float> {
  public:
    selectedVertexSumPt2() : VarBase("selectedVertexSumPt2") { m_default = -99; m_recoOnly = true; }
    ~selectedVertexSumPt2() { }
  };

  //____________________________________________________________________________
  class selectedVertexZ : public VarBase<float> {
  public:
    selectedVertexZ() : VarBase("selectedVertexZ") { m_default = -999; m_recoOnly = true; }
    ~selectedVertexZ() { }
  };

  //____________________________________________________________________________
  class selectedVertexPhi : public VarBase<float> {
  public:
    selectedVertexPhi() : VarBase("selectedVertexPhi") { m_default = -99; m_recoOnly = true; }
    ~selectedVertexPhi() { }
  };

  //____________________________________________________________________________
  class hardestVertexSumPt2 : public VarBase<float> {
  public:
    hardestVertexSumPt2() : VarBase("hardestVertexSumPt2") { m_default = -99; m_recoOnly = true; }
    ~hardestVertexSumPt2() { }
  };

  //____________________________________________________________________________
  class hardestVertexZ : public VarBase<float> {
  public:
    hardestVertexZ() : VarBase("hardestVertexZ") { m_default = -999; m_recoOnly = true; }
    ~hardestVertexZ() { }
  };

  //____________________________________________________________________________
  class hardestVertexPhi : public VarBase<float> {
  public:
    hardestVertexPhi() : VarBase("hardestVertexPhi") { m_default = -99; m_recoOnly = true; }
    ~hardestVertexPhi() { }
  };

  //____________________________________________________________________________
  class pileupVertexSumPt2 : public VarBase<float> {
  public:
    pileupVertexSumPt2() : VarBase("pileupVertexSumPt2") { m_default = -99; m_recoOnly = true; }
    ~pileupVertexSumPt2() { }
  };

  //____________________________________________________________________________
  class pileupVertexZ : public VarBase<float> {
  public:
    pileupVertexZ() : VarBase("pileupVertexZ") { m_default = -999; m_recoOnly = true; }
    ~pileupVertexZ() { }
  };

  //____________________________________________________________________________
  class pileupVertexPhi : public VarBase<float> {
  public:
    pileupVertexPhi() : VarBase("pileupVertexPhi") { m_default = -99; m_recoOnly = true; }
    ~pileupVertexPhi() { }
  };

  //____________________________________________________________________________
  class zCommon : public VarBase<float> {
  public:
    zCommon() : VarBase("zCommon") { m_default = -999; m_recoOnly = true; }
    ~zCommon() { }
  };

  //____________________________________________________________________________
  class mu : public VarBase<float> {
  public:
    mu() : VarBase("mu") { m_default = -99; m_recoOnly = true; }
    ~mu() { }
  };

  //____________________________________________________________________________
  class vertexZ : public VarBase<float> {
  public:
    vertexZ() : VarBase("vertexZ") { m_default = -999; m_truthOnly = true; }
    ~vertexZ() { }
  };

  //____________________________________________________________________________
  class catCoup : public VarBase<int> {
  public:
    catCoup() : VarBase("catCoup") { m_default = 0; m_truthOnly = true; }
    ~catCoup() { }
  };

  //____________________________________________________________________________
  class procCoup : public VarBase<int> {
  public:
    procCoup() : VarBase("procCoup") { m_default = 0; m_truthOnly = true; }
    ~procCoup() { }
  };
  //____________________________________________________________________________
  class isVyyOverlap : public VarBase<char> {
  public:
    isVyyOverlap() : VarBase("isVyyOverlap") { m_default = false; m_truthOnly = true; }
    ~isVyyOverlap() { }
  };

  //____________________________________________________________________________
  class weightCatCoup_SFXGBoost_ttH : public VarBase<float> {
  public:
    weightCatCoup_SFXGBoost_ttH() : VarBase("weightCatCoup_SFXGBoost_ttH") { m_default = 1.0; }
    ~weightCatCoup_SFXGBoost_ttH() { }
  };
  //____________________________________________________________________________
  class weightCatCoup_XGBoost_ttH : public VarBase<float> {
  public:
    weightCatCoup_XGBoost_ttH() : VarBase("weightCatCoup_XGBoost_ttH") { m_default = 1.0; }
    ~weightCatCoup_XGBoost_ttH() { }
  };
  //____________________________________________________________________________
  class catCoup_XGBoost_ttH : public VarBase<int> {
  public:
    catCoup_XGBoost_ttH() : VarBase("catCoup_XGBoost_ttH") { m_default = -99; }
    ~catCoup_XGBoost_ttH() { }
  };
  //____________________________________________________________________________
  class score_ttH : public VarBase<float> {
  public:
    score_ttH() : VarBase("score_ttH") { m_default = -99.; }
    ~score_ttH() { }
  };

  //____________________________________________________________________________
  class weightCatCoup_SFXGBoost_ttHCP : public VarBase<float> {
  public:
    weightCatCoup_SFXGBoost_ttHCP() : VarBase("weightCatCoup_SFXGBoost_ttHCP") { m_default = 1.0; }
    ~weightCatCoup_SFXGBoost_ttHCP() { }
  };
  //____________________________________________________________________________
  class weightCatCoup_XGBoost_ttHCP : public VarBase<float> {
  public:
    weightCatCoup_XGBoost_ttHCP() : VarBase("weightCatCoup_XGBoost_ttHCP") { m_default = 1.0; }
    ~weightCatCoup_XGBoost_ttHCP() { }
  };
  //____________________________________________________________________________
  class catCoup_XGBoost_ttHCP : public VarBase<int> {
  public:
    catCoup_XGBoost_ttHCP() : VarBase("catCoup_XGBoost_ttHCP") { m_default = -99; }
    ~catCoup_XGBoost_ttHCP() { }
  };
  //____________________________________________________________________________
  class score_ttHCP : public VarBase<float> {
  public:
    score_ttHCP() : VarBase("score_ttHCP") { m_default = -99.; }
    ~score_ttHCP() { }
  };

  //____________________________________________________________________________
  class score_ttH_ICHEP2020 : public VarBase<float> {
  public:
    score_ttH_ICHEP2020() : VarBase("score_ttH_ICHEP2020") { m_default = -99.; }
    ~score_ttH_ICHEP2020() { }
  };
  //____________________________________________________________________________
  class score_tHjb_ICHEP2020 : public VarBase<float> {
  public:
    score_tHjb_ICHEP2020() : VarBase("score_tHjb_ICHEP2020") { m_default = -99.; }
    ~score_tHjb_ICHEP2020() { }
  };
  //____________________________________________________________________________
  class score_tWH_ICHEP2020 : public VarBase<float> {
  public:
    score_tWH_ICHEP2020() : VarBase("score_tWH_ICHEP2020") { m_default = -99.; }
    ~score_tWH_ICHEP2020() { }
  };
  //____________________________________________________________________________
  class score_WH_ICHEP2020 : public VarBase<float> {
  public:
    score_WH_ICHEP2020() : VarBase("score_WH_ICHEP2020") { m_default = -99.; }
    ~score_WH_ICHEP2020() { }
  };
  //____________________________________________________________________________
  class score_ZH_ICHEP2020 : public VarBase<float> {
  public:
    score_ZH_ICHEP2020() : VarBase("score_ZH_ICHEP2020") { m_default = -99.; }
    ~score_ZH_ICHEP2020() { }
  };

  //____________________________________________________________________________
  class weightCatCoup_SFMonoH_2var : public VarBase<float> {
  public:
    weightCatCoup_SFMonoH_2var() : VarBase("weightCatCoup_SFMonoH_2var") { m_default = 1.0; }
    ~weightCatCoup_SFMonoH_2var() { }
  };
  //____________________________________________________________________________
  class weightCatCoup_MonoH_2var : public VarBase<float> {
  public:
    weightCatCoup_MonoH_2var() : VarBase("weightCatCoup_MonoH_2var") { m_default = 1.0; }
    ~weightCatCoup_MonoH_2var() { }
  };
  //____________________________________________________________________________
  class catCoup_MonoH_2var : public VarBase<int> {
  public:
    catCoup_MonoH_2var() : VarBase("catCoup_MonoH_2var") { m_default = -99; }
    ~catCoup_MonoH_2var() { }
  };
  //____________________________________________________________________________
  class score_MonoH_2var : public VarBase<float> {
  public:
    score_MonoH_2var() : VarBase("score_MonoH_2var") { m_default = -99.; }
    ~score_MonoH_2var() { }
  };

}

namespace var {
  extern HG::pT_h1 pT_h1;
  extern HG::pT_h2 pT_h2;
  extern HG::y_h1 y_h1;
  extern HG::y_h2 y_h2;
  extern HG::m_h1 m_h1;
  extern HG::m_h2 m_h2;
  extern HG::yAbs_yy yAbs_yy;
  extern HG::pTt_yy pTt_yy;
  extern HG::m_yy m_yy;
  extern HG::passMeyCut passMeyCut;
  extern HG::pT_yy pT_yy;
  extern HG::pT_y1 pT_y1;
  extern HG::pT_y2 pT_y2;
  extern HG::E_y1 E_y1;
  extern HG::E_y2 E_y2;
  extern HG::sumpT_y_y sumpT_y_y;
  extern HG::DpT_y_y DpT_y_y;
  extern HG::pT_hard pT_hard;
  extern HG::cosTS_yy cosTS_yy;
  extern HG::phiStar_yy phiStar_yy;
  extern HG::Dphi_y_y Dphi_y_y;
  extern HG::Dy_y_y Dy_y_y;
  extern HG::N_j N_j;
  extern HG::N_j_30 N_j_30;
  extern HG::N_j_50 N_j_50;
  extern HG::N_j_central N_j_central;
  extern HG::N_j_central30 N_j_central30;
  extern HG::N_j_btag N_j_btag;
  extern HG::N_j_btag30 N_j_btag30;
  extern HG::N_e N_e;
  extern HG::N_mu N_mu;
  extern HG::N_lep N_lep;
  extern HG::N_lep_15 N_lep_15;
  extern HG::weightN_lep weightN_lep;
  extern HG::weightN_lep_15 weightN_lep_15;
  extern HG::pT_j1 pT_j1;
  extern HG::pT_j1_30 pT_j1_30;
  extern HG::pT_j2 pT_j2;
  extern HG::pT_j2_30 pT_j2_30;
  extern HG::pT_j3_30 pT_j3_30;
  extern HG::yAbs_j1 yAbs_j1;
  extern HG::yAbs_j1_30 yAbs_j1_30;
  extern HG::yAbs_j2 yAbs_j2;
  extern HG::yAbs_j2_30 yAbs_j2_30;
  extern HG::pT_jj pT_jj;
  extern HG::pT_yyj pT_yyj;
  extern HG::pT_yyj_30 pT_yyj_30;
  extern HG::m_yyj m_yyj;
  extern HG::m_yyj_30 m_yyj_30;
  extern HG::m_jj m_jj;
  extern HG::m_jj_30 m_jj_30;
  extern HG::m_jj_50 m_jj_50;
  extern HG::Dy_j_j Dy_j_j;
  extern HG::Deta_j_j Deta_j_j;
  extern HG::Dy_j_j_30 Dy_j_j_30;
  extern HG::Dy_yy_jj Dy_yy_jj;
  extern HG::Dy_yy_jj_30 Dy_yy_jj_30;
  extern HG::Dphi_j_j Dphi_j_j;
  extern HG::Dphi_j_j_30 Dphi_j_j_30;
  extern HG::Dphi_j_j_50 Dphi_j_j_50;
  extern HG::Dphi_j_j_30_signed Dphi_j_j_30_signed;
  extern HG::Dphi_j_j_50_signed Dphi_j_j_50_signed;
  extern HG::Dphi_yy_jj Dphi_yy_jj;
  extern HG::Dphi_yy_jj_30 Dphi_yy_jj_30;
  extern HG::Dphi_yy_jj_50 Dphi_yy_jj_50;
  extern HG::m_yyjj m_yyjj;
  extern HG::pT_yyjj pT_yyjj;
  extern HG::pT_yyjj_30 pT_yyjj_30;
  extern HG::pT_yyjj_50 pT_yyjj_50;
  extern HG::m_ee m_ee;
  extern HG::pt_ee pt_ee;
  extern HG::pt_mumu pt_mumu;
  extern HG::pt_llmax pt_llmax;
  extern HG::m_mumu m_mumu;
  extern HG::DRmin_y_j DRmin_y_j;
  extern HG::DRmin_y_j_2 DRmin_y_j_2;
  extern HG::DR_y_y DR_y_y;
  extern HG::Zepp Zepp;
  extern HG::cosTS_yyjj cosTS_yyjj;
  extern HG::sumTau_yyj_30 sumTau_yyj_30;
  extern HG::maxTau_yyj_30 maxTau_yyj_30;
  extern HG::met_TST met_TST;
  extern HG::met_Sig met_Sig;
  extern HG::sumet_TST sumet_TST;
  extern HG::phi_TST phi_TST;
  extern HG::N_conv N_conv;
  extern HG::m_alljet_30 m_alljet_30;
  extern HG::m_alljet m_alljet;
  extern HG::eta_alljet eta_alljet;
  extern HG::pT_alljet pT_alljet;
  extern HG::HT_30 HT_30;
  extern HG::HTall_30 HTall_30;
  extern HG::massTrans massTrans;
  extern HG::pTlepMET pTlepMET;
  extern HG::etalepMET etalepMET;
  extern HG::philepMET philepMET;
  extern HG::score_recotop1 score_recotop1;
  extern HG::pT_recotop1 pT_recotop1;
  extern HG::eta_recotop1 eta_recotop1;
  extern HG::phi_recotop1 phi_recotop1;
  extern HG::m_recotop1 m_recotop1;
  extern HG::score_recotop2 score_recotop2;
  extern HG::pT_recotop2 pT_recotop2;
  extern HG::eta_recotop2 eta_recotop2;
  extern HG::phi_recotop2 phi_recotop2;
  extern HG::m_recotop2 m_recotop2;
  extern HG::pT_hybridtop2 pT_hybridtop2;
  extern HG::eta_hybridtop2 eta_hybridtop2;
  extern HG::phi_hybridtop2 phi_hybridtop2;
  extern HG::m_hybridtop2 m_hybridtop2;
  extern HG::score_hadtop1 score_hadtop1;
  extern HG::pT_hadtop1 pT_hadtop1;
  extern HG::eta_hadtop1 eta_hadtop1;
  extern HG::phi_hadtop1 phi_hadtop1;
  extern HG::m_hadtop1 m_hadtop1;
  extern HG::score_hadtop2 score_hadtop2;
  extern HG::pT_hadtop2 pT_hadtop2;
  extern HG::eta_hadtop2 eta_hadtop2;
  extern HG::phi_hadtop2 phi_hadtop2;
  extern HG::m_hadtop2 m_hadtop2;
  extern HG::pT_had_hybrid_top2 pT_had_hybrid_top2;
  extern HG::eta_had_hybrid_top2 eta_had_hybrid_top2;
  extern HG::phi_had_hybrid_top2 phi_had_hybrid_top2;
  extern HG::m_had_hybrid_top2 m_had_hybrid_top2;
  extern HG::score_leptop1 score_leptop1;
  extern HG::pT_leptop1 pT_leptop1;
  extern HG::eta_leptop1 eta_leptop1;
  extern HG::phi_leptop1 phi_leptop1;
  extern HG::m_leptop1 m_leptop1;
  extern HG::pT_lep_hybrid_top2 pT_lep_hybrid_top2;
  extern HG::eta_lep_hybrid_top2 eta_lep_hybrid_top2;
  extern HG::phi_lep_hybrid_top2 phi_lep_hybrid_top2;
  extern HG::m_lep_hybrid_top2 m_lep_hybrid_top2;
  extern HG::idx_jets_recotop1 idx_jets_recotop1;
  extern HG::idx_jets_recotop2 idx_jets_recotop2;
  extern HG::index_leptop1 index_leptop1;
  extern HG::index_hadtop1 index_hadtop1;
  extern HG::index_hadtop2 index_hadtop2;
  extern HG::isPassedBasic isPassedBasic;
  extern HG::isDalitzEvent isDalitzEvent;
  extern HG::isPassed isPassed;
  extern HG::isPassedJetEventClean isPassedJetEventClean;
  extern HG::isFiducial isFiducial;
  extern HG::isFiducialKinOnly isFiducialKinOnly;
  extern HG::cutFlow cutFlow;
  extern HG::weightInitial weightInitial;
  extern HG::vertexWeight vertexWeight;
  extern HG::pileupWeight pileupWeight;
  extern HG::weightSF weightSF;
  extern HG::weightTrigSF weightTrigSF;
  extern HG::weight weight;
  extern HG::catCoup_Moriond2017BDTlep catCoup_Moriond2017BDTlep;
  extern HG::weightCatCoup_Moriond2017BDTlep weightCatCoup_Moriond2017BDTlep;
  extern HG::weightCatCoup_SFMoriond2017BDTlep weightCatCoup_SFMoriond2017BDTlep;
  extern HG::catCoup_GlobalICHEP catCoup_GlobalICHEP;
  extern HG::weightCatCoup_SFGlobalICHEP weightCatCoup_SFGlobalICHEP;
  extern HG::weightCatCoup_GlobalICHEP weightCatCoup_GlobalICHEP;
  extern HG::scoreBinaryCatCoup_GlobalICHEP scoreBinaryCatCoup_GlobalICHEP;
  extern HG::multiClassCatCoup_GlobalICHEP multiClassCatCoup_GlobalICHEP;
  extern HG::catCoup_HybridICHEP catCoup_HybridICHEP;
  extern HG::weightCatCoup_SFHybridICHEP weightCatCoup_SFHybridICHEP;
  extern HG::weightCatCoup_HybridICHEP weightCatCoup_HybridICHEP;
  extern HG::scoreBinaryCatCoup_HybridICHEP scoreBinaryCatCoup_HybridICHEP;
  extern HG::multiClassCatCoup_HybridICHEP multiClassCatCoup_HybridICHEP;
  extern HG::catCoup_Moriond2017BDT_qqH2jet catCoup_Moriond2017BDT_qqH2jet;
  extern HG::weightCatCoup_Moriond2017BDT_qqH2jet weightCatCoup_Moriond2017BDT_qqH2jet;
  extern HG::weightCatCoup_SFMoriond2017BDT_qqH2jet weightCatCoup_SFMoriond2017BDT_qqH2jet;
  extern HG::catCoup_Moriond2017BDT catCoup_Moriond2017BDT;
  extern HG::weightCatCoup_Moriond2017BDT weightCatCoup_Moriond2017BDT;
  extern HG::weightCatCoup_SFMoriond2017BDT weightCatCoup_SFMoriond2017BDT;
  extern HG::catCoup_Moriond2017 catCoup_Moriond2017;
  extern HG::weightCatCoup_Moriond2017 weightCatCoup_Moriond2017;
  extern HG::weightCatCoup_SFMoriond2017 weightCatCoup_SFMoriond2017;
  extern HG::catMass_Run1 catMass_Run1;
  extern HG::catMass_pT catMass_pT;
  extern HG::catMass_eta catMass_eta;
  extern HG::catMass_conv catMass_conv;
  extern HG::catMass_mu catMass_mu;
  extern HG::catXS_VBF catXS_VBF;
  extern HG::numberOfPrimaryVertices numberOfPrimaryVertices;
  extern HG::selectedVertexSumPt2 selectedVertexSumPt2;
  extern HG::selectedVertexZ selectedVertexZ;
  extern HG::selectedVertexPhi selectedVertexPhi;
  extern HG::hardestVertexSumPt2 hardestVertexSumPt2;
  extern HG::hardestVertexZ hardestVertexZ;
  extern HG::hardestVertexPhi hardestVertexPhi;
  extern HG::pileupVertexSumPt2 pileupVertexSumPt2;
  extern HG::pileupVertexZ pileupVertexZ;
  extern HG::pileupVertexPhi pileupVertexPhi;
  extern HG::zCommon zCommon;
  extern HG::vertexZ vertexZ;
  extern HG::catCoup catCoup;
  extern HG::procCoup procCoup;
  extern HG::mu mu;
  extern HG::isVyyOverlap isVyyOverlap;

  extern HG::weightCatCoup_SFXGBoost_ttH weightCatCoup_SFXGBoost_ttH;
  extern HG::weightCatCoup_XGBoost_ttH weightCatCoup_XGBoost_ttH;
  extern HG::catCoup_XGBoost_ttH catCoup_XGBoost_ttH;
  extern HG::score_ttH score_ttH;

  extern HG::weightCatCoup_SFXGBoost_ttHCP weightCatCoup_SFXGBoost_ttHCP;
  extern HG::weightCatCoup_XGBoost_ttHCP weightCatCoup_XGBoost_ttHCP;
  extern HG::catCoup_XGBoost_ttHCP catCoup_XGBoost_ttHCP;
  extern HG::score_ttHCP score_ttHCP;

  extern HG::score_ttH_ICHEP2020 score_ttH_ICHEP2020;
  extern HG::score_tHjb_ICHEP2020 score_tHjb_ICHEP2020;
  extern HG::score_tWH_ICHEP2020 score_tWH_ICHEP2020;
  extern HG::score_WH_ICHEP2020 score_WH_ICHEP2020;
  extern HG::score_ZH_ICHEP2020 score_ZH_ICHEP2020;

  extern HG::weightCatCoup_SFMonoH_2var weightCatCoup_SFMonoH_2var;
  extern HG::weightCatCoup_MonoH_2var weightCatCoup_MonoH_2var;
  extern HG::catCoup_MonoH_2var catCoup_MonoH_2var;
  extern HG::score_MonoH_2var score_MonoH_2var;
}
