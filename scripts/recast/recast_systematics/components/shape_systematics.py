import os
import re
from itertools import repeat
from functools import partial

import ROOT

from recast_systematics.components.base_systematics import BaseSystematics
from recast_systematics.utils.root import is_corrupt
from recast_systematics.ops.root import TH1
from recast_systematics.ops.math import *
from recast_systematics.utils.common import execute_multi_tasks

macro_compute_seed = \
"""
std::vector<int> ComputeSeed(int& eventNumber){{
    std::vector<int> result; 
    result.reserve({n_toys}); 
    for (std::size_t i=0; i < {n_toys};i++)
        result.push_back(1 + 10000 * eventNumber + i);
    return result;
}}
"""

macro_compute_weight_bootstrap = \
"""
std::vector<double> ComputeWeightBootstrap(double& weight, std::vector<int> &seed){
    std::vector<double> result;
    size_t n_toys = seed.size();
    result.reserve(n_toys);
    TRandom3 rand3;
    for (std::size_t i=0; i < n_toys; i++){
        rand3.SetSeed(seed[i]);
        int weight_Poisson = rand3.Poisson(1.);
        double weight_total = weight * weight_Poisson;
        result.push_back(weight_total);
    }
    return result;
}
"""

class ShapeSystematics(BaseSystematics):
    
    TREE_NAMES = {
        'nominal'  : 'tree_sel_nominal_{category}',
        'sys'      : 'tree_sel_{syst_name}{variation}_{category}',
    }
    
    FILE_NAMES = {
        'BOOTSTRAP_NOM': 'tree_bootstrap_nominal_{syst_theme}_{process}_n_toys_{n_toys}_{category}.root',
        'BOOTSTRAP_SYS': 'tree_bootstrap_syst_{syst_name}{variation}_{process}_n_toys_{n_toys}_{category}.root',
        'TOYS': 'toy_result_{syst_method}_{syst_name}{variation}_{process}_n_toys_{n_toys}_{category}.npz'
    }
    
    NON_RES_PROC = ['HH_non_resonant_kappa_lambda_01', 'HH_non_resonant_kappa_lambda_10',
                    'PowhegH7_HHbbyy_cHHH01d0', 'PowhegH7_HHbbyy_cHHH10d0',
                    'aMCnloHwpp_hh_yybb', 'aMCnloHwpp_hh_yybb_AF2',
                    'MGH7_hh_bbyy_vbf_l1cvv1cv1', 'NonResonantPlusSingle',
                    'SingleHiggs', 'PowhegPy8_NNLOPS_ggH125', 'ZH125',
                    'PowhegPy8_ZH125J', 'ttH125']
    RES_PROC     = [r'MGH7_X(\d+)']
    
    PRIMARY_KEYS = { 'position_shape': [('position_shape', 'up', 'rel_effect'), ('position_shape', 'down', 'rel_effect')],
                     'spread_shape': [('spread_shape', 'up', 'rel_effect'), ('spread_shape', 'down', 'rel_effect')]}
    AUXILIARY_KEYS = None
    
    def __init__(self, observable="m_yy", partition_method="bootstrap", syst_method="mean_IQR", n_toys=100,
                 bootstrap_outdir="./", save_toys=True, prune_threshold=0.1, prune_significative=True, *args,
                 **kwargs):
        super().__init__(prune_threshold=prune_threshold, 
                         prune_significative=prune_significative,
                         *args, **kwargs)
        self.observable = observable
        self.partition_method = partition_method
        self.syst_method = syst_method
        self.n_toys = n_toys
        self.bootstrap_outdir = bootstrap_outdir
        self.save_toys = save_toys
        
        if self.partition_method == "bootstrap":
            self.declare_bootstrap_macros(self.n_toys)
            
    def get_shape_info(self, process):
        shape_info = {}
        if any(re.search(pattern, process) for pattern in self.NON_RES_PROC):
            if self.observable == "m_yy":
                shape_info['functional_form'] = "DoubleCrystalBall"
                shape_info['mass_resonance'] = 125
            elif self.observable == "m_bb":
                shape_info['functional_form'] = "Bukin"
                shape_info['mass_resonance'] = 125
            else:
                raise ValueError("modelization with observable `{}` not anticipated".format(self.observable))
        elif any(re.search(pattern, process) for pattern in self.RES_PROC):
            if self.observable == "m_yy":
                shape_info['functional_form'] = "DoubleCrystalBall"
                shape_info['mass_resonance'] = 125
            elif self.observable == "m_bb":
                shape_info['functional_form'] = "DoubleCrystalBall"
                shape_info['mass_resonance'] = 125
            else:
                raise ValueError("treatment for observable `{}` not implemented".format(self.observable))
        else:
            raise ValueError("treatment for process `{}` not implemented".format(process))
            
        if self.observable == "m_yy":
            shape_info["low_shape"] = 13
            shape_info["high_shape"] = 13
            shape_info["n_bins"] = 52
        elif self.observable == "m_bb":
            shape_info["low_shape"] = 45
            shape_info["high_shape"] = 45
            shape_info["n_bins"] = 18
        else:
            raise ValueError("treatment for observable `{}` not implemented".format(self.observable))
            
        return shape_info
    
    @staticmethod
    def declare_bootstrap_macros(n_toys):
        if not hasattr(ROOT, 'ComputeSeed'):
            ROOT.gInterpreter.Declare(macro_compute_seed.format(n_toys=n_toys))
        if not hasattr(ROOT, 'ComputeWeightBootstrap'):
            ROOT.gInterpreter.Declare(macro_compute_weight_bootstrap)
        #ROOT.EnableImplicitMT()
        
    def get_nominal_bootstrap_tree_path(self, process, category, syst_theme):
        base_name = self.FILE_NAMES['BOOTSTRAP_NOM'].format(
            syst_theme=syst_theme, process=process, n_toys=self.n_toys, category=category)
        path = os.path.join(self.bootstrap_outdir, 'trees_bootstrap', process, base_name)
        return path
    
    def get_syst_bootstrap_tree_path(self, process, category, syst_name, variation):
        base_name = self.FILE_NAMES['BOOTSTRAP_SYS'].format(
            syst_name=syst_name, variation=variation, process=process, 
            n_toys=self.n_toys, category=category)
        path = os.path.join(self.bootstrap_outdir, 'trees_bootstrap', process, base_name)
        return path
    
    def get_toys_result_path(self, process, category, syst_name, variation):
        base_name = self.FILE_NAMES['TOYS'].format(syst_method=self.syst_method,
                                                   syst_name=syst_name,
                                                   variation=variation,
                                                   process=process,
                                                   n_toys=self.n_toys,
                                                   category=category)
        path = os.path.join(self.bootstrap_outdir, 'results_toys', process, base_name)
        return path          
        
    def create_bootstrap_trees(self, tree, outname):
        tree_name = tree.GetName()
        #cloned_tree = tree.Clone()
        rdf = ROOT.RDataFrame(tree)
        rdf = rdf.Define("seed", "ComputeSeed(eventNumber)")
        rdf = rdf.Define("weight_total_bootstrap", "ComputeWeightBootstrap(weight_total, seed)")
        rdf.Snapshot(tree_name, outname)
  
    def create_bootstrap_trees_from_root_file(self, fname, categories, systematics_list, 
                                              process, cache=True, remove_corrupt=True):
        if not os.path.exists(fname):
            raise FileNotFoundError("In <TFile::TFile>, file `{}` does not exist".format(fname))
        f = ROOT.TFile(fname)
        if f.IsZombie():
            raise RuntimeError("In <TFile::TFile>, corrupted file `{}`".format(fname))
            
        syst_themes = list(set([self.get_syst_theme(syst_name) for syst_name in systematics_list]))
        
        if not os.path.exists(self.bootstrap_outdir):
            os.makedirs(self.bootstrap_outdir)
        
        # accumulate jobs
        tree_names = []
        outnames = []
        
        # nominal tree with bootstrap
        for syst_theme in syst_themes:
            for category in categories:
                tree_name = self.TREE_NAMES['nominal'].format(category=category)
                tree_names.append(tree_name)
                outname = self.get_nominal_bootstrap_tree_path(process, category, syst_theme)
                outnames.append(outname)

        # systematics tree with bootstrap 
        for syst_name in systematics_list:
            for category in categories:
                is_symmetric = self.is_symmetric(syst_name)
                if is_symmetric:
                    variations = [""]
                else:
                    variations = ["__1up", "__1down"]
                for variation in variations:
                    tree_name = self.TREE_NAMES['sys'].format(syst_name=syst_name, 
                                                                  variation=variation,
                                                                  category=category)
                    tree_names.append(tree_name)
                    outname = self.get_syst_bootstrap_tree_path(process, category, syst_name, variation)
                    outnames.append(outname)
                    
        if remove_corrupt:
            print("INFO: Checking for corrupted files...")
            for tree_name, outname in zip(tree_names, outnames):
                if os.path.exists(outname) and is_corrupt(outname):
                    print("WARNING: Corrupted file `{}` detected. It will be deleted".format(outname))
                    os.remove(outname)
    
        for tree_name, outname in zip(tree_names, outnames):
            if os.path.exists(outname) and cache:
                print("INFO: Cache finished bootstrap trees `{}`".format(outname))
                continue
            else:
                tree = f.Get(tree_name)
                if not tree:
                    print("WARNING: In <TFILE::Get>, tree `{}` does not exist. Skipping.".format(tree_name))
                    continue
                self.create_bootstrap_trees(tree, outname)
                print("INFO: Created bootstrap trees `{}`".format(outname))
    
    @staticmethod
    def _get_IQR_systematics_data(tree_nom, tree_sys, xmin, xmax, observable, selection="weight_total"):
        n_bins = int((xmax - xmin)/0.001)
        hist_nominal_fine_bins = ROOT.TH1D("hist_nominal_fine_bins", "hist_nominal_fine_bins", n_bins, xmin, xmax)
        hist_syst_fine_bins = ROOT.TH1D("hist_syst_fine_bins", "hist_syst_fine_bins", n_bins, xmin, xmax)
        tree_nom.Project("hist_nominal_fine_bins", observable, selection)
        hist_nominal_fine_bins.Scale(1. / hist_nominal_fine_bins.Integral(0, n_bins + 1))
        tree_sys.Project("hist_syst_fine_bins", observable, selection)
        hist_syst_fine_bins.Scale(1. / hist_syst_fine_bins.Integral(0, n_bins + 1))

        nom_val = hist_nominal_fine_bins.GetMean()
        sys_val = hist_syst_fine_bins.GetMean()
        nom_err = hist_nominal_fine_bins.GetMeanError()
        sys_err = hist_syst_fine_bins.GetMeanError()
        rel_effect_position_shape = get_relative_effect(nom_val, sys_val)
        err_rel_effect_position_shape = get_err_relative_effect_uncorrelated(nom_val, nom_err, sys_val, sys_err)
        
        result = [nom_val, nom_err, sys_val, sys_err]
        
        bin_center = TH1.GetBinCenterArray(hist_nominal_fine_bins)
        try:
            bin_content_nom, bin_error_nom = TH1.GetCulmulativeData(hist_nominal_fine_bins)
            IQR_nominal, IQR_nominal_err = get_interquartile_data(bin_content_nom, bin_error_nom, bin_center)

            bin_content_sys, bin_error_sys = TH1.GetCulmulativeData(hist_syst_fine_bins)
            IQR_sys, IQR_sys_err = get_interquartile_data(bin_content_sys, bin_error_sys, bin_center)
            result += [IQR_nominal, IQR_nominal_err, IQR_sys, IQR_sys_err]
        except:
            raise
        finally:
            # need to free memory no matter what
            hist_nominal_fine_bins.Delete()
            hist_syst_fine_bins.Delete()
            
        return result

    @staticmethod
    def _get_toy_systematics_data(evaluator, tree_nom, tree_sys, n_toys, cache=True, save_as=None):
        if cache and ((save_as is not None) and (os.path.exists(save_as))):
            data = dict(np.load(save_as, allow_pickle=True))
            position_rel_effect = data['position_rel_effect']
            spread_rel_effect = data['spread_rel_effect']
        else:
            toy_results = []
            for toy_index in range(n_toys):
                selection = "weight_total_bootstrap[{}]".format(toy_index)
                toy_result = evaluator(tree_nom=tree_nom, tree_sys=tree_sys, selection=selection)
                toy_results.append(toy_result)
            toy_results = np.transpose(toy_results)
            position_rel_effect = get_relative_effect(toy_results[0], toy_results[2])
            spread_rel_effect = get_relative_effect(toy_results[4], toy_results[6])
            if save_as is not None:
                directory = os.path.dirname(save_as)
                if not os.path.exists(directory):
                    os.makedirs(directory)
                toy_results = np.append(toy_results, [position_rel_effect, spread_rel_effect], axis=0)
                data = {
                    'nom_val': toy_results[0],
                    'nom_err': toy_results[1],
                    'sys_val': toy_results[2],
                    'sys_err': toy_results[3],
                    'IQR_nom_val': toy_results[4],
                    'IQR_nom_err': toy_results[5],
                    'IQR_sys_val': toy_results[6],
                    'IQR_sys_err': toy_results[7],
                    'position_rel_effect': toy_results[8],
                    'spread_rel_effect': toy_results[9]
                }
                np.savez(save_as, **data)
        position_rel_effect_val = np.mean(position_rel_effect)
        position_rel_effect_err = np.std(position_rel_effect)
        spread_rel_effect_val = np.mean(spread_rel_effect)
        spread_rel_effect_err = np.std(spread_rel_effect)
        return [position_rel_effect_val, position_rel_effect_err, 
                spread_rel_effect_val, spread_rel_effect_err]

    def _get_bootstrap_systematics(self, evaluator, process, category, syst_name, cache=True):
        print("INFO: Evaluating shape systematics for process=`{}`, category=`{}`, systematics=`{}`".format(
              process, category, syst_name))
        syst_theme = self.get_syst_theme(syst_name)
        is_symmetric = self.is_symmetric(syst_name)
        syst_object = self.get_syst_object(syst_name)
        variations = self.get_variations(is_symmetric)
        all_data = [process, category, syst_name, syst_theme, syst_object, is_symmetric]
        status = 1
        try:
            fname_nom = self.get_nominal_bootstrap_tree_path(process, category, syst_theme)
            if not os.path.exists(fname_nom):
                raise FileNotFoundError("In <TFile::TFile>, file `{}` does not exist".format(fname_nom))
            if is_corrupt(fname_nom):
                raise RuntimeError("In <TFile::TFile>, corrupted file `{}`".format(fname_nom))
            f_nom = ROOT.TFile(fname_nom)
            tree_name_nom = self.TREE_NAMES['nominal'].format(category=category)
            tree_nom = f_nom.Get(tree_name_nom)
            
            systematics_data = []
            for variation in variations:
                fname_sys = self.get_syst_bootstrap_tree_path(process, category, syst_name, variation)
                if not os.path.exists(fname_sys):
                    raise FileNotFoundError("In <TFile::TFile>, file `{}` does not exist".format(fname_sys))
                if is_corrupt(fname_sys):
                    raise RuntimeError("In <TFile::TFile>, corrupted file `{}`".format(fname_sys))
                f_sys = ROOT.TFile(fname_sys)
                tree_name_sys = self.TREE_NAMES['sys'].format(syst_name=syst_name, 
                                                              variation=variation,
                                                              category=category)
                tree_sys = f_sys.Get(tree_name_sys)
                if not tree_sys:
                    raise RuntimeError("in <TFILE::Get>, tree `{}` does not exist.".format(tree_name_sys))
                if self.save_toys:
                    save_as = self.get_toys_result_path(process, category, syst_name, variation)
                else:
                    save_as = None
                try:
                    toy_systematics_data = self._get_toy_systematics_data(evaluator, 
                                                                          tree_nom, 
                                                                          tree_sys,
                                                                          self.n_toys,
                                                                          cache=cache,
                                                                          save_as=save_as)
                except:
                    toy_systematics_data = [0.]*4
                    #status = -2
                systematics_data += toy_systematics_data
            if is_symmetric:
                systematics_data = systematics_data*2
        except:
            systematics_data = [None]*8
            status = -1
        all_data.append(status)
        all_data += systematics_data
        return all_data
    
    def get_evaluator(self, process):
        if self.syst_method == "mean_IQR":
            shape_info = self.get_shape_info(process)
            xmin = shape_info['mass_resonance'] - shape_info['low_shape']
            xmax = shape_info['mass_resonance'] + shape_info['high_shape']
            evaluator = partial(self._get_IQR_systematics_data, 
                                xmin=xmin, xmax=xmax, observable=self.observable)
        else:
            raise RuntimeError("`{}` is a supported method for evaluating shape systematics")
        return evaluator
    
    def get_bootstrap_systematics(self, process, categories, systematics_list, save_toys=True,
                                  cache=True, append=True, parallel=-1):
        
        # checkout the method for evaluating systematics
        evaluator = self.get_evaluator(process)
        columns = ['process', 'category', 'systematics', 'theme', 'object', 'symmetric', 'status']
        for variation in ['up', 'down']:
            columns += [('position_shape', variation, 'rel_effect', 'val'),
                        ('position_shape', variation, 'rel_effect', 'err'),
                        ('spread_shape', variation, 'rel_effect', 'val'),
                        ('spread_shape', variation, 'rel_effect', 'err')]        
        args_categories = []
        args_systematics = []
        for category in categories:
            for syst_name in systematics_list:
                args_categories.append(category)
                args_systematics.append(syst_name)
        task_args = (repeat(evaluator), repeat(process), args_categories, args_systematics, repeat(cache))
        results = execute_multi_tasks(self._get_bootstrap_systematics, *task_args, parallel=parallel)
        if append:
            self.append(results, columns=columns)
        return results