#include "HGamAnalysisFramework/HGamCategoryToolBinaryDiscriminator2020.h"
#include "HGamAnalysisFramework/HGamCategoryToolMulticlass2020.h"

#include <TTree.h>

using namespace Coupling2020;

BinaryDiscriminatorLGBM::BinaryDiscriminatorLGBM(TTree *tree)
  : m_bdt(tree) {}

float BinaryDiscriminatorLGBM::get_result(const CategorizationInputs &inputs) const
{
  const std::vector<float> input_values_binary = get_input(inputs);
  return m_bdt.GetClassification(input_values_binary);
}

BinaryDiscriminatorFromVar::BinaryDiscriminatorFromVar(HG::VarBase<float> &var_score)
  : m_var_score(var_score) {}

float BinaryDiscriminatorFromVar::get_result(const CategorizationInputs & /*inputs*/) const
{
  return m_var_score();
}

BinaryDiscriminatorFromVarComplementary::BinaryDiscriminatorFromVarComplementary(HG::VarBase<float> &var_score)
  : m_var_score(var_score) {}

float BinaryDiscriminatorFromVarComplementary::get_result(const CategorizationInputs & /*inputs*/) const
{
  return 1. - m_var_score();
}

std::ostream &Coupling2020::operator << (std::ostream &out, const BinaryDiscriminator &instance) { return instance.format(out); }