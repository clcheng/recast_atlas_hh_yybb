{
  "sequencer":["DataAnalysis"],
  "directories": {
    "data1": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/data15/",
    "data2": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/data16/",
    "data3": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/data17/",
    "data4": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/data18/"
  },
  "dumper": {
    "data1": true,
    "data2": true,
    "data3": true,
    "data4": true
  },
  "variables": {
    "m_yy": {
      "var": "HGamEventInfoAuxDyn.m_yy*0.001",
      "bins": {
        "nbins": 55,
        "lbins": 105,
        "ubins": 160
      }
    }
  },
  "selections": {
    "tightScore_HMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2 && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_Cat/1000) % 10) == 1) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 ) ",
        
    "looseScore_HMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2 && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_Cat/1000) % 10) == 2) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 ) ",
        
    "tightScore_LMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2 && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_Cat/1000) % 10) == 3) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 ) ",
        
    "looseScore_LMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2 && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_BCal_Cat/1000) % 10) == 4) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 ) "
  },
  "lumi": {
    "data1": 1,
    "data2": 1,
    "data3": 1,
    "data4": 1
  },
  "samples": {
    "data": {
      "datafiles": {
        "data1": "",
        "data2": "",
        "data3": "",
        "data3": ""
      },
      "histoName": "data_hist"
    }
  }
}