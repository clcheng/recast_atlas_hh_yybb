#ifndef HGAMCATEGORYTOOLLHCPGLOBAL__H
#define HGAMCATEGORYTOOLLHCPGLOBAL__H

#include "AsgTools/AsgMessaging.h"

#include "HGamAnalysisFramework/HGamCategoryToolMulticlass2020.h"
#include "HGamAnalysisFramework/HGamCategoryToolBinaryDiscriminator2020.h"

#include <vector>
#include <string>
#include <memory>

namespace Coupling2020 {

  /**
   * Base class to implement the scheme proposed for 2020 for multiclass + binary one vs all
   *
   * This class should not be instantiated, but used to derive concrete one. The concrete one should
   * implement the list of variables used for the multiclass and the binary BDTs.
   *
   **/
  class CategorizationMultiClassPlusBinaries : public asg::AsgMessaging {
  public:
    struct Result {
      int index;
      int index_multiclass;
      float score_binary;
      float weight_SF;
    };

    /**
     * name: just the name of the tool, used for logging
     * boundaries: vector of boundaries for the binary BDTs, the external index is the one
     *             of the binary BDT, the internal one is the one for the boundary. For each
     *             binary BDT the number of bondaries must be the number of categories of that BDT
     *             minus one.
     * weight_multiclass: weights for the d-optimality, the size must be the number of classes (size of the
     *                    output of the multiclass)
     **/
    CategorizationMultiClassPlusBinaries(const std::string &name,
                                         std::unique_ptr<const CategorizationMultiClass> multiclass,
                                         std::vector<std::unique_ptr<const BinaryDiscriminator>> binaries,
                                         const std::vector<std::vector<float>> &boundaries);

    Result get_category(const CategorizationInputs &inputs, bool add_weight_fjvt) const;

  private:
    std::vector<int> m_offset_cat;

    std::unique_ptr<const CategorizationMultiClass> m_multiclass;
    std::vector<std::unique_ptr<const BinaryDiscriminator>> m_binaries;
    std::vector<std::vector<float>> m_boundaries;
    unsigned int m_nclasses;
  };
} // namespace Coupling2020

#endif
