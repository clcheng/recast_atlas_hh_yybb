///////////////////////// -*- C++ -*- /////////////////////////////
/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/
///////////////////////////////////////////////////////////////////

#ifndef IBJetCalibrationTool_H
#define IBJetCalibrationTool_H

#include "AsgTools/IAsgTool.h"

#include "xAODJet/Jet.h"

class IBJetCalibrationTool : virtual public asg::IAsgTool {
  
  ASG_TOOL_INTERFACE(IBJetCalibrationTool)

  public:
  
    /// Destructor: 
    virtual ~IBJetCalibrationTool() {};

    virtual StatusCode applyBJetCalibration(xAOD::Jet& jet) = 0;
    virtual void reset() = 0;
  
}; 

#endif
