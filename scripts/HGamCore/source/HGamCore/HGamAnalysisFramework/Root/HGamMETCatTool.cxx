// Local include(s):
#include <HGamAnalysisFramework/HGamMETCatTool.h>

namespace HG {

  //______________________________________________________________________________
  HGamMETCatTool::HGamMETCatTool(HG::EventHandler *eventHandler)
    : m_eventHandler(eventHandler)
  {}

  //______________________________________________________________________________
  HGamMETCatTool::~HGamMETCatTool()
  {

  }

  //______________________________________________________________________________
  // Return eventHandler when called as a function
  HG::EventHandler *HGamMETCatTool::eventHandler()
  {
    return m_eventHandler;
  }

  //______________________________________________________________________________
  EL::StatusCode HGamMETCatTool::initialize(Config &/*config*/)
  {
    m_metCutHigh       = 100.0 * HG::GeV;
    m_metCutLow        = 50.0 * HG::GeV;
    m_yy_MoriondptRest = 15.0 * HG::GeV;

    btag_WP_Name = "DL1r_FixedCutBEff_70";

    return EL::StatusCode::SUCCESS;
  }

  //______________________________________________________________________________
  std::pair<int, float> HGamMETCatTool::getEventInfoCategoryAndWeight()
  {
    if (not HG::isMAOD())
    { HG::fatal("HGamMETCatTool::getEventInfoCategoryAndWeight() - Requires MxAOD as input to use this function. If running over DxAOD please use getCategoryAndWeight() passing all relevant objects."); }

    float metWeight = eventHandler()->getVar<float>("met_weight");
    int   metCat    = eventHandler()->getVar<int>("met_cat");
    return std::make_pair(metCat, metWeight);
  }

  //______________________________________________________________________________
  void HGamMETCatTool::saveCategoryAndWeight(xAOD::PhotonContainer    photons,
                                             xAOD::ElectronContainer  electrons,
                                             xAOD::MuonContainer      muons,
                                             xAOD::JetContainer       jets,
                                             xAOD::MissingETContainer met)
  {
    std::pair<int, float> cat_weight = getCategoryAndWeight(photons, electrons, muons, jets, met);
    eventHandler()->storeVar<int>("met_cat", cat_weight.first);
    eventHandler()->storeVar<float>("met_weight", cat_weight.second);
  }

  //______________________________________________________________________________
  std::pair<int, float> HGamMETCatTool::getCategoryAndWeight(xAOD::PhotonContainer    photons,
                                                             xAOD::ElectronContainer  /*electrons*/,
                                                             xAOD::MuonContainer      /*muons*/,
                                                             xAOD::JetContainer       jets,
                                                             xAOD::MissingETContainer met)
  {
    /*
    Moriond_NOCAT  = 0
    Moriond_HMHP   = 1
    Moriond_HMLP   = 2
    Moriond_IntMET = 3
    Moriond_Rest   = 4
    Moriond_RR     = 5
    */

    if (photons.size() < 2)                  { return std::make_pair(Moriond_NOCAT, 0.0); }

    if (PassesHMHP(photons, met))            { return std::make_pair(Moriond_HMHP, m_metCatWeight); }

    if (PassesHMLP(photons, met))            { return std::make_pair(Moriond_HMLP, m_metCatWeight); }

    if (PassesIntMET(photons, jets, met))    { return std::make_pair(Moriond_IntMET, m_metCatWeight); }

    if (PassesMoriondRest(photons))          { return std::make_pair(Moriond_Rest, m_metCatWeight); }

    return std::make_pair(Moriond_RR, 1.0);
  }
  //__________________________________________

  bool HGamMETCatTool::PassesHMHP(xAOD::PhotonContainer    photons,
                                  xAOD::MissingETContainer met)
  {
    m_metCatWeight = 1.0;
    TLorentzVector gam1, gam2;
    gam1 = photons[0]->p4();
    gam2 = photons[1]->p4();

    float pT_yy = (gam1 + gam2).Pt() * HG::invGeV;
    float TST_met = (met)["TST"]->met() * HG::invGeV;
    float TST_sumet = (met)["TST"]->sumet() * HG::invGeV;
    float metSig    = TST_met / sqrt(TST_sumet);

    if (metSig > 7.0 && pT_yy > 90) { return true; }

    return false;
  }

  bool HGamMETCatTool::PassesHMLP(xAOD::PhotonContainer    photons,
                                  xAOD::MissingETContainer met)
  {
    m_metCatWeight = 1.0;
    TLorentzVector gam1, gam2;
    gam1 = photons[0]->p4();
    gam2 = photons[1]->p4();

    float pT_yy = (gam1 + gam2).Pt() * HG::invGeV;
    float TST_met = (met)["TST"]->met() * HG::invGeV;
    float TST_sumet = (met)["TST"]->sumet() * HG::invGeV;
    float metSig    = TST_met / sqrt(TST_sumet);

    if (metSig > 7.0 && pT_yy <= 90) { return true; }

    return false;
  }

  bool HGamMETCatTool::PassesIntMET(xAOD::PhotonContainer photons, xAOD::JetContainer /*jets*/, xAOD::MissingETContainer met)
  {
    m_metCatWeight = 1.0;
    TLorentzVector gam1, gam2;
    gam1 = photons[0]->p4();
    gam2 = photons[1]->p4();

    float pT_yy     = (gam1 + gam2).Pt() * HG::invGeV;
    float TST_met   = (met)["TST"]->met() * HG::invGeV;
    float TST_sumet = (met)["TST"]->sumet() * HG::invGeV;
    float metSig    = TST_met / sqrt(TST_sumet);

    if (metSig > 4.0 && metSig <= 7 && pT_yy > 25) { return true; }

    return false;
  }

  bool HGamMETCatTool::Passes2LepOrMore(xAOD::ElectronContainer electrons,
                                        xAOD::MuonContainer     muons)
  {
    m_metCatWeight = 1.0;
    int n_electrons = electrons.size();
    int n_muons = muons.size();
    int n_leptons = (n_electrons + n_muons);

    if (n_leptons < 2) { return false; }

    m_metCatWeight *= getLeptonSFs(electrons, muons);
    return true;
  }

  bool HGamMETCatTool::Passes1Lep(xAOD::ElectronContainer electrons,
                                  xAOD::MuonContainer     muons)
  {
    m_metCatWeight = 1.0;
    int n_electrons = electrons.size();
    int n_muons = muons.size();
    int n_leptons = (n_electrons + n_muons);

    if (n_leptons != 1) { return false; }

    m_metCatWeight *= getLeptonSFs(electrons, muons);
    return true;
  }

  bool HGamMETCatTool::Passes2JetOrMore1bJetOrMore(xAOD::JetContainer jets)
  {
    m_metCatWeight = 1.0;
    int nBjets = 0;
    int njets = jets.size();

    if (njets < 2) { return false; } //false if jet size is less than two

    for (auto jet : jets) {
      bool BTagWP = jet->auxdata<char>(btag_WP_Name.Data());
      m_metCatWeight   *= jet->auxdata<float>(("SF_" + btag_WP_Name).Data()); // b-tagging SFs (correction not implemented)
      m_metCatWeight   *= jet->auxdata<float>("SF_jvt") * jet->auxdata<float>("SF_fjvt"); // JVT SFs

      if (BTagWP) { nBjets++; }
    }

    if (njets >= 2 && nBjets >= 1) { return true; }

    return false;
  }

  bool HGamMETCatTool::Passes3JetOrMore(xAOD::JetContainer jets)
  {
    m_metCatWeight = 1.0;
    int njets = jets.size();

    if (njets < 3) { return false; }

    for (auto jet : jets) {
      m_metCatWeight *= jet->auxdata<float>("SF_jvt") * jet->auxdata<float>("SF_fjvt"); // JVT SFs
    }

    return true;
  }

  bool HGamMETCatTool::PassesMoriondRest(xAOD::PhotonContainer photons)
  {
    m_metCatWeight = 1.0;

    if (photons.size() < 2) { return false; }

    TLorentzVector gam1, gam2;
    gam1 = photons[0]->p4();
    gam2 = photons[1]->p4();
    float pT_yy = (gam1 + gam2).Pt();

    if (pT_yy <= m_yy_MoriondptRest) { return false; }

    return true;
  }

  double HGamMETCatTool::getLeptonSFs(xAOD::ElectronContainer electrons,
                                      xAOD::MuonContainer     muons)
  {
    double lepSF = 1.0;

    for (auto el : electrons) { lepSF *= el->auxdata<float>("scaleFactor"); }

    for (auto mu : muons)     { lepSF *= mu->auxdata<float>("scaleFactor"); }

    return lepSF;
  }


}
