Documentation
=============

This documentation lives inside the HGam repository, in this folder: https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/tree/master/docs

If you want to contributed to the documentation just push a merge request.

.. note::
   The documentation is built and deployed on this web site by a job available in gitlab, triggered manually (on the merge request page, inside the pipeline box click on the last circle on the right :menuselection:`deploy_docs --> Play`)
