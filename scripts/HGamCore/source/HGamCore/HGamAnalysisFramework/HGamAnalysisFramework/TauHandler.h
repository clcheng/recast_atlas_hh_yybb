#ifndef HGamAnalysisFramework_TauHandler
#define HGamAnalysisFramework_TauHandler

#include "HGamAnalysisFramework/HgammaHandler.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"

#include "TauAnalysisTools/TauSelectionTool.h"
#include "TauAnalysisTools/TauSmearingTool.h"
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "TauAnalysisTools/TauEfficiencyCorrectionsTool.h"
#include "TauAnalysisTools/TauTruthTrackMatchingTool.h"
#include "TauAnalysisTools/TauOverlappingElectronLLHDecorator.h"


namespace HG {

  class TauHandler : public HgammaHandler<xAOD::TauJet, xAOD::TauJetContainer, xAOD::TauJetAuxContainer> {

  public:
    TauHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~TauHandler();

    virtual EL::StatusCode initialize(Config &config) override;
    virtual CP::SystematicCode applySystematicVariation(const CP::SystematicSet &sys) override;
    virtual xAOD::TauJetContainer getCorrectedContainer() override;
    virtual xAOD::TauJetContainer applySelection(xAOD::TauJetContainer &container) override;

    const xAOD::TruthParticle *getTruthTau(xAOD::TauJet *tau);

  private:
    std::string getNameOfJetIDWorkingPoint(int wp);
    std::string getNameOfEleOLRWorkingPoint(int wp);

    TauAnalysisTools::TauTruthMatchingTool *m_tauTruthMatchingTool;
    TauAnalysisTools::TauSmearingTool      *m_tauSmearingTool;
    TauAnalysisTools::TauSelectionTool     *m_tauSelectionTool;

    std::map<std::string, std::unique_ptr<TauAnalysisTools::TauEfficiencyCorrectionsTool>> m_tauEfficiencyCorrectionsTools;

  };

} // namespace HG

#endif // HGamAnalysisFramework_TauHandler
