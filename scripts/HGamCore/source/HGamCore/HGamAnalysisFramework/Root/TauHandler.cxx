// STL include(s):
#include <stdexcept>

#include "xAODTau/TauxAODHelpers.h"

#include "HGamAnalysisFramework/TauHandler.h"
#include "HGamAnalysisFramework/HGamCommon.h"

namespace HG {

  TauHandler::TauHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store) :
    HgammaHandler(name, event, store),
    m_tauTruthMatchingTool(nullptr),
    m_tauSmearingTool(nullptr),
    m_tauSelectionTool(nullptr)
  {
  }



  TauHandler::~TauHandler()
  {
    SafeDelete(m_tauTruthMatchingTool);
    SafeDelete(m_tauSmearingTool);
    SafeDelete(m_tauSelectionTool);
  }



  EL::StatusCode TauHandler::initialize(Config &config)
  {
    ANA_CHECK_SET_TYPE(EL::StatusCode);
    ANA_CHECK(HgammaHandler::initialize(config));

    const xAOD::EventInfo *eventInfo = nullptr;
    ANA_CHECK(m_event->retrieve(eventInfo, "EventInfo"));
    m_containerName = config.getStr(m_name + ".ContainerName");

    // initialize TauTruthMatchingTool
    Info("TauHandler::initialize", "initializing TauTruthMatchingTool");
    m_tauTruthMatchingTool = new TauAnalysisTools::TauTruthMatchingTool("TauTruthMatchingTool");
    CP_CHECK(m_name, m_tauTruthMatchingTool->setProperty("MaxDeltaR", config.getNum(m_name + ".TruthMatching.MaxDeltaR", .2)));
    CP_CHECK(m_name, m_tauTruthMatchingTool->setProperty("WriteTruthTaus", config.getBool(m_name + ".TruthMatching.WriteTruthTaus", true)));
    ANA_CHECK(m_tauTruthMatchingTool->initialize());

    // initialize TauSmearingTool
    Info("TauHandler::initialize", "initializing TauSmearingTool");
    m_tauSmearingTool = new TauAnalysisTools::TauSmearingTool("TauSmearingTool");
    CP_CHECK(m_name, m_tauSmearingTool->setProperty("RecommendationTag", config.getStr(m_name + ".Smearing.RecommendationTag", "2019-summer")));
    ANA_CHECK(m_tauSmearingTool->initialize());

    // initialize TauSelectionTool
    Info("TauHandler::initialize", "initializing TauSelectionTool");
    m_tauSelectionTool = new TauAnalysisTools::TauSelectionTool("TauSelectionTool");
    CP_CHECK(m_name, m_tauSelectionTool->setProperty("ConfigPath", config.getStr(m_name + ".Selection.ConfigPath", "")));
    ANA_CHECK(m_tauSelectionTool->initialize());

    // initialize TauEfficiencyCorrectionTool
    Info("TauHandler::initialize", "initializing TauEfficiencyCorrectionsTool");

    // --- Reconstruction
    std::vector<int> efficiencyCorrectionTypes = {TauAnalysisTools::SFRecoHadTau};
    auto recoTool = std::make_unique<TauAnalysisTools::TauEfficiencyCorrectionsTool>("TauEfficiencyCorrectionsTool_Reco");
    CP_CHECK(m_name, recoTool->setProperty("RecommendationTag", config.getStr(m_name + ".EfficiencyCorrection.RecommendationTag", "2019-summer")));
    CP_CHECK(m_name, recoTool->setProperty("EfficiencyCorrectionTypes", efficiencyCorrectionTypes));
    CP_CHECK(m_name, recoTool->setProperty("isAFII", HG::isAFII()));
    ANA_CHECK(recoTool->initialize());
    recoTool->printConfig(true);
    m_tauEfficiencyCorrectionsTools.insert(std::make_pair("Reco", std::move(recoTool)));

    // --- Electron OLR
    std::vector<int> eleOLRLevels = {TauAnalysisTools::ELEBDTLOOSE, TauAnalysisTools::ELEBDTMEDIUM, TauAnalysisTools::TAUELEOLR};
    efficiencyCorrectionTypes = {TauAnalysisTools::SFEleOLRHadTau, TauAnalysisTools::SFEleOLRElectron};

    for (int eleOLRLevel : eleOLRLevels) {
      std::string wp = getNameOfEleOLRWorkingPoint(eleOLRLevel);
      auto tool = std::make_unique<TauAnalysisTools::TauEfficiencyCorrectionsTool>("TauEfficiencyCorrectionsTool_" + wp);
      CP_CHECK(m_name, tool->setProperty("RecommendationTag", config.getStr(m_name + ".EfficiencyCorrection.RecommendationTag", "2019-summer")));
      CP_CHECK(m_name, tool->setProperty("OLRLevel", eleOLRLevel));
      CP_CHECK(m_name, tool->setProperty("EfficiencyCorrectionTypes", efficiencyCorrectionTypes));
      CP_CHECK(m_name, tool->setProperty("isAFII", HG::isAFII()));
      ANA_CHECK(tool->initialize());
      tool->printConfig(true);
      m_tauEfficiencyCorrectionsTools.insert(std::make_pair(wp, std::move(tool)));
    }

    // --- RNN TauID
    std::vector<int> jetIDLevels = {TauAnalysisTools::JETIDRNNLOOSE, TauAnalysisTools::JETIDRNNMEDIUM, TauAnalysisTools::JETIDRNNTIGHT};
    efficiencyCorrectionTypes = {TauAnalysisTools::SFJetIDHadTau};

    for (int jetIDLevel : jetIDLevels) {
      std::string wp = getNameOfJetIDWorkingPoint(jetIDLevel);
      auto tool = std::make_unique<TauAnalysisTools::TauEfficiencyCorrectionsTool>("TauEfficiencyCorrectionsTool_" + wp);
      CP_CHECK(m_name, tool->setProperty("RecommendationTag", config.getStr(m_name + ".EfficiencyCorrection.RecommendationTag", "2019-summer")));
      CP_CHECK(m_name, tool->setProperty("IDLevel", jetIDLevel));
      CP_CHECK(m_name, tool->setProperty("EfficiencyCorrectionTypes", efficiencyCorrectionTypes));
      CP_CHECK(m_name, tool->setProperty("isAFII", HG::isAFII()));
      ANA_CHECK(tool->initialize());
      tool->printConfig(true);
      m_tauEfficiencyCorrectionsTools.insert(std::make_pair(wp, std::move(tool)));
    }

    return EL::StatusCode::SUCCESS;
  }



  xAOD::TauJetContainer TauHandler::getCorrectedContainer()
  {
    bool calib = false;
    xAOD::TauJetContainer shallowContainer = getShallowContainer(calib);

    if (calib) { return shallowContainer; }

    // apply truth mathcing, calibraction, efficiency correction
    for (xAOD::TauJet *tau : shallowContainer) {
      // truth matching, needed to run here to add truthJetLink
      if (HG::isMC()) {
        m_tauTruthMatchingTool->getTruth(*tau);
      }

      // calibration
      m_tauSmearingTool->applyCorrection(*tau);

      // efficiency correction
      double sf = 1.0;

      if (HG::isMC()) {
        for (const auto &tool : m_tauEfficiencyCorrectionsTools) {
          sf = 1.0;
          tool.second->getEfficiencyScaleFactor(*tau, sf);
          tau->auxdecor<float>("TauEffSF_" + tool.first) = (float)sf;
        }
      }
    }

    shallowContainer.sort(comparePt);

    return shallowContainer;
  }



  xAOD::TauJetContainer TauHandler::applySelection(xAOD::TauJetContainer &container)
  {
    xAOD::TauJetContainer selected(SG::VIEW_ELEMENTS);

    // apply selection, i.e. pt and eta requirements,  to tau candidate
    for (auto tau : container) {
      if (m_tauSelectionTool->accept(tau)) {
        selected.push_back(tau);
      }
    }

    return selected;
  }



  CP::SystematicCode TauHandler::applySystematicVariation(const CP::SystematicSet &sys)
  {
    setVertexCorrected(false);

    bool isAffected = false;

    // check whether tau is affected by the systemaic uncertainty
    for (auto var : sys) {
      if (m_tauSmearingTool->isAffectedBySystematic(var)) {
        isAffected = true;
        break;
      }

      for (const auto &tool : m_tauEfficiencyCorrectionsTools) {
        if (tool.second->isAffectedBySystematic(var)) {
          isAffected = true;
          break;
        }
      }

      if (isAffected) { break; }
    }

    if (isAffected) {
      m_tauSmearingTool->applySystematicVariation(sys);

      for (const auto &tool : m_tauEfficiencyCorrectionsTools) {
        tool.second->applySystematicVariation(sys);
      }

      m_sysName = (sys.name() == "" ? "" : "_" + sys.name());
    } else {
      m_tauSmearingTool->applySystematicVariation(CP::SystematicSet());

      for (const auto &tool : m_tauEfficiencyCorrectionsTools) {
        tool.second->applySystematicVariation(CP::SystematicSet());
      }

      m_sysName = "";
    }

    return CP::SystematicCode::Ok;
  }



  const xAOD::TruthParticle *TauHandler::getTruthTau(xAOD::TauJet *tau)
  {
    const xAOD::TruthParticle *xTruthParticle = m_tauTruthMatchingTool->applyTruthMatch(*tau);
    return xTruthParticle;
  }



  std::string TauHandler::getNameOfJetIDWorkingPoint(int wp)
  {
    std::string name = "";

    switch (wp) {
      case TauAnalysisTools::JETIDRNNLOOSE :
        name += "JetIDRNNLoose";
        break;

      case TauAnalysisTools::JETIDRNNMEDIUM :
        name += "JetIDRNNMedium";
        break;

      case TauAnalysisTools::JETIDRNNTIGHT :
        name += "JetIDRNNTight";
        break;

      default:
        Error("TauHandler", "Working point is not supported");
        break;
    }

    return name;
  }



  std::string TauHandler::getNameOfEleOLRWorkingPoint(int wp)
  {
    std::string name = "";

    switch (wp) {
      case TauAnalysisTools::ELEBDTLOOSE :
        name += "EleBDTLoose";
        break;

      case TauAnalysisTools::ELEBDTMEDIUM :
        name += "EleBDTMedium";
        break;

      case TauAnalysisTools::TAUELEOLR :
        name += "TauEleOLR";
        break;

      default:
        Error("TauHandler", "Working point is not supported");
        break;
    }

    return name;
  }


} // end of namespace HG
