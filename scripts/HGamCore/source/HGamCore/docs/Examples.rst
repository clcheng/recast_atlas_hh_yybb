HGamAnalysisFrameworkTutorial
=============================

Basic setup
-----------

First, log into lxplus. Next, if you haven't done so already, follow the instructions on Installation part 
If you follow the Installation section, you should have a directory with source, build and run subdirectories. 
The HGamCore package should be checked out and compiled.

Next, copy an input xAOD file from the following location to your tmp folder on lxplus 
and remember your lxplus machine number in case you log off and log in again.

.. code-block:: bash

   echo $HOSTNAME # this is your lxplus machine
   cp /eos/atlas/user/y/yanlin/tutorials/mc16_13TeV/DAOD_HIGG1D1.20695145._000001.pool.root.1 /tmp/$USER/data_test_DxAOD.root

Note that if you have a grid certificate, you can grab the same file via:

.. code-block:: bash

   lsetup rucio
   rucio download mc16_13TeV:DAOD_HIGG1D1.20695145._000001.pool.root.1 # this is a Rel21 MC DxAOD from mc16_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.deriv.DAOD_HIGG1D1.e5607_s3126_r9364_p4097 
   mv mc16_13TeV/DAOD_HIGG1D1.20695145._000001.pool.root.1 /tmp/$USER/data_test_DxAOD.root
   rmdir mc16_13TeV

Next we will make a skeleton package inheriting from the HgammaAnalysis class. Go to the source directory and run:

.. code-block:: bash

   make_hgam_pkg HGamTutorialPackage
   add_hgam_alg_to_pkg HGamTutorialPackage TutorialAnalysis

After doing this, you should now have a new package in with the following files:

.. code-block:: bash

   HGamTutorialPackage/CMakeLists.txt
   HGamTutorialPackage/HGamTutorialPackage/TutorialAnalysis.h
   HGamTutorialPackage/Root/TutorialAnalysis.cxx
   HGamTutorialPackage/Root/LinkDef.h  
   HGamTutorialPackage/data/TutorialAnalysis.cfg
   HGamTutorialPackage/util/runTutorialAnalysis.cxx

The main work is happening in in the source file TutorialAnalysis.cxx.

And then you can compile:

.. code-block:: bash

   cd $TestArea/../build 
   cmake ../source 
   make -j4

Next time
~~~~~~~~~

As a test, you can log in to lxplus again and go to your working area you created above and get ready for analysis by doing:

.. code-block:: bash
   
   cd HGam/source # or wherever you made your area
   setupATLAS
   asetup AnalysisBase,21.2.113,here # Note: use the base release matched to the framework tag you checked out
   source $TestArea/../build/$AnalysisBase_PLATFORM/setup.sh


Testing the setup
-----------------

If you followed the above instructions, you should have got an executable called runTutorialAnalysis. Try running it:

.. code-block:: bash

   cd $TestArea/../run
   runTutorialAnalysis

This should abort, since you don't specify any input or configuration. A nice help message is printed to the screen explaining this.
To run successfully you need to specify the configuration, try this:

.. code-block:: bash

   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config /tmp/$USER/data_test_DxAOD.root

This should run over the file - but so far nothing is being done event-by-event. As specified by the help message when the command aborted earlier, 
there are many other way to specify the input. For example using a text file:

.. code-block:: bash

   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config InputFileList: myfilelist.txt

where myfilelist.txt contains a list of input xAOD files. See `Running your job`_ on this page for more details.


Modifying the code: Photon Histograms
-------------------------------------

Now let's edit the code and add some histograms. Open TutorialAnalysis.cxx with your favorite c++ editor (vi, emacs, XCode). 
There you will see two main methods: createOuput() and execute().

Add the following lines to createOutput() to create a few histograms. The syntax is name, number of bins, the x-axis range (min max) and finally the title.
The title formatting follows root's standard: "top title ; x-title ; y-title", where each title is separated with a semi-colon character.

.. code-block:: bash

   histoStore()->createTH1F("m_yy", 60, 100, 160,";#it{m}_{#gamma#gamma} [GeV]");
   histoStore()->createTH1F("pT_y1", 80, 100, 200,";#it{p}_{T#gamma1} [GeV]");

   // Simple histogram to hold cut-flow
   histoStore()->createTH1F("cut_flow", 10, 0, 10);
   TAxis *axis = histoStore()->getTH1F("cut_flow")->GetXaxis();
   HG::StrV binLabels = {"all events","2 photon candidates","2 tight photons"};
   for (size_t i=0;i<binLabels.size();++i)
     axis->SetBinLabel(i+1,binLabels[i]);

Next, let's add some code to the execute method - which is executed for each event. Here we can fill the histograms we created.

.. code-block:: bash

   // Important to keep this, so that internal tools / event variables
   // are filled properly.
   HgammaAnalysis::execute();

   xAOD::PhotonContainer allPhotons = photonHandler()->getCorrectedContainer();
   histoStore()->fillTH1F("cut_flow",0);

   // Cut 1: require two photons
   if (allPhotons.size()<2) return EL::StatusCode::SUCCESS;
   histoStore()->fillTH1F("cut_flow",1);

   // Now apply the photon selection
   xAOD::PhotonContainer photons = photonHandler()->applySelection(allPhotons);
  
   // Cut 2: require two tight photons
   if (photons.size() < 2) return EL::StatusCode::SUCCESS;
   histoStore()->fillTH1F("cut_flow",2);

   // Now fill the histograms histogram 
   TLorentzVector y1 = photons[0]->p4(), y2 = photons[1]->p4();

   // convert these 4-vectors to GeV!
   y1 *= HG::invGeV;
   y2 *= HG::invGeV; 

   TLorentzVector yy = y1+y2;
   histoStore()->fillTH1F("pT_y1",y1.Pt());
   histoStore()->fillTH1F("m_yy",yy.M());

Now compile and run.

.. code-block:: bash

   cd $TestArea/../build 
   make -j4 
   cd ../run
   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config /tmp/$USER/data_test_DxAOD.root

You should now get an output folder (whose name includes a timestamp) with file named hist-sample.root. Open it and have a look what's inside.

.. code-block:: bash

   root -l TutorialAnalysis_2019.02.06_hh.mm.ss/hist-sample.root

And you will find all the histograms.

Accessing common variables using VarHandler
-------------------------------------------

The framework contains a class called the VarHandler which gives access to many common variables used in the HGam group, 
such as m_yy, pt_y1, etc. (a fairly full list is available in 
`HGamAnalysisFramework/HGamVariables.h
<https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/blob/master/HGamAnalysisFramework/HGamAnalysisFramework/HGamVariables.h>`_).
After you determine your selected objects in e.g. the applySelection(allPhotons) step, simply call

.. code-block:: bash

   setSelectedObjects(&photons);

which will set up the photon-related quantities. (Note that you must give setSelectedObjects() the 
containers for photons, electrons, muons, jets, and met if you want quantities related to those objects.) Now the lines above:

.. code-block:: bash

   // Now fill the histograms histogoram
   TLorentzVector y1 = photons[0]->p4(), y2 = photons[1]->p4();

   // convert these 4-vectors to GeV!
   y1 *= HG::invGeV;
   y2 *= HG::invGeV;

   TLorentzVector yy = y1+y2;
   histoStore()->fillTH1F("pT_y1",y1.Pt());

can be replaced by

.. code-block:: bash

   setSelectedObjects(&photons); // (call only once)
   histoStore()->fillTH1F("pT_y1" ,var::pT_y1()*HG::invGeV);
   histoStore()->fillTH1F("m_yy",var::m_yy()*HG::invGeV);

Modifying the code: Adding information to the Output xAOD file
--------------------------------------------------------------

The heavy lifting of writing the output xAOD is done in HgammaAnalysis.cxx; take a look at all of the methods
there to see the details (setupJob(), initialize(), finalize()). 
From the user side you just need to decide which containers to save and then to fill the output xAOD. 
This is done in your TutorialAnalysis.cxx in the execute() method:

.. code-block:: bash

   if(!event()->copy("Photons").isSuccess()){
     HG::fatal("Failed to copy Photons container!");
     return EL::StatusCode::FAILURE;
   }

   // You absolutely need to have the following line in order to fill the output xAOD.
   event()->fill(); // Only call this once per event

This method will copy the original input container to the output xAOD file. Go to your output folder and investigate the file in the data-MxAOD folder. 
And you will see the "Photons" container in the "CollectionTree"? In this way you can copy any xAOD container from the input xAOD file.

Now if you want to copy your modified (calibrated) photons container, you should use the writeContainer() from the photonHandler():

.. code-block:: bash

   if(photonHandler()->writeContainer(photons) == StatusCode::FAILURE)
     HG::fatal("Can't write corrected photons container");

Recompile, run again and investigate the output xAOD file. You should see a container named "HGamPhotons" with the calibrated photon objects there.

To write "EventInfo" and "HGamEventInfo" containers to output file, you could use the following syntax:

.. code-block:: bash

   if(eventHandler()->writeEventInfo() == StatusCode::FAILURE)
      HG::fatal("Failed to copy EventInfo container!");

   if(HG::VarHandler::getInstance()->write() == StatusCode::FAILURE)
      HG::fatal("Failed to copy HGamEventInfo container!");

Modifying the code: Working with EventHandler
---------------------------------------------

Repeat the exercise but include systematic variations - see which ones are available, how they are used, 
and what is the effect of applying them on our previously saved histograms. In createOutput() you can add:

.. code-block:: bash
 
  for (auto sys: getSystematics()) {
    if (sys.name() == "") continue; // the nominal was already handled
    TString suffix = sys.name();
    histoStore()->createTH1F("m_yy_" + suffix, 60, 100, 160);
  }

In execute, you can then loop over the systematic shifts and fill the histograms:

.. code-block:: bash
  
   for (auto sys: getSystematics()) {
     if (sys.name() == "") continue;
     applySystematicVariation(sys);
     xAOD::PhotonContainer sysPhotons  = photonHandler()->getCorrectedContainer();
     xAOD::PhotonContainer allSysPhotons  = photonHandler()->applySelection(sysPhotons);

     if (allSysPhotons.size() < 2) continue;
     TLorentzVector yy = allSysPhotons[0]->p4() + allSysPhotons[1]->p4();
     yy *= HG::invGeV;

     TString suffix = sys.name();
     histoStore()->fillTH1F("m_yy_"+suffix, yy.M());
   }

Modifying the code: Other objects
---------------------------------

You should try all of the above example on the other objects. Right now we have handlers for: electrons, muons, photons and jets, MET and truth particles.

Making an output TTree
----------------------

For some use-cases it may be convenient to output data from the HGamAnalysisFramework into a TTree.
This can be easily accomplished by adding the following lines.

In HGamTutorialPackage/HGamTutorialPackage/TutorialAnalysis.h, add:

.. code-block:: bash

   private:
     /// Output ntuple
     TTree *m_output_tree; //!
     double m_output_double; //!

In HGamTutorialPackage/Root/TutorialAnalysis.cxx, add the following to the createOutput() function:

.. code-block:: bash

   EL::StatusCode HGamTutorialPackage::createOutput()
   {
     TFile *file = wk()->getOutputFile("MxAOD");
     m_output_tree = new TTree("output","output");
     m_output_tree->SetDirectory(file);
     m_output_tree->Branch( "output_double", &m_output_double );
   }

Then go to the execute() function and add:

.. code-block:: bash

   EL::StatusCode HGamTutorialPackage::execute()
   {
     m_output_double = 5.0;
     m_output_tree->Fill();
   }

Running your job
----------------

Running on local files
~~~~~~~~~~~~~~~~~~~~~~

Let's say you want to run over one file, file1.root. You can do this in any of the following ways:

.. code-block:: bash

   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config file1.root
   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config InputFile: file1.root
   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config InputFileList: list.txt

where in the last case, list.txt can be a file containing a list of files you want to run over:

.. code-block:: bash

   # This is a comment
   file1.root
   # add more files here

If you want to run over more than one file, then the following work:

.. code-block:: bash

   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config file1.root file2.root
   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config InputFileList: list.txt

(note that the "InputFile:" argument no longer works for >1 file.)

Running on the grid
~~~~~~~~~~~~~~~~~~~

If you want to run on the grid, you can use the following prescription. Note that you must specify both the GridDS and the OutputDS:

.. code-block:: bash

   runTutorialAnalysis HGamTutorialPackage/TutorialAnalysis.config GridDS: data16_13TeV.00297730.physics_Main.deriv.DAOD_HIGG1D1.r9264_p3083_p3142 OutputDS: user.$USER.myAnalysisOutputDS

where $USER is your grid username. The details of grid submission via EventLoop can be found on the `EventLoop twiki
<https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/EventLoop>`_.
Many (but not all) of the available options are implemented in the HGam framework. The details can be found in `RunUtils.cxx
<https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/blob/master/HGamAnalysisFramework/Root/RunUtils.cxx>`_.

Running on GridEngine batch systems
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The framework can be run using GridEngine batch systems (e.g. Sun Grid Engine SGE) using the following arguments:

.. code-block:: bash

   runTutorialAnalysis InputFileList: list.txt HGamTutorialPackage/TutorialAnalysis.config BatchGE: YES nc_EventLoop_EventsPerWorker: 1000 

where the number of events per worker must be set.

The GridEngine batch options can be set using the nc_EventLoop_SubmitFlags option:

.. code-block:: bash

   nc_EventLoop_SubmitFlags: "-q default.q,short.q,long.q -l h_vmem=4G -V"

The code defaults to the options above; check which batch options are appropriate for your system. 
(Note that "-V" seems to be needed in order to copy the current environment to the subjobs.)

Running on HTCondor (Condor) batch systems
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The framework can be run using HTCondor batch systems using the following arguments:

.. code-block:: bash

   runTutorialAnalysis InputFileList: list.txt HGamTutorialPackage/TutorialAnalysis.config BatchCondor: YES nc_EventLoop_EventsPerWorker: 1000 

where the number of events per worker must be set. The code uses "getEnv = True" to capture the current environment and send it to the worker node.

In case the Condor configuration does not work out-of-the-box, there are two additional options one can use to configure Condor:

- Condor_shellInit: Any required shellInit commands
- optCondorConf: Configuration lines to be added to the Condor config file


In particular, if you receive the following error or something similar:

.. code-block:: bash

   eventloop_batch_worker: error while loading shared libraries: libEventLoop.so: cannot open shared object file: No such file or directory

In particular, if you receive the following error or something similar:

.. code-block:: bash

   eventloop_batch_worker: error while loading shared libraries: libEventLoop.so: cannot open shared object file: No such file or directory

then you are likely missing the environment variable LD_LIBRARY_PATH in your Condor job (the reason for this error is typically
that you are submitting from screen or a nested bash shell, and for security reasons LD_LIBRARY_PATH is prevented from being reported to the system).
This can be fixed by adding the following parameter to your command-line, which transfers the current definition of LD_LIBRARY_PATH to the worker node:

.. code-block:: bash

   runTutorialAnalysis BatchCondor: YES (...) Condor_shellInit: "export LD_LIBRARY_PATH="$LD_LIBRARY_PATH

Running skimming and slimming code with the framework
-----------------------------------------------------

Simple skimming / slimming through configuration file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There is a SkimAndSlim class in HGamTools which allows for easy skimming and slimming of `MxAODs
<https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MxAODs>`_. The default executable can currently:

- Remove branches (slimming)
- Remove unwanted events based on the cutFlow variable (skimming)
- Remove duplicate events (skimming)
- Remove events based on a new GRL (skimming, data only)
- Update pileup weighting based on new ilumicalc file (MC simulation only)
- Update mu variable based on new ilumicalc file (data only)


The sum of all cut flow histograms of the same name is also saved to the output. This means that the first three entries of the resulting MC 
cut flow histogram can be used for cross-section normalization. For data, a different cut flow histogram will show up in the output for each run number. 
In case events are removed from data using a new GRL, the cut flow histograms are updated as much as possible given the skimming of the input MxAOD.

A beta version is currently available. The executable can be run using:

.. code-block:: bash

   runSkimAndSlim skim.config

where you specify basic steering options in the usual way:

.. code-block:: bash

   OutputDir:        SkimAndSlim_DATE
   SampleName:       sample
   InputFileList:    input_data.txt
   BaseConfig:       HGamAnalysisFramework/HGamRel21.config

To slim away unwanted branches, you specify a list of containers to keep and the decorations you'd like to keep for each. 
A simple example to only keep photons and some event variables is:

.. code-block:: bash

   SkimAndSlim.ContainersToSave:  HGamPhotons
   +SkimAndSlim.ContainersToSave: HGamEventInfo

   SkimAndSlim.BranchesToSave.HGamPhotons:  pt.ptcone20.topoetcone40.topoetcone40_SC
   SkimAndSlim.BranchesToSave.HGamEventInfo: m_yy.isPassed.weight.pileupWeight.vertexWeight.isDalitz

If you don't specify which decorations to keep with a BranchesToSave option, the entire container will be saved. 
In order to save different systematic variations of HGamEventInfo, you need to list the specific variations you'd like to keep:

.. code-block:: bash

   SkimAndSlim.SystematicsToSave:  _EG_SCALE_ALLCORR__1up
   +SkimAndSlim.SystematicsToSave: _EG_SCALE_ALLCORR__1down
   and so on

To skip duplicate events:

.. code-block:: bash

   SkimAndSlim.RemoveDuplicateEvents: YES

To skim based on cutFlow level:

.. code-block:: bash

   SkimAndSlim.SkimmingCutFlowName: cutFlow
   SkimAndSlim.SkimmingCutFlowMin:  15

To apply a tighter GRL to an MxAOD, you can use the following:

.. code-block:: bash

   SkimAndSlim.ApplyGRL: YES
   SkimAndSlim.GRL:      GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml
   +SkimAndSlim.GRL:     GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml
   +SkimAndSlim.GRL:     GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml
   +SkimAndSlim.GRL:     GoodRunsLists/data18_13TeV/20181111/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml

where you should specify the new GRL you're interested in. To update the mu decoration on HGamEventInfo you can specify a new ilumicalc like this:

.. code-block:: bash

   SkimAndSlim.UpdateMu:            YES
   SkimAndSlim.ConfigFiles:         HGamAnalysisFramework/PRW_mc16a_fullSim.root
   +SkimAndSlim.ConfigFiles:        HGamAnalysisFramework/PRW_mc16d_fullSim.root
   +SkimAndSlim.ConfigFiles:        HGamAnalysisFramework/PRW_mc16e_fullSim.root 
   SkimAndSlim.LumiCalcFiles:       GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root 
   +SkimAndSlim.LumiCalcFiles:      GoodRunsLists/data16_13TeV/20170605/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-008.root 
   +SkimAndSlim.LumiCalcFiles:      GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root 
   +SkimAndSlim.LumiCalcFiles:      GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root 
   # Set to 1.0/1.03=0.97087379 for MC16 as recommended by ASG
   SkimAndSlim.DataScaleFactor:     0.97087379

To update pileup weighting, you can specify the new ilumicalc file:

.. code-block:: bash

   SkimAndSlim.UpdatePileupWeight:  YES
   SkimAndSlim.ConfigFiles:         HGamAnalysisFramework/PRW_mc16a_fullSim.root
   +SkimAndSlim.ConfigFiles:        HGamAnalysisFramework/PRW_mc16d_fullSim.root
   +SkimAndSlim.ConfigFiles:        HGamAnalysisFramework/PRW_mc16e_fullSim.root 
   SkimAndSlim.LumiCalcFiles:       GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root 
   +SkimAndSlim.LumiCalcFiles:      GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root
   +SkimAndSlim.LumiCalcFiles:      GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root 
   +SkimAndSlim.LumiCalcFiles:      GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root 
   # Set to 1.0/1.03=0.97087379 for MC16 as recommended by ASG
   SkimAndSlim.DataScaleFactor:     0.97087379

Working examples of skimming configuration files can be found in the `HGamTools
<https://gitlab.cern.ch/atlas-hgam-sw/HGamCore/tree/master/HGamTools/data/>`_ subdirectory of the HGamCore package.
