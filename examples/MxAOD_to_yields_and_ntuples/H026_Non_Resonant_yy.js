{
  "sequencer":["YieldCalculator"],

    "name":{
        "yields" : "yields"
    },

  "directories": {
    "mc16a": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/mc16a/Nominal/",
    "mc16d": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/mc16d/Nominal/",
    "mc16e": "root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h026/mc16e/Nominal/"
  },

  "dumper": {
    "mc16a": false,
    "mc16d": false,
    "mc16e": false
  },

  "variables": {
    "m_yy": {
      "var": "HGamEventInfoAuxDyn.m_yy*0.001",
      "bins": {
        "nbins": 55,
        "lbins": 105,
        "ubins": 160
      }
    }
  },
  "selections": {

    "weight": "HGamEventInfoAuxDyn.crossSectionBRfilterEff*HGamEventInfoAuxDyn.weight*HGamEventInfoAuxDyn.yybb_weight*HGamEventInfoAuxDyn.weightFJvt*4",

    "tightScore_HMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2 && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat/1000) % 10) == 1) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 && EventInfoAux.eventNumber % 4 == 3) ",

    "looseScore_HMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2 && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat/1000) % 10) == 2) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 && EventInfoAux.eventNumber % 4 == 3)   ",

    "tightScore_LMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2  && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat/1000) % 10) == 3) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 && EventInfoAux.eventNumber % 4 == 3)   ",

    "looseScore_LMass": "(HGamEventInfoAuxDyn.isPassed == 1 && Sum$(HGamAntiKt4PFlowCustomVtxHggJetsAuxDyn.DL1r_FixedCutBEff_77>0)>=2  && (((HGamEventInfoAuxDyn.yybb_nonRes_XGBoost_btag77_BCal_Cat/1000) % 10) == 4) && HGamEventInfoAuxDyn.m_yy*0.001 > 105 && HGamEventInfoAuxDyn.m_yy*0.001 < 160 && EventInfoAux.eventNumber % 4 == 3)   "
  },

  "lumi": {
    "mc16a": 36207.66,
    "mc16d": 44307.4,
    "mc16e": 58450.1
  },

  "samples": {
    "yy": {
      "datafiles": {
        "mc16a": "mc16a.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.e6452_a875_r9364_p4204_h026.root",
        "mc16d": "mc16d.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.e6452_a875_r10201_p4204_h026.root",
        "mc16e": "mc16e.Sherpa2_diphoton_myy_90_175_AF2.MxAODDetailed.e6452_a875_r10724_p4204_h026.root"
      },
      "histoName": "CutFlow_Sherpa2_myy_90_175_noDalitz_weighted"
    }
  }
}


