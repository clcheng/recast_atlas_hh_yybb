#pragma once

/********************
 * HistogramStore:
 *   class to create, store, fill and retrieve TH1 and TH2 histograms
 *
 * Usage:
 *   HistogramStore HistoStore;
 *   HistoStore.createTH1F("Nphotons",40,-0.5,39.5,";#it{N}_{photon-clusters}");
 *   vector<TH1*> AllHistos = HistoStore.getListOfHistograms();
 *
 */

// STL include(s):
#include <map>
#include <vector>

// ROOT include(s):
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TProfile.h"
#include "TString.h"

/*! \brief Class that handles creating, filling, and retrieving histograms
 *        through use of an internal map
 *  \author Nathan Readioff
 */
class HistogramStore {
private:
  std::map<TString, TH1F *>     m_histoTH1F;
  std::map<TString, TH2F *>     m_histoTH2F;
  std::map<TString, TH3F *>     m_histoTH3F;
  std::map<TString, TProfile *> m_histoTProfile;
  std::map<TString, TH1D *>     m_histoTH1D;
  std::map<TString, TH2D *>     m_histoTH2D;
  std::map<TString, TH3D *>     m_histoTH3D;
public:
  //! \brief Create and store TH1F histogram
  void createTH1F(TString name, int Nbins, double xmin, double xmax, TString title = "");

  //! \brief Create and store TH1F histogram
  void createTH1F(TString name, const std::vector<double> &bins, TString title = "");

  //! \brief Create and store TH2F histogram
  void createTH2F(TString name, int NbinsX, double xmin, double xmax, int NBinsY, double ymin, double ymax, TString title = "");

  //! \brief Create and store TH2F histogram
  void createTH2F(TString name, const std::vector<double> &xbins, const std::vector<double> &ybins, TString title = "");

  //! \brief Create and store TH3F histogram
  void createTH3F(TString name, int NbinsX, double xmin, double xmax, int NBinsY, double ymin, double ymax, int NBinsZ, double zmin, double zmax, TString title = "");

  //! \brief Create and store TH3F histogram
  void createTH3F(TString name, const std::vector<double> &xbins, const std::vector<double> &ybins, const std::vector<double> &zbins, TString title = "");

  //! \brief Create and store TProfile histogram
  void createTProfile(TString name, int NbinsX, double xmin, double xmax, TString title = "");

  //! \brief Create and store TProfile histogram
  void createTProfile(TString name, const std::vector<double> &xbins, TString title = "");



  //! \brief Fill existing TH1F histogram
  inline void fillTH1F(TString name, double x, double w = 1.0) {getTH1F(name)->Fill(x, w);}

  //! \brief Fill existing TH2F histogram
  inline void fillTH2F(TString name, double x, double y, double w = 1.0) {getTH2F(name)->Fill(x, y, w);}

  //! \brief Fill existing TH3F histogram
  inline void fillTH3F(TString name, double x, double y, double z, double w = 1.0) {getTH3F(name)->Fill(x, y, z, w);}

  //! \brief Fill existing TProfile histogram
  inline void fillTProfile(TString name, double x, double y, double w = 1.0) {getTProfile(name)->Fill(x, y, w);}


  //! \brief check whether a given TH1F exist in the store
  inline bool hasTH1F(TString name) {return m_histoTH1F.count(name) > 0;}

  //! \brief check whether a given TH2F exist in the store
  inline bool hasTH2F(TString name) {return m_histoTH2F.count(name) > 0;}

  //! \brief check whether a given TH3F exists in the store
  inline bool hasTH3F(TString name) {return m_histoTH3F.count(name) > 0;}

  //! \brief check whether a given TH2F exist in the store
  inline bool hasTProfile(TString name) {return m_histoTProfile.count(name) > 0;}



  //! \brief Retrieve TH1F histogram from internal store
  TH1F *getTH1F(TString name);

  //! \brief Retrieve TH2F histogram from internal store
  TH2F *getTH2F(TString name);

  //! \brief Retrieve TH3F histogram from internal store
  TH3F *getTH3F(TString name);

  //! \briefRetrieve TProfile histogram from internal store
  TProfile *getTProfile(TString name);

  //! \brief Retrieve List of all histograms in internal store
  std::vector<TH1 *> getListOfHistograms();

  // JB

  //! \brief Create and store TH1D histogram
  void createTH1D(TString name, int Nbins, double xmin, double xmax, TString title = "");

  //! \brief Create and store TH1D histogram
  void createTH1D(TString name, const std::vector<double> &bins, TString title = "");

  //! \brief Create and store TH2D histogram
  void createTH2D(TString name, int NbinsX, double xmin, double xmax, int NBinsY, double ymin, double ymax, TString title = "");

  //! \brief Create and store TH2D histogram
  void createTH2D(TString name, const std::vector<double> &xbins, const std::vector<double> &ybins, TString title = "");

  //! \brief Create and store TH3D histogram
  void createTH3D(TString name, int NbinsX, double xmin, double xmax, int NBinsY, double ymin, double ymax, int NBinsZ, double zmin, double zmax, TString title = "");

  //! \brief Create and store TH3D histogram
  void createTH3D(TString name, const std::vector<double> &xbins, const std::vector<double> &ybins, const std::vector<double> &zbins, TString title = "");

  //! \brief Fill existing TH1D histogram
  inline void fillTH1D(TString name, double x, double w = 1.0) {getTH1D(name)->Fill(x, w);}

  //! \brief Fill existing TH2D histogram
  inline void fillTH2D(TString name, double x, double y, double w = 1.0) {getTH2D(name)->Fill(x, y, w);}

  //! \brief Fill existing TH3D histogram
  inline void fillTH3D(TString name, double x, double y, double z, double w = 1.0) {getTH3D(name)->Fill(x, y, z, w);}

  //! \brief check whether a given TH1D exist in the store
  inline bool hasTH1D(TString name) {return m_histoTH1D.count(name) > 0;}

  //! \brief check whether a given TH2D exist in the store
  inline bool hasTH2D(TString name) {return m_histoTH2D.count(name) > 0;}

  //! \brief check whether a given TH3D exists in the store
  inline bool hasTH3D(TString name) {return m_histoTH3D.count(name) > 0;}

  //! \brief Retrieve TH1D histogram from internal store
  TH1D *getTH1D(TString name);

  //! \brief Retrieve TH2D histogram from internal store
  TH2D *getTH2D(TString name);

  //! \brief Retrieve TH3D histogram from internal store
  TH3D *getTH3D(TString name);

};
