
def WriteRunCard(j):
   f = open(j["logdir"] + "/submit", "w")
   f.write("universe = vanilla\n")
   f.write("\n")
   f.write("executable = " + j["logdir"] + "/" + j["run_script"] + "\n")
   f.write("\n")
   f.write("output = " + j["logdir"] + "/output_$(ProcId)\n")
   f.write("error = " + j["logdir"] + "/error_$(ProcId)\n")
   f.write("log = " + j["logdir"] + "/log_$(ProcId)\n")
   f.write("\n")
   f.write("+JobFlavour = \"" + j["jobflavour"] + "\"\n")
   f.write("\n")
   f.write("environment = \"INPUT=input_data_subjob$(ProcId).txt \\\n")
   if (len(j["files"]) == 1):
      f.write("               OUTPUT=" + j["xrootd"] + j["output"] + "/" + j["name"] + ".root \\\n")
   else:
      f.write("               OUTPUT=" + j["xrootd"] + j["stage"] + "/" + j["name"] + "_subjob$(ProcId).root \\\n")
   f.write("               WORKAREA=" + j["workarea"] + " \\\n")
   f.write("               LOGDIR=" + j["logdir"] + " \\\n")
   f.write("               CONFIG=" + j["config"] + "\"\n")
   f.write("\n")
   f.write("queue " + str(len(j["files"])) + "\n")
   f.close()

def WriteMergeCard(j):
   f = open(j["logdir"] + "/submit_m", "w")
   f.write("universe = vanilla\n")
   f.write("\n")
   f.write("executable = " + j["logdir"] + "/" + j["merge_script"] + "\n")
   f.write("\n")
   f.write("output = " + j["logdir"] + "/output_m\n")
   f.write("error = " + j["logdir"] + "/error_m\n")
   f.write("log = " + j["logdir"] + "/log_m\n")
   f.write("\n")
   f.write("+JobFlavour = \"" + j["jobflavour"] + "\"\n")
   f.write("\n")
   for i in range(len(j["files"])):
      if (i == 0):
         f.write("arguments = \"" + j["xrootd"] + j["stage"] + "/" + j["name"] + "_subjob" + str(i) + ".root \\\n")
      elif (i == len(j["files"])-1):
         f.write("             " + j["xrootd"] + j["stage"] + "/" + j["name"] + "_subjob" + str(i) + ".root\"\n")
      else:
         f.write("             " + j["xrootd"] + j["stage"] + "/" + j["name"] + "_subjob" + str(i) + ".root \\\n")
   f.write("\n")
   f.write("environment = \"OUTPUT=" + j["xrootd"] + j["output"] + "/" + j["name"] + ".root \\\n")
   f.write("               WORKAREA=" + j["workarea"] + " \\\n")
   f.write("               LOGDIR=" + j["logdir"] + "\"\n")
   f.write("\n")
   f.write("queue\n")
   f.close()

