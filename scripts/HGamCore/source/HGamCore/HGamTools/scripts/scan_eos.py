#!/bin/env python

__author__ = "Ruggero Turra"
__doc__ = """
scan directories BASE_DIRS (usually eos directories) searching for ROOT files
corresponding to MC mxAOD.
This works only with POSIX folders (e.g. no xrootd, ok /eos on lxplus)
It writes a csv file with the following informations:

   * filename full path,
   * mc production (mc16a/...),
   * mxAOD short name,
   * mxAOD flavour (MxAODFlavorSys/...),
   * ptag (p3418/...),
   * htag (mxAOD tag) (h019/...),
   * file number (when more than one file per sample) (001/...),
   * number of events read (from the unweighted cutflow histogram bin 1)

Each line corresponds to a ROOT file (a sample can be made by multiple files),
example:

    /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h019/mc16a/FlavorSys/mc16a.PowhegPy8_bbH.MxAODFlavorSys.p3418.h019.root,mc16a,PowhegPy8_bbH,MxAODFlavorSys,p3418,h019,,100000.0
    /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h019/mc16a/FlavorSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODFlavorSys.p3404.h019.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODFlavorSys.p3404.h019.001.root,mc16a,PowhegPy8_NNLOPS_ggH125,MxAODFlavorSys,p3404,h019,001,300000.0
    /eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h019/mc16a/FlavorSys/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODFlavorSys.p3404.h019.root/mc16a.PowhegPy8_NNLOPS_ggH125.MxAODFlavorSys.p3404.h019.002.root,mc16a,PowhegPy8_NNLOPS_ggH125,MxAODFlavorSys,p3404,h019,002,200000.0

If you want the number of events for each sample you need to sum the events
looping over the file number (group for all properties except for file number
and full path and sum). In pandas:

    eos = pd.read_csv('scan_eos.csv', header=None, names=['fn', 'mc', 'short_name', 'flavour', 'ptag', 'htag', 'm', 'nevents'])
    eos = eos.groupby(['short_name', 'mc', 'flavour', 'ptag', 'htag'])['nevents'].sum().to_frame().sort_index()
"""


import os
import ROOT
import re
import logging
logging.basicConfig(level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler())
logging.getLogger().addHandler(logging.FileHandler("scan_eos.log"))

BASE_DIRS = ["/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h019/",
             "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h021/",
             "/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h021a/"]

fout = open("scan_eos.csv", "w")

re_filename = re.compile(r'(mc16[acde])\.(.+?)\.(MxAOD.+?)\.(p[0-9]+)\.(h[0-9]+[a-z]?)(?:(?:\.|_)([0-9]+))?\.root')


def get_entries(full_path):
    f = ROOT.TFile.Open(full_path)
    if not f:
        logging.error("cannot open file %s", full_path)
        return 0
    for key in (x.GetName() for x in f.GetListOfKeys()):
        if "CutFlow" not in key:
            continue
        if "noDalitz" in key or "weighted" in key:
            continue
        h = f.Get(key)
        if type(h) != ROOT.TH1F:
            continue
        entries = h.GetBinContent(1)
        break
    else:
        logging.error("Cannot find cuflow histogram in file %s", full_path)
        f.Close()  # RAII??
        return 0
    f.Close()
    return entries


for base_dir in BASE_DIRS:
    if not os.path.isdir(base_dir):
        logging.error("directory %s is not an existing directory, skipping", base_dir)
        continue
    for root, subdirs, files in os.walk(base_dir):
        if '/data' in root:  # bad trick
            continue
        for fn in files:
            full_path = os.path.join(root, fn)
            m = re_filename.match(fn)
            if not m:
                m = re_filename.search(full_path)  # case when there are multiple files
                if not m:
                    logging.error("cannot parse '%s' in %s", fn, full_path)
                    continue
            entries = get_entries(full_path)
            if os.path.isfile(full_path):
                fout.write(','.join(map(lambda x: str(x) if x is not None else '', (full_path,) + m.groups() + (entries,))) + '\n')
