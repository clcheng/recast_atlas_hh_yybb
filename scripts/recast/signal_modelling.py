import os
import re
import glob

import json
import numpy as np
import click

from utils import category_map

def load_macros():
    import ROOT
    macros_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), "macros")
    macros_name = "Modelling_bbyy.C"
    macros_path = os.path.join(macros_dir, macros_name)
    print('INFO: Loading macro "{}"...'.format(macros_name))
    if not os.path.exists(macros_path):
        print('ERROR: Cannot locate macro files from {}. ROOT macros will not be compiled.'.format(macros_dir))
        return -1
    return ROOT.gROOT.LoadMacro(macros_path)

xml_expression_map = {
    'meanNom': 'muCBNom',
    'sigmaCBNom': 'sigmaCBNom',
    'alphaCBLo': 'alphaCBLo',
    'alphaCBHi': 'alphaCBHi',
    'nCBHi': 'nCBHi',
    'nCBLo': 'nCBLo'
}

def parse_model_xmls(base_path, prefix, category_map):
    model_params = {}
    for category in category_map:
        old_cat = category_map[category]
        model_params[category] = {}
        xml_path = os.path.join(base_path, "{}_{}_DSCB_Unbinned.xml".format(prefix, old_cat))
        if not os.path.exists(xml_path):
            raise ValueError("model xml {} does not exist".format(xml_path))
        lines = open(xml_path).readlines()
        for expr in xml_expression_map:
            for line in lines:
                if 'Item' in line and expr in line:
                    value = re.findall(r"[-+]?\d*\.\d+|\d+", line)[0]
                    model_params[category][xml_expression_map[expr]] = value
                    break
    return model_params

@click.command(name='signal_modelling')    
@click.option('-i', '--input_path', required=True, help='directory containing minitrees for signal modelling')
@click.option('--resonant/--non-resonant', default=False, help='resonant or non-resonant analysis')
@click.option('-o', '--outname', default="model_parameters.json", help='output file name')
@click.option('-p', '--prefix', default="HH", help='name prefix of minitrees to be processed')
def run_signal_modelling(input_path, resonant, outname, prefix):
    """ Tool for signal modelling """
    import ROOT
    ROOT.gROOT.SetBatch(True)
    load_macros()
    resonant_type = "resonant" if resonant else "non-resonant"
    categories = category_map[resonant_type]
    print("INFO: Begin signal modelling for {} analysis".format(resonant_type))
    outdir = str(os.path.dirname(os.path.abspath(outname)))
    if not os.path.exists(outdir):
        os.makedirs(outdir)
    
    for cat in categories:
        old_cat = categories[cat] # old category name
        minitree_path = os.path.join(input_path, "{}_{}_tree.root".format(prefix, old_cat))
        if not os.path.exists(minitree_path):
            raise ValueError("mimitree {} does not exist".format(minitree_path))
        print("=======================================================================")
        print("INFO: Processing minitree {}".format(minitree_path))
        print("Process: {}, Category: {}".format(prefix, old_cat))
        print("=======================================================================")
        ROOT.Modelling_bbyy(str(outdir), False, str(prefix), str(old_cat), str(input_path), "DSCB")
    
    xml_base_path = os.path.join(outdir, "model")
    model_params = parse_model_xmls(xml_base_path, prefix, categories)
    with open(outname, 'w') as output:
        json.dump(model_params, output, indent=2)
        print('INFO: Model parameters saved as {}'.format(outname))