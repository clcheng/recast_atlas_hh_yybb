import os
import json
import glob
from collections import OrderedDict, defaultdict

import oyaml as yaml
import numpy as np
import click

#now using values from Marc
ggF_HH_params = {'SM_1': (1.56276, -0.803479, 0.12113),
'SM_2': (0.695215, -0.398999, 0.0646446),
'BSM_1': (0.195258, -0.206064, 0.0578087),
'BSM_2': (0.32765, -0.36127, 0.106953)}

VBF_HH_params = {'SM_1': (0.0460971, -0.0470876, 0.0137609),
'SM_2': (0.0387746, -0.0304524, 0.00811418),
'BSM_1': (0.0188482, -0.0244384, 0.00888752),
'BSM_2': (0.0357513, -0.0457254, 0.0171165)}

kl_reweighting_syst = {'SM_1': 0.0271,
'SM_2': 0.0461,
'BSM_1': 0.0387,
'BSM_2': 0.0313}

category_map = {
'SM_1' : 'tightScore_HMass',
'SM_2' : 'looseScore_HMass',
'BSM_1' : 'tightScore_LMass',
'BSM_2' : 'looseScore_LMass',
}

nested_dict = lambda: defaultdict(nested_dict)

SIG_PRUNE_THRESHOLD = 0 #0.1% #0.002
SYST_PRUNE_THRESHOLD = 0    #1% #0.001
SYST_SCALE_PRUNE_THRESHOLD = 0
SYST_RES_PRUNE_THRESHOLD = 0
XS_OR_MU = 'XS' #treatment of theory uncertainties

if XS_OR_MU == 'XS': DEFF_OR_DNRT = 'dEff'
elif XS_OR_MU == 'MU': DEFF_OR_DNRT = 'dNrt'

#convert HComb name to production mode
#outputs: ggH, VBF, ZH, WH, ggZH, ttH, bbH, tHW, tHqb
def HComb_to_prodmode(process_name):
    if process_name.startswith('ggF_'): return 'ggH'
    if process_name.startswith('VBF_') or process_name.startswith('ZH_') or process_name.startswith('WH_') or process_name.startswith('ggZH_') or process_name.startswith('ttH') or process_name.startswith('bbH') or process_name.startswith('tHW') or process_name.startswith('tHqb'): return process_name.split('_', 1)[0]
    if process_name.startswith('qq2Hlnu'): return 'WH'
    if process_name.startswith('qq2Hll'): return 'ZH'
    if process_name.startswith('gg2Hll'): return 'ggZH'

def HComb_to_STXS1p2(process_name):
    if process_name.startswith('ggF_') or process_name.startswith('VBF_') or process_name.startswith('ZH_') or process_name.startswith('WH_') or process_name.startswith('ggZH_'):
        return process_name.split('_', 1)[1]
    else: 
        return process_name

def HComb_to_STXS0(process_name):
    process_stage0_names = ['ggF_gg2H', 'VBF_qq2Hqq', 'WH_qq2Hqq', 'ZH_qq2Hqq', 'qq2Hlnu', 'qq2Hll', 'ggZH_gg2H', 'gg2Hll', 'ttH', 'bbH', 'tHqb_tH', 'tHW_tH']
    for process_stage0_name in process_stage0_names:
        if process_name.startswith(process_stage0_name): return process_stage0_name
    return 'MISSING'

def isHH(process_name):
    return ('HH' in process_name or 'hh' in process_name)

def isH(process_name):
    return (not isHH(process_name) and ('ggH' in process_name or 'VBF' in process_name or 'WH' in process_name or 'ZH' in process_name or 'ttH' in process_name or 'tHjb' in process_name or 'tWH' in process_name))

def process_Marc_name(name):
    if name == 'PDF_alpha_s': return 'THEO_ACC_PDFalphas'
    if name == 'QCD': return 'THEO_ACC_SCALE'
    if name == 'PartonShowering': return 'THEO_ACC_PS'
    return 'ATLAS_' + name

def load_Marc_txt(dict_name, cat_name, process, file):
    try:
        f = open(file)
        lines = f.readlines()
        for x in lines:
            if x.startswith('#'): continue
            nvals = len(x.split(' '))
            if nvals == 3:
                dict_name[cat_name][process][process_Marc_name(x.split(' ')[0])]['up'] = float(x.split(' ')[1])/100.
                dict_name[cat_name][process][process_Marc_name(x.split(' ')[0])]['down'] = float(x.split(' ')[2].replace('\n', ''))/100.
            elif nvals == 2:
                dict_name[cat_name][process][process_Marc_name(x.split(' ')[0])]['up'] = float(x.split(' ')[1].replace('\n', ''))/100.

    except IOError:
        print(file, "not found; intentional?")
    except IndexError:
        print(file, "missing some syst, intentional?")

        #TODO properly fix missing files or missing entries

def load_Alex_txt(dict_name, cat_name, process, file):
    try:
        f = open(file)
        lines = f.readlines()
        for x in lines:
            nvals = len(x.split(' '))
            if nvals == 3:
                dict_name[cat_name][process][x.split(' ')[0].split('_' + process)[0]]['up'] = float(x.split(' ')[1])
                dict_name[cat_name][process][x.split(' ')[0].split('_' + process)[0]]['down'] = float(x.split(' ')[2].replace('\n', ''))
            elif nvals == 2:
                dict_name[cat_name][process][x.split(' ')[0].split('_' + process)[0]]['up'] = float(x.split(' ')[1].replace('\n', ''))

    except IOError:
        print(file, "not found; intentional?")
    except IndexError:
        print(file, "missing some syst, intentional?")

        #TODO properly fix missing files or missing entries

def printXML(yield_dict, syst_res_dict, syst_scale_dict, syst_exp_yield_dict, syst_th_res_dict, syst_th_xs_dict, syst_th_acc_dict, syst_th_yield_dict, syst_th_PS_dict, ss_dict, output_dir, kl, param = 0, mu = 0):
    #print yields in format useful for Hongtaoworkspacebuilder
    for category_name, process_dict in yield_dict.items():

        #get total yield in category to help with pruning
        total_yield = 0
        total_yield_pruned = 0
        for process_name, process_yield in yield_dict[category_name].items():
            if not process_name == 'data':
                total_yield += process_yield
        #print 'Total yield is', total_yield, 'for', category_name

    #1 workspace for each klambda
        if param == 0: filename = '%s/category_%s%s_%s.xml' %(output_dir, kl, '_mu' if mu == 1 else '', category_name)
        else: filename = '%s/category_%s%s_%s.xml' %(output_dir, 'param', '_mu' if mu == 1 else '', category_name)
        if not os.path.exists(os.path.dirname(filename)): os.makedirs(os.path.dirname(filename))
        with open(filename, 'w') as outfile:
            outfile.write('<!DOCTYPE Channel SYSTEM \'AnaWSBuilder.dtd\'>\n')
            outfile.write('<Channel Name="' + category_name + '" Type="shape" Lumi="1">\n')

            #data or MC input for background
            outfile.write('  <Data InputFile="config/data/mass_points_data_' + category_name + '.txt" Observable="atlas_invMass_' + category_name + '[105,160]" Binning="550" InjectGhost="1" BlindRange="120,130"/>\n\n')
            #outfile.write('  <Data InputFile="config/data/mass_points_data_' + category_name + '.root" FileType="histogram" HistName="h_print_myy" Observable="atlas_invMass_' + category_name + '[105,160]" Binning="550" InjectGhost="1" BlindRange="120,130"/>\n\n')

            #outfile.write('  <Data InputFile="config/MC/SS_templates.root" FileType="histogram" HistName="h_'%(workspace_name) + category_name + '" Observable="atlas_invMass[105,160]" Binning="55" InjectGhost="1" BlindRange="120,130"/>\n\n')


            outfile.write('  <Systematic Name="ATLAS_LHCmass" Constr="gaus" CentralValue="1" Mag="0.00191861859461" WhereTo="shape"/>\n')
            outfile.write('  <Systematic Name="ATLAS_lumi_run2" Constr="logn" CentralValue="1" Mag="0.017" WhereTo="yield"/>\n\n')
            outfile.write('  <Systematic Name="THEO_BR_Hyy" Constr="asym" CentralValue="1" Mag="0.0290,0.0284" WhereTo="yield"/>\n\n')

            #Impact on HH shape
            #Resolution
            for shape_group in ['HH_ggF', 'HH_VBF', 'ttH', 'ggH', 'ZH']:
                for sys_name, sys_val in syst_res_dict[category_name][shape_group].items():
                    if sys_name != 'ATLAS_EG_RESOLUTION_ALL': continue
                    if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_RES_PRUNE_THRESHOLD: continue
                    outfile.write('  <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape" Process="%s"/>\n' %(sys_name, sys_val['up'], sys_val['down'], shape_group))
                for sys_name, sys_val in syst_scale_dict[category_name][shape_group].items():
                    if sys_name != 'ATLAS_EG_SCALE_ALL': continue
                    if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_SCALE_PRUNE_THRESHOLD: continue
                    outfile.write('  <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape" Process="%s"/>\n' %(sys_name, sys_val['up'], sys_val['down'], shape_group))

            outfile.write('\n')

            '''
            sys_name_string = ''
            for sys_name, sys_val in syst_res_dict[category_name]['HH'].items():
                if sys_name != 'EG_RESOLUTION_ALL': continue
                if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_RES_PRUNE_THRESHOLD: continue
                outfile.write('  <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up'], sys_val['down']))
                sys_name_string += 'response::%s,' %sys_name
            #print syst_th_res_dict[category_name]
            for sys_name, sys_val in syst_th_res_dict[category_name]['HH'].items():
                if abs(sys_val['up']) < SYST_RES_PRUNE_THRESHOLD: continue
                outfile.write('  <Systematic Name="%s" Constr="gaus" CentralValue="1" Mag="%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up']))
                sys_name_string += 'response::%s,' %sys_name
            outfile.write('  <Item Name="prod::resp_RES(%s)"/>\n\n' %sys_name_string[:-1])

            #Scale
            sys_name_string = ''
            for sys_name, sys_val in syst_scale_dict[category_name]['HH'].items():
                if sys_name != 'EG_SCALE_ALL': continue
                if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_SCALE_PRUNE_THRESHOLD: continue
                outfile.write('  <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up'], sys_val['down']))
                sys_name_string += 'response::%s,' %sys_name
            outfile.write('  <Item Name="prod::resp_SCALE(%s)"/>\n\n' %sys_name_string[:-1])
            '''
            '''
            #Impact on H shape
            #Resolution
            sys_name_string = ''
            for sys_name, sys_val in syst_res_dict[category_name]['H'].items():
                if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_RES_PRUNE_THRESHOLD: continue
                outfile.write('  <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up'], sys_val['down']))
                sys_name_string += 'response::%s,' %sys_name
            for sys_name, sys_val in syst_th_res_dict[category_name]['H'].items():
                if abs(sys_val) < SYST_RES_PRUNE_THRESHOLD: continue
                outfile.write('  <Systematic Name="%s" Constr="gaus" CentralValue="1" Mag="%s" WhereTo="shape"/>\n' %(sys_name, sys_val))
                sys_name_string += 'response::%s,' %sys_name
            outfile.write('  <Item Name="prod::resp_RES(%s)"/>\n\n' %sys_name_string[:-1])

            #Scale
            sys_name_string = ''
            for sys_name, sys_val in syst_scale_dict[category_name]['H'].items():
                if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_SCALE_PRUNE_THRESHOLD: continue
                outfile.write('  <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up'], sys_val['down']))
                sys_name_string += 'response::%s,' %sys_name
            outfile.write('  <Item Name="prod::resp_SCALE(%s)"/>\n\n' %sys_name_string[:-1])
            '''

            for process_name, process_eff in process_dict.items():

                if process_name == 'data' or 'fwdH' in process_name: continue
                if (isHH(process_name)) and not kl in process_name: continue
                process_name_short = process_name #for parametrized workspace 
                #if (isHH(process_name)) and param == 1: process_name_short = process_name.split('_kl')[0]
                if (isHH(process_name)): process_name_short = process_name.split('_kl')[0]
                if process_eff == 0: continue
                #if process_eff < 0.001: continue
                #if yield_dict[category_name][process_name] < total_yield * SIG_PRUNE_THRESHOLD: continue        
                #no longer prune the yield of signals. But instead prune their associated systematics.

                #outfile.write('  <Sample Name="' + process_name + '" XSection="1" SelectionEff="1" InputFile="config/model/signal_' + category_name + '.xml" ImportSyst=":common:" SharePdf="commonSig" MultiplyLumi="1">\n'%(workspace_name))
                if isHH(process_name) and 'ggF' in process_name: 
                    outfile.write('  <Sample Name="' + process_name_short + '" XSection="1" SelectionEff="1" InputFile="config/models/signal_HH_ggF_' + category_name + '.xml" ImportSyst=":common:,HH_ggF" MultiplyLumi="1">\n')
                elif isHH(process_name) and 'VBF' in process_name: 
                    outfile.write('  <Sample Name="' + process_name_short + '" XSection="1" SelectionEff="1" InputFile="config/models/signal_HH_VBF_' + category_name + '.xml" ImportSyst=":common:,HH_VBF" MultiplyLumi="1">\n')
                elif process_name == 'ttH' or process_name == 'ggH' or process_name == 'ZH':
                    outfile.write('  <Sample Name="' + process_name_short + '" XSection="1" SelectionEff="1" InputFile="config/models/signal_' + process_name_short + '_' + category_name + '.xml" ImportSyst=":common:,%s" MultiplyLumi="1">\n' %(process_name_short))
                else:
                    outfile.write('  <Sample Name="' + process_name_short + '" XSection="1" SelectionEff="1" InputFile="config/models/signal_H_' + category_name + '.xml" ImportSyst=":common:" MultiplyLumi="1">\n')

                '''
                if isHH(process_name): 

                    for sys_name, sys_val in syst_res_dict[category_name]['HH'].items():
                        if sys_name != 'ATLAS_EG_RESOLUTION_ALL': continue
                        if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_RES_PRUNE_THRESHOLD: continue
                        outfile.write('    <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up'], sys_val['down']))

                    for sys_name, sys_val in syst_scale_dict[category_name]['HH'].items():
                        if sys_name != 'ATLAS_EG_SCALE_ALL': continue
                        if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_SCALE_PRUNE_THRESHOLD: continue
                        outfile.write('    <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up'], sys_val['down']))

                elif process_name == 'ggH' or process_name == 'ttH' or process_name == 'ZH':

                    for sys_name, sys_val in syst_res_dict[category_name][process_name].items():
                        if sys_name != 'ATLAS_EG_RESOLUTION_ALL': continue
                        if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_RES_PRUNE_THRESHOLD: continue
                        outfile.write('    <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up'], sys_val['down']))

                    for sys_name, sys_val in syst_scale_dict[category_name][process_name].items():
                        if sys_name != 'ATLAS_EG_SCALE_ALL': continue
                        if max(abs(sys_val['up']), abs(sys_val['down'])) < SYST_SCALE_PRUNE_THRESHOLD: continue
                        outfile.write('    <Systematic Name="%s" Constr="asym" CentralValue="1" Mag="%s,%s" WhereTo="shape"/>\n' %(sys_name, sys_val['up'], sys_val['down']))
                '''
                if yield_dict[category_name][process_name] > total_yield * SIG_PRUNE_THRESHOLD:
                        total_yield_pruned += yield_dict[category_name][process_name]
                        #HF uncertainty
                        if process_name == 'ggH':
                            outfile.write('    <Systematic Name="ATLAS_HF_ggH" Constr="logn" CentralValue="1" Mag="1.0" WhereTo="yield"/>\n')
                        if process_name == 'VBF':                                                                                              
                            outfile.write('    <Systematic Name="ATLAS_HF_VBF" Constr="logn" CentralValue="1" Mag="1.0" WhereTo="yield"/>\n')
                        elif process_name == 'WH':
                            outfile.write('    <Systematic Name="ATLAS_HF_WH" Constr="logn" CentralValue="1" Mag="1.0" WhereTo="yield"/>\n')

                        #PS systematic
                        '''
                        if HComb_to_prodmode(process_name) == 'bbH' or HComb_to_prodmode(process_name) == 'tHW' or HComb_to_prodmode(process_name) == 'tHqb':
                            pass
                        else: 
                            ps_value = syst_ps_dict[HComb_to_prodmode(process_name)][category_map[int(category_name.split('hybrid')[1])]][process_name][DEFF_OR_DNRT]['val']
                            if abs(ps_value) > SYST_PRUNE_THRESHOLD:
                                outfile.write('    <Systematic Name="UEPS_%s" Constr="gaus" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(HComb_to_prodmode(process_name), ps_value))
                        '''
                        #QCDScale and PDFAlphaS and FxFx
                        if isHH(process_name): 
                            outfile.write('    <Systematic Name="THEO_BR_Hbb" Constr="asym" CentralValue="1" Mag="0.0170,0.0173" WhereTo="yield"/>\n')
                            #XS uncertainties on signal
                            if mu == 1:
                                if 'ggF' in process_name:
                                    outfile.write('    <Systematic Name="THEO_XS_SCALE_%s" Constr="asym" CentralValue="1" Mag="0.0478,-0.0846" WhereTo="yield"/>\n' %process_name_short)
                                    outfile.write('    <Systematic Name="THEO_XS_PDFalphas_%s" Constr="logn" CentralValue="1" Mag="0.03" WhereTo="yield"/>\n' %process_name_short)
                                    outfile.write('    <Systematic Name="THEO_XS_Mtop_%s" Constr="logn" CentralValue="1" Mag="0.026" WhereTo="yield"/>\n' %process_name_short)
                                elif 'VBF' in process_name: 
                                    outfile.write('    <Systematic Name="THEO_XS_SCALE_%s" Constr="asym" CentralValue="1" Mag="0.0003,-0.0004" WhereTo="yield"/>\n' %process_name_short)
                                    outfile.write('    <Systematic Name="THEO_XS_PDFalphas_%s" Constr="logn" CentralValue="1" Mag="0.021" WhereTo="yield"/>\n' %process_name_short)
                            if 'ggF' in process_name and (param or (not kl == 'kl1p0')):
                                outfile.write('    <Systematic Name="ATLAS_KLAMBDA_REWEIGHTING_%s" Constr="logn" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(process_name_short, kl_reweighting_syst[category_name]))
                        #effects on eff or (yield for single H)
                        category_dict = []
                        if isHH(process_name) and 'ggF' in process_name: category_dict = syst_th_acc_dict[category_name]['HH']
                        elif process_name == 'ZH': category_dict = syst_th_yield_dict[category_name][process_name_short] #to also include ttH when those are available 
                        elif process_name == 'ttH': category_dict = syst_th_yield_dict[category_name][process_name_short] #done!!
                        #print syst_th_acc_dict
                        if not category_dict == []:
                            for syst_name, syst_value in category_dict.items():  #format as [down, up, type]
                                if abs(syst_value['up']) < SYST_PRUNE_THRESHOLD: continue
                                if process_name == 'ZH' or process_name == 'ttH':
                                    outfile.write('    <Systematic Name="%s_%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name.replace('ACC', 'YIELD'), process_name_short, 'logn', syst_value['up']))
                                else:
                                    outfile.write('    <Systematic Name="%s_%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name, process_name_short, 'logn', syst_value['up']))

                        #Parton showering
                        category_dict = []
                        if isHH(process_name) and 'ggF' in process_name: category_dict = syst_th_PS_dict[category_name]['HH']
                        elif process_name == 'ZH': category_dict = syst_th_PS_dict[category_name][process_name_short] 
                        elif process_name == 'ttH': category_dict = syst_th_PS_dict[category_name][process_name_short] 
                        #print syst_th_acc_dict
                        if not category_dict == []:
                            for syst_name, syst_value in category_dict.items():  #format as [down, up, type]
                                if abs(syst_value['up']) < SYST_PRUNE_THRESHOLD: continue
                                outfile.write('    <Systematic Name="%s_%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name, process_name_short, 'logn', syst_value['up']))

                        '''
                        if process_name == 'bbH':      #use YR4 values for bbH, tHjb, tWH
                            if XS_OR_MU == 'MU':
                                outfile.write('    <Systematic Name="ATLAS_QCDscalePDFAlphaS_YR4_bbH" Constr="asym" CentralValue="1" Mag="0.201,0.239" WhereTo="yield"/>\n')
                        elif process_name == 'tHjb':
                            if XS_OR_MU == 'MU':
                                outfile.write('    <Systematic Name="ATLAS_QCDscale_YR4_tHjb" Constr="asym" CentralValue="1" Mag="0.065,0.147" WhereTo="yield"/>\n')
                                outfile.write('    <Systematic Name="ATLAS_PDFAlphaS_YR4_tHjb" Constr="asym" CentralValue="1" Mag="0.037,0.037" WhereTo="yield"/>\n')
                        elif process_name == 'tWH':
                            if XS_OR_MU == 'MU':
                                outfile.write('    <Systematic Name="ATLAS_QCDscale_YR4_tWH" Constr="asym" CentralValue="1" Mag="0.049,0.067" WhereTo="yield"/>\n')
                                outfile.write('    <Systematic Name="ATLAS_PDFAlphaS_YR4_tWH" Constr="asym" CentralValue="1" Mag="0.063,0.063" WhereTo="yield"/>\n')
                        else:
                            try: 
                                #maybe not correct
                                if XS_OR_MU == 'MU':
                                    if (isHH(process_name)): category_dict = syst_th_xs_dict[category_name]['HH']
                                    else: category_dict = syst_th_xs_dict[category_name][process_name]
                                    #print category_dict
                                    for syst_name, syst_value in category_dict.items():  #format as [down, up, type]
                                        if syst_name == 'BR_gamgam': continue #we already have this defined above
                                        if abs(syst_value['up']) < SYST_PRUNE_THRESHOLD: continue
                                        if 'down' in syst_value: outfile.write('    <Systematic Name="%s" Constr="%s" CentralValue="1" Mag="%s,%s" WhereTo="yield"/>\n' %(syst_name, 'asym', syst_value['up'], syst_value['down']))
                                        else: outfile.write('    <Systematic Name="%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name, 'logn', syst_value['up']))

                                category_dict = syst_th_acc_dict[category_name][process_name]
                                #print category_dict
                                for syst_name, syst_value in category_dict.items():  #format as [down, up, type]
                                    if abs(syst_value['up']) < SYST_PRUNE_THRESHOLD: continue
                                    outfile.write('    <Systematic Name="%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name, 'logn', syst_value['up']))



                            except KeyError:
                                print 'syst for category', category_name_fixed, 'not found for process', process_name
                        '''
                        #Experimental yield systematics
                        if isHH(process_name) and 'ggF' in process_name: outfile.write('    <ImportItems FileName="config/systs/syst_%s_%s.xml" />\n' %('HH_ggF', category_name))
                        if isHH(process_name) and 'VBF' in process_name: outfile.write('    <ImportItems FileName="config/systs/syst_%s_%s.xml" />\n' %('HH_VBF', category_name))
                        elif process_name == 'ttH' or process_name == 'ggH' or process_name == 'ZH': outfile.write('    <ImportItems FileName="config/systs/syst_%s_%s.xml" />\n' %(process_name_short, category_name))

                #Efficiency and XSBR
                #outfile.write('    <NormFactor Name="eff_' + process_name + '_yy[' +  str(process_eff)  + ']" />\n')
                #outfile.write('    <NormFactor Name="XS13_' + process_name + '_yy[' +  str(xsbr_dict[process_name])  + ']" />\n')
                if ((isHH(process_name)) and 'ggF' in process_name and param == 1):
                    # ggF HH 
                    outfile.write('    <NormFactor Name="expr::yield_HH_ggF(\'@0 + @1*@3 + @2*@3*@3\', HH_ggF_param_p0[%s], HH_ggF_param_p1[%s], HH_ggF_param_p2[%s], klambda[1,-10,10])" />\n' %(ggF_HH_params[category_name][0], ggF_HH_params[category_name][1], ggF_HH_params[category_name][2]))
                elif ((isHH(process_name)) and 'VBF' in process_name and param == 1):
                    # VBF HH 
                    outfile.write('    <NormFactor Name="expr::yield_HH_VBF(\'@0 + @1*@3 + @2*@3*@3\', HH_VBF_param_p0[%s], HH_VBF_param_p1[%s], HH_VBF_param_p2[%s], klambda[1,-10,10])" />\n' %(VBF_HH_params[category_name][0], VBF_HH_params[category_name][1], VBF_HH_params[category_name][2]))
                else:
                    outfile.write('    <NormFactor Name="yield_' + process_name_short + '[' +  str(process_eff)  + ']" />\n')

                #disable by default
                #if 'mzp' in process_name or ((isHH(process_name)) and process_name != 'HH_ggF_kl1p0' and process_name != 'HH_VBF_kl1p0cvv1'):
                #        outfile.write('    <NormFactor Name="mu_XS_' + process_name + '[0]" />\n')
                #else:
                #        outfile.write('    <NormFactor Name="mu_XS_' + process_name + '[1]" />\n')
                outfile.write('    <NormFactor Name="mu_XS_' + process_name_short + '[1]" />\n')


                #Add the 5 POIs
                if process_name.startswith('ZH') or process_name.startswith('WH'):
                    outfile.write('    <NormFactor Name="mu_XS_VH[1]" />\n')
                if process_name.startswith('tHjb') or process_name.startswith('tWH'):
                    outfile.write('    <NormFactor Name="mu_XS_tH[1]" />\n')
                if process_name.startswith('ttH') or process_name.startswith('tHjb') or process_name.startswith('tWH'):
                    outfile.write('    <NormFactor Name="mu_XS_ttHtH[1]" />\n')

                if isHH(process_name):
                    outfile.write('    <NormFactor Name="mu_XS_HH_%s[1]" />\n' %category_name)
                    outfile.write('    <NormFactor Name="mu_XS_HH[1]" />\n')
                if isH(process_name):
                    outfile.write('    <NormFactor Name="mu_XS_H[1]" />\n')

                outfile.write('    <NormFactor Name="mu_yy[1]" />\n')
                outfile.write('    <NormFactor Name="mu[1]" />\n')
                outfile.write('  </Sample>\n\n')


            nss = ss_dict[category_name]
            outfile.write('  <Sample Name="spurious" XSection="1" SelectionEff="1" InputFile="config/models/signal_HH_ggF_' + category_name + '.xml" ImportSyst=":self:" MultiplyLumi="0">\n')
            outfile.write('    <Systematic Name="SPURIOUS_' + category_name + '" Constr="gaus" CentralValue="0" Mag="' + str(nss) + '" WhereTo="yield"/>\n')
            outfile.write('  </Sample>\n\n')

            nbkg = yield_dict[category_name]['data']
            outfile.write('  <Sample Name="background" XSection="1" SelectionEff="1" InputFile="config/models/background_' + category_name + '.xml" ImportSyst=":self:" MultiplyLumi="0">\n')
            outfile.write('    <NormFactor Name="nbkg_' + category_name + '[' + str(nbkg) + ',0,10000000]"/>\n')
            outfile.write('  </Sample>\n\n')

            outfile.write('</Channel>')

            #print 'Category', category_name, 'total pruned yield: ', total_yield_pruned, 'vs total yield: ', total_yield, '(', total_yield_pruned/total_yield * 100, ')'

def printXMLbkg(output_dir):
    #print the background cards
    for category_name in category_map.keys():
        if not os.path.exists(output_dir): os.makedirs(output_dir)
        file = open('%s/background_%s.xml' %(output_dir, category_name), "w")
        file.write('<!DOCTYPE Model SYSTEM \'AnaWSBuilder.dtd\'>\n')
        file.write('<Model Type=\"UserDef\">\n')
        file.write('  <ModelItem Name=\"EXPR::bkgPdf_%s(\'exp((@0-100)*@1/100)\', :observable:,  BKG_p0_%s[-1,-1000,1000])\"/>\n' %(category_name, category_name))
        file.write("</Model>\n")
        file.close()

DEFAULT_MODEL_PARAMETERS = {
                                'SM_1': {
                                    "muCBNom" : 125.09,
                                    "sigmaCBNom" : 1.3257,
                                    "alphaCBLo" : 1.3995,
                                    "nCBLo" : 13.428,
                                    "alphaCBHi" : 1.4617,
                                    "nCBHi" : 38.479,
                                },
                                'SM_2': {
                                    "muCBNom" : 125.13,
                                    "sigmaCBNom" : 1.4654,
                                    "alphaCBLo" : 1.4004,
                                    "nCBLo" : 11.227,
                                    "alphaCBHi" : 1.3981,
                                    "nCBHi" : 35.703,
                                },
                                'BSM_1': {
                                    "muCBNom" : 125.15,
                                    "sigmaCBNom" : 1.4995,
                                    "alphaCBLo" : 1.2834,
                                    "nCBLo" : 33.399,
                                    "alphaCBHi" : 1.3437,
                                    "nCBHi" : 37.929,
                                },
                                'BSM_2': {
                                    "muCBNom" : 125.08,
                                    "sigmaCBNom" : 1.6436,
                                    "alphaCBLo" : 1.5120,
                                    "nCBLo" : 16.392,
                                    "alphaCBHi" : 1.3689,
                                    "nCBHi" : 118.83,
                                }
                            }

def printXMLsig(output_dir, model_parameters=None):
    if model_parameters is None:
        model_parameters = DEFAULT_MODEL_PARAMETERS
    for sig in ['H', 'HH_ggF', 'HH_VBF', 'ggH', 'ttH', 'ZH']: 
        for category_name in category_map.keys():
            if not os.path.exists(output_dir): os.makedirs(output_dir)
            file = open('%s/signal_%s_%s.xml' %(output_dir, sig, category_name), "w")
            file.write('<!DOCTYPE Model SYSTEM \'AnaWSBuilder.dtd\'>\n')
            file.write('<Model Type=\"UserDef\" CacheBinning="100000">\n')
            file.write('  <Item Name=\"mHNom[125.09]\"/>\n')
            file.write('  <Item Name=\"prod::mH(mHNom, response::ATLAS_LHCmass)\"/>\n')

            if sig == 'H':
                file.write('  <Item Name=\"expr::muCB_%s(\'%s-125+@0\',mH)\"/>\n' %(sig, model_parameters[category_name]["muCBNom"]))
                file.write('  <Item Name=\"expr::sigmaCB_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["sigmaCBNom"]))
            else: 
                file.write('  <Item Name=\"expr::muCBNom_%s(\'%s-125+@0\',mH)\"/>\n' %(sig, model_parameters[category_name]["muCBNom"]))
                file.write('  <Item Name=\"prod::muCB_%s(muCBNom_%s, response::ATLAS_EG_SCALE_ALL_%s)\"/>\n' %(sig, sig, sig))
                file.write('  <Item Name=\"expr::sigmaCBNom_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["sigmaCBNom"]))
                file.write('  <Item Name=\"prod::sigmaCB_%s(sigmaCBNom_%s, response::ATLAS_EG_RESOLUTION_ALL_%s)\"/>\n' %(sig, sig, sig))

            file.write('  <Item Name=\"expr::alphaCBLo_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["alphaCBLo"]))
            file.write('  <Item Name=\"expr::alphaCBHi_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["alphaCBHi"]))
            file.write('  <Item Name=\"expr::nCBLo_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["nCBLo"]))
            file.write('  <Item Name=\"expr::nCBHi_%s(\'%s\',mH)\"/>\n' %(sig, model_parameters[category_name]["nCBHi"]))
            file.write('  <ModelItem Name=\"RooTwoSidedCBShape::signal(:observable:, muCB_%s, sigmaCB_%s, alphaCBLo_%s, nCBLo_%s, alphaCBHi_%s, nCBHi_%s)"/>\n' %(sig, sig, sig, sig, sig, sig))
            file.write('</Model>')
            file.close()

def printXMLsystexpyield(syst_exp_yield_dict, output_dir):
    #print the experimental yield systematics
    for category_name in category_map.keys():
        category_dict = syst_exp_yield_dict[category_name]
        for process_name, process_dict in category_dict.items():
            if not os.path.exists(output_dir): os.makedirs(output_dir)
            file = open('%s/syst_%s_%s.xml' %(output_dir, process_name, category_name), "w")
            file.write('<!DOCTYPE SampleItems SYSTEM \'AnaWSBuilder.dtd\'>\n')
            file.write('<SampleItems>\n')
            for syst_name, syst_value in process_dict.items(): 
                #if syst_value['constr'] == 'asym':
                if max(abs(syst_value['up']), abs(syst_value['down'])) < SYST_PRUNE_THRESHOLD: continue
                file.write('  <Systematic Name="%s" Constr="%s" CentralValue="1" Mag="%s,%s" WhereTo="yield"/>\n' %(syst_name, 'asym', syst_value['up'], syst_value['down']))
                #else: #gaus or logn
                #    if abs(syst_value['up']) < SYST_PRUNE_THRESHOLD: continue
                #    file.write('  <Systematic Name="%s" Constr="%s" CentralValue="1" Mag="%s" WhereTo="yield"/>\n' %(syst_name, syst_value['constr'], syst_value['up']))
            file.write('</SampleItems>\n')
            file.close()

def printXMLtop(output_dir, kl, param = 0, mu = 0):
    if param: kl = 'param'
    filename = '%s/input_%s%s.xml' %(output_dir, kl, '_mu' if mu == 1 else '')
    if not os.path.exists(output_dir): os.makedirs(output_dir)
    file = open(filename, "w")
    file.write('<!DOCTYPE Combination  SYSTEM \'AnaWSBuilder.dtd\'>\n')
    file.write('<Combination WorkspaceName="combWS" ModelConfigName="ModelConfig" DataName="combData" OutputFile="workspace/WS-yybb-non-resonant_%s%s.root" Blind="0">\n' %(kl, '_mu' if mu == 1 else ''))
    file.write('\n')
    file.write('  <Input>config/categories/category_%s_SM_1.xml</Input>\n' %(kl + '_mu' if mu == 1 else kl))
    file.write('  <Input>config/categories/category_%s_SM_2.xml</Input>\n' %(kl + '_mu' if mu == 1 else kl))
    file.write('  <Input>config/categories/category_%s_BSM_1.xml</Input>\n' %(kl + '_mu' if mu == 1 else kl))
    file.write('  <Input>config/categories/category_%s_BSM_2.xml</Input>\n' %(kl + '_mu' if mu == 1 else kl))
    file.write('\n')

    if param:
        file.write('  <POI>mu_XS_HH,mu_XS_HH_SM_1,mu_XS_HH_SM_2,mu_XS_HH_BSM_1,mu_XS_HH_BSM_2,mu_XS_HH_ggF,mu_XS_HH_VBF,mu,mu_yy,mu_XS_H,mu_XS_VH,mu_XS_ttHtH,mu_XS_tH,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH,klambda</POI> \n')
        file.write('\n')
        file.write('  <Asimov Name="setup"   Setup="klambda=1,mu_XS_HH_ggF=1,mu_XS_HH_VBF=1"     Action=""/> \n')
    elif kl == 'kl1p0' or kl == 'kl10p0' or kl == 'kl0p0' or kl == 'kl2p0':
        file.write('  <POI>mu_XS_HH,mu_XS_HH_SM_1,mu_XS_HH_SM_2,mu_XS_HH_BSM_1,mu_XS_HH_BSM_2,mu_XS_HH_ggF,mu_XS_HH_VBF,mu,mu_yy,mu_XS_H,mu_XS_VH,mu_XS_ttHtH,mu_XS_tH,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH</POI> \n')
        file.write('\n')
        file.write('  <Asimov Name="setup"   Setup="mu_XS_HH_ggF=1,mu_XS_HH_VBF=1"     Action=""/> \n')
    else: 
        file.write('  <POI>mu_XS_HH,mu_XS_HH_SM_1,mu_XS_HH_SM_2,mu_XS_HH_BSM_1,mu_XS_HH_BSM_2,mu_XS_HH_ggF,mu,mu_yy,mu_XS_H,mu_XS_VH,mu_XS_ttHtH,mu_XS_tH,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH</POI> \n')
        file.write('\n')
        file.write('  <Asimov Name="setup"   Setup="mu_XS_HH_ggF=1"     Action=""/> \n')
     #if kl == 'kl1p0':
     #   file.write('  <POI>mu_XS_HH,mu_XS_HH_ggF_%s,mu_XS_HH_VBF_%scvv1,mu,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH</POI>\n' %(kl, kl))
     #   file.write('\n')
     #   file.write('  <Asimov Name="setup"   Setup="mu_XS_HH_ggF_%s=1,mu_XS_HH_VBF_%scvv1=1"     Action=""/> \n' %(kl, kl))
#    if kl == 'kl1p0':
#        file.write('  <POI>mu_XS_HH,mu_XS_HH_ggF_%s,mu_XS_HH_VBF_%scvv1,mu_XS_HH_VBF_%scvv0,mu,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH</POI>\n' %(kl, kl, kl))
#        file.write('\n')
#        file.write('  <Asimov Name="setup"   Setup="mu_XS_HH_ggF_%s=1,mu_XS_HH_VBF_%scvv1=1,mu_XS_HH_VBF_%scvv0=0"     Action=""/> \n' %(kl, kl, kl))
    #elif kl == 'kl10p0':
    #    file.write('  <POI>mu_XS_HH,mu_XS_HH_ggF_%s,mu_XS_HH_VBF_%scvv1,mu,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH</POI>\n' %(kl, kl))
    #    file.write('\n')
    #    file.write('  <Asimov Name="setup"   Setup="mu_XS_HH_ggF_%s=1,mu_XS_HH_VBF_%scvv1=1"     Action=""/> \n' %(kl, kl))
    #else:
    #    file.write('  <POI>mu_XS_HH,mu_XS_HH_ggF_%s,mu,mu_XS_ggH,mu_XS_VBF,mu_XS_WH,mu_XS_ZH,mu_XS_ttH,mu_XS_tHjb,mu_XS_tWH</POI>\n' %kl)
    #    file.write('\n')
    #    file.write('  <Asimov Name="setup"   Setup="mu_XS_HH_ggF_%s=1"     Action=""/> \n' %kl)

    #file.write('  <Asimov Name="asimovData_0"   Setup="mu=0,mu_XS_HH=1"     Action="fixsyst:fit:genasimov:float:savesnapshot" SnapshotNuis="nominalNuis" SnapshotGlob="nominalGlob"/>\n')
    #file.write('  <Asimov Name="combData"   Setup="mu=1,mu_XS_HH=0"     Action="genasimov"/>\n')

    file.write('  <Asimov Name="POISnap"  Setup=""  Action="savesnapshot" SnapshotPOI="nominalPOI"/> \n')
    file.write('  <Asimov Name="NPSnap"   Setup="mu=1"     Action="fixsyst:fit:float:savesnapshot:nominalPOI" SnapshotNuis="nominalNuis" SnapshotGlob="nominalGlobs"/> \n')
    file.write('  <Asimov Name="asimovData_0" Setup="mu=1,mu_XS_HH=0"      Action="fit:genasimov:matchglob:savesnapshot:nominalGlobs" SnapshotGlob="conditionalGlobs_0" SnapshotNuis="conditionalNuis_0"/> \n')
    file.write('  <Asimov Name="UCMLES0Snap" Setup=""         Action="savesnapshot" SnapshotAll="ucmles_0"/> \n')
    file.write('  <Asimov Name="asimovData_1" Setup="mu=1,mu_XS_HH=1"      Action="fit:genasimov:matchglob:savesnapshot:nominalGlobs" SnapshotGlob="conditionalGlobs_1" SnapshotNuis="conditionalNuis_1"/> \n')
    file.write('  <Asimov Name="UCMLES1Snap" Setup=""         Action="savesnapshot" SnapshotAll="ucmles_1"/> \n')
    '''
    if param: 
        file.write('  <Asimov Name="asimovData_HH_kl1p0"   Setup="mu=1,mu_XS_HH=1"     Action="genasimov"/>\n')
        file.write('  <Asimov Name="asimovData_HH_kl10p0"   Setup="klambda=10,mu=1,mu_XS_HH=1"     Action="genasimov:reset"/>\n')
        file.write('  <Asimov Name="asimovData_HH_kl0p0"   Setup="klambda=0,mu=1,mu_XS_HH=1"     Action="genasimov:reset"/>\n')
        file.write('  <Asimov Name="asimovData_HH_kl2p0"   Setup="klambda=2,mu=1,mu_XS_HH=1"     Action="genasimov:reset"/>\n')
    else: file.write('  <Asimov Name="asimovData_HH_%s"   Setup="mu=1,mu_XS_HH=1"     Action="genasimov"/>\n' %kl)
    '''
    file.write('  <Asimov Name="setup2" Setup="mu_XS_HH=0" Action="nominalNuis:nominalGlobs"/> \n')
    file.write('\n')
    file.write('</Combination>')

def create_non_res_xmls(yield_path, model_parameters_path, database_path, outdir="yybb_non_resonant"):
    
    #prepare the inputs
    print('INFO: Loading information')

    #load efficiencies
    #eff_dict = yaml.load(open('coupling2020_hybrid/efficiencies.yaml', 'r'), Loader=yaml.FullLoader)

    #load XS*BRs
    #xsbr_dict = yaml.load(open('/afs/cern.ch/work/a/alwang/FullRun2/stxs_cross_sections/xs_coarse.yaml', 'r'), Loader=yaml.FullLoader)

    #load yields
    with open(yield_path, 'r') as yield_file:
        yield_dict = yaml.safe_load(yield_file)
        
    #load signal model parameters
    with open(model_parameters_path, 'r') as param_file:
        model_parameters = json.load(param_file) 
        
    marc_syst_path = os.path.join(database_path, 'NonResonant_IJCLab', 'h026_baseline_4categories_ggH')
    #load experimental systematics
    syst_res_dict = nested_dict()
    syst_scale_dict = nested_dict()
    syst_exp_yield_dict = nested_dict()
    exp_syst_path = os.path.join(marc_syst_path, 'Experimental_systematics')
    for category in category_map: #to be added: VBFHH
        load_Marc_txt(syst_res_dict, category, 'HH_ggF', '%s/version_with_gg_HH_from_Py8/datacard_experimental_spread_shape_m_yy_proc_gg_HH_non_resonant_Max_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'HH_ggF', '%s/version_with_gg_HH_from_Py8/datacard_experimental_position_shape_m_yy_proc_gg_HH_non_resonant_Max_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'HH_ggF', '%s/version_with_gg_HH_from_Py8/datacard_experimental_yield_proc_gg_HH_non_resonant_Max_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))

        load_Marc_txt(syst_res_dict, category, 'HH_VBF', '%s/datacard_experimental_spread_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_vbf_HH_non_resonant_kappa_lambda_01_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'HH_VBF', '%s/datacard_experimental_position_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_vbf_HH_non_resonant_kappa_lambda_01_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'HH_VBF', '%s/datacard_experimental_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_vbf_HH_non_resonant_kappa_lambda_01_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))

        load_Marc_txt(syst_res_dict, category, 'ttH', '%s/datacard_experimental_spread_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'ttH', '%s/datacard_experimental_position_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'ttH', '%s/datacard_experimental_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_res_dict, category, 'ggH', '%s/datacard_experimental_spread_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_NNLOPS_ggH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'ggH', '%s/datacard_experimental_position_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_NNLOPS_ggH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'ggH', '%s/datacard_experimental_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_NNLOPS_ggH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_res_dict, category, 'ZH', '%s/datacard_experimental_spread_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_ZH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_scale_dict, category, 'ZH', '%s/datacard_experimental_position_shape_m_yy_proc_h026_mc16a_h026_mc16d_h026_mc16e_ZH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))
        load_Marc_txt(syst_exp_yield_dict, category, 'ZH', '%s/datacard_experimental_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_ZH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(exp_syst_path, category_map[category]))

    #theoretical
    theo_syst_path = os.path.join(marc_syst_path, 'Theoretical_systematics')
    syst_th_res_dict = nested_dict()
    syst_th_xs_dict = nested_dict()
    syst_th_acc_dict = nested_dict()
    syst_th_yield_dict = nested_dict()
    syst_th_PS_dict = nested_dict()

    for category in category_map: 
        #for now: use values for klambda = 1 for all HH values
        #no spread
        #load_Marc_txt(syst_th_res_dict, category, 'HH', '%s/datacard_spread_shape_m_yy_h025_mc16a_h025_mc16d_h025_mc16e_PowhegH7_HHbbyy_cHHH01d0_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        #load_Marc_txt(syst_th_res_dict, category, 'HH_ggF_kl1p0', '%s/datacard_spread_shape_m_yy_h025_mc16a_h025_mc16d_h025_mc16e_PowhegH7_HHbbyy_cHHH01d0_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        #load_Marc_txt(syst_th_res_dict, category, 'HH_ggF_kl10p0', '%s/datacard_spread_shape_m_yy_h025_mc16a_h025_mc16d_h025_mc16e_PowhegH7_HHbbyy_cHHH10d0_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        #load_Marc_txt(syst_th_res_dict, category, 'H', '%s/datacard_spread_shape_m_yy_h025_mc16a_h025_mc16d_h025_mc16e_SingleHiggs_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))

        #load_Marc_txt(syst_th_xs_dict, category, 'HH', '%s/datacard_yield_cross_section_gg_HH_kappa_lambda_01.txt' %(theo_syst_path))
        #HH: m_top 2.6, QCD_XS_GGHH_4.78, -8.46, PDF alphaS XS: 3.0

        #load_Marc_txt(syst_th_xs_dict, category, 'HH_kl1p0', '%s/datacard_yield_cross_section_gg_HH_kappa_lambda_01.txt' %(theo_syst_path))
        #load_Marc_txt(syst_th_xs_dict, category, 'HH_kl10p0', '%s/datacard_yield_cross_section_gg_HH_kappa_lambda_10.txt' %(theo_syst_path))
        #load_Marc_txt(syst_th_xs_dict, category, 'HH_kl10p0', '%s/datacard_yield_cross_section_vbf_HH_kappa_lambda_01.txt' %(theo_syst_path))

        #load_Marc_txt(syst_th_xs_dict, category, 'ggH', '%s/SingleHiggs_version_yield/datacard_yield_cross_section_ggH.txt' %(theo_syst_path))
        #load_Marc_txt(syst_th_xs_dict, category, 'ttH', '%s/SingleHiggs_version_yield/datacard_yield_cross_section_ttH.txt' %(theo_syst_path))
        #load_Marc_txt(syst_th_xs_dict, category, 'ZH', '%s/SingleHiggs_version_yield/datacard_yield_cross_section_ZH.txt' %(theo_syst_path))

        load_Marc_txt(syst_th_acc_dict, category, 'HH', '%s/HH_version_efficiency/version_gg_HH_Py8/datacard_theory_yield_proc_gg_HH_non_resonant_Max_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        #load_Marc_txt(syst_th_acc_dict, category, 'HH_ggF_kl1p0', '%s/datacard_theory_yield_h026_mc16a_h026_mc16d_h026_mc16e_HH_non_resonant_kappa_lambda_01_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        #load_Marc_txt(syst_th_acc_dict, category, 'HH_ggF_kl10p0', '%s/datacard_theory_yield_h026_mc16a_h026_mc16d_h026_mc16e_HH_non_resonant_kappa_lambda_10_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        load_Marc_txt(syst_th_yield_dict, category, 'ttH', '%s/SingleHiggs_version_yield/datacard_theory_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_ttH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        #load_Marc_txt(syst_th_acc_dict, category, 'ggH', '%s/datacard_theory_yield_h026_mc16a_h026_mc16d_h026_mc16e_PowhegPy8_NNLOPS_ggH125_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))
        load_Marc_txt(syst_th_yield_dict, category, 'ZH', '%s/SingleHiggs_version_yield/datacard_theory_yield_proc_h026_mc16a_h026_mc16d_h026_mc16e_ZH125_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))

        load_Marc_txt(syst_th_PS_dict, category, 'HH', '%s/datacard_theory_yield_parton_showering_proc_HH_non_resonant_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(theo_syst_path, category_map[category]))

    ps_syst_path = os.path.join(database_path, 'NonResonant_Wisc', 'SingleHiggs_PS')
    for category in category_map: 
        load_Alex_txt(syst_th_PS_dict, category, 'ttH', '%s/datacard_yield_ttH_%s' %(ps_syst_path, category_map[category]))
        load_Alex_txt(syst_th_PS_dict, category, 'ZH', '%s/datacard_yield_ZH_%s' %(ps_syst_path, category_map[category]))

    #print(syst_th_PS_dict)

    ss_syst_path = os.path.join(marc_syst_path, 'BkgModelling_SpuriousSignal')
    ss_dict = {}
    for category in category_map: 
        f = open('%s/version_where_gg_part_of_shape_is_Py8/spurious_signal_spurious_signal_cat_XGBoost_btag77_withTop_BCal_%s.txt' %(ss_syst_path, category_map[category]))
        lines = f.readlines()
        for x in lines:
            ss_dict[category] = float(x.replace('\n', ''))
    '''

    #load bkg function and ss 
    bkgfunc_dict = OrderedDict()
    ss_dict = OrderedDict()

    f = open('/afs/cern.ch/work/a/alwang/FullRun2/SS_results.txt', 'r')
    lines = f.readlines()
    result = []
    for x in lines:
        cat = 'hybrid' + x.split(':')[0]
        #print(x.split())
        bkgfunc = x.split()[2]
        bkgfunc_dict[cat] = bkgfunc
        #if bkgfunc == 'NoPDF' or 'Bern' in bkgfunc: 
        #    bkgfunc_dict[cat] = 'Exp2'
        #    ss_dict[cat] = 0    
        #else: 
        ss_dict[cat] = x.split()[3]
    f.close()
    '''
    print('INFO: Printing category cards')

    lambdas = np.linspace(-10, 10, 101)
    xml_path = os.path.join(outdir, 'config')
    category_xml_path = os.path.join(xml_path, 'categories')
    '''
    for kl in lambdas:
        kl_text = 'kl%.1f'%kl
        kl_text = kl_text.replace('.', 'p').replace('-', 'n')
        printXML(yield_dict, syst_res_dict, syst_scale_dict, syst_exp_yield_dict, syst_th_res_dict, syst_th_xs_dict, syst_th_acc_dict, syst_th_yield_dict, syst_th_PS_dict, ss_dict, workspace_name, category_xml_path, kl_text, param = 0, mu = 0)
        printXMLtop(xml_path, kl_text, mu = 0)
    
    # mu workspace (including theoretical uncertainties on signal XS)
    printXML(yield_dict, syst_res_dict, syst_scale_dict, syst_exp_yield_dict, syst_th_res_dict, syst_th_xs_dict, syst_th_acc_dict, syst_th_yield_dict, syst_th_PS_dict, ss_dict, workspace_name, category_xml_path, 'kl1p0', param = 0, mu = 1)
    printXMLtop(xml_path, 'kl1p0', param = 0, mu = 1)
    '''
    
    # mu workspace (including theoretical uncertainties on signal XS)
    printXML(yield_dict, syst_res_dict, syst_scale_dict, syst_exp_yield_dict, syst_th_res_dict, syst_th_xs_dict, syst_th_acc_dict, syst_th_yield_dict, syst_th_PS_dict, ss_dict, category_xml_path, 'kl1p0', param = 0, mu = 0)
    printXMLtop(xml_path, 'kl1p0', param = 0, mu = 0)    

    # and for the parametrized workspace: 
    printXML(yield_dict, syst_res_dict, syst_scale_dict, syst_exp_yield_dict, syst_th_res_dict, syst_th_xs_dict, syst_th_acc_dict, syst_th_yield_dict, syst_th_PS_dict, ss_dict, category_xml_path, 'kl1p0', param = 1, mu = 0)
    printXMLtop(xml_path, 'kl1p0', param = 1, mu = 0)

    print('INFO: Printing background model cards')
    model_xml_path = os.path.join(xml_path, 'models')
    printXMLbkg(model_xml_path)

    print('INFO: Printing signal model cards')
    printXMLsig(model_xml_path, model_parameters)

    print('INFO: Printing experimental systematic yield cards')
    syst_xml_path = os.path.join(xml_path, 'systs')
    printXMLsystexpyield(syst_exp_yield_dict, syst_xml_path)

    print('INFO: Done')