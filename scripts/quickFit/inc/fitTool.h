#ifndef FITTOOL_HEADER
#define FITTOOL_HEADER

#include "CommonHead.h"
#include "RooFitHead.h"
#include "RooStatsHead.h"
#include "utils.h"

using namespace std;
using namespace RooFit;
using namespace RooStats;

class fitTool : public TObject{
private:
  TString _minAlgo, _outputFile, _ssName;
  float _minTolerance, _errorLevel;
  bool _nllOffset, _useHESSE, _useSIMPLEX, _saveWS, _fixStarCache, _saveErrors, _saveFitResult, _GKIntegrator, _saveNP, _doSumW2;
  int _minStrat, _optConst, _printLevel, _nCPU, _useMINOS; 
  RooArgSet *_externalConstraint, *_paramList;
  int _status;
  unique_ptr<RooFitResult> _result;
public:
  fitTool();
  void useHESSE( bool flag ) { _useHESSE = flag; };
  void useMINOS( int flag ) { _useMINOS = flag; };
  void useSIMPLEX( bool flag ) { _useSIMPLEX = flag; };
  void setNLLOffset( bool flag ) { _nllOffset = flag; };
  void saveWorkspace( bool flag ) { _saveWS = flag; };
  void saveErrors( bool flag ) { _saveErrors = flag; };
  void saveFitResult( bool flag ) { _saveFitResult = flag; };
  void setFixStarCache( bool flag ) { _fixStarCache = flag; };
  void setTolerance( float val ) { _minTolerance = val; };
  void setNCPU( int val ) { _nCPU = val; };
  void setStrategy( int val ) { _minStrat = val; };
  void setOptConst( int val ) { _optConst = val; };
  void setPrintLevel( int val ) { _printLevel = val; };
  void setOutputFile( TString str ) { _outputFile = str; };
  void setSnapshotName( TString str ) { _ssName = str; };
  void setMinAlgo( TString str ) { _minAlgo = str; };
  void setExternalConstraint( RooArgSet* set ) { _externalConstraint = set; };
  void setParamList( RooArgSet* set ) { _paramList = set; };
  void setGKIntegrator( bool flag ) { _GKIntegrator = flag; };
  void setErrorLevel( float level ) { _errorLevel = level; };
  void setSaveNP( bool flag ) { _saveNP = flag; };
  void setSumW2( bool flag ) { _doSumW2 = flag; };
  bool checkModel(const RooStats::ModelConfig &model, bool throwOnFail=false) ;
  int profileToData(ModelConfig *mc, RooAbsData *data);
  void minimize(RooAbsReal* nll, ModelConfig *mc);
  int status(){return _status;}
  void setBinnedLHAttr(ModelConfig *mc);
  RooAbsReal *createNLL(ModelConfig *mc, RooAbsData *data);
  void setStorageLevel(int level);
  ClassDef(fitTool, 1);
};

#endif
