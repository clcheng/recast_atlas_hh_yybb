#include "ROOTHelper.h"
#include <map>
#include <string>
#include "TFile.h"
#include "TList.h"
#include "TSystemFile.h"
#include "TSystemDirectory.h"

TFile* ROOTHelper::GetTFile(std::string sample,std::string mcset, std::string fileName)
{
  static std::map<std::string,TFile*,std::less<std::string> > tfileMap;
  std::string temp=sample+"_"+mcset;
  if (tfileMap.find(temp) != tfileMap.end())
    return tfileMap[temp];
  else
  {
    TFile* tf=new TFile(fileName.c_str());
    tfileMap[temp]=tf;
    return tf;
  }
}

TList* ROOTHelper::GetListOfFiles(const char *filename) {
    TSystemFile *file = new TSystemFile(filename, "");
    if (file->IsDirectory()){
        TSystemDirectory dir(filename, filename); 
        return dir.GetListOfFiles(); 
    }
    TList *files  = new TList;
    files->SetOwner();
    files->Add(file);
    return files;
}