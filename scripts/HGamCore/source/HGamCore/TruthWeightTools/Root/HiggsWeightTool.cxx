/*
   Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
 */

// EDM include(s):
#include "PathResolver/PathResolver.h"
// #include "xAODEventInfo/EventInfo.h"

// Local include(s):
#include "TruthWeightTools/HiggsWeightTool.h"

// libxml2 include(s):
#include <libxml/xmlreader.h>

// ROOT include(s):
#include "TString.h"
#include <sstream>

namespace TruthWeightTools
{

  HiggsWeightTool::HiggsWeightTool(const std::string &name)
    : asg::AsgTool(name)
    , m_init(false)
    , m_nWeights(-1)
    , m_mcID(999)
    , m_weightTool("PMGTools::PMGTruthWeightTool", this)
    , m_cutOff(false)
    , m_Nnom(0)
    , m_Nws(0)
    , m_sumw_nom(.0)
    , m_sumw2_nom(.0)
    , m_sumw(.0)
    , m_sumw2(.0)
    , m_sumw_nomC(.0)
    , m_sumw2_nomC(.0)
    , m_sumwC(.0)
    , m_sumw2C(.0)
  {
    // Whether to put constraints on the weights
    declareProperty("RequireFinite", m_requireFinite = false);
    declareProperty("WeightCutOff", m_weightCutOff = -1.0);

    // Force modes
    declareProperty("ForceNNLOPS", m_forceNNLOPS = false); // Run2-default Powheg NNLOPS ggF
    declareProperty("ForceVBF",    m_forceVBF = false);    // Run2-default Powheg VBF
    declareProperty("ForceVH",     m_forceVH = false);     // Run2-default Powheg VH (WpH, WmH, qq->ZH)
    declareProperty("ForceTTH",    m_forceTTH = false);    // Run2-default Powheg TTH


    // production mode
    declareProperty("ProdMode",   m_prodMode = "ggF");


  }

  StatusCode HiggsWeightTool::initialize()
  {

    int sum = m_forceNNLOPS + m_forceVBF + m_forceVH + m_forceTTH;

    if (sum > 1) {
      ANA_MSG_ERROR("Must not call more than one of ForceNNLOPS, ForceVBF, ForceVH or ForceTTH");
      return StatusCode::FAILURE;
    }

    if (m_weightCutOff > 0) { m_cutOff = true; m_requireFinite = true; }

    ANA_CHECK(m_weightTool.initialize());

    if (sum == 0) {
      m_mode = AUTO;
      ANA_MSG_INFO("AUTO MODE");
    } else if (m_forceNNLOPS) {
      m_mode = FORCE_GGF_NNLOPS;
      ANA_MSG_INFO("FORCE_GGF_NNLOPS MODE");
    } else if (m_forceVBF) {
      m_mode = FORCE_POWPY8_VBF;
      ANA_MSG_INFO("FORCE_POWPY8_VBF MODE");
    } else if (m_forceVH) {
      m_mode = FORCE_POWPY8_VH;
      ANA_MSG_INFO("FORCE_POWPY8_VH MODE");
    } else if (m_forceTTH) {
      m_mode = FORCE_POWPY8_TTH;
      ANA_MSG_INFO("FORCE_POWPY8_TTH MODE");
    }

    if(m_prodMode != "ggF" 
      && m_prodMode != "VBF"
      && m_prodMode != "WH"
      && m_prodMode != "qqZH"
      && m_prodMode != "ggZH"
      )
    {
      ANA_MSG_ERROR("Cannot recongnize prod mode");
      ANA_MSG_ERROR("Prod mode: " + m_prodMode);
      return StatusCode::FAILURE;
    }

    ANA_MSG_INFO("Prod mode: " + m_prodMode);

    m_init = true;
    return StatusCode::SUCCESS;
  }

  void HiggsWeightTool::setupWeights(size_t Nweights)
  {
    static TString name = "HiggsWeightTool::setupWeights";
    const std::vector<std::string> wNames = getWeightNames();
    m_nWeights = wNames.size();
    ANA_MSG_INFO("Setting up weights in " << (m_mode == AUTO ? "AUTO mode" : Form("FORCE mode %i", m_mode)) << ". " <<
                 Nweights << " weights available in input file, " << m_nWeights << " expected.");

    if (m_nWeights != Nweights) {
      throw std::runtime_error(Form("Current event has %lu weights, while we expect %lu weights from the metadata",
                                    Nweights, m_nWeights));
    }

    bool isNNLOPS = hasWeight(" nnlops-nominal-pdflhc ");

    // Always use PDF4LHC as central value. For NNLOPS, use PDF4LHC @NNLO (91400), while VBF, VH and TTH use PDF4LHC @NLO (90400)
    m_nom = isNNLOPS ? getWeightIndex(" nnlops-nominal-pdflhc ") : getWeightIndex(" PDF set = 90400 ");
    ANA_MSG_INFO("  Nominal weight at index " << m_nom);

    // Check if we have the Higgs PDF uncertainty variations are there
    m_pdfUnc.clear();

    if (hasWeight(" PDF set = 90400 ") && hasWeight(" PDF set = 90430 ")) {
      for (int id = 90400; id <= 90430; ++id) { m_pdfUnc.push_back(getWeightIndex(Form(" PDF set = %i ", id))); }

      ANA_MSG_INFO("  PDF4LHC uncertainty variations at positions " << getWeightIndex(" PDF set = 90400 ") << "-" << getWeightIndex(" PDF set = 90430 "));
    }

    m_pdfNNPDF30.clear();

    if (hasWeight(" PDF set = 260001 ")) {
      m_pdfNNPDF30.push_back(isNNLOPS ? getWeightIndex(" PDF set = 260000 ") : 0);

      for (int id = 260001; id <= 260100; ++id) { m_pdfNNPDF30.push_back(getWeightIndex(Form(" PDF set = %i ", id))); }

      ANA_MSG_INFO("  NNPDF30 NLO uncertainty variations at positions " << getWeightIndex(" PDF set = 260001 ") << "-" << getWeightIndex(" PDF set = 260100 "));
    }

    m_aS_up = m_aS_dn = 0;

    if (hasWeight(" PDF set = 90431 ") && hasWeight(" PDF set = 90432 ")) {
      m_aS_dn = getWeightIndex(" PDF set = 90431 ");
      m_aS_up = getWeightIndex(" PDF set = 90432 ");
      ANA_MSG_INFO("  PDF4LHC alphaS variations identified");
    }

    m_qcd.clear();

    if (hasWeight(" muR = 0.5, muF = 0.5 ")) {
      for (TString mur : {"0.5", "1.0", "2.0"})
        for (TString muf : {"0.5", "1.0", "2.0"})
          if (!(mur == "1.0" && muf == "1.0")) {
            std::string wn = (" muR = " + mur + ", muF = " + muf + " ").Data();

            // for some strange reason, VBF is missing some weights
            if (hasWeight(wn)) { m_qcd.push_back(getWeightIndex(wn)); }
          }

      ANA_MSG_INFO("  Read in " << m_qcd.size() << " standard QCD variations");
    }
    else if (hasWeight(" muR = 0.50, muF = 0.50 ")) {
      for (TString mur : {"0.50", "1.00", "2.00"})
	for (TString muf : {"0.50", "1.00", "2.00"})
	  if (!(mur == "1.00" && muf == "1.00")) {
	    std::string wn = (" muR = " + mur + ", muF = " + muf + " ").Data();
	    // keep the check although VBF now has all weights
	    if (hasWeight(wn)) { m_qcd.push_back(getWeightIndex(wn)); }
	  }
      ANA_MSG_INFO("  Read in " << m_qcd.size() << " standard QCD variations");
    }


    m_tinf = m_bminlo = m_nnlopsNom = 0;
    m_qcd_nnlops.clear();

    if (isNNLOPS) {
      // NNLOPS quark mass variations (with NNLO QCD correction)
      m_tinf       = getWeightIndex(" nnlops-mtinf ");
      m_bminlo     = getWeightIndex(" nnlops-bminlo ");

      if (hasWeight(" nnlops-nominal ")) {
        m_nnlopsNom  = getWeightIndex(" nnlops-nominal ");
      }

      // NNLOPS QCD scale variations
      for (TString nn : {"Dn", "Nom", "Up"})

        for (TString mur : {"Dn", "Nom", "Up"})

          for (TString muf : {"Dn", "Nom", "Up"})

            if (!(mur == "Nom" && muf == "Nom" && nn == "Nom")) {
              m_qcd_nnlops.push_back(getWeightIndex((" nnlops-nnlo" + nn + "-pwg" + mur + muf + " ").Data()));
            }

      ANA_MSG_INFO("  Read in 3 NNLOPS quark mass weights and " << m_qcd_nnlops.size() << " QCD weights");
    }

    // special PDFs
    m_pdf4lhc_nnlo = getIndex(" PDF set = 91400 ");
    m_pdf4lhc_nlo  = getIndex(" PDF set = 90400 ");
    m_nnpdf30_nnlo = getIndex(" PDF set = 261000 "); // NNLOPS nominal
    m_nnpdf30_nlo  = getIndex(" PDF set = 260000 "); // VBF+VH nominal
    m_ct10nlo      = getIndex(" PDF set = 11000 ");
    m_ct10nlo_0118 = getIndex(" PDF set = 11068 ");
    m_ct14nlo      = getIndex(" PDF set = 13100 ");
    m_ct14nlo_0118 = getIndex(" PDF set = 13165 ");
    m_mmht2014nlo  = getIndex(" PDF set = 25200 ");
  }

  const std::vector<std::string> HiggsWeightTool::getWeightNames()
  {
    if (!m_init) { initialize(); }

    if (m_mode == AUTO) { return m_weightTool->getWeightNames(); }

    static std::vector<std::string> wNames =
      m_mode == FORCE_GGF_NNLOPS ? loadXMLWeightNames(PathResolverFindCalibFile("TruthWeightTools/ggF_NNLOPS_weights.xml")) :
      m_mode == FORCE_POWPY8_VBF ? loadXMLWeightNames(PathResolverFindCalibFile("TruthWeightTools/VBF_weights.xml")) :
      m_mode == FORCE_POWPY8_VH ? loadXMLWeightNames(PathResolverFindCalibFile("TruthWeightTools/VH_weights.xml")) :
                                  loadXMLWeightNames(PathResolverFindCalibFile("TruthWeightTools/ttH_weights.xml"));
    return wNames;
  }


  bool HiggsWeightTool::hasWeight(std::string wName)
  {
    if (m_mode == AUTO) { return m_weightTool->hasWeight(wName); }

    const std::vector<std::string> &wnames = getWeightNames();
    return std::find(wnames.begin(), wnames.end(), wName) != wnames.end();
  }


  size_t HiggsWeightTool::getWeightIndex(std::string wName)
  {
    if (!hasWeight(wName)) { throw std::runtime_error("Weight " + wName + " doesn't exist."); }

    const std::vector<std::string> &wnames = (m_mode == AUTO ? m_weightTool->getWeightNames() : getWeightNames());
    return static_cast<size_t>(std::find(wnames.begin(), wnames.end(), wName) - wnames.begin());
  }


  float HiggsWeightTool::getWeight(std::string wName)
  {
    return m_weightTool->getWeight(wName);
  }


  const std::vector<float> HiggsWeightTool::getEventWeights() const
  {
    std::vector<float> weights;

    for (const auto &name : m_weightTool->getWeightNames()) { weights.push_back(m_weightTool->getWeight(name)); }

    return weights;
  }



  /// Access MC weight for uncertainty propagation
  /// Note: input kinematics should be HTXS_Higgs_pt, HTXS_Njets_pTjet30, and HTXS_Stage1_Category_pTjet30, HTXS_Stage1_1_Category_pTjet30
  HiggsWeights HiggsWeightTool::getHiggsWeights(int STXS_Njets30, double STXS_pTH, int STXS_Stage1, int STXS_Stage1p1, int STXS_Stage1p1Fine)
  {
    HiggsWeights hw = getHiggsWeightsInternal(STXS_Njets30, STXS_pTH, STXS_Stage1, STXS_Stage1p1, STXS_Stage1p1Fine);

    if (m_requireFinite) { updateWeights(hw); }

    return hw;
  }


  /// Access MC weight for uncertainty propagation
  /// Note: input kinematics should be HTXS_Higgs_pt, HTXS_Njets_pTjet30, and HTXS_Stage1_Category_pTjet30
  /// for backwards compatibility
  HiggsWeights HiggsWeightTool::getHiggsWeights(int STXS_Njets30, double STXS_pTH, int STXS_Stage1)
  {
    HiggsWeights hw = getHiggsWeightsInternal(STXS_Njets30, STXS_pTH, STXS_Stage1, -1, -1);

    if (m_requireFinite) { updateWeights(hw); }

    return hw;
  }


  void HiggsWeightTool::updateWeights(HiggsWeights &hw)
  {
    // first check nominal weight
    if (!std::isfinite(hw.nominal)) { hw.nominal = m_sumw_nom / m_Nws; }

    // add up stats on nominal weight
    ++m_Nnom;
    m_sumw_nom += hw.nominal;
    m_sumw2_nom += pow(hw.nominal, 2);
    updateWeight(hw.nominal, hw.nominal);
    m_sumw_nomC += hw.nominal;
    m_sumw2_nomC += pow(hw.nominal, 2);

    updateWeights(hw.nominal, hw.weight0, hw.alphaS_up, hw.alphaS_dn);
    updateWeights(hw.nominal, hw.pdf4lhc_unc);
    updateWeights(hw.nominal, hw.nnpdf30_unc);
    updateWeights(hw.nominal, hw.qcd);
    updateWeights(hw.nominal, hw.ggF_qcd_nnlops);
    updateWeights(hw.nominal, hw.ggF_mt_inf, hw.ggF_mb_minlo);
    updateWeights(hw.nominal, hw.nnpdf30_nlo, hw.nnpdf30_nnlo, hw.mmht2014nlo);
    updateWeights(hw.nominal, hw.pdf4lhc_nlo, hw.pdf4lhc_nnlo);
    updateWeights(hw.nominal, hw.ct10nlo, hw.ct10nlo_0118);
    updateWeights(hw.nominal, hw.ct14nlo, hw.ct14nlo_0118);
    updateWeights(hw.nominal, hw.ggF_qcd_wg1_mu, hw.ggF_qcd_wg1_res);
    updateWeights(hw.nominal, hw.ggF_qcd_wg1_mig01, hw.ggF_qcd_wg1_mig12);
    updateWeights(hw.nominal, hw.ggF_qcd_wg1_pTH, hw.ggF_qcd_wg1_qm_b, hw.ggF_qcd_wg1_qm_t);
    updateWeights(hw.nominal, hw.ggF_qcd_wg1_vbf2j, hw.ggF_qcd_wg1_vbf3j);
    updateWeights(hw.nominal, hw.ggF_qcd_nnlops_nnlo, hw.ggF_qcd_nnlops_pow);
    updateWeights(hw.nominal, hw.ggF_qcd_stxs);
    updateWeights(hw.nominal, hw.ggF_qcd_2017);
    updateWeights(hw.nominal, hw.ggF_qcd_jve);
    updateWeight(hw.nominal, hw.ggF_qcd_pTH_nJ0);
    updateWeights(hw.nominal, hw.qq2Hqq_VBF_scheme);
    updateWeights(hw.nominal, hw.qq2Hll_scheme);
    updateWeights(hw.nominal, hw.gg2Hll_scheme);
  }


  void HiggsWeightTool::updateWeight(const double &w_nom, double &w)
  {
    if (m_requireFinite && !std::isfinite(w)) { w = w_nom; }

    ++m_Nws;
    m_sumw += w;
    m_sumw2 += w * w;

    if (!m_cutOff) { return; }

    if (w > m_weightCutOff) { w = m_weightCutOff; }

    if (w < -m_weightCutOff) { w = -m_weightCutOff; }

    // stats on all weights
    m_sumwC += w;
    m_sumw2C += w * w;
  }


  void HiggsWeightTool::printSummary()
  {
    if (m_requireFinite) {
      ANA_MSG_INFO("");
      ANA_MSG_INFO("==============================");
      ANA_MSG_INFO("==  HiggsWeightTool SUMMARY ==");
      ANA_MSG_INFO("");

      auto mean = Form("%.2e", m_sumw_nom / m_Nnom);
      auto RMS = Form("%.2e", sqrt(m_sumw2_nom * m_Nnom - m_sumw_nom * m_sumw_nom) / m_Nnom);
      auto Neff = Form("%.1f", m_sumw_nom * m_sumw_nom / m_sumw2_nom);
      ANA_MSG_INFO("  " << m_Nnom << " nominal weights extracted with mean " << mean << " and RMS " << RMS << ", Neff = " << Neff);

      if (m_cutOff) {
        auto cutoff = Form("%.1f", m_weightCutOff);
        mean = Form("%.2e", m_sumw_nomC / m_Nnom);
        RMS = Form("%.2e", sqrt(m_sumw2_nomC * m_Nnom - m_sumw_nomC * m_sumw_nomC) / m_Nnom);
        Neff = Form("%.1f", m_sumw_nomC * m_sumw_nomC / m_sumw2_nomC);
        ANA_MSG_INFO("  After the weight cutoff of " << cutoff << " we get mean " << mean << " and RMS " << RMS << ", Neff = " << Neff);
        ANA_MSG_INFO("");
      }

      mean = Form("%.2e", m_sumw / m_Nws);
      RMS = Form("%.2e", sqrt(m_sumw2 * m_Nws - m_sumw * m_sumw) / m_Nws);
      ANA_MSG_INFO("  In total " << m_Nws << " weights extracted with mean " << mean << " and RMS " << RMS);
      ANA_MSG_INFO("");

      if (m_cutOff) {
        auto cutoff = Form("%.1f", m_weightCutOff);
        mean = Form("%.2e", m_sumwC / m_Nws);
        RMS = Form("%.2e", sqrt(m_sumw2C * m_Nws - m_sumwC * m_sumwC) / m_Nws);
        ANA_MSG_INFO("  After the weight cutoff of " << cutoff << " we get mean " << mean << " and RMS " << RMS);
        ANA_MSG_INFO("");
      }

      ANA_MSG_INFO("==============================");
      ANA_MSG_INFO("");
    }
  }


  /// Access MC weight for uncertainty propagation
  /// Note: input kinematics should be HTXS_Higgs_pt, HTXS_Njets_pTjet30, and HTXS_Stage1_Category_pTjet30, HTXS_Stage1_1_Category_pTjet30, HTXS_Stage1_1_Fine_Category_pTjet30
  HiggsWeights HiggsWeightTool::getHiggsWeightsInternal(int STXS_Njets30, double STXS_pTH, int STXS_Stage1, int STXS_Stage1p1, int STXS_Stage1p1Fine)
  {
    // convert to GeV
    double pTH = STXS_pTH / 1000;
    int Njets = STXS_Njets30;
    const std::vector<float> weights = getEventWeights();

    // if needed, setup weight structure
    if (m_nWeights != weights.size()) { setupWeights(weights.size()); }

    HiggsWeights hw;
    // set kinematics
    hw.pTH = pTH;
    hw.Njets30 = Njets;
    hw.STXS = STXS_Stage1;

    // 1. Nominal weight
    double w_nom = weights[m_nom];
    hw.nominal = w_nom;
    hw.weight0 = weights[0];

    // 2. PDF weights
    hw.alphaS_up = hw.alphaS_dn = 0;

    if (m_pdfUnc.size() == 31) {
      double pdf0 = weights[m_pdfUnc[0]];

      // PDF uncertainty. Scale to nominal weight (since PDF0 is relative to Powheg NLO)
      //   w_PDF[i] = PDF[i] / PDF[0] * w_nominal
      for (size_t i = 1; i <= 30; ++i) {
        hw.pdf4lhc_unc.push_back(weights[m_pdfUnc[i]] / pdf0 * w_nom);
      }

      // 2b. alphaS weights
      if (m_aS_up && m_aS_dn) {
        hw.alphaS_up = weights[m_aS_up] / pdf0 * w_nom;
        hw.alphaS_dn = weights[m_aS_dn] / pdf0 * w_nom;
      }
    }

    if (m_pdfNNPDF30.size() == 101) {
      double pdf0 = weights[m_pdfNNPDF30[0]];

      for (size_t i = 1; i <= 100; ++i)
      { hw.nnpdf30_unc.push_back(weights[m_pdfNNPDF30[i]] / pdf0 * w_nom); }
    }

    // Standard QCD uncertainties (which are relative to weight 0)
    for (auto idx : m_qcd) { hw.qcd.push_back(getWeight(weights, idx)*w_nom / hw.weight0); }

    // NNLOPS QCD uncertainties
    double nnlo = getWeight(weights, m_nnlopsNom);
    if (!m_nnlopsNom) { nnlo = w_nom; } // fix for rare NNLOPS samples

    // Central values of different PDF sets
    // for NNLOPS, these are relative to the NLO cross section - need to adjust with k-factor
    double k = m_nnlopsNom ? nnlo / hw.weight0 : 1.0;

    //hw.nnpdf30_nnlo = m_nnlopsNom ? weights[m_nnlopsNom] : 0; <<<<<
    hw.nnpdf30_nlo  = getWeight(weights, m_nnpdf30_nlo) * k;
    hw.nnpdf30_nnlo = getWeight(weights, m_nnpdf30_nnlo) * k;
    hw.mmht2014nlo  = getWeight(weights, m_mmht2014nlo) * k;
    hw.pdf4lhc_nlo  = getWeight(weights, m_pdf4lhc_nlo) * k;
    hw.pdf4lhc_nnlo = getWeight(weights, m_pdf4lhc_nnlo) * k;
    hw.ct10nlo      = getWeight(weights, m_ct10nlo) * k;
    hw.ct10nlo_0118 = getWeight(weights, m_ct10nlo_0118) * k;
    hw.ct14nlo      = getWeight(weights, m_ct14nlo) * k;
    hw.ct14nlo_0118 = getWeight(weights, m_ct14nlo_0118) * k;

    // special catch
    if (m_bminlo || m_qcd_nnlops.size()) { // NNLOPS!!
      hw.nnpdf30_nnlo = hw.weight0 * k;
      //      hw.pdf4lhc_nnlo = w_nom; // usually close, but can be 4% different...
    } else if (m_pdfUnc.size() == 31) // should be VBF or VH
    { hw.nnpdf30_nlo = hw.weight0; } // sf=1 here


    
    // Process specific uncertainities

    // VBF specific
    if(m_prodMode == "VBF")       fillVBFWeights(hw, STXS_Stage1p1Fine);
    else if(m_prodMode == "WH")   fillqqVHWeights(hw, STXS_Stage1p1Fine);
    else if(m_prodMode == "qqZH") fillqqVHWeights(hw, STXS_Stage1p1Fine);
    else if(m_prodMode == "ggZH") fillggVHWeights(hw, STXS_Stage1p1Fine);
    else                          fillggFWeights(hw, STXS_Njets30, STXS_pTH, STXS_Stage1, STXS_Stage1p1);


    
    return hw;

  }


  ///////////////////////////////////////
  // ggF
  ///////////////////////////////////////
  void HiggsWeightTool::fillggFWeights(HiggsWeights& hw, int STXS_Njets30, double STXS_pTH, int STXS_Stage1, int STXS_Stage1p1)
  {

    // Implementation for this comes from Dag

    // convert to GeV
    double pTH = STXS_pTH / 1000;
    int Njets = STXS_Njets30;
    const std::vector<float> weights = getEventWeights();

    // if needed, setup weight structure
    if (m_nWeights != weights.size()) { setupWeights(weights.size()); }
    double w_nom = weights[m_nom];

    // NNLOPS QCD uncertainties
    double nnlo = getWeight(weights, m_nnlopsNom);

    if (!m_nnlopsNom) { nnlo = w_nom; } // fix for rare NNLOPS samples

    for (auto idx : m_qcd_nnlops) { hw.ggF_qcd_nnlops.push_back(getWeight(weights, idx)*w_nom / nnlo); }

    // 3. Quark mass variations
    //m_tinf=m_bminlo=m_nnlopsNom=0;
    hw.ggF_mt_inf   = getWeight(weights, m_tinf) * w_nom / nnlo;
    hw.ggF_mb_minlo = getWeight(weights, m_bminlo) * w_nom / nnlo;


    // Powheg uncertainty
    hw.ggF_qcd_nnlops_nnlo = hw.ggF_qcd_nnlops_pow = w_nom;

    if (m_qcd_nnlops.size() == 26) {
      hw.ggF_qcd_nnlops_nnlo = hw.ggF_qcd_nnlops[4]; // nnlops-nnloDn-PowNomNom
      hw.ggF_qcd_nnlops_pow  = hw.ggF_qcd_nnlops[9]; // nnlops-nnloNom-PowDnDn
    }



    if (Njets < 0) { return; }

    /*********
     *  WG1 proposed ggF QCD uncertainty
     */

    // Cross sections in the =0, =1, and >=2 jets of Powheg ggH after reweighing scaled to  sigma(N3LO)
    //static std::vector<double> sig({30.26,13.12,5.14});
    //static std::vector<double> sig({30.117,12.928,5.475}); // NNLOPS 2M
    static double sig0 = 30.117, sig1 = 12.928, sig_ge2 = 5.475,
                  sig_ge1 = sig1 + sig_ge2, sig_tot = sig0 + sig_ge1,
                  sig_vbfTopo = 0.630, sig_ge2noVBF = sig_ge2 - sig_vbfTopo, sig_ge1noVBF = sig_ge1 - sig_vbfTopo;
    static std::vector<double> sig({sig0, sig1, sig_ge2noVBF}); // NNLOPS subtracting VBF

    // BLPTW absolute uncertainties in pb
    static std::vector<double> yieldUnc({ 1.12, 0.66, 0.42});
    static std::vector<double> resUnc({ 0.03, 0.57, 0.42});
    static std::vector<double> cut01Unc({-1.22, 1.00, 0.21});
    static std::vector<double> cut12Unc({    0, -0.86, 0.86});

    // account for missing EW+quark mass effects by scaling BLPTW total cross section to sigma(N3LO)
    double sf = 48.52 / 47.4;

    int jetBin = (Njets > 1 ? 2 : Njets);
    hw.ggF_qcd_wg1_mu    = (1.0 + yieldUnc[jetBin] / sig[jetBin] * sf) * w_nom;
    hw.ggF_qcd_wg1_res   = (1.0 + resUnc[jetBin] / sig[jetBin] * sf) * w_nom;
    hw.ggF_qcd_wg1_mig01 = (1.0 + cut01Unc[jetBin] / sig[jetBin] * sf) * w_nom;
    hw.ggF_qcd_wg1_mig12 = (1.0 + cut12Unc[jetBin] / sig[jetBin] * sf) * w_nom;

    // High pT uncertainty
    static double y1_1 = 0.88, y2_1 = 1.16, x2_1 = 150;
    static double y1_ge2 = 0.88, y2_ge2 = 1.16, x2_ge2 = 225;
    double pTH_uncSF = 1.0;

    if (Njets == 1) { pTH_uncSF = linInter(pTH, 0, y1_1, x2_1, y2_1); }
    else if (Njets >= 2) { pTH_uncSF = linInter(pTH, 0, y1_ge2, x2_ge2, y2_ge2); }

    //pTH>x2_ge2?y2_ge2:y1_ge2+(y2_ge2-y1_ge2)*pTH/x2_ge2;
    //else if (Njets==1) pTH_uncSF = pTH>x2_1?y2_1:y1_1+(y2_1-y1_1)*pTH/x2_1;
    hw.ggF_qcd_wg1_pTH = pTH_uncSF * w_nom;

    double qmSF = 1.0;

    if (Njets == 0) { qmSF = linInter(pTH, 6, 0.92, 24, 1.05); }
    else if (Njets == 1) { qmSF = linInter(pTH, 35, 0.92, 62.5, 1.05); }
    else if (Njets >= 2) { qmSF = linInter(pTH, 40, 0.92, 120, 1.05); }

    hw.ggF_qcd_wg1_qm_b = w_nom * qmSF;
    hw.ggF_qcd_wg1_qm_t = w_nom * linInter(pTH, 160, 1.0, 500, 1.37);

    hw.ggF_qcd_wg1_vbf2j = w_nom;
    hw.ggF_qcd_wg1_vbf3j = w_nom;

    if (STXS_Stage1 == 101) { // GG2H_VBFTOPO_JET3VETO, tot unc 38%
      hw.ggF_qcd_wg1_mu = hw.ggF_qcd_wg1_res = hw.ggF_qcd_wg1_mig01 = hw.ggF_qcd_wg1_mig12 = w_nom;
      hw.ggF_qcd_wg1_vbf2j = w_nom * 1.20;
      hw.ggF_qcd_wg1_vbf3j = w_nom * (1.0 - 0.32);
    }

    if (STXS_Stage1 == 102) { // GG2H_VBFTOPO_JET3, tot unc 30.4%
      hw.ggF_qcd_wg1_mu = hw.ggF_qcd_wg1_res = hw.ggF_qcd_wg1_mig01 = hw.ggF_qcd_wg1_mig12 = w_nom;
      hw.ggF_qcd_wg1_vbf2j = w_nom * 1.20;
      hw.ggF_qcd_wg1_vbf3j = w_nom * 1.235;
    }

    // Quark-mass uncdertainty - TODO
    //if (pTH>500) qm=1.5; else if (pTH>160) qm=1.0+0.5*(pTH-160)/340;
    //hw.qcd_wg1_qm = w_nom;


    /********
     *  JVE as a cross check
     */
    // Central values for eps0 and eps1 from Powheg NNLOPS
    //   eps0 = 0.617 +- 0.012 <= from Fabrizio and Pier
    //   eps1 = 0.681 +- 0.057 <= from Fabrizio and Pier
    // and setting inclusive uncertainty to 3.9% (YR4 for N3LO)
    static double
    Dsig_tot = sig_tot * 0.039, D01 = sig_tot * 0.012, D12 = sig_ge1 * 0.057;
    //Dsig_tot=2.48, D01=1.25, D12=0.88; // For cross check with BLPTW

    hw.ggF_qcd_jve.push_back(w_nom * (1.0 + Dsig_tot / sig_tot)); // incl

    // eps0, i.e. mig 0 -> ge1
    if (Njets == 0) { hw.ggF_qcd_jve.push_back(w_nom * (1.0 - D01 / sig0)); }
    else { hw.ggF_qcd_jve.push_back(w_nom * (1.0 + D01 / sig_ge1noVBF)); }

    // eps1, i.e. mig 1 -> ge2
    if (Njets == 0) { hw.ggF_qcd_jve.push_back(w_nom); }
    else if (Njets == 1) { hw.ggF_qcd_jve.push_back(w_nom * (1.0 - D12 / sig1)); }
    else { hw.ggF_qcd_jve.push_back(w_nom * (1.0 + D12 / sig_ge2noVBF)); }

    if (STXS_Stage1 == 101 || STXS_Stage1 == 102)
    { hw.ggF_qcd_jve[0] = hw.ggF_qcd_jve[1] = hw.ggF_qcd_jve[2] = w_nom; }

    hw.ggF_qcd_jve.push_back(hw.ggF_qcd_wg1_vbf2j);
    hw.ggF_qcd_jve.push_back(hw.ggF_qcd_wg1_vbf3j);
    hw.ggF_qcd_jve.push_back(hw.ggF_qcd_wg1_pTH);
    hw.ggF_qcd_jve.push_back(hw.ggF_qcd_wg1_qm_t);

    /*********
     *  Tackmann STXS uncertainty scheme
     */

    // First four sources are the BLPTW ones
    hw.ggF_qcd_stxs.push_back(hw.ggF_qcd_wg1_mu);
    hw.ggF_qcd_stxs.push_back(hw.ggF_qcd_wg1_res);
    hw.ggF_qcd_stxs.push_back(hw.ggF_qcd_wg1_mig01);
    hw.ggF_qcd_stxs.push_back(hw.ggF_qcd_wg1_mig12);

    // Add in the VBF uncertainties here
    hw.ggF_qcd_stxs.push_back(hw.ggF_qcd_wg1_vbf2j);
    hw.ggF_qcd_stxs.push_back(hw.ggF_qcd_wg1_vbf3j);

    // This is followed by Dsig60, Dsig120 and Dsig200
    // These are extracted from Powheg NNLOPS scale variations (using this tool!)
    // As the envelope of hw.qcd_nnlops, which gives (all x-sec below have Njets>=1):
    //   sig(60,200)  = 9.095 +/- 1.445 pb, BLPTW 10.9%
    //   sig(120,200) = 1.961 +/- 0.401 pb, BLPTW 13.1%
    //   sig(200,inf) = 0.582 +/- 0.121 pb, BLPTW 15.1%
    static double sig0_60 = 8.719, sig60_200 = 9.095, sig120_200 = 1.961,
                  sig0_120 = sig0_60 + sig60_200 - sig120_200, sig200_plus = 0.582; // 0.121 (-) 0.151*0.582
    // static double Dsig60_200=1.457, Dsig120_200=0.411, Dsig200_plus=0.115; // with 40k events, and no BLPTW subraction
    static double Dsig60_200 = 1.055, Dsig120_200 = 0.206, Dsig200_plus = 0.0832; // with 2M evts, and subtraction
    double dsig60 = 0, dsig120 = 0, dsig200 = 0;

    if (Njets >= 1) {
      if (pTH < 60)  { dsig60 = -Dsig60_200 / sig0_60; } // -17.2%
      else if (pTH < 200) { dsig60 = Dsig60_200 / sig60_200; } // +16.0%

      if (pTH < 120) { dsig120 = -Dsig120_200 / sig0_120; }    //  -2.6%
      else if (pTH < 200) { dsig120 =  Dsig120_200 / sig120_200; } // +20.8%

      if (pTH > 200) { dsig200 = Dsig200_plus / sig200_plus; } // +14.3%
    }

    hw.ggF_qcd_stxs.push_back((1.0 + dsig60)*w_nom);
    hw.ggF_qcd_stxs.push_back((1.0 + dsig120)*w_nom);
    hw.ggF_qcd_stxs.push_back((1.0 + dsig200)*w_nom);



    // First four sources are the BLPTW ones
    hw.ggF_qcd_2017.push_back(hw.ggF_qcd_wg1_mu);
    hw.ggF_qcd_2017.push_back(hw.ggF_qcd_wg1_res);
    hw.ggF_qcd_2017.push_back(hw.ggF_qcd_wg1_mig01);
    hw.ggF_qcd_2017.push_back(hw.ggF_qcd_wg1_mig12);

    // Add in the VBF uncertainties here
    hw.ggF_qcd_2017.push_back(hw.ggF_qcd_wg1_vbf2j);
    hw.ggF_qcd_2017.push_back(hw.ggF_qcd_wg1_vbf3j);

    // Smooth pTH uncertainties
    double pTH60 = w_nom, pTH120 = w_nom;

    if (Njets == 1) { pTH60 *= linInter(pTH, 20, 0.9, 100, 1.1); }
    else if (Njets >= 2) { pTH60 *= linInter(pTH, 0, 0.9, 180, 1.10); } // >=2 jets

    if (Njets > 0) { pTH120 *= linInter(pTH, 90, 0.984, 160, 1.14); }

    hw.ggF_qcd_2017.push_back(pTH60);
    hw.ggF_qcd_2017.push_back(pTH120);

    hw.ggF_qcd_2017.push_back(hw.ggF_qcd_wg1_qm_t);


    hw.ggF_qcd_pTH_nJ0 = w_nom;
    if (Njets == 0 && pTH <= 15) { hw.ggF_qcd_pTH_nJ0 *= linInter(pTH, 0, 1.2175, 15, 0.96); }
    if (Njets == 0 && pTH > 15) { hw.ggF_qcd_pTH_nJ0 *= linInter(pTH, 15, 0.96, 140, 0.895); }

    return;
  }


  ///////////////////////////////////////
  // VBF
  ///////////////////////////////////////
  void HiggsWeightTool::fillVBFWeights(HiggsWeights& hw, int STXS_Stage1p1Fine)
  {
    if(STXS_Stage1p1Fine == -1)
    {
      ANA_MSG_DEBUG("STXS_Stage1p1 is -1 - returning without calculating VBF weight");
    }

    const std::vector<float> weights = getEventWeights();

    // if needed, setup weight structure
    if (m_nWeights != weights.size()) { setupWeights(weights.size()); }
    double w_nom = weights[m_nom];

    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(0, STXS_Stage1p1Fine)); // mu
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(1, STXS_Stage1p1Fine)); // PTH200
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(2, STXS_Stage1p1Fine)); // Mjj60
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(3, STXS_Stage1p1Fine)); // Mjj120
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(4, STXS_Stage1p1Fine)); // Mjj350
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(5, STXS_Stage1p1Fine)); // Mjj700
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(6, STXS_Stage1p1Fine)); // Mjj1000
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(7, STXS_Stage1p1Fine)); // Mjj1500
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(8, STXS_Stage1p1Fine)); // 25
    hw.qq2Hqq_VBF_scheme.push_back(w_nom * vbf_uncert_stage_1_1(9, STXS_Stage1p1Fine)); // JET01

    return;
  }


  void HiggsWeightTool::fillqqVHWeights(HiggsWeights& hw, int STXS_Stage1p1Fine)
  {

    if(STXS_Stage1p1Fine == -1)
    {
      ANA_MSG_DEBUG("STXS_Stage1p1 is -1 - returning without calculating qqVH weight");
    }

    const std::vector<float> weights = getEventWeights();

    // if needed, setup weight structure
    if (m_nWeights != weights.size()) { setupWeights(weights.size()); }
    double w_nom = weights[m_nom];

    hw.qq2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(0, STXS_Stage1p1Fine)); // mu
    hw.qq2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(1, STXS_Stage1p1Fine)); // PTV75
    hw.qq2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(2, STXS_Stage1p1Fine)); // PTV150
    hw.qq2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(3, STXS_Stage1p1Fine)); // PTV250
    hw.qq2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(4, STXS_Stage1p1Fine)); // PTV400
    hw.qq2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(5, STXS_Stage1p1Fine)); // NJ1
    hw.qq2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(6, STXS_Stage1p1Fine)); // NJ2

    // Return the nominal
    hw.gg2Hll_scheme.push_back(w_nom); // mu
    hw.gg2Hll_scheme.push_back(w_nom); // PTV75
    hw.gg2Hll_scheme.push_back(w_nom); // PTV150
    hw.gg2Hll_scheme.push_back(w_nom); // PTV250
    hw.gg2Hll_scheme.push_back(w_nom); // PTV400
    hw.gg2Hll_scheme.push_back(w_nom); // NJ1
    hw.gg2Hll_scheme.push_back(w_nom); // NJ2


    return;
  }

  void HiggsWeightTool::fillggVHWeights(HiggsWeights& hw, int STXS_Stage1p1Fine)
  {

    if(STXS_Stage1p1Fine == -1)
    {
      ANA_MSG_DEBUG("STXS_Stage1p1 is -1 - returning without calculating ggVH weight");
    }

    const std::vector<float> weights = getEventWeights();

    // if needed, setup weight structure
    if (m_nWeights != weights.size()) { setupWeights(weights.size()); }
    double w_nom = weights[m_nom];

    hw.gg2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(0, STXS_Stage1p1Fine)); // mu
    hw.gg2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(1, STXS_Stage1p1Fine)); // PTV75
    hw.gg2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(2, STXS_Stage1p1Fine)); // PTV150
    hw.gg2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(3, STXS_Stage1p1Fine)); // PTV250
    hw.gg2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(4, STXS_Stage1p1Fine)); // PTV400
    hw.gg2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(5, STXS_Stage1p1Fine)); // NJ1
    hw.gg2Hll_scheme.push_back(w_nom * VHlep_uncert_stage_1_1_fine(6, STXS_Stage1p1Fine)); // NJ2

    // Return the nominal
    hw.qq2Hll_scheme.push_back(w_nom); // mu
    hw.qq2Hll_scheme.push_back(w_nom); // PTV75
    hw.qq2Hll_scheme.push_back(w_nom); // PTV150
    hw.qq2Hll_scheme.push_back(w_nom); // PTV250
    hw.qq2Hll_scheme.push_back(w_nom); // PTV400
    hw.qq2Hll_scheme.push_back(w_nom); // NJ1
    hw.qq2Hll_scheme.push_back(w_nom); // NJ2


    return;
  }



  // Copied from
  // https://gitlab.cern.ch/LHCHIGGSXS/LHCHXSWG2/STXS/VBF-Uncertainties/blob/master/qq2Hqq_uncert_scheme.cpp
  double HiggsWeightTool::vbf_uncert_stage_1_1(int source, int event_STXS, double Nsigma)
  {

    // STXS bin definition can be found in the HXSWG gitlab respository:
    // https://gitlab.cern.ch/LHCHIGGSXS/LHCHXSWG2/STXS/blob/master/HiggsTemplateCrossSections.h

    // bin acceptances extracted from POWHEG VBFH (NLO) + PYTHIA8 for showering
    // note that the dipoleShower option in pythia8 is turned on 

  static std::map<int, std::vector<double> > stxs_acc =
    {//STXS   TOT   ,  PTH200,  Mjj60 , Mjj120 , Mjj350 , Mjj700 ,Mjj1000 ,Mjj1500  ,  25       , JET01
     { 200 , {0.07  ,  0     , 0      , 0      , 0      , 0      , 0      , 0       ,        0  ,    0   }},
     { 201 , {0.0744,  0     , 0      , 0      , 0      , 0      , 0      , 0       ,        0  ,-0.1649 }}, // Jet0
     { 202 , {0.3367,  0     , 0      , 0      , 0      , 0      , 0      , 0       ,        0  ,-0.7464 }}, // Jet1
     { 203 , {0.0092,  0     ,-0.6571 , 0      , 0      , 0      , 0      , 0       ,   -0.0567 ,  0.0178}}, // Mjj 0-60,      PTHjj 0-25
     { 204 , {0.0143,  0     , 0.0282 ,-0.5951 , 0      , 0      , 0      , 0       ,   -0.0876 ,  0.0275}}, // Mjj 60-120,    PTHjj 0-25
     { 205 , {0.0455,  0     , 0.0902 , 0.0946 ,-0.3791 , 0      , 0      , 0       ,   -0.2799 ,  0.0877}}, // Mjj 120-350,   PTHjj 0-25
     { 206 , {0.0048,  0     ,-0.3429 , 0      , 0      , 0      , 0      , 0       ,   +0.0567 ,  0.0093}}, // Mjj 0-60,      PTHjj 25-inf
     { 207 , {0.0097,  0     , 0.0192 ,-0.4049 , 0      , 0      , 0      , 0       ,   +0.0876 ,  0.0187}}, // Mjj 60-120,    PTHjj 25-inf
     { 208 , {0.0746,  0     , 0.1477 , 0.0155 ,-0.6209 , 0      , 0      , 0       ,   +0.2799 ,  0.1437}}, // Mjj 120-350,   PTHjj 25-inf
     { 209 , {0.0375, 0.1166 , 0.0743 , 0.078  , 0.1039 ,-0.2757 , 0      , 0       ,   -0.2306 ,  0.0723}}, // Mjj 350-700,   PTHjj 0-25    , pTH 0-200
     { 210 , {0.0985, 0.3062 , 0.1951 , 0.2048 , 0.273  ,-0.7243 , 0      , 0       ,   +0.2306 ,  0.1898}}, // Mjj 350-700,   PTHjj 25-inf  , pTH 0-200
     { 211 , {0.0166, 0.0515 , 0.0328 , 0.0345 , 0.0459 , 0.0773 ,-0.2473 , 0       ,   -0.1019 ,  0.0319}}, // Mjj 700-1000,  PTHjj 0-25    , pTH 0-200
     { 212 , {0.0504, 0.1568 , 0.0999 , 0.1049 , 0.1398 , 0.2353 ,-0.7527 , 0       ,   +0.1019 ,  0.0972}}, // Mjj 700-1000,  PTHjj 25-inf  , pTH 0-200
     { 213 , {0.0137, 0.0426 , 0.0271 , 0.0285 , 0.0379 , 0.0639 , 0.0982 ,-0.2274  ,   -0.0842 ,  0.0264}}, // Mjj 1000-1500, PTHjj 0-25    , pTH 0-200
     { 214 , {0.0465, 0.1446 , 0.0922 , 0.0967 , 0.1289 , 0.2171 , 0.3335 ,-0.7726  ,   +0.0842 ,  0.0897}}, // Mjj 1000-1500, PTHjj 25-inf  , pTH 0-200
     { 215 , {0.0105, 0.0327 , 0.0208 , 0.0219 , 0.0291 , 0.0491 , 0.0754 , 0.1498  ,   -0.0647 ,  0.0203}}, // Mjj 1500-inf , PTHjj 0-25    , pTH 0-200
     { 216 , {0.048 , 0.1491 , 0.095  , 0.0998 , 0.133  , 0.2239 , 0.344  , 0.6836  ,   +0.0647 ,  0.0925}}, // Mjj 1500-inf , PTHjj 25-inf  , pTH 0-200
     { 217 , {0.0051,-0.1304 , 0.0101 , 0.0106 , 0.0141 , 0.0238 , 0.0366 , 0.0727  ,   -0.0314 ,  0.0098}}, // Mjj 350-700,   PTHjj 0-25    , pTH 200-inf
     { 218 , {0.0054,-0.1378 , 0.0107 , 0.0112 , 0.0149 , 0.0251 , 0.0386 , 0.0768  ,   +0.0314 ,  0.0104}}, // Mjj 350-700,   PTHjj 25-inf  , pTH 200-inf
     { 219 , {0.0032,-0.0816 , 0.0063 , 0.0066 , 0.0088 , 0.0149 , 0.0229 , 0.0455  ,   -0.0196 ,  0.0062}}, // Mjj 700-1000,  PTHjj 0-25    , pTH 200-inf
     { 220 , {0.0047,-0.1190 , 0.0092 , 0.0097 , 0.0129 , 0.0217 , 0.0334 , 0.0663  ,   +0.0196 ,  0.0090}}, // Mjj 700-1000,  PTHjj 25-inf  , pTH 200-inf
     { 221 , {0.0034,-0.0881 , 0.0068 , 0.0072 , 0.0096 , 0.0161 , 0.0247 , 0.0491  ,   -0.0212 ,  0.0066}}, // Mjj 1000-1500, PTHjj 0-25    , pTH 200-inf
     { 222 , {0.0056,-0.1440 , 0.0112 , 0.0117 , 0.0156 , 0.0263 , 0.0404 , 0.0802  ,   +0.0212 ,  0.0109}}, // Mjj 1000-1500, PTHjj 25-inf  , pTH 200-inf
     { 223 , {0.0036,-0.0929 , 0.0072 , 0.0076 , 0.0101 , 0.0169 , 0.026  , 0.0518  ,   -0.0223 ,  0.0070}}, // Mjj 1500-inf , PTHjj 0-25    , pTH 200-inf
     { 224 , {0.0081,-0.2062 , 0.016  , 0.0168 , 0.0223 , 0.0376 , 0.0578 , 0.1149  ,   +0.0223 ,  0.0155}}  // Mjj 1500-inf , PTHjj 25-inf  , pTH 200-inf
     
  };

  // uncertainty sources extracted from proVBF NNLO
  // 10 nuissances:  1 x yield, 1 x 3rd jet veto, 6 x Mjj cuts, 1 x 01->2 jetBin, 1 x PTH cut
  //+--------------------+--------+-------+-------+--------+--------+--------+---------+---------+--------+--------+
  //|                    |  tot   |  200  | Mjj60 | Mjj120 | Mjj350 | Mjj700 | Mjj1000 | Mjj1500 |   25   |  2jet  |
  //+--------------------+--------+-------+-------+--------+--------+--------+---------+---------+--------+--------+
  //|   DELTA (POWHEG)   | 14.872 | 1.275 | 6.421 | 5.572  | 3.516  | 7.881  |  5.721  |  4.579  |  4.66  | 1.866  |
  //|     DELTA (FO)     | 14.867 | 0.394 | 9.762 | 6.788  | 7.276  | 3.645  |  2.638  |  1.005  | 20.073 | 18.094 |
  //+--------------------+--------+-------+-------+--------+--------+--------+---------+---------+--------+--------+
  std::vector<double> uncert_deltas({14.867, 0.394, 9.762, 6.788, 7.276, 3.645, 2.638, 1.005, 20.073, 18.094});

    // cross sections from different STXS bins
    // prediction at NLO from POWEHG VBFH + PYTHIA8(dipoleShower=on)

  std::map<int, double> powheg_xsec
    {{200,  273.952 },
     {201,  291.030 },
     {202, 1317.635 },
     {203,   36.095 },
     {204,   55.776 },
     {205,  178.171 },
     {206,   18.839 },
     {207,   37.952 },
     {208,  291.846 },
     {209,  146.782 },
     {210,  385.566 },
     {211,   64.859 },
     {212,  197.414 },
     {213,   53.598 },
     {214,  182.107 },
     {215,   41.167 },
     {216,  187.823 },
     {217,   19.968 },
     {218,   21.092 },
     {219,   12.496 },
     {220,   18.215 },
     {221,   13.490 },
     {222,   22.044 },
     {223,   14.220 },
     {224,   31.565 }};



    // return a single weight for a given souce
    if(source < 10)
    {
      if(event_STXS < 200 || event_STXS > 224)
      {
        ANA_MSG_WARNING("VBF - cannot recognize STXS. Returning 1");
        ANA_MSG_WARNING("event_STXS: " + std::to_string(event_STXS));
        return 1.0;
      }

      double delta_var = stxs_acc[event_STXS][source] * uncert_deltas[source];
      return  1.0 + Nsigma * (delta_var/powheg_xsec[event_STXS]);
    }
    else
    {
      ANA_MSG_WARNING("VBF - cannot recognize source. Returning 1");
      ANA_MSG_WARNING("source: " + std::to_string(source));
      return 1.0;
    }
  }


  double HiggsWeightTool::VHlep_uncert_stage_1_1_fine(int source, int event_STXS, double Nsigma)
  {

    // STXS bin definition can be found in the HXSWG gitlab respository:
    // https://gitlab.cern.ch/LHCHIGGSXS/LHCHXSWG2/STXS/blob/master/HiggsTemplateCrossSections.h

    // These uncertainities are derived by the Hbb group and published in ATL-PHYS-PUB-2018-035

    static std::map<int, std::vector<double> > stxs_error =
      { //STXS   mu         , Delta75    , Delta150   , Delta250   , Delta400   , nJ1        , nJ2
        {300  , {0.7        , 0          , 0          , 0          , 0          , 0          , 0         }}, // QQ2HLNU_FWDH
        {301  , {0.7        , -2.9       , 0          , 0          , 0          , -3         , 0         }}, // QQ2HLNU_PTV_0_75_0J
        {302  , {0.7        , 3.3        , -0.53      , 0          , 0          , -3.6       , 0         }}, // QQ2HLNU_PTV_75_150_0J
        {303  , {0.7        , 3.3        , 1.3        , -0.4       , 0          , -4.1       , 0         }}, // QQ2HLNU_PTV_150_250_0J
        {304  , {0.7        , 3.3        , 1.3        , 1.4        , -0.38      , -5.4       , 0         }}, // QQ2HLNU_PTV_250_400_0J
        {305  , {0.7        , 3.3        , 1.3        , 1.4        , 1.8        , -6.8       , 0         }}, // QQ2HLNU_PTV_GT400_0J
        {306  , {0.7        , -2.9       , 0          , 0          , 0          , 6.8        , -4.5      }}, // QQ2HLNU_PTV_0_75_1J
        {307  , {0.7        , 3.3        , -0.53      , 0          , 0          , 6          , -4.7      }}, // QQ2HLNU_PTV_75_150_1J
        {308  , {0.7        , 3.3        , 1.3        , -0.4       , 0          , 5.1        , -5        }}, // QQ2HLNU_PTV_150_250_1J
        {309  , {0.7        , 3.3        , 1.3        , 1.4        , -0.38      , 5.3        , -5        }}, // QQ2HLNU_PTV_250_400_1J
        {310  , {0.7        , 3.3        , 1.3        , 1.4        , 1.8        , 5.5        , -5.7      }}, // QQ2HLNU_PTV_GT400_1J
        {311  , {0.7        , -2.9       , 0          , 0          , 0          , 6.8        , 10        }}, // QQ2HLNU_PTV_0_75_GE2J
        {312  , {0.7        , 3.3        , -0.53      , 0          , 0          , 6          , 9.3       }}, // QQ2HLNU_PTV_75_150_GE2J
        {313  , {0.7        , 3.3        , 1.3        , -0.4       , 0          , 5.1        , 8         }}, // QQ2HLNU_PTV_150_250_GE2J
        {314  , {0.7        , 3.3        , 1.3        , 1.4        , -0.38      , 5.3        , 6.7       }}, // QQ2HLNU_PTV_250_400_GE2J
        {315  , {0.7        , 3.3        , 1.3        , 1.4        , 1.8        , 5.5        , 6.7       }}, // QQ2HLNU_PTV_GT400_GE2J

        {400  , {0.6        , 0          , 0          , 0          , 0          , 0          , 0         }}, // QQ2HLL_FWDH
        {401  , {0.6        , -2.9       , 0          , 0          , 0          , -3         , 0         }}, // QQ2HLL_PTV_0_75_0J
        {402  , {0.6        , 3.3        , -0.53      , 0          , 0          , -3.6       , 0         }}, // QQ2HLL_PTV_75_150_0J
        {403  , {0.6        , 3.3        , 1.3        , -0.4       , 0          , -4.1       , 0         }}, // QQ2HLL_PTV_150_250_0J
        {404  , {0.6        , 3.3        , 1.3        , 1.4        , -0.38      , -5.4       , 0         }}, // QQ2HLL_PTV_250_400_0J
        {405  , {0.6        , 3.3        , 1.3        , 1.4        , 1.8        , -6.8       , 0         }}, // QQ2HLL_PTV_GT400_0J
        {406  , {0.6        , -2.9       , 0          , 0          , 0          , 6.8        , -4.5      }}, // QQ2HLL_PTV_0_75_1J
        {407  , {0.6        , 3.3        , -0.53      , 0          , 0          , 6          , -4.7      }}, // QQ2HLL_PTV_75_150_1J
        {408  , {0.6        , 3.3        , 1.3        , -0.4       , 0          , 5.1        , -5        }}, // QQ2HLL_PTV_150_250_1J
        {409  , {0.6        , 3.3        , 1.3        , 1.4        , -0.38      , 5.3        , -5        }}, // QQ2HLL_PTV_250_400_1J
        {410  , {0.6        , 3.3        , 1.3        , 1.4        , 1.8        , 5.5        , -5.7      }}, // QQ2HLL_PTV_GT400_1J
        {411  , {0.6        , -2.9       , 0          , 0          , 0          , 6.8        , 0         }}, // QQ2HLL_PTV_0_75_GE2J
        {412  , {0.6        , 3.3        , -0.53      , 0          , 0          , 6          , 10        }}, // QQ2HLL_PTV_75_150_GE2J
        {413  , {0.6        , 3.3        , 1.3        , -0.4       , 0          , 5.1        , 9.3       }}, // QQ2HLL_PTV_150_250_GE2J
        {414  , {0.6        , 3.3        , 1.3        , 1.4        , -0.38      , 5.3        , 8         }}, // QQ2HLL_PTV_250_400_GE2J
        {415  , {0.6        , 3.3        , 1.3        , 1.4        , 1.8        , 5.5        , 6.7       }}, // QQ2HLL_PTV_GT400_GE2J

        {500  , {25         , 0          , 0          , 0          , 0          , 0          , 0         }}, // GG2HLL_FWDH
        {501  , {25         , -99        , 0          , 0          , 0          , -31        , 0         }}, // GG2HLL_PTV_0_75_0J
        {502  , {25         , 26         , -9.4       , 0          , 0          , -30        , 0         }}, // GG2HLL_PTV_75_150_0J
        {503  , {25         , 26         , 13         , -2.6       , 0          , -50        , 0         }}, // GG2HLL_PTV_150_250_0J
        {504  , {25         , 26         , 13         , 14         , -1.3       , -99        , 0         }}, // GG2HLL_PTV_250_400_0J
        {505  , {25         , 26         , 13         , 14         , 15         , -99        , 0         }}, // GG2HLL_PTV_GT400_0J
        {506  , {25         , -99        , 0          , 0          , 0          , 25         , -15       }}, // GG2HLL_PTV_0_75_1J
        {507  , {25         , 26         , -9.4       , 0          , 0          , 25         , -15       }}, // GG2HLL_PTV_75_150_1J
        {508  , {25         , 26         , 13         , -2.6       , 0          , 26         , -20       }}, // GG2HLL_PTV_150_250_1J
        {509  , {25         , 26         , 13         , 14         , -1.3       , 28         , -38       }}, // GG2HLL_PTV_250_400_1J
        {510  , {25         , 26         , 13         , 14         , 15         , 30         , -66       }}, // GG2HLL_PTV_GT400_1J
        {511  , {25         , -99        , 0          , 0          , 0          , 25         , 25        }}, // GG2HLL_PTV_0_75_GE2J
        {512  , {25         , 26         , -9.4       , 0          , 0          , 25         , 26        }}, // GG2HLL_PTV_75_150_GE2J
        {513  , {25         , 26         , 13         , -2.6       , 0          , 26         , 26        }}, // GG2HLL_PTV_150_250_GE2J
        {514  , {25         , 26         , 13         , 14         , -1.3       , 28         , 28        }}, // GG2HLL_PTV_250_400_GE2J
        {515  , {25         , 26         , 13         , 14         , 15         , 30         , 30        }}  // GG2HLL_PTV_GT400_GE2J
    };




    // return a single weight for a given souce
    if(source < 7)
    {
      // Silent Return 1 for hadronic V decays
      if((event_STXS >= 100 && event_STXS <= 127)  &&  m_prodMode == "ggZH") return 1;
      if((event_STXS >= 200 && event_STXS <= 224)  &&  (m_prodMode == "qqZH" || m_prodMode == "WH")) return 1;
      

      if(!(event_STXS >= 300 && event_STXS <= 315) && !(event_STXS >= 400 && event_STXS <= 415) && !(event_STXS >= 500 && event_STXS <= 515))
      {
        ANA_MSG_WARNING("VHlep - cannot recongnize STXS. Returning 1");
        ANA_MSG_WARNING("event_STXS: " + std::to_string(event_STXS));
        return 1.0;
      }

      double sys = stxs_error[event_STXS][source]/100;
      return  1.0 + Nsigma * sys;
    }
    else
    {
      ANA_MSG_WARNING("VHlep - cannot recongnize source. Returning 1");
      ANA_MSG_WARNING("source: " + std::to_string(source));
      return 1.0;
    }
  }



  /// Read XML weight descriptions into a vector of strings
  std::vector<std::string> HiggsWeightTool::loadXMLWeightNames(const std::string &filename)
  {
    // Open the file and get the root element
    xmlDoc *doc = xmlReadFile(PathResolverFindCalibFile(filename).c_str(), NULL, 0);
    xmlNode *root_element = xmlDocGetRootElement(doc);

    // Define element names that we are looking for
    const xmlChar *schemaElem = reinterpret_cast<const unsigned char *>("weightschema");
    const xmlChar *weightElem = reinterpret_cast<const unsigned char *>("weight");

    // Get the (first) weightschema element
    xmlNode *weightschema = nullptr;

    for (auto node = root_element; node; node = node->next) {
      if (xmlStrEqual(node->name, schemaElem)) {
        weightschema = node;
        break;
      }
    }

    // Get the individual weight elements
    std::vector<std::string> weightNames;

    for (auto node = weightschema->children; node; node = node->next) 
    {
       std::stringstream ss;
       ss<<"Name: " << node->name << ", content: '" << xmlNodeGetContent(node) << "'\n";
       ANA_MSG_DEBUG(ss.str());

      if (xmlStrEqual(node->name, weightElem)) {
        weightNames.push_back(reinterpret_cast<char *>(xmlNodeGetContent(node)));
      }
    }

    xmlFreeDoc(doc);
    return weightNames;
  }


  double HiggsWeightTool::linInter(double x, double x1, double y1, double x2, double y2)
  {
    if (x < x1) { return y1; }

    if (x > x2) { return y2; }

    return y1 + (y2 - y1) * (x - x1) / (x2 - x1);
  }


  double HiggsWeightTool::getWeight(const std::vector<float> &ws, size_t idx)
  {
    if (idx == 0) { return 0.0; }

    return ws.at(idx);
  }


  size_t HiggsWeightTool::getIndex(std::string wn)
  {
    if (hasWeight(wn)) { return getWeightIndex(wn); }

    return 0;
  }

} // namespace TruthWeightTools
